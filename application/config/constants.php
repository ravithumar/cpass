<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code




defined('BASE_URL')      OR define('BASE_URL', 'http://localhost/cpass/'); // highest automatically-assigned error code
defined('S3_URL')      OR define('S3_URL', 'https://c-pass.s3.eu-west-2.amazonaws.com/production/'); // highest automatically-assigned error code
defined('LIMIT')      OR define('LIMIT', 50); // highest automatically-assigned error code
defined('TAX_PRICE')      OR define('TAX_PRICE', 50); // highest automatically-assigned error code
defined('MIN_PRICE')      OR define('MIN_PRICE', 10); // highest automatically-assigned error code
defined('MAX_PRICE')      OR define('MAX_PRICE', 500); // highest automatically-assigned error code
defined('MIN_KM')      OR define('MIN_KM', 1); // highest automatically-assigned error code
defined('MAX_KM')      OR define('MAX_KM', 50); // highest automatically-assigned error code


defined('SMTP_HOST')      OR define('SMTP_HOST', "ssl://smtp.gmail.com");
defined('SMTP_PORT')      OR define('SMTP_PORT', '465');
defined('SMTP_USER')      OR define('SMTP_USER', "dev.cpass@gmail.com");
defined('SMTP_PASS')      OR define('SMTP_PASS', "agrbalvedrmqjlwb");
defined('SMTP_FROM')      OR define('SMTP_FROM', "dev.cpass@gmail.com");
defined('SMTP_HEADER')    OR define('SMTP_HEADER', "C-PASS ADMIN");
defined('MAIL_TYPE')      OR define('MAIL_TYPE', "SMTP");

// ------------- ship station --------------

defined('SHIP_API_KEY')      OR define('SHIP_API_KEY', "dd99c89d66184e1da7222f379e0dfc86");
defined('SHIP_SECRET_KEY')   OR define('SHIP_SECRET_KEY', "b71492587b49461983bd57df2ff982c1");
defined('SHIP_URL')      	 OR define('SHIP_URL', "https://ssapi.shipstation.com/");

// ------------- JUMIO  --------------

defined('JUMIO_API_KEY')      OR define('JUMIO_API_KEY', "0a850024-3992-4069-aa1e-ba1a7b324bd4");
defined('JUMIO_SECRET_KEY')   OR define('JUMIO_SECRET_KEY', "hDCdYojepsvB1kf1C1hVU1sqtjuhkDgk");

// ------------ Image upload URL ----------
defined('IMAGE_UPLOAD_API_URL')   OR define('IMAGE_UPLOAD_API_URL', "http://54.173.152.252/corona-new/api/image-with-url");