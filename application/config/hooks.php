<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$hook['pre_system']                 = array(
    'class' => 'PreSystem',
    'function' => 'index',
    'filename' => 'PreSystem.php',
    'filepath' => 'hooks'
);
$hook['post_controller_constructor'] = array(
    'class' => 'PostControllerConstructor',
    'function' => 'index',
    'filename' => 'PostControllerConstructor.php',
    'filepath' => 'hooks'
);
$hook['post_controller']             = array(
    'class' => 'PostController',
    'function' => 'index',
    'filename' => 'PostController.php',
    'filepath' => 'hooks'
);
$hook['pre_controller']             = array(
    'class' => 'PreController',
    'function' => 'index',
    'filename' => 'PreController.php',
    'filepath' => 'hooks'
);
// $hook['display_override'] = array(
//    'class'    => 'DisplayOverride',
//    'function' => 'maintenance',
//    'filename' => 'DisplayOverride.php',
//    'filepath' => 'hooks'
// );
// $hook['cache_override']              = array(
//     'class' => 'CacheOverride',
//     'function' => 'index',
//     'filename' => 'CacheOverride.php',
//     'filepath' => 'hooks'
// );
$hook['post_system']                 = array(
    'class' => 'PostSystem',
    'function' => 'index',
    'filename' => 'PostSystem.php',
    'filepath' => 'hooks'
);