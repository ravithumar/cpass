<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */

$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['admin/change/status/(:any)/(:num)'] = 'Admin/AdminController/change_status/$1/$2';
$route['admin/system/setting'] = 'Admin/SystemController/index';
$route['admin/app/setting'] = 'Admin/SystemController/app_setting';
$route['admin/app/save_setting'] = 'Admin/SystemController/save_app_config';
$route['admin'] = 'Admin/DashboardController/index';
$route['check-duplicate-data'] = 'CommonController/check_duplicate_data';

//website

$route['jumiotest/demo'] = 'api/Jumiotest/demo';
$route['jumio'] = 'Web/JumioController/index';
$route['home'] = 'Web/HomeController/index';
$route['accept_terms_condition'] = 'Web/HomeController/accept_terms_condition';
$route['services'] = 'Web/HomeController/services';
$route['about-us'] = 'Web/HomeController/about_us';
$route['contact-us'] = 'Web/HomeController/contact_us';
$route['privacy-policy'] = 'Web/HomeController/privacy_policy';
$route['terms-conditions'] = 'Web/HomeController/terms_conditions';
$route['test-email'] = 'Web/HomeController/test_email';
$route['login'] = 'Web/LoginController/index';
$route['signup'] = 'Web/LoginController/signup';
$route['otp-verify'] = 'Web/LoginController/otp_verify';
$route['resend-otp'] = 'Web/LoginController/resend_otp';
$route['verification-success'] = 'Web/LoginController/verification_success';
////$route['activate-account'] = 'Web/LoginController/activate_account';
//$route['activate-account/(:any)'] = 'auth/activate_account/$1';
$route['forgot-password'] = 'Web/LoginController/forgot_password';
$route['edit-profile'] = 'Web/LoginController/edit_profile';
$route['change-password'] = 'Web/LoginController/change_password/$1';
$route['resets-password'] = 'Web/LoginController/reset_password';
$route['resets-password/(:any)'] = 'Web/LoginController/reset_password/$1';
$route['clinic-registration'] = 'Web/LoginController/clinic_registration';  

// Booking
$route['new-booking'] = 'Web/BookingController/index';
$route['new-booking/demo'] = 'Web/BookingDemoController/index';
$route['search'] = 'Web/BookingController/search';
$route['complete-your-registration'] = 'Web/BookingController/complete_your_registration';
$route['select-members'] = 'Web/BookingController/select_members';
$route['scan-documents'] = 'Web/BookingController/scan_documents';
$route['time-slots'] = 'Web/BookingController/time_slot';
$route['review-confirm'] = 'Web/BookingController/review_confirm';
$route['payment'] = 'Web/BookingController/payment';
$route['add-cart'] = 'Web/BookingController/add_cart';
$route['search-map'] = 'Web/BookingController/search_map';
$route['get-hours'] = 'Web/BookingController/get_hours';
$route['get-report-test'] = 'Web/BookingController/get_report_test';
$route['get-report-test1'] = 'Web/BookingDemoController/get_report_test';
$route['get-clinic-from-map'] = 'Web/BookingController/get_clinic_from_map';

//Added by Nayan
$route['get-vaccine-dose-details/(:num)'] = 'Web/BookingController/get_vaccine_dose_detail/$1';
//End Added

$route['booking/member/edit/(:num)'] = 'Web/BookingController/add_booking_member/$1';
$route['save_booking_session'] = 'Web/BookingController/save_booking_session';
$route['save_booking_session_demo'] = 'Web/BookingDemoController/save_booking_session';

$route['booking/member/remove/(:num)/(:num)'] = 'Web/BookingController/booking_member_remove/$1/$2';
$route['is_member_selected'] = 'Web/BookingController/is_member_selected';
$route['select-payment-card'] = 'Web/BookingController/get_payment_cards';
$route['add-payment-card'] = 'Web/BookingController/add_card';
$route['book'] = 'Web/BookingController/book';
$route['save-hospital-id/(:num)'] = 'Web/BookingController/save_hospital_id/$1';
$route['tracking/(:num)'] = 'Web/BookingController/track_data/$1';

// My Booking
$route['my-booking'] = 'Web/MyBookingController/index';
$route['my-booking/detail/(:any)'] = 'Web/MyBookingController/detail/$1';
$route['my-booking/upload-plf-form/(:num)/(:num)'] = 'Web/MyBookingController/upload_plf/$1/$2';
//Added by Nayan
$route['my-booking/upload-kit/(:any)'] = 'Web/MyBookingController/upload_kit/$1';

// Members
$route['members'] = 'Web/MembersController/index';
$route['members/add'] = 'Web/MembersController/add';
$route['members/add-member'] = 'Web/MembersController/add_member';
$route['members/edit-member/(:num)'] = 'Web/MembersController/edit_member/$1';
$route['members/add/save'] = 'Web/MembersController/save';
$route['members/edit/(:num)'] = 'Web/MembersController/edit/$1';
$route['members/scan-documents'] = 'Web/MembersController/scan_documents';
$route['booking-edit-member/(:num)/(:any)'] = 'Web/MembersController/booking_edit_member/$1/$2';
$route['booking-edit-member/(:num)'] = 'Web/MembersController/booking_edit_member/$1';
$route['members/delete/(:any)'] = 'Web/MembersController/delete/$1';
$route['details/edit'] = 'Web/BookingController/edit_detail';
$route['edit_detail_save'] = 'Web/BookingController/edit_detail_save';
$route['get-jumio-status'] = 'Web/BookingController/get_jumio_status';

$route['get-addresses-by-postcode'] = 'Web/MembersController/get_addresses_by_postcode';
$route['review-confirm/(:any)'] = 'Web/MembersController/review_confirm/$1';
$route['expire-token'] = 'Web/MembersController/expire_token';

// Fit to Fly
$route['fit-to-fly/clinic-detail'] = 'Web/FitToFlyController/clinic_detail';
$route['fit-to-fly/search-map'] = 'Web/FitToFlyController/search_map';
$route['fit-to-fly/search'] = 'Web/FitToFlyController/search';
$route['fit-to-fly/third-step'] = 'Web/FitToFlyController/third_step';
$route['fit-to-fly/second-step'] = 'Web/FitToFlyController/second_step';
$route['fit-to-fly/scan-document'] = 'Web/FitToFlyController/scan_document';
$route['fit-to-fly/complete-your-registration'] = 'Web/FitToFlyController/complete_your_registration';
$route['fit-to-fly/member-list'] = 'Web/FitToFlyController/member_list';
$route['fit-to-fly/review-confirm'] = 'Web/FitToFlyController/review_confirm';
$route['fit-to-fly/payment'] = 'Web/FitToFlyController/payment';
$route['fit-to-fly/add-card'] = 'Web/FitToFlyController/add_card';
$route['fit-to-fly/international-search'] = 'Web/FitToFlyController/international_search';
$route['fit-to-fly/edit-detail'] = 'Web/FitToFlyController/edit_detail';
$route['fit-to-fly/edit-profile'] = 'Web/FitToFlyController/edit_profile';

// Lab
$route['lab'] = 'Lab/DashboardController';

// Government
$route['government'] = 'Gov/DashboardController';

// Hospital
$route['hospital']          = 'Hospital/DashboardController';
$route['hospital/lab']      = 'Hospital/LabController/index';
$route['hospital/booking']  = 'Hospital/BookingController/index';
$route['hospital/booking/details/(:any)'] = 'Hospital/BookingController/details/$1';
$route['hospital/lab/add']  = 'Hospital/LabController/add';
$route['hospital/lab/edit/(:any)'] = 'Hospital/LabController/edit/$1';

// Network
$route['network'] = 'Network/DashboardController';
$route['network/dashboard/test'] = 'Network/DashboardController/test';

$route['network/clinic'] = 'Network/ClinicController/index';
$route['network/clinic/add'] = 'Network/ClinicController/add';
$route['network/clinic/edit/(:any)'] = 'Network/ClinicController/edit/$1';
$route['network/clinic/details/(:any)'] = 'Network/ClinicController/details/$1';
$route['network/clinic/delete/(:num)'] = 'Network/ClinicController/delete/$1';

$route['network/booking'] = 'Network/BookingController/index';
$route['network/booking/edit/(:any)'] = 'Network/BookingController/edit/$1';
$route['network/booking/delete/(:num)'] = 'Network/BookingController/delete/$1';
$route['network/change-booking-status'] = 'Network/BookingController/change_booking_status';
$route['network/change-booking-detail-status'] = 'Network/BookingController/change_booking_detail_status';
$route['network/booking/details/(:any)'] = 'Network/BookingController/details/$1';
$route['network/send-certificate'] = 'Network/BookingController/sendCertificate';
$route['network/booking-history'] = 'Network/BookingController/history';


$route['network/shipStation/getReports']               = 'Network/ShipStationController/get_reports';
$route['network/order_edit_popup']          = 'Network/ShipStationController/order_edit_popup';
$route['network/shipStation']               = 'Network/ShipStationController/index';
$route['network/shipStation/shipAwaiting']  = 'Network/ShipStationController/ship_awaiting';
$route['network/shipStation/get_order_detail']  = 'Network/ShipStationController/get_order_detail';
$route['network/shipStation/save_barcode']  = 'Network/ShipStationController/save_barcode';
$route['network/shipStation/save_shipstation_order']  = 'Network/ShipStationController/save_shipstation_order';
$route['network/shipStation/initiate']      = 'Network/ShipStationController/initiate_fulfillment';
$route['network/shipStation/carriers/(:any)']      = 'Network/ShipStationController/carriers_detail/$1';
$route['network/shipStation/carriers']      = 'Network/ShipStationController/carriers';
$route['network/shipStation/products/detail/(:any)']      = 'Network/ShipStationController/product_detail/$1';
$route['network/shipStation/products']      = 'Network/ShipStationController/product_list';
$route['network/shipStation/stores']      = 'Network/ShipStationController/store_list';
$route['network/shipStation/stores/detail/(:any)']      = 'Network/ShipStationController/store_detail/$1';
$route['network/shipStation/shipments']      = 'Network/ShipStationController/shipments_list';
$route['network/shipStation/create-label-for-order/(:any)'] = 'Network/ShipStationController/create_label_for_order/$1';
$route['network/shipStation/create-order-label'] = 'Network/ShipStationController/create_order_label';
$route['network/slot'] = 'Network/SlotController/index';
$route['network/slot/add'] = 'Network/SlotController/add';
$route['network/slot/edit/(:num)'] = 'Network/SlotController/edit/$1';
$route['network/slot/details/(:any)'] = 'Network/SlotController/details/$1';
$route['network/slot/delete/(:num)'] = 'Network/SlotController/delete/$1';

$route['network/block-slot'] = 'Network/BlockslotController/index';
$route['network/block-slot/add'] = 'Network/BlockslotController/add';
$route['network/block-slot/edit/(:num)'] = 'Network/BlockslotController/edit/$1';
$route['network/block-slot/details/(:any)'] = 'Network/BlockslotController/details/$1';
$route['network/block-slot/delete/(:num)'] = 'Network/BlockslotController/delete/$1';

$route['network/test-tracking'] = 'Network/AftershipController';
$route['network/tracking'] = 'Network/AftershipController/track_data';

// User Network 
$route['network/user'] = 'Network/userController/index';
$route['network/user/details/(:any)'] = 'Network/userController/details/$1';
$route['network/user/delete/(:num)'] = 'Network/userController/delete/$1';

// admin network,clinic,lab
$route['admin/lab'] = 'Admin/LabController/index';
$route['admin/lab/edit/(:any)'] = 'Admin/LabController/edit/$1';
$route['admin/lab/details/(:any)'] = 'Admin/LabController/details/$1';
$route['admin/clinic'] = 'Admin/ClinicController/index';
$route['admin/clinic/add'] = 'Admin/ClinicController/add';
$route['admin/clinic/edit/(:any)'] = 'Admin/ClinicController/edit/$1';
$route['admin/clinic/details/(:any)'] = 'Admin/ClinicController/details/$1';
$route['admin/clinic/delete/(:num)'] = 'Admin/ClinicController/delete/$1';
$route['admin/network'] = 'Admin/NetworkController/index';
$route['admin/network/add'] = 'Admin/NetworkController/add';
$route['admin/network/edit/(:any)'] = 'Admin/NetworkController/edit/$1';
$route['admin/network/delete/(:num)'] = 'Admin/NetworkController/delete/$1';
$route['admin/booking'] = 'Admin/BookingController/index';
$route['admin/booking/details/(:any)'] = 'Admin/BookingController/details/$1';
$route['admin/booking/delete/(:num)'] = 'Admin/BookingController/delete/$1';

$route['admin/block-slot'] = 'Admin/BlockslotController/index';
$route['admin/block-slot/add'] = 'Admin/BlockslotController/add';
$route['admin/block-slot/edit/(:num)'] = 'Admin/BlockslotController/edit/$1';
$route['admin/block-slot/details/(:any)'] = 'Admin/BlockslotController/details/$1';
$route['admin/block-slot/delete/(:num)'] = 'Admin/BlockslotController/delete/$1';

// Report
$route['admin/report'] = 'Admin/ReportController/index';
$route['admin/report/add'] = 'Admin/ReportController/add';
$route['admin/report/edit/(:any)'] = 'Admin/ReportController/edit/$1';
$route['admin/report/delete/(:num)'] = 'Admin/ReportController/delete/$1';

// Report Type
$route['admin/report-type'] = 'Admin/ReportTypeController/index';
$route['admin/report-type/add'] = 'Admin/ReportTypeController/add';
$route['admin/report-type/edit/(:any)'] = 'Admin/ReportTypeController/edit/$1';
$route['admin/report-type/delete/(:num)'] = 'Admin/ReportTypeController/delete/$1';
$route['change-password/(:any)'] = 'auth/change_password/$1';
$route['reset-password/(:any)'] = 'auth/reset_password/$1';

// User 
$route['admin/user'] = 'Admin/userController/index';
$route['admin/user/details/(:any)'] = 'Admin/userController/details/$1';
$route['admin/user/delete/(:num)'] = 'Admin/UserController/delete/$1';

// Category
$route['admin/category'] = 'Admin/CategoryController/index';
$route['admin/category/add'] = 'Admin/CategoryController/add';
$route['admin/category/edit/(:any)'] = 'Admin/CategoryController/edit/$1';
$route['admin/category/delete/(:num)'] = 'Admin/CategoryController/delete/$1';
$route['admin/category/sub/(:num)'] = 'Admin/CategoryController/sub_category/$1';
$route['admin/category/sub/add/(:num)'] = 'Admin/CategoryController/sub_category_add/$1';
$route['admin/category/sub/edit/(:num)'] = 'Admin/CategoryController/sub_category_edit/$1';
$route['admin/category/sub/delete/(:num)'] = 'Admin/CategoryController/sub_delete/$1';

//Country
$route['admin/country'] = 'Admin/CountryController/index';
$route['admin/country/add'] = 'Admin/CountryController/add';
$route['admin/country/edit/(:num)'] = 'Admin/CountryController/edit/$1';
$route['admin/country/delete/(:num)'] = 'Admin/CountryController/delete/$1';

// Sub Category
$route['admin/sub-category'] = 'Admin/SubcategoryController/index';
$route['admin/sub-category/add'] = 'Admin/SubcategoryController/add';
$route['admin/sub-category/edit/(:any)'] = 'Admin/SubcategoryController/edit/$1';

// Services
$route['admin/service'] = 'Admin/ServicesController/index';
$route['admin/service/add'] = 'Admin/ServicesController/add';
$route['admin/service/edit/(:any)'] = 'Admin/ServicesController/edit/$1';
$route['admin/service/remove-image'] = 'Admin/ServicesController/remove_image';
$route['admin/service/profile'] = 'Admin/ServicesController/profile';

//Certified Vaccines
$route['admin/certified-vaccines'] = 'Admin/CertifiedVaccinesController/index';
$route['admin/certified-vaccines/add'] = 'Admin/CertifiedVaccinesController/add';
$route['admin/certified-vaccines/edit/(:num)'] = 'Admin/CertifiedVaccinesController/edit/$1';
$route['admin/certified-vaccines/delete/(:num)'] = 'Admin/CertifiedVaccinesController/delete/$1';

//Zone
$route['admin/zone'] = 'Admin/ZoneController/index';
$route['admin/zone/add'] = 'Admin/ZoneController/add';
$route['admin/zone/edit/(:num)'] = 'Admin/ZoneController/edit/$1';
$route['admin/zone/delete/(:num)'] = 'Admin/ZoneController/delete/$1';

$route['auth/forgot-password'] = 'auth/forgot_password/$1';

// Maulik Changes

$route['roundtrip-search'] = 'Web/BookingController/roundtrip_search';
$route['booking/member/edit'] = 'Web/BookingController/add_booking_member';
$route['booking/member/remove-selected-member/(:num)/(:num)'] = 'Web/BookingController/remove_selected_member/$1/$2';	
$route['booking/member/remove-selected-member/(:num)/(:num)/(:num)'] = 'Web/BookingController/remove_selected_member/$1/$2/$3';

$route['fit-to-fly-return'] = 'Web/BookingController/fit_to_fly_return';	
$route['fit-to-fly-arrival'] = 'Web/BookingController/fit_to_fly_arrival';	

$route['get-address-list'] = 'Web/BookingController/get_address_list';	
$route['save-fit-to-fly-return'] = 'Web/BookingController/save_fit_to_fly_return';	
$route['member-list'] = 'Web/BookingController/member_list';



$route['network/change-booking-status-demo/(:any)'] = 'Network/BookingController/change_booking_status_demo/$1';


$route['about-us/(:any)'] = 'Web/HomeController/about_us/$1';
$route['data-privacy'] = 'Web/HomeController/data_privacy';
$route['data-privacy/(:any)'] = 'Web/HomeController/data_privacy/$1';
$route['registration-consent'] = 'Web/HomeController/registration_consent';
$route['registration-consent/(:any)'] = 'Web/HomeController/registration_consent/$1';
$route['privacy'] = 'Web/HomeController/privacy';
$route['privacy/(:any)'] = 'Web/HomeController/privacy/$1';


$route['cronjob/booking_delete'] = 'CronjobController/delete_bookings';
