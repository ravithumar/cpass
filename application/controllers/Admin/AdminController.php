<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AdminController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
	}
	public function change_status($table,$id)
	{		
		$data = $this->db->get_where($table,['id'=>$id])->row();
		$status = 1;
		if($table == 'users')
		{
			if($data->active == 1)
			{
				$status = 0;
			}	
			$this->db->where('id',$id)->update($table,['active'=>$status]);
		}
		else
		{
			if($data->status == 1)
			{
				$status = 0;
			}	
			$this->db->where('id',$id)->update($table,['status'=>$status]);
		}
		
			
		echo $status;
	}
	
}