<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
		$this->load->model('Lab');
		$this->load->model('Bookings');
		$this->load->model('BookingSession');
		$this->load->model('BookingSlots');
		$this->load->model('BookingsMembers');
        $this->table_name = "booking";
        $this->load->model('user');
        $this->title = "Booking";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);
	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');

        $filter_request = $this->input->post();
        $where = [];
        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();

        // $user_id =  $this->session->userdata('hospital')['user_id'];
        // $hospitalArr = $this->db->get_where('hospital',['user_id'=>$user_id])->row();
      
        $product->select('booking.id, booking.booking_no,users.phone,hospital.company_name,booking.type,booking.total_price,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website, booking.status,booking.booking_status', false)
            ->from("booking")
            ->where('booking.is_deleted','0')
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('users', 'booking_member.user_id = users.id')
            // ->where('booking_member.clinic_id',$hospitalArr->id)
            ->where($where)
            ->group_by('booking.id');

        $action['view'] = base_url('admin/booking/details/');
        $action['delete'] = base_url('admin/booking/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Reference Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'type',function ($type){
                return ucfirst($type);
            })
            ->column('Contact No', 'phone')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
	            if ($status == "success") {
	               return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Success</span></h5>';
	            }
                
	            else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst($status).'</span></h5>';
	            }else{
	                return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst($status).'</span></h5>';
                }
	        })
           
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['view'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Field"><i class="la la-eye "></i></a>';

                 $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="booking"><i class="fa fa-trash"></i></a>';

		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('booking/index', $data);
	}
	
       public function details($id = '')
    {

        $this->db->select('booking_member.*,booking.type,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->result_array();

        if (!empty($data['booking'])) {
            foreach ($data['booking'] as $key => $value) {
                $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                if ($value['is_excempted'] == "yes") {
                    $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                    if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                }else{  $data['booking'][$key]['cat_name'] = ""; }

                if($value['vaccine_id'] != ''){
                    $vaccine = $this->db->get_where('certified_vaccines',array('id'=>$value['vaccine_id']))->row_array();
                    $data['booking'][$key]['vaccine_name'] = $vaccine['name'];
                }

                if($value['destination_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['destination_country']))->row_array();
                    $data['booking'][$key]['destination_country'] = $country['name'];
                }

                if($value['is_visiting_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['is_visiting_country']))->row_array();
                    $data['booking'][$key]['is_visiting_country'] = $country['name'];
                }

                if($value['departure_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['departure_country']))->row_array();
                    $data['booking'][$key]['departure_country'] = $country['name'];
                }

                if($value['return_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['return_country']))->row_array();
                    $data['booking'][$key]['return_country'] = $country['name'];
                }
                
                $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row(); 
                $this->db->select('booking_member.id,booking_member.booking_id,booking_member.reference_member_id,booking_member.report_id,booking_member.shipping_method,booking_member.shipping_date,booking_member.pickup_date,booking_member.report_status,booking_member.report_file,booking.type as purpose,hospital.company_name,booking_slots.date as slot_date,booking_slots.slots as slot_time');
                $this->db->from("booking_member");
                $this->db->join('booking', 'booking.id = booking_member.booking_id');
                $this->db->join('users', 'booking_member.user_id = users.id');
                $this->db->join('hospital','booking_member.clinic_id = hospital.id','left');
                $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left');
                $this->db->where('booking_member.user_id',$value['user_id']);
                $this->db->where_not_in('booking_member.booking_id', [$id]);
                $this->db->order_by('booking_member.booking_id',"desc");
                $query = $this->db->get();
                $booking_history = $query->result_array();
                if(!empty($booking_history)){
                    foreach($booking_history as $booking_key=>$booking_value){
                        $report_name = get_column_value("reports",array("id"=>$booking_value['report_id']),"name");
                        $booking_history[$booking_key]['report_name'] = $report_name;
                    }
                }
                
                $data['booking'][$key]['booking_history'] = $booking_history;

                
                

            }
        }

        $data['home'] = base_url('admin/booking');
        $data['title'] = "Booking Details";
        $data['main_title'] = $this->title;
        $this->renderAdmin('booking/details', $data);

    }


    public function test($value='')
    {
        // code...
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo "1";
    }
        
    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
