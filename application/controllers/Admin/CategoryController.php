<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CategoryController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
        $this->load->model('Category');
		$this->load->model('Report');
		$this->load->model('Zone');
        $this->table_name = "category";
        $this->title = "Category";
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('*', false)
            ->from($this->table_name);
        $action['edit'] = base_url('admin/category/edit/');
		$action['delete'] = base_url('admin/category/delete/');
        $action['add_cat'] = base_url('admin/category/sub/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'name')
            // ->column('Image', 'image', function ($image_url)
	        // {
	        //     if ($image_url != '' && isset($image_url))
	        //     {
	        //         $option = '<img src="' . base_url() . $image_url . '" name="product_image" class="image_size">';
	        //     }
	        //     else
	        //     {
	        //         $image_url = "assets/images/default-profile.png";
	        //         $option = '<img src="' . base_url() . $image_url . '" name="product_image" class="image_size">';
	        //     }
	        //     return $option;
	        // })
            ->column('Approve', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
		        $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model mr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="country"><i class="fa fa-trash"></i></a>';
                $option .= '<a href="' . $action['add_cat'] . $id . '"  class="on-default text-info" data-toggle="tooltip" data-placement="bottom" title=""><u>Category Rules</u></a>';
				return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/category/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('category/index', $data);
    }

    public function add()
    {
        $data['title'] = "Category Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/category');
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
            // if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name']))
            // {
            //     $image_is_uploaded = image_upload('image', 'images/category/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
            //     if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])
            //     {
            //         $post['image'] = $image_is_uploaded['uploaded_path'];
            //     }
            // }
            Category::insert($post);
            $this->session->set_flashdata('success', __('Category Add successfully'));
            redirect('admin/category');
        }
        $this->renderAdmin('category/add', $data);
    }

    public function edit($id = '')
    {
        $data['category'] = Category::find($id);
        $data['home'] = base_url('admin/category');
        $data['title'] = "Category Edit";
        $data['main_title'] = $this->title;
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
            // if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name']))
            // {
            //     $image_is_uploaded = image_upload('image', 'images/category/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
            //     if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])
            //     {
            //         $post['image'] = $image_is_uploaded['uploaded_path'];
            //     }
            // }
            Category::where('id', $id)->update($post);
            $this->session->set_flashdata('success', __('Category Update successfully'));
            redirect('admin/category');
        }
        $this->renderAdmin('category/edit', $data);
    }

	public function delete($id) {

        $current_date = current_date();

        $this->db->delete($this->table_name, array('id' => $id));

        echo 1;
	}

    public function sub_category($id = '')
    {
        $data['title'] = 'Category Rules';
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('*', false)
            ->from('category_rules')
            ->where('cat_id',$id);
        $action['edit'] = base_url('admin/category/sub/edit/');
        $action['delete'] = base_url('admin/category/sub/delete/');
        $action['add_cat'] = base_url('admin/category/sub/add/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Description', 'description')
            ->column('Zone', 'zone', function ($zone)
            {
               if(!empty($zone)){
					$zone_array = explode(',',$zone);
					$zone_names = Zone::whereIn("id",$zone_array)->pluck("name")->toArray();
					return implode(', ',$zone_names);
			   }
			   return "";
            })
			->column('Report Name', 'report_id', function ($report_id)
            {
               if(!empty($report_id)){
					$report_array = explode(',',$report_id);
					$report_names = Report::whereIn("id",$report_array)->pluck("name")->toArray();
					return implode(', ',$report_names);
			   }
			   return "";
            })
            ->column('Actions', 'id', function ($id) use ($action)
            {
                $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model mr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="category_rules" data-href="country"><i class="fa fa-trash"></i></a>';
                return $option;
            });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/category/sub/add/'.$id);        
        $data['title'] = 'Category Rules';
        $data['main_title'] = 'Category Rules';
        $this->renderAdmin('category/sub_index', $data);
    }

     public function sub_category_add($id = '')
    {
        $data['title'] = "Category Rules Add";
        $data['main_title'] = 'Category Rules';
        $data['home'] = base_url('admin/category');
        $data['days'] = $this->db->get_where('days',array('status' => '1'))->result_array();
        $data['reports'] = $this->db->get_where('reports',array('status' => '1'))->result_array();
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
		$data['vaccine_status'] = $this->config->item('vaccine_status');
        $data['id'] = $id;

        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            
            $post_data['cat_id'] = $id;
            $post_data['zone'] = implode(',', $request['zone']);
            $post_data['vaccination_status'] = implode(',', $request['vaccination_status']);
            $post_data['description'] = $request['description'];
            $post_data['days'] = implode(',', $request['days']);
            $post_data['report_id'] = implode(',', $request['report_id']);
            // _pre($post_data);

            $this->db->insert('category_rules',$post_data);
            $this->session->set_flashdata('success', __('Category Rules Add successfully'));
            redirect('admin/category/sub/'.$id);
        }
        $this->renderAdmin('category/sub_add', $data);
    }

    public function sub_category_edit($id = '')
    {
        $data['sub_category'] = $this->db->get_where('category_rules',array('id' => $id))->row_array();
        $data['home'] = base_url('admin/category');
        $data['title'] = "Category Rules Edit";
        $data['main_title'] = "Category Rules Edit";
        $data['days'] = $this->db->get_where('days',array('status' => '1'))->result_array();
        $data['reports'] = $this->db->get_where('reports',array('status' => '1'))->result_array();
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
		$data['vaccine_status'] = $this->config->item('vaccine_status');
        $data['id'] = $id;
        $cat_id = $data['sub_category']['cat_id'];

        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();

            $post_data['zone'] = implode(',', $request['zone']);
            $post_data['vaccination_status'] = implode(',', $request['vaccination_status']);
            $post_data['description'] = $request['description'];
            $post_data['days'] = implode(',', $request['days']);
            $post_data['report_id'] = implode(',', $request['report_id']);
     
            $this->db->update('category_rules',$post_data,array('id' => $id));
            $this->session->set_flashdata('success', __('Category Rules Update successfully'));
            redirect('admin/category/sub/'.$cat_id);
        }
        $this->renderAdmin('category/sub_edit', $data);
    }

    public function sub_delete($id) {

        $current_date = current_date();

        $this->db->delete('category_rules', array('id' => $id));

        echo 1;
    }
}

