<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CertifiedVaccinesController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
        $this->load->model('CertifiedVaccines');
        $this->table_name = "certified_vaccines";
        $this->title = "Certified Vaccines";
		$this->date = date('y-m-d');
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $country = new Datatables;

		$country->select('id, name, total_required_does, status', false)->from($this->table_name);
		$action['edit'] = base_url('admin/certified-vaccines/edit/');
		$action['delete'] = base_url('admin/certified-vaccines/delete/');
        $country->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'name')
			// ->column('Zone', 'zone')
			->column('Total required does for fully vaccinates', 'total_required_does', function ($zone)
	        {
                return ucfirst($zone);
	            
	        })
			->column('Status', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square"></i></a>';
		        
				$option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="certified-vaccines"><i class="fa fa-trash"></i></a>';
				return $option;
		    });

        //$country->set_options(["columnDefs" => "[ { targets: [4], sortable: true}]"]);
        $country->set_options(["columnDefs" => "[ { targets: [4], sortable: true}]"]);
        $country->searchable('id, name, total_required_does,status');
        $country->datatable($this->table_name);
        $country->init();
        $data['datatable'] = true;
		$data['export'] = true;
		$data['export_columns'] = [0,1,2];
        $data['add_url'] = base_url('admin/certified-vaccines/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('certified_vaccines/index', $data);
    }
    public function add()
    {
		$current_date = current_date();

        $data['title'] = "Certified Vaccine Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/certified-vaccines');
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
			$post['total_required_does'] = $request['total_required_does'];
			$post['days'] = $request['days'];
			$post['status'] = '1';
			$post['created_at'] = $current_date;
            CertifiedVaccines::insert($post);
            $this->session->set_flashdata('success', __('Vaccine added sucessfully'));
            redirect('admin/certified-vaccines');
        }
        $this->renderAdmin('certified_vaccines/add', $data);
    }

    public function edit($id = '') {
        $data['home'] = base_url('admin/certified-vaccines');
        $data['title'] = "Certified Vaccine Edit";        
        $data['main_title'] = $this->title;
        $data['row'] = CertifiedVaccines::find($id);
        // print_r($data); exit();
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
			$post['total_required_does'] = $request['total_required_does'];
			$post['days'] = $request['days'];
            CertifiedVaccines::where('id', $id)->update($post);
            $this->session->set_flashdata('success', __('Vaccine updated sucessfully'));
            redirect('admin/certified-vaccines');
        }
        $this->renderAdmin('certified_vaccines/edit', $data);
    }

	public function delete($id) {

        $current_date = current_date();

        $this->db->delete($this->table_name, array('id' => $id));

        echo 1;
	}
}

