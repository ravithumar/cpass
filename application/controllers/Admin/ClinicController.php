<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ClinicController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
		$this->load->model('Clinic');
        $this->table_name = "hospital";
        $this->load->model('User');
        $this->title = "Clinic";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('users.email,hospital.id, hospital.company_name,hospital.company_number,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website, hospital.status', false)
            ->from($this->table_name)
            ->where('hospital.is_deleted',NULL)
            ->join('users', 'users.id = hospital.user_id');
        $action['edit'] = base_url('admin/clinic/edit/');
        $action['details'] = base_url('admin/clinic/details/');
        $action['delete'] = base_url('admin/clinic/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Clinic Name', 'company_name')
            ->column('Email Address', 'email')
            ->column('Company Number', 'company_number')
            ->column('Company Website', 'company_website')
            ->column('No Of Employee', 'no_of_employee')
            ->column('Point Of Contact', 'point_of_contact')
            ->column('Approve', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
		        $option .= '<a href="' . $action['details'] . $id . '" class="on-default text-secondary ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Details Field"><i class="la la-eye "></i></a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="clinic"><i class="fa fa-trash"></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/clinic/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('clinic/index', $data);
	}
	public function add(){
        $data['title'] = "Clinic Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/clinic');
        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['all_county'] = $this->db->get_where('county',['status'=>'1'])->result();
        $data['purpose_type'] = $this->config->item("purpose_type"); 

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));
        $this->form_validation->set_rules('point_of_contact', 'Phone Number', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('county', 'County', 'required');
        $this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
        $this->form_validation->set_rules('report_type_id[]','Report id','trim|required');
        $this->form_validation->set_rules('purpose_type[]','Purpose type','trim|required');
        $this->form_validation->set_rules('price[]','Price','trim|required');

        // $this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
        // $this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[users.username]', array('is_unique' => __('This username is already exist!.')));
        // $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        // $this->form_validation->set_rules('daily_basis_case', 'Daily Basis Cases', 'trim|required|numeric');
        // $this->form_validation->set_rules('no_of_sites', 'No of Sites', 'trim|required');
        // $this->form_validation->set_rules('covid_testing', 'Covid Testing', 'trim|required');
        // $this->form_validation->set_rules('travel_covid_test', 'Travel Covid Test', 'trim|required');
        // $this->form_validation->set_rules('start_offering_service', 'Start Offering Service Date', 'trim|required');
        // $this->form_validation->set_rules('advertising', 'advertising', 'trim|required');

        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('clinic/add', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post();
                $post['network_id'] = $this->session->userdata('network')['user_id'];
                $input_password = '12345678'; 

                $post['network_id'] = $this->session->userdata('network')['user_id'];
                $post['company_name'] = $request['company_name'];
                //$user['username'] = $request['username'];
                $user['full_name'] = $request['company_name'] ? $request['company_name'] : NULL;
                $user['email'] = $request['email'];
                $user['postcode'] = $request['postal_code'] ? $request['postal_code'] : NULL;
                $user['address'] = $request['address'] ? $request['address'] : NULL;
                $user['phone'] = $request['company_number'] ? $request['company_number'] : NULL;

                $post['company_number'] = $request['company_number'] ? $request['company_number'] : NULL;
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['company_website'] = $request['company_website'] ? $request['company_website'] : NULL;
                $post['no_of_employee'] = $request['no_of_employee'] ? $request['no_of_employee'] : NULL;
                $post['point_of_contact'] = $request['point_of_contact'] ? $request['point_of_contact'] : NULL;
                $post['daily_basis_case'] = $request['daily_basis_case'] ? $request['daily_basis_case'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                $post['lat'] = $request['latitude'] ? $request['latitude'] : NULL;
                $post['lng'] = $request['longitude'] ? $request['longitude'] : NULL;
                $post['postal_code'] = $request['postal_code'] ? $request['postal_code'] : NULL;
                $post['no_of_sites'] = $request['no_of_sites'] ? $request['no_of_sites'] : NULL;
                $post['covid_testing'] = $request['covid_testing'] ? $request['covid_testing'] : NULL;
                $post['travel_covid_test'] = $request['travel_covid_test'] ? $request['travel_covid_test'] : NULL;
                $post['start_offering_service'] = $request['start_offering_service'] ? $request['start_offering_service'] : NULL;
                $post['advertising'] = $request['advertising'] ? $request['advertising'] : NULL;
                $post['county'] = $request['county'] ? $request['county'] : NULL;

                $tables = $this->config->item('tables', 'ion_auth'); 

                if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name']))
                {
                    $type = explode('.', $_FILES["profile_picture"]["name"]);
                    $type = strtolower($type[count($type) - 1]);
                    $name = mt_rand(100000000, 999999999);
                    $filename = $name . '.' . $type;
                    $url = "assets/images/users/" . $filename;
                    move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
                    $folder = "assets/images/users/";
                    s3Upload($url, $folder);
                    $image = S3_URL.$url;
                    $user['profile_picture'] = $image;
                }

                $user_id = $this->ion_auth->register($request['company_name'], $input_password, $request['email'], $user, [4], 1);
                 // _pre($user_id);
                $post['user_id'] = $user_id;

                $id = Clinic::insert($post);
                $clinic_id = $this->db->from('hospital')->order_by('id','DESC')->limit(1)->get()->row_array();

                if(isset($request['report_type_id']) && !empty($request['report_type_id'])){
                    foreach($request['report_type_id'] as $key=>$report_type_row){
                        $insert_report_price = array();
                        $insert_report_price['hospital_id'] = $clinic_id['id'];
                        $insert_report_price['report_type_id'] = $report_type_row;
                        $insert_report_price['purpose_type'] = $request['purpose_type'][$key];
                        $insert_report_price['price'] = $request['price'][$key];
                        $this->db->insert("network_report_price",$insert_report_price);
                    }
                }

                $this->session->set_flashdata('message', $this->ion_auth->messages());    
			
                if ($user_id) {
                    $user = $this->db->get_where('users',['id'=>$user_id])->row();
                    $remember_token = bin2hex(random_bytes(20));
                    $email_var_data["link"] = base_url('reset-password/'). $remember_token;
                    $email_var_data["full_name"] = $request['company_name'];
                    $email_var_data["email_template"] = 'hospital_register';
                    $mail = sendEmail('Reset Password', $request['email'], 'hospital_register', $email_var_data);
                
                    if($mail){
                        $update_data = array('remember_token' => $remember_token);
                        $this->db->where('id', $user_id);
                        $this->db->update('users', $update_data);

                        $update_data1 = array('user_id' => $user_id);
                        $this->db->where('id', $clinic_id['id']);
                        $this->db->update('hospital', $update_data1);
                    }else{
                        $this->session->set_flashdata('success', __('Error into sending mail. please try again later'));
                        $this->renderAdmin('clinic/add', $data);
                    }
                }

                $this->session->set_flashdata('success', __('Clinic Add successfully'));
                redirect('admin/clinic');
            }else{ 
                $this->renderAdmin('clinic/add', $data);
            }
        }
    }

    public function edit($id = ''){
        $data['clinic'] = Clinic::find($id);
        $data['network_report'] = $this->db->get_where('network_report_price',['hospital_id'=>$data['clinic']['id']])->result();
        $data['all_county'] = $this->db->get_where('county',['status'=>'1'])->result();
        $data['user'] = $this->db->get_where('users',['id'=>$data['clinic']['user_id']])->row();
        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['purpose_type'] = $this->config->item("purpose_type");
        $data['home'] = base_url('admin/clinic');
        $data['title'] = "Clinic Edit";
        $data['main_title'] = $this->title;

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('point_of_contact', 'Contact Number', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
        $this->form_validation->set_rules('county', 'County', 'required');

        // $this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
        // $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        // $this->form_validation->set_rules('daily_basis_case', 'Daily Basis Cases', 'trim|required|numeric');
        // $this->form_validation->set_rules('no_of_sites', 'No of Sites', 'trim|required');
        // $this->form_validation->set_rules('covid_testing', 'Covid Testing', 'trim|required');
        // $this->form_validation->set_rules('travel_covid_test', 'Travel Covid Test', 'trim|required');
        // $this->form_validation->set_rules('start_offering_service', 'Start Offering Service Date', 'trim|required');
        // $this->form_validation->set_rules('advertising', 'advertising', 'trim|required');


        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('clinic/edit', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post();
                $post['network_id'] = $this->session->userdata('network')['user_id'];
                $post['company_name'] = $request['company_name'];
                $post['company_number'] = $request['company_number'] ? $request['company_number'] : NULL;
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['company_website'] = $request['company_website'] ? $request['company_website'] : NULL;
                $post['no_of_employee'] = $request['no_of_employee'] ? $request['no_of_employee'] : NULL;
                $post['point_of_contact'] = $request['point_of_contact'] ? $request['point_of_contact'] : NULL;
                $post['daily_basis_case'] = $request['daily_basis_case'] ? $request['daily_basis_case'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                $post['postal_code'] = $request['postal_code'] ? $request['postal_code'] : NULL;
                $post['no_of_sites'] = $request['no_of_sites'] ? $request['no_of_sites'] : NULL;
                $post['covid_testing'] = $request['covid_testing'] ? $request['covid_testing'] : NULL;
                $post['travel_covid_test'] = $request['travel_covid_test'] ? $request['travel_covid_test'] : NULL;
                $post['start_offering_service'] = $request['start_offering_service'] ? $request['start_offering_service'] : NULL;
                $post['advertising'] = $request['advertising'] ? $request['advertising'] : NULL;
                $post['lat'] = $request['latitude'];
                $post['lng'] = $request['longitude'];
                $post['county'] = $request['county'];

                Clinic::where('id', $id)->update($post);
                $clinic_id = $this->db->from('hospital')->order_by('id','DESC')->limit(1)->get()->row_array();

                if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name']))
                {
                    $type = explode('.', $_FILES["profile_picture"]["name"]);
                    $type = strtolower($type[count($type) - 1]);
                    $name = mt_rand(100000000, 999999999);
                    $filename = $name . '.' . $type;
                    $url = "assets/images/users/" . $filename;
                    move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
                    $folder = "assets/images/users/";
                    s3Upload($url, $folder);
                    $image = S3_URL.$url;
                    $profile['profile_picture'] = $image;

                    $this->db->where('id',$data['user']->id);
                    $this->db->update('users',$profile);    
                }
                

                $this->db->where('hospital_id',$id);
                $this->db->delete('network_report_price');
                

                 if(isset($request['report_type_id']) && !empty($request['report_type_id'])){
                    foreach($request['report_type_id'] as $key=>$report_type_row){
                        $insert_report_price = array();
                        $insert_report_price['hospital_id'] = $id;
                        $insert_report_price['report_type_id'] = $report_type_row;
                        $insert_report_price['purpose_type'] = $request['purpose_type'][$key];
                        $insert_report_price['price'] = $request['price'][$key];
                        $this->db->insert("network_report_price",$insert_report_price);
                    }

                }

                
                $this->session->set_flashdata('success', __('Clinic Update successfully'));
                redirect('admin/clinic');
            }else{
                $this->renderAdmin('clinic/edit', $data);
            }
        }
    }

    public function details($id = ''){
        $data['clinic'] = Clinic::find($id);
        $data['user'] = $this->db->get_where('users',['id'=>$data['clinic']['user_id']])->row();
        $query = $this->db->select('*,lab.id as lab_id')->from('lab')->join('users', 'users.id = lab.user_id')->where('hospital_id',$data['clinic']['user_id'])->get();
        $data['lab'] = $query->result();
        $data['booking'] = $this->db->get_where('booking',['clinic_id'=>$id])->result();

        $this->db->select('B.report_price,B.total_price,B.payment_status,BS.date,U.full_name');
        $this->db->from('booking B');
        $this->db->join('booking_member BM','BM.booking_id=B.id');
        $this->db->join('users U','U.id=BM.user_id');
        $this->db->join('booking_slots BS','BS.booking_member_id=BM.id');
        $this->db->where('BM.clinic_id',$id);
        $this->db->where('B.payment_status =','success');
        $query = $this->db->get();
        $data['booking_details'] = $query->result_array();

        $data['home'] = base_url('admin/clinic');
        $data['title'] = "Clinic Details";
        $data['main_title'] = $this->title;
        $this->renderAdmin('clinic/details', $data);
        
    }

    public function delete($id)
    {

        $data = $this->db->get_where('hospital',array('id'=>$id))->row_array();

        if ($data)
         {
            $update_data['is_deleted'] = '1';

            $this->db->where('id',$id);
            $this->db->update($this->table_name,$update_data);

            $this->db->where('id',$data['user_id']);
            $this->db->update('users',$update_data);
         }

        echo 1;
    }

    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
