<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CountryController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
        $this->load->model('Zone');
        $this->load->model('Country');
		$this->load->model('CertifiedVaccines');
        $this->table_name = "country";
        $this->title = "Country";
		$this->date = date('y-m-d');
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $country = new Datatables;

		$country->select('country.id, country.name, country.status,zones.name as zone_name', false)
				->from($this->table_name)
				->join('zones', 'zones.id = country.zone');
		$action['edit'] = base_url('admin/country/edit/');
		$action['delete'] = base_url('admin/country/delete/');
        $country->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'name')
			// ->column('Zone', 'zone')
			->column('Zone', 'zone_name', function ($zone)
	        {
                return ucfirst($zone);
	            
	        })
			->column('Status', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square"></i></a>';
		        
				$option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="country"><i class="fa fa-trash"></i></a>';
				return $option;
		    });

        //$country->set_options(["columnDefs" => "[ { targets: [4], sortable: true}]"]);
        $country->set_options(["columnDefs" => "[ { targets: [4], sortable: true}]"]);
        $country->searchable('country.id, country.name, zones.name, country.status');
        $country->datatable($this->table_name);
        $country->init();
        $data['datatable'] = true;
		$data['export'] = true;
		$data['export_columns'] = [0,1,2];
        $data['add_url'] = base_url('admin/country/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('country/index', $data);
    }
    public function add()
    {
		$current_date = current_date();

        $data['title'] = "Country Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/country');
		$data['certified_vaccines'] = CertifiedVaccines::where("status",1)->select("id","name")->get();
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();


			$certified_vaccine_id = null;
			if (!empty($request['certified_vaccine_id'])) {
				$certified_vaccine_id = implode(",", $request['certified_vaccine_id']);
			}

            $reports_id = null;
            if (!empty($request['reports_id'])) {
                $reports_id = implode(",", $request['reports_id']);
            }


            $post['certified_vaccine_id'] = $certified_vaccine_id;
            $post['reports'] = $reports_id;
            $post['name'] = $request['name'];
			$post['zone'] = $request['zone'];
            $post['days'] = $request['international_days'];
            $post['oneway_days'] = $request['one_way_days'];
			$post['status'] = '1';
			$post['created_at'] = $current_date;

            if (isset($_FILES['country_img']) && !empty($_FILES['country_img']['name']))
                {
                    $type = explode('.', $_FILES["country_img"]["name"]);
                    $type = strtolower($type[count($type) - 1]);
                    $name = mt_rand(100000000, 999999999);
                    $filename = $name . '.' . $type;
                    $url = "assets/images/users/" . $filename;
                    move_uploaded_file($_FILES["country_img"]["tmp_name"], $url);
                    $folder = "assets/images/users/";
                    s3Upload($url, $folder);
                    $image = S3_URL.$url;
                    $post['image'] = $image;
                }

            Country::insert($post);
            $this->session->set_flashdata('success', __('County added sucessfully'));
            redirect('admin/country');
        }
        $this->renderAdmin('country/add', $data);
    }

    public function edit($id = '') {
        $data['home'] = base_url('admin/country');
        $data['title'] = "Country Edit";        
        $data['main_title'] = $this->title;
        
        // $data['country'] = $this->db->get_where('country',['id'=>$id])->row();
		$data['certified_vaccines'] = CertifiedVaccines::where("status",1)->select("id","name")->get();
        $data['country'] = Country::find($id);
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        // print_r($data); exit();
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();

			$certified_vaccine_id = null;
			if (!empty($request['certified_vaccine_id'])) {
				$certified_vaccine_id = implode(",", $request['certified_vaccine_id']);
			}

            $reports_id = null;
            if (!empty($request['reports_id'])) {
                $reports_id = implode(",", $request['reports_id']);
            }

            $post['certified_vaccine_id'] = $certified_vaccine_id;
            $post['reports'] = $reports_id;
            $post['name'] = $request['name'];
			$post['zone'] = $request['zone'];
            $post['days'] = $request['international_days'];
            $post['oneway_days'] = $request['one_way_days'];
            Country::where('id', $id)->update($post);

            if (isset($_FILES['country_img']) && !empty($_FILES['country_img']['name']))
                {
                    $type = explode('.', $_FILES["country_img"]["name"]);
                    $type = strtolower($type[count($type) - 1]);
                    $name = mt_rand(100000000, 999999999);
                    $filename = $name . '.' . $type;
                    $url = "assets/images/users/" . $filename;
                    move_uploaded_file($_FILES["country_img"]["tmp_name"], $url);
                    $folder = "assets/images/users/";
                    s3Upload($url, $folder);
                    $image = S3_URL.$url;
                    $profile['image'] = $image;

                    $this->db->where('id',$id);
                    $this->db->update('country',$profile);    
                }


            $this->session->set_flashdata('success', __('Country updated sucessfully'));
            redirect('admin/country');
        }
        $this->renderAdmin('country/edit', $data);
    }

	public function delete($id) {

        $current_date = current_date();

        $this->db->delete($this->table_name, array('id' => $id));

        echo 1;
	}
}

