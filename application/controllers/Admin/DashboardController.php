<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
			// redirect('/');
		$this->load->model('Country');
		$this->load->model('Category');
        $this->load->model('User');

	}
	public function index() {
		
		$data['user'] = User::count();		
		$data['category'] = Category::count();		
		$data['country'] = Country::count();		
		$this->renderAdmin('dashboard',$data);
	}
}
