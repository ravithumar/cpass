<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NetworkController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
		$this->load->model('Network');
        $this->table_name = "network";
        $this->load->model('User');
        $this->title = "Network";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('users.email,network.id, network.company_name,network.site_name,network.contact_no,network.site_reference_number,network.registration_number,network.vat_number, network.address', false)
            ->from($this->table_name)
            ->where('network.is_deleted',NULL)
            ->join('users', 'users.id = network.user_id');
        $action['edit'] = base_url('admin/network/edit/');
        $action['delete'] = base_url('admin/network/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Network Name', 'company_name')
            ->column('Email Address', 'email')
            ->column('Site Name', 'site_name')
            ->column('Contact Number', 'contact_no')
            ->column('Registration Number', 'registration_number')
            ->column('VAT Number', 'vat_number')
            ->column('Address', 'address')
            ->column('Actions', 'id', function ($id) use ($action)
            {               
                $option = '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="network"><i class="fa fa-trash"></i></a>';
                return $option;
            });
        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/network/add');
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('network/index', $data);
	}
	public function add(){
        $data['title'] = "Network Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/network'); 

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('site_name', 'Website Name', 'trim|required');
        $this->form_validation->set_rules('site_reference_number', 'Site Reference Number', 'trim|required');
        $this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[users.username]', array('is_unique' => __('This username is already exist!.')));
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));
        $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        $this->form_validation->set_rules('contact_no', 'Phone Number', 'trim|required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('network/add', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post(); 
                $post['company_name'] = $request['company_name'];
                $user['username'] = $request['username'];
                $user['email'] = $request['email'];
                $user['phone'] = $request['contact_no'] ? $request['contact_no'] : NULL;
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['site_name'] = $request['site_name'] ? $request['site_name'] : NULL;
                $post['contact_no'] = $request['contact_no'] ? $request['contact_no'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                $id = Network::insert($post);
                $network_id = $this->db->from('network')->order_by('id','DESC')->limit(1)->get()->row_array();

                $tables = $this->config->item('tables', 'ion_auth'); 
			    $user_id = $this->ion_auth->register($request['username'], NULL, $request['email'],$user, [2], 1);
                $this->session->set_flashdata('message', $this->ion_auth->messages());    
			
                if ($user_id) {
                    $user = $this->db->get_where('users',['id'=>$user_id])->row();
                    $remember_token = bin2hex(random_bytes(20));
                    $email_var_data["link"] = base_url('reset-password/'). $remember_token;
                    $email_var_data["full_name"] = $request['company_name'];
                    $email_var_data["email_template"] = 'hospital_register';
                    $mail = sendEmail('Reset Password', $request['email'], 'hospital_register', $email_var_data);
                
                    if($mail){
                        $update_data = array('remember_token' => $remember_token);
                        $this->db->where('id', $user_id);
                        $this->db->update('users', $update_data);

                        $update_data1 = array('user_id' => $user_id);
                        $this->db->where('id', $network_id['id']);
                        $this->db->update('network', $update_data1);
                    }else{
                        $this->session->set_flashdata('success', __('Error into sending mail. please try again later'));
                        $this->renderAdmin('network/add', $data);
                    }
                }

                $this->session->set_flashdata('success', __('Network Add successfully'));
                redirect('admin/network');
            }else{ 
                $this->renderAdmin('network/add', $data);
            }
        }
    }

    public function edit($id = ''){
        $data['network'] = Network::find($id);
      
        $data['user'] = $this->db->get_where('users',['id'=>$data['network']['user_id']])->row();
        $data['home'] = base_url('admin/network');
        $data['title'] = "Network Edit";
        $data['main_title'] = $this->title;

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('site_name', 'Company Website', 'trim|required');
        $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        $this->form_validation->set_rules('contact_no', 'Contact Number', 'trim|required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('network/edit', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post();
                $post['company_name'] = $request['company_name'];
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['site_name'] = $request['site_name'] ? $request['site_name'] : NULL;
                $post['contact_no'] = $request['contact_no'] ? $request['contact_no'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                Network::where('id', $id)->update($post);
                $this->session->set_flashdata('success', __('Network Update successfully'));
                redirect('admin/network');
            }else{
                $this->renderAdmin('network/edit', $data);
            }
        }
    }

    public function delete($id)
    {
        $data = $this->db->get_where('network',array('id'=>$id))->row_array();

         if ($data)
         {
              $update_data['is_deleted'] = '1';

            $this->db->where('id',$id);
            $this->db->update($this->table_name,$update_data);

            $this->db->where('id',$data['user_id']);
            $this->db->update('users',$update_data);
         }
       

        echo 1;
    }

    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
