<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ReportController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
		$this->load->model('Zone');
        $this->load->model('Report');
        $this->load->model('ReportType');
        $this->load->model('CertifiedVaccines');
        $this->table_name = "reports";
        $this->title = "Report";
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('report_type.name as report_name, reports.id, reports.name,reports.days,reports.minutes,reports.SKU,reports.price,reports.zone,reports.is_domestic, reports.status', false)
            ->from($this->table_name)
            ->join('report_type', 'report_type.id = reports.report_type_id')
            ->where('reports.is_deleted',NULL);
        $action['edit'] = base_url('admin/report/edit/');
        $action['delete'] = base_url('admin/report/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Report Type Name', 'report_name')
            ->column('Report Name', 'name')
            ->column('Days', 'days')
            ->column('Minutes', 'minutes')
            //->column('Zone', 'zone')
            ->column('SKU', 'SKU')
            ->column('Price', 'price')
            ->column('Approve', 'status', function ($status, $row)
            {
                if ($status == 1)
                {
                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
                }
                else
                {
                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
                }
            })
            ->column('Actions', 'id', function ($id) use ($action)
            {
                $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';

                 $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="reports"><i class="fa fa-trash"></i></a>';

                return $option;
            }); 

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/report/add');
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('report/index', $data);
    }
    
    public function add()
    {
        $data['title'] = "Report Add";
        $data['main_title'] = $this->title;
        $data['report_type'] = ReportType::all();
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
		$data['vaccines'] = CertifiedVaccines::where("status",1)->select("id","name")->get();
		$data['purpose_type'] = $this->config->item('purpose_type');
		$data['vaccine_status'] = $this->config->item('vaccine_status');
        $data['home'] = base_url('admin/report');
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            
			$days = "";
            $daysArr = $request['days'];
            if (!empty($daysArr)) {
                $days = implode(",",array_filter($daysArr));
            } 
			$zone = NULL;
			if(isset($request['zone']) && !empty($request['zone'])){
				$zone = implode(',',$request['zone']);
			}
			
			$purpose_type = NULL;
			if(isset($request['purpose_type']) && !empty($request['purpose_type'])){
				$purpose_type = implode(',',$request['purpose_type']);
			}
			
			$vaccine = NULL;
			if(isset($request['vaccine']) && !empty($request['vaccine'])){
				$vaccine = implode(',',$request['vaccine']);
			}
			$vaccination_status = NULL;
			if(isset($request['vaccination_status']) && !empty($request['vaccination_status'])){
				$vaccination_status = implode(',',$request['vaccination_status']);
			}

            $post['select_steps'] = $request['select_step'] ? $request['select_step'] : null;
            $post['medical_exemptions'] = $request['medical_exemptions'];
            $post['report_type_id'] = $request['report_type_id'];
            $post['name'] = $request['name'];
            $post['minutes'] = $request['minutes'];
            //$post['is_domestic'] = $request['is_domestic'];
            $post['zone'] = $zone;
            $post['purpose_type'] = $purpose_type;
            $post['vaccine'] = $vaccine;
			$post['vaccination_status'] = $vaccination_status;
            $post['SKU'] = $request['SKU'];
            $post['price'] = $request['price'];
            $post['days'] = $days ? $days : NULL;
            Report::insert($post);
            $report_data = $this->db->from('reports')->order_by('id','DESC')->limit(1)->get()->row_array();
            $report_id = $report_data['id'];
            if(isset($request['report_zone']) && !empty($request['report_zone'])){
                foreach($request['report_zone'] as $key=>$value){
                    if(!empty($value) && !empty($request['report_vaccination_status'][$key]) && !empty($request['report_booking_test_type'][$key]) && !empty($request['report_lab_test'][$key])){
                        $insert_sku['report_id'] = $report_id;
                        $insert_sku['zone_id'] = $value;
                        $insert_sku['vaccination_status'] = !empty($request['report_vaccination_status'][$key])?$request['report_vaccination_status'][$key]:null;
                        $insert_sku['booking_test_type'] = !empty($request['report_booking_test_type'][$key])?$request['report_booking_test_type'][$key]:null;
                        $insert_sku['lab_test'] = !empty($request['report_lab_test'][$key])?$request['report_lab_test'][$key]:null;

                        $this->db->insert("reports_sku",$insert_sku);
                    }
                }
            }
            
            $this->session->set_flashdata('success', __('Report Added successfully'));
            redirect('admin/report');
        }
        $this->renderAdmin('report/add', $data);
    }

    public function edit($id = '')
    {
        $data['report'] = Report::find($id);
        $data['report_type'] = ReportType::all();
		$data['zones'] = Zone::where("status",1)->select("id","name")->get();
		$data['vaccines'] = CertifiedVaccines::where("status",1)->select("id","name")->get();
        $data['reports_sku'] = $this->db->from('reports_sku')->where("report_id",$id)->get()->result_array();
		$data['purpose_type'] = $this->config->item('purpose_type');
		$data['vaccine_status'] = $this->config->item('vaccine_status');
        $data['title'] = "Report Edit";
        $data['home'] = base_url('admin/report');
        $data['main_title'] = $this->title;
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();

			$days = "";
            $daysArr = $request['days'];
            if (!empty($daysArr)) {
                $days = implode(",",array_filter($daysArr));
            } 
			$zone = NULL;
			if(isset($request['zone']) && !empty($request['zone'])){
				$zone = implode(',',$request['zone']);
			}
			$purpose_type = NULL;
			if(isset($request['purpose_type']) && !empty($request['purpose_type'])){
				$purpose_type = implode(',',$request['purpose_type']);
			}
			$vaccine = NULL;
			if(isset($request['vaccine']) && !empty($request['vaccine'])){
				$vaccine = implode(',',$request['vaccine']);
			}
			$vaccination_status = NULL;
			if(isset($request['vaccination_status']) && !empty($request['vaccination_status'])){
				$vaccination_status = implode(',',$request['vaccination_status']);
			}
            
			$post['report_type_id'] = $request['report_type_id'];
            $post['medical_exemptions'] = $request['medical_exemptions'];
            $post['name'] = $request['name'];
            $post['minutes'] = $request['minutes'];
            //$post['is_domestic'] = $request['is_domestic'];
            $post['zone'] = $zone;
            $post['purpose_type'] = $purpose_type;
			$post['vaccine'] = $vaccine;
			$post['vaccination_status'] = $vaccination_status;
            $post['days'] = $days ? $days : NULL;
            $post['SKU'] = $request['SKU'] ? $request['SKU'] : NULL;
            $post['price'] = $request['price'] ? $request['price'] : NULL;
            $post['report_type_id'] = $request['report_id'];
            $post['select_steps'] = $request['select_step'] ? $request['select_step'] : NULL;

            Report::where('id', $id)->update($post);

            $report_id = $id;
            $this->db->where('report_id',$report_id);
            $this->db->delete("reports_sku");
            if(isset($request['report_zone']) && !empty($request['report_zone'])){
                foreach($request['report_zone'] as $key=>$value){
                    if(!empty($value) && !empty($request['report_vaccination_status'][$key]) && !empty($request['report_booking_test_type'][$key]) && !empty($request['report_lab_test'][$key])){
                        $insert_sku['report_id'] = $report_id;
                        $insert_sku['zone_id'] = $value;
                        $insert_sku['vaccination_status'] = !empty($request['report_vaccination_status'][$key])?$request['report_vaccination_status'][$key]:null;
                        $insert_sku['booking_test_type'] = !empty($request['report_booking_test_type'][$key])?$request['report_booking_test_type'][$key]:null;
                        $insert_sku['lab_test'] = !empty($request['report_lab_test'][$key])?$request['report_lab_test'][$key]:null;

                        $this->db->insert("reports_sku",$insert_sku);
                    }
                }
            }

            $this->session->set_flashdata('success', __('Report Updated successfully'));
            redirect('admin/report');
        }
        $this->renderAdmin('report/edit', $data);
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo '1';
    }
}

