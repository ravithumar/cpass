<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ReportTypeController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
        $this->load->model('ReportType');
        $this->table_name = "report_type";
        $this->title = "Report Type";
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('*', false)
            ->from($this->table_name)
            ->where('is_deleted',NULL);
        $action['edit'] = base_url('admin/report-type/edit/');
        $action['delete'] = base_url('admin/report-type/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Report Name', 'name')
			->column('Description', 'description')
            ->column('Approve', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="report_type"><i class="fa fa-trash"></i></a>';

		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/report-type/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('report_type/index', $data);
    }
    public function add()
    {
        $data['title'] = "Report Type Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/report-type');
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post(); 
            $days = "";
            $daysArr = $request['days'];
            if (!empty($daysArr)) {
                $days = implode(",",array_filter($daysArr));
            } 
            $post['name'] = $request['name'];
            $post['description'] = $request['description'];

            ReportType::insert($post);
            $this->session->set_flashdata('success', __('Report Type Add successfully'));
            redirect('admin/report-type');
        }
        $this->renderAdmin('report_type/add', $data);
    }

    public function edit($id = '')
    {
        $data['reporttype'] = ReportType::find($id);
        $data['home'] = base_url('admin/report-type');
        $data['title'] = "Report Type Edit";
        $data['main_title'] = $this->title;
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $days = "";
            $daysArr = $request['days'];
            if (!empty($daysArr)) {
                $days = implode(",",array_filter($daysArr));
            } 
           
            $post['name'] = $request['name'];
            $post['description'] = $request['description'];
            ReportType::where('id', $id)->update($post);
            $this->session->set_flashdata('success', __('Report Type Update successfully'));
            redirect('admin/report-type');
        }
        $this->renderAdmin('report_type/edit', $data);
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo '1';
    }
}

