<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SystemController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
		
	}
	public function index() {
        $this->load->helper('directory');
        $data['backup'] = directory_map('assets/DB/');
        $this->load->library('breadcrumbs');
		$this->breadcrumbs->push('Dashboard', '/admin');
		$this->breadcrumbs->push('System Configuration', 'admin/system/setting');
		$this->renderAdmin('system_config/index',$data);
	}
	public function save_system_config() {
		$request = $this->input->post();
		$request['shipper_individual_account'] = isset($request['shipper_individual_account'])?1:0;
		$request['shipper_company_account'] = isset($request['shipper_company_account'])?1:0;
		$request['carrier_individual_account'] = isset($request['carrier_individual_account'])?1:0;
		$request['carrier_company_account'] = isset($request['carrier_company_account'])?1:0;
		foreach ($request as $key => $value) {
			$key_array[$key] = $value;
		}
		if ($_FILES['site_logo']['name'] != "") {
			$image_upload_resp = image_upload('site_logo', 'images/logo');
			if ($image_upload_resp['status']) {
				$key_array['site_logo'] = $image_upload_resp['file_name'];
			} else {
				$this->session->set_flashdata('danger', $image_upload_resp['error']);
			}
		}
		config_set($key_array);
		$this->session->set_flashdata('success', __('System Configuration details updated successfully.'));
		redirect('admin/system/setting');
	}
	public function app_setting() {
		$data['app'] = $this->db->get('appsetting')->result();
		$this->load->library('breadcrumbs');
		$this->breadcrumbs->push('Dashboard', '/admin');
		$this->breadcrumbs->push('App Configuration', 'admin/app/setting');
		$this->renderAdmin('system_config/app_setting', $data);
	}
	public function save_app_config() {
		$request = $this->input->post();
		// $android_customer['app_version']=$request['android_version'];
		$android_customer['app_version']=$request['android_version'];
		$android_customer['updates']=isset($request['android_update'])?1:0;
		// $android_customer['maintenance_mode']=isset($request['android_maintenance'])?1:0;
		// _pre($android_customer);exit();
		$this->db->where('app_name','android')->update('appsetting',$android_customer);

		$ios_customer['app_version']=$request['ios_version'];
		$ios_customer['updates']=isset($request['ios_update'])?1:0;
		$this->db->where('app_name','ios')->update('appsetting',$ios_customer);

		
		$maintenance_mode['updates']=isset($request['maintenance_mode'])?1:0;
		$this->db->where('app_name','maintenance_mode')->update('appsetting',$maintenance_mode);

		$this->session->set_flashdata('success', __('App Configuration details updated successfully.'));
		redirect('admin/app/setting');
	}
	public function backup() {
		$this->load->dbutil();
		$filename = time() . '.zip';
		$prefs = array(
			'ignore' => array(), // List of tables to omit from the backup
			'format' => 'zip', // gzip, zip, txt
			'filename' => $filename, // File name - NEEDED ONLY WITH ZIP FILES
			'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
			'add_insert' => TRUE, // Whether to add INSERT data to backup file
			'newline' => "\n", // Newline character used in backup file
		);
		$backup = $this->dbutil->backup($prefs);
		$this->load->helper('file');
		write_file('assets/DB/' . $filename, $backup);
		// $this->load->helper('download');
		// force_download($filename, $backup);
        $this->session->set_flashdata('success', __('Database backup successfully.'));
        redirect('admin/system/setting');
	}
    public function download($filename="")
    {
        if($filename !="")
        {
            $this->load->helper('download');
            $data = file_get_contents("assets/DB/".$filename); // Read the file's contents
            $name = $filename;
            force_download($name, $data);
            $this->session->set_flashdata('success', __('Backup download successfully.'));
            redirect('admin/system/setting');
        }
        else
        {
            $this->session->set_flashdata('error', __('Somthing went wrong due to download backup!'));
            redirect('admin/system/setting');   
        }
    }
    public function delete_backup($filename="")
    {
        if($filename !="")
        {
           unlink('assets/DB/'.$filename);
           $this->session->set_flashdata('success', __('Backup deleted successfully.'));
            redirect('admin/system/setting');
        }
        else
        {
            $this->session->set_flashdata('error', __('Somthing went wrong due to delete backup!'));
            redirect('admin/system/setting');   
        }
    }
}