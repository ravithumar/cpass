<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/auth/login');
		}
		$this->load->model('Clinic');
        $this->table_name = "users";
        $this->load->model('User');
        $this->title = "Users";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('users.*', false)
            ->from($this->table_name)
            ->join('users_groups', 'users.id = users_groups.user_id')
            ->where('users.is_deleted',NULL)
            ->where('users_groups.group_id',2);
        $action['edit'] = base_url('admin/user/edit/');
        $action['details'] = base_url('admin/user/details/');
        $action['delete'] = base_url('admin/user/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'full_name')
            ->column('Email Address', 'email')
            ->column('Phone Number', 'phone')
            ->column('Approve', 'active', function ($status, $row)
	        {
	            if ($status == 1) {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }else {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        // $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
		        $option = '<a href="' . $action['details'] . $id . '" class="on-default text-secondary ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Details Field"><i class="la la-eye "></i></a>';

                 $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="Users"><i class="fa fa-trash"></i></a>';

		        return $option;
		    }); 
        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('admin/user/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('clinic/index', $data);
	}
	public function add(){
        $data['title'] = "Clinic Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/clinic'); 

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
        $this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[users.username]', array('is_unique' => __('This username is already exist!.')));
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));
        $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        $this->form_validation->set_rules('point_of_contact', 'Phone Number', 'trim|required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('daily_basis_case', 'Daily Basis Cases', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
        $this->form_validation->set_rules('no_of_sites', 'No of Sites', 'trim|required');
        $this->form_validation->set_rules('covid_testing', 'Covid Testing', 'trim|required');
        $this->form_validation->set_rules('travel_covid_test', 'Travel Covid Test', 'trim|required');
        $this->form_validation->set_rules('start_offering_service', 'Start Offering Service Date', 'trim|required');
        $this->form_validation->set_rules('advertising', 'advertising', 'trim|required');
        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('clinic/add', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post(); 

                $post['network_id'] = $this->session->userdata('network')['user_id'];
                $post['company_name'] = $request['company_name'];
                $user['username'] = $request['username'];
                $user['email'] = $request['email'];
                $post['company_number'] = $request['company_number'] ? $request['company_number'] : NULL;
                $user['phone'] = $request['company_number'] ? $request['company_number'] : NULL;
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['company_website'] = $request['company_website'] ? $request['company_website'] : NULL;
                $post['no_of_employee'] = $request['no_of_employee'] ? $request['no_of_employee'] : NULL;
                $post['point_of_contact'] = $request['point_of_contact'] ? $request['point_of_contact'] : NULL;
                $post['daily_basis_case'] = $request['daily_basis_case'] ? $request['daily_basis_case'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                $post['postal_code'] = $request['postal_code'] ? $request['postal_code'] : NULL;
                $post['no_of_sites'] = $request['no_of_sites'] ? $request['no_of_sites'] : NULL;
                $post['covid_testing'] = $request['covid_testing'] ? $request['covid_testing'] : NULL;
                $post['travel_covid_test'] = $request['travel_covid_test'] ? $request['travel_covid_test'] : NULL;
                $post['start_offering_service'] = $request['start_offering_service'] ? $request['start_offering_service'] : NULL;
                $post['advertising'] = $request['advertising'] ? $request['advertising'] : NULL;
                $id = Clinic::insert($post);
                $clinic_id = $this->db->from('hospital')->order_by('id','DESC')->limit(1)->get()->row_array();

                $tables = $this->config->item('tables', 'ion_auth'); 
			    $user_id = $this->ion_auth->register($request['username'], NULL, $request['email'],$user, [4], 1);
                $this->session->set_flashdata('message', $this->ion_auth->messages());    
			
                if ($user_id) {
                    $user = $this->db->get_where('users',['id'=>$user_id])->row();
                    $remember_token = bin2hex(random_bytes(20));
                    $email_var_data["link"] = base_url('reset-password/'). $remember_token;
                    $email_var_data["full_name"] = $request['company_name'];
                    $email_var_data["email_template"] = 'hospital_register';
                    $mail = sendEmail('Reset Password', $request['email'], 'hospital_register', $email_var_data);
                
                    if($mail){
                        $update_data = array('remember_token' => $remember_token);
                        $this->db->where('id', $user_id);
                        $this->db->update('users', $update_data);

                        $update_data1 = array('user_id' => $user_id);
                        $this->db->where('id', $clinic_id['id']);
                        $this->db->update('hospital', $update_data1);
                    }else{
                        $this->session->set_flashdata('success', __('Error into sending mail. please try again later'));
                        $this->renderAdmin('clinic/add', $data);
                    }
                }

                $this->session->set_flashdata('success', __('Clinic Add successfully'));
                redirect('admin/clinic');
            }else{ 
                $this->renderAdmin('clinic/add', $data);
            }
        }
    }

    public function edit($id = ''){
        $data['clinic'] = Clinic::find($id);
      
        $data['user'] = $this->db->get_where('users',['id'=>$data['clinic']['user_id']])->row();
        $data['home'] = base_url('admin/clinic');
        $data['title'] = "Clinic Edit";
        $data['main_title'] = $this->title;

        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('company_website', 'Company Website', 'trim|required');
        $this->form_validation->set_rules('no_of_employee', 'Number Of Employee', 'trim|required');
        $this->form_validation->set_rules('point_of_contact', 'Contact Number', 'trim|required|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('daily_basis_case', 'Daily Basis Cases', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('postal_code', 'Postal Code', 'trim|required');
        $this->form_validation->set_rules('no_of_sites', 'No of Sites', 'trim|required');
        $this->form_validation->set_rules('covid_testing', 'Covid Testing', 'trim|required');
        $this->form_validation->set_rules('travel_covid_test', 'Travel Covid Test', 'trim|required');
        $this->form_validation->set_rules('start_offering_service', 'Start Offering Service Date', 'trim|required');
        $this->form_validation->set_rules('advertising', 'advertising', 'trim|required');
        if($this->form_validation->run() == FALSE){ 
            $this->renderAdmin('clinic/edit', $data);
        }else { 
            if (isset($_POST) && !empty($_POST)) {
                $request = $this->input->post();
                $post['network_id'] = $this->session->userdata('network')['user_id'];
                $post['company_name'] = $request['company_name'];
                $post['company_number'] = $request['company_number'] ? $request['company_number'] : NULL;
                $post['vat_number'] = $request['vat_number'] ? $request['vat_number'] : NULL;
                $post['company_website'] = $request['company_website'] ? $request['company_website'] : NULL;
                $post['no_of_employee'] = $request['no_of_employee'] ? $request['no_of_employee'] : NULL;
                $post['point_of_contact'] = $request['point_of_contact'] ? $request['point_of_contact'] : NULL;
                $post['daily_basis_case'] = $request['daily_basis_case'] ? $request['daily_basis_case'] : NULL;
                $post['address'] = $request['address'] ? $request['address'] : NULL;
                $post['postal_code'] = $request['postal_code'] ? $request['postal_code'] : NULL;
                $post['no_of_sites'] = $request['no_of_sites'] ? $request['no_of_sites'] : NULL;
                $post['covid_testing'] = $request['covid_testing'] ? $request['covid_testing'] : NULL;
                $post['travel_covid_test'] = $request['travel_covid_test'] ? $request['travel_covid_test'] : NULL;
                $post['start_offering_service'] = $request['start_offering_service'] ? $request['start_offering_service'] : NULL;
                $post['advertising'] = $request['advertising'] ? $request['advertising'] : NULL;

                Clinic::where('id', $id)->update($post);
                $this->session->set_flashdata('success', __('Clinic Update successfully'));
                redirect('admin/clinic');
            }else{
                $this->renderAdmin('clinic/edit', $data);
            }
        }
    }

    public function details($id = ''){
        $data['user'] = User::find($id);
        // $data['clinic'] = Clinic::find($id);
        // $query = $this->db->select('*,lab.id as lab_id')->from('lab')->join('users', 'users.id = lab.user_id')->where('hospital_id',$data['clinic']['user_id'])->get();
        // $data['lab'] = $query->result();
        $data['members'] = $this->db->get_where('users',['parent_id'=>$id])->result();
        $data['booking'] = $this->db->get_where('booking',['user_id'=>$id])->result();

        $data['home'] = base_url('admin/user');
        $data['title'] = "User Details";
        $data['main_title'] = $this->title;
            
        $this->renderAdmin('user/details', $data);
        
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo '1';
    }

    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
