<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ZoneController extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->is_admin())
        {
            redirect('/auth/login');
        }
        $this->load->model('Zone');
        $this->table_name = "zones";
        $this->title = "Zone";
		$this->date = date('y-m-d');
    }
    public function index()
    {
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $country = new Datatables;

		$country->select('id, name, status', false)->from($this->table_name);
		$action['edit'] = base_url('admin/zone/edit/');
		$action['delete'] = base_url('admin/zone/delete/');
        $country->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'name')
			// ->column('Zone', 'zone')
			->column('Status', 'status', function ($status, $row)
	        {
	            if ($status == 1)
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }
	            else
	            {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square"></i></a>';
		        
				$option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="zone"><i class="fa fa-trash"></i></a>';
				return $option;
		    });

        //$country->set_options(["columnDefs" => "[ { targets: [4], sortable: true}]"]);
        $country->set_options(["columnDefs" => "[ { targets: [2], sortable: true}]"]);
        $country->searchable('id, name, status');
        $country->datatable($this->table_name);
        $country->init();
        $data['datatable'] = true;
		$data['export'] = true;
		$data['export_columns'] = [0,1,2];
        $data['add_url'] = base_url('admin/zone/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderAdmin('zones/index', $data);
    }
    public function add()
    {
		$current_date = current_date();

        $data['title'] = "Zone Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('admin/zone');
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
			$post['status'] = '1';
			$post['created_at'] = $current_date;
            Zone::insert($post);
            $this->session->set_flashdata('success', __('Zone added sucessfully'));
            redirect('admin/zone');
        }
        $this->renderAdmin('zones/add', $data);
    }

    public function edit($id = '') {
        $data['home'] = base_url('admin/zone');
        $data['title'] = "Zone Edit";        
        $data['main_title'] = $this->title;
        $data['row'] = Zone::find($id);
        // print_r($data); exit();
        if (isset($_POST) && !empty($_POST))
        {
            $request = $this->input->post();
            $post['name'] = $request['name'];
            Zone::where('id', $id)->update($post);
            $this->session->set_flashdata('success', __('Zone updated sucessfully'));
            redirect('admin/zone');
        }
        $this->renderAdmin('zones/edit', $data);
    }

	public function delete($id) {

        $current_date = current_date();

        $this->db->delete($this->table_name, array('id' => $id));

        echo 1;
	}
}

