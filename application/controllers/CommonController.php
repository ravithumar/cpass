<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CommonController extends CI_Controller {
	public function index()
	{
		$this->load->view('front/welcome_message');
	}

	public function check_duplicate_data()
    {         
        
        if(isset($_POST['old_value']))
        {
            $old_value = strtolower($_POST['old_value']);    
        }        
        $new_value = strtolower($_POST['new_value']);
        $field_name = $_POST['field_name'];        
        $table_name = $_POST['table_name'];                 
        if(isset($old_value))
        {
            if($old_value == $new_value)
            {
                echo 'true';
            }
            else
            {
                $results = $this->db->get_where($table_name,array($field_name => $new_value))->num_rows();
                if($results > 0)
                {
                    
                }
                else
                {
                    echo 'true';
                }
            }
        }
        else
        {      
            $results = $this->db->get_where($table_name,array($field_name => $new_value))->num_rows();      
            
            if($results > 0)
            {
                
            }
            else
            {
                echo 'true';
            }
        }        
    }

    
}
