<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CronjobController extends CI_Controller {
	public function index()
	{
		$this->load->view('front/welcome_message');
	}

	public function delete_bookings() {

		$this->load->model('Bookings');
        $minutes  = 5;
        // $date = date('Y-m-d', strtotime($request['date']));
        // $time = date('H:i', strtotime($request['time']));

        $this->db->set('is_deleted', 1)
				 ->where('booking_status !=', 'success')
				 ->update('booking');
        
    }   
}