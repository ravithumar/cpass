<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_gov())
		{
			redirect('/auth/login');
		}
		$this->load->model('category');
        $this->load->model('user');

	}
	public function index() {
		
		$data['user'] = User::count();		
		$data['category'] = Category::count();		
		$data['advertisement'] = 0;		
		$data['service'] = 0;		
		$this->renderGov('dashboard',$data);
	}
}
