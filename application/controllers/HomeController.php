<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeController extends CI_Controller {
	function __construct() {
		parent::__construct();
		// if(!$this->ion_auth->is_admin()) {
		// 	redirect('/auth/login');
		// }
        // $this->load->model('User');

	}
	public function index()
	{
		webRender('index');
	}

	public function services()
	{
		render('services');
	}

	public function about_us()
	{
		render('about');
	}

	public function contact_us()
	{
		render('contact-us');
	}

	public function privacy_policy()
	{
		render('privacy-policy');
	}

	public function terms_conditions()
	{
		render('terms-and-conditions');
	}
}
