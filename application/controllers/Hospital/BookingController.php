<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_hospital())
		{
			redirect('/auth/login');
		}
		$this->load->model('Lab');
		$this->load->model('Bookings');
		$this->load->model('BookingSession');
		$this->load->model('BookingSlots');
		$this->load->model('BookingsMembers');
        $this->table_name = "booking";
        $this->load->model('user');
        $this->title = "Booking";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);
	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');

        $filter_request = $this->input->post();
        $where = [];
        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();

        $user_id =  $this->session->userdata('hospital')['user_id'];
        $hospitalArr = $this->db->get_where('hospital',['user_id'=>$user_id])->row();
      
        $product->select('booking.id, booking.booking_no,users.phone,hospital.company_name,booking.type,booking.total_price,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website, booking.status,booking.booking_status', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('users', 'booking_member.user_id = users.id')
            ->where('booking_member.clinic_id',$hospitalArr->id)
            ->where($where)
            ->group_by('booking.id');

        $action['view'] = base_url('hospital/booking/details/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Reference Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'type',function ($type){
                return ucfirst($type);
            })
            ->column('Contact No', 'phone')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
	            if ($status == "success") {
	               return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Success</span></h5>';
	            }
                
	            else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst($status).'</span></h5>';
	            }else{
	                return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst($status).'</span></h5>';
                }
	        })
           
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['view'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Field"><i class="la la-eye "></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderHospital('booking/index', $data);
	}
	

    public function details($id = ''){

        $this->db->select('booking.type,booking.total_price,booking.tax_price,booking.report_price,booking_member.*,booking_member.id as booking_member_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->result_array();

        if (!empty($data['booking'])) {
            foreach ($data['booking'] as $key => $value) {
                $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                if ($value['is_excempted'] == "yes") {
                    $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                    if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                }else{  $data['booking'][$key]['cat_name'] = ""; }

                // $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('id' => $value['booking_id']))->row();

                $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

            }
        }
        // echo "<pre>"; print_r($data['booking']);  exit();

        $data['home'] = base_url('hospital/booking');
        $data['title'] = "Booking Details";
        $data['main_title'] = $this->title;
        $this->renderHospital('booking/details', $data);

    }
    
    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
