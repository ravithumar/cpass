<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

use Phppot\DataSource;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

defined('BASEPATH') OR exit('No direct script access allowed');
class ImportController extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Report');
        $this->load->model('Network');
        $this->load->model('Clinic');
        $this->load->model('Slot');
        $this->load->model('User');
        $this->load->library(['ion_auth']);
    }

	public function index()
	{
        if (isset($_POST["import"])) {

            $allowedFileType = [
                'application/vnd.ms-excel',
                'text/xls',
                'text/xlsx',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ];

            if (in_array($_FILES["file"]["type"], $allowedFileType)) {

                $targetPath = 'uploads/' . $_FILES['file']['name'];
                move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

                $Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

                $spreadSheet = $Reader->load($targetPath);
                $excelSheet = $spreadSheet->getActiveSheet();
                $spreadSheetAry = $excelSheet->toArray();
                $sheetCount = count($spreadSheetAry);

                for ($i = 0; $i <= $sheetCount; $i ++) {
                    $name = "";
                    echo "<pre>";
                    print_r($spreadSheetAry);
                }
            } else {
                $type = "error";
                $message = "Invalid File Type. Upload Excel File.";
            }

            exit;
        }

		render('about');
	}

    public function uploaded()
    {
        echo "df"; exit;
        $report_ids = [];
        $report = Report::pluck('id');
        if(isset($report) && count($report) > 0){
            $report_ids = $report->toArray();

        }

        $Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $spreadSheet = $Reader->load('uploads/networks.xlsx');
        $excelSheet = $spreadSheet->getActiveSheet();
        $sheet_data = $excelSheet->toArray();
        $sheetCount = count($sheet_data);

        $google_key = $this->config->item('google_key');

        $users_email = User::all()->pluck('email')->toArray(); 
        //_pre($users);

        $user_data = $error = [];
        for ($i = 1; $i <= $sheetCount; $i ++) {
            
            $temp = array();
            $row_num = $i + 1;

            if(isset($sheet_data[$i][0]) && $sheet_data[$i][0] != ''){
                $sheet_data[$i] = array_map('trim', $sheet_data[$i]);
            }else{
                break;
            }

            if (isset($sheet_data[$i][0]) && $sheet_data[$i][0] != ''){
                $temp['full_name'] = $sheet_data[$i][0];
            }else{
                $error[] = 'Provider name is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][1]) && $sheet_data[$i][1] != ''){
                if(filter_var($sheet_data[$i][1], FILTER_VALIDATE_URL) == TRUE){
                    $temp['website'] = $sheet_data[$i][1];
                }else{
                    $error[] = 'Site website format is not valid in row '.$row_num;
                }
            }else{
                $error[] = 'Site website is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][2]) && $sheet_data[$i][2] != ''){
                $temp['contact_no'] = preg_replace('/[^0-9.]+/', '', $sheet_data[$i][2]);
                $temp['country_code'] = "+44";
            }else{
                $error[] = 'Contact number is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][3]) && $sheet_data[$i][3] != ''){
                $temp['site_reference_number'] = $sheet_data[$i][3];
            }else{
                $temp['site_reference_number'] = '';
            }

            if (isset($sheet_data[$i][4]) && $sheet_data[$i][4] != ''){
                $temp['registration_number'] = $sheet_data[$i][4];
            }else{
                $temp['registration_number'] = '';
            }

            if (isset($sheet_data[$i][5]) && $sheet_data[$i][5] != ''){
                $sheet_report_ids = explode(',', $sheet_data[$i][5]);
                $contain_all_vals = !array_diff($sheet_report_ids, $report_ids);
                if($contain_all_vals == TRUE){
                    $temp['report_id'] = $sheet_data[$i][5];
                }else{
                    $error[] = 'Report id(s) is incorrect in row '.$row_num;
                }

            }else{
                $error[] = 'Report id(s) is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][6]) && $sheet_data[$i][6] != ''){
                $temp['vat_number'] = $sheet_data[$i][6];
            }else{
                $temp['vat_number'] = '';
            }

            if (isset($sheet_data[$i][7]) && $sheet_data[$i][7] != ''){
                $temp['pcr_test_initially'] = $sheet_data[$i][7];
            }else{
                $temp['pcr_test_initially'] = '';
            }

            if (isset($sheet_data[$i][8]) && $sheet_data[$i][8] != ''){
                if (!filter_var($sheet_data[$i][8], FILTER_VALIDATE_EMAIL)) {
                    $error[] = 'Email format is incorrect in row '.$row_num;
                }else{
                    //$user = User::where('email', $sheet_data[$i][8])->first();  
                    if(in_array($sheet_data[$i][8], $users_email)){
                        $error[] = 'Email is already in use for row '.$row_num;
                    }else{
                        $temp['email'] = $sheet_data[$i][8];
                    }
                    
                }
            }else{
                $error[] = 'Email is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][9]) && $sheet_data[$i][9] != ''){
                if(strlen($sheet_data[$i][9]) >= 8){
                    $temp['password'] = $sheet_data[$i][9];
                }else{
                    $error[] = 'Password should be atleast 8 character in row '.$row_num;
                }
            }else{
                $error[] = 'Password is missing in row '.$row_num;
            }

            $fetch_lat_long = FALSE;
            if (isset($sheet_data[$i][10]) && $sheet_data[$i][10] != ''){
                if(strlen($sheet_data[$i][10]) >= 3){
                    $temp['address'] = $sheet_data[$i][10];
                    $g_address = str_replace(" ","+",trim($sheet_data[$i][10]));
                    $fetch_lat_long = TRUE;
                }else{
                    $error[] = 'Address is invalid in row '.$row_num;
                }
            }else{
                $error[] = 'Address is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][11]) && $sheet_data[$i][11] != ''){
                if(strlen($sheet_data[$i][11]) >= 4){
                    $temp['postcode'] = $sheet_data[$i][11];
                }else{
                    $error[] = 'Postcode is invalid in row '.$row_num;
                }
            }else{
                $error[] = 'Postcode is missing in row '.$row_num;
            }

            if (isset($sheet_data[$i][12]) && $sheet_data[$i][12] != ''){
                if(strlen($sheet_data[$i][12]) >= 2){
                    $temp['city'] = $sheet_data[$i][12];
                }else{
                    $error[] = 'City is invalid in row '.$row_num;
                }
            }else{
                $error[] = 'City is missing in row '.$row_num;
            }

            if(count($error) == 0 && $fetch_lat_long == TRUE){
                $temp['lat'] = $temp['lan'] = '0';

                $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$g_address&key=$google_key");
                $json = json_decode($json);

                if(isset($json->{'results'}[0])){
                    $temp['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
                    $temp['lan'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
                }else{
                    $error[] = "Address is invalid in row ".$row_num;
                }
                
            }
            array_push($user_data, $temp);
        }
        
        $insert_count = 0;
        if(count($error) == 0){
            if(isset($user_data) && count($user_data) > 0){
                foreach ($user_data as $key => $value) {

                    $user_data = array();
                    $user_data['parent_id'] = 0;
                    $user_data['full_name'] = $value['full_name'];
                    $user_data['country_code'] = $value['country_code'];
                    $user_data['address'] = $value['address'];
                    $user_data['postcode'] = $value['postcode'];
                    $user_data['city'] = $value['city'];

                    $user_id = $this->ion_auth->register($value['full_name'], $value['password'], $value['email'], $user_data, [4], 1);
                    echo "User insering ".$user_id." Time ".date('H:i A');
                    echo "<br>";
                    $user = User::find($user_id);
                    $user->active = '1';
                    $user->save();

                    $network = new Network();
                    $network->user_id = $user_id;
                    $network->company_name = $value['full_name'];
                    $network->site_name = $value['website'];
                    $network->contact_no = $value['contact_no'];
                    $network->site_reference_number = $value['site_reference_number'];
                    $network->registration_number = $value['registration_number'];
                    $network->vat_number = $value['vat_number'];
                    $network->postal_code = $value['postcode'];
                    $network->address = $value['address'];
                    $network->lat = $value['lat'];
                    $network->lan = $value['lan'];
                    $network->pcr_test_initially = $value['pcr_test_initially'];
                    $network->save();
                    $network_id = $network->id;


                    $hospital = new Clinic();
                    $hospital->user_id = $user_id;
                    $hospital->network_id = $network_id;
                    $hospital->report_id = $value['report_id'];
                    $hospital->type = 'clinic';
                    $hospital->company_name = $value['full_name'];
                    $hospital->vat_number = $value['vat_number'];
                    $hospital->company_website = $value['website'];
                    $hospital->point_of_contact = $value['contact_no'];
                    $hospital->address = $value['address'];
                    $hospital->postal_code = $value['postcode'];
                    $hospital->lat = $value['lat'];
                    $hospital->lng = $value['lan'];
                    $hospital->save();
                    $hospital_id = $hospital->id;

                    $days = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
                    $slot_data = array();
                    foreach ($days as $key => $value) {

                        $slot = array();
                        $slot['user_id'] = $hospital_id;
                        $slot['day'] = $value;
                        $slot['morning_start'] = '08:30';
                        $slot['morning_end'] = '11:30';
                        $slot['morning_status'] = '1';
                        $slot['afternoon_start'] = '13:30';
                        $slot['afternoon_end'] = '15:30';
                        $slot['afternoon_status'] = '1';
                        $slot['evening_start'] = '16:00';
                        $slot['evening_start'] = '18:30';
                        $slot['evening_status'] = '1';
                        array_push($slot_data, $slot);
                    }

                    Slot::insert($slot_data);

                    $insert_count++;
                    echo "User Inserted ".$user_id;
                    echo "<br>";


                }
            }

            echo "<pre>";
            print_r('insert_count'.$insert_count);
        }else{
            echo "<pre>";
            print_r($error);
        }

        exit;
    }
    
}
