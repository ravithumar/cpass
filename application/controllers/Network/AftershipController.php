<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AftershipController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		
	}
	public function index() {
		$get_data = $this->input->get();
		//print_r($get_data); die;
		$data['tracking_data'] = array(); 
		if(isset($get_data) && !empty($get_data)){
			try{
				$trackings = new AfterShip\Trackings($this->config->item("after_ship_api_key"));
				$response = $trackings->get("dpd", $get_data['tracking_number']);
				// _pre($response);
				$data['tracking_data'] = $response;
			}
			catch(Exception $e){

			}
		}
		$this->renderNetwork('after_ship/index', $data);
		//$html = $this->load->view('network/pages/after_ship/index',$data,true);
		//print json_encode(array("html"=>$html));
	}
		
	public function track_data() {
		$get_data = $this->input->post();
		//print_r($get_data); die;
		$data['tracking_data'] = array(); 
		if(isset($get_data) && !empty($get_data)){
			try{
				$trackings = new AfterShip\Trackings($this->config->item("after_ship_api_key"));
				$response = $trackings->get("interlink-express", $get_data['tracking_number']);
				//_pre($response);
				$data['tracking_data'] = $response;
			}
			catch(Exception $e){

			} 
		}
		// _pre($data);	
		$html = $this->load->view('network/pages/after_ship/view_result',$data,true);
		print json_encode(array("html"=>$html));
	}
}

