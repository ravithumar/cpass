<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BlockslotController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		$this->load->model('Clinic');
        $this->table_name = "block_slot";
        $this->load->model('User');
        $this->title = "Block Slot";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $network_id = $this->session->userdata('network')['user_id'];

        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('block_slot.id as block_slot_id,hospital.id, hospital.company_name,hospital.company_number,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website, hospital.status', false)
            ->from($this->table_name)
            ->join('hospital', 'hospital.id = block_slot.clinic_id')
            ->where('hospital.network_id',$network_id)
            ->where('block_slot.is_deleted',NULL)
            ->group_by('block_slot.clinic_id');

    
        $action['edit'] = base_url('network/block-slot/edit/');
        $action['details'] = base_url('network/block-slot/details/');
        $action['delete'] = base_url('network/block-slot/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Clinic Name', 'company_name')
            ->column('Company Website', 'company_website')
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
                $option .= '<a href="' . $action['details'] . $id . '" class="on-default text-secondary ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Details Field"><i class="la la-eye "></i></a>';
                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="block_slot"><i class="fa fa-trash"></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('network/block-slot/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('block_slot/index', $data);
	}

	public function add(){
        $data['title'] = "Block Slot Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('network/block-slot'); 
        $network_id = $this->session->userdata('network')['user_id'];

        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['hospital'] = $this->db->get_where('hospital',['network_id' => $network_id])->result_array();
        $request = $this->input->post(); 
        if (isset($_POST) && !empty($_POST)) {
            $request = $this->input->post();

            $slot = $this->db->get_where('block_slot',array('clinic_id' => $request['clinic_id']))->result_array();
            if(!empty($slot))
            {
                $this->session->set_flashdata('success', __('Slot Already Added successfully.'));
                redirect('network/block-slot');
            }
            else
            {
                $count = count($request['start_date']);

                for ($i=0; $i <$count ; $i++)
                { 
                    $insert_data['clinic_id'] = $request['clinic_id'];
                    $insert_data['start_date'] = $request['start_date'][$i];
                    $insert_data['end_date'] = $request['end_date'][$i];
                    $insert_data['start_time'] = $request['start_time'][$i];
                    $insert_data['end_time'] = $request['end_time'][$i];

                    $this->db->insert('block_slot',$insert_data);
                }
                
                $this->session->set_flashdata('success', __('Slot Added successfully.'));
                redirect('network/block-slot');
            }
            
        } else {
            $data['days'] = $this->db->get_where('days',['status'=>'1'])->result_array();
            $this->renderNetwork('block_slot/add', $data);
        }
    }

    public function edit($id = ''){
        $data['title'] = "Block Slot Edit";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('network/block-slot'); 
        $network_id = $this->session->userdata('network')['user_id'];

        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['hospital'] = $this->db->get_where('hospital',['network_id' => $network_id])->result_array();
        $request = $this->input->post(); 
        if (isset($_POST) && !empty($_POST)) {
            $request = $this->input->post();
                
                $slot_data = $this->db->get_where('block_slot',array('clinic_id'=>$id))->result_array();

                if ($slot_data)
                {   
                    $this->db->where('clinic_id',$id);
                    $this->db->delete('block_slot');

                    $count = count($request['start_date']);

                    for ($i=0; $i <$count ; $i++)
                    { 
                        $insert_data['clinic_id'] = $request['clinic_id'];
                        $insert_data['start_date'] = $request['start_date'][$i];
                        $insert_data['end_date'] = $request['end_date'][$i];
                        $insert_data['start_time'] = $request['start_time'][$i];
                        $insert_data['end_time'] = $request['end_time'][$i];

                        $this->db->insert('block_slot',$insert_data);
                    }
                }
                else
                {
                    $this->session->set_flashdata('warning', __('Your Data is not available.'));
                    redirect('network/block-slot');
                }
                
            
            $this->session->set_flashdata('success', __('Slot Updated successfully.'));
            redirect('network/block-slot');

        } else {
            $slot_avail = $this->db->get_where('block_slot',array('clinic_id' => $id))->result_array();
    
            $data['slot_avail'] = $slot_avail;
            $data['clinic_id'] = $id;
            $this->renderNetwork('block_slot/edit', $data);
        }
    }

    public function details($id = ''){
        $data['clinic'] = Clinic::find($id);
        $data['block_slot'] = $this->db->get_where('block_slot', array('clinic_id' => $id))->result_array();

        $data['home'] = base_url('network/block-slot');
        $data['title'] = "Block Slot Details";
        $data['main_title'] = $this->title;
        $this->renderNetwork('block_slot/details', $data);
        
    }

    public function delete($id)
    {
        $data = $this->db->get_where('block_slot',array('clinic_id'=>$id))->row_array();

         if ($data)
         {
              $update_data['is_deleted'] = '1';

            $this->db->where('clinic_id',$id);
            $this->db->update('block_slot',$update_data);
         }
       

        echo 1;
    }


    // public function validation_errors_response() {
	// 	$err_array = array();
	// 	$err_str = "";
	// 	$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
	// 	$err_str = ltrim($err_str, '|');
	// 	$err_str = rtrim($err_str, '|');
	// 	$err_array = explode('|', $err_str);
	// 	$err_array = array_filter($err_array);
	// 	return $err_array;
	// }

}
