<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PKPass\PKPass;

class BookingController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		$this->load->model('Lab');
		$this->load->model('Bookings');
		$this->load->model('BookingSession');
		$this->load->model('BookingSlots');
		$this->load->model('BookingsMembers');
        $this->table_name = "booking";
        $this->load->model('user');
        $this->title = "Booking";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);
	}


    public function sendCertificate() {

        $this->load->library('Pdf');
        $html = "";
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);


        // set document information
        $pdf->SetCreator('C-Pass');
        $pdf->SetAuthor("C-Pass");
        $pdf->SetTitle('G-16 Covid Certificate');
        $pdf->SetSubject('G-16 Covid Certificate');
        $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

        $pdf->SetPrintHeader(false);
        // $pdf->SetPrintFooter(false);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(10);

        $pdf->SetHeaderMargin(20);
        $pdf->SetFooterMargin(20);

        $pdf->SetAutoPageBreak(TRUE, 20);
        
        $pdf->SetDisplayMode('real', 'default');
        $pdf->SetFont('helvetica', '', 10);
        $pdf->AddPage('H');

        $pdf_logo = base_url('assets/images/logo/logo.png');
        $pdf_check = base_url('assets/images/logo/clinic.png');

        $pdf->Image($pdf_check, 150, 15, 40, 20);
        $pdf->Image($pdf_logo, 10, 15, 20, '');

        $pdf->SetFont('PDFAHelvetica', 'B', 18);
        $pdf->SetTextColor(30,30,30);
        $pdf->Text(25, 50, 'Certificate of Coronavirus (COVID-19) rtPCR Testing', false, false, true, 0, 1, 'L', false, '', 3, false, 'T', 'T');

        $pdf->Ln(5);

        $pdf->setCellPadding(3);

        $pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(209, 209, 211)));
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, '', "TL", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Company: force scaling', "TR", 1, 'Last', 0, '', 1);
       
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Full Name: jane', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Address: force scaling', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Date of birth: 02/12/1990', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Tel: force scaling', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Passport No: 02/12/1990', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Email: force scaling', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, '', "BL", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Website: force scaling', "BR", 1, 'Last', 0, '', 1);

        $pdf->Ln(2);

        $pdf->setCellPadding(2);

        $pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(209, 209, 211)));
        $pdf->SetFontSize(11);
        // $pdf->Cell(40, 0, 'Clinic:', "TL", 0, 'L', 0, '', 1);
        $txt = "The doctors clinic, \nLorem ipsum dolor sit amet, \nTel: +44 444444444, \nwww.rrtr.com";
        $txt1 = "Clinic:\n\n\n\n\n";
        $pdf->MultiCell(40, -10, $txt1, "TL", 'L', 0, 1, 10, 130.4, true, 0, false, true, 0, 'M', true);
        // $pdf->MultiCell(150, 0, $txt, "TR", 1, 'Last', 0, '', 1);
        $pdf->MultiCell(150, -10, $txt, "TR", 'L', 0, 1, 40, 130.4, true, 0, false, true, 0, 'M', true);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Sample Date: 02/12/1990, 23:10', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Result Date: 02/12/1990, 23:10', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Order ID: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Accession No: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Test Code: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Test Name: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Platform: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Result: 123456', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);
        // $pdf->setCellPaddings(0,50,0,0);

        $sign = base_url('assets/images/logo/signature.png');
        $qr_code = base_url('assets/images/logo/qr_code.png');
        $pdf->Image($qr_code, 100, 210, 30, 30);
        $pdf->Image($sign, 13, 220, 50, 20);
        
        $pdf->setCellPaddings(3,15,3,3);
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, '', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->setCellPaddings(3,0,0,3);
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'Mr james kinnor MBBS', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, 'Scan qr code', "R", 1, 'Last', 0, '', 1);

        $pdf->setCellPaddings(3,0,0,0);
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'MBBS Director', "L", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

        $pdf->setCellPaddings(3,0,0,0);
        $pdf->SetFontSize(11);
        $pdf->Cell(90, 0, 'MBBS Health', "BL", 0, 'L', 0, '', 1);
        $pdf->Cell(90, 0, '', "BR", 1, 'Last', 0, '', 1);

        $pdf->SetFont('PDFAHelvetica', 'B', 10);
        $pdf->SetTextColor(30,30,30);
        $pdf->Text(25, 260, 'Cerulean Health Ltd, Reg. No. 12345647477', false, false, true, 0, 1, 'C', false, '', 3, false, 'T', 'T');
        $pdf->Text(25, 265, 'Kemp house, 152-160 City Road, London, EC!VX', false, false, true, 0, 1, 'C', false, '', 3, false, 'T', 'T');

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('Certificate.pdf', 'I');

        // return $data;
    }
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');

        $filter_request = $this->input->post();
        $where = [];
        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();

        // hospital.company_name,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website,

        $product->select('booking.id, booking.booking_no,users.phone,booking.type,booking.total_price, booking.status,booking.booking_status,booking_member.reference_member_id,booking.created_at,booking_member.report_file', false)
            ->from("booking")
            ->where('booking.is_deleted','0')
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('users', 'booking_member.user_id = users.id')
            // ->where('booking_member.clinic_id',$hospitalArr->id)
            ->where($where)
            ->where('booking.booking_status !=','in-complete')
            ->group_by('booking.id');

        $action['view'] = base_url('network/booking/details/');
        $action['edit'] = base_url('network/booking/edit/');
        $action['delete'] = base_url('network/booking/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            // ->column('', 'created_at')
            ->column('Create Date','created_at', function($created_at){
                return date('d-m-Y',strtotime($created_at));
            })
            ->column('Reference Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Booking Reference Number','reference_member_id', function($reference_member_id){
                return $reference_member_id;
            })
            // ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'type',function ($type){
                return ucfirst($type);
            })
            ->column('Contact No', 'phone')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
	            if ($status == "success") {
	               return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Booking Completed</span></h5>';
	            }
	            else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >In-Fulfillment</span></h5>';
	            }else{
	                return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst(str_replace('_',' ',$status)).'</span></h5>';
                }
	        })
            ->column('Download Certificate', 'report_file',function ($report_file){
                if ($report_file != "") { 
                    return '<a target="_blank" href="' .$report_file . '" >Download Certificate</a>';
                }else{
                    return '';
                }
            })           
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['view'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Field"><i class="la la-eye "></i></a>';
                $option .= '<a href="' . $action['edit'] . $id . '" class="on-default ml-2 text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="booking"><i class="fa fa-trash"></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('booking/index', $data);
	}
	
	public function history() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');

        $filter_request = $this->input->post();
        $where["booking_member.report_status != "] = null;
        /* if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        } */
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();


        /* $product->select('booking.id, booking.booking_no,users.phone,booking.type,booking.total_price, booking.status,booking.booking_status,booking_member.reference_member_id,booking.created_at,booking_member.report_file', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            ->join('users', 'booking_member.user_id = users.id')
            ->where($where)
            ->group_by('booking.id'); */


		$product->select('booking_member.id,booking_member.booking_id,booking_member.reference_member_id,booking_member.report_id,
						booking_member.shipping_method,booking_member.shipping_date,booking_member.pickup_date,booking_member.report_status,
						booking_member.report_file,booking.type as purpose,booking.created_at as booking_created_at,booking.booking_no,
						booking.total_price,users.phone,booking.booking_status',false)
		->from("booking_member")
		->join('booking', 'booking.id = booking_member.booking_id')
		->join('users', 'booking_member.user_id = users.id')
		->where($where)
        ->where('booking.booking_status !=','in-complete')
        
		//->where("booking_member.report_status != ",null)
		//->join('hospital','booking_member.clinic_id = hospital.id','left')
		//->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left')
		->group_by('booking.id');
        $action['view'] = base_url('network/booking/details/');
        //$action['edit'] = base_url('network/booking/edit/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'booking_id')
            // ->column('', 'created_at')
            ->column('Create Date','booking_created_at', function($created_at){
                return date('d-m-Y',strtotime($created_at));
            })
            ->column('Reference Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Booking Reference Number','reference_member_id', function($reference_member_id){
                return $reference_member_id;
            })
            // ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'purpose',function ($type){
                return ucfirst($type);
            })
            ->column('Contact No', 'phone')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
	            if ($status == "success") {
	               return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Booking Completed</span></h5>';
	            }
	            else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >In-Fulfillment</span></h5>';
	            }else{
	                return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst(str_replace('_',' ',$status)).'</span></h5>';
                }
	        })
            ->column('Download Certificate', 'report_file',function ($report_file){
                if ($report_file != "") { 
                    return '<a target="_blank" href="' .$report_file . '" >Download Certificate</a>';
                }else{
                    return '';
                }
            })           
	        ->column('Actions', 'booking_id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['view'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Field"><i class="la la-eye "></i></a>';
                //$option .= '<a href="' . $action['edit'] . $id . '" class="on-default ml-2 text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('booking/booking_history', $data);
	}
	

    public function details($id = ''){

        $this->db->select('booking_member.*,booking.type,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,booking_member.report_file');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->result_array();
        //_pre($data);


        if (!empty($data['booking'])) {
            foreach ($data['booking'] as $key => $value) {

                // $this->db->select('users.email,users.phone,users.date_of_birth,users.passport,users.full_name,booking_member.home_address,reports.name as test,reports.id as report_id,hospital.id as hospital_id,booking_slots.date as booking_date,booking_slots.date as date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,users.id as user_id, hospital.company_name as place_of_test, booking_member.id as booking_member_id');
                //     $this->db->from('booking_member');
                //     $this->db->join('hospital','booking_member.clinic_id = hospital.id');
                //     $this->db->join('users','booking_member.user_id = users.id');
                //     $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                //     $this->db->join('reports','reports.id = booking_member.report_id');
                //     $this->db->where('booking_member.id',$value['booking_member_id']);
                //     $this->db->where('users.active',1);
                //     $query = $this->db->get();
                //     $bookings = $query->row_array();

                //     $data['booking'][$key]['booking_member'] = $bookings;
				
				$this->db->select('booking_member.id,booking_member.booking_id,booking_member.reference_member_id,booking_member.report_id,booking_member.shipping_method,booking_member.shipping_date,booking_member.pickup_date,booking_member.report_status,booking_member.report_file,booking.type as purpose,hospital.company_name,booking_slots.date as slot_date,booking_slots.slots as slot_time');
				$this->db->from("booking_member");
				$this->db->join('booking', 'booking.id = booking_member.booking_id');
				$this->db->join('users', 'booking_member.user_id = users.id');
				$this->db->join('hospital','booking_member.clinic_id = hospital.id','left');
				$this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left');
				//$this->db->where("booking.booking_status","success");
				$this->db->where('booking_member.user_id',$value['user_id']);
				$this->db->where_not_in('booking_member.booking_id', [$id]);
				//$this->db->group_by('booking.id');
				$this->db->order_by('booking_member.booking_id',"desc");
				$query = $this->db->get();
		     	$booking_history = $query->result_array();
				if(!empty($booking_history)){
					foreach($booking_history as $booking_key=>$booking_value){
						$report_name = get_column_value("reports",array("id"=>$booking_value['report_id']),"name");
						$booking_history[$booking_key]['report_name'] = $report_name;
					}
				}
				
				$data['booking'][$key]['booking_history'] = $booking_history;
				
                $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                if ($value['is_excempted'] == "yes") {
                    $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                    if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                }else{  $data['booking'][$key]['cat_name'] = ""; }

                if($value['vaccine_id'] != ''){
                    $vaccine = $this->db->get_where('certified_vaccines',array('id'=>$value['vaccine_id']))->row_array();
                    $data['booking'][$key]['vaccine_name'] = $vaccine['name'];
                }
                  
                if($value['destination_country'] != ''){                  
                    $country = $this->db->get_where('country',array('id'=>$value['destination_country']))->row_array();
                    $data['booking'][$key]['destination_country'] = $country['name'];
                }
               
                if($value['is_visiting_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['is_visiting_country']))->row_array();
                    $data['booking'][$key]['is_visiting_country'] = $country['name'];
                }
                
                if($value['departure_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['departure_country']))->row_array();
                    $data['booking'][$key]['departure_country'] = $country['name'];
                }
                
                if($value['return_country'] != ''){
                    $country = $this->db->get_where('country',array('id'=>$value['return_country']))->row_array();
                    $data['booking'][$key]['return_country'] = $country['name'];
                }
                
                $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                $data['booking'][$key]['booking_member']['report_file'] = $value['report_file'];
                // $data['booking'][$key]['booking_member']['purpose_type'] = $value['type'];
                $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

                //  $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                // if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                // if ($value['is_excempted'] == "yes") {
                //     $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                //     if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                // }else{  $data['booking'][$key]['cat_name'] = ""; }

                // $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                // // $data['booking'][$key]['booking_member']['purpose_type'] = $value['type'];
                // $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

            }
        }
        $data['home'] = base_url('network/booking');
        $data['title'] = "Booking Details";
        $data['main_title'] = $this->title;
        $this->renderNetwork('booking/details', $data);

    }

    public function detailstest($id = ''){

        $this->db->select('booking_member.*,booking.type,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->result_array();

        if (!empty($data['booking'])) {
            foreach ($data['booking'] as $key => $value) {

                // $this->db->select('users.email,users.phone,users.date_of_birth,users.passport,users.full_name,booking_member.home_address,reports.name as test,reports.id as report_id,hospital.id as hospital_id,booking_slots.date as booking_date,booking_slots.date as date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,users.id as user_id, hospital.company_name as place_of_test, booking_member.id as booking_member_id');
                //     $this->db->from('booking_member');
                //     $this->db->join('hospital','booking_member.clinic_id = hospital.id');
                //     $this->db->join('users','booking_member.user_id = users.id');
                //     $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                //     $this->db->join('reports','reports.id = booking_member.report_id');
                //     $this->db->where('booking_member.id',$value['booking_member_id']);
                //     $this->db->where('users.active',1);
                //     $query = $this->db->get();
                //     $bookings = $query->row_array();

                //     $data['booking'][$key]['booking_member'] = $bookings;



                $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                if ($value['is_excempted'] == "yes") {
                    $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                    if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                }else{  $data['booking'][$key]['cat_name'] = ""; }

                $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                // $data['booking'][$key]['booking_member']['purpose_type'] = $value['type'];
                $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

                //  $reports = (array)$this->db->get_where('reports',array('id' => $value['report_id']))->row();
                // if (!empty($reports)) {$data['booking'][$key]['report_name'] = $reports['name'];}else{ $data['booking'][$key]['report_name'] = ""; }
                // if ($value['is_excempted'] == "yes") {
                //     $categories = (array)$this->db->get_where('category',array('id' => $value['excempted_cat']))->row();
                //     if (!empty($categories)) {$data['booking'][$key]['cat_name'] = $categories['name'];}else{ $data['booking'][$key]['cat_name'] = ""; }
                // }else{  $data['booking'][$key]['cat_name'] = ""; }

                // $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                // // $data['booking'][$key]['booking_member']['purpose_type'] = $value['type'];
                // $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

            }
        }

        // _pre($data['booking']); exit();

        $data['home'] = base_url('network/booking');
        $data['title'] = "Booking Details";
        $data['main_title'] = $this->title;
        $this->renderNetwork('booking/details', $data);

    }
    
    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

    public function change_booking_status(){
        $input = $this->input->post();

        //$this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone,users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,ud.full_name as clinic_user_name,booking.user_id as u_id');
        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone, users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,users.full_name as clinic_user_name,booking.user_id as u_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        //$this->db->join('hospital','booking_member.clinic_id = hospital.id','left');
        $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left');
        $this->db->join('reports','reports.id = booking_member.report_id');
        $this->db->join('users','booking_member.user_id = users.id');
        //$this->db->join('users as ud','ud.id = hospital.user_id');
        $this->db->where('booking.id',$input['booking_id']);
        $get_data = $this->db->get()->result_array();
       // print_r($get_data);
       // die;

        if(isset($get_data) && !empty($get_data))
        {
            foreach ($get_data as $key => $value) 
            {

                if($value['type'] == "international-arrival"){ 
                    if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
                        $booking_date = date('d/m/Y', strtotime($value['pickup_date']));
                    }else{
                        $booking_date = date('d/m/Y', strtotime($value['shipping_date']));
                    }
                }else{
                    $booking_date = date('d/m/Y', strtotime($value['booking_date']));
                }

                $result_date = date('d/m/Y');
                $user_dob = "";
                if(!empty($value['user_dob'])){
                    $user_dob = date('d/m/Y', strtotime($value['user_dob']));
                }
                $reference_member_id = $value['booking_no'];
               
                $clinic = $value['qr_code'];
                $qr_code = base_url('assets/images/logo/qr_code.png');
                if($clinic != '' && $clinic != null)
                {
                    $qr_code = $clinic;
                }

                $this->load->library('Pdf');
                $html = "";
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

                $pdf->SetCreator('C-Pass');
                $pdf->SetAuthor("C-Pass");
                $pdf->SetTitle('G-16 Covid Certificate');
                $pdf->SetSubject('G-16 Covid Certificate');
                $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetTopMargin(10);
                $pdf->setFooterMargin(10);

                $pdf->SetHeaderMargin(20);
                $pdf->SetFooterMargin(20);

                $pdf->SetAutoPageBreak(TRUE, 20);
                
                $pdf->SetDisplayMode('real', 'default');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->AddPage('H');

                $pdf_logo = base_url('assets/images/logo/logo.png');
                $pdf_header_logo = base_url('assets/images/logo/logo-shadow.png');
                $header_logo = base_url('assets/images/logo/pdf-header.png');
                $pdf_check = base_url('assets/images/logo/clinic.png');

                ob_start(); 
                $header_table = '<table border=0>
                    <tr>
                        <td><img src="'.$header_logo.'" style="margin-top:20px"></td>
                        
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $header_table, 0, 1, 0, true, '', true);
                $pdf->Image($pdf_header_logo, 15, 13, 35, 35);
                $pdf->Image($qr_code, 150, 13, 35, 35);
                $pdf->SetTextColor(12, 91, 151);
                $pdf->Text(115, 41.7, $reference_member_id,false, false, true, 0, 1, 'L', false, '', 1, false, 'T', 'T');
                $pdf->Ln(14);
                $pdf->SetTextColor(30,30,30);
                
                $result_table = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <th><b>COVID-19 TEST RESULT CERTIFICATE</b></th>
                    </tr>
                    <tr>'; 
                    if (ucfirst($input['status']) == "Positive") {
                        $result_table .= '<th><b>RESULT: <span style="color:#FF0000">'.strtoupper($input['status']).'</span></b></th>';
                    }else{
                        $result_table .= '<th><b>RESULT: <span style="color:#008000">'.strtoupper($input['status']).'</span></b></th>';
                    }
                    $result_table .='</tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $result_table, 0, 1, 0, true, '', true);

                $pdf->Ln(5);

                $user_info_table = '<table border="1" style="font-size:14px;border-collapse: collapse;">
                    <tr nobr="true">
                        <th colspan="2"><span style="color:#0C5B97;font-weight:bold">NAME:</span> '.$value['full_name'].'</th>
                    </tr>
                    <tr nobr="true">
                        <th colspan="2"></th>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">SPECIMEN DATE:</span> '.$result_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">PASSPORT NUMBER:</span> '.$value['user_passport'].'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">REPORT DATE:</span> '. $booking_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">DATE OF BIRTH:</span> '.$user_dob.'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">GENDER:</span> '.strtoupper($value['user_gender']).'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">TEST TYPE:</span> '.$value['test_name'].'</td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $user_info_table, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $first_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p><b>'.ucfirst($input['status']).' Result</b></p></td>
                    </tr>';
                    if (ucfirst($input['status']) == "Positive") {
                        
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Detected. You can use this certificate to
                            contact your travel agent/airline and work to confirm that based on their policies and
                            regulations you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';
                    }else{
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Not Detected. You can use this certificate to 
                            contact your travel agent/airline and work to confirm that based on their policies and regulations 
                            you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';

                    }

                $first_paragraph .= '</table>';
                $pdf->writeHTMLCell(0, 0, '', '', $first_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $second_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">For more advice, please visit www.gov.uk/coronavirus</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $second_paragraph, 0, 1, 0, true, '', true);
                
                $pdf->Ln(3);
                $third_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">This test was validated by G16 Enterprises Ltd, confirmation of these results will be recorded 
                        on our database. Your data will be held securely and will not be passed to any third parties. 
                        We are required to submit details of results if requested to do so by UK
                        government agencies and/or regulatory bodies such as Medicines and Healthcare
                        Regulatory Authority (MHRA) Department of Health & Social Care (DHSC) or Public Health
                        England (PHE)</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $third_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $note = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left"><b>This certificate shall not be reproduced except in full without approval from the issuer.</b></p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $note, 0, 1, 0, true, '', true);
                $sign = base_url('assets/images/logo/signature.png');
                $pdf->Ln(3);
                $footer = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td width="200" align="center">
                            G16 Enterprises Ltd <br/>
                            2 Kings Court <br/>
                            Horsham <br/>
                            West Sussex <br/>
                            RH13 9FZ <br/>
                            United Kingdom<br/>
                        </td>
                        <td width="50">
                            <img src="'.$pdf_logo.'" width="50" style="margin-bottom:0px">
                        </td>
                        <td width="50">
                            <img src="'.$sign.'" width="50" style="margin-top:0px">
                        </td>
                        <td width="250">
                            UKAS REGISTRATION: 22906 <br/><br/>
                            info@cpass.co.uk <br/>
                            +44 (0)203 991 6494
                        </td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $footer, 0, 1, 0, true, '', true);
                $pdf->writeHTML($html, true, false, true, false, '');
                ob_end_clean();
                $base_url = base_url();
                $base_path = $_SERVER['DOCUMENT_ROOT'];
                $base_path .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
               
                $pdf_name = "hazard_register_".date('Y_m_d_H_i_s').".pdf";
                $save_base_pdf_path = $base_url."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_full_pdf_path = $base_path."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_pdf_path = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                
                $pdf->Output($save_full_pdf_path, 'F');

                $url = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
		        $folder = "assets/pdf/";
		       	s3Upload($url, $folder);
		        $pdf_url = S3_URL.$url;


                $update_data['report_file'] = $pdf_url;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $update_data['report_status'] = $input['status'];
                
                $this->db->update('booking_member',$update_data, array('booking_id' => $value['booking_id']));
                //$booking_date = date('d/m/Y', strtotime($value['booking_date']));

                $email_var_data["user_name"] = $value['full_name'];
                $email_var_data["booking_id"] = $input['booking_id'];
                $email_var_data["pdf_link"] = $pdf_url; 
                $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is POSITIVE.  Please stay at home and isolate from your family members.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is POSITIVE.  Please stay at home and isolate from your family members.";
                
                if($input['status'] == 'negative')
                {
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is NEGATIVE. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                    $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is NEGATIVE. You did not have the virus when the test was done.";
                }
                
                $message = $this->load->view('email/certificate_information',$email_var_data,true);
                $notification_title = "You result is ".strtoupper($input['status']).".";
                send_push_notification($value['u_id'],$notification_title,$notification_message,$value['booking_id'],$input['status'],"bookingDetail");
                $mail = send_mail($value['user_email'],'Booking Information', $message);

                // genrate pass

                $pass = new PKPass(base_url()."Certificates.p12", 'apple');

                $booking_id = $reference_member_id;
                $name = $value['full_name'];
                if($input['status'] == 'negative'){
                    $result = "NEGATIVE";
                }else{
                    $result = "POSITIVE";
                }
                
                $test_type = $value['test_name'];

                // Variables
                $id = rand(100000, 999999) . '-' . rand(100, 999) . '-' . rand(100, 999); // Every card should have a unique serialNumber
                $name = stripslashes($name);
                // "backgroundColor": "rgb(12,91,151)",

                $pass->setData('{
                    "passTypeIdentifier": "pass.com.g16.CPass", 
                    "formatVersion": 1,
                    "organizationName": "CPASS",
                    "teamIdentifier": "D483AH4KH4",
                    "serialNumber": "' . $id . '",
                    "backgroundColor": "rgb(12,91,151)",
                    "foregroundColor": "rgb(255,255,255)",
                    "labelColor": "rgb(255,255,255)",
                    "logoText": "C-PASS",
                    "description": "C Pass",
                    "storeCard": {
                        "primaryFields": [
                            {
                                "key": "name",
                                "label": "NAME",
                                "value": "' . $name . '"
                            },
                            

                        ],
                        "secondaryFields": [
                            {
                                "key": "result",
                                "label": "RESULT",
                                "value": "' . $result . '"
                            }

                        ],
                        "auxiliaryFields": [
                            {
                                "key": "test_type",
                                "label": "TEST TYPE",
                                "value": "' . $test_type . '"
                            },
                            {
                                "key": "date",
                                "label": "RESULT DATE",
                                "value": "' . date('d M, Y') . '"
                            }

                        ],
                        "backFields": [
                            {
                                "key": "id",
                                "label": "Booking Number",
                                "value": "' . $booking_id . '"
                            }
                        ]
                    }
                    }');

                // add files to the PKPass package
                $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/icon.png');
                // $pass->addFile('icon@2x.png');
                $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/logo.png');
                // $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/header.png');
                $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/background.png');
                // $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/thumbnail.png');
                // $pass->addFile('/var/www/dev.coronatest.co.uk/assets/pkpass/footer.png');

                if(!$pass->create(true, $booking_id)) { // Create and output the PKPass
                    $error = 'Error: ' . $pass->getError();
                }


            }
        }

        Bookings::where("id",$input['booking_id'])->update(array("booking_status"=>'success', "pass_link" => base_url('pkpass_files/').$booking_id.'.pkpass'));
        
        print json_encode(array("status"=>true));
    }

    public function change_booking_status_bkp(){
        $input = $this->input->post();

        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email,users.gender as user_gender, hospital.company_name, hospital.company_website, hospital.company_number, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,ud.full_name as clinic_user_name');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
        $this->db->join('reports','reports.id = booking_member.report_id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->join('users as ud','ud.id = hospital.user_id');
        $this->db->where('booking.id',$input['booking_id']);
        $get_data = $this->db->get()->result_array();


        if(isset($get_data) && !empty($get_data))
        {
            foreach ($get_data as $key => $value) 
            {
                $this->load->library('Pdf');
                $html = "";
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

                $pdf->SetCreator('C-Pass');
                $pdf->SetAuthor("C-Pass");
                $pdf->SetTitle('G-16 Covid Certificate');
                $pdf->SetSubject('G-16 Covid Certificate');
                $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetTopMargin(10);
                $pdf->setFooterMargin(10);

                $pdf->SetHeaderMargin(20);
                $pdf->SetFooterMargin(20);

                $pdf->SetAutoPageBreak(TRUE, 20);
                
                $pdf->SetDisplayMode('real', 'default');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->AddPage('H');

                $pdf_logo = base_url('assets/images/logo/logo.png');
                $pdf_check = base_url('assets/images/logo/clinic.png');

                $pdf->Image($pdf_check, 150, 15, 40, 20);
                $pdf->Image($pdf_logo, 10, 15, 20, '');

                $pdf->SetFont('PDFAHelvetica', 'B', 18);
                $pdf->SetTextColor(30,30,30);
                $pdf->Text(25, 50, 'Certificate of Coronavirus (COVID-19) Testing', false, false, true, 0, 1, 'L', false, '', 3, false, 'T', 'T');

                $pdf->Ln(5);

                $pdf->setCellPadding(3);

                $pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(209, 209, 211)));
                $pdf->SetFontSize(11);
                
                $company_name = $value['company_name'];
                $pdf->Cell(90, 0, '', "TL", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Company: '.$company_name, "TR", 1, 'Last', 0, '', 1);
                
                $full_name = $value['full_name'];
                $user_address = $value['user_address'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Full Name: '.$full_name, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Address: '.$user_address, "R", 1, 'Last', 0, '', 1);

                $user_dob = date('d/m/Y', strtotime($value['user_dob']));
                $user_phone = $value['user_phone'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Date of birth: '.$user_dob, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Tel: '.$user_phone, "R", 1, 'Last', 0, '', 1);

                $user_passport = $value['user_passport'];
                $user_email = $value['user_email'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Passport No: '.$user_passport, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Email: '.$user_email, "R", 1, 'Last', 0, '', 1);


                $company_website = $value['company_website'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, '', "BL", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Website: '.$company_website, "BR", 1, 'Last', 0, '', 1);

                $pdf->Ln(2);

                $pdf->setCellPadding(2);

                $pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(209, 209, 211)));
                $pdf->SetFontSize(11);

                $company_name = $value['company_name'];
                $company_number = $value['company_number'];
                $company_website = $value['company_website'];

                $txt = $company_name.", \nTel: ".$company_number.", \n".$company_website;
                $txt1 = "Clinic:\n\n\n\n\n";
                $pdf->MultiCell(40, -10, $txt1, "TL", 'L', 0, 1, 10, 130.4, true, 0, false, true, 0, 'M', true);
                $pdf->MultiCell(150, -10, $txt, "TR", 'L', 0, 1, 40, 130.4, true, 0, false, true, 0, 'M', true);

                $test_name = $value['test_name'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Test Name: '.$test_name, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                $result_date = date('d/m/Y, h:i');
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Result Date: '.$result_date, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                $reference_member_id = $value['booking_no'];
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, 'Order Id: '.$reference_member_id, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                if($value['type'] == "international-arrival"){ 
                    if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
                        $booking_date = date('d/m/Y', strtotime($value['pickup_date']));
                        $booking_label = "Pickup Date:";
                    }else{
                        $booking_date = date('d/m/Y', strtotime($value['shipping_date']));
                        $booking_label = "Shipping Date:";
                    }
                }else{
                    $booking_date = date('d/m/Y', strtotime($value['booking_date']));
                    $booking_label = "Booking Date:";
                }

                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, $booking_label.' '.$booking_date, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                if($value['type'] = "international-arrival"){ 
                    $booking_time = $value['booking_time'];
                    $pdf->SetFontSize(11);
                    $pdf->Cell(90, 0, 'Booking Time: '.$booking_time, "L", 0, 'L', 0, '', 1);
                    $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);
                }

                $has_covid = $input['status'];
                $pdf->SetFontSize(11); 
                $pdf->Cell(90, 0, 'Result: '.$has_covid, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                $sign = base_url('assets/images/logo/signature.png');

                $clinic = $value['qr_code'];
                $qr_code = base_url('assets/images/logo/qr_code.png');
                if($clinic != '' && $clinic != null)
                {
                    $qr_code = $clinic;
                }
                $pdf->Image($qr_code, 100, 190, 40, 40);
                $pdf->Image($sign, 13, 205, 50, 20);
                
                $pdf->setCellPaddings(3,15,3,3);
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, '', "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                $clinic_user_name = $value['clinic_user_name'];
                $pdf->setCellPaddings(3,0,0,3);
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, $clinic_user_name, "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, 'Scan QR code', "R", 1, 'Last', 0, '', 1);

                $pdf->setCellPaddings(3,0,0,0);
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, '', "L", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "R", 1, 'Last', 0, '', 1);

                $pdf->setCellPaddings(3,0,0,0);
                $pdf->SetFontSize(11);
                $pdf->Cell(90, 0, '', "BL", 0, 'L', 0, '', 1);
                $pdf->Cell(90, 0, '', "BR", 1, 'Last', 0, '', 1);

                $pdf->SetFont('PDFAHelvetica', 'B', 10);
                $pdf->SetTextColor(30,30,30);
                
                $pdf->writeHTML($html, true, false, true, false, '');

                $base_url = base_url();
                $base_path = $_SERVER['DOCUMENT_ROOT'];
                $base_path .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
               
                $pdf_name = "hazard_register_".date('Y_m_d_H_i_s').".pdf";
                $save_base_pdf_path = $base_url."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_full_pdf_path = $base_path."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_pdf_path = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";

                $pdf->Output($save_full_pdf_path, 'F');
                $update_data['report_file'] = $save_pdf_path;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $update_data['report_status'] = $input['status'];
                
                $this->db->update('booking_member',$update_data, array('booking_id' => $value['booking_id']));
                $booking_date = date('d/m/Y', strtotime($value['booking_date']));

                $email_var_data["user_name"] = $value['full_name'];
                $email_var_data["booking_id"] = $input['booking_id'];
                $email_var_data["pdf_link"] = base_url($save_pdf_path); 
                $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is POSITIVE.  Please stay at home and isolate from your family members.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                if($input['status'] == 'negative')
                {
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is NEGATIVE. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                }
                
                $message = $this->load->view('email/certificate_information',$email_var_data,true);
             
                $mail = send_mail($value['user_email'],'Booking Information', $message);
            }
        }

        Bookings::where("id",$input['booking_id'])->update(array("booking_status"=>'success'));
        
        print json_encode(array("status"=>true));
    }

    public function change_booking_status_demo($id){
        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone,users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, hospital.company_name, hospital.company_website, hospital.company_number, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,ud.full_name as clinic_user_name');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
        $this->db->join('reports','reports.id = booking_member.report_id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->join('users as ud','ud.id = hospital.user_id');
        $this->db->where('booking.id',$id);
        $get_data = $this->db->get()->result_array();

        if(isset($get_data) && !empty($get_data)){
            foreach ($get_data as $key => $value){
                if($value['type'] == "international-arrival"){ 
                    if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
                        $booking_date = date('d/m/Y', strtotime($value['pickup_date']));
                    }else{
                        $booking_date = date('d/m/Y', strtotime($value['shipping_date']));
                    }
                }else{
                    $booking_date = date('d/m/Y', strtotime($value['booking_date']));
                }

                $result_date = date('d/m/Y');
                $user_dob = date('d/m/Y', strtotime($value['user_dob']));
                $reference_member_id = $value['booking_no'];
               
                $clinic = $value['qr_code'];
                $qr_code = base_url('assets/images/logo/qr_code.png');
                if($clinic != '' && $clinic != null)
                {
                    $qr_code = $clinic;
                }

                $this->load->library('Pdf');
				
                $html = "";
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

                // set document information
                $pdf->SetCreator('C-Pass');
                $pdf->SetAuthor("C-Pass");
                $pdf->SetTitle('G-16 Covid Certificate');
                $pdf->SetSubject('G-16 Covid Certificate');
                $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetTopMargin(10);
                $pdf->setFooterMargin(10);

                $pdf->SetHeaderMargin(20);
                $pdf->SetFooterMargin(20);

                $pdf->SetAutoPageBreak(TRUE, 20);
                
                $pdf->SetDisplayMode('real', 'default');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->AddPage('H');

                $pdf_logo = base_url('assets/images/logo/logo.png');
                $pdf_header_logo = base_url('assets/images/logo/logo-shadow.png');
                $header_logo = base_url('assets/images/logo/pdf-header.png');
                $pdf_check = base_url('assets/images/logo/clinic.png');

               /*  $pdf->Image($pdf_check, 150, 15, 40, 20);
                $pdf->Image($pdf_logo, 10, 15, 20, '');
                $pdf->Ln(25); */
                //$pdf->Ln(5);
                
                ob_start(); 
                $header_table = '<table border=0>
                    <tr>
                        <td><img src="'.$header_logo.'" style="margin-top:20px"></td>
                        
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $header_table, 0, 1, 0, true, '', true);

                $pdf->Image($pdf_header_logo, 15, 13, 35, 35);
                $pdf->Image($qr_code, 150, 13, 35, 35);
                //$pdf->Text(105, 50, $reference_member_id, false, false, true, 0, 1, 'L', false, '', 1, false, 'T', 'T');
                $pdf->SetTextColor(12, 91, 151);
                $pdf->Text(115, 41.7, $reference_member_id,false, false, true, 0, 1, 'L', false, '', 1, false, 'T', 'T');
                $pdf->Ln(14);
                $pdf->SetTextColor(30,30,30);
                $result_table = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <th><b>COVID-19 TEST RESULT CERTIFICATE</b></th>
                    </tr>
                    <tr>
                        <th><b>RESULT: <span style="color:#008000">NEGATIVE</span></b></th>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $result_table, 0, 1, 0, true, '', true);

                $pdf->Ln(5);

                $user_info_table = '<table border="1" style="font-size:14px;border-collapse: collapse;">
                    <tr nobr="true">
                        <th colspan="2"><span style="color:#0C5B97;font-weight:bold">NAME:</span> '.$value['full_name'].'</th>
                    </tr>
                    <tr nobr="true">
                        <th colspan="2"></th>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">SPECIMEN DATE:</span> '.$result_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">PASSPORT NUMBER:</span> '.$value['user_passport'].'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">REPORT DATE:</span> '. $booking_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">DATE OF BIRTH:</span> '.$user_dob.'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">GENDER:</span> '.strtoupper($value['user_gender']).'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">TEST TYPE:</span> '.$value['test_name'].'</td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $user_info_table, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $first_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="justify" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:center"><b>Negative Result</b></p></td>
                    </tr>
                    <tr>
                        <td><p style="text-align:justify">At the time of your test Covid-19 was Not Detected. You can use this certificate to contact 
                        your travel agent/airline and work to confirm that based on their policies and regulations 
                        you are permitted to either fly and or go back to work. If someone you live with had a test 
                        and Covid-19 was detected or you have been traced as a contact of someone who was
                        detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $first_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $second_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="justify" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:justify">For more advice, please visit www.gov.uk/coronavirus</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $second_paragraph, 0, 1, 0, true, '', true);
                
                $pdf->Ln(3);
                $third_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="justify" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:justify">This test was validated by G16 Enterprises Ltd, confirmation of these results will be recorded 
                        on our database. Your data will be held securely and will not be passed to any third parties. 
                        We are required to submit details of results if requested to do so by UK
                        government agencies and/or regulatory bodies such as Medicines and Healthcare
                        Regulatory Authority (MHRA) Department of Health & Social Care (DHSC) or Public Health
                        England (PHE)</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $third_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $note = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="justify" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:justify"><b>This certificate shall not be reproduced except in full without approval from the issuer.</b></p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $note, 0, 1, 0, true, '', true);
                $sign = base_url('assets/images/logo/signature.png');
                $pdf->Ln(3);
                $footer = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td width="200" align="center">
                            G16 Enterprises Ltd <br/>
                            2 Kings Court <br/>
                            Horsham <br/>
                            West Sussex <br/>
                            RH13 9FZ <br/>
                            United Kingdom<br/>
                        </td>
                        <td width="50">
                            <img src="'.$pdf_logo.'" width="50" style="margin-bottom:0px">
                        </td>
                        <td width="50">
                            <img src="'.$sign.'" width="50" style="margin-top:0px">
                        </td>
                        <td width="250">
                            UKAS REGISTRATION: 22906 <br/><br/>
                            info@cpass.co.uk <br/>
                            +44 (0)203 991 6494
                        </td>
                    </tr>
                </table>';
                //echo $footer; die;
                $pdf->writeHTMLCell(0, 0, '', '', $footer, 0, 1, 0, true, '', true);

            
                $pdf->writeHTML($html, true, false, true, false, '');
                ob_end_clean();
                $pdf->Output('Certificate.pdf', 'I'); //preview pdf
                die;

                $base_url = base_url();
                $base_path = $_SERVER['DOCUMENT_ROOT'];
                $base_path .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
               
                $pdf_name = "hazard_register_".date('Y_m_d_H_i_s').".pdf";
                $save_base_pdf_path = $base_url."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_full_pdf_path = $base_path."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_pdf_path = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                ob_end_clean();
                $pdf->Output($save_full_pdf_path, 'I');
                //die;
                $update_data['report_file'] = $save_pdf_path;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $update_data['report_status'] = "negative";
                
                $this->db->update('booking_member',$update_data, array('booking_id' => $value['booking_id']));
                $booking_date = date('d/m/Y', strtotime($value['booking_date']));

                $email_var_data["user_name"] = $value['full_name'];
                $email_var_data["booking_id"] = $id;
                $email_var_data["pdf_link"] = base_url($save_pdf_path); 
                $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is POSITIVE.  Please stay at home and isolate from your family members.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                /* if($input['status'] == 'negative')
                {
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is NEGATIVE. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                } */
                
                //$message = $this->load->view('email/certificate_information',$email_var_data,true);
             
                //$mail = send_mail($value['user_email'],'Booking Information', $message);
            }
        }

        //Bookings::where("id",$id)->update(array("booking_status"=>'success'));
        
        //print json_encode(array("status"=>true));
    }

    public function change_booking_detail_status(){
        $input = $this->input->post();

        //$this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone, users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, hospital.company_name, hospital.company_website, hospital.company_number, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,ud.full_name as clinic_user_name,booking.user_id as u_id');
        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone, users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,users.full_name as clinic_user_name,booking.user_id as u_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        //$this->db->join('hospital','booking_member.clinic_id = hospital.id','left');
        $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left');
        $this->db->join('reports','reports.id = booking_member.report_id');
        $this->db->join('users','booking_member.user_id = users.id');
        //$this->db->join('users as ud','ud.id = hospital.user_id');
        $this->db->where('booking_member.id',$input['book_member_id']);
        $get_data = $this->db->get()->row_array();
        //_pre($get_data);

        if(isset($get_data) && !empty($get_data))
        {
                $value = $get_data;
                //echo $value['user_id'];
                if($value['type'] == "international-arrival"){ 
                    if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
                        $booking_date = date('d/m/Y', strtotime($value['pickup_date']));
                    }else{
                        $booking_date = date('d/m/Y', strtotime($value['shipping_date']));
                    }
                }else{
                    $booking_date = date('d/m/Y', strtotime($value['booking_date']));
                }

                $result_date = date('d/m/Y');
                $user_dob = "";
                if(!empty($value['user_dob'])){
                    $user_dob = date('d/m/Y', strtotime($value['user_dob']));
                }
                $reference_member_id = $value['booking_no'];
               
                $clinic = $value['qr_code'];
                $qr_code = base_url('assets/images/logo/qr_code.png');
                if($clinic != '' && $clinic != null)
                {
                    $qr_code = $clinic;
                }


                $this->load->library('Pdf');
                $html = "";
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);


                // set document information
                $pdf->SetCreator('C-Pass');
                $pdf->SetAuthor("C-Pass");
                $pdf->SetTitle('G-16 Covid Certificate');
                $pdf->SetSubject('G-16 Covid Certificate');
                $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetTopMargin(10);
                $pdf->setFooterMargin(10);

                $pdf->SetHeaderMargin(20);
                $pdf->SetFooterMargin(20);

                $pdf->SetAutoPageBreak(TRUE, 20);
                
                $pdf->SetDisplayMode('real', 'default');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->AddPage('H');

                $pdf_logo = base_url('assets/images/logo/logo.png');
                $pdf_header_logo = base_url('assets/images/logo/logo-shadow.png');
                $header_logo = base_url('assets/images/logo/pdf-header.png');
                $pdf_check = base_url('assets/images/logo/clinic.png');

                ob_start(); 
                $header_table = '<table border=0>
                    <tr>
                        <td><img src="'.$header_logo.'" style="margin-top:20px"></td>
                        
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $header_table, 0, 1, 0, true, '', true);
                $pdf->Image($pdf_header_logo, 15, 13, 35, 35);
                $pdf->Image($qr_code, 150, 13, 35, 35);
                $pdf->SetTextColor(12, 91, 151);
                $pdf->Text(115, 41.7, $reference_member_id,false, false, true, 0, 1, 'L', false, '', 1, false, 'T', 'T');
                $pdf->Ln(14);
                $pdf->SetTextColor(30,30,30);
                
                $result_table = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <th><b>COVID-19 TEST RESULT CERTIFICATE</b></th>
                    </tr>
                    <tr>'; 
                    if (ucfirst($input['status']) == "Positive") {
                        $result_table .= '<th><b>RESULT: <span style="color:#FF0000">'.strtoupper($input['status']).'</span></b></th>';
                    }else{
                        $result_table .= '<th><b>RESULT: <span style="color:#008000">'.strtoupper($input['status']).'</span></b></th>';
                    }
                    $result_table .='</tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $result_table, 0, 1, 0, true, '', true);

                $pdf->Ln(5);

                $user_info_table = '<table border="1" style="font-size:14px;border-collapse: collapse;">
                    <tr nobr="true">
                        <th colspan="2"><span style="color:#0C5B97;font-weight:bold">NAME:</span> '.$value['full_name'].'</th>
                    </tr>
                    <tr nobr="true">
                        <th colspan="2"></th>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">SPECIMEN DATE:</span> '.$result_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">PASSPORT NUMBER:</span> '.$value['user_passport'].'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">REPORT DATE:</span> '. $booking_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">DATE OF BIRTH:</span> '.$user_dob.'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">GENDER:</span> '.strtoupper($value['user_gender']).'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">TEST TYPE:</span> '.$value['test_name'].'</td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $user_info_table, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $first_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p><b>'.ucfirst($input['status']).' Result</b></p></td>
                    </tr>';
                    if (ucfirst($input['status']) == "Positive") {
                        
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Detected. You can use this certificate to
                            contact your travel agent/airline and work to confirm that based on their policies and
                            regulations you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';
                    }else{
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Not Detected. You can use this certificate to 
                            contact your travel agent/airline and work to confirm that based on their policies and regulations 
                            you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';

                    }

                $first_paragraph .= '
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $first_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $second_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">For more advice, please visit www.gov.uk/coronavirus</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $second_paragraph, 0, 1, 0, true, '', true);
                
                $pdf->Ln(3);
                $third_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">This test was validated by G16 Enterprises Ltd, confirmation of these results will be recorded 
                        on our database. Your data will be held securely and will not be passed to any third parties. 
                        We are required to submit details of results if requested to do so by UK
                        government agencies and/or regulatory bodies such as Medicines and Healthcare
                        Regulatory Authority (MHRA) Department of Health & Social Care (DHSC) or Public Health
                        England (PHE)</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $third_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $note = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left"><b>This certificate shall not be reproduced except in full without approval from the issuer.</b></p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $note, 0, 1, 0, true, '', true);
                $sign = base_url('assets/images/logo/signature.png');
                $pdf->Ln(3);
                $footer = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td width="200" align="center">
                            G16 Enterprises Ltd <br/>
                            2 Kings Court <br/>
                            Horsham <br/>
                            West Sussex <br/>
                            RH13 9FZ <br/>
                            United Kingdom<br/>
                        </td>
                        <td width="50">
                            <img src="'.$pdf_logo.'" width="50" style="margin-bottom:0px">
                        </td>
                        <td width="50">
                            <img src="'.$sign.'" width="50" style="margin-top:0px">
                        </td>
                        <td width="250">
                            UKAS REGISTRATION: 22906 <br/><br/>
                            info@cpass.co.uk <br/>
                            +44 (0)203 991 6494
                        </td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $footer, 0, 1, 0, true, '', true);
                $pdf->writeHTML($html, true, false, true, false, '');
                ob_end_clean();

                $base_url = base_url();
                $base_path = $_SERVER['DOCUMENT_ROOT'];
                $base_path .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
               
                $pdf_name = "hazard_register_".date('Y_m_d_H_i_s').".pdf";
                $save_base_pdf_path = $base_url."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_full_pdf_path = $base_path."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_pdf_path = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";

                $pdf->Output($save_full_pdf_path, 'F');


                $url = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
		        $folder = "assets/pdf/";
		       	s3Upload($url, $folder);
		        $pdf_url = S3_URL.$url;

                $update_data['report_file'] = $pdf_url;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $update_data['report_status'] = $input['status'];
                
                $this->db->update('booking_member',$update_data, array('id' => $input['book_member_id']));
                //$booking_date = date('d/m/Y', strtotime($value['booking_date']));

                $email_var_data["user_name"] = $value['full_name'];
                $email_var_data["booking_id"] = $value['booking_id'];
                $email_var_data["pdf_link"] = $pdf_url; 
                $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is POSITIVE.  Please stay at home and isolate from your family members.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is POSITIVE.  Please stay at home and isolate from your family members.";
                if($input['status'] == 'negative'){
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is NEGATIVE. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                    $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is NEGATIVE. You did not have the virus when the test was done.";
                }
                
                $message = $this->load->view('email/certificate_information',$email_var_data,true);
                $mail = send_mail($value['user_email'],'Booking Information', $message);
                $notification_title = "You result is ".strtoupper($input['status']).".";
                send_push_notification($value['u_id'],$notification_title,$notification_message,$value['booking_id'],$input['status'],"bookingDetail");
                Bookings::where("id",$value['booking_id'])->update(array("booking_status"=>'success'));
                print json_encode(array("status"=>true));
        }
        else
        {
			print json_encode(array("status"=>false));

        }
        
        
    }

    public function edit($id = ''){
       
        $this->db->select('booking.type,booking.total_price,booking.tax_price,booking.report_price,booking_member.*,booking_member.id as booking_member_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->result_array();
        if (!empty($data['booking'])) {
            foreach ($data['booking'] as $key => $value) {
                $data['booking'][$key]['booking_member'] = (array)$this->db->get_where('users',array('id' => $value['user_id']))->row();
                $data['booking'][$key]['booking_slots'] = (array)$this->db->get_where('booking_slots',array('booking_member_id' => $value['booking_member_id']))->row();

            }
        }
        $data['booking_id'] = $id;
        $data['home'] = base_url('network/booking');
        $data['title'] = "Booking Edit";
        $data['main_title'] = $this->title;
        
        if (isset($_POST) && !empty($_POST)) {
            $request = $this->input->post();
            if(!empty($request['id'])){
                foreach($request['id'] as $key=>$id){
                    $post['date'] = $request['slot_date'][$key];
                    $post['slots'] = $request['slot'][$key] ? $request['slot'][$key] : NULL;
                    BookingSlots::where('id', $id)->update($post);
                }
            }
            
            $this->session->set_flashdata('success', __('Booking Update successfully'));
            redirect('network/booking');
        }else{
            $this->renderNetwork('booking/edit', $data);
        }
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo "1";
    }
        
}
