<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		$this->load->model('category');
        $this->load->model('user');
        $this->load->model('Report');

	}


	public function index() {
		
		$where = [];
		if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }

        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        $events1 = array();
        // hospital.company_name,hospital.address as hospital_address,hospital.point_of_contact,hospital.company_website,hospital.no_of_employee,
		$events = $this->db->select('booking.id, booking.booking_no,booking.booking_status,booking.total_price,booking.payment_status,users.phone,users.email,users.gender,users.date_of_birth,users.address,booking.type,booking.total_price,booking_slots.date as slot_date,booking_slots.slots as slot_time, booking_slots.id as slot_id, booking.status,booking.booking_status,users.username ,users.full_name,booking_member.id as booking_member_id,booking_member.reference_member_id,booking_member.report_id, booking_member.clinic_id,booking_member.shipping_date, booking_member.pickup_date,booking_member.shipping_method', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left')
            ->join('users', 'booking_member.user_id = users.id')
            ->where($where)
            ->where('booking.booking_status !=','in-complete')
            ->group_by('booking.booking_no')->get()->result_array();

            if (!empty($events)) {

                foreach ($events as $key => $value) {
                	// $slotdata = $this->db->select('date, slots, booking_status', false)->from('booking_slots')->where('booking_member_id',$value['booking_member_id'])->get()->row_array();
                    $booking_member = $this->db->get_where('booking_member',array('booking_id' => $value['id']))->row_array();
                    // if ($value['clinic_id'] !="") {
                    // }
                    $hospital = $this->db->get_where('hospital',array('id' => $value['clinic_id']))->row_array();
                    if (!empty($hospital)) {
                        $events1[$key]['company_name'] = $hospital['company_name'];
                        $events1[$key]['hospital_address'] = $hospital['address'];
                        $events1[$key]['company_website'] = $hospital['company_website'];
                    }else{
                        $events1[$key]['company_name'] ="";
                        $events1[$key]['hospital_address'] = "";
                        $events1[$key]['company_website'] = "";
                    }
                    
                    $report_name = Report::where("id",$value['report_id'])->value("name");
                    if(empty($report_name)){
                        $report_name = "";
                    }

                	$events1[$key]['title'] = '#'.$value['booking_no'];
                    $events1[$key]['booking_id'] = $value['id'];

                    if($value['type'] == "international-arrival"){
                        if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
        					$events1[$key]['start'] = date('Y-m-d',strtotime($value['pickup_date']));
                            $events1[$key]['date_label'] = "Pickup date: ";
        				}else{
        					$events1[$key]['start'] = date('Y-m-d',strtotime($value['shipping_date']));
                            $events1[$key]['date_label'] = "Shipping date: ";
        				}
                        $events1[$key]['slot_time'] = $value['slot_time'];
                    }else{
                        $events1[$key]['date_label'] = "Slot date: ";
                        $events1[$key]['start'] = date('Y-m-d',strtotime($value['slot_date']));
                        $events1[$key]['slot_time'] = $value['slot_time'];
                    }
                	
                    $events1[$key]['phone'] = $value['phone'];
                    $events1[$key]['username'] = $value['full_name'];
                    $events1[$key]['email'] = $value['email'];
                    $events1[$key]['booking_status'] = $value['booking_status'];
                    $events1[$key]['total_price'] = $value['total_price'];
                    $events1[$key]['report_name'] = $report_name;
                    $events1[$key]['reference_member_id'] = $value['reference_member_id'];
                    $events1[$key]['payment_status'] = $value['payment_status'];
                    $events1[$key]['gender'] = $value['gender'];
                    $events1[$key]['address'] = $value['address'];
                    // $events1[$key]['hospital_address'] = $value['hospital_address'];
                    $events1[$key]['date_of_birth'] = (!empty($value['date_of_birth']))?date('d-m-Y',strtotime($value['date_of_birth'])):'';
                    $events1[$key]['result_status'] = $booking_member['report_status'];
                    $events1[$key]['type'] = $value['type'];
                    	
                }
        }
        
        //_pre($events1);
      
        // $events[0]['start'] = '2021-08-23 08:15';
        // $events[0]['end'] = '2021-08-23 08:30';
        // $events[1]['start'] = '2021-08-24 12:00';
        // $events[1]['end'] = '2021-08-24 14:00';
        // $events[2]['start'] = '2021-08-25 08:00';
        // $events[2]['end'] = '2021-08-25 10:00';
        // $events[3]['start'] = '2021-08-26 15:00';
        // $events[3]['end'] = '2021-08-26 17:00';
        // $events[4]['start'] = '2021-08-27 18:00';
        // $events[4]['end'] = '2021-08-27 19:00';
        // _pre($events1);    exit();   

		$data['events'] = $events1;
        // _pre($data);
		$data['user'] = User::count();		
		$data['category'] = Category::count();		
		$data['advertisement'] = 0;		
		$data['service'] = 0;		
		$this->renderNetwork('dashboard',$data);
	}

	public function test() {

		$where = [];
		if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }

        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }

        // hospital.no_of_employee,hospital.point_of_contact,hospital.company_website,hospital.address as hospital_address,hospital.company_name,

		$events = $this->db->select('booking.id, booking.booking_no,booking.booking_status,booking.total_price,booking.payment_status,users.phone,users.email,users.gender,users.date_of_birth,users.address,booking.type,booking.total_price,booking_slots.date as slot_date,booking_slots.slots as slot_time, booking_slots.id as slot_id, booking.status,booking.booking_status,users.username ,users.full_name,booking_member.id as booking_member_id,booking_member.reference_member_id,booking_member.report_id,booking_member.clinic_id', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('booking_slots','booking_slots.booking_member_id = booking_member.id')
            ->join('users', 'booking_member.user_id = users.id')
            ->where($where)
            ->group_by('booking.id')->get()->result_array();

        foreach ($events as $key => $value) {
            	// $slotdata = $this->db->select('date, slots, booking_status', false)->from('booking_slots')->where('booking_member_id',$value['booking_member_id'])->get()->row_array();
                
                $report_name = Report::where("id",$value['report_id'])->value("name");
                if(empty($report_name)){
                    $report_name = "";
                }
                if ($value['clinic_id'] !="") {
                    $hospital = $this->db->get_where('hospital',array('id' => $value['clinic_id']))->row_array();
                }
                if (!empty($hospital)) {
                    $events1[$key]['company_name'] = $hospital['company_name'];
                    $events1[$key]['hospital_address'] = $hospital['address'];
                    $events1[$key]['company_website'] = $hospital['company_website'];
                }else{
                    $events1[$key]['company_name'] ="";
                    $events1[$key]['hospital_address'] = "";
                    $events1[$key]['company_website'] = "";
                }
            	$events1[$key]['title'] = '#'.$value['booking_no'];
                $events1[$key]['booking_id'] = $value['id'];
            	$events1[$key]['start'] = $value['slot_date'];
                $events1[$key]['slot_time'] = $value['slot_time'];
                $events1[$key]['phone'] = $value['phone'];
                $events1[$key]['username'] = $value['full_name'];
                $events1[$key]['email'] = $value['email'];
                $events1[$key]['booking_status'] = $value['booking_status'];
                $events1[$key]['total_price'] = $value['total_price'];
                $events1[$key]['report_name'] = $report_name;
                $events1[$key]['reference_member_id'] = $value['reference_member_id'];
                $events1[$key]['payment_status'] = $value['payment_status'];
                $events1[$key]['gender'] = $value['gender'];
                $events1[$key]['address'] = $value['address'];
                // $events1[$key]['hospital_address'] = $value['hospital_address'];
                $events1[$key]['date_of_birth'] = (!empty($value['date_of_birth']))?date('d-m-Y',strtotime($value['date_of_birth'])):'';
            	
        }
      
        // $events[0]['start'] = '2021-08-23 08:15';
        // $events[0]['end'] = '2021-08-23 08:30';
        // $events[1]['start'] = '2021-08-24 12:00';
        // $events[1]['end'] = '2021-08-24 14:00';
        // $events[2]['start'] = '2021-08-25 08:00';
        // $events[2]['end'] = '2021-08-25 10:00';
        // $events[3]['start'] = '2021-08-26 15:00';
        // $events[3]['end'] = '2021-08-26 17:00';
        // $events[4]['start'] = '2021-08-27 18:00';
        // $events[4]['end'] = '2021-08-27 19:00';
        // _pre($events);    

		$data['events'] = $events1;
		$data['user'] = User::count();		
		$data['category'] = Category::count();		
		$data['advertisement'] = 0;		
		$data['service'] = 0;		
		$this->renderNetwork('dashboardtest',$data);
	}
}
