<?php

use function Webmozart\Assert\Tests\StaticAnalysis\false;

defined('BASEPATH') OR exit('No direct script access allowed');
class ShipStationController extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!$this->ion_auth->is_network())
        {
            redirect('/auth/login');
        }
        $this->table_name = "booking";
        // $this->load->model('User');
        $this->title = "Ship Station Orders";
        $this->load->model('ion_auth_model'); 
    }

    public function index(){
        $curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            // CURLOPT_URL => "https://ssapi.shipstation.com/accounts/listtags",
            // CURLOPT_URL => "https://ssapi.shipstation.com/stores",
            //CURLOPT_URL => SHIP_URL."orders?storeId=28571&sortBy=OrderDate&orderStatus=shipped&sortDir=DESC",
            CURLOPT_URL => SHIP_URL."orders?sortBy=OrderDate&sortDir=DESC&orderStatus=shipped",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username
            ),
        ));
        $response = curl_exec($curl);
		// _pre(json_decode($response));
        curl_close($curl);
        if (!empty($response)) {
            $result = (json_decode($response)); 
            if (isset($result->orders)) {
                $data['orders'] = $result->orders; 
            }
        }

        $data['title'] = $this->title;
        $data['add_url'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('shipstation/index', $data);
    }

    public function get_reports(){
        $data['title'] = $this->title;
        $this->load->library('Datatables');
        $filter_request = $this->input->post();
        $where = [];
        if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
            $where['booking.booking_status'] = $filter_request['filter_value'];
        }
        if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
            $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();
         // hospital.company_name,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website,
        $str = "booking_member.shipstation IS NULL AND (booking.booking_status != 'in-complete' AND booking.booking_status != 'pending')";

        $product->select('booking.id, booking.booking_no,users.phone,users.email,users.full_name,booking.type,booking.total_price, booking.status,booking.booking_status,booking_member.reference_member_id,booking_member.id as booking_member_id,booking_member.qr_code, booking.created_at,booking_member.report_file', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('users', 'booking_member.user_id = users.id')
            ->where('booking.type','domestic')
            ->where('booking_member.type','home')
            ->where($str)
            ->where($where);
            //->group_by('booking.id');
        // $action['view'] = base_url('network/booking/details/');
        $action['edit'] = base_url('network/shipstation/fullfillment/edit/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Order Date','created_at', function($created_at){
                return date('d-m-Y',strtotime($created_at));
            })
            ->column('Order Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Booking Reference Number','reference_member_id', function($reference_member_id){
                return $reference_member_id;
            })
            ->column('Name', 'full_name')
            ->column('Email', 'email')
            ->column('Contact No', 'phone')
            // ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'type',function ($type){
                return ucfirst($type);
            })
            // ->column('Barcode Number', 'qr_code')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
                if ($status == "success") {
                   return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Success</span></h5>';
                }
                else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >In-Fulfillment</span></h5>';
                }else{
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst(str_replace('_',' ',$status)).'</span></h5>';
                }
            })
                 
            ->column('Actions', 'booking_member_id', function ($booking_member_id) use ($action) {
                $option ='<button type="button" class="btn btn-sm btn-success waves-effect waves-light edit_order" title="Save Barcode" data-toggle="modal" data-booking_member_id="'.$booking_member_id.'" data-target="#close-modal">Edit & Save order</button>';
                return $option;
            });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('shipstation/fullfillment/initiate_index_test', $data);
    }
    public function order_edit_popup() {
        $data = array();
        //_pre("{\"orderId\":55304110,\"orderNumber\":\"1002322435\",\"orderKey\":\"8cae0894f6d04949b97bcfebc3103cd7\",\"orderDate\":\"2021-09-29T08:46:27.0000000\",\"createDate\":\"2021-10-21T01:06:16.8500000\",\"modifyDate\":\"2021-10-21T01:06:16.8030000\",\"paymentDate\":\"2021-09-29T08:46:27.0000000\",\"shipByDate\":\"2021-09-29T00:00:00.0000000\",\"orderStatus\":\"awaiting_shipment\",\"customerId\":null,\"customerUsername\":\"dharii@yopmail.com\",\"customerEmail\":\"dharii@yopmail.com\",\"billTo\":{\"name\":\"Terris\",\"company\":null,\"street1\":null,\"street2\":null,\"street3\":null,\"city\":null,\"state\":null,\"postalCode\":null,\"country\":null,\"phone\":null,\"residential\":null,\"addressVerified\":null},\"shipTo\":{\"name\":\"The President\",\"company\":\"US Govt\",\"street1\":\"1600 Pennsylvania Ave\",\"street2\":null,\"street3\":null,\"city\":\"Washington\",\"state\":\"\",\"postalCode\":\"382480\",\"country\":\"US\",\"phone\":\"555-555-5555\",\"residential\":false,\"addressVerified\":\"Address validation failed\"},\"items\":[{\"orderItemId\":67339979,\"lineItemKey\":\"vd08-MSLbtx\",\"sku\":\"ABC123\",\"name\":\"Test item #1\",\"imageUrl\":null,\"weight\":{\"value\":24.00,\"units\":\"ounces\",\"WeightUnits\":1},\"quantity\":2,\"unitPrice\":0.00,\"taxAmount\":null,\"shippingAmount\":null,\"warehouseLocation\":null,\"options\":[],\"productId\":null,\"fulfillmentSku\":null,\"adjustment\":false,\"upc\":null,\"createDate\":\"2021-10-21T01:06:16.803\",\"modifyDate\":\"2021-10-21T01:06:16.803\"}],\"orderTotal\":0.00,\"amountPaid\":0.00,\"taxAmount\":0.00,\"shippingAmount\":0.00,\"customerNotes\":\"Please ship as soon as possible!\",\"internalNotes\":\"Customer called and would like to upgrade shipping\",\"gift\":false,\"giftMessage\":null,\"paymentMethod\":\"Credit Card\",\"requestedShippingService\":null,\"carrierCode\":null,\"serviceCode\":null,\"packageCode\":null,\"confirmation\":\"none\",\"shipDate\":\"2021-09-29\",\"holdUntilDate\":null,\"weight\":{\"value\":25.00,\"units\":\"ounces\",\"WeightUnits\":1},\"dimensions\":null,\"insuranceOptions\":{\"provider\":null,\"insureShipment\":false,\"insuredValue\":0.0},\"internationalOptions\":{\"contents\":\"merchandise\",\"customsItems\":null,\"nonDelivery\":\"return_to_sender\"},\"advancedOptions\":{\"warehouseId\":29368,\"nonMachinable\":false,\"saturdayDelivery\":false,\"containsAlcohol\":false,\"mergedOrSplit\":false,\"mergedIds\":[],\"parentId\":null,\"storeId\":38870,\"customField1\":null,\"customField2\":null,\"customField3\":null,\"source\":\"Webstore\",\"billToParty\":null,\"billToAccount\":null,\"billToPostalCode\":null,\"billToCountryCode\":null,\"billToMyOtherAccount\":null},\"tagIds\":null,\"userId\":null,\"externallyFulfilled\":false,\"externallyFulfilledBy\":null,\"labelMessages\":null}");
        $data1 = $this->input->post();
        if (isset($data1['booking_member_id'])) {
            // booking_member.*,
            $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price ,users.full_name as name,users.email as email,users.phone as phone, booking.tax_price,booking.report_price,booking_member.id as booking_member_id,booking.payment_type,booking.created_at');
            $this->db->from('booking');
            $this->db->join('booking_member','booking.id = booking_member.booking_id');
            $this->db->join('users','booking_member.user_id = users.id');
            $this->db->where('booking.type','domestic');
            $this->db->where('booking_member.type','home');
            $this->db->where('booking_member.id',$data1['booking_member_id']);
            $query = $this->db->get();
            $data['data'] = $query->row_array();

            $carrier_code = "apicode-dpd-local";
            $api_key = SHIP_API_KEY;
            $secret_key = SHIP_SECRET_KEY;
            $username =  base64_encode($api_key.":".$secret_key);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => SHIP_URL."carriers/listpackages?carrierCode=$carrier_code",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Host: ssapi.shipstation.com",
                    "Authorization: Basic ".$username

                ),
            ));
            $package_list_response = curl_exec($curl);
            $package_list_response = json_decode($package_list_response);

            curl_setopt_array($curl, array(
                CURLOPT_URL => SHIP_URL."carriers/listservices?carrierCode=$carrier_code",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Host: ssapi.shipstation.com",
                    "Authorization: Basic ".$username

                ),
            ));
            $service_list_response = curl_exec($curl);
            $service_list_response = json_decode($service_list_response);
           
            $data['package_list'] = $package_list_response;
            $data['service_list'] = $service_list_response;
            curl_close($curl);

            $html = $this->load->view('network/pages/shipstation/fullfillment/edit_order',$data,true);

        }else{
            $html ="";
        }
        print json_encode(array("html"=>$html));
    }

    public function ship_awaiting(){ 
        $curl = curl_init();
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        // $username =  base64_encode("b441cb0939ae4370a13e97545774b1d4:3e0c7a92ddf14484ad7d94f9b2481a4c");
        // $curl = curl_init();
            curl_setopt_array($curl, array(
            // CURLOPT_URL => "https://ssapi.shipstation.com/accounts/listtags",
            // CURLOPT_URL => "https://ssapi.shipstation.com/products",
            CURLOPT_URL => SHIP_URL."orders?storeId=38870&sortBy=OrderDate&orderStatus=awaiting_shipment&sortDir=DESC",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

              ),
            ));
        
        $response = curl_exec($curl);

        curl_close($curl);
        //_pre(json_decode($response));

        if (!empty($response)) {
            $data['orderData'] = (json_decode($response)); 
            if (isset($data['orderData']->orders)) {
                $data['orders'] = $data['orderData']->orders; 
            }
        }

        $data['title'] = $this->title;
        $data['add_url'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('shipstation/awaiting_index', $data);
    }

    public function initiate_fulfillment(){

        $data['title'] = $this->title;
        $this->load->library('Datatables');

        $filter_request = $this->input->post();
        // $where = [];
        // if (isset($filter_request['filter_value']) && !empty($filter_request['filter_value']) && $filter_request['filter_value'] != '') {
        //     $where['booking.booking_status'] = $filter_request['filter_value'];
        // }
        // if (isset($filter_request['filter_date']) && !empty($filter_request['filter_date']) && $filter_request['filter_date'] != '') {
        //     $where['DATE(booking.created_at)'] = $filter_request['filter_date'];
        // }
        
        $product = new Datatables;
        $clinicIds = array();
        $bookIds = array();
        // $where['shipstation'] = ;
        // hospital.company_name,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website,
        $str = "booking_member.shipstation IS NULL AND (booking.booking_status != 'in-complete' AND booking.booking_status != 'pending') AND (booking.type = 'international-arrival' OR booking.type = 'domestic')";
        $product->select('booking.id, booking.booking_no,users.phone,booking.type,booking.total_price, booking.status,booking.booking_status,booking_member.reference_member_id,booking_member.id as booking_member_id,booking_member.qr_code, booking.created_at,booking_member.report_file', false)
            ->from("booking")
            ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('hospital', 'booking_member.clinic_id = hospital.id')
            ->join('users', 'booking_member.user_id = users.id')
            ->where('booking_member.type','home')
            ->where($str);  
            // ->where($where);
            //->group_by('booking.id');
        // $action['view'] = base_url('network/booking/details/');
        $action['edit'] = base_url('network/shipstation/fullfillment/edit/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            // ->column('', 'created_at')
            ->column('Create Date','created_at', function($created_at){
                return date('d-m-Y',strtotime($created_at));
            })
            ->column('Reference Number','booking_no', function($booking_no){
                return "#".$booking_no;
            })
            ->column('Booking Reference Number','reference_member_id', function($reference_member_id){
                return $reference_member_id;
            })
            // ->column('Place of test', 'company_name')
            ->column('Total Price', 'total_price')
            ->column('Purpose of Type', 'type',function ($type){
                return ucfirst($type);
            })
            ->column('Contact No', 'phone')
            // ->column('Barcode Number', 'qr_code')
            ->column('Booking Status', 'booking_status', function ($status, $row) {
                if ($status == "success") {
                   return '<h5 class="mb-0 mt-0"><span class="badge badge-success cursor-pointer font-15 status_' . $status . '" >Booking Completed</span></h5>';
                }
                else if ($status == "in-process") {
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-secondary cursor-pointer font-15 status_' . $row['id'] . '" >In-Fulfillment</span></h5>';
                }else{
                    return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" >'.ucfirst(str_replace('_',' ',$status)).'</span></h5>';
                }
            })
            ->column('Download Certificate', 'report_file',function ($report_file){
                if ($report_file != "") { 
                    return '<a target="_blank" href="'.$report_file.'" >Download Certificate</a>';
                }else{
                    return '';
                }
            })           
            ->column('Actions', 'booking_member_id', function ($booking_member_id) use ($action)
            {
                $option = '<button type="button" class="btn btn-success waves-effect waves-light" title="Save Barcode" data-toggle="modal" data-target="#close-modal'.$booking_member_id . '"><i class="la la-pencil-square "></i></button>

                <div id="close-modal'.$booking_member_id . '" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-modal="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Scan Barcode</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body p-4">
                                <div class="row">
                                    <div class="col-md-12">  
                                        <div class="status_success_message'.$booking_member_id.'">
                                   
                                        </div>      
                                        <label for="field-1" class="control-label">Barcode Number</label>
                                        <input type="text" class="form-control member'.$booking_member_id.'" data-memberId="'.$booking_member_id.'" placeholder="29567946565" autocomplete="off" >
                                        <div class="text-danger err_barcode" style="display:none;"></div>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" onclick="save_barcode('.$booking_member_id.');" class=" btn btn-info waves-effect waves-light">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div> ';
                return $option;
            });

        $product->datatable($this->table_name);
        $product->init();

        $data['datatable'] = true;
        $data['export'] = false;
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('shipstation/fullfillment/initiate_index', $data);
    }

    public function save_barcode(){
        $input = $this->input->post();

        $update_data['updated_at'] = date('Y-m-d H:i:s');
        $update_data['qr_code'] = $input['barcode'];
        $id = $input['id'];
        $this->db->update('booking_member',$update_data, array('id' => $id));

        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price ,users.full_name as name,users.email as email,users.phone as phone, booking.tax_price,booking.report_price,booking_member.id as booking_member_id,booking_member.qr_code,booking.payment_type,booking_member.report_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('users','booking_member.user_id = users.id');
        // $this->db->where('booking.type','domestic');
        $this->db->where('booking_member.type','home');
        $this->db->where('booking_member.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->row_array();
        // _pre($data['booking']);
        
        if (!empty($data['booking'])) {
            $reports = (array)$this->db->get_where('reports',array('id' => $data['booking']['report_id']))->row();
            if (!empty($reports)) {$data['booking']['report_name'] = $reports['name']; $data['booking']['SKU'] = $reports['SKU'];  }else{ $data['booking']['report_name'] = ""; $data['booking']['SKU'] = "";  }
            if ($data['booking']['reference_member_id'] != "") {
                $booking_refernce_id = $data['booking']['reference_member_id'];
            }else{
                $booking_refernce_id = $data['booking']['booking_no'];
            }
            $booking_no = $data['booking']['booking_no'];
            $report_name = $data['booking']['report_name'];
            $SKU = $data['booking']['SKU'];
            $shippingpostal_code = $data['booking']['shippingpostal_code'];
            $shipping_address = $data['booking']['shipping_address'];
            $shipping_date = $data['booking']['shipping_date'];
            $payment_type = $data['booking']['payment_type'];
            $shipname = $data['booking']['name'];
            $email = $data['booking']['email'];
            $phone = $data['booking']['phone'];
            $updated_at = $data['booking']['updated_at'];
            $qr_code =$data['booking']['qr_code'];
            $randItemKey = $this->rand_char();
            if ($data['booking']['address_line1'] != "") {
                $address_line1 = $data['booking']['address_line1'];
                $address_line2 = $data['booking']['address_line2'];
                $address_line3 = $data['booking']['address_line3'];
                $shipping_city = $data['booking']['shipping_city'];
                $shipping_state = $data['booking']['shipping_state'];
            }else{

                $address_line1  = substr($shipping_address, 0, 49);
                $address_line2  = substr($shipping_address, 49,99);
                $address_line3  = substr($shipping_address, 100);
                $shipping_city = $data['booking']['shipping_city'];
                $shipping_state = $data['booking']['shipping_state'];
                // $address_line2  = strlen($shipping_address) > 49 ? substr($shipping_address,49,100): $shipping_address;
                // $shipping_address = data;
            }
            // exit();
            // echo "   string  ";
            // _pre($address_line2);

            // $insertKey ='{"derNumber":"'.$booking_no.'","orderDate":"'.$updated_at.'","paymentDate":"'.$updated_at.'","shipByDate":"'.$shipping_date.'", "orderStatus":"awaiting_shipment","customerUsername":"'.$shipname.'","customerEmail":"'.$email.'","billTo":{"name":"'.$shipname.'"},"shipTo":{"name":"'.$shipname.'","company":"","street1":"'.$shipping_address.'","city":"","state":"","postalCode":"'.$shippingpostal_code.'","country":"","phone":"'.$phone.'"},"items":[{"lineItemKey":"'.$randItemKey.'","sku":"'.$SKU.'","name":"'.$report_name.'","quantity":1,"productId":5089702}],"customerNotes":"","internalNotes":"","customField1":"'.$qr_code.'","paymentMethod":"'.$payment_type.'","shipDate":"'.$shipping_date.'","advancedOptions":{"storeId":38870,"source":"Webstore"}}';
            // _pre($insertKey);
            // '.$shipping_address.'
			$insertKey = '{"orderNumber":"'.$booking_refernce_id.'","orderDate":"'.$updated_at.'","paymentDate":"'.$updated_at.'","shipByDate":"'.$shipping_date.'","carrierCode":"apicode-dpd-local", "packageCode":"dpd_local_package","serviceCode":"dpd_local_parcel_next_day","orderStatus":"awaiting_shipment","customerUsername":"'.$shipname.'","customerId":"222223","customerEmail":"'.$email.'","billTo":{"name":"'.$shipname.'"},"shipTo":{"name":"'.$shipname.'","company":"","street1":"'.$address_line1.'","street2":"'.$address_line2.'","street3":"'.$address_line3.'","city":"'.$shipping_city.'","state":"'.$shipping_state.'", "postalCode":"'.$shippingpostal_code.'","country":"GB","phone":"'.$phone.'"}, "items":[{"lineItemKey":"'.$randItemKey.'","sku":"'.$SKU.'","name":"'.$report_name.'","weight":{"value":24,"units":"ounces"},"quantity":1,"productId":5089702}],"customerNotes":"", "internalNotes":"","paymentMethod":"'.$payment_type.'", "shipDate":"'.$shipping_date.'","weight":{"value":100,"units":"grams"}, "dimensions": { "units": "centimeters", "length": 15, "width": 15, "height": 15 }, "advancedOptions":{"storeId":38870,"source":"Webstore"}}';
            // _pre($insertKey);

            //$data1 = '{"orderNumber":"1002322435","orderDate":"2021-09-29T08:46:27.0000000","paymentDate":"2021-09-29T08:46:27.0000000","shipByDate":"2021-09-29T00:00:00.0000000","orderStatus":"awaiting_shipment","customerId":37701499,"customerUsername":"dharii@yopmail.com","customerEmail":"dharii@yopmail.com","billTo":{"name":"Terris"},"shipTo":{"name":"The President","company":"US Govt","street1":"1600 Pennsylvania Ave","city":"Washington","state":"DC","postalCode":"20500", "country":"US","phone":"555-555-5555"},"items":[{"lineItemKey":"vd08-MSLbtx","sku":"ABC123", "name":"Test item #1","weight":{"value":24,"units":"ounces"},"quantity":2,"productId":5072970}],"customerNotes":"Please ship as soon as possible!","internalNotes":"Customer called and would like to upgrade shipping","paymentMethod":"Credit Card","shipDate":"2021-09-29","weight":{"value":25,"units":"ounces"},"advancedOptions":{"storeId":38870,"source":"Webstore"}}';

			
            $curl = curl_init();
            $api_key = SHIP_API_KEY;
            $secret_key = SHIP_SECRET_KEY;
            $username =  base64_encode($api_key.":".$secret_key);            

            curl_setopt_array($curl, array(
                CURLOPT_URL => SHIP_URL."orders/createorder",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>$insertKey,
                CURLOPT_HTTPHEADER => array(
                    "Host: ssapi.shipstation.com",
                    "Authorization: Basic ".$username,
                    "Content-Type: application/json"

                ),
            ));
            $response = curl_exec($curl);
			if (curl_errno($curl)) {
				$error_msg = curl_error($curl);
				//_pre($error_msg);
			}

            curl_close($curl);

            $data = array();
            // _pre($data->Message);
            if (!empty($response)) {
                $data1 = json_decode($response);
                if (isset($data1->Message)) {
                    $data['message'] ="Something went wrong";
                }else{
                    $update_data['updated_at'] = date('Y-m-d H:i:s');
                    $update_data['shipstation'] =  json_encode($response);
                    $id = $input['id'];
                    $this->db->update('booking_member',$update_data, array('id' => $id));
                    $data['message'] ="success"; 
                }
            }else{
                $data['message'] ="Something went wrong";
            }
        }
            // _pre($response);

        print json_encode(array("status"=>true,"data"=>$data));
    }

    public function save_shipstation_order(){
        $input = $this->input->post();
        //print_r($input);
        $update_data['updated_at'] = date('Y-m-d H:i:s');
        $update_data['qr_code'] = $input['barcode_number'];
        $id = $input['id'];
        $this->db->update('booking_member',$update_data, array('id' => $id));

        $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price ,users.full_name as name,users.email as email,users.phone as phone, booking.tax_price,booking.report_price,booking_member.id as booking_member_id,booking_member.qr_code,booking.payment_type,booking_member.report_id');
        $this->db->from('booking');
        $this->db->join('booking_member','booking.id = booking_member.booking_id');
        // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
        $this->db->join('users','booking_member.user_id = users.id');
        $this->db->where('booking.type','domestic');
        $this->db->where('booking_member.type','home');
        $this->db->where('booking_member.id',$id);
        $query = $this->db->get();
        $data['booking'] = $query->row_array();
        
        if (!empty($data['booking'])) {
            $reports = (array)$this->db->get_where('reports',array('id' => $data['booking']['report_id']))->row();
            if (!empty($reports)) {$data['booking']['report_name'] = $reports['name']; $data['booking']['SKU'] = $reports['SKU'];  }else{ $data['booking']['report_name'] = ""; $data['booking']['SKU'] = "";  }
       
            if ($data['booking']['reference_member_id'] != "") {
                $booking_refernce_id = $data['booking']['reference_member_id'];
            }else{
                $booking_refernce_id = $data['booking']['booking_no'];
            }

            $booking_no = $data['booking']['booking_no'];
            $report_name = $data['booking']['report_name'];
            $SKU = $data['booking']['SKU'];
            $shippingpostal_code = $input['postcode'];
            $shipping_date = date('Y-m-d',strtotime($input['shipping_date']));
            $payment_type = $data['booking']['payment_type'];
            $shipname = $input['name'];
            $email = $input['email'];
            $phone = $input['phone'];
            $updated_at = $data['booking']['updated_at'];
            $qr_code = $input['barcode_number'];
            $randItemKey = $this->rand_char();
            $address_line1 = $input['address_line1'];
            $address_line2 = $input['address_line2'];
            $address_line3 = $input['address_line3'];
            $shipping_city = $input['city'];
            $shipping_state = $input['state'];
            $weight = $input['weight'];
            $service_code = $input['service_code'];
            $package_code = $input['package_code'];
            $lenght = $input['lenght'];
            $width = $input['width'];
            $height = $input['height'];
          
            $insertKey = '{"orderNumber":"'.$booking_refernce_id.'","orderDate":"'.$updated_at.'","paymentDate":"'.$updated_at.'","shipByDate":"'.$shipping_date.'","carrierCode":"apicode-dpd-local", "packageCode":"'.$package_code.'","serviceCode":"'.$service_code.'","orderStatus":"awaiting_shipment","customerUsername":"'.$shipname.'","customerId":"222223","customerEmail":"'.$email.'","billTo":{"name":"'.$shipname.'","company":"","street1":"'.$address_line1.'","street2":"'.$address_line2.'","street3":"'.$address_line3.'","city":"'.$shipping_city.'","state":"'.$shipping_state.'", "postalCode":"'.$shippingpostal_code.'","country":"GB","phone":"'.$phone.'"},"shipTo":{"name":"'.$shipname.'","company":"","street1":"'.$address_line1.'","street2":"'.$address_line2.'","street3":"'.$address_line3.'","city":"'.$shipping_city.'","state":"'.$shipping_state.'", "postalCode":"'.$shippingpostal_code.'","country":"GB","phone":"'.$phone.'"}, "items":[{"lineItemKey":"'.$randItemKey.'","sku":"'.$SKU.'","name":"'.$report_name.'","weight":{"value":'.$weight.',"units":"ounces"},"quantity":1,"productId":5089702}],"customerNotes":"", "internalNotes":"","paymentMethod":"'.$payment_type.'", "shipDate":"'.$shipping_date.'","weight":{"value":'.$weight.',"units":"grams"}, "dimensions": { "units": "centimeters", "length": '.$lenght.', "width": '.$width.', "height": '.$height.' }, "advancedOptions":{"storeId":38870,"source":"Webstore"}}';

            
            $curl = curl_init();
            $api_key = SHIP_API_KEY;
            $secret_key = SHIP_SECRET_KEY;
            $username =  base64_encode($api_key.":".$secret_key);            

            curl_setopt_array($curl, array(
                CURLOPT_URL => SHIP_URL."orders/createorder",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>$insertKey,
                CURLOPT_HTTPHEADER => array(
                    "Host: ssapi.shipstation.com",
                    "Authorization: Basic ".$username,
                    "Content-Type: application/json"

                ),
            ));
            $response = curl_exec($curl);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
                //_pre($error_msg);
            }

            curl_close($curl);

            $data = array();
            // _pre($data->Message);
            if (!empty($response)) {
                $data1 = json_decode($response);
                if (isset($data1->Message)) {
                    $data['message'] ="Something went wrong";
                }else{
                    $update_data['updated_at'] = date('Y-m-d H:i:s');
                    $update_data['shipstation'] =  json_encode($response);
                    $id = $input['id'];
                    $this->db->update('booking_member',$update_data, array('id' => $id));
                    $data['message'] ="success"; 
                }
            }else{
                $data['message'] ="Something went wrong";
            }
        }
            // _pre($response);

        print json_encode(array("status"=>true,"data"=>$data));
    }

	public function carriers(){
        $curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
		$data['result'] = array();
        if (!empty($response)) {
            $data['result'] = (json_decode($response)); 
        }

        $data['title'] = "Carriers";
        $data['main_title'] = "Carriers";
        $this->renderNetwork('shipstation/carriers/index', $data);
    }

	public function carriers_detail($carrier_code){
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers/getcarrier?carrierCode=$carrier_code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $carrier_detail_response = curl_exec($curl);
		$carrier_detail_response = json_decode($carrier_detail_response);

		curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers/listpackages?carrierCode=$carrier_code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
		$package_list_response = curl_exec($curl);
		$package_list_response = json_decode($package_list_response);

		curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers/listservices?carrierCode=$carrier_code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
		$service_list_response = curl_exec($curl);
		$service_list_response = json_decode($service_list_response);
		
		$data['carrier_detail'] = $carrier_detail_response;
		$data['package_list'] = $package_list_response;
		$data['service_list'] = $service_list_response;
        curl_close($curl);
		$data['title'] = "Carriers Detail";
        $data['main_title'] = "Carriers Detail";
        $this->renderNetwork('shipstation/carriers/details', $data);
	}

	public function product_list(){
		$request = $this->input->get();
		$page = 1;
		$pageSize = 20;
		if(isset($request['page']) && !empty($request['page'])){
			$page = $request['page'];
		}
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."products?pageSize=$pageSize&page=$page",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
		$response = json_decode($response);
		$data['result'] = $response;
        curl_close($curl);
		$data['title'] = "Products";
        $data['main_title'] = "Products";
        $this->renderNetwork('shipstation/products/index', $data);
	}
	
	public function product_detail($product_id){
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."products/$product_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
		$response = json_decode($response);
		//_pre($response);
		$data['result'] = $response;
        curl_close($curl);
		$data['title'] = "Product Detail";
        $data['main_title'] = "Product Detail";
        $this->renderNetwork('shipstation/products/details', $data);
	}

	public function store_list(){
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."stores",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
		$response = json_decode($response);
		$data['result'] = $response;
        curl_close($curl);
		$data['title'] = "Stores";
        $data['main_title'] = "Stores";
        $this->renderNetwork('shipstation/stores/index', $data);
	}

	public function store_detail($store_id){
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."stores/$store_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
		$response = json_decode($response);
		$data['result'] = $response;
        curl_close($curl);
		$data['title'] = "Stores";
        $data['main_title'] = "Stores";
        $this->renderNetwork('shipstation/stores/details', $data);
	}
	
	public function shipments_list(){
		$request = $this->input->get();
		$page = 1;
		$pageSize = 20;
		if(isset($request['page']) && !empty($request['page'])){
			$page = $request['page'];
		}
		$curl = curl_init(); 
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."shipments?pageSize=$pageSize&page=$page",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $response = curl_exec($curl);
		$response = json_decode($response);
		$data['result'] = $response;
        curl_close($curl);
		$data['title'] = "Shipments";
        $data['main_title'] = "Shipments";
        $this->renderNetwork('shipstation/shipments/index', $data);
	}

	public function create_label_for_order($order_id){
		
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
		/**
		 * Get Order Detail Start
		 */
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
			CURLOPT_URL => SHIP_URL."orders/$order_id",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Host: ssapi.shipstation.com",
				"Authorization: Basic ".$username

			),
	 	));
	 	$order_detail_response = curl_exec($curl);
	 	curl_close($curl);
		/**
		 * Get Order Detail End
		*/
		$order_data = json_decode($order_detail_response);
		$package_code = $order_data->packageCode;
		//_pre($order_data);
		/**
		 * Update Order Start
		 */
		// $create_order_data['orderId'] = $order_data->orderId;
		// $create_order_data['orderNumber'] = $order_data->orderNumber;
		// $create_order_data['orderKey'] = $order_data->orderKey;
		// $create_order_data['orderDate'] = $order_data->orderDate;
		// $create_order_data['orderStatus'] = "shipped";
		// $create_order_data['billTo'] = $order_data->billTo;
		// $create_order_data['shipTo'] = $order_data->shipTo;

		// $create_order_post_data = json_encode($create_order_data);
		// $curl1 = curl_init(); 
		// curl_setopt_array($curl1, array(
		// 	// CURLOPT_URL => SHIP_URL."orders/createorder",
		// 	CURLOPT_RETURNTRANSFER => true,
		// 	CURLOPT_ENCODING => "",
		// 	CURLOPT_MAXREDIRS => 10,
		// 	CURLOPT_TIMEOUT => 0,
		// 	CURLOPT_FOLLOWLOCATION => true,
		// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		// 	CURLOPT_CUSTOMREQUEST => "POST",
		// 	CURLOPT_POSTFIELDS =>$create_order_post_data,
		// 	CURLOPT_HTTPHEADER => array(
		// 		"Host: ssapi.shipstation.com",
		// 		"Authorization: Basic ".$username,
		// 		"Content-Type: application/json" 
		// 	),
		// ));
  //       $response = curl_exec($curl1);
		// //_pre(json_decode($response));
  //       curl_close($curl1);
		/**
		 * Update Order End
		 */

		/**
		  * Print order label start
		*/
		$create_label_data['orderId'] = $order_data->orderId;
		$create_label_data['carrierCode'] = $order_data->carrierCode;
		$create_label_data['serviceCode'] = $order_data->serviceCode;
		$create_label_data['confirmation'] = "signature";
		$create_label_data['shipDate'] = date('Y-m-d');
        $create_label_data['weight'] = array("value"=>500,"units"=>"grams","WeightUnits"=>500);
        $create_label_data['dimensions'] = array("length"=>10,"width"=>10,"height"=>10,"units"=>"centimeters");
		$create_label_data['testLabel'] = false;

		$post_data = json_encode($create_label_data);
		$curl1 = curl_init(); 
		curl_setopt_array($curl1, array(
			CURLOPT_URL => SHIP_URL."orders/createlabelfororder",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS =>$post_data,
			CURLOPT_HTTPHEADER => array(
				"Host: ssapi.shipstation.com",
				"Authorization: Basic ".$username,
				"Content-Type: application/json" 
			),
		));
        $response = curl_exec($curl1);
        curl_close($curl1);
		/**
		  * Print order label end
		*/

		$label_data = json_decode($response);
		// _pre($label_data);
		if(isset($label_data->labelData) && !empty($label_data->labelData)){
			$trackingNumber = $label_data->trackingNumber;
			$decoded = base64_decode($label_data->labelData);
			$file = 'assets/pdf/Label-'.$order_data->orderNumber.'.pdf';
            //$base_path = $_SERVER['DOCUMENT_ROOT'];
			file_put_contents($file, $decoded);

			$trackings = new AfterShip\Trackings($this->config->item("after_ship_api_key"));
			$tracking_info = [
				'slug'    => 'interlink-express',
                 "language"=> "en",
                 "order_id"=> "ID ".$order_data->orderNumber,
                 "order_promised_delivery_date"=> date('Y-m-d'),
                 "custom_fields" => [
                    "product_name"=> "PCR Standard",
                    "product_price"=> "£0.00"
                ],
				// 'tracking_number'   => $trackingNumber,
			];
			$response = $trackings->create($trackingNumber, $tracking_info);
			//$data['tracking_data'] = $response;

            $url = "assets/pdf/Label-".$order_data->orderNumber.".pdf";
            $folder = "assets/pdf/";
            s3Upload($file, $folder);
            $pdf_url = S3_URL.$url;

            $update_data['tracking_number'] = $trackingNumber;
            $update_data['shipstation_label'] = $pdf_url;
            $update_data['updated_at'] = date('Y-m-d H:i:s');
			

            // $booking_data = (array)$this->db->get_where('booking',array('booking_no' => $order_data->orderNumber))->row();
            $update = $this->db->update('booking_member',$update_data, array('reference_member_id' => $order_data->orderNumber));
            // $update = $this->db->update('booking_member',$update_data, array('booking_no' => $order_data->orderNumber));
            // _pre($response);
			redirect(base_url('network/shipStation/'));
            //echo '<script>window.open("'. $decoded.'","_blank")</script>';
			//header("Content-type: application/pdf");
			/* if (file_exists($file)) {
				header('Content-Description: File Transfer');
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment; filename="'.basename($file).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file));
				readfile($file);
				unlink($file);
				exit;
			} */
		}else{
			//_pre($label_data);
			$this->session->set_flashdata('error', $label_data->ExceptionMessage);
            // echo '<script>window.open("'. $decoded.'","_blank")</script>';
			redirect(base_url('network/shipStation/shipAwaiting'));
		}
	}

    public function create_order_label(){

        $input = $this->input->post();
        
        $order_id = $input['orderId'];
        $order_key = $input['order_key'];
        
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);

        /**
         * Get Order Detail Start
         */
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."orders/$order_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $order_detail_response = curl_exec($curl);
        curl_close($curl);
        
        /**
         * Get Order Detail End
        */
        $order_data = json_decode($order_detail_response);
        //print_r($order_data); die;
        $package_code = $order_data->packageCode;
       
        
        /**
         * Update Order Start
         */
    
        $shippingpostal_code = $input['postcode'];
        $shipping_date = date('Y-m-d',strtotime($input['shipping_date']));
        $order_date = date('Y-m-d',strtotime($input['order_date']));
        $shipname = $input['name'];
        $email = $input['email'];
        $phone = $input['phone'];
        $address_line1 = $input['address_line1'];
        $address_line2 = $input['address_line2'];
        $address_line3 = $input['address_line3'];
        $shipping_city = $input['city'];
        $shipping_state = $input['state'];
        $weight = $input['weight'];
        $service_code = $input['service_code'];
        $package_code = $input['package_code'];
        $lenght = $input['lenght'];
        $width = $input['width'];
        $height = $input['height'];
        $orderNumber = $input['orderNumber'];
        $randItemKey = $order_data->items[0]->lineItemKey;
        $SKU = $order_data->items[0]->sku;
        $report_name = $order_data->items[0]->name;

        $insertKey = '{"orderId":'.$order_id.', "orderNumber":"'.$orderNumber.'","orderDate":"'.$order_date.'","shipByDate":"'.$shipping_date.'","carrierCode":"apicode-dpd-local", "packageCode":"'.$package_code.'","serviceCode":"'.$service_code.'","orderStatus":"awaiting_shipment","customerUsername":"'.$shipname.'","customerId":"222223","customerEmail":"'.$email.'","billTo":{"name":"'.$shipname.'","company":"","street1":"'.$address_line1.'","street2":"'.$address_line2.'","street3":"'.$address_line3.'","city":"'.$shipping_city.'","state":"'.$shipping_state.'", "postalCode":"'.$shippingpostal_code.'","country":"GB","phone":"'.$phone.'"}, "items":[{"lineItemKey":"'.$randItemKey.'","sku":"'.$SKU.'","name":"'.$report_name.'","weight":{"value":'.$weight.',"units":"ounces"},"quantity":1,"productId":5089702}],"shipTo":{"name":"'.$shipname.'","company":"","street1":"'.$address_line1.'","street2":"'.$address_line2.'","street3":"'.$address_line3.'","city":"'.$shipping_city.'","state":"'.$shipping_state.'", "postalCode":"'.$shippingpostal_code.'","country":"GB","phone":"'.$phone.'"} ,"customerNotes":"", "internalNotes":"", "shipDate":"'.$shipping_date.'","weight":{"value":'.$weight.',"units":"grams"}, "dimensions": { "units": "centimeters", "length": '.$lenght.', "width": '.$width.', "height": '.$height.' }, "advancedOptions":{"storeId":38870,"source":"Webstore"}}';

         //print_r(json_decode($insertKey)); die;
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."orders/createorder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$insertKey,
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username,
                "Content-Type: application/json"

            ),
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            $error_msg = curl_error($curl);
            //_pre($error_msg);
        }

        curl_close($curl);
        //print_r($response); die;
        /**
         * Update Order End
         */

         

        /**
          * Print order label start
        */
        $create_label_data['orderId'] = $order_data->orderId;
        $create_label_data['carrierCode'] = $order_data->carrierCode;
        $create_label_data['serviceCode'] = $order_data->serviceCode;
        $create_label_data['confirmation'] = "signature";
        $create_label_data['shipDate'] = date('Y-m-d',strtotime($input['shipping_date']));
        $create_label_data['weight'] = array("value"=>$input['weight'],"units"=>"grams","WeightUnits"=>$input['weight']);
        $create_label_data['dimensions'] = array("length"=>$input['lenght'],"width"=>$input['width'],"height"=>$input['height'],"units"=>"centimeters");
        $create_label_data['testLabel'] = false;

        $post_data = json_encode($create_label_data);
        $curl1 = curl_init(); 
        curl_setopt_array($curl1, array(
            CURLOPT_URL => SHIP_URL."orders/createlabelfororder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$post_data,
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username,
                "Content-Type: application/json" 
            ),
        ));
        $response = curl_exec($curl1);
        curl_close($curl1);
        /**
          * Print order label end
        */

        $label_data = json_decode($response);
        //_pre($label_data);
        if(isset($label_data->labelData) && !empty($label_data->labelData)){
            $trackingNumber = $label_data->trackingNumber;
            $decoded = base64_decode($label_data->labelData);
            $file = 'assets/pdf/Label-'.$order_data->orderNumber.'.pdf';
            //$base_path = $_SERVER['DOCUMENT_ROOT'];
            file_put_contents($file, $decoded);

            $trackings = new AfterShip\Trackings($this->config->item("after_ship_api_key"));
            $tracking_info = [
                'slug'    => 'interlink-express',
                 "language"=> "en",
                 "order_id"=> "ID ".$order_data->orderNumber,
                 "order_promised_delivery_date"=> date('Y-m-d'),
                 "custom_fields" => [
                    "product_name"=> $report_name,
                    "product_price"=> "£0.00"
                ],
            ];
            $response = $trackings->create($trackingNumber, $tracking_info);
            
            $url = "assets/pdf/Label-".$order_data->orderNumber.".pdf";
            $folder = "assets/pdf/";
            s3Upload($file, $folder);
            $pdf_url = S3_URL.$url;

            $update_data['tracking_number'] = $trackingNumber;
            $update_data['shipstation_label'] = $pdf_url;
            $update_data['updated_at'] = date('Y-m-d H:i:s');
            $update = $this->db->update('booking_member',$update_data, array('reference_member_id' => $order_data->orderNumber));
            print json_encode(array("status"=>true,"message"=>"Order mark shipped successfully"));
        }else{
            //_pre($label_data);
            $this->session->set_flashdata('error', $label_data->ExceptionMessage);
            print json_encode(array("status"=>false,"message"=>$label_data->ExceptionMessage));
        }
    }

    public function get_order_detail(){
        $input = $this->input->post();

        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);
        $carrier_code = "apicode-dpd-local";
        $order_id = $input['order_id'];
        /**
         * Get Order Detail Start
         */
        $curl = curl_init(); 
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."orders/$order_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $order_detail_response = curl_exec($curl);
        curl_close($curl);
        /**
         * Get Order Detail End
        */
        $order_data = json_decode($order_detail_response);
        $data['data'] = $order_data;
        //_pre($data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers/listpackages?carrierCode=$carrier_code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $package_list_response = curl_exec($curl);
        $package_list_response = json_decode($package_list_response);

        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."carriers/listservices?carrierCode=$carrier_code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username

            ),
        ));
        $service_list_response = curl_exec($curl);
        $service_list_response = json_decode($service_list_response);
       
        $data['package_list'] = $package_list_response;
        $data['service_list'] = $service_list_response;
        curl_close($curl);

        $html = $this->load->view('network/pages/shipstation/mark_as_shipped_order',$data,true);
        print json_encode(array("html"=>$html));
    }

    public function rand_char() {
        $characters = '0123456789-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomstr = '';
        for ($i = 0; $i < random_int(15, 30); $i++) {
          $randomstr .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomstr;
      }

    public function validation_errors_response() {
        $err_array = array();
        $err_str = "";
        $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
        $err_str = ltrim($err_str, '|');
        $err_str = rtrim($err_str, '|');
        $err_array = explode('|', $err_str);
        $err_array = array_filter($err_array);
        return $err_array;
    }

}
