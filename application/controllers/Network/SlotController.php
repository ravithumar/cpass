<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SlotController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		$this->load->model('Clinic');
        $this->table_name = "slot";
        $this->load->model('User');
        $this->title = "Slot";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $network_id = $this->session->userdata('network')['user_id'];

        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('slot.id as slot_id,hospital.id, hospital.company_name,hospital.company_number,hospital.no_of_employee,hospital.point_of_contact,hospital.company_website, hospital.status,slot.is_deleted', false)
            ->from($this->table_name)
            ->join('hospital', 'hospital.id = slot.user_id')
            ->where('hospital.network_id',$network_id)
            ->where('slot.is_deleted',NULL)
            ->group_by('slot.user_id');

        $action['edit'] = base_url('network/slot/edit/');
        $action['details'] = base_url('network/slot/details/');
        $action['delete'] = base_url('network/slot/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Clinic Name', 'company_name')
            ->column('Company Number', 'company_number')
            ->column('Company Website', 'company_website')
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['edit'] . $id . '" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Edit Field"><i class="la la-pencil-square "></i></a>';

                $option .= '<a href="' . $action['details'] . $id . '" class="on-default text-secondary ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Details Field"><i class="la la-eye "></i></a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="slot"><i class="fa fa-trash"></i></a>';
		        return $option;
		    });

        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('network/slot/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('slot/index', $data);
	}

	public function add(){
        $data['title'] = "Slot Add";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('network/slot'); 
        $network_id = $this->session->userdata('network')['user_id'];

        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['hospital'] = $this->db->get_where('hospital',['network_id' => $network_id])->result_array();
        $request = $this->input->post(); 
        if (isset($_POST) && !empty($_POST)) {
            $request = $this->input->post();
            $slot = $this->db->get_where('slot',array('user_id' => $request['user_id']))->result_array();
            if(!empty($slot))
            {
                $this->session->set_flashdata('success', __('Slot Already Added successfully.'));
                redirect('network/slot');
            }
            else
            {
                $days = $this->db->get_where('days',['status'=>'1'])->result_array();
                $day_name = array_column($days, 'name');
                $slot = [];

                for($i=0; $i< count($days); $i++)
                {
                   $slot[$i]['day'] = $day_name[$i];
                   $slot[$i]['user_id'] = $request['user_id'];
                   $slot[$i]['morning_start'] = '';
                   $slot[$i]['morning_end'] = '';
                   $slot[$i]['morning_status'] = '1';
                   $slot[$i]['afternoon_start'] = '';
                   $slot[$i]['afternoon_end'] = '';
                   $slot[$i]['afternoon_status'] = '1';
                   $slot[$i]['evening_start'] = '';
                   $slot[$i]['evening_end'] = '';
                   $slot[$i]['evening_status'] = '1';

                    if(isset($request[$day_name[$i]]))
                    {
                        $slot[$i]['morning_start'] = date('H:i', strtotime($request[$day_name[$i]][0]));
                        $slot[$i]['morning_end'] = date('H:i', strtotime($request[$day_name[$i]][1]));
                        $slot[$i]['afternoon_start'] = date('H:i', strtotime($request[$day_name[$i]][2]));
                        $slot[$i]['afternoon_end'] = date('H:i', strtotime($request[$day_name[$i]][3]));
                        $slot[$i]['evening_start'] = date('H:i', strtotime($request[$day_name[$i]][4]));
                        $slot[$i]['evening_end'] = date('H:i', strtotime($request[$day_name[$i]][5]));
                    }    
                }

                foreach ($slot as $key => $value) 
                {
                   $clinic['user_id'] = $request['user_id'];
                   $clinic['day'] = substr($value['day'], 0, 3);
                   $clinic['morning_start'] = $value['morning_start'];
                   $clinic['morning_end'] = $value['morning_end']; 
                   $clinic['morning_status'] = $value['morning_status'];
                   $clinic['afternoon_start'] = $value['afternoon_start'];
                   $clinic['afternoon_end'] = $value['afternoon_end'];
                   $clinic['afternoon_status'] = $value['afternoon_status'];
                   $clinic['evening_start'] = $value['evening_start'];
                   $clinic['evening_end'] = $value['evening_end'];
                   $clinic['evening_status'] = $value['evening_status'];
                   $this->db->insert('slot',$clinic);
                }
                
                $this->session->set_flashdata('success', __('Slot Added successfully.'));
                redirect('network/slot');
            }
            
        } else {
            $data['days'] = $this->db->get_where('days',['status'=>'1'])->result_array();
            $this->renderNetwork('slot/add', $data);
        }
    }

    public function edit($id = ''){
        $data['title'] = "Slot Edit";
        $data['main_title'] = $this->title;
        $data['home'] = base_url('network/slot'); 
        $network_id = $this->session->userdata('network')['user_id'];

        $data['reports'] = $this->db->get_where('reports',['status'=>'1'])->result();
        $data['hospital'] = $this->db->get_where('hospital',['network_id' => $network_id])->result_array();
        $request = $this->input->post(); 
        if (isset($_POST) && !empty($_POST)) {
            $request = $this->input->post();
            
            $days = $this->db->get_where('days',['status'=>'1'])->result_array();
            $day_name = array_column($days, 'name');
            $slot = [];
            
            for($i=0; $i< count($days); $i++)
            {
               $slot[$i]['day'] = $day_name[$i];
               $slot[$i]['user_id'] = $request['user_id'];
               $slot[$i]['morning_start'] = '';
               $slot[$i]['morning_end'] = '';
               $slot[$i]['morning_status'] = '1';
               $slot[$i]['afternoon_start'] = '';
               $slot[$i]['afternoon_end'] = '';
               $slot[$i]['afternoon_status'] = '1';
               $slot[$i]['evening_start'] = '';
               $slot[$i]['evening_end'] = '';
               $slot[$i]['evening_status'] = '1';
               $str_day_name = substr($day_name[$i], 0, 3);

                if(isset($request[$str_day_name]))
                {
                    $slot[$i]['morning_start'] = date('H:i', strtotime($request[$str_day_name][0]));
                    $slot[$i]['morning_end'] = date('H:i', strtotime($request[$str_day_name][1]));
                    $slot[$i]['afternoon_start'] = date('H:i', strtotime($request[$str_day_name][2]));
                    $slot[$i]['afternoon_end'] = date('H:i', strtotime($request[$str_day_name][3]));
                    $slot[$i]['evening_start'] = date('H:i', strtotime($request[$str_day_name][4]));
                    $slot[$i]['evening_end'] = date('H:i', strtotime($request[$str_day_name][5]));
                }    
            }
            
            foreach ($slot as $key => $value) 
            {
               $clinic['user_id'] = $request['user_id'];
               $clinic['day'] = substr($value['day'], 0, 3);
               $clinic['morning_start'] = $value['morning_start'];
               $clinic['morning_end'] = $value['morning_end']; 
               $clinic['morning_status'] = $value['morning_status'];
               $clinic['afternoon_start'] = $value['afternoon_start'];
               $clinic['afternoon_end'] = $value['afternoon_end'];
               $clinic['afternoon_status'] = $value['afternoon_status'];
               $clinic['evening_start'] = $value['evening_start'];
               $clinic['evening_end'] = $value['evening_end'];
               $clinic['evening_status'] = $value['evening_status'];
               
               $this->db->update('slot',$clinic,array('user_id' => $request['user_id'], 'day' => $clinic['day']));
            }
            
            $this->session->set_flashdata('success', __('Slot Updated successfully.'));
            redirect('network/slot');

        } else {
            $slot_avail = $this->db->get_where('slot',array('user_id' => $id))->result_array();
            foreach ($slot_avail as $key => $value) 
            {
                $slot_avail[$key]['morning_start'] = date("h:i A", strtotime($value['morning_start']));
                $slot_avail[$key]['morning_end'] = date("h:i A", strtotime($value['morning_end']));
                $slot_avail[$key]['afternoon_start'] = date("h:i A", strtotime($value['afternoon_start']));
                $slot_avail[$key]['afternoon_end'] = date("h:i A", strtotime($value['afternoon_end']));
                $slot_avail[$key]['evening_start'] = date("h:i A", strtotime($value['evening_start']));
                $slot_avail[$key]['evening_end'] = date("h:i A", strtotime($value['evening_end']));
            }
            
            $data['slot_avail'] = $slot_avail;
            $data['user_id'] = $id;
            $data['days'] = $this->db->get_where('days',['status'=>'1'])->result_array();
            $this->renderNetwork('slot/edit', $data);
        }
    }

    public function details($id = ''){
        $data['clinic'] = Clinic::find($id);
        $data['slot'] = $this->db->order_by('day','asc')->get_where('slot', array('user_id' => $id))->result_array();

        $data['home'] = base_url('network/slot');
        $data['title'] = "Slot Details";
        $data['main_title'] = $this->title;
        $this->renderNetwork('slot/details', $data);
        
    }

    public function delete($id)
    {
        $data = $this->db->get_where($this->table_name,array('id'=>$id))->row_array();

         if ($data)
         {
              $update_data['is_deleted'] = '1';

            $this->db->where('id',$id);
            $this->db->update($this->table_name,$update_data);
         }
       

        echo 1;
    }

    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
