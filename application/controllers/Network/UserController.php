<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_network())
		{
			redirect('/auth/login');
		}
		$this->load->model('Clinic');
        $this->table_name = "users";
        $this->load->model('User');
        $this->title = "Users";
        $this->load->model('ion_auth_model'); 
		$this->load->library(['ion_auth', 'form_validation']);

	}
	public function index() {
		$data['title'] = $this->title;
        $this->load->library('Datatables');
        $product = new Datatables;
        $product->select('users.*', false)
            ->from($this->table_name)
            ->join('users_groups', 'users.id = users_groups.user_id')
            ->where('users.is_deleted',NULL)
            ->where('users_groups.group_id',2);
        $action['details'] = base_url('network/user/details/');
        $action['delete'] = base_url('network/user/delete/');
        $product->style(['class' => 'table table-striped table-bordered nowrap'])
            ->column('#', 'id')
            ->column('Name', 'full_name')
            ->column('Email Address', 'email')
            ->column('Phone Number', 'phone')
            ->column('Approve', 'active', function ($status, $row)
	        {
	            if ($status == 1) {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';
	            }else {
	                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';
	            }
	        })
	        ->column('Actions', 'id', function ($id) use ($action)
		    {
		        $option = '<a href="' . $action['details'] . $id . '" class="on-default text-secondary ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Details Field"><i class="la la-eye "></i></a>';

		        $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model ml-2" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="'.current_url().'"  data-table="'.$this->table_name.'" data-href="User"><i class="fa fa-trash"></i></a>';

		        return $option;
		    }); 
        $product->datatable($this->table_name);
        $product->init();
        $data['datatable'] = true;
        $data['export'] = false;
        $data['add_url'] = base_url('network/user/add');        
        $data['title'] = $this->title;
        $data['main_title'] = $this->title;
        $this->renderNetwork('user/index', $data);
	}
	
    public function details($id = ''){
        $data['user'] = User::find($id);
        $data['members'] = $this->db->get_where('users',['parent_id'=>$id])->result();
		$this->db->select('booking.id, booking.booking_no,users.phone,booking.type,booking.total_price, booking.status,booking.booking_status,booking_member.reference_member_id,booking.created_at,booking_member.report_file');
		$this->db->from("booking");
		$this->db->join('booking_member', 'booking_member.booking_id = booking.id');
		$this->db->join('users', 'booking_member.user_id = users.id');
		$this->db->where("booking.user_id",$id);
		$this->db->group_by('booking.id');
		$this->db->order_by('booking.id',"desc");
		$query = $this->db->get();
		$booking_history = $query->result_array();
		$data['booking'] = $booking_history;
        $data['home'] = base_url('network/user');
        $data['title'] = "User Details";
        $data['main_title'] = $this->title;
        $this->renderNetwork('user/details', $data);
    }

    public function delete($id)
    {
        $update_data['is_deleted'] = '1';

        $this->db->where('id',$id);
        $this->db->update($this->table_name,$update_data);

        echo '1';
    }

    public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
