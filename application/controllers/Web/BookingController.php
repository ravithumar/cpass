 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingController extends MY_Controller {
	protected $booking_api;
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
        $this->load->model('User');
		$this->booking_api = base_url().'api/booking/';
		$this->aftership_api = base_url().'api/aftership/track_data';

		$this->user = $this->session->userdata('members');
		if(!check_data("tokens",array("token"=>$this->user['api_key']))){
			redirect(base_url().'auth/logout/members');
		}
	}
	public function index() {
		$user_data = $this->session->userdata('members');
		$purpose_type_url = $this->booking_api.'purpose_type';

		$method = 'POST';
		$api_key = $this->user['api_key'];
		$purpose_type_response = api_call($purpose_type_url,array("user_id"=>$user_data['user_id']), $api_key, $method);
		$purpose_type_response = json_decode($purpose_type_response);
		
		$place_test_url = $this->booking_api.'place_test';
		$place_test_response = api_call($place_test_url,[],$api_key,'GET');
		$place_test_response = json_decode($place_test_response);

		$data['country_list'] = countries_list($api_key);
		$data['exemptioncat_list'] = exemptioncat_list($api_key);
		$data['certified_vaccines_list'] = certified_vaccines_list($api_key);
		//echo date('H',strtotime("+2 Hours")); exit;
		$next_time = date('h',strtotime("+2 Hours"));
		$current_time_array = array();
		for($i=$next_time;$i<=23;$i++){
			array_push($current_time_array,$i);
		}

		//_pre($current_time_array);
		$data['current_time_array'] = $current_time_array;
		$data['purpose_type_response'] = $purpose_type_response;
		$data['place_test_response'] = $place_test_response;
		$data['county'] = $this->db->get_where('county', array('status' => 1))->result_array();

		//_pre(TAX_PRICE);

		webRender('booking/new_booking',$data);
	}

	public function track_data($tracking_no) {
		
		$user_data = $this->session->userdata('members');
		$purpose_type_url = $this->aftership_api;
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$response = api_call($purpose_type_url,array("tracking_no"=>$tracking_no), $api_key, $method);
		$response = json_decode($response);

		//echo date('H',strtotime("+2 Hours")); exit;
		
		//_pre($current_time_array);

		$data['response'] = $response;
		//_pre($data);

		webRender('my_booking/track_report',$data);
	}

	public function get_hours(){
		$input = $this->input->post();
		$current_time_array = array();
		if(date('d-m-Y') == $input['date']){
			$next_time = date('H',strtotime("+2 Hours"));
			for($i=$next_time;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}else{
			for($i=1;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}
		$data['current_time_array'] = $current_time_array;
		print json_encode($data);
		exit();
	}

	public function get_report_test(){
		$request = $this->input->post();
		$user_data = $this->session->userdata('members');
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/reporttype/reportbycountry';
		$input = array(
			'purpose_id' => $request['purpose_id']
		);
		if(isset($request['country_id']) && $request['country_id'] != ''){
			$input['id'] = $request['country_id'];
		}
		if(isset($request['step']) && $request['step'] != ''){
			$input['step'] = $request['step'];
		}
		$response = api_call($url, $input ,$api_key,$method);
		$response = json_decode($response);
		$data = array();
		if($response->status == 1){
			$data = $response->data;
			$this->session->set_userdata('tax_price', $response->tax_price);
		}
		print json_encode($data);
		exit();
	}

	public function edit_detail(){
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$id = $this->user['user_id'];
		$data = [];
		$session_data = $this->session->userdata("input_data");
		$purpose_id = $this->session->userdata('purpose_id');
		//_pre($_SESSION);
		if(!$this->session->userdata('purpose_id')){
			$purpose_id = $session_data['purpose_id'];
		}

		$reportbycountry_data['purpose_id'] = $purpose_id;

		if($purpose_id == '4'){
			$reportbycountry_data['id'] = $this->session->userdata('country_id');
		}

		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$response = api_call($purpose_type_url, $reportbycountry_data, $api_key, $method);
		$response = json_decode($response);
		if($response->status == 1){
			$data['test_types'] = $response->data;
		}else{
			$this->session->set_flashdata('error', 'Something is wrong with the booking data.');
			redirect(base_url('new-booking'));
		}

		$data['purpose_id'] = $this->session->userdata("purpose_id");
		$data['place_type'] = $this->session->userdata("place_type");
		$data['test_date'] = $this->session->userdata("test_date");
		$data['test_time'] = $this->session->userdata("test_time");
		$hospital_name = $this->db->select('company_name')->from('hospital')->where('id',$this->session->userdata('clinic_id'))->get()->row_array();
		 $data['test_place_name'] = $hospital_name['company_name'];
		//_pre($_SESSION);

		if(($data['purpose_id'] == '1' && $data['place_type'] == '1') || ($data['purpose_id'] == '4')){
			$url = base_url().'api/customer/time_slot';
			$slot_date = date('Y-m-d',strtotime($this->session->userdata('test_date')));
			$api_data = array(
				'slot_date' => $slot_date,
				// 'report_type' => $purpose_id,
				'report_type' => $this->session->userdata('report_id'),
				'hospital_id' => $this->session->userdata('clinic_id')
			);

			// _pre($api_data);
			$response = api_call($url, $api_data, $api_key, $method);
			$response = json_decode($response);
			// _pre($response);
			if($response->status == 1){
				$data['slots'] = $response->slots;
			}
		}
		// _pre($data);

		webRender('booking/edit_detail', $data);
	}

	public function edit_detail_save(){
		$request = $_POST;
		// _pre($request);
		$this->session->set_userdata('report_id', $request['test_type']);
		//$this->session->set_userdata('test_type_name', $request['test_type_name']);
		$this->session->set_userdata('test_date', $request['booking_date']);

		if(isset($request['selected_slot']) && $request['selected_slot'] != ''){
			$this->session->set_userdata('test_time', $request['selected_slot']);
		}

		// _pre($this->user['user_id']);
		// echo json_encode(array('status' => TRUE, 'data' => $request));
		// webRender('booking/review_confirm');
		 //redirect(base_url('booking/member/edit/'.$this->user['user_id']));
		redirect(base_url('review-confirm'));
	}

	public function my_booking(){
		webRender('booking/my_booking');
	}

	public function search_old(){
		$data = array();
		$input = $this->input->post();

		$user_data = $this->session->userdata('members');
		$url = base_url().'api/hospital/list';
		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$method = 'POST';
		$api_key = $this->user['api_key'];


		// $purpose_type_response = api_call($purpose_type_url,array("purpose_id"=>$input['purpose_id']), $api_key, $method);
		// $purpose_type_response = json_decode($purpose_type_response);
		// _pre($input);
		if (isset($input['test_type'])) {
			// code...
			$post_data['test_type'] = $input['test_type'];
			$post_data['test_time'] = $input['test_hours'].":".$input['test_minute'];
			$post_data['test_date'] = date('Y-m-d',strtotime($input['test_date']));
			$post_data['postcode'] = $input['postcode'];
			$post_data['search_key'] = '';
			$post_data['search_value'] = '';	

			$response = api_call($url,$post_data,$api_key,$method);
			$response = json_decode($response, true); 

			$data['list'] = [];
			$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
			$purpose_type = $this->config->item("purpose_type");

			if($response['status'] == 1 && !empty($response['data']))
			{
				$p_data['place_test'] = $input['place_test'];
				$p_data['purpose_id'] = $input['purpose_id'];

				$data['list'] = $response['data'];
				$data['post'] = $post_data;
				$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
				$data['purpose_type_name'] = $purpose_type_name['name'];
				$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
				$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
				$data['pur_test_place'] = '';
				$purpose_place_name = $this->config->item("place_test");
				foreach ($purpose_place_name as $key => $value) {
					if($value['id'] == $p_data['place_test'])
					{
						$data['pur_test_place'] = $value['name'];
					}
				}
				$data['pur_type_place'] = '';
				$purpose_place_type = $this->config->item("purpose_type");
				foreach ($purpose_place_type as $key => $value) {
					if($value['id'] == $p_data['purpose_id'])
					{
						$data['pur_type_place'] = $value['name'];
					}
				}
			}	
		}else{
			$data['message'] = "No hospital found.";
		}
		
		// $post_data['test_type'] = '3';
		// $post_data['test_date'] = '2021-09-04';
		// $post_data['test_time'] = '18:00';
		// $post_data['postcode'] = 'GU167HF';
		// $post_data['search_key'] = '';
		// $post_data['search_value'] = '';	

		// _pre($data);
		webRender('booking/search',$data);
	}
	
	public function search(){
		$data = array();
		// _pre($this->session->userdata("purpose_id"));
		// _pre($_SESSION);
		
		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$method = 'POST';
		$api_key = $this->user['api_key'];
		// code...

		$post_data['test_type'] = $this->session->userdata('report_id');
		$purpose_id = $this->session->userdata("purpose_id");

		if(isset($_REQUEST['distance_filter']) && !empty($_REQUEST['distance_filter'])){
			$distance_filter = explode(',',$_REQUEST['distance_filter']);
			$post_data['min_distance'] = $distance_filter[0];
			$post_data['max_distance'] = $distance_filter[1];
		}
		
		if(isset($_REQUEST['price_filter']) && !empty($_REQUEST['price_filter'])){
			$price_filter = explode(',',$_REQUEST['price_filter']);
			//$post_data['search_key'] = "price";
			$post_data['min_price'] = $price_filter[0];
			$post_data['max_price'] = $price_filter[1];
		}

		if ($purpose_id == "5") {
			// _pre($_SESSION);
			$url = base_url().'api/hospital/international_list';

			if ($this->session->userdata('shipping_method') == "2") {
				$post_data['date'] = $this->session->userdata('shipping_date');
			}else{
				$post_data['date'] = $this->session->userdata('pickup_date');
			}

			$test_date_obj = DateTime::createFromFormat('d-m-Y', $post_data['date']);
			$post_data['date'] = $test_date_obj->format('Y-m-d');
		}else{
			
			$url = base_url().'api/hospital/list';
			if($this->session->userdata('test_time')){
				$post_data['test_time'] = $this->session->userdata('test_time');
			}

			if($this->session->userdata('test_date')){
				$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
				$post_data['test_date'] = $test_date_obj->format('Y-m-d');
			}
		}
		
		
		if(isset($_REQUEST['postcode']) && !empty($_REQUEST['postcode'])){
			//$post_data['postcode'] = $_REQUEST['postcode'];
			$data['postcode'] = $_REQUEST['postcode'];
			$post_data['search_key'] = 'postcode';
			$post_data['search_value'] = $_REQUEST['postcode'];
		}
		if(isset($_REQUEST['regionId']) && !empty($_REQUEST['regionId'])){
			$post_data['search_key'] = "region";
			$post_data['search_value'] = $_REQUEST['regionId'];
			$data['regionId'] = $_REQUEST['regionId'];
		}

		$post_data['purpose_id'] = $purpose_id;
		//echo $url;
		//_pre($post_data);
		/*$post_data['test_type'] = '3';
		$post_data['test_date'] = '2021-09-04';
		$post_data['test_time'] = '18:00';
		$post_data['postcode'] = 'GU167HF';
		$post_data['search_key'] = '';
		$post_data['search_value'] = '';	*/

		$response = api_call($url,$post_data,$api_key,$method);
		$response = json_decode($response, true); 
		//_pre($response);
		$data['list'] = [];
		$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
		$purpose_type = $this->config->item("purpose_type");

		//_pre($post_data);
		if($response['status'] == 1 && !empty($response['data']))
		{
			/*$p_data['place_test'] = $input['place_test'];
			$p_data['purpose_id'] = $input['purpose_id'];*/

			$data['list'] = $response['data'];
			/*$data['post'] = $post_data;
			$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
			$data['purpose_type_name'] = $purpose_type_name['name'];
			$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
			$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
			$data['pur_test_place'] = '';
			$purpose_place_name = $this->config->item("place_test");
			foreach ($purpose_place_name as $key => $value) {
				if($value['id'] == $p_data['place_test'])
				{
					$data['pur_test_place'] = $value['name'];
				}
			}
			$data['pur_type_place'] = '';
			$purpose_place_type = $this->config->item("purpose_type");
			foreach ($purpose_place_type as $key => $value) {
				if($value['id'] == $p_data['purpose_id'])
				{
					$data['pur_type_place'] = $value['name'];
				}
			}*/
		}	

		/**
		 * Filter API Call Start
		 */
		$filter_url = $url = base_url().'api/hospital/filter';
		$method = 'GET';
		$api_key = $this->user['api_key'];
		$filter_response = api_call($filter_url,null,$api_key,$method);
		$filter_response = json_decode($filter_response, true);
		$filter_array = array();
		if($filter_response['status'] == 1){
			$filter_array = $filter_response['data'];
		}
		$data['filter_data'] = $filter_array;
		/**
		 * Filter API Call End
		 */
		
		webRender('booking/search',$data);
	}

	public function roundtrip_search(){
		$input = $this->input->post();
		$user_data = $this->session->userdata('members');
		$url = base_url().'api/hospital/list';
		$method = 'POST';
		$api_key = $this->user['api_key'];

		if(isset($input) && !empty($input)){
		
			if(isset($input['is_filter']) && $input['is_filter'] == "yes"){
				$post_data = $this->session->userdata("search_data");
				$input_data = $this->session->userdata("input_data");
				if(isset($input['distance_filter']) && !empty($input['distance_filter'])){
					$distance_array = explode(',',$input['distance_filter']);
					$post_data['min_distance'] = $distance_array[0];
					$post_data['max_distance'] = $distance_array[1];
				}
				if(isset($input['price_filter']) && !empty($input['price_filter'])){
					$price_array = explode(',',$input['price_filter']);
					$post_data['min_price'] = $price_array[0];
					$post_data['max_price'] = $price_array[1];
				}
				$this->session->set_userdata("search_data",$post_data);
				
			}else{
				$this->session->set_userdata('purpose_of_test_name', $input['purpose_of_test_name']);
				$post_data['test_type'] = $input['test_type'];
				$post_data['test_date'] = date('Y-m-d',strtotime($input['booking_date']));
				if($input['purpose_id'] == 3){
					$post_data['test_time'] = date('H:i',strtotime($input['booking_time']));
				}else{
					$post_data['test_time'] = $input['test_hours'].":".$input['test_minute'];
				}
				if(!empty($input['postcode'])){
					$post_data['postcode'] = $input['postcode'];
					$post_data['search_key'] = 'postcode';
					$post_data['search_value'] = $input['postcode'];	
				}else if(!empty($input['regionId'])){
					$post_data['search_key'] = 'region ';
					$post_data['search_value'] = $input['regionId'];	
				}
				$this->session->set_userdata("search_data",$post_data);
				$this->session->set_userdata("input_data",$input);
				$input_data = $input;
			}
		}else{
			$post_data = $this->session->userdata("search_data");
			$input_data = $this->session->userdata("input_data");
		}
		$post_data['purpose_id'] = 3;
		
		$data['search_data'] = $input_data;
		
		$response = api_call($url,$post_data,$api_key,$method);
		$response = json_decode($response, true);

		$data['list'] = [];
		$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
		$purpose_type = $this->config->item("purpose_type");

		if($response['status'] == 1 && !empty($response['data']))
		{
			if(isset($input_data['place_test']) && !empty($input_data['place_test'])){
				$p_data['place_test'] = $input_data['place_test'];
			}else{
				$p_data['place_test'] = "";
			}

			$p_data['purpose_id'] = $input_data['purpose_id'];

			$data['list'] = $response['data'];
			
			$data['post'] = $post_data;
			$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
			$data['purpose_type_name'] = $purpose_type_name['name'];
			$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
			$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
			$data['pur_test_place'] = '';
			$data['country_name'] = '';
			if(isset($input_data['country_id']) && !empty($input_data['country_id'])){
				$data['country_name'] = get_column_value("country",array("id"=>$input_data['country_id']),"name");
			}
			$purpose_place_name = $this->config->item("place_test");
			foreach ($purpose_place_name as $key => $value) {
				if($value['id'] == $p_data['place_test'])
				{
					$data['pur_test_place'] = $value['name'];
				}
			}
			$data['pur_type_place'] = '';
			$purpose_place_type = $this->config->item("purpose_type");
			foreach ($purpose_place_type as $key => $value) {
				if($value['id'] == $p_data['purpose_id'])
				{
					$data['pur_type_place'] = $value['name'];
				}
			}
		}

		/**
		 * Filter API Call Start
		 */
		$filter_url = $url = base_url().'api/hospital/filter';
		$method = 'GET';
		$api_key = $this->user['api_key'];
		$filter_response = api_call($filter_url,null,$api_key,$method);
		$filter_response = json_decode($filter_response, true);
		$filter_array = array();
		if($filter_response['status'] == 1){
			$filter_array = $filter_response['data'];
		}
		$data['filter_data'] = $filter_array;
		/**
		 * Filter API Call End
		 */
		
		if($input_data['purpose_id'] == 3){
			webRender('fit_to_fly/search',$data, 'refresh');
		}else{
			webRender('booking/search',$data, 'refresh');
		}
	}

	public function fit_to_fly_return(){
		//_pre($_SESSION); exit();
		$request = $this->input->post();
		if(isset($request) && !empty($request)){
		
			$session_data = $this->session->userdata("input_data");
			$session_data['clinic_id'] = $request['clinic_id'];
			$session_data['clinic_price'] = $request['clinic_price'];
			$session_data['country_name'] = $request['country_name'];
			$session_data['test_type_name'] = $request['test_type_name'];
			$session_data['test_place_name'] = $request['test_place_name'];
			$this->session->set_userdata("input_data",$session_data);
			$search_data = $this->session->userdata("input_data");
		}else{
			$search_data = $this->session->userdata("input_data");
		}
		
		$api_key = $this->user['api_key'];
		$method = 'POST';
		$data['country_list'] = countries_list($api_key);
		// _pre($search_data); exit();
		
		$data['search_data'] = $search_data;

		webRender('fit_to_fly/step_02',$data);
	}

	public function save_fit_to_fly_return(){
		$session_data = $this->session->userdata("input_data");
		$request = $this->input->post();
		if(isset($request) && !empty($request)){
			$session_data['fit_to_fly_input_data'] = $request;
			$this->session->set_userdata("input_data",$session_data);
		}
		$session_data = $this->session->userdata("input_data");
		// _pre($session_data);
		redirect(base_url().'fit-to-fly-arrival');
	}

	public function fit_to_fly_arrival(){
		webRender('fit_to_fly/step_03');
	}

	public function complete_your_registration(){

		//_pre($_SESSION); 
		$api_key = $this->user['api_key'];
		$data['ethnicity_list'] = ethnicity($api_key);
		webRender('booking/complete_your_registration', $data);
	}

	public function select_members(){

		//_pre($_SESSION);
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$session_data = $this->session->userdata("input_data");
		$purpose_id = $this->session->userdata('purpose_id');
		if(!$this->session->userdata('purpose_id')){
			$purpose_id = $session_data['purpose_id'];
		}

		if(!empty($purpose_id)){
			if($purpose_id == '1'){
				$url = base_url().'api/customer/members_list';
			}else if($purpose_id == '4'){
				$url = base_url().'api/customer/members_list_fitoneway';
			}else if($purpose_id == '5'){
				$url = base_url().'api/booking/members_list_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/members_list_fitroundtrip';
			}else{
				$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
				redirect(base_url('new-booking'));
			}
			
			$id = $this->user['user_id'];
			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);
			$data['purpose_id'] = $purpose_id;
			$data['members'] = array();

			foreach ($response->data as $birth_date)
			{
				$date = $birth_date->date_of_birth;
			}

			$num = count($response->data);

			if ($num == 1)
			{
				if (empty($date))
				{
					redirect('scan-documents');
				}
			}

			if($response->status == 1){
				$data['members'] = $response->data;
			}else{
				redirect(base_url('members/add'), 'refresh');
			}
			// _pre($data);

			webRender('booking/member_list2', $data);

		}else{
			$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
			redirect(base_url('new-booking'));
		}

	}

	public function scan_documents(){
		$url = base_url()."/api/jumio/create_account";
		$api_key = $this->user['api_key'];
		$id = $this->user['user_id'];
		$method = 'POST';
		$response = api_call($url, array("user_id"=>$id) ,$api_key,$method);
		$response = json_decode($response);
		if($response->status == 1){
			$data['access_token'] = $response->access_token;
			$data['sdk_token'] = $response->sdk_token;
			$data['web_href'] = $response->web_href;
			$data['account_id'] = $response->account_id;
			$data['workflow_id'] = $response->workflow_id;
			webRender('members/scan_documents', $data);
		}else{
			redirect(base_url().'new-booking');
		}
	}

	public function get_jumio_status(){
		$request = $this->input->post();
		$url = base_url()."/api/jumio/jumio_status";
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$post_data['account_id'] = $request['account_id'];
		$post_data['workflow_id'] = $request['workflow_id'];
		$post_data['token'] = $request['access_token'];

		$response = api_call($url, $post_data ,$api_key,$method);
		$response = json_decode($response);
		
		$status = false;
		$status_label = "";
		$response_data = [];
		$decision_type = "NOT_EXECUTED";
		$is_open_modal = false;
		$is_redirect_page = false;
		$is_loader = false;
		if(isset($response->workflowExecution) && !empty($response->workflowExecution)){
			$workflow_status = $response->workflowExecution->status;
			if($workflow_status == "PROCESSED"){
				$is_loader = true;
				$status = true;
				$surl = base_url().'api/jumio/get_workflow_details';
				$method = 'POST';

				$data['token'] = $post_data['token'];
				$data['account_id'] = $post_data['account_id'];
				$data['workflow_id'] = $post_data['workflow_id'];
				
				$workflow_response = api_call($surl, $data ,$api_key,$method);
				$workflow_response = json_decode($workflow_response);
				if($workflow_response->status == TRUE){
					$decision_type = $workflow_response->data->decision_type;
					$response_data['decision_type'] = $workflow_response->data->decision_type;
					$response_data['issuingCountry'] = isset($workflow_response->data->issuingCountry)?$workflow_response->data->issuingCountry:'';
					$response_data['fullName'] = isset($workflow_response->data->firstName)?$workflow_response->data->firstName.' '.$workflow_response->data->lastName:'';
					$response_data['dateOfBirth'] = isset($workflow_response->data->dateOfBirth)?$workflow_response->data->dateOfBirth:'';
					$response_data['documentNumber'] = isset($workflow_response->data->documentNumber)?$workflow_response->data->documentNumber:'';
					$this->session->set_userdata("jumio_member_data",$response_data);
				}
				if($decision_type != "PASSED"){
					$is_open_modal = true;
				}else{
					$is_redirect_page = true;
				}
			}else{
				if($workflow_status == "INITIATED" || $workflow_status == "ACQUIRED"){
					$status = true;
				}
			}
			$status_label = $workflow_status;
		}
		print json_encode(array("status"=>$status,"is_open_modal"=>$is_open_modal,"is_redirect_page"=>$is_redirect_page,"is_loader"=>$is_loader));
	}

	public function add_booking_member_old($id){

		$id = $this->user['user_id'];

		if(!isset($_POST) || empty($_POST)){
			redirect(base_url('select-members'), 'refresh');
		}

		$session_data = $this->session->userdata("input_data");
		
		$purpose_id = $this->session->userdata('purpose_id');
		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}


		if(!empty($purpose_id)){
			$place_type = $this->session->userdata('place_type');

			$request = $_POST;
	
			$data['full_name'] = $request['full_name'];
			$data['email'] = $request['email'];
			$data['city'] = $request['city'];
			$data['is_homeaddress'] = $request['is_homeaddress'];
			$data['postcode'] = $request['postcode'];
			$data['is_excempted'] = 'no';

			if(isset($request['is_homeaddress']) && $request['is_homeaddress'] == 'home-address-yes'){
				$data['address'] = $request['address_dropdown'];
			}else{
				$data['address'] = $request['address'];
			}

			if($purpose_id == '1' && $place_type == '1'){
				$url = base_url().'api/customer/addnewmember';
				$data['clinic_id'] = $this->session->userdata('clinic_id');

				
			}else if($purpose_id == '1' && $place_type == '2'){

				$url = base_url().'api/customer/addnewmember';

				if(isset($request['is_shippingaddress']) && $request['is_shippingaddress'] == 'yes'){
					$data['shipping_postcode'] = $data['postcode'];
					$data['shipping_address'] = $data['address'];
				}else{
					$data['shipping_postcode'] = $request['shipping_postcode'];
					if($request['is_homeaddress'] == 'home-address-yes'){
						$data['shipping_address'] = $request['shipping_address_dropdown'];
					}else{
						$data['shipping_address'] = (isset($data['shipping_address']) ? $data['shipping_address']:'');
					}
				}
			}else if($purpose_id == '4'){

				$url = base_url().'api/customer/addnewmember_fitoneway';

				$data['dest_country'] = $this->session->userdata('country_id');

				$departure_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('departure_date'));
				$data['departure_date'] = $departure_date_obj->format('Y-m-d');
				$data['departure_time'] = $this->session->userdata('departure_time');
				$data['clinic_id'] = $this->session->userdata('clinic_id');
				$data['excempted_cat'] = $request['excempted_cat'];
				$data['is_excempted'] = $request['is_excempted'];
				$data['travel_destination'] = $request['travel_destination'];
				//$data['date_of_travel'] = $request['date_of_travel'];

			}else if($purpose_id == '5'){
				// _pre($this->user);
				// _pre($request);exit();
				$data['clinic_id'] = $this->session->userdata('clinic_id');

				if($this->session->userdata('postcode') != ""){
					$data['postcode'] = $this->session->userdata('postcode');
				}
				$data['postcode'] = $this->session->userdata('postcode');
				$data['visiting_country'] = $this->session->userdata('visiting_country');
				$data['is_visiting_country'] = $this->session->userdata('is_visiting_country');
				$data['res_add'] = $this->session->userdata('res_add');
				$data['pickup_date'] = $this->session->userdata('pickup_date');
				$data['ukshipping_postcode'] = $this->session->userdata('ukshipping_postcode');
				$data['resshipping_flag'] = $this->session->userdata('residency_flag');
				$data['residency_flag'] = $this->session->userdata('ukresident');

				if ($data['residency_flag'] == "1") {
					$data['residency_postcode'] = $this->session->userdata('residency_postcode');
					$data['residency_address'] = $this->session->userdata('residency_address');
				}else{
					$data['residency_postcode']="";
				
				$data['residency_postcode'] = $this->session->userdata('nonresidency_postcode');
				$data['uk_isolation_postcode'] = $this->session->userdata('uk_isolation_postcode');
				$data['uk_isolation_address'] = $this->session->userdata('uk_isolation_address');
				
				$data['residency_address'] = $this->session->userdata('nonukaddress1')." ".$this->session->userdata('nonukaddress2').", ".$this->session->userdata('nonukcity')." ".$this->session->userdata('nonukcountry');

				}
				$data['shipping_method'] = $this->session->userdata('shipping_method');
				if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "1") {
					$data['shipping_address'] = $this->session->userdata('uk_isolation_address');
					
				}else if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "2") {
					$data['shipping_address'] = $this->session->userdata('resshipping_address');
				}
				$data['shipping_date'] = $this->session->userdata('shipping_date');
				$data['return_date'] = $this->session->userdata('return_date');
				$data['return_time'] = $this->session->userdata('return_time');
				$data['is_excempted'] = $this->session->userdata('is_excempted');
				$data['excempted_cat'] = $this->session->userdata('excempted_cat');
				$data['vaccination_status'] = $this->session->userdata('vaccination_status');
				$data['dest_country'] = $this->session->userdata('dest_country');
				$data['ukresident'] = $this->session->userdata('ukresident');
				$data['nonresidency_postcode'] = $this->session->userdata('nonresidency_postcode');
				$data['nonukcity'] = $this->session->userdata('nonukcity');
				$data['nonukcountry'] = $this->session->userdata('nonukcountry');
				$data['nonukaddress1'] = $this->session->userdata('nonukaddress1');
		       
				$url = base_url().'api/booking/addnewmember_intarrival';
			}else if($purpose_id == '3'){

				$data['date'] = date('Y-m-d',strtotime($session_data['booking_date']));
				$data['time'] = date('H:i',strtotime($session_data['booking_time']));
				$data['report_id'] = $session_data['test_type'];
				$data['clinic_id'] = $session_data['clinic_id'];
				$data['dest_country'] = $session_data['country_id'];
				$data['return_country'] = $session_data['fit_to_fly_input_data']['return_country'];
				$data['is_visiting_country'] = $session_data['fit_to_fly_input_data']['is_visiting_country'];
				$data['visiting_country'] = $session_data['fit_to_fly_input_data']['visiting_country'];
				$data['second_report_id'] = $session_data['fit_to_fly_input_data']['test_type'];
				$data['transport_type'] = $session_data['fit_to_fly_input_data']['transport_type'];
				$data['transport_number'] = $session_data['fit_to_fly_input_data']['transport_number'];
				$data['shipping_method'] = $session_data['fit_to_fly_input_data']['shipping_method'];
				if($data['shipping_method'] == 2){
					$data['shippingaddress'] = $session_data['fit_to_fly_input_data']['shippingaddress'];
					$data['shippingpostal_code'] = $session_data['fit_to_fly_input_data']['shippingpostal_code'];
					$data['shipping_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['shipping_date']));
				}
				$data['return_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['return_date']));
				$data['departure_date'] = date('Y-m-d',strtotime($session_data['departure_date']));
				$data['departure_time'] = date('H:i',strtotime($session_data['departure_time']));

				$url = base_url().'api/booking/addnewmember_fitroundtrip';
			}else{
				$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
				redirect(base_url('new-booking'));
			}

			if($purpose_id != 3){

			if ($this->session->userdata('test_date') !="") {
				$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
				$data['date'] = $test_date_obj->format('Y-m-d');
				}else{
					$data['date'] ="";
				}
				$data['time'] = $this->session->userdata('test_time');
			}
			// if (isset($this->session->userdata('test_time'))) {

			$data['nationality'] = $request['nationality'];
			$data['phone'] = $request['phone'];
			$data['gender'] = $request['gender'];
			$data['date_of_birth'] = $request['date_of_birth'];
			$data['ethnicity'] = $request['ethnicity'];
			$data['passport'] = $request['passport'];
			$data['symptoms'] = $request['symptoms'];
			$data['vaccine_status'] = $request['vaccine_status'];
			$data['has_covid'] = $request['has_covid'];
			// if (isset($this->user['parent_id'])) {
			// 	$data['parent_id'] = $this->user['parent_id'];
			// }else{
			// }
			$data['parent_id'] = $id;
			if($purpose_id != 3){
				$data['report_id'] = $this->session->userdata('report_id');
				//$data['clinic_id'] = 2;
				$data['place_type'] = $this->session->userdata('place_type');
			}
			// _pre($data);

			$api_key = $this->user['api_key'];
			$method = 'POST';

			$response = api_call($url, $data ,$api_key,$method);
			$response = json_decode($response);
			//echo $url;
			//_pre($response); exit();
			if($response->status == 1){
				redirect(base_url('select-members'), 'refresh');
			}else{
				redirect(base_url('booking-edit-member/').$id, 'refresh');
			}

		}else{
			$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
			redirect(base_url('new-booking'));
		}
	}

	public function add_booking_member($id){
		// _pre($id);

		$id = $this->user['user_id'];

		if(!isset($_POST) || empty($_POST)){
			redirect(base_url('select-members'), 'refresh');
		}

		$session_data = $this->session->userdata("input_data");
		
		$purpose_id = $this->session->userdata('purpose_id');
		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}

		$place_type = $this->session->userdata('place_type');

		$request = $_POST;
		
		if(isset($request['is_shippingaddress'])){
			$this->session->set_userdata("is_shippingaddress_selected","yes");
		}else{
			$this->session->set_userdata("is_shippingaddress_selected","no");
		}

		$data['full_name'] = $request['full_name'];
		$data['email'] = $request['email'];
		$data['city'] = $request['city'];
		if($request['is_homeaddress'] == 'home-address-yes'){
			$data['is_homeaddress'] = "yes";
		}else{
			$data['is_homeaddress'] = "no";
		}
		$data['postcode'] = $request['postcode'];
		$data['is_excempted'] = 'no';

		$redirect_to = base_url('select-members');

		if(isset($request['is_homeaddress']) && $request['is_homeaddress'] == 'home-address-yes'){
			$data['address'] = $request['address_dropdown'];
		}else{
			$data['address'] = $request['address'];
		}
		if(isset($request['date_of_birth']) && !empty($request['date_of_birth'])){
			$data['date_of_birth'] = $request['date_of_birth'];
		}

		if(isset($request['address_line1']) && !empty($request['address_line1'])){
			$data['address_line1'] = $request['address_line1'];
		}
		if(isset($request['address_line2']) && !empty($request['address_line2'])){
			$data['address_line2'] = $request['address_line2'];
		}
		if(isset($request['address_line3']) && !empty($request['address_line3'])){
			$data['address_line3'] = $request['address_line3'];
		}
		if(isset($request['shipping_city']) && !empty($request['shipping_city'])){
			$data['shipping_city'] = $request['shipping_city'];
		}
		if(isset($request['shipping_state']) && !empty($request['shipping_state'])){
			$data['shipping_state'] = $request['shipping_state'];
		}

		if($purpose_id == '1' && $place_type == '1'){
			$url = base_url().'api/customer/addnewmember';
			$data['clinic_id'] = $this->session->userdata('clinic_id');

			
		}else if($purpose_id == '1' && $place_type == '2'){

			$url = base_url().'api/customer/addnewmember';

			if(isset($request['is_shippingaddress']) && $request['is_shippingaddress'] == 'yes'){
				$data['shipping_postcode'] = $data['postcode'];
				$data['shipping_address'] = $data['address'];
			}else{
				$data['shipping_postcode'] = $request['shipping_postcode'];
				if($request['is_homeaddress'] == 'home-address-yes'){
					$data['shipping_address'] = $request['shipping_address_dropdown'];
				}else{
					$data['shipping_address'] = (isset($data['shipping_address']) ? $data['shipping_address']:'');
				}

			}
		}else if($purpose_id == '4'){

			$url = base_url().'api/customer/addnewmember_fitoneway';

			$data['dest_country'] = $this->session->userdata('country_id');

			$departure_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('departure_date'));
			$data['departure_date'] = $departure_date_obj->format('Y-m-d');
			$data['departure_time'] = $this->session->userdata('departure_time');
			$data['clinic_id'] = $this->session->userdata('clinic_id');
			$data['excempted_cat'] = $request['excempted_cat'];
			$data['is_excempted'] = $request['is_excempted'];
			$data['travel_destination'] = $request['travel_destination'];
			//$data['date_of_travel'] = $request['date_of_travel'];

		}else if($purpose_id == '5'){
			// _pre($this->user);
			
			if($this->session->userdata('clinic_id')){
				$data['clinic_id'] = $this->session->userdata('clinic_id');
			}else{
				$data['clinic_id'] = "12";
			}

			if($this->session->userdata('postcode') != ""){
				$data['postcode'] = $this->session->userdata('postcode');
			}
			//$data['postcode'] = $this->session->userdata('postcode');
			$data['visiting_country'] = $this->session->userdata('visiting_country');
			$data['is_visiting_country'] = $this->session->userdata('is_visiting_country');
			$data['res_add'] = $this->session->userdata('res_add');
			$data['pickup_date'] = $this->session->userdata('pickup_date');
			$data['ukshipping_postcode'] = $this->session->userdata('ukshipping_postcode');
			$data['resshipping_flag'] = $this->session->userdata('residency_flag');
			$data['residency_flag'] = $this->session->userdata('ukresident');

			if ($data['residency_flag'] == "1") {
				$data['residency_postcode'] = $this->session->userdata('residency_postcode');
				$data['residency_address'] = $this->session->userdata('residency_address');
			}else{
				$data['residency_postcode']="";
			
			$data['residency_postcode'] = $this->session->userdata('nonresidency_postcode');
			$data['uk_isolation_postcode'] = $this->session->userdata('uk_isolation_postcode');
			$data['uk_isolation_address'] = $this->session->userdata('uk_isolation_address');
			
			$data['residency_address'] = $this->session->userdata('nonukaddress1')." ".$this->session->userdata('nonukaddress2').", ".$this->session->userdata('nonukcity')." ".$this->session->userdata('nonukcountry');

			}
			$data['shipping_method'] = $this->session->userdata('shipping_method');
			if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "1") {
				$data['shipping_address'] = $this->session->userdata('uk_isolation_address');
				
			}else if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "2") {
				$data['shipping_address'] = $this->session->userdata('resshipping_address');
			}
			if ($this->session->userdata('shipping_date') != "") {
				$data['shipping_date'] = date('Y-m-d',strtotime($session_data['shipping_date']));
			}else{
				$data['shipping_date'] = "";
			}
			$data['return_date'] = $this->session->userdata('return_date');
			$data['return_time'] = $this->session->userdata('return_time');
			$data['is_excempted'] = $this->session->userdata('is_excempted');
			$data['excempted_cat'] = $this->session->userdata('excempted_cat');
			$data['vaccination_status'] = $this->session->userdata('vaccination_status');
			$data['dest_country'] = $this->session->userdata('dest_country');
			$data['ukresident'] = $this->session->userdata('ukresident');
			$data['nonresidency_postcode'] = $this->session->userdata('nonresidency_postcode');
			$data['nonukcity'] = $this->session->userdata('nonukcity');
			$data['nonukcountry'] = $this->session->userdata('nonukcountry');
			$data['nonukaddress1'] = $this->session->userdata('nonukaddress1');

			if($this->session->userdata('vaccine_date')){
				$data['vaccine_date'] = date('Y-m-d',strtotime($this->session->userdata('vaccine_date')));
			}
			if($this->session->userdata('date_of_birth')){
				$data['date_of_birth'] = date('Y-m-d',strtotime($this->session->userdata('date_of_birth')));
			}
			if($this->session->userdata('vaccine_id')){
				$data['vaccine_id'] = $this->session->userdata('vaccine_id');
			}
			if($this->session->userdata('is_medical_excempted')){
				$data['is_medical_excempted'] = $this->session->userdata('is_medical_excempted');
			}
			if($this->session->userdata('proof_document')){
				$proof_document = $this->session->userdata('proof_document');
		        $data['proof_document'] = $proof_document;
			}

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $img_url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $img_url);
		        $folder = "assets/images/users/";
		       	s3Upload($img_url, $folder);
		        $image = S3_URL.$img_url;
		        $data['profile_picture'] = $image;
	        }	
			$url = base_url().'api/booking/addnewmember_intarrival';
		}else if($purpose_id == '3'){

			$data['date'] = date('Y-m-d',strtotime($session_data['booking_date']));
			$data['time'] = date('H:i',strtotime($session_data['booking_time']));
			$data['report_id'] = $session_data['test_type'];
			$data['clinic_id'] = $session_data['clinic_id'];
			$data['dest_country'] = $session_data['country_id'];
			$data['return_country'] = $session_data['fit_to_fly_input_data']['return_country'];
			$data['is_visiting_country'] = $session_data['fit_to_fly_input_data']['is_visiting_country'];
			$data['visiting_country'] = $session_data['fit_to_fly_input_data']['visiting_country'];
			$data['second_report_id'] = $session_data['fit_to_fly_input_data']['test_type'];
			$data['transport_type'] = $session_data['fit_to_fly_input_data']['transport_type'];
			$data['transport_number'] = $session_data['fit_to_fly_input_data']['transport_number'];
			$data['shipping_method'] = $session_data['fit_to_fly_input_data']['shipping_method'];
			if($data['shipping_method'] == 2){
				$data['shippingaddress'] = $session_data['fit_to_fly_input_data']['shippingaddress'];
				$data['shippingpostal_code'] = $session_data['fit_to_fly_input_data']['shippingpostal_code'];
				$data['shipping_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['shipping_date']));
			}
			$data['return_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['return_date']));
			$data['departure_date'] = date('Y-m-d',strtotime($session_data['departure_date']));
			$data['departure_time'] = date('H:i',strtotime($session_data['departure_time']));

			$url = base_url().'api/booking/addnewmember_fitroundtrip';
		}else{
			$url = base_url().'api/customer/addnewmember';
			$redirect_to = base_url('members');
			$data['is_editprofile'] = 1;
		}

		if($purpose_id != 3){

		if ($this->session->userdata('test_date') !="") {
			$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
			$data['date'] = $test_date_obj->format('Y-m-d');
			}else{
				$data['date'] ="";
			}
			$data['time'] = $this->session->userdata('test_time');
		}
		// if (isset($this->session->userdata('test_time'))) {

		$data['nationality'] = $request['nationality'];
		$data['phone'] = $request['phone'];
		$data['gender'] = $request['gender'];
		$data['date_of_birth'] = $request['date_of_birth'];
		$data['ethnicity'] = $request['ethnicity'];
		$data['passport'] = $request['passport'];
		$data['symptoms'] = $request['symptoms'];
		$data['vaccine_status'] = $request['vaccine_status'];
		$data['has_covid'] = $request['has_covid'];
		// if (isset($this->user['parent_id'])) {
		// 	$data['parent_id'] = $this->user['parent_id'];
		// }else{
		// }
		$data['parent_id'] = $id;
		if($purpose_id != 3){
			$data['report_id'] = $this->session->userdata('report_id');
			//$data['clinic_id'] = 2;
			$data['place_type'] = $this->session->userdata('place_type');
		}

		if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
			$type = explode('.', $_FILES["profile_picture"]["name"]);
	        $type = strtolower($type[count($type) - 1]);
	        $name = mt_rand(100000000, 999999999);
	        $filename = $name . '.' . $type;
	        $img_url = "assets/images/users/" . $filename;
	        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $img_url);
	        $folder = "assets/images/users/";
	       	s3Upload($img_url, $folder);
	        $image = S3_URL.$img_url;
	        $data['profile_picture'] = $image;
        }

		if(isset($request['vaccine_id']) && !empty($request['vaccine_id'])){
			$data['vaccine_id'] = $request['vaccine_id'];
		}
		//_pre($url);
		$api_key = $this->user['api_key'];
		$method = 'POST';
		//echo $url;
		//_pre($data); 
		$response = api_call($url, $data ,$api_key,$method);
		$response = json_decode($response);
		if(isset($request['is_review']) && $request['is_review'] == 1){
			$redirect_to = base_url('review-confirm');
		}
		if($response->status == 1){
			$this->session->set_flashdata('success', 'Member updated.');
			redirect($redirect_to, 'refresh');
		}else{
			redirect(base_url('booking-edit-member/').$id, 'refresh');
		}

		
	}

	public function booking_member_remove($user_id, $booking_id){

		$id = $this->user['user_id'];
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/customer/remove_selected_member';
		$data['user_id'] = $user_id;
		$data['booking_id'] = $booking_id;
		
		$response = api_call($url, $data ,$api_key,$method);
		$response = json_decode($response);

		//_pre($response);
		if($response->status == TRUE){
			redirect(base_url('select-members'), 'refresh');
		}else{
			redirect(base_url('select-members'), 'refresh');
		}

		//_pre($data);
	}

	public function remove_selected_member($user_id, $booking_id,$selected_user_id=null){

		$id = $this->user['user_id'];
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/customer/remove_selected_member';
		$data['user_id'] = $user_id;
		$data['booking_id'] = $booking_id;
		
		$response = api_call($url, $data ,$api_key,$method);
		$response = json_decode($response);
		
		//_pre($response);
		if($response->status == TRUE){
			if(!empty($selected_user_id)){
				redirect(base_url('booking-edit-member/'.$selected_user_id), 'refresh');
			}else{
				redirect(base_url('members/add'), 'refresh');
			}
		}else{
			redirect(base_url('select-members'), 'refresh');
		}
	}
	
	public function save_booking_session(){
		$request = $_POST;
		// _pre($request);
		$this->session->set_userdata('report_id', $request['report_id']);
		$this->session->set_userdata('purpose_id', $request['purpose_id']);
         
        if(isset($request['postcode'])){
			$this->session->set_userdata('postcode', $request['postcode']);
		}
        if(isset($request['visiting_country'])){
			$this->session->set_userdata('visiting_country', $request['visiting_country']);
		}
        if(isset($request['is_visiting_country'])){
			$this->session->set_userdata('is_visiting_country', $request['is_visiting_country']);
		}
        if(isset($request['res_add'])){
			$this->session->set_userdata('res_add', $request['res_add']);
		}
        if(isset($request['pickup_date'])){
			$this->session->set_userdata('pickup_date', $request['pickup_date']);
		}

        if(isset($request['ukshipping_postcode'])){
			$this->session->set_userdata('ukshipping_postcode', $request['ukshipping_postcode']);
		}
		if(isset($request['residency_flag'])){
			$this->session->set_userdata('residency_flag', $request['residency_flag']);
		}
		if(isset($request['shipping_method'])){
			$this->session->set_userdata('shipping_method', $request['shipping_method']);
		}
		if(isset($request['shipping_date'])){
			$this->session->set_userdata('shipping_date', $request['shipping_date']);
		}
		if(isset($request['return_date'])){
			$this->session->set_userdata('return_date', $request['return_date']);
		}
		if(isset($request['return_time'])){
			$this->session->set_userdata('return_time', $request['return_time']);
		}
		if(isset($request['is_excempted'])){
			$this->session->set_userdata('is_excempted', $request['is_excempted']);
		}
		if(isset($request['excempted_cat'])){
			$this->session->set_userdata('excempted_cat', $request['excempted_cat']);
		}

		if(isset($request['vaccination_status'])){
			$this->session->set_userdata('vaccination_status', $request['vaccination_status']);
		}
		if(isset($request['dest_country'])){
			$this->session->set_userdata('dest_country', $request['dest_country']);
		}

		if(isset($request['ukresident'])){
			$this->session->set_userdata('ukresident', $request['ukresident']);
		}
		if(isset($request['residency_postcode'])){
			$this->session->set_userdata('residency_postcode', $request['residency_postcode']);
		}
		if(isset($request['residency_address'])){
			$this->session->set_userdata('residency_address', $request['residency_address']);
		}
		if(isset($request['nonresidency_postcode'])){
			$this->session->set_userdata('nonresidency_postcode', $request['nonresidency_postcode']);
		}
		if(isset($request['nonukaddress1'])){
			$this->session->set_userdata('nonukaddress1', $request['nonukaddress1']);
		}
		if(isset($request['nonukaddress2'])){
			$this->session->set_userdata('nonukaddress2', $request['nonukaddress2']);
		}
		if(isset($request['nonukcity'])){
			$this->session->set_userdata('nonukcity', $request['nonukcity']);
		}
		if(isset($request['nonukcountry'])){
			$this->session->set_userdata('nonukcountry', $request['nonukcountry']);
		}
		if(isset($request['res_add'])){
			$this->session->set_userdata('res_add', $request['res_add']);
		}

		if(isset($request['place_type'])){
			$this->session->set_userdata('place_type', $request['place_type']);
		}

		if(isset($request['country_id'])){
			$this->session->set_userdata('country_id', $request['country_id']);
		}

		if(isset($request['test_date'])){
			$this->session->set_userdata('test_date', $request['test_date']);
		}
		if(isset($request['test_time'])){
			$this->session->set_userdata('test_time', $request['test_time']);
		}
		//for display purpose
		if(isset($request['place_of_test_name'])){
			$this->session->set_userdata('place_of_test_name', $request['place_of_test_name']);
		}
		$this->session->set_userdata('purpose_of_test_name', $request['purpose_of_test_name']);
		$this->session->set_userdata('test_type_name', $request['test_type_name']);
		$this->session->set_userdata('test_price', $request['test_price']);
			
	
		if(isset($request['departure_date'])){
			$this->session->set_userdata('departure_date', $request['departure_date']);
			$this->session->set_userdata('departure_time', $request['departure_time']);
		}
		if(isset($request['return_date1'])){
			$this->session->set_userdata('return_date', $request['return_date1']);
		}
		if(isset($request['vaccine_date'])){
			$this->session->set_userdata("vaccine_date",'"'.$request['dose_1_date'].','.$request['dose_2_date'].'"');
		}
		// if(isset($request['dose_1_date'])){
		// 	$this->session->set_userdata("dose_1_date",$request['dose_1_date']);
		// }
		// if(isset($request['dose_2_date'])){
		// 	$this->session->set_userdata("dose_2_date",$request['dose_2_date']);
		// }
		if(isset($request['date_of_birth'])){
			$this->session->set_userdata("date_of_birth",$request['date_of_birth']);
		}
		if(isset($request['is_medical_excempted'])){
			$this->session->set_userdata("is_medical_excempted",$request['is_medical_excempted']);
		}
		if(isset($request['vaccine_id'])){
			$this->session->set_userdata("vaccine_id",$request['vaccine_id']);
		}
	
		if(isset($_FILES['proof_document']['name']) && !empty($_FILES['proof_document']['name'])){
			$type = explode('.', $_FILES["proof_document"]["name"]);
	        $type = strtolower($type[count($type) - 1]);
	        $name = mt_rand(100000000, 999999999);
	        $filename = $name . '.' . $type;
	        $img_url = "assets/images/proof_document/" . $filename;
	        move_uploaded_file($_FILES["proof_document"]["tmp_name"], $img_url);
	        $folder = "assets/images/proof_document/";
	       	s3Upload($img_url, $folder);
	        $image = S3_URL.$img_url;
	        $data['profile_picture'] = $image;
	        $this->session->set_userdata("proof_document",$image);
		}
		// _pre($this->session->get_userdata());
		echo json_encode(array('status' => TRUE));
	}

	public function save_hospital_id($clinic_id){
		$this->session->set_userdata('clinic_id', $clinic_id);
		if($clinic_id !=""){
			$hospital_name = get_column_value("hospital",array("id"=>$clinic_id),"company_name");
			$this->session->set_userdata('place_of_test_name', $hospital_name);
		}

		redirect(base_url('select-members'), 'refresh');
	}

	public function is_member_selected(){
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$purpose_id = $this->session->userdata('purpose_id');
		$session_data = $this->session->userdata("input_data");
		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}
		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{

		}

		if($url != ''){
			$id = $this->user['user_id'];
			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);

			if(isset($response->data) && count($response->data) > 0){
				$status = false;
				foreach ($response->data as $key => $value) {
					if($value->booking_id != ''){
						$status = true;
						break;
					}
				}
				if($status == true){
					echo json_encode( array('status' => TRUE, 'data' => $response ) );
				}else{
					echo json_encode( array('status' => false, 'data' => $response ) );
				}
				
			}else{
				echo json_encode( array('status' => FALSE ) );
			}
		}else{
			echo json_encode( array('status' => FALSE ) );
		}
		
	}

	public function time_slot(){
		webRender('booking/time_slot');
	}
	
	public function review_confirm(){

		$api_key = $this->user['api_key'];
		$method = 'POST';

		// _pre($_SESSION);
		// _pre($this->session->get_userdata());
		$purpose_id = $this->session->userdata('purpose_id');
		$session_data = $this->session->userdata("input_data");
		if(isset($session_data['fit_to_fly_input_data']))
		{
			$test_details = $this->db->select('*')->from('reports')->where('id',$session_data['fit_to_fly_input_data']['test_type'])->get()->row_array();
		}
		// _pre($test_details);
		if(isset($test_details)){
			$session_data['test_name'] = $test_details['name'];
			$session_data['test_price'] = $test_details['price'];
		}

		if(isset($session_data['fit_to_fly_input_data']) && $purpose_id !== $session_data['fit_to_fly_input_data']['purpose_id']){
			$purpose_id = $session_data['fit_to_fly_input_data']['purpose_id'];
		}

		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}

		if($purpose_id == '1' && $this->session->userdata('purpose_of_test_name') == 'UK Departure and Return (Roundtrip)'){
			$purpose_id = $session_data['purpose_id'];
		}
		else if($purpose_id == '1' && $this->session->userdata('purpose_of_test_name') == 'General (Work, Events, Internal travel,general)'){
			$purpose_id = 1;
		}
		// _pre($purpose_id);

		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{
			$this->session->set_flashdata('error', 'Booking session expired.');
			redirect(base_url('new-booking'), 'refresh');
		}

		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);
		$data['members'] = array();
		$data['purpose_id'] = $purpose_id;

			$data['session_data'] = $session_data;
			$data['booking_data'] = $this->session->get_userdata();

		// _pre($data);
		if($response->status == 1){
			$booking_id = '';
			foreach ($response->data as $key => $value) {
				if($value->booking_id != ''){
					$booking_id = $value->booking_id;
					break;
				}
			}
			if($booking_id == ''){
				$this->session->set_flashdata('error', 'Booking session expired.');
				redirect(base_url('new-booking'), 'refresh');
			}
			$data['members'] = $response->data;
		}else{
			redirect(base_url('select-members'), 'refresh');
		}

		// _pre($data);

		webRender('booking/review_confirm', $data);
	}
	
	public function get_payment_cards(){
		if($this->session->userdata("is_shippingaddress_selected")){
			$this->session->unset_userdata('is_shippingaddress_selected');
		}

		$api_key = $this->user['api_key'];
		$method = 'POST';
		$id = $this->user['user_id'];
		// $output_data = array();
		// $output_data['booking_id'] = 1;
		// $output_data['booking_reference_id'] = 1;
		// $output_data['customer_id'] =$customer_id = 1;

		// webRender('booking/payment', $output_data);

		$purpose_id = $this->session->userdata('purpose_id');
		$session_data = $this->session->userdata("input_data");
		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}
		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{
			$this->session->set_flashdata('error', 'Booking session expired.');
			redirect(base_url('new-booking'), 'refresh');
		}

		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);
		$data['members'] = array();
		if($response->status == 1){
			$members = $response->data;
			$total_member = count($members);
			$report_price = $this->session->userdata('test_price');
			$sub_total = $total_member * $report_price;
			$total_price = $sub_total + TAX_PRICE;

			//_pre($members);

			foreach ($members as $key => $value) {
				if($value->booking_id != ''){
					$booking_id = $value->booking_id;
					continue;
				}
			}

			if(isset($booking_id) && $booking_id != ''){

				$booking_data = array(
						'user_id' => $id,
						'report_price' => $report_price,
						'tax_price' => TAX_PRICE,
						'member_details' => '',
						'total_price' => $total_price,
						'booking_id' => $booking_id
					);

				//_pre($booking_data);

				$url = base_url().'api/booking/add_booking';
				$response = api_call($url,$booking_data,$api_key,$method);
				$response = json_decode($response);
				if($response->status == TRUE){
					//_pre($response);
					$output_data['booking_id'] = $response->data->booking_id;
					$output_data['booking_reference_id'] = $response->data->booking_reference_id;
					$output_data['customer_id'] =$customer_id = $response->data->customer_id;

					$this->load->helper('strp_helper');
					//$cards = get_payment_card('cus_KC9l2j2LGceCpT');
					$cards = get_payment_card($customer_id);

					// _pre($cards);
					if($cards['status'] == 1 && isset($cards['data'])){
						$output_data['cards'] = $cards['data'];
					}


					webRender('booking/payment', $output_data);

				}else{
					redirect(base_url('select-members'), 'refresh');
				}							

			}else{
				$this->session->set_flashdata('error', 'Booking session has been expired' );
				redirect(base_url('new-booking'));
			}
			
		}else{
			$this->session->set_flashdata('error', 'Booking session has been expired.');
			redirect(base_url('select-members'), 'refresh');
		}

	}

	public function book(){
		$request = $_POST;

		$this->load->helper('strp_helper');
		$purpose_id = $this->session->userdata('purpose_id');
		$session_data = $this->session->userdata("input_data");
		if(!$this->session->userdata('purpose_id')){	
			$purpose_id = $session_data['purpose_id'];	
		}
		if($purpose_id == 3){
			$sub_total = $session_data['clinic_price'];
		}else{
			$sub_total = $this->session->userdata('test_price');
		}
		$total_price = $sub_total + TAX_PRICE;

		$transact = array();
        $transact['description'] = "Web Booking id : ".$request['booking_reference_id'];
        $transact['amount'] = $total_price * 100;
        $transact['customer'] = $request['customer_id'];
        $transact['card'] = $request['payment_card'];
        $paymentResponse = addCharge($transact);

        if( isset($paymentResponse['status']) && $paymentResponse['status'] == 1){

        	$api_key = $this->user['api_key'];
			$method = 'POST';

			$url = base_url().'api/booking/booking_status';

			$data = array(
				'booking_id' => $request['booking_id'],
				'payment_status' => 1,
				'payment_type' => 'card'
			);
			$response = api_call($url,$data,$api_key,$method);
			$response = json_decode($response);

			$this->session->unset_userdata('report_id');	
			$this->session->unset_userdata('purpose_id');	
			$this->session->unset_userdata('place_type');	
			$this->session->unset_userdata('test_date');	
			$this->session->unset_userdata('test_time');	
			$this->session->unset_userdata('place_of_test_name');	
			$this->session->unset_userdata('purpose_of_test_name');	
			$this->session->unset_userdata('test_type_name');	
			$this->session->unset_userdata('test_price');	
			$this->session->unset_userdata('clinic_id');	
			$this->session->unset_userdata('country_id');	
			$this->session->unset_userdata('departure_date');	
			$this->session->unset_userdata('departure_time');
			$this->session->unset_userdata('input_data');

			echo json_encode(array('status' => TRUE, 'booking_reference_id' => $request['booking_reference_id']));

        }else{
        	$error = $paymentResponse['error']['message'];
        	echo json_encode(array('status' => FALSE, 'message' => $error));
        }
		
	}
	
	public function add_card(){
		$request = $_POST;
		$this->load->helper('strp_helper');
		$month_year = explode('-', $request['expire']);

		$card = array(
			'name' => trim($request['name']),
			'card_number' => trim($request['card_number']),
			'month' => $month_year[0],
			'year' =>  $month_year[1],
			'cvc_number' => trim($request['cvc_number'])
			);

		$last4_of_card = substr($request['card_number'], -4);

		$result = create_token($card);

		
		if($result['status'] == true){

			$token = $result['token'];
			$api_key = $this->user['api_key'];
			$method = 'POST';
			$id = $this->user['user_id'];

			$session_data = $this->session->userdata("input_data");
			$purpose_id = $this->session->userdata('purpose_id');
			if(!$this->session->userdata('purpose_id')){	
				$purpose_id = $session_data['purpose_id'];	
			}

			if($purpose_id == '1'){
				$url = base_url().'api/customer/members_list';
			}else if($purpose_id == '4'){
				$url = base_url().'api/customer/members_list_fitoneway';
			}else if($purpose_id == '5'){
				$url = base_url().'api/booking/members_list_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/members_list_fitroundtrip';
			}else{
				echo json_encode(array('status' => FALSE, 'message' => 'Booking session has been expired12'));
			}

			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);
			if($response->status == 1){
				$members = $response->data;
				
				foreach ($members as $key => $value) {
					if($value->id == $id){
						$customer_id = $value->stripe_key;
						break;
					}
				}

				$methods = \Stripe\PaymentMethod::all([
					'customer' => $customer_id, // $this->customer->id is running PHP object instance customer ID variable.
					'type'     => 'card',
				]);

				// _pre($methods['data']);
				// _pre($methods['data'][0]->card->last4);

				if(isset($customer_id) && $customer_id != ''){
					
					$card = add_payment_card($customer_id, $token);
					// echo "<pre>";
					// print_r($card['data']);exit;
					$flag = 1;
					foreach($methods['data'] as $card_key => $card_data){

						if($card_data->card->fingerprint == $card['data']->fingerprint)
						{
							\Stripe\Customer::deleteSource($card['data']->customer,$card['data']->id, []);

							echo json_encode(array('status' => FALSE, 'message' => 'This card has already been added'));
							$flag = 0;
							break;
						}
					}


					if($flag == 1){

						$card_id = $card['data']['id'];
						$last4 = $card['data']['last4'];
						echo json_encode(array('status' => TRUE, 'card_id' => $card_id, 'last4' => $last4, 'card' => $card));
					}

				}else{
					echo json_encode(array('status' => FALSE, 'message' => 'Booking session has been expired34'));
				}

			}
			
		}else{
			echo json_encode(array('status' => FALSE, 'message' => $result['error']['message']));
		}


		
	}
	
	public function search_map(){
		
		$post_data['test_type'] = $this->session->userdata('report_id');
		$purpose_id = $this->session->userdata("purpose_id");
		$test_date = $this->session->userdata("test_date");
		$test_time = $this->session->userdata("test_time");
		if($purpose_id == 3){
			$input_data['postcode'] = '';
			$input_data['purpose_id'] = $this->session->userdata("purpose_id");
			$input_data['purpose_of_test_name'] = $this->session->userdata("purpose_of_test_name");
			$input_data['country_id'] = $this->session->userdata("country_id");
			$input_data['test_type'] = $this->session->userdata("report_id");
			$input_data['place_test'] = $this->session->userdata("place_type");
			$input_data['departure_date'] = $this->session->userdata("departure_date");
			$input_data['departure_time'] = $this->session->userdata("departure_time");
			$input_data['return_date1'] = $this->session->userdata("return_date");
			$input_data['booking_date'] = $this->session->userdata("test_date");
			$input_data['booking_time'] = $this->session->userdata("test_time");
			$this->session->set_userdata("input_data",$input_data);
		}

		$filter_url = $url = base_url().'api/hospital/filter';
		$method = 'GET';
		$api_key = $this->user['api_key'];
		$filter_response = api_call($filter_url,null,$api_key,$method);
		$filter_response = json_decode($filter_response, true);
		$filter_array = array();
		if($filter_response['status'] == 1){
			$filter_array = $filter_response['data'];
		}
		$data['filter_data'] = $filter_array;
		//_pre($_SESSION['input_data']);
		webRender('booking/search_map',$data);
	}

	public function get_clinic_from_map(){
		$request = $this->input->post();

		$purpose_id = $this->session->userdata('purpose_id');
		$session_data = $this->session->userdata("input_data");
		if(!$this->session->userdata("purpose_id")){	
			$purpose_id = $session_data['purpose_id'];	
		}

	
		$test_date = $this->session->userdata("test_date");
		$test_time = $this->session->userdata("test_time");
		$purpose_of_test_name = $this->session->userdata("purpose_of_test_name");
		$test_type_name = $this->session->userdata("test_type_name");
		$method = 'POST';
		$api_key = $this->user['api_key'];
		$post_data['test_type'] = $this->session->userdata('report_id');
		$post_data['search_key'] = "map";
		$post_data['lat'] = $request['lat'];
		$post_data['lng'] = $request['lng'];
		$post_data['purpose_id'] = $purpose_id;
		$test_time = "";
		$test_date = "";
		if($purpose_id == 1 || $purpose_id == 4){
			$url = base_url().'api/hospital/list';
			if($this->session->userdata('test_time')){
				$post_data['test_time'] = $this->session->userdata('test_time');
				$test_time = $post_data['test_time'];
			}

			if($this->session->userdata('test_date')){
				$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
				$post_data['test_date'] = $test_date_obj->format('Y-m-d');
				$test_date = $post_data['test_date'];
			}
			
		}else if($purpose_id == 3){
			$url = base_url().'api/hospital/list';
			$input_data = $this->session->userdata("input_data");
			$post_data['test_type'] = $input_data['test_type'];
			$post_data['test_date'] = date('Y-m-d',strtotime($input_data['booking_date']));
			if($input_data['purpose_id'] == 3){
				$post_data['test_time'] = date('H:i',strtotime($input_data['booking_time']));
				$test_time = $post_data['test_time'];
			}
			$test_date = $post_data['test_date'];
		}else{
			$url = base_url().'api/hospital/international_list';

			if ($this->session->userdata('shipping_method') == "2") {
				$post_data['date'] = $this->session->userdata('shipping_date');
			}else{
				$post_data['date'] = $this->session->userdata('pickup_date');
			}
			$test_date = $post_data['date'];
			$test_date_obj = DateTime::createFromFormat('d-m-Y', $post_data['date']);
			$post_data['date'] = $test_date_obj->format('Y-m-d');
		}
		//_pre($post_data);
		$response = api_call($url,$post_data,$api_key,$method);
		$response = json_decode($response, true); 
		$final_result = array();
		$i = 0;

		if(!empty($response['data'])){
			foreach($response['data'] as $row){
				$final_result[$i][0]['lat'] = (float)$row['lat'];
				$final_result[$i][0]['lng'] = (float)$row['long'];
				$final_result[$i][1] = $row['company_name'];
				$final_result[$i][2] = $row['address'];
				$final_result[$i][3] = $row['id'];
				$final_result[$i][4] = $purpose_id;
				$final_result[$i][5] = $purpose_of_test_name;
				$final_result[$i][6] = $test_type_name;
				$final_result[$i][7] = $test_date;
				$final_result[$i][8] = $test_time;
				$final_result[$i][9] = $row['price'];
				$i++;
			}
		}
		echo json_encode(array('status' => true, 'data' => $final_result));
	}


	public function get_vaccine_dose_detail($vaccine_id){

		$api_key = $this->user['api_key'];
		$certified_vaccines_list = certified_vaccines_list($api_key);
		// _pre($certified_vaccines_list);
		foreach($certified_vaccines_list['data'] as $vaccine)
		{
			if($vaccine['id'] == $vaccine_id){

				$vaccine_dose = $vaccine['total_required_does'];
			}
		}
		echo json_encode(array('status' => true, 'data' => $vaccine_dose));
		// echo json_encode($vaccine_dose);
		exit();
	}
}
