 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingDemoController extends MY_Controller {
	protected $booking_api;
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
        $this->load->model('User');
		$this->booking_api = base_url().'api/booking/';
		$this->user = $this->session->userdata('members');
	}
	public function index() {
		
		$user_data = $this->session->userdata('members');
		$purpose_type_url = $this->booking_api.'purpose_type';
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$purpose_type_response = api_call($purpose_type_url,array("user_id"=>$user_data['user_id']), $api_key, $method);
		$purpose_type_response = json_decode($purpose_type_response);
		
		$place_test_url = $this->booking_api.'place_test';
		$place_test_response = api_call($place_test_url,[],$api_key,'GET');
		$place_test_response = json_decode($place_test_response);

		$data['country_list'] = countries_list($api_key);
		$data['exemptioncat_list'] = exemptioncat_list($api_key);

		//echo date('H',strtotime("+2 Hours")); exit;
		$next_time = date('h',strtotime("+2 Hours"));
		$current_time_array = array();
		for($i=$next_time;$i<=23;$i++){
			array_push($current_time_array,$i);
		}

		//_pre($current_time_array);
		$data['current_time_array'] = $current_time_array;
		$data['purpose_type_response'] = $purpose_type_response;
		$data['place_test_response'] = $place_test_response;
		//$data['county'] = $this->db->get_where('county', array('status' => 1))->result_array();
		webRender('booking/new_booking_test',$data);
	}

	public function get_hours(){
		$input = $this->input->post();
		$current_time_array = array();
		if(date('d-m-Y') == $input['date']){
			$next_time = date('H',strtotime("+2 Hours"));
			for($i=$next_time;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}else{
			for($i=1;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}
		$data['current_time_array'] = $current_time_array;
		print json_encode($data);
		exit();
	}

	public function get_report_test(){
		$request = $this->input->post();
		$user_data = $this->session->userdata('members');
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/reporttype/reportbycountry';
		$data = array(
			"purpose_id"		=> $request['purpose_id'],
			"country_id"		=> (isset($request['country_id'])) ? $request['country_id'] : '',
			"vaccination_status"=> (isset($request['vaccination_status'])) ? $request['vaccination_status'] : '',
			"excempted_cat"		=> (isset($request['excempted_cat'])) ? $request['excempted_cat'] : '',
			"vaccine_id"		=> (isset($request['vaccine_id'])) ? $request['vaccine_id'] : '',
			"is_medical_excempted"		=> (isset($request['is_medical_excempted'])) ? $request['is_medical_excempted'] : '',
		);
		$response = api_call($url,$data,$api_key,$method);
		$response = json_decode($response);
		$data = array();
		if($response->status == 1){
			$data = $response->data;
		}
		print json_encode($data);
		exit();
	}

	public function save_booking_session(){
		$request = $_POST;
		// print_r($request); exit();
		$this->session->set_userdata('report_id', $request['report_id']);
		$this->session->set_userdata('purpose_id', $request['purpose_id']);
            
		if(isset($request['vaccination_status'])){
			$this->session->set_userdata('vaccination_status', $request['vaccination_status']);
		}
		if(isset($request['dest_country'])){
			$this->session->set_userdata('dest_country', $request['dest_country']);
		}

		if(isset($request['ukresident'])){
			$this->session->set_userdata('ukresident', $request['ukresident']);
		}
		if(isset($request['residency_postcode'])){
			$this->session->set_userdata('residency_postcode', $request['residency_postcode']);
		}
		if(isset($request['residency_address'])){
			$this->session->set_userdata('residency_address', $request['residency_address']);
		}
		if(isset($request['nonresidency_postcode'])){
			$this->session->set_userdata('nonresidency_postcode', $request['nonresidency_postcode']);
		}
		if(isset($request['nonukaddress1'])){
			$this->session->set_userdata('nonukaddress1', $request['nonukaddress1']);
		}
		if(isset($request['nonukaddress2'])){
			$this->session->set_userdata('nonukaddress2', $request['nonukaddress2']);
		}
		if(isset($request['nonukcity'])){
			$this->session->set_userdata('nonukcity', $request['nonukcity']);
		}
		if(isset($request['nonukcountry'])){
			$this->session->set_userdata('nonukcountry', $request['nonukcountry']);
		}

		if(isset($request['place_type'])){
			$this->session->set_userdata('place_type', $request['place_type']);
		}

		if(isset($request['country_id'])){
			$this->session->set_userdata('country_id', $request['country_id']);
		}

		if(isset($request['test_date'])){
			$this->session->set_userdata('test_date', $request['test_date']);
		}
		if(isset($request['test_time'])){
			$this->session->set_userdata('test_time', $request['test_time']);
		}
		//for display purpose
		if(isset($request['place_of_test_name'])){
			$this->session->set_userdata('place_of_test_name', $request['place_of_test_name']);
		}
		$this->session->set_userdata('purpose_of_test_name', $request['purpose_of_test_name']);
		$this->session->set_userdata('test_type_name', $request['test_type_name']);
		$this->session->set_userdata('test_price', $request['test_price']);
		// $this->session->set_userdata('test_date', $request['test_date']);
		// $this->session->set_userdata('test_time', $request['test_time']);

		if(isset($request['departure_date'])){
			$this->session->set_userdata('departure_date', $request['departure_date']);
			$this->session->set_userdata('departure_time', $request['departure_time']);
		}	

		echo json_encode(array('status' => TRUE));
	}

	public function my_booking(){
		webRender('booking/my_booking');
	}

	public function search(){
		$input = $this->input->post();

		$user_data = $this->session->userdata('members');
		$url = base_url().'api/hospital/list';
		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$method = 'POST';
		$api_key = $this->user['api_key'];


		// $purpose_type_response = api_call($purpose_type_url,array("purpose_id"=>$input['purpose_id']), $api_key, $method);
		// $purpose_type_response = json_decode($purpose_type_response);
		// _pre($input);
		
		$post_data['test_type'] = $input['test_type'];
		if (isset($input['test_date'])) {
			$post_data['test_date'] = date('Y-m-d',strtotime($input['test_date']));
			if (isset($input['test_minute'])) {
				// code...
				$post_data['test_time'] = $input['test_hours'].":".$input['test_minute'];
			}
		}
		$post_data['postcode'] = $input['postcode'];
		$post_data['search_key'] = '';
		$post_data['search_value'] = '';	
		
		// $post_data['test_type'] = '3';
		// $post_data['test_date'] = '2021-09-04';
		// $post_data['test_time'] = '18:00';
		// $post_data['postcode'] = 'GU167HF';
		// $post_data['search_key'] = '';
		// $post_data['search_value'] = '';	

		$response = api_call($url,$post_data,$api_key,$method);
		$response = json_decode($response, true);

		$data['list'] = [];
		$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
		$purpose_type = $this->config->item("purpose_type");

		if($response['status'] == 1 && !empty($response['data']))
		{
			$p_data['place_test'] = $input['place_test'];
			$p_data['purpose_id'] = $input['purpose_id'];

			$data['list'] = $response['data'];
			$data['post'] = $post_data;
			$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
			$data['purpose_type_name'] = $purpose_type_name['name'];
			$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
			$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
			$data['pur_test_place'] = '';
			$purpose_place_name = $this->config->item("place_test");
			foreach ($purpose_place_name as $key => $value) {
				if($value['id'] == $p_data['place_test'])
				{
					$data['pur_test_place'] = $value['name'];
				}
			}
			$data['pur_type_place'] = '';
			$purpose_place_type = $this->config->item("purpose_type");
			foreach ($purpose_place_type as $key => $value) {
				if($value['id'] == $p_data['purpose_id'])
				{
					$data['pur_type_place'] = $value['name'];
				}
			}
		}

		// _pre($data);
		webRender('booking/search',$data);
	}
	
	public function complete_your_registration(){

		//_pre($_SESSION); 
		$api_key = $this->user['api_key'];
		$data['ethnicity_list'] = ethnicity($api_key);
		webRender('booking/complete_your_registration', $data);
	}

	public function select_members(){
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/customer/members_list';
		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);
		$data['members'] = array();
		if($response->status == 1){
			$data['members'] = $response->data;
		}else{
			redirect(base_url('members/add?redirect_url=dgdfg'), 'refresh');
		}

		webRender('booking/member_list2', $data);
	}
	
	public function time_slot(){
		webRender('booking/time_slot');
	}
	
	public function review_confirm(){
		webRender('booking/review_confirm');
	}
	
	public function payment(){
		webRender('booking/payment');
	}
	
	public function add_cart(){
		webRender('booking/add_card');
	}
	
	public function search_map(){
		webRender('booking/search-map');
	}
}
