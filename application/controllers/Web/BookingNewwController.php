 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingController extends MY_Controller {
	protected $booking_api;
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
        $this->load->model('User');
		$this->booking_api = base_url().'api/booking/';
		$this->user = $this->session->userdata('members');
	}
	public function index() {

		/*$this->load->helper('strp_helper');

		$card = array(
			'name' => 'Dhrumi One',
			'card_number' => '42424242424242421',
			'month' => '12',
			'year' => '2023',
			'cvc_number' => '381'
			);

		$result = create_token($card);
		$token = $result['token'];

		$card = add_payment_card('cus_KC9l2j2LGceCpT', $token);

		echo "card";
		var_dump($card['data']['id']);

		$customer = get_payment_card('cus_KC9l2j2LGceCpT');
		_pre($customer);
		$this->load->helper('strp_helper');
		$transact = array();
        $transact['description'] = "Order ID : Te1st";
        $transact['amount'] = 5 * 100;
        // $transact['customer'] = 'cus_KC9l2j2LGceCpT';
        $transact['customer'] = 'cus_K95Noc3xOuUBtV';
        $transact['card'] = 'card_1JZuclKbzedpBRX3HJ6PXPqK';
        $paymentResponse = addCharge($transact);

		_pre($paymentResponse);*/
		
		
		$user_data = $this->session->userdata('members');
		$purpose_type_url = $this->booking_api.'purpose_type';
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$purpose_type_response = api_call($purpose_type_url,array("user_id"=>$user_data['user_id']), $api_key, $method);
		$purpose_type_response = json_decode($purpose_type_response);
		
		$place_test_url = $this->booking_api.'place_test';
		$place_test_response = api_call($place_test_url,[],$api_key,'GET');
		$place_test_response = json_decode($place_test_response);

		$data['country_list'] = countries_list($api_key);
		
		
		//echo date('H',strtotime("+2 Hours")); exit;
		$next_time = date('h',strtotime("+2 Hours"));
		$current_time_array = array();
		for($i=$next_time;$i<=23;$i++){
			array_push($current_time_array,$i);
		}

		//_pre($current_time_array);
		$data['current_time_array'] = $current_time_array;
		$data['purpose_type_response'] = $purpose_type_response;
		$data['place_test_response'] = $place_test_response;
		//$data['county'] = $this->db->get_where('county', array('status' => 1))->result_array();

		//_pre(TAX_PRICE);

		webRender('booking/new_booking',$data);
	}

	public function get_hours(){
		$input = $this->input->post();
		$current_time_array = array();
		if(date('d-m-Y') == $input['date']){
			$next_time = date('H',strtotime("+2 Hours"));
			for($i=$next_time;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}else{
			for($i=1;$i<=23;$i++){
				array_push($current_time_array,$i);
			}
		}
		$data['current_time_array'] = $current_time_array;
		print json_encode($data);
		exit();
	}

	public function get_report_test(){
		$request = $this->input->post();
		$user_data = $this->session->userdata('members');
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/reporttype/reportbycountry';
		$input = array(
			'purpose_id' => $request['purpose_id']
		);
		if(isset($request['country_id']) && $request['country_id'] != ''){
			$input['id'] = $request['country_id'];
		}


		$response = api_call($url, $input ,$api_key,$method);
		$response = json_decode($response);
		$data = array();
		if($response->status == 1){
			$data = $response->data;
			$this->session->set_userdata('tax_price', $response->tax_price);
		}
		print json_encode($data);
		exit();
	}

	public function my_booking(){
		webRender('booking/my_booking');
	}

	public function search_old(){
		$data = array();
		$input = $this->input->post();

		$user_data = $this->session->userdata('members');
		$url = base_url().'api/hospital/list';
		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$method = 'POST';
		$api_key = $this->user['api_key'];


		// $purpose_type_response = api_call($purpose_type_url,array("purpose_id"=>$input['purpose_id']), $api_key, $method);
		// $purpose_type_response = json_decode($purpose_type_response);
		// _pre($input);
		if (isset($input['test_type'])) {
			// code...
			$post_data['test_type'] = $input['test_type'];
			$post_data['test_time'] = $input['test_hours'].":".$input['test_minute'];
			$post_data['test_date'] = date('Y-m-d',strtotime($input['test_date']));
			$post_data['postcode'] = $input['postcode'];
			$post_data['search_key'] = '';
			$post_data['search_value'] = '';	

			$response = api_call($url,$post_data,$api_key,$method);
			$response = json_decode($response, true); 

			$data['list'] = [];
			$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
			$purpose_type = $this->config->item("purpose_type");

			if($response['status'] == 1 && !empty($response['data']))
			{
				$p_data['place_test'] = $input['place_test'];
				$p_data['purpose_id'] = $input['purpose_id'];

				$data['list'] = $response['data'];
				$data['post'] = $post_data;
				$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
				$data['purpose_type_name'] = $purpose_type_name['name'];
				$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
				$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
				$data['pur_test_place'] = '';
				$purpose_place_name = $this->config->item("place_test");
				foreach ($purpose_place_name as $key => $value) {
					if($value['id'] == $p_data['place_test'])
					{
						$data['pur_test_place'] = $value['name'];
					}
				}
				$data['pur_type_place'] = '';
				$purpose_place_type = $this->config->item("purpose_type");
				foreach ($purpose_place_type as $key => $value) {
					if($value['id'] == $p_data['purpose_id'])
					{
						$data['pur_type_place'] = $value['name'];
					}
				}
			}	
		}else{
			$data['message'] = "No hospital found.";
		}
		
		// $post_data['test_type'] = '3';
		// $post_data['test_date'] = '2021-09-04';
		// $post_data['test_time'] = '18:00';
		// $post_data['postcode'] = 'GU167HF';
		// $post_data['search_key'] = '';
		// $post_data['search_value'] = '';	




		// _pre($data);
		webRender('booking/search',$data);
	}
	
	public function search(){
		$data = array();
		//_pre($_REQUEST);
		
		$url = base_url().'api/hospital/list';
		$purpose_type_url = base_url().'api/reporttype/reportbycountry';
		$method = 'POST';
		$api_key = $this->user['api_key'];

		// code...
		$post_data['test_type'] = $this->session->userdata('report_id');
		if($this->session->userdata('test_time')){
			$post_data['test_time'] = $this->session->userdata('test_time');
		}

		if($this->session->userdata('test_date')){
			$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
			$post_data['test_date'] = $test_date_obj->format('Y-m-d');
		}
		
		
		$post_data['postcode'] = $_REQUEST['postcode'];

		$post_data['search_key'] = '';
		$post_data['search_value'] = '';	

		//_pre($post_data);


		$post_data['test_type'] = '3';
		$post_data['test_date'] = '2021-09-04';
		$post_data['test_time'] = '18:00';
		$post_data['postcode'] = 'GU167HF';
		$post_data['search_key'] = '';
		$post_data['search_value'] = '';	



		$response = api_call($url,$post_data,$api_key,$method);
		$response = json_decode($response, true); 

		$data['list'] = [];
		$data['message'] = (isset($response['message']) && $response['message'] != '' ? $response['message'] : '');
		$purpose_type = $this->config->item("purpose_type");

		//_pre($response);
		if($response['status'] == 1 && !empty($response['data']))
		{
			/*$p_data['place_test'] = $input['place_test'];
			$p_data['purpose_id'] = $input['purpose_id'];*/

			$data['list'] = $response['data'];
			/*$data['post'] = $post_data;
			$purpose_type_name = $this->db->get_where('reports', array('id' => $post_data['test_type']))->row_array();
			$data['purpose_type_name'] = $purpose_type_name['name'];
			$data['pur_test_date'] = date('M d Y',strtotime($post_data['test_date']));
			$data['pur_test_time'] = date('h:i A',strtotime($post_data['test_time']));
			$data['pur_test_place'] = '';
			$purpose_place_name = $this->config->item("place_test");
			foreach ($purpose_place_name as $key => $value) {
				if($value['id'] == $p_data['place_test'])
				{
					$data['pur_test_place'] = $value['name'];
				}
			}
			$data['pur_type_place'] = '';
			$purpose_place_type = $this->config->item("purpose_type");
			foreach ($purpose_place_type as $key => $value) {
				if($value['id'] == $p_data['purpose_id'])
				{
					$data['pur_type_place'] = $value['name'];
				}
			}*/
		}	
		
		webRender('booking/search',$data);
	}

	public function complete_your_registration(){

		//_pre($_SESSION); 
		$api_key = $this->user['api_key'];
		$data['ethnicity_list'] = ethnicity($api_key);
		webRender('booking/complete_your_registration', $data);
	}

	public function select_members(){

		//_pre($_SESSION);
		$api_key = $this->user['api_key'];
		$method = 'POST';

		if($this->session->userdata('purpose_id')){
			$purpose_id = $this->session->userdata('purpose_id');
			if($purpose_id == '1'){
				$url = base_url().'api/customer/members_list';
			}else if($purpose_id == '4'){
				$url = base_url().'api/customer/members_list_fitoneway';
			}else if($purpose_id == '5'){
				$url = base_url().'api/booking/members_list_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/members_list_fitroundtrip';
			}else{
				$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
				redirect(base_url('new-booking'));
			}

			$id = $this->user['user_id'];
			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);
			$data['members'] = array();
			if($response->status == 1){
				$data['members'] = $response->data;
			}else{
				redirect(base_url('members/add'), 'refresh');
			}

			webRender('booking/member_list2', $data);

		}else{
			$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
			redirect(base_url('new-booking'));
		}

	}

	public function add_booking_member($id){

		$id = $this->user['user_id'];

		if(!isset($_POST) || empty($_POST)){
			redirect(base_url('select-members'), 'refresh');
		}

		if($this->session->userdata('purpose_id')){
			$purpose_id = $this->session->userdata('purpose_id');
			$place_type = $this->session->userdata('place_type');

			$request = $_POST;
	
			$data['full_name'] = $request['full_name'];
			$data['email'] = $request['email'];
			$data['city'] = $request['city'];
			$data['is_homeaddress'] = $request['is_homeaddress'];
			$data['postcode'] = $request['postcode'];

			if(isset($request['is_homeaddress']) && $request['is_homeaddress'] == 'home-address-yes'){
				$data['address'] = $request['address_dropdown'];
			}else{
				$data['address'] = $request['address'];
			}

			if($purpose_id == '1' && $place_type == '1'){
				$url = base_url().'api/customer/addnewmember';
				$data['clinic_id'] = $this->session->userdata('clinic_id');
			}else if($purpose_id == '1' && $place_type == '2'){

				$url = base_url().'api/customer/addnewmember';

				if(isset($request['is_shippingaddress']) && $request['is_shippingaddress'] == 'yes'){
					$data['shipping_postcode'] = $data['postcode'];
					$data['shipping_address'] = $data['address'];
				}else{
					$data['shipping_postcode'] = $request['shipping_postcode'];
					if($request['is_homeaddress'] == 'home-address-yes'){
						$data['shipping_address'] = $request['shipping_address_dropdown'];
					}else{
						$data['shipping_address'] = (isset($data['shipping_address']) ? $data['shipping_address']:'');
					}
				}
			}else if($purpose_id == '4'){

				$url = base_url().'api/customer/addnewmember_fitoneway';

				$data['dest_country'] = $this->session->userdata('country_id');

				$departure_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('departure_date'));
				$data['departure_date'] = $departure_date_obj->format('Y-m-d');
				$data['departure_time'] = $this->session->userdata('departure_time');
				$data['clinic_id'] = $this->session->userdata('clinic_id');
				$data['excempted_cat'] = $request['excempted_cat'];
				$data['is_excempted'] = $request['is_excempted'];
				$data['travel_destination'] = $request['travel_destination'];
				//$data['date_of_travel'] = $request['date_of_travel'];

			}else if($purpose_id == '5'){
				// _pre($this->user);
				// _pre($request);exit();

				// if($this->session->userdata('postcode') != ""){
				// 	$data['postcode'] = $this->session->userdata('postcode');
				// }
				$data['postcode'] = $this->session->userdata('postcode');
				$data['visiting_country'] = $this->session->userdata('visiting_country');
				$data['is_visiting_country'] = $this->session->userdata('is_visiting_country');
				$data['res_add'] = $this->session->userdata('res_add');
				$data['pickup_date'] = $this->session->userdata('pickup_date');
				$data['ukshipping_postcode'] = $this->session->userdata('ukshipping_postcode');
				$data['resshipping_flag'] = $this->session->userdata('residency_flag');
				$data['residency_flag'] = $this->session->userdata('ukresident');

				if ($data['residency_flag'] == "1") {
					$data['residency_postcode'] = $this->session->userdata('residency_postcode');
					$data['residency_address'] = $this->session->userdata('residency_address');
				}else{
					$data['residency_postcode']="";
				
				$data['residency_postcode'] = $this->session->userdata('nonresidency_postcode');
				$data['uk_isolation_postcode'] = $this->session->userdata('uk_isolation_postcode');
				$data['uk_isolation_address'] = $this->session->userdata('uk_isolation_address');
				
				$data['residency_address'] = $this->session->userdata('nonukaddress1')." ".$this->session->userdata('nonukaddress2').", ".$this->session->userdata('nonukcity')." ".$this->session->userdata('nonukcountry');

				}
				$data['shipping_method'] = $this->session->userdata('shipping_method');
				if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "1") {
					$data['shipping_address'] = $this->session->userdata('uk_isolation_address');
					
				}else if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "2") {
					$data['shipping_address'] = $this->session->userdata('resshipping_address');
				}
				$data['return_date'] = $this->session->userdata('return_date');
				$data['return_time'] = $this->session->userdata('return_time');
				$data['is_excempted'] = $this->session->userdata('is_excempted');
				$data['excempted_cat'] = $this->session->userdata('excempted_cat');
				$data['vaccination_status'] = $this->session->userdata('vaccination_status');
				$data['dest_country'] = $this->session->userdata('dest_country');
				// $data['ukresident'] = $this->session->userdata('ukresident');
				// $data['nonresidency_postcode'] = $this->session->userdata('nonresidency_postcode');
				// $data['nonukcity'] = $this->session->userdata('nonukcity');
				// $data['nonukcountry'] = $this->session->userdata('nonukcountry');
				// $data['nonukaddress1'] = $this->session->userdata('nonukaddress1');
		       
				$url = base_url().'api/booking/addnewmember_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/addnewmember_fitroundtrip';
			}else{
				$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
				redirect(base_url('new-booking'));
			}

			if ($this->session->userdata('test_date') !="") {
				$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
				$data['date'] = $test_date_obj->format('Y-m-d');
			}else{
				$data['date'] ="";
			}

			// if (isset($this->session->userdata('test_time'))) {
			$data['time'] = $this->session->userdata('test_time');

			$data['nationality'] = $request['nationality'];
			$data['phone'] = $request['phone'];
			$data['gender'] = $request['gender'];
			$data['date_of_birth'] = $request['date_of_birth'];
			$data['ethnicity'] = $request['ethnicity'];
			$data['passport'] = $request['passport'];
			$data['symptoms'] = $request['symptoms'];
			$data['vaccine_status'] = $request['vaccine_status'];
			$data['has_covid'] = $request['has_covid'];
			if (isset($this->user['parent_id'])) {
				$data['parent_id'] = $this->user['parent_id'];
			}else{
				$data['parent_id'] = '0';
			}
			$data['report_id'] = $this->session->userdata('report_id');
			//$data['clinic_id'] = 2;
			$data['place_type'] = $this->session->userdata('place_type');
			//_pre($data);

			$api_key = $this->user['api_key'];
			$method = 'POST';

			$response = api_call($url, $data ,$api_key,$method);
			$response = json_decode($response);
			//echo $url;
			//_pre($response);
			if($response->status == 1){
				redirect(base_url('select-members'), 'refresh');
			}else{
				redirect(base_url('booking-edit-member/').$id, 'refresh');
			}


		}else{
			$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
			redirect(base_url('new-booking'));
		}
	}

	public function booking_member_remove($user_id, $booking_id){

		$id = $this->user['user_id'];
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$url = base_url().'api/customer/remove_selected_member';
		$data['user_id'] = $id;
		$data['booking_id'] = $booking_id;
		
		$response = api_call($url, $data ,$api_key,$method);
		$response = json_decode($response);

		//_pre($response);
		if($response->status == TRUE){
			redirect(base_url('select-members'), 'refresh');
		}else{
			redirect(base_url('select-members'), 'refresh');
		}

		//_pre($data);
	}
	
	public function save_booking_session(){
		$request = $_POST;
		$this->session->set_userdata('report_id', $request['report_id']);
		$this->session->set_userdata('purpose_id', $request['purpose_id']);
         
        if(isset($request['postcode'])){
			$this->session->set_userdata('postcode', $request['postcode']);
		}
        if(isset($request['visiting_country'])){
			$this->session->set_userdata('visiting_country', $request['visiting_country']);
		}
        if(isset($request['is_visiting_country'])){
			$this->session->set_userdata('is_visiting_country', $request['is_visiting_country']);
		}
        if(isset($request['res_add'])){
			$this->session->set_userdata('res_add', $request['res_add']);
		}
        if(isset($request['pickup_date'])){
			$this->session->set_userdata('pickup_date', $request['pickup_date']);
		}

        if(isset($request['ukshipping_postcode'])){
			$this->session->set_userdata('ukshipping_postcode', $request['ukshipping_postcode']);
		}
		if(isset($request['residency_flag'])){
			$this->session->set_userdata('residency_flag', $request['residency_flag']);
		}
		if(isset($request['shipping_method'])){
			$this->session->set_userdata('shipping_method', $request['shipping_method']);
		}
		if(isset($request['return_date'])){
			$this->session->set_userdata('return_date', $request['return_date']);
		}
		if(isset($request['return_time'])){
			$this->session->set_userdata('return_time', $request['return_time']);
		}
		if(isset($request['is_excempted'])){
			$this->session->set_userdata('is_excempted', $request['is_excempted']);
		}
		if(isset($request['excempted_cat'])){
			$this->session->set_userdata('excempted_cat', $request['excempted_cat']);
		}

		if(isset($request['vaccination_status'])){
			$this->session->set_userdata('vaccination_status', $request['vaccination_status']);
		}
		if(isset($request['dest_country'])){
			$this->session->set_userdata('dest_country', $request['dest_country']);
		}

		if(isset($request['ukresident'])){
			$this->session->set_userdata('ukresident', $request['ukresident']);
		}
		if(isset($request['residency_postcode'])){
			$this->session->set_userdata('residency_postcode', $request['residency_postcode']);
		}
		if(isset($request['residency_address'])){
			$this->session->set_userdata('residency_address', $request['residency_address']);
		}
		if(isset($request['nonresidency_postcode'])){
			$this->session->set_userdata('nonresidency_postcode', $request['nonresidency_postcode']);
		}
		if(isset($request['nonukaddress1'])){
			$this->session->set_userdata('nonukaddress1', $request['nonukaddress1']);
		}
		if(isset($request['nonukaddress2'])){
			$this->session->set_userdata('nonukaddress2', $request['nonukaddress2']);
		}
		if(isset($request['nonukcity'])){
			$this->session->set_userdata('nonukcity', $request['nonukcity']);
		}
		if(isset($request['nonukcountry'])){
			$this->session->set_userdata('nonukcountry', $request['nonukcountry']);
		}
		if(isset($request['res_add'])){
			$this->session->set_userdata('res_add', $request['res_add']);
		}

		if(isset($request['place_type'])){
			$this->session->set_userdata('place_type', $request['place_type']);
		}

		if(isset($request['country_id'])){
			$this->session->set_userdata('country_id', $request['country_id']);
		}

		if(isset($request['test_date'])){
			$this->session->set_userdata('test_date', $request['test_date']);
		}
		if(isset($request['test_time'])){
			$this->session->set_userdata('test_time', $request['test_time']);
		}
		//for display purpose
		if(isset($request['place_of_test_name'])){
			$this->session->set_userdata('place_of_test_name', $request['place_of_test_name']);
		}
		$this->session->set_userdata('purpose_of_test_name', $request['purpose_of_test_name']);
		$this->session->set_userdata('test_type_name', $request['test_type_name']);
		$this->session->set_userdata('test_price', $request['test_price']);
			
	
		if(isset($request['departure_date'])){
			$this->session->set_userdata('departure_date', $request['departure_date']);
			$this->session->set_userdata('departure_time', $request['departure_time']);
		}
		

		echo json_encode(array('status' => TRUE));
	}

	public function save_hospital_id($clinic_id){
		$this->session->set_userdata('clinic_id', $clinic_id);
		redirect(base_url('select-members'), 'refresh');
	}

	public function is_member_selected(){
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$purpose_id = $this->session->userdata('purpose_id');

		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{

		}

		if($url != ''){
			$id = $this->user['user_id'];
			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);

			if(isset($response->data) && count($response->data) > 0){
				$status = false;
				foreach ($response->data as $key => $value) {
					if($value->booking_id != ''){
						$status = true;
						break;
					}
				}
				if($status == true){
					echo json_encode( array('status' => TRUE, 'data' => $response ) );
				}else{
					echo json_encode( array('status' => false, 'data' => $response ) );
				}
				
			}else{
				echo json_encode( array('status' => FALSE ) );
			}
		}else{
			echo json_encode( array('status' => FALSE ) );
		}
		
	}

	public function time_slot(){
		webRender('booking/time_slot');
	}
	
	public function review_confirm(){

		$api_key = $this->user['api_key'];
		$method = 'POST';

		//_pre($_SESSION);

		$purpose_id = $this->session->userdata('purpose_id');

		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{
			$this->session->set_flashdata('error', 'Booking session expired.');
			redirect(base_url('new-booking'), 'refresh');
		}

		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);
		$data['members'] = array();
		if($response->status == 1){
			$booking_id = '';
			foreach ($response->data as $key => $value) {
				if($value->booking_id != ''){
					$booking_id = $value->booking_id;
					break;
				}
			}
			if($booking_id == ''){
				$this->session->set_flashdata('error', 'Booking session expired.');
				redirect(base_url('new-booking'), 'refresh');
			}
			$data['members'] = $response->data;
		}else{
			redirect(base_url('select-members'), 'refresh');
		}

		//_pre($data);

		webRender('booking/review_confirm', $data);
	}
	
	public function get_payment_cards(){

		$api_key = $this->user['api_key'];
		$method = 'POST';
		$id = $this->user['user_id'];
		// $output_data = array();
		// $output_data['booking_id'] = 1;
		// $output_data['booking_reference_id'] = 1;
		// $output_data['customer_id'] =$customer_id = 1;

		// webRender('booking/payment', $output_data);

		$purpose_id = $this->session->userdata('purpose_id');

		if($purpose_id == '1'){
			$url = base_url().'api/customer/members_list';
		}else if($purpose_id == '4'){
			$url = base_url().'api/customer/members_list_fitoneway';
		}else if($purpose_id == '5'){
			$url = base_url().'api/booking/members_list_intarrival';
		}else if($purpose_id == '3'){
			$url = base_url().'api/booking/members_list_fitroundtrip';
		}else{
			$this->session->set_flashdata('error', 'Booking session expired.');
			redirect(base_url('new-booking'), 'refresh');
		}

		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);
		$data['members'] = array();
		if($response->status == 1){
			$members = $response->data;
			$total_member = count($members);
			$report_price = $this->session->userdata('test_price');
			$sub_total = $total_member * $report_price;
			$total_price = $sub_total + TAX_PRICE;

			//_pre($members);

			foreach ($members as $key => $value) {
				if($value->booking_id != ''){
					$booking_id = $value->booking_id;
					continue;
				}
			}

			if(isset($booking_id) && $booking_id != ''){

				$booking_data = array(
						'user_id' => $id,
						'report_price' => $report_price,
						'tax_price' => TAX_PRICE,
						'member_details' => '',
						'total_price' => $total_price,
						'booking_id' => $booking_id
					);

				//_pre($booking_data);

				$url = base_url().'api/booking/add_booking';
				$response = api_call($url,$booking_data,$api_key,$method);
				$response = json_decode($response);
				if($response->status == TRUE){
					//_pre($response);
					$output_data['booking_id'] = $response->data->booking_id;
					$output_data['booking_reference_id'] = $response->data->booking_reference_id;
					$output_data['customer_id'] =$customer_id = $response->data->customer_id;

					$this->load->helper('strp_helper');
					//$cards = get_payment_card('cus_KC9l2j2LGceCpT');
					$cards = get_payment_card($customer_id);

					if($cards['status'] == 1 && isset($cards['data'])){
						$output_data['cards'] = $cards['data'];
					}

					//_pre($output_data);

					webRender('booking/payment', $output_data);

				}else{
					redirect(base_url('select-members'), 'refresh');
				}							

			}else{
				$this->session->set_flashdata('error', 'Booking session has been expired' );
				redirect(base_url('new-booking'));
			}
			
		}else{
			$this->session->set_flashdata('error', 'Booking session has been expired.');
			redirect(base_url('select-members'), 'refresh');
		}

	}

	public function book(){
		$request = $_POST;

		$this->load->helper('strp_helper');
		$sub_total = $this->session->userdata('test_price');
		$total_price = $sub_total + TAX_PRICE;

		$transact = array();
        $transact['description'] = "Web Booking id : ".$request['booking_reference_id'];
        $transact['amount'] = $total_price * 100;
        $transact['customer'] = $request['customer_id'];
        $transact['card'] = $request['payment_card'];
        $paymentResponse = addCharge($transact);

        if( isset($paymentResponse['status']) && $paymentResponse['status'] == 1){

        	$api_key = $this->user['api_key'];
			$method = 'POST';

			$url = base_url().'api/booking/booking_status';

			$data = array(
				'booking_id' => $request['booking_id'],
				'payment_status' => 1
			);
			$response = api_call($url,$data,$api_key,$method);
			$response = json_decode($response);

			echo json_encode(array('status' => TRUE, 'booking_reference_id' => $request['booking_reference_id']));

        }else{
        	$error = $paymentResponse['error']['message'];
        	echo json_encode(array('status' => FALSE, 'message' => $error));
        }
		
	}
	
	public function add_card(){
		$request = $_POST;
		$this->load->helper('strp_helper');
		$month_year = explode('-', $request['expire']);

		$card = array(
			'name' => trim($request['name']),
			'card_number' => trim($request['card_number']),
			'month' => $month_year[0],
			'year' =>  $month_year[1],
			'cvc_number' => trim($request['cvc_number'])
			);

		$result = create_token($card);
		if($result['status'] == true){

			$token = $result['token'];
			$api_key = $this->user['api_key'];
			$method = 'POST';
			$id = $this->user['user_id'];

			$purpose_id = $this->session->userdata('purpose_id');

			if($purpose_id == '1'){
				$url = base_url().'api/customer/members_list';
			}else if($purpose_id == '4'){
				$url = base_url().'api/customer/members_list_fitoneway';
			}else if($purpose_id == '5'){
				$url = base_url().'api/booking/members_list_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/members_list_fitroundtrip';
			}else{
				echo json_encode(array('status' => FALSE, 'message' => 'Booking session has been expired12'));
			}

			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);
			if($response->status == 1){
				$members = $response->data;
				
				foreach ($members as $key => $value) {
					if($value->id == $id){
						$customer_id = $value->stripe_key;
						break;
					}
				}

				if(isset($customer_id) && $customer_id != ''){
					
					$card = add_payment_card($customer_id, $token);
					$card_id = $card['data']['id'];
					$last4 = $card['data']['last4'];
					echo json_encode(array('status' => TRUE, 'card_id' => $card_id, 'last4' => $last4, 'card' => $card));

				}else{
					echo json_encode(array('status' => FALSE, 'message' => 'Booking session has been expired34'));
				}

			}
			
		}else{
			echo json_encode(array('status' => FALSE, 'message' => $result['error']['message']));
		}


		
	}
	
	public function search_map(){
		webRender('booking/search-map');
	}
}
