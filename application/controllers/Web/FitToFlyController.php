<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class FitToFlyController extends MY_Controller {
	function __construct() {
		parent::__construct();

	}
	public function clinic_detail() {
		webRender('fit_to_fly/clinic_detail');
	}

	public function third_step(){
		webRender('fit_to_fly/step_03');
	}
	
	public function second_step(){
		webRender('fit_to_fly/step_02');
	}

	public function search_map(){
		webRender('fit_to_fly/search-map');
	}
	
	public function search(){
		webRender('fit_to_fly/search');
	}
	
	public function scan_document(){
		webRender('fit_to_fly/scan_document');
	}
	
	public function complete_your_registration(){
		webRender('fit_to_fly/complete_your_registration');
	}
	
	public function member_list(){
		webRender('fit_to_fly/member_list');
	}
	
	public function review_confirm(){
		webRender('fit_to_fly/review_confirm');
	}
	
	public function payment(){
		webRender('fit_to_fly/payment');
	}
	
	public function add_card(){
		webRender('fit_to_fly/add_card');
	}
	
	public function international_search(){
		webRender('fit_to_fly/international_search');
	}
	
	public function edit_detail(){
		webRender('fit_to_fly/edit_detail');
	}

	public function edit_profile(){
		webRender('fit_to_fly/edit_profile');
	}
}
