<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeController extends MY_Controller {
	function __construct() {
		parent::__construct();
		// if(!$this->ion_auth->is_admin()) {
		// 	redirect('/auth/login');
		// }
        // $this->load->model('User');

	}
	public function index() {
		
		//$data['user'] = User::count();		
		webRender('index');
	}

	public function services()
	{
		echo phpinfo(); exit();
		webRender('services');
	}

	public function about_us($user_id = null)
	{
		$data['user_id'] = $user_id;
		$this->load->view('web/pages/content/about-us',$data);
	}

	public function contact_us()
	{
		webRender('contact-us');
	}

	public function privacy_policy()
	{
		webRender('privacy-policy');
	}

	public function terms_conditions()
	{
		webRender('terms-and-conditions');
	}
	public function test_email()
	{
		// webRender('terms-and-conditions');
		$this->load->view('email/hospital_register');

	}

	public function data_privacy($user_id = null){
		$data['user_id'] = $user_id;
		$this->load->view('web/pages/content/data_privacy',$data);
	}
	
	public function registration_consent($user_id = null){
		$data['user_id'] = $user_id;
		$this->load->view('web/pages/content/registration_consent',$data);
	}

	public function accept_terms_condition(){
		$request = $this->input->post();
		if(!empty($request) && isset($request['user_id']) && !empty($request['user_id'])){
			$this->db->set("is_give_consent","1");
			$this->db->where("id",$request['user_id']);
			$this->db->update("users");
		}
		print json_encode(array("status"=>true));
	}

	public function privacy($user_id = null){
		$data['user_id'] = $user_id;
		$this->load->view('web/pages/content/privacy',$data);
	}
}
