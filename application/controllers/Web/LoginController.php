<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LoginController extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User');
		$this->load->library(['ion_auth','form_validation']);
		$this->user = $this->session->userdata('members');
	}
	public function index() {
		// _pre($this->session->get_userdata());
		if ($this->ion_auth->logged_in()) {
			redirect('home');
		} 
		webRender('sign-in');
	}

	public function signup() {
		$request = $this->input->post();
		if (isset($request) && !empty($request)) { 
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_error_delimiters('<label class="text-danger">', '</label>');
			if ($this->form_validation->run() == TRUE){
				/* $email = $request['email'];
				$fullname = rtrim(ltrim($request['name']));
				$password = $request['password'];
				$post_data['active'] = 0;
				$post_data['full_name'] = $fullname;
				$post_data['phone'] = $request['phone'];
				$post_data['country_code'] = $request['phone_phoneCode'];
				$id = $this->ion_auth->register($email, $password, $email, $post_data, [2], 0); */
				
				$request['phone_phoneCode'] = "+".$request['phone_phoneCode'];
				$post_data['phone'] = $request['phone'];
				$post_data['country_code'] = $request['phone_phoneCode'];
				$post_data['email'] = $request['email'];

				$url = base_url().'api/auth/send_otp';
				$response = api_call($url,$post_data,"","POST");
				$response = json_decode($response);
				//_pre($response);
				if(isset($response->status) && $response->status == 1){
					$request['otp'] = $response->otp;
					$this->session->set_userdata("register_data",$request);
					$this->session->set_flashdata('success', $response->message);
					//$this->session->set_flashdata('success', 'Registration successfully. Please check your email for account activation after registration ');
					redirect('otp-verify');
				}else{
					$this->session->set_flashdata('error', $response->message);
					redirect('signup');
				}
			}
		}
		webRender('sign-up');
	}

	public function verification_success(){
		$this->load->view('web/pages/verification-success');
	}


	public function otp_verify(){
		$request = $this->input->post();
		if(isset($request) && !empty($request)){
			$register_session = $this->session->userdata("register_data");
			if(isset($register_session) && !empty($register_session)){
				if($request['otp'] == $register_session['otp']){
					$email = $register_session['email'];
					$fullname = rtrim(ltrim($register_session['name']));
					$password = $register_session['password'];
					$post_data['active'] = 0;
					$post_data['full_name'] = $fullname;
					$post_data['phone'] = $register_session['phone'];
					$post_data['country_code'] = $register_session['phone_phoneCode'];
					$id = $this->ion_auth->register($email, $password, $email, $post_data, [2], 0);
					$this->session->unset_userdata('register_data');
					$this->session->set_flashdata('success', 'Registration successfully. Please check your email for account activation after registration ');
					redirect('login');
				}else{
					$this->session->set_flashdata('error', "Invalid OTP");
					redirect('otp-verify');
				}
				
			}else{
				$this->session->set_flashdata('error', "Something is wrong. please try again");
				redirect('signup');
			}
		}
		webRender('otp_verify');
	}

	public function resend_otp(){
		$register_session = $this->session->userdata("register_data");
		$post_data['phone'] = $register_session['phone'];
		$post_data['country_code'] = $register_session['phone_phoneCode'];
		$post_data['email'] = $register_session['email'];

		$url = base_url().'api/auth/send_otp';
		$response = api_call($url,$post_data,"","POST");
		$response = json_decode($response);
		if(isset($response->status) && $response->status == 1){
			$register_session['otp'] = $response->otp;
			$this->session->set_userdata("register_data",$register_session);
			$this->session->set_flashdata('success', $response->message);
			redirect('otp-verify');
		}else{
			$this->session->set_flashdata('error', $response->message);
			redirect('signup');
		}
	}

	public function activate_account($code = null){
		if(!empty($code)){
			if(User::where("remember_token",$code)->exists()){
				User::where("remember_token",$code)->update(array("remember_token"=>null));
				$this->session->set_flashdata('success', 'Account activate successfully');
				redirect('login');
			}else{
				$this->session->set_flashdata('error', 'Activation Fail');
				redirect('login');
			}
		}else{
			$this->session->set_flashdata('error', 'Activation Fail');
			redirect('login');
		}
	}
	
	public function forgot_password() {
	/* 	$request = $this->input->post();
		if (isset($request) && !empty($request)) {    	
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			$this->form_validation->set_error_delimiters('<label class="text-danger">', '</label>');
			if ($this->form_validation->run() == TRUE){
				$email = $request['email'];
				if(User::where("email",$email)->exists()){
					
					$forgot_token = bin2hex(random_bytes(20));
					User::where("email",$email)->update(array("forgotten_password_code"=>$forgot_token));
					$email_var_data["link"] = base_url('resets-password/'). $forgot_token;
					$message = $this->load->view('email/forgot_password',$email_var_data,true);
					$mail = send_mail($email,'Forgotten Password Verification', $message);
					$this->session->set_flashdata('success', 'Reset Link Sent');
					redirect('login');
				}else{
					$this->session->set_flashdata('error', 'No record of that email address.');
					redirect('forgot-password');
				}
				
			}
		} */
		webRender('forgot-password');
	}

	public function edit_profile(){
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
		if(!check_data("tokens",array("token"=>$this->user['api_key']))){
			redirect(base_url().'auth/logout/members');
		}
		$user_id = $this->user['user_id'];

		$request = $_POST;
		if (isset($request) && !empty($request)) { 
			//_pre($request);
			$post_data['user_id'] = $user_id;
			$post_data['full_name'] = $request['full_name'];
			$post_data['email'] = $request['email'];
			$post_data['address'] = $request['address'];
			$post_data['city'] = $request['city'];
			$post_data['postal_code'] = $request['postal_code'];
			$post_data['ethnicity'] = $request['ethnicity'];
			$post_data['nhs_number'] = $request['nhs_number'];
			$post_data['date_of_birth'] = $request['date_of_birth'];
			$post_data['passport'] = $request['passport'];
			//$data['profile_picture'] = $request['profile_picture'];
			$post_data['gender'] = $request['gender'];
			$post_data['nationality'] = $request['nationality'];

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
		        $folder = "assets/images/users/";
		       	s3Upload($url, $folder);
		        $image = S3_URL.$url;
		        $post_data['profile_picture'] = $image;
            }

			$api_key = $this->user['api_key'];
			$method = 'POST';

			$url = base_url().'api/customer/profile_update';
			
			$response = api_call($url, $post_data ,$api_key,$method);
			$response = json_decode($response);
			//print_r($response); exit;
			if($response->status == TRUE){
				$data['message'] = $response->message;
				$this->session->set_flashdata('success', $data['message']);

				redirect(base_url('edit-profile'), 'refresh');
			}else{
				redirect(base_url('edit-profile'), 'refresh');
			}
			//_pre($post_data);
			
		}

		$purpose_type_url = base_url().'api/customer/getprofile';
		$api_key = $this->user['api_key'];
		$method = 'POST';
		
		
		$member_data = api_call($purpose_type_url,array("user_id"=>$user_id), $api_key, $method);
		$member_data = json_decode($member_data, true);

		if($member_data['status'] != TRUE){
			redirect('/login');
		}

		$data['member_detail'] = $member_data['data'];

		$method1 = 'GET';
		$ethnicitylist_url = base_url().'api/customer/ethnicitylist';
		$ethnicitylist = api_call($ethnicitylist_url,[], $api_key, $method1);
		$ethnicitylist = json_decode($ethnicitylist, true);

		$nationality_url = base_url().'api/customer/nationalitylist';
		$nationality_list = api_call($nationality_url,[], $api_key, $method1);
		$nationality_list = json_decode($nationality_list, true);

		$data['nationality'] = $nationality_list['data'];
		$data['ethnicity'] = $ethnicitylist['data'];
		webRender('profile/edit_profile', $data);
	}
	
	public function change_password(){
		webRender('profile/change-password');
	}
	
	public function reset_password($code=null){
		$request = $this->input->post();
		if (isset($request) && !empty($request)) {    
			$code = $request['code'];
			$this->form_validation->set_rules('new_password', 'New Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
			$this->form_validation->set_error_delimiters('<label class="text-danger">', '</label>');
			if ($this->form_validation->run() == TRUE){
				
			}else{
				$data['code'] = $code;
				webRender('profile/reset-password',$data);
			}
		}else{
			if(User::where("forgotten_password_code",$code)->exists()){
				$data['code'] = $code;
				webRender('profile/reset-password',$data);
			}else{
				$this->session->set_flashdata('error', 'Invalid Request');
				redirect('forgot-password');
			}
		}
	}
	
	public function clinic_registration(){
		webRender('clinic-registration');
	}
}
