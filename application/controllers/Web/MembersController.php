<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MembersController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
        $this->load->model('User');
        $this->member_api = base_url().'api/customer/';
        $this->user = $this->session->userdata('members');
        if(!check_data("tokens",array("token"=>$this->user['api_key']))){
			redirect(base_url().'auth/logout/members');
		}
	}
	public function index() {
		
		$user_data = $this->session->userdata('members');
		$member_list_url =  $this->member_api.'members_list';
		$api_key = $this->user['api_key'];
		$method = 'POST';
		$this->session->unset_userdata('purpose_id');


		$my_member_list = api_call($member_list_url,array("user_id"=>$user_data['user_id'], "booking_key" => 'history'), $api_key, $method);
		$my_member_list = json_decode($my_member_list, true);
		//_pre($my_member_list);
		$data['member_list'] = $my_member_list;

		webRender('members/member_list', $data);
	}

	public function add_member(){
		$api_key = $this->user['api_key'];
		$id = $this->user['user_id'];
		$jumio_member_data = array();
		if($this->session->userdata("jumio_member_data")){
			$jumio_member_data = $this->session->userdata("jumio_member_data");
		}
		if(isset($_POST) && !empty($_POST)){
			$request = $_POST;
			// _pre($request);

			$url = base_url().'api/customer/addnewmember';

			$data['full_name'] = $request['full_name'];
			$data['email'] = $request['email'];
			$data['city'] = $request['city'];
			$data['is_homeaddress'] = $request['is_homeaddress'];
			$data['postcode'] = $request['postcode'];
			$data['is_excempted'] = $request['is_excempted'];
			$data['excempted_cat'] = $request['is_excempted'] == 'yes' ? $request['excempted_cat'] : '';

			if(isset($request['is_homeaddress']) && $request['is_homeaddress'] == 'home-address-yes'){
				$data['address'] = $request['address_dropdown'];
			}else{
				$data['address'] = $request['address'];
			}

			$data['nationality'] = $request['nationality'];
			$data['phone'] = $request['phone'];
			$data['gender'] = $request['gender'];
			$data['date_of_birth'] = $request['date_of_birth'];
			$data['ethnicity'] = $request['ethnicity'];
			$data['passport'] = $request['passport'];
			$data['symptoms'] = $request['symptoms'];
			$data['vaccine_status'] = $request['vaccine_status'];
			$data['has_covid'] = $request['has_covid'];
			$data['parent_id'] = $id;
			$data['is_editprofile'] = 1;

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $img_url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $img_url);
		        $folder = "assets/images/users/";
		       	s3Upload($img_url, $folder);
		        $image = S3_URL.$img_url;
		        $data['profile_picture'] = $image;
	        }

			//_pre($data);
			if(isset($request['vaccine_id']) && !empty($request['vaccine_id'])){
				$data['vaccine_id'] = $request['vaccine_id'];
			}
			$method = 'POST';
			$response = api_call($url, $data ,$api_key,$method);
			$response = json_decode($response);

			if($response->status == 1){
				if($response->data->active == '1'){
					$this->session->set_flashdata('success', 'Member details updated successfully.' );
				}else{
					$this->session->set_flashdata('success', 'Account activation email sent successfully. Verify email to add as member.' );
				}
				redirect(base_url('members'), 'refresh');
				
			}else{
				redirect(base_url('members/add-member'), 'refresh');
			}

		}
		$method = 'GET';

		$url = base_url().'api/customer/nationalitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['nationalitylist'] = $response->data;

		$url = base_url().'api/customer/ethnicitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['ethnicitylist'] = $response->data;

		$url = base_url().'api/customer/excempted_categories';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['excempted_categories'] = $response->data;

		$url = base_url().'api/countries/list';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['countries_list'] = $response->data;
		$data['certified_vaccines_list'] = certified_vaccines_list($api_key);
		$data['title'] = 'Add New Member';
		$data['jumio_member_data'] = $jumio_member_data;
		webRender('members/add_member', $data);
	}

	public function edit_member($member_id = NULL){

		$api_key = $this->user['api_key'];
		$method = 'GET';

		$url = base_url().'api/customer/nationalitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['nationality_list'] = $response->data;

		$url = base_url().'api/customer/ethnicitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['ethnicitylist'] = $response->data;
		
		$url = base_url().'api/customer/excempted_categories';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['excempted_categories'] = $response->data;

		$url = base_url().'api/countries/list';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['countries_list'] = $response->data;
		$data['certified_vaccines_list'] = certified_vaccines_list($api_key);
		$url = base_url().'api/customer/members_list';
		$method = 'POST';
		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);

		if($response->status == 1){
			$members = $response->data;
			foreach ($members as $key => $value) {
				if($value->id == $member_id){
					$member = $value;
				}
			}
		}else{
			$this->session->set_flashdata('error', 'Something is wrong.');
			redirect(base_url('select-members'), 'refresh');
		}

		if(!isset($member) || empty($member)){
			redirect(base_url('select-members'), 'refresh');
		}else{
			if($member->is_homeaddress == ''){	
				$member->is_homeaddress = 'no';	
			}
			$data['member'] = $member;
		}

		$data['title'] = 'Edit Member';
		webRender('members/add_member', $data);

		
	}


	public function add(){

		$api_key = $this->user['api_key'];
		$id = $this->user['user_id'];
		$jumio_member_data = array();
		if($this->session->userdata("jumio_member_data")){
			$jumio_member_data = $this->session->userdata("jumio_member_data");
		}
		if(isset($_POST) && !empty($_POST)){
			$request = $_POST;
			// _pre($request);

			$url = base_url().'api/customer/addnewmember';

			$session_data = $this->session->userdata("input_data");
			$purpose_id = $this->session->userdata('purpose_id');
			if(!$this->session->userdata('purpose_id')){	
				$purpose_id = $session_data['purpose_id'];	
			}

			if(isset($purpose_id) && $purpose_id != ''){
				$place_type = $this->session->userdata('place_type');
			}

			$data['full_name'] = $request['full_name'];
			$data['email'] = $request['email'];
			$data['city'] = $request['city'];
			$data['is_homeaddress'] = $request['is_homeaddress'];
			$data['postcode'] = $request['postcode'];

			if(isset($request['is_homeaddress']) && $request['is_homeaddress'] == 'home-address-yes'){
				$data['address'] = $request['address_dropdown'];
			}else{
				$data['address'] = $request['address'];
			}

			if($purpose_id == '1' && $place_type == '1'){
				$data['clinic_id'] = $this->session->userdata('clinic_id');
			}else if($purpose_id == '1' && $place_type == '2'){

				if(isset($request['is_shippingaddress']) && $request['is_shippingaddress'] == 'yes'){
					$data['shipping_postcode'] = $data['postcode'];
					$data['shipping_address'] = $data['address'];
				}else{
					$data['shipping_postcode'] = $request['shipping_postcode'];
					if($request['is_homeaddress'] == 'home-address-yes'){
						$data['shipping_address'] = $request['shipping_address_dropdown'];
					}else{
						$data['shipping_address'] = (isset($data['shipping_address']) ? $data['shipping_address']:'');
					}
				}
			}else if($purpose_id == '4'){

				$url = base_url().'api/customer/addnewmember_fitoneway';

				$data['dest_country'] = $this->session->userdata('country_id');

				$departure_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('departure_date'));
				$data['departure_date'] = $departure_date_obj->format('Y-m-d');
				$data['departure_time'] = $this->session->userdata('departure_time');
				$data['clinic_id'] = $this->session->userdata('clinic_id');
				$data['excempted_cat'] = $request['excempted_cat'];
				$data['is_excempted'] = $request['is_excempted'];
				$data['travel_destination'] = $request['travel_destination'];
				//$data['date_of_travel'] = $request['date_of_travel'];

			}else if($purpose_id == '5'){

				$data['clinic_id'] = $this->session->userdata('clinic_id');

				if($this->session->userdata('postcode') != ""){
					$data['postcode'] = $this->session->userdata('postcode');
				}
				$data['postcode'] = $this->session->userdata('postcode');
				$data['visiting_country'] = $this->session->userdata('visiting_country');
				$data['is_visiting_country'] = $this->session->userdata('is_visiting_country');
				$data['res_add'] = $this->session->userdata('res_add');
				$data['pickup_date'] = $this->session->userdata('pickup_date');
				$data['ukshipping_postcode'] = $this->session->userdata('ukshipping_postcode');
				$data['resshipping_flag'] = $this->session->userdata('residency_flag');
				$data['residency_flag'] = $this->session->userdata('ukresident');

				if ($data['residency_flag'] == "1") {
					$data['residency_postcode'] = $this->session->userdata('residency_postcode');
					$data['residency_address'] = $this->session->userdata('residency_address');
				}else{
					$data['residency_postcode']="";
				
				$data['residency_postcode'] = $this->session->userdata('nonresidency_postcode');
				$data['uk_isolation_postcode'] = $this->session->userdata('uk_isolation_postcode');
				$data['uk_isolation_address'] = $this->session->userdata('uk_isolation_address');
				
				$data['residency_address'] = $this->session->userdata('nonukaddress1')." ".$this->session->userdata('nonukaddress2').", ".$this->session->userdata('nonukcity')." ".$this->session->userdata('nonukcountry');

				}
				$data['shipping_method'] = $this->session->userdata('shipping_method');
				if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "1") {
					$data['shipping_address'] = $this->session->userdata('uk_isolation_address');
					
				}else if ($data['shipping_method'] =="2" && $data['resshipping_flag'] == "2") {
					$data['shipping_address'] = $this->session->userdata('resshipping_address');
				}
				$data['return_date'] = $this->session->userdata('return_date');
				$data['return_time'] = $this->session->userdata('return_time');
				$data['is_excempted'] = $this->session->userdata('is_excempted');
				$data['excempted_cat'] = $this->session->userdata('excempted_cat');
				$data['vaccination_status'] = $this->session->userdata('vaccination_status');
				$data['dest_country'] = $this->session->userdata('dest_country');
				$data['ukresident'] = $this->session->userdata('ukresident');
				$data['nonresidency_postcode'] = $this->session->userdata('nonresidency_postcode');
				$data['nonukcity'] = $this->session->userdata('nonukcity');
				$data['nonukcountry'] = $this->session->userdata('nonukcountry');
				$data['nonukaddress1'] = $this->session->userdata('nonukaddress1');
		       
				$url = base_url().'api/booking/addnewmember_intarrival';
			}else if($purpose_id == '3'){

				$data['date'] = date('Y-m-d',strtotime($session_data['booking_date']));
				$data['time'] = date('H:i',strtotime($session_data['booking_time']));
				$data['report_id'] = $session_data['test_type'];
				$data['clinic_id'] = $session_data['clinic_id'];
				$data['dest_country'] = $session_data['country_id'];
				$data['return_country'] = $session_data['fit_to_fly_input_data']['return_country'];
				$data['is_visiting_country'] = $session_data['fit_to_fly_input_data']['is_visiting_country'];
				$data['visiting_country'] = $session_data['fit_to_fly_input_data']['visiting_country'];
				$data['second_report_id'] = $session_data['fit_to_fly_input_data']['test_type'];
				$data['transport_type'] = $session_data['fit_to_fly_input_data']['transport_type'];
				$data['transport_number'] = $session_data['fit_to_fly_input_data']['transport_number'];
				$data['shipping_method'] = $session_data['fit_to_fly_input_data']['shipping_method'];
				if($data['shipping_method'] == 2){
					$data['shippingaddress'] = $session_data['fit_to_fly_input_data']['shippingaddress'];
					$data['shippingpostal_code'] = $session_data['fit_to_fly_input_data']['shippingpostal_code'];
					$data['shipping_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['shipping_date']));
				}
				$data['return_date'] = date('Y-m-d',strtotime($session_data['fit_to_fly_input_data']['return_date']));
				$data['departure_date'] = date('Y-m-d',strtotime($session_data['departure_date']));
				$data['departure_time'] = date('H:i',strtotime($session_data['departure_time']));

				$url = base_url().'api/booking/addnewmember_fitroundtrip';
			}else{
				
			}

			if($purpose_id != 3){

				if ($this->session->userdata('test_date') !="") {
					$test_date_obj = DateTime::createFromFormat('d-m-Y', $this->session->userdata('test_date'));
					$data['date'] = $test_date_obj->format('Y-m-d');
				}else{
					$data['date'] ="";
				}
				$data['time'] = $this->session->userdata('test_time');
			}

			$data['nationality'] = $request['nationality'];
			$data['phone'] = $request['phone'];
			$data['gender'] = $request['gender'];
			$data['date_of_birth'] = $request['date_of_birth'];
			$data['ethnicity'] = $request['ethnicity'];
			$data['passport'] = $request['passport'];
			$data['symptoms'] = $request['symptoms'];
			$data['vaccine_status'] = $request['vaccine_status'];
			$data['has_covid'] = $request['has_covid'];
			$data['parent_id'] = $id;

			if($purpose_id != 3){
				$data['report_id'] = $this->session->userdata('report_id');
				$data['place_type'] = $this->session->userdata('place_type');
			}

			if(isset($purpose_id) && $purpose_id != ''){
				$data['is_editprofile'] = 0;
			}else{
				$data['is_editprofile'] = 1;
			}

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $img_url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $img_url);
		        $folder = "assets/images/users/";
		       	s3Upload($img_url, $folder);
		        $image = S3_URL.$img_url;
		        $data['profile_picture'] = $image;
	        }

			if(isset($request['vaccine_id']) && !empty($request['vaccine_id'])){
				$data['vaccine_id'] = $request['vaccine_id'];
			}
			//_pre($data);

			$method = 'POST';
			$response = api_call($url, $data ,$api_key,$method);
			$response = json_decode($response);

			//_pre($response);
			if($response->status == 1){
				$this->session->set_flashdata('success', 'Account activation email sent successfully. Verify email to add as member.' );
				redirect(base_url('select-members'), 'refresh');
				
			}else{
				redirect(base_url('members/add'), 'refresh');
			}

		}
		$method = 'GET';

		$url = base_url().'api/customer/nationalitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['nationalitylist'] = isset($response->data) ? $response->data : [];

		$url = base_url().'api/customer/ethnicitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['ethnicitylist'] = isset($response->data) ? $response->data : [];

		$data['certified_vaccines_list'] = certified_vaccines_list($api_key);

		$purpose_id = $this->session->userdata('purpose_id');
		if(!empty($purpose_id)){

			if($purpose_id == '5'){

				$url = base_url().'api/customer/excempted_categories';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['excempted_categories'] = $response->data;

				$url = base_url().'api/countries/list';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['countries_list'] = $response->data;

			}
		}
		$data['title'] = 'Add New Member';
		$data['purpose_id'] = $purpose_id;
		$data['jumio_member_data'] = $jumio_member_data;
		webRender('members/add_new_member', $data);
	}

	public function booking_edit_member($member_id = NULL,$is_review=0){
		$this->session->set_userdata('member_id', $member_id);
		//$this->session->set_flashdata('success', 'Account activate successfully');

		$session_data = $this->session->userdata("input_data");
		$purpose_id = $this->session->userdata('purpose_id');
		if(isset($session_data['purpose_id'])){
			$purpose_id = $session_data['purpose_id'];
		}

		if(!empty($purpose_id)){

			$data['purpose_id'] = $purpose_id;
			$api_key = $this->user['api_key'];
			$method = 'GET';

			// print_r($purpose_id); exit();

			$url = base_url().'api/customer/nationalitylist';
			$response = api_call($url,[],$api_key,$method);
			$response = json_decode($response);
			$data['nationality_list'] = $response->data;

			$url = base_url().'api/customer/ethnicitylist';
			$response = api_call($url,[],$api_key,$method);
			$response = json_decode($response);
			$data['ethnicitylist'] = $response->data;

			$data['certified_vaccines_list'] = certified_vaccines_list($api_key);

			if($purpose_id == '1'){
				$url = base_url().'api/customer/members_list';
			}else if($purpose_id == '4'){

				$url = base_url().'api/customer/excempted_categories';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['excempted_categories'] = $response->data;

				$url = base_url().'api/countries/list';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['countries_list'] = $response->data;

				$url = base_url().'api/customer/members_list_fitoneway';
			}else if($purpose_id == '5'){

				$url = base_url().'api/customer/excempted_categories';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['excempted_categories'] = $response->data;

				$url = base_url().'api/countries/list';
				$response = api_call($url,[],$api_key,$method);
				$response = json_decode($response);
				$data['countries_list'] = $response->data;

				$url = base_url().'api/booking/members_list_intarrival';
			}else if($purpose_id == '3'){
				$url = base_url().'api/booking/members_list_fitroundtrip';
			}else{
				$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
				redirect(base_url('new-booking'));
			}

			$method = 'POST';
			$id = $this->user['user_id'];
			$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
			$response = json_decode($response);
			//_pre($response);
			if($response->status == 1){
				$members = $response->data;
				foreach ($members as $key => $value) {
					if($value->id == $member_id){
						$member = $value;
					}
				}
			}else{
				$this->session->set_flashdata('error', 'Something is wrong.');
				redirect(base_url('select-members'), 'refresh');
			}
			
			if(!isset($member) || empty($member)){
				redirect(base_url('select-members'), 'refresh');
			}else{
				if($member->is_homeaddress == ''){	
					$member->is_homeaddress = 'no';	
				}
				$data['member'] = $member;
			}
			//_pre($url);
			$data['title'] = 'Edit Member';
			$data['is_review'] = $is_review;
			webRender('members/add_new_member', $data);

		}else{
			$this->session->set_flashdata('error', 'Something is wrong. Please select test purpose.');
			redirect(base_url('new-booking'));
		}
	}

	public function edit($member_id = NULL){
		//$this->session->set_flashdata('success', 'Account activate successfully');

		$api_key = $this->user['api_key'];
		$method = 'GET';

		$url = base_url().'api/customer/nationalitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['nationality_list'] = $response->data;

		$url = base_url().'api/customer/ethnicitylist';
		$response = api_call($url,[],$api_key,$method);
		$response = json_decode($response);
		$data['ethnicitylist'] = $response->data;

		$url = base_url().'api/customer/members_list';

		$method = 'POST';
		$id = $this->user['user_id'];
		$response = api_call($url,array("user_id"=> $id ),$api_key,$method);
		$response = json_decode($response);

		if($response->status == 1){
			$members = $response->data;
			foreach ($members as $key => $value) {
				if($value->id == $member_id){
					$member = $value;
				}
			}
		}else{
			$this->session->set_flashdata('error', 'Something is wrong.');
			redirect(base_url('members'), 'refresh');
		}

		if(!isset($member) || empty($member)){
			redirect(base_url('members'), 'refresh');
		}else{
			if($member->is_homeaddress == ''){	
				$member->is_homeaddress = 'no';	
			}
			$data['member'] = $member;
		}

		$data['title'] = 'Edit Member';
		webRender('members/add_new_member', $data);

		
	}

	public function save(){

		echo "save";
		exit;
	}

	public function delete($member_id = NULL){
		//$this->session->set_flashdata('success', 'Account activate successfully');

		$method = 'POST';
		$api_key = $this->user['api_key'];
		$id = $this->user['user_id'];
		$url = base_url().'api/customer/removemember';

		$response = api_call($url,array("user_id"=> $id, 'member_id' => $member_id ),$api_key,$method);
		$response = json_decode($response);

		if($response->status == 1){
			$this->session->set_flashdata('success', $response->message);
		}else{
			$this->session->set_flashdata('error', 'Something is wrong.');
			
		}
		redirect(base_url('members'));

	}

	public function get_addresses_by_postcode()
	{
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		$request = $_POST;
		$request['postcode'] = str_replace(" ","",$request['postcode']);
		if($request['postcode'] != ''){

			$url = 'https://api.getaddress.io/find/'.$request['postcode'].'?api-key=9LSFuRdkV068ieTkd41S1Q32641&expand=true';
			$method = 'GET';
			$api_key = '9LSFuRdkV068ieTkd41S1Q32641';

			$response = api_call($url,[],$api_key,$method);
			//_pre($response);
			$response = json_decode($response, true);
			
			if(isset($response['addresses']) && count($response['addresses']) > 0){
				$data['status'] = TRUE;
				$data['addresses'] = $response['addresses'];
			}else{
				$data['status'] = FALSE;
			}

		}else{
			$data['status'] = FALSE;
		}
		
		echo json_encode($data);
		return TRUE;

	}

	public function scan_documents(){
		$url = base_url()."/api/jumio/create_account";
		$api_key = $this->user['api_key'];
		$id = $this->user['user_id'];
		$method = 'POST';
		$response = api_call($url, array("user_id"=>$id) ,$api_key,$method);
		$response = json_decode($response);
		if($response->status == 1){
			$data['access_token'] = $response->access_token;
			$data['sdk_token'] = $response->sdk_token;
			$data['web_href'] = $response->web_href;
			$data['account_id'] = $response->account_id;
			$data['workflow_id'] = $response->workflow_id;
			webRender('members/add_member_scan_documents', $data);
		}else{
			redirect(base_url().'members');
		}
	}

	public function expire_token()
	{
		webRender('members/expire_token');
	}

	public function review_confirm($user_ids){
		// _pre($user_ids);
		webRender('booking/review_confirm');
	}

	public function search(){
		webRender('booking/search');
	}
	
	public function complete_your_registration(){
		webRender('booking/complete_your_registration');
	}

	public function member_list(){
		webRender('booking/member_list2');
	}
	
	public function time_slot(){
		webRender('booking/time_slot');
	}
	
	public function payment(){
		webRender('booking/payment');
	}
	
	public function add_cart(){
		webRender('booking/add_card');
	}
}
