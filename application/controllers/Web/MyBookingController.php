<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MyBookingController extends MY_Controller {
	function __construct() {
		parent::__construct();
		// if(!$this->ion_auth->is_admin()) {
		// 	redirect('/auth/login');
		// }
		if(!$this->ion_auth->is_member()) {
			redirect('/login');
		}
        $this->load->model('User');
        $this->booking_api = base_url().'api/booking/';
		$this->user = $this->session->userdata('members');
		if(!check_data("tokens",array("token"=>$this->user['api_key']))){
			redirect(base_url().'auth/logout/members');
		}
	}

	public function index() {
		$user_data = $this->session->userdata('members');
		$booking_list_url =  $this->booking_api.'list';
		$api_key = $this->user['api_key'];
		$method = 'POST';

		$my_booking_list = api_call($booking_list_url,array("user_id"=>$user_data['user_id'], "booking_key" => 'schedule'), $api_key, $method);
		$my_booking_list = json_decode($my_booking_list, true);
		
		//_pre($my_booking_list);
		
		$data['booking_list'] = $my_booking_list;

		$my_booking_list_history = api_call($booking_list_url,array("user_id"=>$user_data['user_id'], "booking_key" => 'history'), $api_key, $method);
		$my_booking_list_history = json_decode($my_booking_list_history, true);

		$data['booking_list_history'] = $my_booking_list_history;
		// _pre($data['booking_list_history']);

		webRender('my_booking/list', $data);
	}
	
	public function detail($id = '') {	
		$user_data = $this->session->userdata('members');
		$booking_list_url =  $this->booking_api.'booking_details';
		$api_key = $this->user['api_key'];
		$method = 'POST';
		
		$my_booking_detail = api_call($booking_list_url,array("booking_id"=>$id), $api_key, $method);
		$my_booking_detail = json_decode($my_booking_detail, true);
		
		$data['booking_detail'] = $my_booking_detail['data'];
		//_pre($data);

		webRender('my_booking/booking_detail', $data);
	}

	public function upload_plf($booking_id, $member_id) {	
		
		if(empty($_FILES['document']['name'])){
			$this->session->set_flashdata('error', 'PLF document is required.');
			redirect(base_url('my-booking/detail/').$booking_id);
		}

		$path_parts = pathinfo($_FILES["document"]["name"]);
		$type = $path_parts['extension'];
		if(strtolower($type) == "pdf"){
			$name = mt_rand(100000000, 999999999);
			$filename = $name . '.' . $type;
			$url = "assets/images/plf_document/" . $filename;
			if(!is_dir("assets/images/plf_document")){
				mkdir("assets/images/plf_document/");
			}
			move_uploaded_file($_FILES["document"]["tmp_name"], $url);
			$folder = "assets/images/plf_document/";
			s3Upload($url, $folder);
			$image = S3_URL.$url;
			$update_data['plf_document'] = $image;
			$this->db->update("booking_member",$update_data,array("booking_id"=>$request['booking_id'],"user_id"=>$request['member_id']));
			$this->session->set_flashdata('success', 'PLF document uploaded successfully.');
			redirect(base_url('my-booking/detail/').$booking_id);

		}else{
        	$this->session->set_flashdata('error', 'Only PDF file allowed.');
			redirect(base_url('my-booking/detail/').$booking_id);
		}

	}

	public function upload_kit($booking_id = '') {

		if(empty($_FILES['upload_kit']['name'])){
			$this->session->set_flashdata('error', 'Kit is required.');
			redirect(base_url('my-booking'));
		}
		$api_image_url =  IMAGE_UPLOAD_API_URL;
		$update_result_url =  $this->booking_api.'update_result';
		$user_data = $this->session->userdata('members');
		$api_key = $this->user['api_key'];

		$token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI";
		$method = 'POST';

		$type = explode('.', $_FILES["upload_kit"]["name"]);
		$type = strtolower($type[count($type) - 1]);
		$name = mt_rand(100000000, 999999999);
		$filename = $name . '.' . $type;
		if(!is_dir("assets/images/kits")){
				mkdir("assets/images/kits/");
		}
		$url = "assets/images/kits/" . $filename;
		move_uploaded_file($_FILES["upload_kit"]["tmp_name"], $url);
		$folder = "assets/images/users/";
		s3Upload($url, $folder);
		$image = S3_URL.$url;
		$post_data = [];
		$post_data['imageUrl'] = $image;
		$post_data['requestFor'] = 2;

		// $image_data = api_call($api_image_url,$post_data, $token, $method);

		//API call

		$CI = get_instance();
        
        $curl = curl_init();
        // $api_key = $CI->session->userdata('members')['api_key'];

        curl_setopt_array($curl, array(
          CURLOPT_URL => $api_image_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => $post_data,
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI',
            'Cookie: ci_session=e31mir92upckj5bikt91eul4rgbgjo3t'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);


        $image_data = $response;
        $image_data_array = json_decode($image_data, TRUE);


		//end API call

		$data = [];
		$data['booking_member_id'] = $_POST['booking_member_id'];
		$data['user_id'] = $user_data['user_id'];
		$data['status'] = $image_data_array['data']['stripResult'];
		$data['result_file_path'] = $image_data_array['data']['filePath'];
		$data['ref_number'] = $image_data_array['data']['refNumber'];
		$success_message = $image_data_array['data']['message'];

		// _pre($data);

		if($image_data_array['data']['stripResult'] == '1' || $image_data_array['data']['stripResult'] == '2'){
			$this->session->set_flashdata('success', $success_message);
			$update_result = api_call($update_result_url,$data, $api_key, $method);
		}else{
			$this->session->set_flashdata('error', 'Invalid kit.Please upload valid kit');
		}

		// _pre($update_result);

		redirect(base_url('my-booking'));

	}
}
