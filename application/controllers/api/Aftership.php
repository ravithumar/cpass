<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Aftership extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
	}
	public function track_data_post()
	{
		$this->form_validation->set_rules('tracking_no', 'Tracking No', 'required');
        // _pre($data1);
        //     CURLOPT_HTTPHEADER => array(
        //         "Host: ssapi.shipstation.com",
        //         "Authorization: Basic ".$username,
        //         "Content-Type: application/json"

        //     ),
        // ));
        // $response = curl_exec($curl);

        // curl_close($curl);

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['message'] = $this->validation_errors_response();
        }else{
            $req = $this->post();
            $data['tracking_data'] = array(); 
            // if(isset($get_data) && !empty($get_data)){
            try{
                $trackings = new AfterShip\Trackings($this->config->item("after_ship_api_key"));
                $response = $trackings->get("interlink-express", $req['tracking_no']);
                $data1 = array();
                if (isset($response['data']['tracking']['checkpoints']) && !empty($response['data']['tracking']['checkpoints'])) {
                    $data['tracking_data'] = $response['data']['tracking']['checkpoints'];
                    // foreach ($response['data']['tracking']['checkpoints'] as $key => $value) {
                        // _pre($value);
                        // $data1 = array_values($response['data']['tracking']['checkpoints']);
                        // $data1 = $value;

                    // array_push($data[,$value);
                    // }
                }else if(isset($response['data']['tracking']['tag']) && ($response['data']['tracking']['tag'] == "Pending")){
                    $data['status'] = true;
                    $data1['tag'] = $response['data']['tracking']['tag'];
                    $data1['location'] = "";
                    $data1['subtag_message'] = "";
                    $data1['checkpoint_time'] = "";                    
                    $data1['message'] = "Awaiting updates from the courier.";
                    array_push($data['tracking_data'], $data1);

                }
            }
            catch(Exception $e){
            } 
        }
        // _pre($response); 

        if (!empty($data['tracking_data'])) {
            $data['status'] = true;
            $data['statusCount'] = 9;
            // $data['tracking_data']['tag'] = "Pending";
            // $data['tracking_data']['message'] = "Awaiting updates from the courier.";
        }else{
            $data['status'] = false;
            $data['statusCount'] = 9;
            $data['tracking_data'] = array();
        }
        $this->response($data);
	}

    public function validation_errors_response() {
        $err_array=array();
        $err_str="";
        $err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));
        $err_str=ltrim($err_str,'|');
        $err_str=rtrim($err_str,'|');
        $err_str=str_replace('|', '', trim($err_str));
        return $err_str;
    } 
}
