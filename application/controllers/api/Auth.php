<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
class Auth extends REST_Controller {
	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Devices');
		$this->load->model('ion_auth_model');
		$this->load->library(['ion_auth', 'form_validation']);
	}
	public function login_post() {
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required');
	
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['message'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$type = "2";
			$login = $this->ion_auth->api_user_login($request['email'],$request['password'], $request['device_token'], $request['device_name'],FALSE,$type);
			// _pre($login);
			if ($login) {
				if ($login->active == 1) {
					$data['status'] = true;
					$user_details = $this->db->select('id,parent_id,email,phone,passport,nhs_number,profile_picture,gender,date_of_birth,address,postcode,city,ethnicity,full_name,nationality,is_give_consent',false)->from('users')->where('id', $login->id)->get()->row_array();
					$user_details['api_key'] = $this->_generate_key($login->id);
					$data['data'] = $user_details;
				} else {
					$data['status'] = false;
					$data['message'] = "This account is not activated!";
				}
			} else {
				$data['status'] = false;
				$data['message'] = "The email or password is not vaild.";
			}
		}
		$this->response($data);
	}

	public function logout_post() 
    {
        $this->form_validation->set_rules('user_id', 'user id', 'required|numeric');
        if ($this->form_validation->run() == FALSE) 
        {
            $response['status'] = false;
            $response['message'] = validation_errors_response();
        } 
        else 
        {
            $request = $this->input->post();

            $user = $this->db->get_where('users', array('id' =>$request['user_id'] ))->row_array();
         	if(!empty($user))
         	{
                $update_token['token'] = "";

                $this->db->update('tokens',$update_token,array('id' => $request['user_id']));

         		$response['status'] = true;
	            $query_result = $this->comman->update_record('users', ['remember_token' => NULL], $request['user_id']);

	            $response['message'] = 'Log out successfully';
         	}
         	else
         	{
         		$response['status'] = true;
         		$response['message'] = 'Invalid User';
         	}
           
        }

        $this->response($response);
    }
	public function userregister_post() {
		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
		$this->form_validation->set_rules('device_token', 'Device Token', 'required');
		$this->form_validation->set_rules('device_name', 'Device Name', 'required');
		$this->form_validation->set_rules('device_type', 'Device Type', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));
		$this->form_validation->set_rules('phone', 'phone', 'required');
		$this->form_validation->set_rules('country_code', 'country_code', 'required');
		
		// $this->form_validation->set_rules('phone', 'phone', 'required|min_length[10]|max_length[10]|is_unique[users.phone]', array('is_unique' => __('This phone is already exist!.')));
		$this->form_validation->set_rules('password', 'password', 'required|min_length[8]');
	
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['message'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$email = $request['email'];
			$fullname = rtrim(ltrim($request['full_name']));
			
			$password = $request['password'];
			unset($request['email']);
			unset($request['password']);
            $remember_token = bin2hex(random_bytes(20));
			$request['active'] = 0;
			$request['remember_token'] = $remember_token;

			$id = $this->ion_auth->register($fullname, $password, $email, $request, [2], 1);
			
			/*$string = 'user_'.$id;
			$qrcode_url = generate_qr_code($string);
			$unique_code = base64_encode($string);
			$update_user['qr_code'] = $qrcode_url;
			User::where("id",$id)->update($update_user);*/

			$user = User::find($id);
            Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'],'type' => $request['device_type']]);

			$user_details = $this->db->select('id,parent_id,email,phone,passport,nhs_number,profile_picture,gender,date_of_birth,address,postcode,city,ethnicity,full_name,nationality,qr_code',false)->from('users')->where('id', $user->id)->get()->row_array();
			$user_details['api_key'] = $this->_generate_key($user->id);
			$user_details['otp'] = "1234";

			$OTP = rand(1000, 9999);
			//$OTP = "1234";
			// print_r($check_mobile); exit();
			$arr['OTP'] = $OTP;

            $this->db->update('users',$arr,array('phone' => $request['phone']));

			$user = $this->db->get_where('users',['id'=>$user->id])->row();
            $email_var_data["link"] = base_url('activate-account/'). $remember_token;
            $email_var_data["full_name"] = $request['full_name'];
            $email_var_data["email_template"] = 'register_temp';

			$message = $this->load->view('email/register_temp',$email_var_data,true);

            //$mail = sendEmail('Verify account', $email, 'register_temp', $email_var_data);
			//send_mail($email,'Verify account', $message);
			// send_mail("Verify account",$user->email,'register_temp',$user);
			$data['status'] = true;
			$data['message'] = 'OTP verify successfully. Please verify your account from the verification link which we have sent on your email.';
			$data['data'] = $user_details;
		}
		$this->response($data);
	}
	
	public function send_otp_post() {
		$this->form_validation->set_rules('phone', 'Mobile Number', 'required');
		$this->form_validation->set_rules('country_code', 'Country Code', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['message'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$check_mobile = User::wherePhone($request['phone'])->first();
			$check_email = User::whereEmail($request['email'])->first();
			if (!empty($check)) {
			// if (!empty($check) && $check_email) {
				$data['status'] = false;
				$data['message'] = "This mobile is already registered!.";
			} 
			else 
			if (!empty($check_mobile)) {
				$data['status'] = false;
				$data['message'] = "This mobile number is already registered!.";
			}
			else if (!empty($check_email)) {
				$data['status'] = false;
				$data['message'] = "This email is already registered!.";
			}
			else {
				$OTP = rand(1000, 9999);
				//$OTP = "1234";
				$email_var_data['otp'] = $OTP;
				// print_r($check_mobile); exit();
				$update_token['otp'] = $OTP;

                // $this->db->update('users',$update_token,array('phone' => $request['phone']));

                $curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => 'https://rest.nexmo.com/sms/json',
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'POST',
				  CURLOPT_POSTFIELDS =>'{
				    "from":"C-Pass",
				    "text":"Your OTP verification code is '.$OTP.'. Do not share it with anyone. C-Pass",
				    "to":"'.$request['country_code'].$request['phone'].'",
				    "api_key":"a91d047f",
				    "api_secret":"CmzUP4CtHpqHdjua"
				}',
				  CURLOPT_HTTPHEADER => array(
				    'api: ',
				    'x-api-key: skos4kc4w4kws8gscgkoc4sow84ww0g08wskscgs',
				    'Content-Type: application/json'
				  ),
				));

				$response = curl_exec($curl);
				// _pre($response);
				// "to":"+919998221513",

				// $message = $this->load->view('email/otp',$email_var_data,true);
				// send_mail($request['email'],'C-pass OTP', $message);
				$data['status'] = true;
				$data['message'] = "OTP sent successfully on your registered mobile number.";
				$data['otp'] = $OTP;
			}
		}
		$this->response($data);
	}
	public function forgot_password_post() {
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['message'] = $this->validation_errors_response();
		} else {
			$email = $this->post('email');
			$user = User::whereEmail($email)->first();
			if (empty($user)) {
				$data['status'] = false;
				$data['message'] = "This email address is not registered";
			} else {

				$identity_column = $this->config->item('identity', 'ion_auth');
				$identity = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();

				$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

				if ($forgotten) {
					
					$email_var_data["link"] = base_url('reset-password/'). $forgotten['forgotten_password_code'];
				
					$message = $this->load->view('email/forgot_password',$email_var_data,true);
		
					//$mail = send_mail($forgotten['identity'],'Forgotten Password Verification', $message);

					$data['status'] = true;
					$data['message'] = "New password sent to your email address"; 
				} 
				else 
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					$data['status'] = false;
					$data['message'] = "Something Wrong";
				}
				
				// $user_meta['link'] = base_url('reset/password/' . urlencode($email) . '/' . time());

				// $email_var_data['link'] = base_url('reset/password/' . urlencode($email) . '/' . time());
				// $message = $this->load->view('email/forgot_password',$email_var_data,true);
				// send_mail($email,'Reset password', $message);

				
			}
		}
		$this->response($data);
	}
	public function _generate_key($uid) {
		do {
			// Generate a random salt
			$salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
			// If an error occurred, then fall back to the previous method
			if ($salt === FALSE) {
				$salt = hash('sha256', time() . mt_rand());
			}
			$new_key = substr($salt, 0, config_item('rest_key_length'));
		} while ($this->_key_exists($new_key));
		$this->_insert_key($new_key, ['level' => 1, 'ignore_limits' => 1, 'user_id' => $uid]);
		return $new_key;
	}
	private function _key_exists($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->count_all_results(config_item('rest_keys_table')) > 0;
	}
	private function _insert_key($key, $data) {
		$data[config_item('rest_key_column')] = $key;
		$data['date_created'] = function_exists('now') ? now() : time();
		$check_key = $this->db->get_where('tokens', array('user_id' => $data['user_id']))->row();
		if (empty($check_key)) {
			return $this->rest->db
				->set($data)
				->insert(config_item('rest_keys_table'));
		} else {
			$this->_update_key($key, $data['user_id']);
		}
	}
	private function _update_key($key, $uid) {
		return $this->rest->db
			->where('user_id', $uid)
			->update(config_item('rest_keys_table'), array('date_created' => time(), 'token' => $key));
	}
	private function _delete_key($key) {
		return $this->rest->db
			->where(config_item('rest_key_column'), $key)
			->delete(config_item('rest_keys_table'));
	}


	public function validation_errors_response() {
		$err_array=array();
    	$err_str="";
    	$err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));
    	$err_str=ltrim($err_str,'|');
    	$err_str=rtrim($err_str,'|');
    	$err_str=str_replace('|', '', trim($err_str));
        return $err_str;
	}

	// public function validation_errors_response() {
	// 	$err_array = array();
	// 	$err_str = "";
	// 	$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
	// 	$err_str = ltrim($err_str, '|');
	// 	$err_str = rtrim($err_str, '|');
	// 	$err_array = explode('|', $err_str);
	// 	$err_array = array_filter($err_array);
	// 	return $err_array;
	// }
	public function testdistance_get() {
		echo $this->getDistanceBetweenPointsNew('23.100878', '72.5347998', '23.0504568', '72.4594858');exit;
	}
	function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
		$theta = $longitude1 - $longitude2;
		$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$distance = acos($distance);
		$distance = rad2deg($distance);
		$distance = $distance * 60 * 1.1515;switch ($unit) {
		case 'Mi':break;case 'Km':$distance = $distance * 2.509322;
		}
		return (round($distance, 2));
	}
	public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
		// Calculate the distance in degrees
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));
		// Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
		switch ($unit) {
		case 'km':
			$distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
			break;
		case 'mi':
			$distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
			break;
		case 'nmi':
			$distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
		}
		return round($distance, $decimals);
	}

	public function change_password_post() {
		$this
			->form_validation
			->set_rules('old_password', 'Current Password', 'required');
		$this
			->form_validation
			->set_rules('new_password', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']');
		// $this->form_validation->set_rules('new_confirm_password', 'Confirm New Password', 'required');
		$this
			->form_validation
			->set_rules('user_id', 'user id', 'required');
		if ($this->form_validation->run() == false) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$id = $this->post('user_id');
			$user = $this
			->ion_auth
			->user($id)->row();
			if (empty($user)) {
				$data['status'] = false;
				$data['message'] = "User not exist.";
			
			} else {
				$response = $this
				->ion_auth
				->change_password($user->email, $this->post('old_password'), $this->post('new_password'), $id);
				if ($response) {
					$data['status'] = true;
					$data['message'] = "Your password changed successfully.";
	
				} else {
					$data['status'] = false;
					$data['message'] = strip_tags($this->ion_auth->errors());
				}
			}
		}
		$this->response($data);
	}
	public function init_get($type = '',$app_version = '', $user_id = ''){
		// _pre($user_id);
		$userId = $this->input->get('user_id');

		// if(isset($user_id) && $user_id !== '')
		// {
		// 	$user=$this->db->from('users')->where('id',$userId)->get()->row();
  //   		$response['is_give_consent'] = $user->is_give_consent;
		// }

		// $xApiKey=$this->input->get_request_header('x-api-key');
		$xApiKey=$this->input->get('user_id');
		if($xApiKey == '')
		{
			$response['loggedin'] = 0;
		}else{
			// $tokenDetails = $this->db->from('tokens')->where('token', $xApiKey)->get()->row();
			// if(!empty($tokenDetails)){
			//  	$userId=$tokenDetails->user_id;
	        $user = $this->db->from('users')->where('id',$userId)->get()->row();
    		$response['is_give_consent'] = !empty($user)?$user->is_give_consent:0;
    		// }else{
    		// 	$response['is_give_consent'] = 0;
    		// }
			$response['loggedin'] = 1;
		}
		$app_version = $this->get('app_version');
		$type = $this->get('type');
        $postFields['app_version'] = $app_version;        
        $type = ($type == 'ios')?'ios':'android';               
        $postFields['type'] = $type;
    	// print_r($type);
    	$response['tax_price'] = TAX_PRICE;


        $MaintenanceMode = (array)$this->db->get_where('appsetting',array('app_name' => 'maintenance_mode'))->row();
        $AppVersion = (array)$this->db->get_where('appsetting',array('app_name' => $type))->row();
        $current_version = (Int)str_replace('.', '',$AppVersion['app_version']);
        $app_version = (Int)str_replace('.', '', $app_version);
        
        if($MaintenanceMode['updates'] == 1){
            $response['status'] = false;
            $response['state'] = 2;
            $response['message'] = 'Server under maintenance, please try again after some time';
        }
        else if($app_version < $current_version && $AppVersion['updates'] == 0){
            $response['status'] = true;
            $response['state'] = 0;
            $response['message'] = 'Covid app new version available';
        }
        else if($app_version < $current_version && $AppVersion['updates'] == 1){
            $response['status'] = false;
            $response['state'] = 1;
            $response['message'] = 'Covid app new version available, please upgrade your application';
        }
        else{
            $response['status'] = true;
            $response['state'] = 3;
		    $response['message'] = '';
        }

        if($response['status'] == TRUE && $user_id != ''){
            $user = User::whereId($user_id)->where("active",1)->first();
            if (isset($user) && count($user) > 0){
                $response['status'] = true;
            }else{
                $response['status'] = true;
                $response['blocked'] = true;
                $response['message'] = 'Your account has been blocked';
            }
        }
        $this->response($response);
        // echo json_encode($response, JSON_UNESCAPED_UNICODE);
        // return TRUE;
    }
}
