<?php defined('BASEPATH') or exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

class Booking extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('Bookings');
        $this->load->model('BookingSession');
        $this->load->model('BookingSlots');
        $this->load->model('BookingVaccine');
        $this->load->model('BookingsMembers');
        $this->load->model('Category');
        $this->load->model('Clinic');
        $this->load->model('CustomerVaccine');
        $this->load->model('Report');
        $this->load->model('ReportType');
        $this->load->model('Slot');
        $this->load->model('User');
        $this->load->helper('strp_helper');
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);
        // date_default_timezone_set('Asia/Kolkata');
    }

    public function index_get() {
        $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        
        $request = $this->get();

        $data['booking_id'] = $request['booking_id'];
        $data['user_id'] = $request['user_id'];
        // https://dev.coronatest.co.uk/
        // print_r($request['user_id']); echo "<br/>"; print_r($request['booking_id']);
        $this->response($data);
        // exit();
    }
    public function update_result_post() {
        $this->form_validation->set_rules('booking_member_id', 'Booking Member Id', 'required');
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        $input = $this->input->post();
        if($input['status'] == 0 || $input['status'] == 1 || $input['status'] == 2){
            $this->db->select('booking_member.*,booking.type,booking.booking_no,booking_member.type as place_type,booking.total_price,booking.tax_price,booking.report_price,booking_member.id as booking_member_id,users.full_name, users.phone as user_phone, users.gender as user_gender, users.passport as user_passport, users.address as user_address, users.date_of_birth as user_dob, users.postcode as user_postcode, users.email as user_email, booking_slots.date as booking_date, booking_slots.date as booking_date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.booking_status as booking_status, booking_slots.id as slot_id,reports.name as test_name,users.full_name as clinic_user_name,booking.user_id as u_id');
            $this->db->from('booking');
            $this->db->join('booking_member','booking.id = booking_member.booking_id');
            //$this->db->join('hospital','booking_member.clinic_id = hospital.id','left');
            $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id','left');
            $this->db->join('reports','reports.id = booking_member.report_id');
            $this->db->join('users','booking_member.user_id = users.id');
            //$this->db->join('users as ud','ud.id = hospital.user_id');
            $this->db->where('booking_member.id',$input['booking_member_id']);
            $get_data = $this->db->get()->row_array();
            if(isset($get_data) && !empty($get_data)){

                if($input['status'] == 1){
                    $status = "positive";
                }
                else if($input['status'] == 2){
                    $status = "negative";
                }else{
                    $status = "invaild";
                }

                $value = $get_data;
                if($value['type'] == "international-arrival"){ 
                    if(!empty($value['pickup_date']) && $value['shipping_method'] == 1){
                        $booking_date = date('d/m/Y', strtotime($value['pickup_date']));
                    }else{
                        $booking_date = date('d/m/Y', strtotime($value['shipping_date']));
                    }
                }else{
                    $booking_date = date('d/m/Y', strtotime($value['booking_date']));
                }

                $result_date = date('d/m/Y');
                $user_dob = "";
                if(!empty($value['user_dob'])){
                    $user_dob = date('d/m/Y', strtotime($value['user_dob']));
                }
                $reference_member_id = $value['booking_no'];
               
                $clinic = $value['qr_code'];
                $qr_code = base_url('assets/images/logo/qr_code.png');
                if($clinic != '' && $clinic != null)
                {
                    $qr_code = $clinic;
                }

                $this->load->library('Pdf');
                $html = "";
                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);


                // set document information
                $pdf->SetCreator('C-Pass');
                $pdf->SetAuthor("C-Pass");
                $pdf->SetTitle('G-16 Covid Certificate');
                $pdf->SetSubject('G-16 Covid Certificate');
                $pdf->SetKeywords('TCPDF, PDF, C-Pass, Certificate,Covid Certificate');

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                $pdf->SetTopMargin(10);
                $pdf->setFooterMargin(10);

                $pdf->SetHeaderMargin(20);
                $pdf->SetFooterMargin(20);

                $pdf->SetAutoPageBreak(TRUE, 20);
                
                $pdf->SetDisplayMode('real', 'default');
                $pdf->SetFont('helvetica', '', 10);
                $pdf->AddPage('H');

                $pdf_logo = base_url('assets/images/logo/logo.png');
                $pdf_header_logo = base_url('assets/images/logo/logo-shadow.png');
                $header_logo = base_url('assets/images/logo/pdf-header.png');
                $pdf_check = base_url('assets/images/logo/clinic.png');

                ob_start(); 
                $header_table = '<table border=0>
                    <tr>
                        <td><img src="'.$header_logo.'" style="margin-top:20px"></td>
                        
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $header_table, 0, 1, 0, true, '', true);
                $pdf->Image($pdf_header_logo, 15, 13, 35, 35);
                $pdf->Image($qr_code, 150, 13, 35, 35);
                $pdf->SetTextColor(12, 91, 151);
                $pdf->Text(115, 41.7, $reference_member_id,false, false, true, 0, 1, 'L', false, '', 1, false, 'T', 'T');
                $pdf->Ln(14);
                $pdf->SetTextColor(30,30,30);
                
                $result_table = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <th><b>COVID-19 TEST RESULT CERTIFICATE</b></th>
                    </tr>
                    <tr>'; 
                    if (ucfirst($input['status']) == "Positive") {
                        $result_table .= '<th><b>RESULT: <span style="color:#FF0000">'.strtoupper($input['status']).'</span></b></th>';
                    }else{
                        $result_table .= '<th><b>RESULT: <span style="color:#008000">'.strtoupper($input['status']).'</span></b></th>';
                    }
                    $result_table .='</tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $result_table, 0, 1, 0, true, '', true);

                $pdf->Ln(5);

                $user_info_table = '<table border="1" style="font-size:14px;border-collapse: collapse;">
                    <tr nobr="true">
                        <th colspan="2"><span style="color:#0C5B97;font-weight:bold">NAME:</span> '.$value['full_name'].'</th>
                    </tr>
                    <tr nobr="true">
                        <th colspan="2"></th>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">SPECIMEN DATE:</span> '.$result_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">PASSPORT NUMBER:</span> '.$value['user_passport'].'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">REPORT DATE:</span> '. $booking_date.'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">DATE OF BIRTH:</span> '.$user_dob.'</td>
                    </tr>
                    <tr nobr="true">
                        <td><span style="color:#0C5B97;font-weight:bold">GENDER:</span> '.strtoupper($value['user_gender']).'</td>
                        <td><span style="color:#0C5B97;font-weight:bold">TEST TYPE:</span> '.$value['test_name'].'</td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $user_info_table, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $first_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p><b>'.ucfirst($status).' Result</b></p></td>
                    </tr>';
                    if (ucfirst($input['status']) == "Positive") {
                        
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Detected. You can use this certificate to
                            contact your travel agent/airline and work to confirm that based on their policies and
                            regulations you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';
                    }else{
                        $first_paragraph  .= '<tr> 
                            <td><p style="text-align:left">At the time of your test Covid-19 was Not Detected. You can use this certificate to 
                            contact your travel agent/airline and work to confirm that based on their policies and regulations 
                            you are permitted to either fly and or go back to work. If someone you live with had a test 
                            and Covid-19 was detected or you have been traced as a contact of someone who was
                            detected to have Covid-19, then you MUST still self-isolate immediately for 14 days.</p></td>
                        </tr>';
                    }

                $first_paragraph .= ' </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $first_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $second_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">For more advice, please visit www.gov.uk/coronavirus</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $second_paragraph, 0, 1, 0, true, '', true);
                
                $pdf->Ln(3);
                $third_paragraph = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left">This test was validated by G16 Enterprises Ltd, confirmation of these results will be recorded 
                        on our database. Your data will be held securely and will not be passed to any third parties. 
                        We are required to submit details of results if requested to do so by UK
                        government agencies and/or regulatory bodies such as Medicines and Healthcare
                        Regulatory Authority (MHRA) Department of Health & Social Care (DHSC) or Public Health
                        England (PHE)</p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $third_paragraph, 0, 1, 0, true, '', true);

                $pdf->Ln(3);
                $note = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td><p style="text-align:left"><b>This certificate shall not be reproduced except in full without approval from the issuer.</b></p></td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $note, 0, 1, 0, true, '', true);
                $sign = base_url('assets/images/logo/signature.png');
                $pdf->Ln(3);
                $footer = '
                <table border="0" cellpadding="2" cellspacing="1" border-collapse="collapse" align="center" style="font-size:14px">
                    <tr>
                        <td width="200" align="center">
                            G16 Enterprises Ltd <br/>
                            2 Kings Court <br/>
                            Horsham <br/>
                            West Sussex <br/>
                            RH13 9FZ <br/>
                            United Kingdom<br/>
                        </td>
                        <td width="50">
                            <img src="'.$pdf_logo.'" width="50" style="margin-bottom:0px">
                        </td>
                        <td width="50">
                            <img src="'.$sign.'" width="50" style="margin-top:0px">
                        </td>
                        <td width="250">
                            UKAS REGISTRATION: 22906 <br/><br/>
                            info@cpass.co.uk <br/>
                            +44 (0)203 991 6494
                        </td>
                    </tr>
                </table>';
                $pdf->writeHTMLCell(0, 0, '', '', $footer, 0, 1, 0, true, '', true);
                $pdf->writeHTML($html, true, false, true, false, '');
                ob_end_clean();

                $base_url = base_url();
                $base_path = $_SERVER['DOCUMENT_ROOT'];
                $base_path .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/';
               
                $pdf_name = "hazard_register_".date('Y_m_d_H_i_s').".pdf";
                $save_base_pdf_path = $base_url."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_full_pdf_path = $base_path."assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $save_pdf_path = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";

                $pdf->Output($save_full_pdf_path, 'F');


                $url = "assets/pdf/report_".date('Y_m_d_H_i_s').".pdf";
                $folder = "assets/pdf/";
                s3Upload($url, $folder);
                $pdf_url = S3_URL.$url;

                $update_data['report_file'] = $pdf_url;
                $update_data['updated_at'] = date('Y-m-d H:i:s');
                $update_data['report_status'] = $status;

                //Added by Nayan
                if(isset($input['result_file_path'])){
                    $update_data['result_file_path'] = $input['result_file_path'];
                }
                if(isset($input['ref_number'])){
                    $update_data['ref_number'] = $input['ref_number'];
                }
                //End added by Nayan
                
                $this->db->update('booking_member',$update_data, array('id' => $input['booking_member_id']));

                $email_var_data["user_name"] = $value['full_name'];
                $email_var_data["booking_id"] = $value['booking_id'];
                $email_var_data["pdf_link"] = $pdf_url; 
                $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is POSITIVE.  Please stay at home and isolate from your family members.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is POSITIVE.  Please stay at home and isolate from your family members.";
                if($status == 'negative'){
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is NEGATIVE. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                    $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is NEGATIVE. You did not have the virus when the test was done.";
                }
                else if($status == 'invaild'){
                    $email_var_data["test_status"] = 'Your COVIN-19 test result for the sample collected on '.$booking_date.' is INVALID. You did not have the virus when the test was done.if you feel unwell or need medical advise, please contact your doctor. For more information of report download below PDF.';
                    $notification_message = "Your COVIN-19 test result for the sample collected on $booking_date is INVALID. You did not have the virus when the test was done.";
                }
                
                $message = $this->load->view('email/certificate_information',$email_var_data,true);
                $mail = send_mail($value['user_email'],'Booking Information', $message);
                $notification_title = "You result is ".strtoupper($status).".";
                send_push_notification($value['u_id'],$notification_title,$notification_message,$value['booking_id'],$status,"bookingDetail");
                Bookings::where("id",$value['booking_id'])->update(array("booking_status"=>'success'));

                $data['status'] = true;
                $data['message'] = "Your report is ready.";
                $data['pdf_url'] = $pdf_url;
                $this->response($data);
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid booking member";
                $this->response($data);
            }
        }else{
            $data['status'] = false;
            $data['message'] = "Invalid status";

            $this->response($data);
        }

    }

    public function save_slots_post() {
        $this->form_validation->set_rules('booking_member_id', 'Booking Member Id', 'required');
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $qry2 = $this->db->select('*')->from('booking_member')->where(['id' => $request['booking_member_id'],'type' => 'home'])->get();
            $book = $qry2->num_rows();
            if ($book > 0) {
                // print_r($request['report_id']);

                if (isset($request['date'])) { $booking['date'] = ($request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d'))  : "";  } 
                if (isset($request['date'])) { $booking_member['shipping_date'] = ($request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d'))  : "";  } 
                if (isset($request['clinic_id'])) { $booking_member['clinic_id'] = ($request['clinic_id']!="") ? $request['clinic_id'] : ""; }
                if (isset($request['report_id'])) { $booking_member['report_id'] = ($request['report_id']!="") ? $request['report_id'] : ""; }
                
                if (isset($booking_member)) {
                    $this->db->where('id',$request['booking_member_id']);
                    $this->db->update('booking_member',$booking_member); 
                }
                if (isset($booking)) {
                    $this->db->where('booking_member_id',$request['booking_member_id']);
                    $this->db->update('booking_slots',$booking);
                }
            }else{
                if (isset($request['time'])) {
                    $date = (isset($request['date'])) ? (date_format(date_create($request['date']),'Y-m-d')) : ""; 
                    $query1 = $this->db->select('*')->from('booking_session')->where(['slot_date' => $date, 'slot_time' => $request['time']])->get();
                    $query2 = $this->db->select('*')->from('booking_slots')->where(['date' => $date, 'slots' => $request['time']])->get();
                    $bookings = $query1->num_rows();
                    $bookings2 = $query2->num_rows();
                    if ($bookings > 0 || $bookings2 > 0) {
                        $data['status'] = false;
                        $data['message'] = 'timeslot already booked. Please select another time.'; 
                    }else{
                        if (isset($request['date'])) { $booking['date'] = ($request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d'))  : "";  }
                        if (isset($request['time'])) { $booking['slots'] = ($request['time']!="") ? $request['time'] : ""; }
                        if (isset($request['clinic_id'])) { $booking_member['clinic_id'] = ($request['clinic_id']!="") ? $request['clinic_id'] : ""; }
                        if (isset($request['report_id'])) { $booking_member['report_id'] = ($request['report_id']!="") ? $request['report_id'] : ""; }
                        if (isset($booking_member)) {
                            $this->db->where('id',$request['booking_member_id']);
                            $this->db->update('booking_member',$booking_member); 
                        }
                        if (isset($booking)) {
                            $this->db->where('booking_member_id',$request['booking_member_id']);
                            $this->db->update('booking_slots',$booking);
                        }
                    }
                    // echo $this->db->last_query();
                }else{
                    if (isset($request['date'])) { $booking['date'] = ($request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d')) : "";  }
                    if (isset($request['time'])) { $booking['slots'] = ($request['time']!="") ? $request['time'] : ""; }
                    if (isset($request['clinic_id'])) { $booking_member['clinic_id'] = ($request['clinic_id']!="") ? $request['clinic_id'] : ""; }
                    if (isset($request['report_id'])) { $booking_member['report_id'] = ($request['report_id']!="") ? $request['report_id'] : ""; }

                    if (isset($booking_member)) {
                        $this->db->where('id',$request['booking_member_id']);
                        $this->db->update('booking_member',$booking_member); 
                    }
                    if (isset($booking)) {       
                        $this->db->where('booking_member_id',$request['booking_member_id']);
                        $this->db->update('booking_slots',$booking);
                    }

                    // echo $this->db->last_query();
                }
            }
                $data['status'] = true;
                $data['message'] = "Booking details updated successfully.";
        }
        $this->response($data);

    }

    public function add_booking_session_post()
    {
        $minutes  = 5;
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');
        $this->form_validation->set_rules('time', 'Time', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        } else {
            $request = $this->post();
            $date = date('Y-m-d', strtotime($request['date']));
            
            $query1 = $this->db->select('*')->from('booking_session')->where(['slot_date' => $date, 'slot_time' => $request['time']])->get();
            $query2 = $this->db->select('*')->from('booking_slots')->where(['date' => $date, 'slots' => $request['time']])->get();
            $bookings = $query1->num_rows();
            $bookings2 = $query2->num_rows();
            if ($bookings > 0 || $bookings2 > 0) {
                $query = $this->db->select('*')->from('booking_session')->where('user_id', $request['user_id'])->get();
                $bookingResult = $query->result_array();
                $bookingCount = $query->num_rows();
               
                $start = new DateTime($bookingResult[0]['created_at']);
                $end = new DateTime();
                $startTime = $start->format('H:i');
                $endTime = $end->format('H:i');
                $interval = $start->diff($end);
                $interval = $interval->format('%i');
                $totalminutes = max((($bookingCount * ($minutes)) - $interval), 0);

                $arr['minutes'] = $totalminutes;
                $data['status'] = false;
                $data['message'] = 'timeslot already booked';
                $data['data'] = $arr;
            } else {
                BookingSession::insert(['user_id' => $request['user_id'], 'slot_date' => $date, 'slot_time' => $request['time']]);
                $query = $this->db->select('*')->from('booking_session')->where('user_id', $request['user_id'])->get();
                $bookingCount = $query->num_rows();
                $bookingResult = $query->result_array();
                if ($bookingCount > 0) {
                    $start = new DateTime($bookingResult[0]['created_at']);
                    $end = new DateTime();
                    $startTime = $start->format('H:i');
                    $endTime = $end->format('H:i');
                    $interval = $start->diff($end);
                    $interval = $interval->format('%i');

                    $totalminutes = max((($bookingCount * ($minutes)) - $interval), 0);
                    $arr['minutes'] = $totalminutes;
                    $data['status'] = true;
                    $data['data'] = $arr;
                } else {
                    $data['status'] = false;
                }
            }
        }
        $this->response($data);
    }

    public function add_booking_post() {
        $data['status'] = false;
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        // $this->form_validation->set_rules('report_price', 'Report Price', 'required');
        $this->form_validation->set_rules('total_price', 'Total Price', 'required');
        // $this->form_validation->set_rules('booking_status', 'Booking Status', 'required');
        // $this->form_validation->set_rules('tax_price', 'Tax Price', 'required');

        $request = $this->post();
        $members = $request['member_details'];
        $booking = array();
        $post = array();

        $booking = $this->db->from('booking')->where('id',$request['booking_id'])->order_by('id','DESC')->limit(1)->get()->row_array();
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            if (!empty($booking)) {
                $books = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();
                if (!empty($books)) {
                    $booking['booking_no'] = ((int)$books['booking_no'])+1;
                }else{
                    $booking['booking_no'] = "100232100";
                }
                // $booking['user_id'] = $request['user_id'];
                $booking['report_price'] = (isset($request['report_price'])) ? $request['report_price'] : 0;
                $booking['total_price'] = ($request['total_price'] != "") ? $request['total_price'] : 0;
                $booking['tax_price'] = (isset($request['tax_price'])) ? $request['tax_price'] : 0;
                // $booking['booking_status'] = "pending";

                if (isset($request['booking_id']) && $request['booking_id'] !="") {
                    // $booking['booking_status'] = "pending";
                    // $booking['payment_status'] = "pending";
                    $booking['payment_type'] = "card";
                    Bookings::whereId($request['booking_id'])->update($booking);

                    $booking_id = $request['booking_id'];
                    $return_array = [];
                    $is_booking = false;
                    
                    $user_data = User::whereId($request['user_id'])->first();
                    if (!empty($user_data)) {
                        if ($user_data->stripe_key == '' && empty($user_data->stripe_key)) {
                            $return_array = $this->stripe("", ($request['total_price'])*100);
                            $update_key['stripe_key'] = $return_array['customer_id'];
                            User::whereId($request['user_id'])->update($update_key);
                        }else{
                            $return_array = $this->stripe($user_data->stripe_key, ($request['total_price'])*100);
                        }

                       $payment_intent = createPaymentIntent($request['total_price']);
                        $data['client_secret'] = $payment_intent['client_secret'];
                        //$data['client_secret'] = "";
                        $data['status'] = true;
                        $return_array['booking_id'] = $booking_id;
                        $return_array['booking_reference_id'] = $booking['booking_no'];
                        $data['message'] = "Thank You for Booking.";
                        $data['data'] = $return_array;
                    } else {
                        $data['status'] = false;
                        $data['message'] = "Something went wrong.";
                    }
                }else{
                    $data['status'] = false;
                    $data['message'] = "Something went wrong.";
                    // $booking_id = Bookings::insert($booking);
                    // $booking = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();
                }
            }
        }
            
        // }

        $this->response($data);
    }


    public function stripe($customer_id = '', $grand_total = '')
    {
        // echo "str";exit();
        // $customer_id = "";
        $payment_mode = $this->config->item('mode','stripe');
        if ($payment_mode == 'live') {
            $sk = $this->config->item('sk_live','stripe');
        } else
        {
            $sk = $this->config->item('sk_test','stripe');
        }
        \Stripe\Stripe::setApiKey($sk);

        if($customer_id == "")
        {
            $customer = \Stripe\Customer::create();
            $customer_id = $customer->id;
        } else
        {
            $customer_id = $customer_id;
        }         
        $ephemeralKey = \Stripe\EphemeralKey::create(
           ['customer' => $customer_id],
           ['stripe_version' => '2020-08-27']
         );
        $paymentIntent = \Stripe\PaymentIntent::create([
           'amount' => (int)$grand_total,
           'currency' => 'gbp', 
           'customer' => $customer_id
         ]);

        return $return_array = array(
          'paymentIntent' => $paymentIntent->client_secret,
           'ephemeralKey' => $ephemeralKey->secret,
           'customer_id' => $customer_id
        );
        // return true;
    }

    public function list_post() {
        $users = array();
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            if (isset($request['page'])) {
                $page =  $request['page'];
                if ($page == 0) {
                    $page = 1;
                }
                if ($page == 1) {
                    $start = 0;
                } else {
                    $start = ($page - 1) * 10;
                }
            }

            // $limit = LIMIT;
            if ($request['booking_key'] == "history") {
                $str = "(`booking_member`.`report_status` != '')";
            }else{
                // $str = "(`booking`.`booking_status` != 'completed' AND `booking`.`booking_status`!= 'cancelled')";
                $str = "(`booking_member`.`report_status` IS NULL AND `booking`.`booking_status` != 'in-complete')";
                // $str = "(`booking`.`booking_status` != 'completed' AND `booking`.`booking_status`!= 'cancelled')";
            }

            $this->db->select('booking.booking_status,booking.booking_no as booking_refernce,booking_member.clinic_id,booking.id as booking_id,booking.type,booking.total_price, booking_member.id as booking_member_id, booking_member.report_status as test_result, booking_member.report_file as pdf,booking_member.pickup_date,booking_member.return_date, booking_member.return_time,booking_member.shipping_date,booking_member.shipping_address,users.full_name as name,booking_member.result_file_path,booking_member.ref_number,booking_member.report_status as test_result,booking_member.report_file as pdf,booking_member.report_id as report_type');
            $this->db->from('booking');
            $this->db->join('booking_member','booking.id = booking_member.booking_id');
            // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
            $this->db->join('users','booking_member.user_id = users.id');
            $this->db->where('booking.user_id',$request['user_id']);
            if (isset($str)) {
                $this->db->where($str);
            }
            $this->db->where('users.active',1);
            if (isset($request['page'])) {            
                $this->db->limit(LIMIT,$start);
            }
            $query = $this->db->group_by('booking.booking_no');
            $query = $this->db->order_by('booking.id','DESC');
            $query = $this->db->get();
            $bookingDetails = $query->result_array();
            // echo $this->db->last_query();
            $userCount = $query->num_rows();

            if ($userCount > 0) {
                foreach ($bookingDetails as $k => $v) {
                    $query1 = $this->db->select('*')->from('booking_member')->where('booking_id', $v['booking_id'])->get(); 
                    $query2 = $this->db->select('date,slots')->from('booking_slots')->where('booking_member_id', $v['booking_member_id'])->get(); 
                    $bookingDetails[$k]['memberCount'] = "".$query1->num_rows()."";
                    $bookingmemData = $query1->row_array();
                    $bookingData = $query2->row_array();
                    $query3 = $this->db->select('*')->from('reports')->where('id', $bookingmemData['report_id'])->get(); 
                    $reportData = $query3->row_array();
                    $bookingDetails[$k]['booking_status'] =ucfirst($v['booking_status']); 

                    $hospital = $this->db->select('*')->from('hospital')->where('id', $bookingmemData['clinic_id'])->get()->row_array();
                    if (!empty($hospital)) {
                        $bookingDetails[$k]['place_of_test'] = $hospital['company_name'];
                    }else{
                        $bookingDetails[$k]['place_of_test'] = "";
                    }

                    if ($v['booking_status'] =="pending") {
                        $bookingDetails[$k]['color'] = "#FF0000";
                    }elseif ($v['booking_status'] == "success") {
                        $bookingDetails[$k]['color'] = "#1abc9c";
                    }elseif ($v['booking_status'] == "in-complete") {
                        $bookingDetails[$k]['color'] = "#f7b84b";
                    }else{
                        $bookingDetails[$k]['color'] = "#f7b84b";
                    }

                    if ($v['test_result'] == NULL || $v['test_result'] == "") {
                        $bookingDetails[$k]['test_result'] = "Result-awaiting";
                        $bookingDetails[$k]['test_color'] = "#f7b84b";
                    }else{
                        $bookingDetails[$k]['test_result'] = ucfirst($v['test_result']);
                        if ($v['test_result'] =="positive") {
                            $bookingDetails[$k]['test_color'] = "#FF0000";
                        }else{
                            $bookingDetails[$k]['test_color'] = "#1CA6EE";
                        }
                    }

                    $bookingDetails[$k]['booking_date'] = ($bookingData['date'] !="") ? date('M d Y',strtotime($bookingData['date'])) : ''; 
                    $bookingDetails[$k]['booking_time'] = $bookingData['slots']; 
                    $bookingDetails[$k]['home_address'] =$bookingmemData['home_address']; 
                    $bookingDetails[$k]['test'] =$reportData['name']; 
                }
            }
            // print_r($bookingDetails);
            if (empty($bookingDetails)) {
                $data['message'] = "No booking found.";
            }
            $data['status'] = true;
            $data['data'] = $bookingDetails;
        }
        $this->response($data);
    }

    public function get_interbooking_post() {
        $users = array();
        $data = array();
        $data1 = array();
        $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            $this->db->select('*,bm.id as booking_member_id');
            $this->db->from('booking b');
            $this->db->join('booking_member as bm','b.id = bm.booking_id');
            $this->db->where('b.id',$request['booking_id']);
            $this->db->where('bm.user_id',$request['user_id']);
            $this->db->where('b.type',"roundtrip");
            $this->db->order_by('bm.id',"DESC");
            $query = $this->db->get();
            $dataArr = $query->row_array();
            // echo $this->db->last_query();

            if (!empty($dataArr)) {
                // _pre($dataArr);
                foreach ($dataArr as $key => $value) {
                    $data['booking_member_id'] = $dataArr['booking_member_id'];
                    $data['clinic_id'] = $dataArr['clinic_id'];
                    $data['departure_date'] = $dataArr['departure_date'];
                    $data['departure_time'] = $dataArr['departure_time'];
                    $data['transport_type'] = $dataArr['transport_type'];
                    $data['transport_number'] = $dataArr['transport_number'];
                    $data['arriving_country'] = $dataArr['destination_country'];
                    $data['visiting_country'] = $dataArr['visiting_country'];
                    $data['return_country'] = $dataArr['return_country'];
                    $data['shipping_method'] = $dataArr['shipping_method'];
                    $data['shipping_date'] = $dataArr['shipping_date'];
                    $data['shipping_address'] = $dataArr['is_excempted'];
                    $data['shippingpostal_code'] = $dataArr['shippingpostal_code'];
                    $data['home_address'] = $dataArr['home_address'];
                    $data['homepostal_code'] = $dataArr['homepostal_code'];
                    $data['vaccine_status'] = $dataArr['vaccine_status'];
                }
            }

            if (empty($dataArr)) {

                $data1['message'] = "No booking found.";
                // $data1['who_can_access'] = array();
                $data1['data'] = array();
            }else{
            }
            $data1['status'] = true;
            $data1['data'] = $data;
        }
        $this->response($data1);
    }

    public function booking_details_post() {
        $users = array();
        $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            if (isset($request['page'])) {
                $page =  $request['page'];
                if ($page == 0) {
                    $page = 1;
                }
                if ($page == 1) {
                    $start = 0;
                } else {
                    $start = ($page - 1) * 10;
                }
            }

            $this->db->select('booking_status,booking_no as booking_refernce,id as booking_id,type,total_price, pass_link');
            $this->db->from('booking');
            $this->db->where('id',$request['booking_id']);
            if (isset($request['page'])) {            
                $this->db->limit(LIMIT,$start);
            }
            $query = $this->db->get();
            $bookingDetails = $query->row_array(); 
            // echo $this->db->last_query();

            $userCount = $query->num_rows();

            if ($userCount > 0) {
                $query1 = $this->db->select('*')->from('booking_member')->where('booking_id', $bookingDetails['booking_id'])->get(); 
                $bookingDetails['memberCount'] = "".$query1->num_rows()."";
                $bookingmemData = $query1->result_array(); 
                foreach ($bookingmemData as $k => $v) {
                    $this->db->select('users.email,users.phone,users.date_of_birth,users.passport,users.full_name,booking_member.home_address,reports.name as test,reports.id as report_id,booking_member.clinic_id as hospital_id,booking_slots.date as booking_date,booking_slots.date as date,booking_slots.slots as time,booking_slots.slots as booking_time,booking_slots.id as slot_id,users.id as user_id, booking_member.shipping_method, booking_member.shipping_date, booking_member.pickup_date, booking_member.id as booking_member_id,booking_member.report_status as test_result,booking_member.report_file as pdf,booking_member.return_date,booking_member.return_time,booking_member.shipping_address,booking_member.user_id as member_id,booking_member.plf_document, booking_member.reference_member_id as member_reference_no, booking_member.tracking_number as tracking_no,booking_member.vaccine_status,booking_member.type as place_type');
                    $this->db->from('booking_member');
                    // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
                    $this->db->join('users','booking_member.user_id = users.id');
                    $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                    $this->db->join('reports','reports.id = booking_member.report_id');
                    $this->db->where('booking_member.id',$v['id']);
                    $this->db->where('users.active',1);
                    $query = $this->db->get();
                    $bookings = $query->row_array();


                    $hospital = $this->db->select('*')->from('hospital')->where('id', $bookings['hospital_id'])->get()->row_array();
                    if (!empty($hospital)) {
                        $bookings['place_of_test'] = $hospital['company_name'];
                    }else{
                        $bookings['place_of_test'] = "";
                    }
                    if (strpos(strtolower($bookings['test']), 'lfd') !== false && empty($bookings['pdf'])) {
                        $bookings['upload_kit_status'] = true;
                        /*if ($bookings['report_id'] == "5" ) {
                            $bookings['upload_kit_status'] = true;
                        }else{
                            $bookings['upload_kit_status'] = false; 
                        }*/
                    }else{
                        $bookings['upload_kit_status'] = false; 
                    }

                    // echo $this->db->last_query();
                    if ($bookingDetails['type'] == "international-arrival") {
                        
                        if ($bookings['shipping_method'] == "2") {
                            $bookings['date_flag'] = "2";
                            $bookings['date'] = $bookings['shipping_date'];
                            $bookings['booking_date'] = $bookings['shipping_date'];
                        }else{
                            $bookings['date_flag'] = "1";
                            $bookings['date'] = $bookings['pickup_date'];
                            $bookings['booking_date'] = $bookings['pickup_date'];
                        }
                    }else{
                        $bookings['date_flag'] = "";
                    }
                    if (!isset($bookings['pdf']) || ($bookings['pdf'] == "" || $bookings['pdf'] == NULL)) {
                        $bookings['pdf'] = "";
                    }else{
                        $bookings['pdf'] = $bookings['pdf'];
                    }
                    if (!isset($bookings['test_result']) || $bookings['test_result'] == "" || $bookings['test_result'] == NULL) {
                        $bookings['test_result'] = "Result-awaiting";
                        $bookings['test_color'] = "#f7b84b";

                    }else{
                        if ($bookings['test_result'] =="positive") {
                            $bookings['test_color'] = "#FF0000";
                        }else{
                            $bookings['test_color'] = "#1CA6EE";
                        }
                        $bookings['test_result'] = ucfirst($bookings['test_result']);
                    }
                    $bookingDetails['booking_status'] = ucfirst($bookingDetails['booking_status']);

                    $bookingDetails['member_details'][] = $bookings;
                }
            }
            if (empty($bookingDetails)) {
                $data['message'] = "No booking found.";
            }
            $data['status'] = true;
            $data['data'] = $bookingDetails;
        }
        $this->response($data);
    }

    public function booking_status_post() {
        $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        $this->form_validation->set_rules('payment_status', 'Payment Status', 'required');
        $this->form_validation->set_rules('payment_type', 'Payment Type', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
            $request = $this->post();
            if ($request['payment_status'] == "1") {
                $update_key['booking_status'] = "success";
                $update_key['payment_status'] = "success";                
            }else if ($request['payment_status'] == "2") {
                $update_key['booking_status'] = "failed";
                $update_key['payment_status'] = "failed";
            }else if ($request['payment_status'] == "3") {
                $update_key['booking_status'] = "payment processing";
                $update_key['payment_status'] = "payment processing";
            }else {
                $update_key['booking_status'] = "pending";
                $update_key['payment_status'] = "pending";
            }

            $update_key['payment_type'] = $request['payment_type'];
            Bookings::whereId($request['booking_id'])->update($update_key);

            if ($request['payment_status'] == 1) {
                $this->db->select('b.*,bm.user_id as book_user_id,bm.id as booking_member_id,u.email as user_email,u.full_name as full_name');
                $this->db->from('booking b');
                $this->db->join('booking_member as bm','b.id = bm.booking_id');
                $this->db->join('users as u','u.id = bm.user_id');
                $this->db->where('bm.booking_id',$request['booking_id']);
                $this->db->where('b.type',"roundtrip");
                $this->db->order_by('bm.id',"DESC");
                $book_data = $this->db->get()->result_array();
                // _pre($book_data);

                foreach ($book_data as $key => $value) {
                    $booking_link = base_url().'api/booking?booking_id='.$request['booking_id'].'&user_id='.$value['book_user_id'];
                    
                    $email_var_data["full_name"] = $value['full_name'];
                    $email_var_data["booking_id"] = $value['booking_no'];
                    $email_var_data["link"] = $booking_link; 
                    
                    $message = $this->load->view('email/booking_information',$email_var_data,true);

                    $mail = send_mail($value['user_email'],'Booking Information', $message);
                }


            }
            $data['status'] = true;
            $data['message'] = "Booking Status Updated";
        }
        $this->response($data);
    }

    public function addnewmember_fitroundtrip_post() {
        $data = array();
        $this->form_validation->set_rules('full_name', 'full name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required'); 
        $this->form_validation->set_rules('city', 'City', 'required'); 
        $this->form_validation->set_rules('postcode', 'Post Code', 'required'); 
        $this->form_validation->set_rules('gender', 'Gender', 'required'); 
        $this->form_validation->set_rules('date_of_birth', 'Birth Date', 'required'); 
        $this->form_validation->set_rules('ethnicity', 'Ethnicity', 'required'); 
        $this->form_validation->set_rules('nationality', 'Nationality', 'required'); 
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');

        $this->form_validation->set_rules('parent_id', 'Parent Id', 'required');
        // $this->form_validation->set_rules('departure_country', 'Departure Country', 'required');
        $this->form_validation->set_rules('dest_country', 'Destination Country', 'required');
        $this->form_validation->set_rules('departure_date', 'Departure Date', 'required');
        $this->form_validation->set_rules('departure_time', 'Departure Time', 'required');
        $this->form_validation->set_rules('report_id', 'Report Id', 'required');
        $this->form_validation->set_rules('date', 'Test Date', 'required');
        $this->form_validation->set_rules('time', 'Test Time', 'required');
        $this->form_validation->set_rules('clinic_id', 'Clinic Id', 'required');
        // $this->form_validation->set_rules('return_country', 'Return Country', 'required');
        // $this->form_validation->set_rules('is_visiting_country', ' Country', 'required');
        // $this->form_validation->set_rules('visiting_country', 'Departure Country', 'required');
        $this->form_validation->set_rules('second_report_id', 'Second Report Id', 'required'); 
        $this->form_validation->set_rules('return_date', 'Return Date', 'required');
        // flight_no / vessel_no / train_no
        // shipping_method
        // shipping_date / shipping_address / is_shipping_address

        // ------------------------------------3rd step ---------------------------------//
        // is_residency / residency_address /
        // arriving_country / transiting_country 
        // is_exempted / exemption_cat
        // vaccination_status
        // second_shipping_date
        // place_type 
        // $this->form_validation->set_rules('third_test_type', 'Third Test Type', 'required');

        // ------------------------------------3rd step ---------------------------------//
        // $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        } else {
            $request = $this->post();

            $email = $request['email']; 
            $phone = $request['phone']; 

            $password = (isset($request['password'])) ? $request['password'] : NULL;

            $query = $this->db->select('*')->from('users')->where('email', $request['email'])->or_where('phone', $request['phone'])->get();
            $userCount = $query->num_rows();
            $userDetails = $query->row_array();

            if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
                $type = explode('.', $_FILES["profile_picture"]["name"]);
                $type = strtolower($type[count($type) - 1]);
                $name = mt_rand(100000000, 999999999);
                $filename = $name . '.' . $type;
                $url = "assets/images/users/" . $filename;
                move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
                $folder = "assets/images/users/";
                s3Upload($url, $folder);
                $image = S3_URL.$url;
                $request['profile_picture'] = $image;
            }

            if ($userCount > 0) {
                // $parent_id = ($request['parent_id']== "0") ? $request['full_name'] : explode('', string) ;
                $userData = array(
                    // 'parent_id'          => $parent_id,
                    'full_name'         => $request['full_name'],
                    'email'             => $request['email'],
                    'address'           => $request['address'],
                    'postcode'          => $request['postcode'],
                    'date_of_birth'     => date('d-m-Y',strtotime($request['date_of_birth'])),
                    'gender'            => $request['gender'],
                    'ethnicity'         => $request['ethnicity'],
                    'passport'          => (isset($request['passport'])) ? $request['passport'] : NULL,
                    'driving_lic'       => (isset($request['driving_lic'])) ? $request['driving_lic'] : NULL,
                    'id_card'           => (isset($request['id_card'])) ? $request['id_card'] : NULL,
                    'city'              => $request['city'],
                    // 'phone'             => $request['phone'],
                    'nationality'       => $request['nationality'],
                    'country_code'      => (isset($request['country_code'])) ? $request['country_code'] : NULL,
                    'vaccine_status'    => (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL,
                    'relation_type'     => (isset($request['relation_type'])) ? $request['relation_type'] : NULL,
                    'excempted_cat'     => (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL,
                    'symptoms'          => (isset($request['symptoms'])) ? $request['symptoms'] : NULL,
                    'is_homeaddress'    => (isset($request['is_homeaddress'])) ? $request['is_homeaddress'] : 'no',
                    'has_covid'         => (isset($request['has_covid'])) ? $request['has_covid'] : NULL,
                    'date_of_travel'    => (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL,
                    'travel_destination'=> (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL,
					'vaccine_id'  		=> (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL,
                    'updated_at'        => current_date()
                );

                if (isset($request['profile_picture'])) {
                    $userData['profile_picture'] = $request['profile_picture'];
                }
                if (isset($request['password'])) {
                    $password = $this->ion_auth->hash_password($password);
                    $userData['password'] = $password;
                }
                unset($request['password']);

                $this->comman->update_record('users', $userData, $userDetails['id']);
                $profile = $this->comman->get_record_byid('users', $userDetails['id']);
                $id = $userDetails['id'];
            }else{
                if (isset($request['password'])) {
                    $password = $this->ion_auth->hash_password($password);
                    $userData['password'] = $password;
                }
                unset($request['email']);
                unset($request['password']);
                $id = $this->ion_auth->register($request['full_name'], $password, $email,$request, [2], 1);

            }
            // $user = User::find($id);
            $query  = $this->db->select('*')->from('users')->where('id', $id)->get();
            $user = $query->row_array();
           
            $booking = array();
            $post = array();

            if (isset($request['booking_id']) && $request['booking_id'] !="") {
                $booking_id = $request['booking_id'];
            }else{
                $books = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array(); 
                if (!empty($books)) {
                    $booking['booking_no'] = ((int)$books['booking_no'])+1;
                }else{
                    $booking['booking_no'] = "100232100";
                }
                $booking['user_id'] = $request['parent_id'];
                $booking['type'] = 'roundtrip';
                $booking['report_price'] = 0;
                $booking['total_price'] = 0;
                $booking['tax_price'] = 0;
                $booking['booking_status'] = "in-complete";
                $booking_id = Bookings::insert($booking);
                $booking = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();
                $booking_id = $booking['id'];
            }
            // $booking = $this->db->select('*')->from('booking')->where('id', $booking_id)->or_where('user_id', $request['parent_id'])->get()->row_array();
            // $booking = $this->db->from('booking')->where('user_id',$request['user_id'])->order_by('id','DESC')->limit(1)->get()->row_array();
            if ($booking_id) {
                // $member_details = json_decode($members, true);
                // if (!empty($member_details)) {
                // foreach ($member_details as $key => $v) { 
                $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
                $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;

                $post['booking_id'] = $booking_id;
                // GTETY0000001 
     //            if (!empty($members)) {
     //                $bookmemid = (int)($members['reference_member_id'])+1;   
     //                $value2 = $members['reference_member_id'];
     //                $explode_value2 = explode('-',$value2);
					// $value2 = $explode_value2[0];
     //                $value2 = substr($value2, 5, 12); //separating numeric part
     //                $value2 = $value2 + 1; //Incrementing numeric part
     //                $value2 = "GTETY" . sprintf('%07s', $value2); //concatenating incremented value
     //                $bookmemid = $value2;   
     //            }else{
     //                $bookmemid = "GTETY0000001";
     //            }
                $members = $this->db->from('booking_member')->like('reference_member_id','100')->order_by('id','DESC')->limit(1)->get()->row_array();
                // $books = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array(); 
                if (!empty($members)) {
                    $post['reference_member_id'] = ((int)$members['reference_member_id'])+1;
                }else{
                    $post['reference_member_id'] = "10010000";
                }
                // $post['reference_member_id'] = $bookmemid;
                $post['user_id'] = $user['id'];
                $post['clinic_id'] = (isset($request['clinic_id'])) ? $request['clinic_id'] : NULL;
                $post['home_address'] = $request['address'];

                $post['homepostal_code'] = $request['postcode'];
                $post['date_of_travel'] = (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL;
                $post['type'] = (isset($request['shipping_method']) && $request['shipping_method'] == "1") ? "clinic" : "home";
                $post['has_covid'] = (isset($request['has_covid'])) ? $request['has_covid'] : NULL;
                $post['travel_destination'] = (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL;
                $post['report_id'] = $request['report_id'];
                $post['destination_country'] = $request['dest_country'];
                // $post['departure_country'] = $request['departure_country'];
                $post['departure_date'] = (date_format(date_create($request['departure_date']),'Y-m-d'));
                $post['departure_time'] = $request['departure_time'];
                $post['return_country'] = (isset($request['return_country'])) ? $request['return_country'] : NULL;
                $post['is_visiting_country'] = (isset($request['is_visiting_country'])) ? $request['is_visiting_country'] : NULL;
                $post['visiting_country'] = (isset($request['visiting_country'])) ? $request['visiting_country'] : NULL;

                $post['second_report_id'] = (isset($request['second_report_id'])) ? $request['second_report_id'] : NULL;
                $post['return_date'] = (isset($request['return_date'])) ? $request['return_date'] : NULL;
                if (isset($request['transport_type'])) {
                    $post['transport_type'] = (isset($request['transport_type'])) ? $request['transport_type'] : NULL;
                    $post['transport_number'] = (isset($request['transport_number'])) ? $request['transport_number'] : NULL;
                }
                $post['shipping_method'] = (isset($request['shipping_method'])) ? $request['shipping_method'] : NULL;
               
                if (isset($request['shipping_method']) && $request['shipping_method'] == "2") {
                    $post['shipping_date'] = (isset($request['shipping_date']) && $request['shipping_date']!="") ? (date_format(date_create($request['shipping_date']),'Y-m-d')) : NULL;
                    $post['shipping_address'] = (isset($request['shippingaddress'])) ? $request['shippingaddress'] : NULL;
                    $post['shippingpostal_code']= (isset($request['shippingpostal_code'])) ? $request['shippingpostal_code'] : NULL;
                    
                }

                $post['vaccine_status'] = (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL;
                $post['relation_type'] = (isset($request['relation_type'])) ? $request['relation_type'] : NULL;
                $post['excempted_cat'] = (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL;
                $post['symptoms'] = (isset($request['symptoms'])) ? $request['symptoms'] : NULL;
				$post['vaccine_id']   = (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL;
                $booking_member_id = BookingsMembers::insert($post);
                $bookingmember = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array();

                if ($booking_member_id) {
                    $slot['booking_member_id'] = $bookingmember['id'];
                    $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
                    $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;
                    $slot_id = BookingSlots::insert($slot);
                }
                    // }
                // }
            }
            if (isset($slot_id)) {
                // $data['status'] = true;
                // $data['message'] = "Thank You for Booking.";
                if ($this->form_validation->run() == FALSE) {
                    // $data['status'] = false;
                    // $data['message'] = validation_errors_response();
                    $data['status'] = false;
                    $data['message'] = "Something went wrong.";
                } else {
                    $data['status'] = true;
                    $data['message'] = "New member added successfully";
                    $data['data']= $user;
                }

                $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
                $members1 = $query->result_array();

                // if (!empty($members1)) {
                    $data['members_list'] = true;
                // }else{
                //     $data['members_list'] = false;
                // }

                $this->response($data); 
            }else{
                $data['status'] = false;
                $data['message'] = "Something went wrong.";
            }

            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
            $members1 = $query->result_array();

            // if (!empty($members1)) {
                $data['members_list'] = true;
            // }else{
            //     $data['members_list'] = false;
            // }
        }
        $this->response($data);
    }

    public function members_list_fitroundtrip_post(){
        $members = array();

        $this->form_validation->set_rules('user_id', 'user ID', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->input->post();
            $members = User::select('*')->where(['parent_id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();
            $membersCount = $members->count();
            $membersDet = User::select('*')->where(['id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();

            $query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members = $query->result_array();

            if (!empty($members)) {
                foreach ($members as $key => $value) {
                    $this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.departure_date,booking_member.departure_time,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code,users.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
                    $this->db->from('booking');
                    $this->db->join('booking_member','booking.id = booking_member.booking_id');
                    $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                    $this->db->join('users','booking_member.user_id = users.id');
                    $this->db->where('booking.user_id',$request['user_id']);
                    $this->db->where('booking_member.user_id',$value['id']);
                    $this->db->where('booking.booking_status',"in-complete");
                    $this->db->where('users.active',1);
                    $this->db->order_by('booking.id','DESC');
                    $query = $this->db->get();
                    $bookingDetails = $query->row_array();
                    if (!empty($bookingDetails)) {
                        $members[$key]['date'] = $bookingDetails['date'];
                        $members[$key]['time'] = $bookingDetails['time'];
                        $members[$key]['booking_id'] = $bookingDetails['booking_id'];
                        $members[$key]['booking_member_id'] = $bookingDetails['booking_member_id'];
                        $members[$key]['clinic_id'] = $bookingDetails['clinic_id'];
                        $members[$key]['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
                        $members[$key]['report_id'] = $bookingDetails['report_id'];
                        $members[$key]['purpose_type'] = $bookingDetails['type'];
                        $members[$key]['destination_country'] = $bookingDetails['destination_country'];
                        $members[$key]['departure_date'] = date('d-m-Y',strtotime($bookingDetails['departure_date']));
                        $members[$key]['departure_time'] = $bookingDetails['departure_time'];
						$members[$key]['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$members[$key]['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	$members[$key]['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$members[$key]['proof_document'] = $bookingDetails['proof_document'];
						$members[$key]['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}
                        $hospital = $this->db->select('*')->from('hospital')->where('id', $bookingDetails['clinic_id'])->get()->row_array();
                        if (!empty($hospital)) {
                            $members[$key]['clinic_name'] = $hospital['company_name'];
                        }else{
                            $members[$key]['clinic_name'] = "";
                        }

                        $country = $this->db->select('*')->from('country')->where('id', $bookingDetails['destination_country'])->get()->row_array();
                        if (!empty($country)) {
                            $members[$key]['dest_country_name'] = $country['name'];
                        }else{
                            $members[$key]['dest_country_name'] = "";
                        }

                        $this->db->select('*');
                        $this->db->from('reports');
                        $this->db->where('id',$bookingDetails['report_id']);
                        $query = $this->db->get();
                        $report = $query->row_array();
                        if (!empty($report)) {
                            $members[$key]['test'] = $report['name'];
                        }else{
                            $members[$key]['test'] = "";
                        }
                    }else{
                        $members[$key]['date'] = "";
                        $members[$key]['time'] = "";
                        $members[$key]['booking_id'] = "";
                        $members[$key]['booking_member_id'] = "";
                        $members[$key]['clinic_id'] = "";
                        $members[$key]['report_id'] = "";
                        $members[$key]['shippingpostal_code'] = "";
                        $members[$key]['purpose_type'] = "";
                        $members[$key]['clinic_name'] = "";
                        $members[$key]['test'] = "";
                        $members[$key]['destination_country'] = "";
                        $members[$key]['departure_date'] = "";
                        $members[$key]['departure_time'] = "";
                        $members[$key]['dest_country_name'] = "";
						//$members[$key]['date_of_birth'] = "";
		            	$members[$key]['vaccine_date'] = "";
		            	//$members[$key]['vaccine_id'] = "";
		            	$members[$key]['proof_document'] = "";
						$members[$key]['vaccine_name'] = "";
                        if(!empty($value['vaccine_id'])){
                            $members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$value['vaccine_id']),"name");
                        }
                    }
                }
            }
            $query = $this->db->select('*')->from('users')->where('id', $request['user_id'])->get(); 
            $custDet = $query->row_array();

            if (!empty($custDet)) {
                $this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.departure_date,booking_member.departure_time,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code,booking_member.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
                $this->db->from('booking');
                $this->db->join('booking_member','booking.id = booking_member.booking_id');
                $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                $this->db->join('users','booking_member.user_id = users.id');
                $this->db->where('booking.user_id',$request['user_id']);
                $this->db->where('booking_member.user_id',$custDet['id']);
                $this->db->where('booking.booking_status',"in-complete");
                $this->db->where('users.active',1);
                $this->db->order_by('booking.id','DESC');
                $query = $this->db->get();
                $bookingDetails = $query->row_array();

                if (!empty($bookingDetails)) {
                    if (!empty($bookingDetails)) {
                        $hospital = $this->db->select('*')->from('hospital')->where('id', $bookingDetails['clinic_id'])->get()->row_array();

                        if (!empty($hospital)) {
                            $custDet['clinic_name'] = $hospital['company_name'];
                        }else{
                            $custDet['clinic_name'] = "";
                        }

                        $this->db->select('*');
                        $this->db->from('reports');
                        $this->db->where('id',$bookingDetails['report_id']);
                        $query = $this->db->get();
                        $report = $query->row_array();
                        if (!empty($report)) {
                            $custDet['test'] = $report['name'];
                        }else{
                            $custDet['test'] = "";
                        }

                        $country = $this->db->select('*')->from('country')->where('id', $bookingDetails['destination_country'])->get()->row_array();
                        if (!empty($country)) {
                            $custDet['dest_country_name'] = $country['name'];
                        }else{
                            $custDet['dest_country_name'] = "";
                        }

                        $custDet['date'] = $bookingDetails['date'];
                        $custDet['time'] = $bookingDetails['time'];
                        $custDet['booking_id'] = $bookingDetails['booking_id'];
                        $custDet['booking_member_id'] = $bookingDetails['booking_member_id'];
                        $custDet['clinic_id'] = $bookingDetails['clinic_id'];
                        $custDet['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
                        $custDet['report_id'] = $bookingDetails['report_id'];
                        $custDet['purpose_type'] = $bookingDetails['type'];
                        $custDet['destination_country'] = $bookingDetails['destination_country'];
                        $custDet['departure_date'] = date('d-m-Y',strtotime($bookingDetails['departure_date']));
                        $custDet['departure_time'] = $bookingDetails['departure_time'];
						//$custDet['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$custDet['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	//$custDet['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$custDet['proof_document'] = $bookingDetails['proof_document'];
						$custDet['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}
                    }else{
                        $custDet['date'] = "";
                        $custDet['time'] = "";
                        $custDet['booking_id'] = "";
                        $custDet['booking_member_id'] = "";
                        $custDet['shippingpostal_code'] = "";
                        $custDet['clinic_id'] = "";
                        $custDet['report_id'] = "";
                        $custDet['purpose_type'] = "";
                        $custDet['destination_country'] = "";
                        $custDet['departure_date'] = "";
                        $custDet['departure_time'] = "";
                        $custDet['dest_country_name'] = "";
						//$custDet['date_of_birth'] = "";
		            	$custDet['vaccine_date'] = "";
		            	//$custDet['vaccine_id'] = "";
		            	$custDet['proof_document'] = "";
						$custDet['vaccine_name'] = "";
                        if(!empty($custDet['vaccine_id'])){
                            $custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
                        }
                    }
                }else{
                    $custDet['date'] = "";
                    $custDet['time'] = "";
                    $custDet['booking_id'] = "";
                    $custDet['booking_member_id'] = "";
                    $custDet['dest_country_name'] = "";
                    $custDet['clinic_id'] = "";
                    $custDet['shippingpostal_code'] = "";
                    $custDet['report_id'] = "";
                    $custDet['purpose_type'] = "";
                    $custDet['destination_country'] = "";
                    $custDet['departure_date'] = "";
                    $custDet['departure_time'] = "";
					//$custDet['date_of_birth'] = "";
					$custDet['vaccine_date'] = "";
					//$custDet['vaccine_id'] = "";
					$custDet['proof_document'] = "";
					$custDet['vaccine_name'] = "";
                    if(!empty($custDet['vaccine_id'])){
                        $custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
                    }
                }
            }

            $membersC = $membersDet->count();
            if ($membersC !=0) {
                $members[] = $custDet;
                // array_push($members,$custDet);
            }
        }
        $data['status'] = true;
        if($membersCount == 0){
            $data['message'] = "No members found";
        }else{
            $data['message'] = "";

        }
        $data['data'] = $members;

        $this->response($data);
    }

    public function members_list_intarrival_post(){
        $members = array();

        $this->form_validation->set_rules('user_id', 'user ID', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->input->post();
            $members = User::select('*')->where(['parent_id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();
            $membersCount = $members->count();
            $membersDet = User::select('*')->where(['id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();

            $query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members = $query->result_array();

            if (!empty($members)) {
                foreach ($members as $key => $value) {
                    $this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.pickup_date,booking_member.shipping_date,booking_member.shipping_method,booking_member.return_date,booking_member.return_time,booking_member.clinic_id,booking.id as booking_id, booking.type, booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code,users.date_of_birth, booking_member.vaccine_date,booking_member.vaccine_id, booking_member.proof_document');
                    $this->db->from('booking');
                    $this->db->join('booking_member','booking.id = booking_member.booking_id');
                    $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                    $this->db->join('users','booking_member.user_id = users.id');
                    $this->db->where('booking.user_id',$request['user_id']);
                    $this->db->where('booking_member.user_id',$value['id']);
                    $this->db->where('booking.booking_status',"in-complete");
                    // $this->db->where('users.active',1);
                    $this->db->order_by('booking.id','DESC');
                    $query = $this->db->get();
                    $bookingDetails = $query->row_array();
                    // echo $this->db->last_query();
                    if (!empty($bookingDetails)) {
                        $members[$key]['time'] = $bookingDetails['time'];
                        $members[$key]['booking_id'] = $bookingDetails['booking_id'];
                        $members[$key]['booking_member_id'] = $bookingDetails['booking_member_id'];
                        $members[$key]['clinic_id'] = $bookingDetails['clinic_id'];
                        $members[$key]['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
                        $members[$key]['report_id'] = $bookingDetails['report_id'];
                        $members[$key]['purpose_type'] = $bookingDetails['type'];
                        $members[$key]['destination_country'] = $bookingDetails['destination_country'];
                        $members[$key]['return_date'] = date('d-m-Y',strtotime($bookingDetails['return_date']));
                        $members[$key]['return_time'] = $bookingDetails['return_time'];
						$members[$key]['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$members[$key]['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	$members[$key]['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$members[$key]['proof_document'] = $bookingDetails['proof_document'];
						$members[$key]['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}

                        if ($bookingDetails['shipping_method'] == "2") {
                            $members[$key]['date_flag'] = "2";
                            $members[$key]['date'] = $bookingDetails['shipping_date'];
                        }else{                             
                            $members[$key]['date_flag'] = "1";
                            $members[$key]['date'] = $bookingDetails['pickup_date'];
                        }

                        $hospital = $this->db->select('*')->from('hospital')->where('id', $bookingDetails['clinic_id'])->get()->row_array();
                        if (!empty($hospital)) {
                            $members[$key]['clinic_name'] = $hospital['company_name'];
                        }else{
                            $members[$key]['clinic_name'] = "";
                        }

                        $country = $this->db->select('*')->from('country')->where('id', $bookingDetails['destination_country'])->get()->row_array();
                        if (!empty($country)) {
                            $members[$key]['dest_country_name'] = $country['name'];
                        }else{
                            $members[$key]['dest_country_name'] = "";
                        }

                        $this->db->select('*');
                        $this->db->from('reports');
                        $this->db->where('id',$bookingDetails['report_id']);
                        $query = $this->db->get();
                        $report = $query->row_array();
                        if (!empty($report)) {
                            $members[$key]['test'] = $report['name'];
                        }else{
                            $members[$key]['test'] = "";
                        }
                    }else{
                        $members[$key]['date'] = "";
                        $members[$key]['time'] = "";
                        $members[$key]['booking_id'] = "";
                        $members[$key]['booking_member_id'] = "";
                        $members[$key]['clinic_id'] = "";
                        $members[$key]['report_id'] = "";
                        $members[$key]['shippingpostal_code'] = "";
                        $members[$key]['purpose_type'] = "";
                        $members[$key]['clinic_name'] = "";
                        $members[$key]['test'] = "";
                        $members[$key]['destination_country'] = "";
                        $members[$key]['return_date'] = "";
                        $members[$key]['return_time'] = "";
                        $members[$key]['dest_country_name'] = "";
						//$members[$key]['date_of_birth'] = "";
		            	$members[$key]['vaccine_date'] = "";
		            	//$members[$key]['vaccine_id'] = "";
		            	$members[$key]['proof_document'] = "";
						$members[$key]['vaccine_name'] = "";
                        if(!empty($value['vaccine_id'])){
                            $members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$value['vaccine_id']),"name");
                        }
                    }
                }
            }
            $query = $this->db->select('*')->from('users')->where('id', $request['user_id'])->get(); 
            $custDet = $query->row_array();

            if (!empty($custDet)) {
                $this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.pickup_date,booking_member.shipping_date,booking_member.shipping_method,booking_member.return_date,booking_member.return_time,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code,booking_member.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');    
                $this->db->from('booking');
                $this->db->join('booking_member','booking.id = booking_member.booking_id');
                $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
                $this->db->join('users','booking_member.user_id = users.id');
                $this->db->where('booking.user_id',$request['user_id']);
                $this->db->where('booking_member.user_id',$custDet['id']);
                $this->db->where('booking.booking_status',"in-complete");
                // $this->db->where('users.active',1);
                $this->db->order_by('booking.id','DESC');
                $query = $this->db->get();
                $bookingDetails = $query->row_array();

                if (!empty($bookingDetails)) {
                    if (!empty($bookingDetails)) {
                        $hospital = $this->db->select('*')->from('hospital')->where('id', $bookingDetails['clinic_id'])->get()->row_array();

                        if (!empty($hospital)) {
                            $custDet['clinic_name'] = $hospital['company_name'];
                        }else{
                            $custDet['clinic_name'] = "";
                        }
                        if ($bookingDetails['shipping_method'] == "2") {

                            $custDet['date_flag'] = "2";
                            $custDet['date'] = $bookingDetails['shipping_date'];
                        }else{                             
                            $custDet['date_flag'] = "1";
                            $custDet['date'] = $bookingDetails['pickup_date'];
                        }

                        $this->db->select('*');
                        $this->db->from('reports');
                        $this->db->where('id',$bookingDetails['report_id']);
                        $query = $this->db->get();
                        $report = $query->row_array();
                        if (!empty($report)) {
                            $custDet['test'] = $report['name'];
                        }else{
                            $custDet['test'] = "";
                        }

                        $country = $this->db->select('*')->from('country')->where('id', $bookingDetails['destination_country'])->get()->row_array();
                        if (!empty($country)) {
                            $custDet['dest_country_name'] = $country['name'];
                        }else{
                            $custDet['dest_country_name'] = "";
                        }

                        // $custDet['date'] = $bookingDetails['date'];
                        $custDet['time'] = $bookingDetails['time'];
                        $custDet['booking_id'] = $bookingDetails['booking_id'];
                        $custDet['booking_member_id'] = $bookingDetails['booking_member_id'];
                        $custDet['clinic_id'] = $bookingDetails['clinic_id'];
                        $custDet['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
                        $custDet['report_id'] = $bookingDetails['report_id'];
                        $custDet['purpose_type'] = $bookingDetails['type'];
                        $custDet['destination_country'] = $bookingDetails['destination_country'];
                        $custDet['return_date'] = date('d-m-Y',strtotime($bookingDetails['return_date']));
                        $custDet['return_time'] = $bookingDetails['return_time'];
						//$custDet['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$custDet['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	//$custDet['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$custDet['proof_document'] = $bookingDetails['proof_document'];
						$custDet['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
                            $custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
                        }
                    }else{
                        $custDet['date'] = "";
                        $custDet['time'] = "";
                        $custDet['booking_id'] = "";
                        $custDet['booking_member_id'] = "";
                        $custDet['clinic_id'] = "";
                        $custDet['shippingpostal_code'] = "";
                        $custDet['report_id'] = "";
                        $custDet['purpose_type'] = "";
                        $custDet['destination_country'] = "";
                        $custDet['return_date'] = "";
                        $custDet['return_time'] = "";
                        $custDet['dest_country_name'] = "";
						//$custDet['date_of_birth'] = "";
		            	$custDet['vaccine_date'] = "";
		            	//$custDet['vaccine_id'] = "";
		            	$custDet['proof_document'] = "";
						$custDet['vaccine_name'] = "";
                        if(!empty($custDet['vaccine_id'])){
                            $custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
                        }
                    }
                }else{
                    $custDet['date'] = "";
                    $custDet['time'] = "";
                    $custDet['booking_id'] = "";
                    $custDet['booking_member_id'] = "";
                    $custDet['dest_country_name'] = "";
                    $custDet['clinic_id'] = "";
                    $custDet['report_id'] = "";
                    $custDet['shippingpostal_code'] = "";
                    $custDet['purpose_type'] = "";
                    $custDet['destination_country'] = "";
                    $custDet['return_date'] = "";
                    $custDet['return_time'] = "";
					//$custDet['date_of_birth'] = "";
					$custDet['vaccine_date'] = "";
					//$custDet['vaccine_id'] = "";
					$custDet['proof_document'] = "";
					$custDet['vaccine_name'] = "";
                    if(!empty($custDet['vaccine_id'])){
                        $custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
                    }
                }
            }

            $membersC = $membersDet->count();
            if ($membersC !=0) {
                $members[] = $custDet;
                // array_push($members,$custDet);
            }
        }
        $data['status'] = true;
        if($membersCount == 0){
            $data['message'] = "No members found";
        }else{
            $data['message'] = "";

        }
        $data['data'] = $members;

        $this->response($data);
    }

    public function addnewmember_intarrival_post() {
        $data = array();
        $this->form_validation->set_rules('full_name', 'full name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required'); 
        $this->form_validation->set_rules('city', 'City', 'required'); 
        $this->form_validation->set_rules('postcode', 'Post Code', 'required'); 
        $this->form_validation->set_rules('gender', 'Gender', 'required'); 
        $this->form_validation->set_rules('date_of_birth', 'Birth Date', 'required'); 
        $this->form_validation->set_rules('ethnicity', 'Ethnicity', 'required'); 
        $this->form_validation->set_rules('nationality', 'Nationality', 'required'); 
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');

        $this->form_validation->set_rules('parent_id', 'Parent Id', 'required');
        // $this->form_validation->set_rules('departure_country', 'Departure Country', 'required');
        $this->form_validation->set_rules('dest_country', 'Return Country', 'required');
        // $this->form_validation->set_rules('departure_date', 'Departure Date', 'required');
        // $this->form_validation->set_rules('departure_time', 'Departure Time', 'required');
        $this->form_validation->set_rules('report_id', 'Report Id', 'required');
        // $this->form_validation->set_rules('date', 'Test Date', 'required');
        // $this->form_validation->set_rules('time', 'Test Time', 'required');
        // $this->form_validation->set_rules('clinic_id', 'Clinic Id', 'required');
        $this->form_validation->set_rules('return_date', 'Return Date', 'required');
        $this->form_validation->set_rules('return_time', 'Return Time', 'required');
        // $post['vaccine_date']  = (isset($request['vaccine_date'])) ? (date_format(date_create($request['vaccine_date']),'Y-m-d')) : NULL;

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        } else {
            $request = $this->post();


            $email = $request['email']; 
            $phone = $request['phone']; 

            $password = (isset($request['password'])) ? $request['password'] : NULL;

            $query = $this->db->select('*')->from('users')->where('email', $request['email'])->or_where('phone', $request['phone'])->get();
            $userCount = $query->num_rows();
            $userDetails = $query->row_array();

            if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
                $type = explode('.', $_FILES["profile_picture"]["name"]);
                $type = strtolower($type[count($type) - 1]);
                $name = mt_rand(100000000, 999999999);
                $filename = $name . '.' . $type;
                $url = "assets/images/users/" . $filename;
                move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
                $folder = "assets/images/users/";
                s3Upload($url, $folder);
                $image = S3_URL.$url;
                $request['profile_picture'] = $image;
            }

            if ($userCount > 0) {
                // $parent_id = ($request['parent_id']== "0") ? $request['full_name'] : explode('', string) ;
                $userData = array(
                    // 'parent_id'          => $parent_id,
                    'full_name'         => $request['full_name'],
                    'email'             => $request['email'],
                    'address'           => $request['address'],
                    'postcode'          => $request['postcode'],
                    'date_of_birth'     => date('d-m-Y',strtotime($request['date_of_birth'])),
                    'gender'            => $request['gender'],
                    'ethnicity'         => $request['ethnicity'],
                    'passport'          => (isset($request['passport'])) ? $request['passport'] : NULL,
                    'driving_lic'       => (isset($request['driving_lic'])) ? $request['driving_lic'] : NULL,
                    'id_card'           => (isset($request['id_card'])) ? $request['id_card'] : NULL,
                    'city'              => $request['city'],
                    // 'phone'             => $request['phone'],
                    'nationality'       => $request['nationality'],
                    'country_code'    => (isset($request['country_code'])) ? $request['country_code'] : NULL,
                    'vaccine_status'    => (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL,
                    'relation_type'     => (isset($request['relation_type'])) ? $request['relation_type'] : NULL,
                    'excempted_cat'     => (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL,
                    'symptoms'          => (isset($request['symptoms'])) ? $request['symptoms'] : NULL,
                    'is_homeaddress'    => (isset($request['is_homeaddress'])) ? $request['is_homeaddress'] : 'no',
                    'has_covid'         => (isset($request['has_covid'])) ? $request['has_covid'] : NULL,
                    'date_of_travel'    => (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL,
                    'travel_destination'=> (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL,
					'vaccine_id'  		=> (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL,
                    'updated_at'        => current_date()
                );
				//_pre($userData);

                if (isset($request['profile_picture'])) {
                    $userData['profile_picture'] = $request['profile_picture'];
                }
                if (isset($request['password'])) {
                    $password = $this->ion_auth->hash_password($password);
                    $userData['password'] = $password;
                }
                unset($request['password']);

                $this->comman->update_record('users', $userData, $userDetails['id']);
                $profile = $this->comman->get_record_byid('users', $userDetails['id']);
                $id = $userDetails['id'];
            }else{
                if (isset($request['password'])) {
                    $password = $this->ion_auth->hash_password($password);
                    $userData['password'] = $password;
                }
                unset($request['email']);
                unset($request['password']);
                $id = $this->ion_auth->register($request['full_name'], $password, $email,$request, [2], 1);

            }
            // $user = User::find($id);
            $query  = $this->db->select('*')->from('users')->where('id', $id)->get();
            $user = $query->row_array();
           
            $booking = array();
            $post = array();

            if (isset($request['booking_id']) && $request['booking_id'] !="") {
                $booking_id = $request['booking_id'];
            }else{
                $books = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array(); 
                if (!empty($books)) {
                    $booking['booking_no'] = (int)($books['booking_no'])+1;
                }else{
                    $booking['booking_no'] = "100232100";
                }
                $booking['user_id'] = $request['parent_id'];
                $booking['type'] = 'international-arrival';
                $booking['report_price'] = 0;
                $booking['total_price'] = 0;
                $booking['tax_price'] = 0;
                $booking['booking_status'] = "in-complete";
                $booking_id = Bookings::insert($booking);
                $booking = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();

                $booking_id = $booking['id'];

            }
            // $booking = $this->db->select('*')->from('booking')->where('id', $booking_id)->or_where('user_id', $request['parent_id'])->get()->row_array();
            // $booking = $this->db->from('booking')->where('user_id',$request['user_id'])->order_by('id','DESC')->limit(1)->get()->row_array();
            if ($booking_id) {
                // $slot['slots'] = (isset($request['shipping_time'])) ? $request['shipping_time'] : NULL;
                // $slot['date'] = (isset($request['shipping_date'])) ? date_format(date_create($request['shipping_date']),'Y-m-d') : NULL;

                $post['booking_id'] = $booking_id;
                // GTETY0000001 
                $members = $this->db->from('booking_member')->like('reference_member_id','GTETY')->order_by('id','DESC')->limit(1)->get()->row_array();
                $report_sku = get_column_value("reports",array("id"=>$request['report_id']),"SKU");
                if (!empty($members)) {
                    $bookmemid = (int)($members['reference_member_id'])+1;   
                    $value2 = $members['reference_member_id'];
                    $explode_value2 = explode('-',$value2);
					$value2 = $explode_value2[0];
                    $value2 = substr($value2, 5, 12); //separating numeric part
                    $value2 = $value2 + 1; //Incrementing numeric part
                    $value2 = "GTETY" . sprintf('%07s', $value2); //concatenating incremented value
                    $bookmemid = $value2.'-'.$report_sku; 
                }else{
                    $bookmemid = "GTETY0000001-".$report_sku;
                }

                $post['reference_member_id'] = $bookmemid;
                $post['user_id'] = $user['id'];
                $post['clinic_id'] = (isset($request['clinic_id'])) ? $request['clinic_id'] : NULL;
                $post['home_address'] = $request['address'];

                $post['homepostal_code'] = $request['postcode'];
                $post['date_of_travel'] = (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL;
                $post['type'] = (isset($request['shipping_method']) && $request['shipping_method'] == "1") ? "clinic" : "home";
                $post['has_covid'] = (isset($request['has_covid'])) ? $request['has_covid'] : NULL;
                $post['travel_destination'] = (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL;
                $post['report_id'] = $request['report_id'];
                // $post['destination_country'] = $request['dest_country'];
                // $post['departure_country'] = $request['departure_country'];
                // $post['departure_date'] = (date_format(date_create($request['departure_date']),'Y-m-d'));
                // $post['departure_time'] = $request['departure_time'];
                $post['return_country'] = (isset($request['dest_country'])) ? $request['dest_country'] : NULL;
                $post['is_visiting_country'] = (isset($request['is_visiting_country'])) ? $request['is_visiting_country'] : NULL;
                $post['visiting_country'] = (isset($request['visiting_country'])) ? $request['visiting_country'] : NULL;

                // $post['second_report_id'] = (isset($request['second_report_id'])) ? $request['second_report_id'] : NULL;
                $post['return_date'] = (isset($request['return_date'])) ? (date_format(date_create($request['return_date']),'Y-m-d')) : NULL;
                $post['return_time'] = (isset($request['return_time'])) ? $request['return_time'] : NULL;
                // if (isset($request['transport_type'])) {
                //     $post['transport_type'] = (isset($request['transport_type'])) ? $request['transport_type'] : NULL;
                //     $post['transport_number'] = (isset($request['transport_number'])) ? $request['transport_number'] : NULL;
                // }
                $post['shipping_method']        = (isset($request['shipping_method'])) ? $request['shipping_method'] : NULL;
                $post['is_medical_excempted']   = (isset($request['is_medical_excempted'])) ? $request['is_medical_excempted'] : NULL;
                if (isset($request['shipping_method']) && $request['shipping_method'] == "2") {
                    $post['shipping_date']      = (isset($request['shipping_date']) && $request['shipping_date']!="") ? (date_format(date_create($request['shipping_date']),'Y-m-d')) : NULL;
                    $post['shipping_address']   = (isset($request['shippingaddress'])) ? $request['shippingaddress'] : NULL;
                    $post['address_line1']      = (isset($request['address_line1'])) ? $request['address_line1'] : NULL;
                    $post['address_line2']      = (isset($request['address_line2'])) ? $request['address_line2'] : NULL;
                    $post['address_line3']      = (isset($request['address_line3'])) ? $request['address_line3'] : NULL;
                    $post['shipping_city']      = (isset($request['shipping_city'])) ? $request['shipping_city'] : NULL;
                    $post['shipping_state']     = (isset($request['shipping_state'])) ? $request['shipping_state'] : NULL;
                    $post['shippingpostal_code']= (isset($request['shippingpostal_code'])) ? $request['shippingpostal_code'] : NULL;
                }

                $post['residency_flag'] = (isset($request['residency_flag'])) ? $request['residency_flag'] : NULL;

                $post['residency_postcode']     = (isset($request['residency_postcode'])) ? $request['residency_postcode'] : NULL;
                $post['residency_address']      = (isset($request['residency_address'])) ? $request['residency_address'] : NULL;
                $post['uk_isolation_address']   = (isset($request['uk_isolation_address'])) ? $request['uk_isolation_address'] : NULL;
                $post['uk_isolation_postcode']  = (isset($request['uk_isolation_postcode'])) ? $request['uk_isolation_postcode'] : NULL;
                $post['pickup_date']            = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;
                $post['vaccine_status']         = (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL;
                $post['relation_type']          = (isset($request['relation_type'])) ? $request['relation_type'] : NULL;
                $post['excempted_cat']          = (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL;
                $post['symptoms']               = (isset($request['symptoms'])) ? $request['symptoms'] : NULL;
				
				$post['date_of_birth'] = (isset($request['date_of_birth'])) ? (date_format(date_create($request['date_of_birth']),'Y-m-d')) : NULL;
                $post['vaccine_id']    = (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL;
                if (isset($_FILES['proof_document']) && !empty($_FILES['proof_document']['name'])) {
					$type = explode('.', $_FILES["proof_document"]["name"]);
					$type = strtolower($type[count($type) - 1]);
					$name = mt_rand(100000000, 999999999);
					$filename = $name . '.' . $type;
					$url = "assets/images/proof_document/" . $filename;
					move_uploaded_file($_FILES["proof_document"]["tmp_name"], $url);
					$folder = "assets/images/proof_document/";
					s3Upload($url, $folder);
					$image = S3_URL.$url;
					$post['proof_document'] = $image;
				}
                if(isset($request['proof_document']) && !empty($request['proof_document'])){
                    $post['proof_document'] = $request['proof_document'];
                }
                $booking_member_id  = BookingsMembers::insert($post);
                $bookingmember      = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array();

                if ($booking_member_id) {
                    $slot['booking_member_id'] = $bookingmember['id'];
                    $slot['slots'] =NULL;
                    $slot['date'] = NULL;
                    $slot_id = BookingSlots::insert($slot);
                }

                if (isset($request['vaccine_date']) && $request['vaccine_date'] != "") {
                    $vdate = str_replace('"','',$request['vaccine_date']);
                    if ($vdate !="") {                    
                        $vdate = explode(",",$vdate);
                        for ($i=0; $i < count($vdate); $i++) {
                            $vaccine['user_id'] =$user['id'];
                            $vaccine['booking_member_id'] = $bookingmember['id'];
                            $vaccine['vaccine_id'] = (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL;
                            $vaccine['vaccine_date'] = date_format(date_create($vdate[$i]),'Y-m-d');
                            $vaccine_id = BookingVaccine::insert($vaccine);
                        }
                    }

                }


                // if ($booking_member_id) {
                //     $slot['booking_member_id'] = $bookingmember['id'];
                //     $slot['slots'] =NULL;
                //     $slot['date'] = NULL;
                //     $slot_id = BookingSlots::insert($slot);
                // }
                    // }
                // }
            }
            if (isset($slot_id)) {
                if ($this->form_validation->run() == FALSE) {
                    $data['status'] = false;
                    $data['message'] = "Something went wrong.";
                } else {
                    $data['status'] = true;
                    $data['message'] = "New member added successfully";
                    $data['data']= $user;
                }

                $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
                $members1 = $query->result_array();

                // if (!empty($members1)) {
                    $data['members_list'] = true;
                // }else{
                //     $data['members_list'] = false;
                // }

                $this->response($data); 
            }else{
                $data['status'] = false;
                $data['message'] = "Something went wrong.";
            }

            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
            $members1 = $query->result_array();

            // if (!empty($members1)) {
                $data['members_list'] = true;
            // }else{
            //     $data['members_list'] = false;
            // }
        }
        $this->response($data);
    }

    public function purpose_type_post(){
        $data = array();
        $data['status'] = true;
        $data['booking_flag'] = false;
        $data['members_list'] = false;
        $request = $this->input->post();

        if (isset($request['user_id'])) {
            $str = "";
            $query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members = $query->result_array();

            $query1 = $this->db->select('*')->from('users')->where('id', $request['user_id'])->where_not_in('date_of_birth', '')->where('active',1)->get(); 
            $members1 = $query1->result_array();
            if (!empty($members1)) {
                $data['members_list'] = true;
            }

            if (!empty($members)) {
                $data['members_list'] = true;

            }

            $query = $this->db->select('*')->from('booking')->where('user_id', $request['user_id'])->where('booking_status','in-complete')->get(); 
            $booking = $query->result_array();

            if (!empty($booking)) {
                $data['booking_flag'] = true;
            }
        }
        $data['data'] = $this->config->item("purpose_type");
        $this->response($data);
    }

	public function upload_plf_form_post(){
		$request = $this->input->post();
		$this->form_validation->set_rules('booking_id', 'booking ID', 'required');
		$this->form_validation->set_rules('member_id', 'member ID', 'required');
		if(empty($_FILES['document']['name'])){
			$this->form_validation->set_rules('document', 'document', 'required');
		}
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = false;
            $data['error'] = $this->validation_errors_response();
        }else{
			$path_parts = pathinfo($_FILES["document"]["name"]);
			$type = $path_parts['extension'];
			if(strtolower($type) == "pdf"){
				$name = mt_rand(100000000, 999999999);
				$filename = $name . '.' . $type;
				$url = "assets/images/plf_document/" . $filename;
				if(!is_dir("assets/images/plf_document")){
					mkdir("assets/images/plf_document/");
				}
				move_uploaded_file($_FILES["document"]["tmp_name"], $url);
				$folder = "assets/images/plf_document/";
				s3Upload($url, $folder);
				$image = S3_URL.$url;
				$update_data['plf_document'] = $image;
				$this->db->update("booking_member",$update_data,array("booking_id"=>$request['booking_id'],"user_id"=>$request['member_id']));
				$data['status'] = true;
				$data['message'] = "PLF document upload successfully";
			}else{
				$data['status'] = false;
            	$data['error'] = "Only PDF file allowed";
			}
		}
		$this->response($data);
	}

    public function place_test_get() {
        $data = array();
        $data['status'] = true;
        $data['data'] = $this->config->item("place_test");
        $this->response($data);
    }

    // public function validation_errors_response() {
    //     $err_array = array();
    //     $err_str = "";
    //     $err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
    //     $err_str = ltrim($err_str, '|');
    //     $err_str = rtrim($err_str, '|');
    //     $err_array = explode('|', $err_str);
    //     $err_array = array_filter($err_array);
    //     return $err_array;
    // }

    public function validation_errors_response() {
        $err_array=array();
        $err_str="";
        $err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));
        $err_str=ltrim($err_str,'|');
        $err_str=rtrim($err_str,'|');
        $err_str=str_replace('|', '', trim($err_str));
        return $err_str;
    }   
}
