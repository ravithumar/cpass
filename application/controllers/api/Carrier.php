<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Carrier extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->helper('Api_helper');
		$this->load->model('CarrierTruck');
	}
	public function add_truck_post()
	{
		$this->form_validation->set_rules('truck_type', 'truck_type', 'required');
		$this->form_validation->set_rules('driver_id', 'driver_id', 'required');
		$this->form_validation->set_rules('make', 'make', 'required');
		$this->form_validation->set_rules('model', 'model', 'required');
		$this->form_validation->set_rules('condition', 'condition', 'required');
		$this->form_validation->set_rules('odometer', 'odometer', 'required');
		$this->form_validation->set_rules('color', 'color', 'required');
		$this->form_validation->set_rules('plate_number', 'plate_number', 'required');
		$this->form_validation->set_rules('max_weight', 'max_weight', 'required');
		$this->form_validation->set_rules('dimensions', 'dimensions', 'required');
		if ($this->form_validation->run() == FALSE)
        {
            $data['status']=false;
            $data['error'] =validation_errors_response(); 
        }   
        else
        {		
        		$request = $this->post();
        }
        $this->response($data);
	}
}
