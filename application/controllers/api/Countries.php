<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Countries extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		// $this->load->helper('Api_helper');
		$this->load->model('Country');
	}
	public function list_get()
	{
	
		$types = Country::select('id','name','days')->whereStatus(1)->orderBy('name','asc')->get();
		$data['status'] = true;
		$data['data'] = $types;
		$this->response($data);
	}
	public function demo_get()
    {

    	// DELETE 
        // $events = $this->db->select('booking.id, booking.booking_no,booking.booking_status,booking.total_price,booking.payment_status,booking.type,booking.total_price,booking_slots.date as slot_date,booking_slots.slots as slot_time, booking_slots.id as slot_id, booking.status,booking.booking_status,booking_member.id as booking_member_id,booking_member.reference_member_id,booking_member.report_id', false)
            // ->from("booking")
            // ->join('booking_member', 'booking_member.booking_id = booking.id')
            // ->join('booking_slots','booking_slots.booking_member_id = booking_member.id')
           // ->where($where)
            // ->limit('250')->group_by('booking.id')->get()->result_array();
            echo $this->db->last_query();
    }
}