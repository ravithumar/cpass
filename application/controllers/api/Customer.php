<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;

class Customer extends REST_Controller {
	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('Category');
		$this->load->model('Bookings');
        $this->load->model('BookingSession');
        $this->load->model('BookingSlots');
        $this->load->model('BookingsMembers');
		$this->load->model('Clinic');
		$this->load->model('CustomerVaccine');
		$this->load->model('Report');
		$this->load->model('ReportType');
		$this->load->model('Slot');
		$this->load->model('User');
		$this->load->helper('strp_helper');
		$this->load->model('ion_auth_model'); 
		// $this->load->library('strpie/Stripe'); 
		$this->load->library(['ion_auth', 'form_validation']);
	}

	public function ethnicitylist_get() {

		$current_date = current_date();
	
		$types = array();
		$query = $this->db->select('id,name')->from('ethnicity')
			->where('status', 1)
			->order_by('id', 'ASC')
			->get();
		if ($query->num_rows()>0)
		{
			$types = $query->result();
			$data['status'] = true;
			$data['data'] = $types;
		}else {
			$data['status'] = false; 
		}
			
		$this->response($data);
		
	}
	public function nationalitylist_get() {

		$current_date = current_date();

		$types = array();
		$query = $this->db->select('id,name')->from('nationality')
			->where('status', 1)
			->order_by('id', 'ASC')
			->get();
		if ($query->num_rows()>0)
		{
			$types = $query->result();
			$data['status'] = true;
			$data['data'] = $types;
		}else {
			$data['status'] = false; 
		}
			
		$this->response($data);
		
	}

	public function remove_incomplete_member_post(){
		// code...
	}

	public function addnewmember_fitoneway_post() {
		$data = array();
    	$this->form_validation->set_rules('full_name', 'full name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required'); 
		$this->form_validation->set_rules('city', 'City', 'required'); 
		$this->form_validation->set_rules('postcode', 'Post Code', 'required'); 
		$this->form_validation->set_rules('gender', 'Gender', 'required'); 
		$this->form_validation->set_rules('date_of_birth', 'Birth Date', 'required'); 
		$this->form_validation->set_rules('ethnicity', 'Ethnicity', 'required'); 
		$this->form_validation->set_rules('nationality', 'Nationality', 'required'); 
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');

		$this->form_validation->set_rules('dest_country', 'Destination Country', 'required');
		$this->form_validation->set_rules('departure_date', 'Departure Date', 'required');
		$this->form_validation->set_rules('departure_time', 'Departure Time', 'required');
		$this->form_validation->set_rules('date', 'Test Date', 'required');
		$this->form_validation->set_rules('time', 'Test Time', 'required');
		// $this->form_validation->set_rules('clinic_id', 'Clinic Id', 'required');
		$this->form_validation->set_rules('report_id', 'Report Id', 'required');
		// $this->form_validation->set_rules('purpose_type', 'Purpose Id', 'required');

        $this->form_validation->set_rules('parent_id', 'Parent Id', 'required');
        // $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
        // $members = $this->db->from('booking_member')->like('reference_member_id','GTETY')->order_by('id','DESC')->limit(1)->get()->row_array();
        // echo $this->db->last_query();	
		// exit();
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();

			$email = $request['email']; 
			$phone = $request['phone']; 

			$password = (isset($request['password'])) ? $request['password'] : NULL ;
						
			$query = $this->db->select('*')->from('users')->where('email', $request['email'])->or_where('phone', $request['phone'])->get();
            $userCount = $query->num_rows();
            $userDetails = $query->row_array();

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
		        $folder = "assets/images/users/";
		       	s3Upload($url, $folder);
		        $image = S3_URL.$url;
		        $request['profile_picture'] = $image;
            }

            if ($userCount > 0) {
				unset($request['password']);
            	// $parent_id = ($request['parent_id']== "0") ? $request['full_name'] : explode('', string) ;
	            $userData = array(
	                // 'parent_id'   		=> $parent_id,
	                'full_name'   		=> $request['full_name'],
	                'email'        		=> $request['email'],
	                'address'        	=> $request['address'],
	                'postcode'    		=> $request['postcode'],
	                'date_of_birth'    	=> date('d-m-Y',strtotime($request['date_of_birth'])),
	                'gender'       		=> $request['gender'],
	                'ethnicity'       	=> $request['ethnicity'],
	                'passport'       	=> (isset($request['passport'])) ? $request['passport'] : NULL,
	                'driving_lic'       => (isset($request['driving_lic'])) ? $request['driving_lic'] : NULL,
	                'id_card'       	=> (isset($request['id_card'])) ? $request['id_card'] : NULL,
	                'city'         		=> $request['city'],
					// 'phone'       		=> $request['phone'],
	                'nationality'       => $request['nationality'],
	                'country_code' 		=> (isset($request['country_code'])) ? $request['country_code'] : NULL,
	                'vaccine_status' 	=> (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL,
                    'relation_type' 	=> (isset($request['relation_type'])) ? $request['relation_type'] : NULL,
                    'excempted_cat' 	=> (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL,
                    'symptoms' 			=> (isset($request['symptoms'])) ? $request['symptoms'] : NULL,
                    'is_homeaddress' 	=> (isset($request['is_homeaddress'])) ? $request['is_homeaddress'] : 'no',
                    'has_covid' 		=> (isset($request['has_covid'])) ? $request['has_covid'] : NULL,
                    'date_of_travel' 	=> (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL,
                    'travel_destination'=> (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL,
					'vaccine_id'  		=> (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL,
	                'updated_at'   		=> current_date()
	            );

	            if (isset($request['profile_picture'])) {
	            	$userData['profile_picture'] = $request['profile_picture'];
	            }
	            if (isset($request['password'])) {
					$password = $this->ion_auth->hash_password($password);
	            	$userData['password'] = $password;
	            }

	            $this->comman->update_record('users', $userData, $userDetails['id']);
	            $profile = $this->comman->get_record_byid('users', $userDetails['id']);
				$id = $userDetails['id'];
            }else{
            	if (isset($request['password'])) {
					$password = $this->ion_auth->hash_password($password);
	            	$userData['password'] = $password;
	            }
            	unset($request['email']);
				unset($request['password']);
				$id = $this->ion_auth->register($request['full_name'], $password, $email,$request, [2], 1);

            }
            // $user = User::find($id);
			$query  = $this->db->select('id,parent_id,email,profile_picture,gender,date_of_birth,address,postcode,city,full_name,phone,country_code,vaccine_status,relation_type,excempted_cat,symptoms,has_covid')->from('users')->where('id', $id)->get();
            $user = $query->row_array();
	       
	        $booking = array();
	        $post = array();

	        if (isset($request['booking_id']) && $request['booking_id'] !="") {
	        	$booking_id = $request['booking_id'];
	        }else{
	        	$books = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array(); 
		        if (!empty($books)) {
		            $booking['booking_no'] = (int)($books['booking_no'])+1;
		        }else{
		            $booking['booking_no'] = "100232100";
		        }
		        $booking['user_id'] = $request['parent_id'];
		        $booking['type'] = 'oneway';
		        $booking['report_price'] = 0;
		        $booking['total_price'] = 0;
		        $booking['tax_price'] = 0;
		        $booking['booking_status'] = "in-complete";
		        $booking_id = Bookings::insert($booking);
        		$booking = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();

        		$booking_id = $booking['id'];

	        }
			// $booking = $this->db->select('*')->from('booking')->where('id', $booking_id)->or_where('user_id', $request['parent_id'])->get()->row_array();
	        // $booking = $this->db->from('booking')->where('user_id',$request['user_id'])->order_by('id','DESC')->limit(1)->get()->row_array();
	        if ($booking_id) {
	            // $member_details = json_decode($members, true);
	            // if (!empty($member_details)) {
	                // foreach ($member_details as $key => $v) { 
                $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
                $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;

                $post['booking_id'] = $booking_id;
                // GTETY0000001 
     //            $members = $this->db->from('booking_member')->like('reference_member_id','GTETY')->order_by('id','DESC')->limit(1)->get()->row_array();
     //            if (!empty($members)) {
     //                $bookmemid = (int)($members['reference_member_id'])+1;   
     //                $value2 = $members['reference_member_id'];
					// $explode_value2 = explode('-',$value2);
					// $value2 = $explode_value2[0];
     //                $value2 = substr($value2, 5, 12); //separating numeric part
     //                $value2 = $value2 + 1; //Incrementing numeric part
     //                $value2 = "GTETY" . sprintf('%07s', $value2); //concatenating incremented value
     //                $bookmemid = $value2; 
     //            }else{
     //                $bookmemid = "GTETY0000001";
     //            }
                $members = $this->db->from('booking_member')->like('reference_member_id','100')->order_by('id','DESC')->limit(1)->get()->row_array();
                // $books = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array(); 
                if (!empty($members)) {
                    $post['reference_member_id'] = ((int)$members['reference_member_id'])+1;
                }else{
                    $post['reference_member_id'] = "10010000";
                }

                // $post['reference_member_id'] = $bookmemid;
                $post['user_id'] = $user['id'];
                $post['clinic_id'] = (isset($request['clinic_id'])) ? $request['clinic_id'] : NULL;
                $post['home_address'] = $request['address'];
                if (isset($request['place_type']) && $request['place_type'] == 2) {
                    $post['shipping_date'] = (isset($request['date']) && $request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d')) : NULL;
                	$post['shipping_address'] = (isset($request['shippingaddress'])) ? $request['shippingaddress'] : NULL;
                	$post['address_line1'] = (isset($request['address_line1'])) ? $request['address_line1'] : NULL;
                	$post['address_line2'] = (isset($request['address_line2'])) ? $request['address_line2'] : NULL;
                	$post['address_line3'] = (isset($request['address_line3'])) ? $request['address_line3'] : NULL;
                	$post['shipping_city'] = (isset($request['shipping_city'])) ? $request['shipping_city'] : NULL;
                	$post['shipping_state'] = (isset($request['shipping_state'])) ? $request['shipping_state'] : NULL;
                	$post['shippingpostal_code'] = (isset($request['shippingpostal_code'])) ? $request['shippingpostal_code'] : NULL;
                }
                $post['homepostal_code'] = $request['postcode'];
                $post['date_of_travel'] = (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL;
                $post['type'] = (isset($request['place_type']) && $request['place_type'] == "1") ? "clinic" : "home";
                $post['has_covid'] = (isset($request['has_covid'])) ? $request['has_covid'] : NULL;
                $post['dob'] = (isset($request['date_of_birth'])) ? date('d-m-Y',strtotime($request['date_of_birth'])) : NULL;
                $post['travel_destination'] = (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL;
                $post['report_id'] = $request['report_id'];
                $post['destination_country'] = $request['dest_country'];
                $post['departure_date'] = $request['departure_date'];
                $post['departure_time'] = $request['departure_time'];

                $post['vaccine_status'] = (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL;
                $post['relation_type'] = (isset($request['relation_type'])) ? $request['relation_type'] : NULL;
                $post['excempted_cat'] = (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL;
                $post['symptoms'] = (isset($request['symptoms'])) ? $request['symptoms'] : NULL;
				$post['vaccine_id']   = (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL;
                $booking_member_id = BookingsMembers::insert($post);
                $bookingmember = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array();

                if ($booking_member_id) {
                    $slot['booking_member_id'] = $bookingmember['id'];
                    $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
                    $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;
                    $slot_id = BookingSlots::insert($slot);
                }
	                // }
	            // }
	        }
	        if (isset($slot_id)) {
	            // $data['status'] = true;
	            // $data['message'] = "Thank You for Booking.";
	            if ($this->form_validation->run() == FALSE) {
	                // $data['status'] = false;
	                // $data['message'] = validation_errors_response();
	                $data['status'] = false;
                	$data['message'] = "Something went wrong.";
	            } else {
	                $data['status'] = true;
					$data['message'] = "New member added successfully";
					$data['data']= $user;
	            }

	            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
	            $members1 = $query->result_array();

	            // if (!empty($members1)) {
                $data['members_list'] = true;
	            // }else{
	            //     $data['members_list'] = false;
	            // }

	            $this->response($data); 
	        }else{
                $data['status'] = false;
                $data['message'] = "Something went wrong.";
            }

            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
            $members1 = $query->result_array();

            // if (!empty($members1)) {
            $data['members_list'] = true;
            // }else{
            //     $data['members_list'] = false;
            // }
		}
		$this->response($data);
	}

	public function addnewmember_post() {
		$data = array();
    	$this->form_validation->set_rules('full_name', 'full name', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required'); 
		$this->form_validation->set_rules('city', 'City', 'required'); 
		$this->form_validation->set_rules('postcode', 'Post Code', 'required'); 
		$this->form_validation->set_rules('gender', 'Gender', 'required'); 
		$this->form_validation->set_rules('date_of_birth', 'Birth Date', 'required'); 
		$this->form_validation->set_rules('ethnicity', 'Ethnicity', 'required'); 
		$this->form_validation->set_rules('nationality', 'Nationality', 'required'); 
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required');

        $this->form_validation->set_rules('parent_id', 'Parent Id', 'required');
        // $this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
		// $this->form_validation->set_rules('password', 'password', 'required|min_length[8]');

		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();

			$email = $request['email']; 
			$phone = $request['phone']; 

			$password = (isset($request['password'])) ? $request['password'] : NULL ;

			$query = $this->db->select('*')->from('users')->where('email', $request['email'])->or_where('phone', $request['phone'])->get();
            $userCount = $query->num_rows();
            $userDetails = $query->row_array();
            // echo $this->db->last_query();

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {
				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
		        $folder = "assets/images/users/";
		       	s3Upload($url, $folder);
		        $image = S3_URL.$url;
		        $request['profile_picture'] = $image;
            }

            if ($userCount > 0) {
				unset($request['password']);
            	// unset($request['email']);
            	// $parent_id = ($request['parent_id']== "0") ? $request['full_name'] : explode('', string) ;
	            $userData = array(
	                // 'parent_id'   		=> $parent_id,
	                'full_name'   		=> $request['full_name'],
	                // 'email'        		=> $request['email'],
	                'address'        	=> $request['address'],
	                'postcode'    		=> $request['postcode'],
	                'date_of_birth'    	=> date('d-m-Y',strtotime($request['date_of_birth'])),
	                'gender'       		=> $request['gender'],
	                'ethnicity'       	=> $request['ethnicity'],
	                'passport'       	=> (isset($request['passport'])) ? $request['passport'] : NULL,
	                'driving_lic'       => (isset($request['driving_lic'])) ? $request['driving_lic'] : NULL,
	                'id_card'       	=> (isset($request['id_card'])) ? $request['id_card'] : NULL,
	                'city'         		=> $request['city'],
					// 'phone'       		=> $request['phone'],
	                'nationality'       => $request['nationality'],	                
	                'country_code' 		=> (isset($request['country_code'])) ? $request['country_code'] : NULL,
	                'vaccine_status' 	=> (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL,
                    'relation_type' 	=> (isset($request['relation_type'])) ? $request['relation_type'] : NULL,
        			//Added by Nayan 
        			'is_excempted' 		=>  $request['is_excempted'],
        			//End Addition
                    'excempted_cat' 	=> (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL,
                    'symptoms' 			=> (isset($request['symptoms'])) ? $request['symptoms'] : NULL,
                    'is_homeaddress' 	=> (isset($request['is_homeaddress'])) ? $request['is_homeaddress'] : 'no',
                    'shipping_address' 	=> (isset($request['shippingaddress'])) ? $request['shippingaddress'] : 'no',
                    'is_shippingaddress' 	=> (isset($request['is_shippingaddress'])) ? $request['is_shippingaddress'] : 'no',
                    'has_covid' 		=> (isset($request['has_covid'])) ? $request['has_covid'] : NULL,
                    'date_of_travel' 	=> (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL,
                    'travel_destination'=> (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL,
					'vaccine_id'  		=> (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL,
	                'updated_at'   		=> current_date()
	            );

	            if (isset($request['profile_picture'])) {
	            	$userData['profile_picture'] = $request['profile_picture'];
	            }
	            if (isset($request['password'])) {
					$password = $this->ion_auth->hash_password($password);
	            	// $userData['password'] = $password;
	            }

	            $this->comman->update_record('users', $userData, $userDetails['id']);
	            $profile = $this->comman->get_record_byid('users', $userDetails['id']);
				$id = $userDetails['id'];
				//$query  = $this->db->select('id,parent_id,email,profile_picture,gender,date_of_birth,address,postcode,city,full_name,phone,country_code,vaccine_status, relation_type,excempted_cat,symptoms,has_covid')->from('users')->where('id', $id)->get();
				$query  = $this->db->select('*')->from('users')->where('id', $id)->get();
            	$user = $query->row_array();
				$data['status'] = true;
				$data['message'] = "New member added successfully";
				$data['data']= $user;
	            $data['members_list'] = true;

            }else{
            	if (isset($request['password'])) {
					$password = $this->ion_auth->hash_password($password);
	            	$userData['password'] = $password;
	            }
            	unset($request['email']);
				unset($request['password']);
				$id = $this->ion_auth->register($request['full_name'], $password, $email,$request, [2], 1);
				
				//$query  = $this->db->select('id,parent_id,email,profile_picture,gender,date_of_birth,address,postcode,city,full_name,phone,vaccine_status,country_code, relation_type,excempted_cat,symptoms,has_covid')->from('users')->where('id', $id)->get();
				$query  = $this->db->select('*')->from('users')->where('id', $id)->get();
            	$user = $query->row_array();
				$data['status'] = true;
				$data['message'] = "New member added successfully";
				$data['data']= $user;
	            $data['members_list'] = true;

            }
            // $user = User::find($id);
			//$query  = $this->db->select('id,parent_id,email,profile_picture,gender,date_of_birth,address,postcode,city,full_name,phone,vaccine_status,country_code, relation_type,excempted_cat,symptoms,has_covid')->from('users')->where('id', $id)->get();
			$query  = $this->db->select('*')->from('users')->where('id', $id)->get();
            $user = $query->row_array();
	       
	        $booking = array();
	        $post = array();

	        if (isset($request['is_editprofile']) && $request['is_editprofile'] == 1) {
	        	// if ($request['is_editprofile'] == 1) {
		        	$data['status'] = true;
					$data['message'] = "New member added successfully";
					$data['data']= $user;
		            $data['members_list'] = true;

	        	}else{
		        	if (isset($request['booking_id']) && $request['booking_id'] !="") {
			        	$booking_id = $request['booking_id'];
			        }else{
			        	$books = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array(); 
				        if (!empty($books)) {
				            $booking['booking_no'] = ((int)$books['booking_no'])+1;
				        }else{
				            $booking['booking_no'] = "100232100";
				        }
				        $booking['user_id'] = $request['parent_id'];
				        $booking['type'] = 'domestic';
				        $booking['report_price'] = 0;
				        $booking['total_price'] = 0;
				        $booking['tax_price'] = 0;
				        $booking['booking_status'] = "in-complete";
				        $booking_id = Bookings::insert($booking);
		        		$booking = $this->db->from('booking')->order_by('id','DESC')->limit(1)->get()->row_array();
		        		$booking_id = $booking['id'];
			        }
					// $booking = $this->db->select('*')->from('booking')->where('id', $booking_id)->or_where('user_id', $request['parent_id'])->get()->row_array();
			        // $booking = $this->db->from('booking')->where('user_id',$request['user_id'])->order_by('id','DESC')->limit(1)->get()->row_array();
			        if ($booking_id) {
			            // $member_details = json_decode($members, true);
			            // if (!empty($member_details)) {
			                // foreach ($member_details as $key => $v) { 
		                $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
		                $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;

		                $post['booking_id'] = $booking_id;
		                // GTETY0000001 
		     //            $members = $this->db->from('booking_member')->like('reference_member_id','GTETY')->order_by('id','DESC')->limit(1)->get()->row_array();
		     //            if (!empty($members)) {
		     //                $bookmemid = (int)($members['reference_member_id'])+1;   
		     //                $value2 = $members['reference_member_id'];
							// $explode_value2 = explode('-',$value2);
							// $value2 = $explode_value2[0];
		     //                $value2 = substr($value2, 5, 12); //separating numeric part
		     //                $value2 = (int)$value2 + 1; //Incrementing numeric part
		     //                $value2 = "GTETY" . sprintf('%07s', $value2); //concatenating incremented value
		     //                $bookmemid = NULL; 
		     //                // $bookmemid = $value2; 
		     //            }else{
		     //                // $bookmemid = "GTETY0000001";
							// $bookmemid = NULL;
		     //            }
		                $members = $this->db->from('booking_member')->like('reference_member_id','100')->order_by('id','DESC')->limit(1)->get()->row_array();
		                // $books = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array(); 
		                if (!empty($members)) {
		                    $post['reference_member_id'] = ((int)$members['reference_member_id'])+1;
		                }else{
		                    $post['reference_member_id'] = "10010000";
		                }

		                $post['user_id'] = $user['id'];
		                $post['clinic_id'] = (isset($request['clinic_id'])) ? $request['clinic_id'] : NULL;
		                $post['home_address'] = $request['address'];
		                if (isset($request['place_type']) && $request['place_type'] == 2) {
							// $post['reference_member_id'] = $bookmemid;
		                    $post['shipping_date'] = (isset($request['date']) && $request['date']!="") ? (date_format(date_create($request['date']),'Y-m-d')) : NULL;
		                    $post['shippingpostal_code'] = (isset($request['shippingpostal_code']) && $request['shippingpostal_code']!="") ? ($request['shippingpostal_code']) : NULL;
		                	$post['shipping_address'] = (isset($request['shippingaddress'])) ? $request['shippingaddress'] : NULL;
		                	$post['address_line1'] = (isset($request['address_line1'])) ? $request['address_line1'] : NULL;
		                	$post['address_line2'] = (isset($request['address_line2'])) ? $request['address_line2'] : NULL;
		                	$post['address_line3'] = (isset($request['address_line3'])) ? $request['address_line3'] : NULL;
		                	$post['shipping_city'] = (isset($request['shipping_city'])) ? $request['shipping_city'] : NULL;
		                	$post['shipping_state'] = (isset($request['shipping_state'])) ? $request['shipping_state'] : NULL;
		                }else{
		                	$post['reference_member_id'] = NULL;
		                }
		                $post['homepostal_code'] = (isset($request['postcode'])) ? $request['postcode'] : NULL;
		                $post['date_of_travel'] = (isset($request['date_of_travel']) && $request['date_of_travel']!="") ? (date_format(date_create($request['date_of_travel']),'Y-m-d')) : NULL;
		                $post['type'] = (isset($request['place_type']) && $request['place_type'] == "1") ? "clinic" : "home";
		                $post['has_covid'] = (isset($request['has_covid'])) ? $request['has_covid'] : NULL;
		                $post['travel_destination'] = (isset($request['travel_destination'])) ? $request['travel_destination'] : NULL;
		                $post['report_id'] = $request['report_id'];
		                $post['vaccine_status'] = (isset($request['vaccine_status'])) ? $request['vaccine_status'] : NULL;
		                $post['relation_type'] = (isset($request['relation_type'])) ? $request['relation_type'] : NULL;
		                $post['excempted_cat'] = (isset($request['excempted_cat'])) ? $request['excempted_cat'] : NULL;
		                $post['symptoms'] = (isset($request['symptoms'])) ? $request['symptoms'] : NULL;
						$post['vaccine_id']   = (isset($request['vaccine_id'])) ? $request['vaccine_id'] : NULL;
		                $booking_member_id = BookingsMembers::insert($post);
		                $bookingmember = $this->db->from('booking_member')->order_by('id','DESC')->limit(1)->get()->row_array();

		                if ($booking_member_id) {
		                    $slot['booking_member_id'] = $bookingmember['id'];
		                    $slot['slots'] = (isset($request['time'])) ? $request['time'] : NULL;
		                    $slot['date'] = (isset($request['date'])) ? date_format(date_create($request['date']),'Y-m-d') : NULL;
		                    $slot_id = BookingSlots::insert($slot);
		                }
			                // }
			            // }
			        }
			        if (isset($slot_id)) {
			            // $data['status'] = true;
			            // $data['message'] = "Thank You for Booking.";
			            if ($this->form_validation->run() == FALSE) {
			                // $data['status'] = false;
			                // $data['message'] = validation_errors_response();
			                $data['status'] = false;
		                	$data['message'] = "Something went wrong.";
				            $data['members_list'] = true;

			            } else {

				            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
				            $members1 = $query->result_array();

				            // if (!empty($members1)) {
				            $data['members_list'] = true;
				            // }else{
				            //     $data['members_list'] = false;
				            // }
			                $data['status'] = true;
							$data['message'] = "New member added successfully";
							$data['data']= $user;
		            		$data['members_list'] = true;

			            }

			            $this->response($data); 
			        }else{
		                $data['status'] = false;
		                $data['message'] = "Something went wrong.";
		            	$data['members_list'] = true;
		            }

		            $query = $this->db->select('*')->from('users')->where('parent_id', $request['parent_id'])->where('active',1)->get(); 
		            $members1 = $query->result_array();
		            $data['members_list'] = true;

		            // if (!empty($members1)) {
		            // }else{
		            //     $data['members_list'] = false;
		            // }
		        }
        	// }
		}
		$this->response($data);
	}

	public function remove_selected_member_post(){
    	$this->form_validation->set_rules('booking_id', 'Booking Id', 'required');
    	$this->form_validation->set_rules('user_id', 'user Id', 'required');
    	if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$query   = $this->db->select('id as booking_member_id')->from('booking_member')->where('booking_id', $request['booking_id'])->where('user_id', $request['user_id'])->get();
            $user = $query->row_array();

            // echo $this->db->last_query();exit();
            if (!empty($user)) {
            	$booking_member_id = $user['booking_member_id'];

            	$this->db->where('booking_member_id', $user['booking_member_id']);
				$this->db->delete('booking_slots');

				$this->db->where('booking_id', $request['booking_id']);
				$this->db->where('user_id', $request['user_id']);
				$this->db->delete('booking_member');  

				$query   = $this->db->select('id as booking_member_id')->from('booking_member')->where('booking_id', $request['booking_id'])->get();
	            $bookingData = $query->row_array(); 
				if (empty($bookingData)) {
					$this->db->where('id', $request['booking_id']);
					$this->db->delete('booking');
				}
            }
            $data['status'] = true;
			$data['message'] = "Member removed successfully";
		}
		$this->response($data);
	}

	public function removemember_post() {
    	$this->form_validation->set_rules('user_id', 'User ID', 'required');
    	$this->form_validation->set_rules('member_id', 'Member ID', 'required');
    	if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();

			$str = "find_in_set('".$request['user_id']."',parent_id) AND 1=1";
			$this->db->select('*');
	        $this->db->from('users');
	        $this->db->where('id',$request['member_id']);
	        $this->db->where($str);
	        $query = $this->db->get();
	        $data = $query->row_array();
			// echo $this->db->last_query();

			if (!empty($data)) {
				$arr = explode(",", $data['parent_id']);
			    foreach ($arr as $key => $val){
			        if ($val == $request['user_id']){
			           unset($arr[$key]);
			        }
			    }
			    if (!empty($arr)) { 
					$arr = implode(",", $arr);  
			    }else{
			    	$arr = "0";
			    }
			    $update_key['parent_id'] = $arr;
				User::whereId($request['member_id'])->update($update_key);
			}
			$query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members1 = $query->result_array();

            if (!empty($members1)) {
                $data['members_list'] = true;
            }else{
                $data['members_list'] = false;
            }
		}
		$this->db->select('id as user_id,email,parent_id');
        $this->db->from('users');
        $this->db->where('id',$request['member_id']);
        $query = $this->db->get();
        $data1 = $query->row_array();

        $data['status'] = true;
        $data['message'] = 'Member removed successfully';
		$this->response($data);

	}

	public function stripe($customer_id = '', $grand_total = '')
    {
    	// echo "str";exit();
    	// $customer_id = "";
        $payment_mode = $this->config->item('mode','stripe');
        if ($payment_mode == 'live') {
            $sk = $this->config->item('sk_live','stripe');
        } else
        {
            $sk = $this->config->item('sk_test','stripe');
        }
        \Stripe\Stripe::setApiKey($sk);

        if($customer_id == "")
        {
            $customer = \Stripe\Customer::create();
            $customer_id = $customer->id;
        } else
        {
            $customer_id = $customer_id;
        }         
        $ephemeralKey = \Stripe\EphemeralKey::create(
           ['customer' => $customer_id],
           ['stripe_version' => '2020-08-27']
         );
        $paymentIntent = \Stripe\PaymentIntent::create([
           'amount' => (int)$grand_total,
           'currency' => 'usd',	
           'customer' => $customer_id
         ]);

        return $return_array = array(
          'paymentIntent' => $paymentIntent->client_secret,
           'ephemeralKey' => $ephemeralKey->secret,
           'customer_id' => $customer_id
        );
        // print_r($return_array);
         // return true;
    }

    public function stripe_post() {
    	$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('grand_total', 'Grand Total', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$request = $this->input->post();
			$return_array = [];
			$is_booking = false;
			
    		$user_data = User::whereId($request['user_id'])->first();
    		if (!empty($user_data)) {
    			if ($user_data->stripe_key == '' && empty($user_data->stripe_key)) {
    				$return_array = $this->stripe("", (int)$request['grand_total']);
    				$update_key['stripe_key'] = $return_array['customer_id'];
    				User::whereId($request['user_id'])->update($update_key);
    			}else{
    				$return_array = $this->stripe($user_data->stripe_key, (int)$request['grand_total']);
    			}
    			$data['status'] = true;
				$data['message'] = "Successfully";
				$data['data'] = $return_array;
    		} else
    		{
    			$data['status'] = false;
				$data['message'] = "User not found.";
    		}
		}

		$this->response($data);		
    }

	public function excempted_categories_get()
	{
	
		$categories = Category::select('id','name')->whereStatus(1)->orderBy('name','asc')->get();
		
		$data['status'] = true;
		$data['data'] = $categories;
		$this->response($data);
	}

	function getTimeSlot($interval, $start_time, $end_time)
	{
		$start = new DateTime($start_time);
		$end = new DateTime($end_time);
		$startTime = $start->format('H:i');
		$endTime = $end->format('H:i');
		$i=0;
		$time = [];
		while(strtotime($startTime) <= strtotime($endTime)){
			$start = $startTime;
			$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($startTime)));
			$startTime = date('H:i',strtotime('+'.$interval.' minutes',strtotime($startTime)));
			$i++;
			if(strtotime($startTime) <= strtotime($endTime)){
				$time[$i]['slot_time'] = $start;
				$time[$i]['is_booked'] = 0;
				// $time[$i]['slot_start_time'] = $start;
				// $time[$i]['slot_end_time'] = $end;
			}
		}
		return $time;
	}

	public function time_slot_post()
	{
		$this->form_validation->set_rules('report_type', 'Report type', 'required');
		$this->form_validation->set_rules('slot_date', 'Slot date', 'required');
		$this->form_validation->set_rules('hospital_id', 'Hospital ID', 'required');
		// $this->form_validation->set_rules('lab_id', 'Lab ID', 'required');
		// $this->form_validation->set_rules('slot_time', 'Slot time', 'required|in_list[morning,afternoon]');

		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$data['status'] = true;
			$request = $this->post();
			$date = strtotime($request['slot_date']);
			$request['slot_date'] = date('Y-m-d',strtotime($request['slot_date']));
			$day = strtolower(date('D', $date));
			// $status = $request['slot_time'].'_status';
			$morning_available_time = Slot::where('user_id',$request['hospital_id'])->where('day',$day)->where('morning_status', 1)->first();
			$afternoon_available_time = Slot::where('user_id',$request['hospital_id'])->where('day',$day)->where('afternoon_status', 1)->first();
			$report_detail = Report::find($request['report_type']);
			$bookings = BookingsMembers::where(['clinic_id'=>$request['hospital_id'], 'report_id'=>$request['report_type']])->get()->toArray();
			// _pre($bookings);
			
			$morning_start = isset($morning_available_time) ? $morning_available_time->morning_start : '08:00';
			$morning_end = isset($morning_available_time) ? $morning_available_time->morning_end : '12:00';
			$afternoon_start = isset($afternoon_available_time) ? $afternoon_available_time->afternoon_start : '12:05';
			$afternoon_end = isset($afternoon_available_time) ? $afternoon_available_time->afternoon_end : '16:00';
			$slots = [];

			$slots['morning'] = $this->getTimeSlot($report_detail->minutes,$morning_start, $morning_end);
			foreach($bookings as $key => $booking){
				$booking_slot = $this->db->select('*')->from('booking_slots')->where('booking_member_id',$booking['id'])->where('date',$request['slot_date'])->get()->row_array();
				$bookings[$key]['slot'] = $booking_slot['slots'];
			}
			// _pre($bookings);

			foreach($bookings as $key => $booking){
				foreach($slots['morning'] as $key => $slot){
					if(isset($booking['slot']) && $booking['slot'] == $slot['slot_time']){						
						$slots['morning'][$key]['is_booked'] = 1;
					}
				}
			}

			$slots['morning'] = array_values($slots['morning']);
			// _pre($slots);

			$slots['afternoon'] = $this->getTimeSlot($report_detail->minutes,$afternoon_start, $afternoon_end);
			foreach($bookings as $key => $booking){
				$booking_slot = $this->db->select('*')->from('booking_slots')->where('booking_member_id',$booking['id'])->where('date',$request['slot_date'])->get()->row_array();
				$bookings[$key]['slot'] = $booking_slot['slots'];
			}

			foreach($bookings as $key => $booking){
				foreach($slots['afternoon'] as $key => $slot){
					if(isset($booking['slot']) && $booking['slot'] == $slot['slot_time']){						
						$slots['afternoon'][$key]['is_booked'] = 1;
					}
				}
			}

			$slots['afternoon'] = array_values($slots['afternoon']);

			$finalArr = array_merge($slots['morning'],$slots['afternoon']);
			// _pre($finalArr);
			$data['slots'] = $finalArr;
		}
		$this->response($data);
	}

	public function booking_information_post()
	{
		$this->form_validation->set_rules('test_purpose', 'Purpose of test', 'required');
		$this->form_validation->set_rules('vaccination_status', 'Vaccination status', 'required');
		$this->form_validation->set_rules('customer_id', 'Customer ID', 'required');
		$this->form_validation->set_rules('report_id', 'Report ID', 'required');
		$this->form_validation->set_rules('hospital_id', 'Hospital ID', 'required');
		$this->form_validation->set_rules('lab_id', 'Lab ID', 'required');
		$this->form_validation->set_rules('slot_date', 'Slot date', 'required');
		$this->form_validation->set_rules('slot_time', 'Slot time', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$data['status'] = true;
			$request = $this->post();
			$vaccination_status = CustomerVaccine::where('customer_id', $request['customer_id'])->first();
			$data['data']['purpose_of_test'] = $request['test_purpose'];
			$data['data']['vaccination_status'] = $request['vaccination_status'];
			if(isset($request['going_to_country'])){
				$data['data']['going_to_country'] = $request['going_to_country'];			
			}
			if(isset($request['is_excempted'])){
				$data['data']['is_excempted'] = $request['is_excempted'];			
			}
			if(isset($request['excemption_category'])){
				$excemption_category = Category::find($request['excemption_category']);
				$data['data']['excemption_category'] = $excemption_category->name;			
			}	
			$report_detail = Report::find($request['report_id']);
			$data['data']['report_name'] = $report_detail->name;
			$hospital_detail = Clinic::find($request['hospital_id']);
			$data['data']['hospital_name'] = $hospital_detail->company_name;
			$input_date = strtotime($request['slot_date']);
			$date = strtolower(date('F d Y', $input_date));
			$data['data']['date'] = $date;
			$slot_session = $request['slot_session'] == 'morning' ? 'AM' : 'PM'; 
			$data['data']['time'] = $request['slot_time'].' '.$slot_session;			
			// $data['data'] = [];
		}
		$this->response($data);
	}

	public function profile_update_post(){

		$this->form_validation->set_rules('user_id', 'user ID', 'required');
		$this->form_validation->set_rules('full_name', 'full name', 'required');
		// $this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('postal_code', 'post code', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		// $this->form_validation->set_rules('phone', 'phone', 'required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('nhs_number', 'NHS number', 'required');
		$this->form_validation->set_rules('date_of_birth', 'Birth Date', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
            $current_date = current_date();

            $request = $this->input->post();

            $user_details = $this->db->select('*',false)->from('users')->where('users.id', $request['user_id'])->get()->row_array();

			$customer_details = $this->db->select('*',false)->from('customer')->where('user_id', $request['user_id'])->get()->row_array();

			if (isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])) {

				$type = explode('.', $_FILES["profile_picture"]["name"]);
		        $type = strtolower($type[count($type) - 1]);
		        $name = mt_rand(100000000, 999999999);
		        $filename = $name . '.' . $type;
		        $url = "assets/images/users/" . $filename;
		        move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $url);
		        $folder = "assets/images/users/";
		       	s3Upload($url, $folder);
		        $image = S3_URL.$url;
		        $request['profile_picture'] = $image;
            }

            $user_data = array(
                'full_name'   => $request['full_name'],
                // 'email'        => $request['email'],
                'address'      => $request['address'],
                'city'         => $request['city'],
                'postcode'  => $request['postal_code'],
                'gender'       => $request['gender'],
                'date_of_birth'       => $request['date_of_birth'],
                'ethnicity'       => $request['ethnicity'],
                'passport'       => isset($request['passport']) ? $request['passport'] : $user_details['passport'],
                'nationality'       => isset($request['nationality']) ? $request['nationality'] : $user_details['nationality'],
                'profile_picture'       => isset($request['profile_picture']) ? $request['profile_picture'] : $user_details['profile_picture'],
                'nhs_number'   => $request['nhs_number'],
				// 'phone'        => $request['phone'],
                'updated_at'   => $current_date
            );

			$customer_data = array(
                'address'      => $request['address'],
                'city'         => $request['city'],
                'postal_code'  => $request['postal_code'],
                'gender'       => $request['gender'],
				// 'phone'        => $request['phone'],
                'nhs_number'   => $request['nhs_number'],
            );

    //         if (isset($_FILES['passport']) && !empty($_FILES['passport'])) {
    //             $image_is_uploaded = image_upload('passport', 'images/customers/', TRUE, 'jpg|JPG|png|PNG|jpeg|JPEG');
				// if (isset($image_is_uploaded['status']) && $image_is_uploaded['status']) {
				// 	$customer_data['passport'] = $image_is_uploaded['uploaded_path'];
				// } else if (isset($image_is_uploaded['error'])) {
				// 	$data['status'] = FALSE;
				// 	$data['message'] = ucfirst($image_is_uploaded['error']);
				// 	$this->response($data);
				// 	die();
				// }
    //         }

            $this->comman->update_record('users', $user_data, $request['user_id']);
            $this->db->update('customer', $customer_data, array('user_id' => $request['user_id']));
            $profile = $this->comman->get_record_byid('users', $request['user_id']);

			$customer_profile = $this->db->select('id,parent_id,email,phone,passport,nhs_number,profile_picture,gender,date_of_birth,address,postcode,city,ethnicity,full_name,nationality,,vaccine_status,relation_type,excempted_cat,symptoms,has_covid',false)->from('users')->where('id', $request['user_id'])->get()->row_array();

            $data['status'] = TRUE;
            $data['message'] = 'Profile updated successfully';
            $token_data = $this->db->get_where('tokens', array('user_id' => $request['user_id']))->row_array();
            $profile['api_key'] = $token_data['token'];
            $data['data'] = $profile;
            // $data['customer_profile'] = $customer_profile;
        }
        $this->response($data);
    }

    public function getprofile_post(){
    	$user_details = array();
		$this->form_validation->set_rules('user_id', 'user ID', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
	        $request 	  = $this->input->post();
            $user_details = $this->db->select('id,parent_id,email,phone,country_code,passport,nhs_number,profile_picture,gender,date_of_birth,address,postcode,city,ethnicity,full_name,nationality',false)->from('users')->where('id', $request['user_id'])->get()->row_array();
        }
        $data['status'] = TRUE;
        $data['data'] = $user_details;
        $this->response($data);
    }

	public function _generate_key($uid)
    {

        do {

            // Generate a random salt

            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method

            if ($salt === FALSE) {

                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        } while ($this->_key_exists($new_key));

        $this->_insert_key($new_key, ['level' => 1, 'ignore_limits' => 1, 'user_id' => $uid]);

        return $new_key;
    }

	private function _key_exists($key)
    {

        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function _insert_key($key, $data)
    {

        $data[config_item('rest_key_column')] = $key;

        $data['date_created'] = function_exists('now') ? now() : time();

        $check_key = $this->db->get_where('tokens', array('user_id' => $data['user_id']))->row();

        if (empty($check_key)) {

            return $this->rest->db
                ->set($data)
                ->insert(config_item('rest_keys_table'));
        } else {

            $this->_update_key($key, $data['user_id']);
        }
    }

	private function _update_key($key, $uid)
    {

        return $this->rest->db
            ->where('user_id', $uid)
            ->update(config_item('rest_keys_table'), array('date_created' => time(), 'token' => $key));
    }

	// public function validation_errors_response() {
	// 	$err_array = array();
	// 	$err_str = "";
	// 	$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
	// 	$err_str = ltrim($err_str, '|');
	// 	$err_str = rtrim($err_str, '|');
	// 	$err_array = explode('|', $err_str);
	// 	$err_array = array_filter($err_array);
	// 	return $err_array;
	// }

	public function validation_errors_response() {
		$err_array=array();
    	$err_str="";
    	$err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));
    	$err_str=ltrim($err_str,'|');
    	$err_str=rtrim($err_str,'|');
    	$err_str=str_replace('|', '', trim($err_str));
        return $err_str;
	}

	public function members_list_post()
	{
		$members = array();

		$this->form_validation->set_rules('user_id', 'user ID', 'required');

		if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
			$request = $this->input->post();
			$members = User::select('*')->where(['parent_id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();
			$membersCount = $members->count();
			$membersDet = User::select('*')->where(['id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();

			$query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members = $query->result_array();

            if (!empty($members)) {
            	foreach ($members as $key => $value) {
            		$this->db->select('booking_member.report_id,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code, users.date_of_birth, booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
		            $this->db->from('booking');
		            $this->db->join('booking_member','booking.id = booking_member.booking_id');
		            // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
		            $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
		            $this->db->join('users','booking_member.user_id = users.id');
		            $this->db->where('booking.user_id',$request['user_id']);
		            $this->db->where('booking_member.user_id',$value['id']);
		            $this->db->where('booking.booking_status',"in-complete");
		            $this->db->where('users.active',1);
		            $this->db->order_by('booking.id','DESC');
		            $query = $this->db->get();
		            $bookingDetails = $query->row_array();
		            // echo $this->db->last_query();
	            	
		            if (!empty($bookingDetails)) {
		            	$members[$key]['date'] = $bookingDetails['date'];
		            	$members[$key]['time'] = $bookingDetails['time'];
		            	$members[$key]['booking_id'] = $bookingDetails['booking_id'];
		            	$members[$key]['booking_member_id'] = $bookingDetails['booking_member_id'];
		            	$members[$key]['clinic_id'] = $bookingDetails['clinic_id'];
		            	$members[$key]['report_id'] = $bookingDetails['report_id'];
		            	$members[$key]['purpose_type'] = $bookingDetails['type'];
		            	$members[$key]['place_type'] = $bookingDetails['place_type'];
		            	$members[$key]['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
		            	$members[$key]['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$members[$key]['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	$members[$key]['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$members[$key]['proof_document'] = $bookingDetails['proof_document'];
						$members[$key]['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}
		            	$this->db->select('*');
	            		$this->db->from('hospital');
	            		$this->db->where('id',$bookingDetails['clinic_id']);
	            		$query = $this->db->get();
			            $hospital = $query->row_array();
			            if (!empty($hospital)) {
			            	$members[$key]['clinic_name'] = $hospital['company_name'];
			            }else{
			            	$members[$key]['clinic_name'] = "";
			            }
	            		$this->db->select('*');
	            		$this->db->from('reports');
	            		$this->db->where('id',$bookingDetails['report_id']);
	            		$query = $this->db->get();
			            $report = $query->row_array();
			            if (!empty($report)) {
			            	$members[$key]['test'] = $report['name'];
			            }else{
			            	$members[$key]['test'] = "";
			            }
		            }else{
		            	$members[$key]['date'] = "";
		            	$members[$key]['time'] = "";
		            	$members[$key]['booking_id'] = "";
		            	$members[$key]['booking_member_id'] = "";
		            	$members[$key]['clinic_id'] = "";
		            	$members[$key]['shippingpostal_code'] = "";
		            	$members[$key]['report_id'] = "";
		            	$members[$key]['purpose_type'] = "";
		            	$members[$key]['clinic_name'] = "";
		            	$members[$key]['test'] = "";
		            	$members[$key]['place_type'] = "";
						//$members[$key]['date_of_birth'] = "";
		            	$members[$key]['vaccine_date'] = "";
		            	//$members[$key]['vaccine_id'] = "";
		            	$members[$key]['proof_document'] = "";
						$members[$key]['vaccine_name'] = "";
						if(!empty($value['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$value['vaccine_id']),"name");
						}
		            }

            	}
            }
	        $query = $this->db->select('*')->from('users')->where('id', $request['user_id'])->get(); 
            $custDet = $query->row_array();

            if (!empty($custDet)) {
            	$this->db->select('booking_member.report_id,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type, booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id,booking_member.shippingpostal_code,booking_member.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
	            $this->db->from('booking');
	            $this->db->join('booking_member','booking.id = booking_member.booking_id');
	            // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
	            $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
	            $this->db->join('users','booking_member.user_id = users.id');
	            $this->db->where('booking.user_id',$request['user_id']);
	            $this->db->where('booking_member.user_id',$custDet['id']);
	            $this->db->where('booking.booking_status',"in-complete");
	            $this->db->where('users.active',1);
	            $this->db->order_by('booking.id','DESC');
	            $query = $this->db->get();
	            $bookingDetails = $query->row_array();

	            if (!empty($bookingDetails)) {
	            	if (!empty($bookingDetails)) {

	            		$this->db->select('*');
	            		$this->db->from('hospital');
	            		$this->db->where('id',$bookingDetails['clinic_id']);
	            		$query = $this->db->get();
			            $hospital = $query->row_array();
			            if (!empty($hospital)) {
			            	$custDet['clinic_name'] = $hospital['company_name'];
			            }else{
			            	$custDet['clinic_name'] = "";
			            }
	            		$this->db->select('*');
	            		$this->db->from('reports');
	            		$this->db->where('id',$bookingDetails['report_id']);
	            		$query = $this->db->get();
			            $report = $query->row_array();
			            if (!empty($report)) {
			            	$custDet['test'] = $report['name'];
			            }else{
			            	$custDet['test'] = "";
			            }

	            		// $this->db->select('*');
		            	$custDet['date'] = $bookingDetails['date'];
		            	$custDet['time'] = $bookingDetails['time'];
		            	$custDet['booking_id'] = $bookingDetails['booking_id'];
		            	$custDet['booking_member_id'] = $bookingDetails['booking_member_id'];
		            	$custDet['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
		            	$custDet['clinic_id'] = $bookingDetails['clinic_id'];
		            	$custDet['report_id'] = $bookingDetails['report_id'];
		            	$custDet['purpose_type'] = $bookingDetails['type'];
		            	$custDet['place_type'] = $bookingDetails['place_type'];
						//$custDet['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$custDet['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	//$custDet['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$custDet['proof_document'] = $bookingDetails['proof_document'];
						$custDet['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}
		            	// code...
		            }else{
		            	$custDet['date'] = "";
			        	$custDet['time'] = "";
			        	$custDet['booking_id'] = "";
			        	$custDet['booking_member_id'] = "";
			        	$custDet['clinic_id'] = "";
		            	$custDet['shippingpostal_code'] = "";
			        	$custDet['report_id'] = "";
			        	$custDet['purpose_type'] = "";
			        	$custDet['place_type'] = "";
						//$custDet['date_of_birth'] = "";
		            	$custDet['vaccine_date'] = "";
		            	//$custDet['vaccine_id'] = "";
		            	$custDet['proof_document'] = "";
						$custDet['vaccine_name'] = "";
						if(!empty($custDet['vaccine_id'])){
							$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
						}
		            }
	            }else{
	            	$custDet['date'] = "";
		        	$custDet['time'] = "";
		        	$custDet['booking_id'] = "";
		        	$custDet['booking_member_id'] = "";
		            $custDet['shippingpostal_code'] = "";
		        	$custDet['clinic_id'] = "";
		        	$custDet['report_id'] = "";
		        	$custDet['purpose_type'] = "";
		        	$custDet['place_type'] = "";
					//$custDet['date_of_birth'] = "";
					$custDet['vaccine_date'] = "";
					//$custDet['vaccine_id'] = "";
					$custDet['proof_document'] = "";
					$custDet['vaccine_name'] = "";
					if(!empty($custDet['vaccine_id'])){
						$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
					}
	            }
            }

			$membersC = $membersDet->count();
			if ($membersC !=0) {
				$members[] = $custDet;

				// array_push($members,$custDet);
			}
		}
		$data['status'] = true;
		if($membersCount == 0){
			$data['message'] = "No members found";
		}else{
			$data['message'] = "";

		}
		$data['data'] = $members;

		$this->response($data);
	}

	public function members_list_fitoneway_post(){
		$members = array();

		$this->form_validation->set_rules('user_id', 'user ID', 'required');

		if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
			$request = $this->input->post();
			$members = User::select('*')->where(['parent_id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();
			$membersCount = $members->count();
			$membersDet = User::select('*')->where(['id'=>$request['user_id'], 'active'=>1])->orderBy('id','desc')->get();

			$query = $this->db->select('*')->from('users')->where('parent_id', $request['user_id'])->where('active',1)->get(); 
            $members = $query->result_array();

            if (!empty($members)) {
            	foreach ($members as $key => $value) {
            		$this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.departure_date,booking_member.departure_time,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date,booking_member.id as booking_member_id, booking_member.shippingpostal_code,users.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
		            $this->db->from('booking');
		            $this->db->join('booking_member','booking.id = booking_member.booking_id');
		            // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
		            $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
		            $this->db->join('users','booking_member.user_id = users.id');
		            $this->db->where('booking.user_id',$request['user_id']);
		            $this->db->where('booking_member.user_id',$value['id']);
		            $this->db->where('booking.booking_status',"in-complete");
		            $this->db->where('users.active',1);
		            $this->db->order_by('booking.id','DESC');
		            $query = $this->db->get();
		            $bookingDetails = $query->row_array();
		            // echo $this->db->last_query();

		            if (!empty($bookingDetails)) {
		            	$members[$key]['date'] = $bookingDetails['date'];
		            	$members[$key]['time'] = $bookingDetails['time'];
		            	$members[$key]['booking_id'] = $bookingDetails['booking_id'];
		            	$members[$key]['booking_member_id'] = $bookingDetails['booking_member_id'];
		            	$members[$key]['clinic_id'] = $bookingDetails['clinic_id'];
		            	$members[$key]['report_id'] = $bookingDetails['report_id'];
		            	$members[$key]['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
		            	$members[$key]['purpose_type'] = $bookingDetails['type'];
		            	$members[$key]['destination_country'] = $bookingDetails['destination_country'];
		            	$members[$key]['departure_date'] = date('d-m-Y',strtotime($bookingDetails['departure_date']));
		            	$members[$key]['departure_time'] = $bookingDetails['departure_time'];
		            	$members[$key]['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$members[$key]['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	$members[$key]['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$members[$key]['proof_document'] = $bookingDetails['proof_document'];
						$members[$key]['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}

		            	// $members[$key]['place_type'] = $bookingDetails['place_type'];
		            	
		            	$this->db->select('*');
	            		$this->db->from('hospital');
	            		$this->db->where('id',$bookingDetails['clinic_id']);
	            		$query = $this->db->get();
			            $hospital = $query->row_array();
			            if (!empty($hospital)) {
			            	$members[$key]['clinic_name'] = $hospital['company_name'];
			            }else{
			            	$members[$key]['clinic_name'] = "";
			            }

			            $this->db->select('*');
	            		$this->db->from('country');
	            		$this->db->where('id',$bookingDetails['destination_country']);
	            		$query = $this->db->get();
			            $country = $query->row_array();
			            if (!empty($country)) {
			            	$members[$key]['dest_country_name'] = $country['name'];
			            }else{
			            	$members[$key]['dest_country_name'] = "";
			            }

	            		$this->db->select('*');
	            		$this->db->from('reports');
	            		$this->db->where('id',$bookingDetails['report_id']);
	            		$query = $this->db->get();
			            $report = $query->row_array();
			            if (!empty($report)) {
			            	$members[$key]['test'] = $report['name'];
			            }else{
			            	$members[$key]['test'] = "";
			            }
		            }else{
		            	$members[$key]['date'] = "";
		            	$members[$key]['time'] = "";
		            	$members[$key]['booking_id'] = "";
		            	$members[$key]['booking_member_id'] = "";
		            	$members[$key]['clinic_id'] = "";
		            	$members[$key]['report_id'] = "";
		            	$members[$key]['shippingpostal_code'] = "";
		            	$members[$key]['purpose_type'] = "";
		            	$members[$key]['clinic_name'] = "";
		            	$members[$key]['test'] = "";
		            	$members[$key]['destination_country'] = "";
		            	$members[$key]['departure_date'] = "";
		            	$members[$key]['departure_time'] = "";
		            	$members[$key]['dest_country_name'] = "";
						///$members[$key]['date_of_birth'] = "";
		            	$members[$key]['vaccine_date'] = "";
		            	//$members[$key]['vaccine_id'] = "";
		            	$members[$key]['proof_document'] = "";
		            	$members[$key]['vaccine_name'] = "";
		            	if(!empty($value['vaccine_id'])){
							$members[$key]['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$value['vaccine_id']),"name");
						}
		            }

            	}
            }
	        $query = $this->db->select('*')->from('users')->where('id', $request['user_id'])->get(); 
            $custDet = $query->row_array();

            if (!empty($custDet)) {

            	$this->db->select('booking_member.report_id,booking_member.destination_country,booking_member.departure_date,booking_member.departure_time,booking_member.clinic_id,booking.id as booking_id,booking.type,booking_member.type as place_type,booking_slots.slots as time,booking_slots.date as date, booking_member.id as booking_member_id, booking_member.shippingpostal_code,users.date_of_birth,booking_member.vaccine_date,booking_member.vaccine_id,booking_member.proof_document');
	            $this->db->from('booking');
	            $this->db->join('booking_member','booking.id = booking_member.booking_id');
	            // $this->db->join('hospital','booking_member.clinic_id = hospital.id');
	            $this->db->join('booking_slots','booking_slots.booking_member_id = booking_member.id');
	            $this->db->join('users','booking_member.user_id = users.id');
	            $this->db->where('booking.user_id',$request['user_id']);
	            $this->db->where('booking_member.user_id',$custDet['id']);
	            $this->db->where('booking.booking_status',"in-complete");
	            $this->db->where('users.active',1);
	            $this->db->order_by('booking.id','DESC');
	            $query = $this->db->get();
	            $bookingDetails = $query->row_array();	
	            
	            if (!empty($bookingDetails)) {
	            	if (!empty($bookingDetails)) {
	            		$this->db->select('*');
	            		$this->db->from('hospital');
	            		$this->db->where('id',$bookingDetails['clinic_id']);
	            		$query = $this->db->get();
			            $hospital = $query->row_array();
			            if (!empty($hospital)) {
			            	$custDet['clinic_name'] = $hospital['company_name'];
			            }else{
			            	$custDet['clinic_name'] = "";
			            }
	            		$this->db->select('*');
	            		$this->db->from('reports');
	            		$this->db->where('id',$bookingDetails['report_id']);
	            		$query = $this->db->get();
			            $report = $query->row_array();
			            if (!empty($report)) {
			            	$custDet['test'] = $report['name'];
			            }else{
			            	$custDet['test'] = "";
			            }

			            $this->db->select('*');
	            		$this->db->from('country');
	            		$this->db->where('id',$bookingDetails['destination_country']);
	            		$query = $this->db->get();
			            $country = $query->row_array();
			            if (!empty($country)) {
			            	$custDet['dest_country_name'] = $country['name'];
			            }else{
			            	$custDet['dest_country_name'] = "";
			            }

	            		// $this->db->select('*');
		            	$custDet['date'] = $bookingDetails['date'];
		            	$custDet['time'] = $bookingDetails['time'];
		            	$custDet['booking_id'] = $bookingDetails['booking_id'];
		            	$custDet['booking_member_id'] = $bookingDetails['booking_member_id'];
		            	$custDet['clinic_id'] = $bookingDetails['clinic_id'];
		            	$custDet['report_id'] = $bookingDetails['report_id'];
		            	$custDet['shippingpostal_code'] = $bookingDetails['shippingpostal_code'];
		            	$custDet['purpose_type'] = $bookingDetails['type'];
		            	$custDet['destination_country'] = $bookingDetails['destination_country'];
		            	$custDet['departure_date'] = date('d-m-Y',strtotime($bookingDetails['departure_date']));
		            	$custDet['departure_time'] = $bookingDetails['departure_time'];
						///$custDet['date_of_birth'] = $bookingDetails['date_of_birth'];
		            	$custDet['vaccine_date'] = $bookingDetails['vaccine_date'];
		            	//$custDet['vaccine_id'] = $bookingDetails['vaccine_id'];
		            	$custDet['proof_document'] = $bookingDetails['proof_document'];
						$custDet['vaccine_name'] = "";
						if(!empty($bookingDetails['vaccine_id'])){
							$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$bookingDetails['vaccine_id']),"name");
						}
		            	// $custDet['place_type'] = $bookingDetails['place_type'];
		            	// code...
		            }else{
		            	$custDet['date'] = "";
			        	$custDet['time'] = "";
			        	$custDet['booking_id'] = "";
			        	$custDet['booking_member_id'] = "";
			        	$custDet['clinic_id'] = "";
			        	$custDet['report_id'] = "";
			        	$custDet['purpose_type'] = "";
			        	$custDet['destination_country'] = "";
		            	$custDet['shippingpostal_code'] = "";
		            	$custDet['departure_date'] = "";
		            	$custDet['departure_time'] = "";
		            	$custDet['dest_country_name'] = "";
						//$custDet['date_of_birth'] = "";
		            	$custDet['vaccine_date'] = "";
		            	//$custDet['vaccine_id'] = "";
		            	$custDet['proof_document'] = "";
						$custDet['vaccine_name'] = "";
			        	if(!empty($custDet['vaccine_id'])){
							$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
						}
		            }
	            }else{
	            	$custDet['date'] = "";
		        	$custDet['time'] = "";
		        	$custDet['booking_id'] = "";
	            	$custDet['dest_country_name'] = "";
		        	$custDet['booking_member_id'] = "";
		            $custDet['shippingpostal_code'] = "";
		        	$custDet['clinic_id'] = "";
		        	$custDet['report_id'] = "";
		        	$custDet['purpose_type'] = "";
		        	$custDet['destination_country'] = "";
	            	$custDet['departure_date'] = "";
	            	$custDet['departure_time'] = "";
					//$custDet['date_of_birth'] = "";
					$custDet['vaccine_date'] = "";
					//$custDet['vaccine_id'] = "";
					$custDet['proof_document'] = "";
					$custDet['vaccine_name'] = "";
					if(!empty($custDet['vaccine_id'])){
						$custDet['vaccine_name'] = get_column_value("certified_vaccines",array("id"=>$custDet['vaccine_id']),"name");
					}
	            }
            }

			$membersC = $membersDet->count();
			if ($membersC !=0) {
				$members[] = $custDet;
				// array_push($members,$custDet);
			}
		}
		$data['status'] = true;
		if($membersCount == 0){
			$data['message'] = "No members found";
		}else{
			$data['message'] = "";

		}
		$data['data'] = $members;

		$this->response($data);
	}

	public function test_post()
	{
		$type = explode('.', $_FILES["image"]["name"]);
        $type = strtolower($type[count($type) - 1]);
        $name = mt_rand(10000000, 99999999);
        $filename = $name . '.' . $type;
        $url = "assets/images/" . $filename;
        move_uploaded_file($_FILES["image"]["tmp_name"], $url);
        $folder = "assets/images/";
       	s3Upload($url, $folder);
        $image = S3_URL.$filename;
        $request['profile_picture'] = $image;
       
	}

	public function certified_vaccines_list_post(){
		$request = $this->input->post();
		$query = $this->db->select('*')->from('certified_vaccines')->where('status', "1")->get(); 
		$result = $query->result_array();
		$data['status'] = true;
		$data['message'] = "Vaccines List";
		$data['data'] = $result;
		$this->response($data);
	}
}
