<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Hospital extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('BookingsMembers');
		$this->load->model('Clinic');
        $this->load->library('SNS');
        $this->load->model('Slot');
        $this->load->model('Report');

	}

	public function sendsms_post() {
		$req = $this->post();
	 	$this->sns->sendSms($req['phone'],$req['message']);
		// $this->response($data);
	}

	public function filter_get() {
		$data = array();

		$data['min_price'] = MIN_PRICE;
		$data['max_price'] = MAX_PRICE;
		$data['min_dist']  = MIN_KM;
		$data['max_dist']  = MAX_KM;
		$data['reports'][0]['name'] = "LFT";
		$data['reports'][1]['name'] = "RT-PCR Test";
		// $data['reports']  =

		$data1['status'] = true;
		$data1['data'] = $data;
		$this->response($data1);
	}



	public function list_old_post(){
		$hospitalArr = array();
		$slot_timing_start = "";
		$slot_timing_end = "";
		$slot_timing_status = "";
		$this->form_validation->set_rules('test_type', 'Test type', 'required');
		$this->form_validation->set_rules('test_date', 'Test date', 'required');
		$this->form_validation->set_rules('test_time', 'Test Time', 'required');
		// $this->form_validation->set_rules('postcode', 'Post Code', 'required');

		if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
			$current_date = current_date();
			$request = $this->post();
			if (isset($request['search_key'])) {
				if ($request['search_key'] == "test_type") {
					$search_test = $request['search_value'];
				}elseif ($request['search_key'] == "price") {
					$max_price = $request['max_price'];
					$min_price = $request['min_price'];
				}elseif ($request['search_key'] == "distance") {
					$max_val = $request['lat'];
					$min_val = $request['lng'];
				}
			}

			$test_type =  isset($request['test_type']) ? $request['test_type'] : '1';
			$test_date =  isset($request['test_date']) ? $request['test_date'] : $current_date;
			$test_time =  isset($request['test_time']) ? $request['test_time'] : '';
			$code =  isset($request['postcode']) ? $request['postcode'] : '';

			$date = strtotime($request['test_date']);
			$day = strtolower(date('D', $date));
			if($test_time >= '08:00' && $test_time <= '12:00'){
				$slot_timing_start = 'morning_start';
				$slot_timing_end = 'morning_end';
				$slot_timing_status = 'morning_status';
			}else if($test_time > '12:00' && $test_time <= '16:00'){
				$slot_timing_start = 'afternoon_start';
				$slot_timing_end = 'afternoon_end';
				$slot_timing_status = 'afternoon_status';
			}else if($test_time > '16:00' && $test_time <= '20:00'){
				$slot_timing_start = 'evening_start';
				$slot_timing_end = 'evening_end';
				$slot_timing_status = 'evening_status';
			}			
			
			$types = array();
			if($code != ''){
				$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
				if (isset($request['lat']) && isset($request['lng'])) {
					if (($request['lat'] !="") && ($request['lng'] !="")) {
					$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture,111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$request['lat'].')) * COS(RADIANS(hospital.lng - '.$request['lng'].')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$request['lat'].'))))) AS distance')->from('hospital');
				}else{
					$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
				}
				}else{
					$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');

				}
				$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
				$this->db->join('users', 'users.id = hospital.user_id', 'left');
				if (isset($request['search_key']) && $request['search_key'] == "region") {
					$this->db->where('region', $request['search_value']);
				}else{
					$this->db->where('postal_code', $code);
				}
				// $this->db->where('postal_code', $code);
				$this->db->where('day' ,$day);
				if ($slot_timing_start !="") {
					// $this->db->where($slot_timing_start.'<=',trim($test_time));
					// $this->db->where($slot_timing_status,1);
				}
				$this->db->where($str);
				// $this->db->where('report_id', $test_type);
				$this->db->order_by('hospital.id', 'desc');
				$query = $this->db->get();
			}else{
				$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
				$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
				$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
				$this->db->join('users', 'users.id = hospital.user_id', 'left');
				if (isset($request['search_key']) && $request['search_key'] == "region") {
				// echo "string";
					$this->db->where('region', $request['search_value']);
				}else{
					$this->db->where('postal_code', $code);
				}
				// $this->db->where('postal_code', $code);
				$this->db->where('day' ,$day);
				if ($slot_timing_start !="") { 
					// $this->db->where($slot_timing_start.'<=',trim($test_time));
					// $this->db->where($slot_timing_status,1);
				}

				$this->db->where($str);
				// $this->db->where('report_id', $test_type);
				$this->db->order_by('hospital.id', 'desc');
				$query = $this->db->get();
				// $types = $query->result_array(); 
			}
					
			if ($query->num_rows()>0) {
				$types = $query->result_array(); 
				$hospitalArr = array();
				foreach ($types as $k => $v) {
					// SELECT * FROM cpass.booking_slots join booking_member on booking_member.id=booking_slots.booking_member_id where CONCAT(booking_slots.date,' ',booking_slots.slots) = "2021-08-21 13:00" and booking_member.clinic_id = 2 and booking_member.report_id = 1;

					$types[$k]['avail_timeslot'] = rand(10,20);
					$query = $this->db->select('*')->from('reports')->where('id', $test_type)->get();
					$arr = $query->row_array(); 
					if (!empty($arr)) {
						$types[$k]['price'] = $arr['price'];
					}else{
						$types[$k]['price'] = "0";
					}
					$this->db->select('*')->from('booking_slots');
					$this->db->join('booking_member', 'booking_member.id = booking_slots.booking_member_id'); 
					$this->db->join('reports', 'reports.id = booking_member.report_id');
					$this->db->where('booking_member.clinic_id', $v['id']);
					$this->db->where('booking_member.report_id' ,$test_type);
					if (isset($min_price) && isset($max_price)) {
						$this->db->where('reports.price >=',$min_price);
						$this->db->where('reports.price <=' ,$max_price);
					}
					$this->db->where('booking_slots.date' ,$test_date);
					$this->db->where('booking_slots.slots' ,$test_time);
					$query1 = $this->db->get();
					$arr1 = $query1->result_array(); 
					// echo $this->db->last_query();

					if (empty($arr1)) {
						array_push($hospitalArr,$types[$k]);
					}
				}

				if (!empty($hospitalArr)) {
					// code...
					$data['status'] = true;
					$data['data'] = $hospitalArr;
				}else{
					$data['status'] = true;
					$data['data'] = $hospitalArr;
					$data['message'] = "Clinic is not available."; 

				}
			}else {
				$types = array();
				$data['status'] = true; 
				$types = $query->result_array(); 
				$data['message'] = "Clinic is not available."; 
				$data['data'] = $types;
			}

		}
		$this->response($data);
	}

	public function international_list_post() {
    $hospitalArr = array();
    $slot_timing_start = "";
    $slot_timing_end = "";
    $slot_timing_status = "";
    $this->form_validation->set_rules('test_type', 'Test type', 'required');
    $this->form_validation->set_rules('date', 'Pickup or Shipping date', 'required');
    // $this->form_validation->set_rules('postcode', 'Post Code', 'required');

    if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
      $current_date = current_date();
      $request = $this->post();
      // _pre($request);
      if (isset($request['search_key'])) {
        if ($request['search_key'] == "test_type") {
          $search_test = $request['search_value'];
        }elseif ($request['search_key'] == "price") {
          $max_price = $request['max_price'];
          $min_price = $request['min_price'];
        }elseif ($request['search_key'] == "distance" || $request['search_key'] == "map") {
          $max_val = $request['lat'];
          $min_val = $request['lng'];
        }else{
        }
      }
		$search_key =  isset($request['search_key']) ? $request['search_key'] : '';

      $test_type =  isset($request['test_type']) ? $request['test_type'] : '';
      $date =  isset($request['date']) ? $request['date'] : $current_date;
      // $code =  isset($request['postcode']) ? $request['postcode'] : '';
      if ($search_key == "postcode" || $search_key == "") {
        $code =  isset($request['postcode']) ? $request['postcode'] : '';
        $google_api_key = $this->config->item("google_api_key");
	            // $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request['search_value'])."&sensor=false&key=".$google_api_key;

	            $url = "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:".rawurlencode(strtolower($request['search_value'].'|'))."country:UK&key=".$google_api_key;
	            // _pre($url);
	            $result_string = file_get_contents($url);
	            $result = json_decode($result_string, true);

	            // _pre($result);
	            if(!empty($result['results'])){
	                $request['lat'] = $result['results'][0]['geometry']['location']['lat'];
	                $request['lng'] = $result['results'][0]['geometry']['location']['lng'];
	            }else{
	                $response['status'] = FALSE; 
	                $response['message'] = 'Postcode is invalid.'; 
	                $this->response($response);
	                exit;
	            }
      }else{
        $code = "";
      }
      $types = array();
      if($code != ''){
  
        $str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
        if (isset($request['lat']) && isset($request['lng'])) 
        {
          if (($request['lat'] !="") && ($request['lng'] !="")) {
            $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture,111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$request['lat'].')) * COS(RADIANS(hospital.lng - '.$request['lng'].')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$request['lat'].'))))) AS distance')->from('hospital');
          }else{
            $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
          }
        } else {
          $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
        }
        // $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
        $this->db->join('slot', 'slot.user_id = hospital.id', 'left');
        $this->db->join('users', 'users.id = hospital.user_id', 'left');
        if (isset($request['search_key']) && $request['search_key'] == "region") {
          
          $this->db->where('region', $request['search_value']);
        }else{
          $this->db->where('postal_code', $code);
        }
        $this->db->where($str);
        $this->db->order_by('hospital.id', 'desc');
        $this->db->group_by('hospital.id');
        $query = $this->db->get();
      }else{
        $str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
        if (isset($request['lat']) && isset($request['lng'])) 
        {
          if (($request['lat'] !="") && ($request['lng'] !="")) {
            $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture,111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$request['lat'].')) * COS(RADIANS(hospital.lng - '.$request['lng'].')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$request['lat'].'))))) AS distance')->from('hospital');
          }else{
            $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
          }
        } else {
          $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
        }
        // $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
        $this->db->join('slot', 'slot.user_id = hospital.id', 'left');
        $this->db->join('users', 'users.id = hospital.user_id', 'left');
        if (isset($request['search_key']) && $request['search_key'] == "region") {
          $this->db->where('county', $request['search_value']);
        }
        if ($code != '')
		{
			$this->db->where('hospital.postal_code', $code);
		}
        // $this->db->where('postal_code', $code);
        $this->db->where($str);
        $this->db->order_by('hospital.id', 'desc');
        $this->db->group_by('hospital.id');
        $query = $this->db->get();
        // _pre($this->db->last_query());
      }
        // $types = $query->result_array(); 
        // echo $this->db->last_query();
        // _pre($types);

      if ($query->num_rows()>0) {
        $types = $query->result_array(); 
        $hospitalArr = array();

        // echo $this->db->last_query();
        foreach ($types as $k => $v) {
          // SELECT * FROM cpass.booking_slots join booking_member on booking_member.id=booking_slots.booking_member_id where CONCAT(booking_slots.date,' ',booking_slots.slots) = "2021-08-21 13:00" and booking_member.clinic_id = 2 and booking_member.report_id = 1;

          $types[$k]['avail_timeslot'] = rand(10,20);
          $query = $this->db->select('*')->from('reports')->where('id', $test_type)->get();
          $arr = $query->row_array(); 
          if (!empty($arr)) {
            $types[$k]['price'] = $arr['price'];
          }else{
            $types[$k]['price'] = "0";
          }

          $this->db->select('*')->from('booking_slots');
          $this->db->join('booking_member', 'booking_member.id = booking_slots.booking_member_id'); 
          $this->db->join('reports', 'reports.id = booking_member.report_id');
          $this->db->where('booking_member.clinic_id', $v['id']);
          $this->db->where('booking_member.report_id' ,$test_type);
          // $this->db->where('booking_slots.date' ,$date);
          $query1 = $this->db->get();
          $arr1 = $query1->result_array(); 
          if(isset($request['max_distance']) && isset($request['min_distance'])) {
            if ($request['max_distance'] !="" && $request['min_distance'] !="") {
              $distance = 0;
              if(isset($request['lat']) && isset($request['lng']) &&  $request['lat'] != '' && $request['lng'] !='') {
                $distance = $this->distance($v['lat'],$v['long'],$request['lat'],$request['lng'],'kilometers');
                if(floatval($request['min_distance']) <= $distance && floatval($request['max_distance']) >= $distance && $request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price']) {
                  array_push($hospitalArr,$types[$k]);
                }
              }else{ 
                if (isset($request['min_price']) && isset($request['max_price'])) { 
                  if (($request['min_price'] !="") && ($request['max_price'] !="")) { 
                    if($request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price']) {
                      array_push($hospitalArr,$types[$k]);
                    }
                  }
                }
              }
            }else{ 
              if (isset($request['lat']) && isset($request['lng']))  { 
                if (($request['lat'] !="") && ($request['lng'] !="")) {
                  if ($v['distance'] <= $this->config->item('distance_radius')) {
                    array_push($hospitalArr,$types[$k]);
                  } 
                }else{
                  array_push($hospitalArr,$types[$k]);
                }
              }else{
                array_push($hospitalArr,$types[$k]); 
              }
            }
          } else if($search_key == '' && $code == '') {
            $distance = 0;
            if($v['lat'] != '' && $v['long']) {
              $distance = $this->distance($v['lat'],$v['long'],$request['lat'],$request['lng'],'kilometers');
              if($distance <= $this->config->item('distance_radius')) {
                array_push($hospitalArr,$types[$k]);  
              }
            }
          } else {
            if (empty($arr1)) {
              array_push($hospitalArr,$types[$k]);
            }
          }
        }

        //Added by Nayan
        if($request['search_key'] == 'postcode' || $request['search_key'] == 'map')
        {
	            	$result_data_3 = [];
	            	$result_data_5 = [];
	            	$result_data_10 = [];
		            $result_data_3 = array_filter($hospitalArr, function($var)
							  {
							     if($var['distance'] < 3){
								  		return $var;
								    }
							  });

		            if(count($result_data_3) == 0)
		            {
								  $result_data_5 = array_filter($hospitalArr, function($var)
								  {
								  	if($var['distance'] < 5){
								  		return $var;
								  	}
								  });
							  }


							  if(isset($result_data_5) && count($result_data_5) ==0 && count($result_data_3) == 0)
		            {
								  $result_data_10 = array_filter($hospitalArr, function($var)
								  {
								    if($var['distance'] < 10){
								  		return $var;
								  	}
								  });
							  }
							  $hospitalArr = [];
							  $hospitalArr = array_merge($result_data_3,$result_data_5,$result_data_10);
				}

							if(count($hospitalArr) == 0)
							{
								$response['status'] = TRUE; 
		            $response['message'] = "Clinic is not available.";
		            $response['data'] = [];
		            $this->response($response);
		            exit;
							}


							foreach ($hospitalArr as $key => $value) {
	            	if ($hospitalArr[$key]['profile_picture'] == "") {
	            		
		            	$hospitalArr[$key]['profile_picture'] = 'https://c-pass.s3.eu-west-2.amazonaws.com/production/assets/images/users/272133542.jpg';
	            	}else{
		            	$hospitalArr[$key]['profile_picture'] = $hospitalArr[$key]['profile_picture'];
	            	}
	            }
        	//End Added by Nayan
        $data['status'] = true;
        $data['data'] = $hospitalArr;
      }else {
        $types = array();
        $data['status'] = true; 
        $types = $query->result_array(); 
        $data['message'] = "Clinic is not available."; 
        $data['data'] = $types;
      }      
    }
    $this->response($data);
  }

	// public function international_list_post() {
	// 	$hospitalArr = array();
	// 	$slot_timing_start = "";
	// 	$slot_timing_end = "";
	// 	$slot_timing_status = "";
	// 	$this->form_validation->set_rules('test_type', 'Test type', 'required');
	// 	$this->form_validation->set_rules('date', 'Test date', 'required');
	// 	// $this->form_validation->set_rules('postcode', 'Post Code', 'required');

	// 	if ($this->form_validation->run() == FALSE) {
 //            $data['status'] = FALSE;
 //            $data['message'] = validation_errors_response();
 //        } else {
	// 		$current_date = current_date();
	// 		$request = $this->post();
	// 		if (isset($request['search_key'])) {
	// 			if ($request['search_key'] == "test_type") {
	// 				$search_test = $request['search_value'];
	// 			}elseif ($request['search_key'] == "price") {
	// 				$max_price = $request['max_price'];
	// 				$min_price = $request['min_price'];
	// 			}elseif ($request['search_key'] == "distance" || $request['search_key'] == "map") {
	// 				$max_val = $request['max_val'];
	// 				$min_val = $request['min_val'];
	// 			}else{
	// 			}
	// 		}

	// 		$test_type =  isset($request['test_type']) ? $request['test_type'] : '';
	// 		$date =  isset($request['date']) ? $request['date'] : $current_date;
	// 		// $code =  isset($request['postcode']) ? $request['postcode'] : '';
	// 		if ($search_key == "postcode" || $search_key == "") {
	// 			$code =  isset($request['postcode']) ? $request['postcode'] : '';
	// 		}else{
	// 			$code = "";
	// 		}

			
	// 		$types = array();
	// 		if($code != ''){
	
	// 			$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
	// 			$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
	// 			$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
	// 			$this->db->join('users', 'users.id = hospital.user_id', 'left');
	// 			if (isset($request['search_key']) && $request['search_key'] == "region") {
					
	// 				$this->db->where('region', $request['search_value']);
	// 			}else{
	// 				$this->db->where('postal_code', $code);
	// 			}
	// 			$this->db->where($str);
	// 			$this->db->order_by('hospital.id', 'desc');
	// 			$this->db->group_by('hospital.id');
	// 			$query = $this->db->get();
	// 		}else{
	// 			$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
	// 			$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
	// 			$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
	// 			$this->db->join('users', 'users.id = hospital.user_id', 'left');
	// 			if (isset($request['search_key']) && $request['search_key'] == "region") {
					
	// 				$this->db->where('region', $request['search_value']);
	// 			}else{
	// 				$this->db->where('postal_code', $code);
	// 			}
	// 			// $this->db->where('postal_code', $code);
	// 			$this->db->where($str);
	// 			$this->db->order_by('hospital.id', 'desc');
	// 			$this->db->group_by('hospital.id');
	// 			$query = $this->db->get();
	// 		}

	// 		if ($query->num_rows()>0) {
	// 			$types = $query->result_array(); 
	// 			$hospitalArr = array();
	// 			// echo $this->db->last_query();
	// 			foreach ($types as $k => $v) {
	// 				// SELECT * FROM cpass.booking_slots join booking_member on booking_member.id=booking_slots.booking_member_id where CONCAT(booking_slots.date,' ',booking_slots.slots) = "2021-08-21 13:00" and booking_member.clinic_id = 2 and booking_member.report_id = 1;

	// 				$types[$k]['avail_timeslot'] = rand(10,20);
	// 				$query = $this->db->select('*')->from('reports')->where('id', $test_type)->get();
	// 				$arr = $query->row_array(); 
	// 				if (!empty($arr)) {
	// 					$types[$k]['price'] = $arr['price'];
	// 				}else{
	// 					$types[$k]['price'] = "0";
	// 				}

	// 				$this->db->select('*')->from('booking_slots');
	// 				$this->db->join('booking_member', 'booking_member.id = booking_slots.booking_member_id'); 
	// 				$this->db->join('reports', 'reports.id = booking_member.report_id');
	// 				$this->db->where('booking_member.clinic_id', $v['id']);
	// 				$this->db->where('booking_member.report_id' ,$test_type);
	// 				$this->db->where('booking_slots.date' ,$date);
	// 				$query1 = $this->db->get();
	// 				$arr1 = $query1->result_array(); 
	// 				if(isset($request['max_distance']) && isset($request['min_distance']))
	// 				{
						
	// 					$distance = 0;
	// 					if($v['lat'] != '' && $v['long'] && isset($request['lat']) && $request['lng'])
	// 					{
	// 						$distance = $this->distance($v['lat'],$v['long'],$request['lat'],$request['lng'],'kilometers');
	// 						//echo $types[$k]['price'].'<br/>';
	// 						if(floatval($request['min_distance']) <= $distance && floatval($request['max_distance']) >= $distance  && $request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price'])
	// 						{
	// 							array_push($hospitalArr,$types[$k]);
	// 						}
	// 					}else{
							
	// 						if($request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price'])
	// 						{
	// 							array_push($hospitalArr,$types[$k]);
	// 						}
	// 					}
	// 				}
	// 				else{
						
	// 					if (empty($arr1)) {
	// 						array_push($hospitalArr,$types[$k]);
	// 					}
	// 				}
	// 			}

	// 			$data['status'] = true;
	// 			$data['data'] = $hospitalArr;
	// 		}else {
	// 			$types = array();
	// 			$data['status'] = true; 
	// 			$types = $query->result_array(); 
	// 			$data['message'] = "Hospital is not available."; 
	// 			$data['data'] = $types;
	// 		}
			
	// 	}
	// 	$this->response($data);
	// }

	public function regionlist_get() {
        $region = $this->db->get_where('county', array('status' =>"1"))->result_array();
    	$response['status'] = true;
    	$response['data'] = $region;       
        $this->response($response);
	}

	public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

	public function list_dharitri_post(){
		$hospitalArr = array();
		$slot_timing_start = "";
		$slot_timing_end = "";
		$slot_timing_status = "";
		$this->form_validation->set_rules('test_type', 'Test type', 'required');
		$this->form_validation->set_rules('test_date', 'Test date', 'required');
		$this->form_validation->set_rules('test_time', 'Test Time', 'required');
		// $this->form_validation->set_rules('postcode', 'Post Code', 'required');

		if ($this->form_validation->run() == FALSE) {
            $data['status'] = FALSE;
            $data['message'] = validation_errors_response();
        } else {
			$current_date = current_date();
			$request = $this->post();
			if (isset($request['search_key'])) {
				if ($request['search_key'] == "test_type") {
					$search_test = $request['search_value'];
				}elseif ($request['search_key'] == "price") {
					$max_price = $request['max_price'];
					$min_price = $request['min_price'];
				}elseif ($request['search_key'] == "distance" || $request['search_key'] == "map") {
					$max_val = $request['lat'];
					$min_val = $request['lng'];
				}
			}

			$test_type =  isset($request['test_type']) ? $request['test_type'] : '1';
			$test_date =  isset($request['test_date']) ? $request['test_date'] : $current_date;
			$test_time =  isset($request['test_time']) ? $request['test_time'] : '';

			$search_key =  isset($request['search_key']) ? $request['search_key'] : '';
			if ($search_key == "postcode" || $search_key == "") {
				$code =  isset($request['postcode']) ? $request['postcode'] : '';
			}else{
				$code = "";
			}
			// _pre($code);
			// $code =  isset($request['postcode']) ? $request['postcode'] : '';

			$date = strtotime($request['test_date']);
			$day = strtolower(date('D', $date));
			if($test_time >= '08:00' && $test_time <= '12:00'){
				$slot_timing_start = 'morning_start';
				$slot_timing_end = 'morning_end';
				$slot_timing_status = 'morning_status';
			}else if($test_time > '12:00' && $test_time <= '16:00'){
				$slot_timing_start = 'afternoon_start';
				$slot_timing_end = 'afternoon_end';
				$slot_timing_status = 'afternoon_status';
			}else if($test_time > '16:00' && $test_time <= '20:00'){
				$slot_timing_start = 'evening_start';
				$slot_timing_end = 'evening_end';
				$slot_timing_status = 'evening_status';
			}			
			
			$types = array();
			if($code != ''){
				$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
				if (isset($request['lat']) && isset($request['lng'])) 
				{
					if (($request['lat'] !="") && ($request['lng'] !="")) {
						$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture,111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$request['lat'].')) * COS(RADIANS(hospital.lng - '.$request['lng'].')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$request['lat'].'))))) AS distance')->from('hospital');
					}else{
						$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
					}
				} else {
					$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
				}
				$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
				$this->db->join('users', 'users.id = hospital.user_id', 'left');
				if (isset($request['search_key']) && $request['search_key'] == "region") 
				{
					$this->db->where('region', $request['search_value']);
				}
				else
				{
					$this->db->where('postal_code', $code);
				}
				// $this->db->where('postal_code', $code);
				$this->db->where('day' ,$day);
				if ($slot_timing_start !="")  {
					// $this->db->where($slot_timing_start.'<=',trim($test_time));
					// $this->db->where($slot_timing_status,1);
				}
				$this->db->where($str);
				// $this->db->where('report_id', $test_type);
				$this->db->order_by('hospital.id', 'desc');
				$query = $this->db->get();
			} else {
				// echo "string";
				$str = "FIND_IN_SET('".$test_type."',report_id) AND 1=1";
				if (isset($request['lat']) && isset($request['lng'])) 
				{
					if (($request['lat'] !="") && ($request['lng'] !="")) {
						$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture,111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$request['lat'].')) * COS(RADIANS(hospital.lng - '.$request['lng'].')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$request['lat'].'))))) AS distance')->from('hospital');
					}else{
						$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
					}
				} else {
					$this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
				}
				// $this->db->select('hospital.id,hospital.company_name,hospital.address,hospital.lat,hospital.lng as long,users.profile_picture')->from('hospital');
				$this->db->join('slot', 'slot.user_id = hospital.id', 'left');
				$this->db->join('users', 'users.id = hospital.user_id', 'left');

				if (isset($request['search_key']) && trim($request['search_key']) == "region") 
				{
					$this->db->where('hospital.region', $request['search_value']);
				}

				if ($code != '')
				{
					$this->db->where('hospital.postal_code', $code);
				}
				// if (($request['lat'] !="") && ($request['lng'] !="")) {
				// 	// $this->db->where('distance <','150');
				// }

				// $this->db->where('postal_code', $code);
				$this->db->where('day' ,$day);
				if ($slot_timing_start !="") 
				{ 
					// $this->db->where($slot_timing_start.'<=',trim($test_time));
					// $this->db->where($slot_timing_status,1);
				}

				$this->db->where($str);
				// $this->db->where('report_id', $test_type);
				$this->db->order_by('hospital.id', 'desc');
				$query = $this->db->get();
				// $types = $query->result_array(); 
			}
					
			if ($query->num_rows()>0) {
				$types = $query->result_array(); 
				// echo $this->db->last_query();
				$hospitalArr = array();
				// _pre($types);
				foreach ($types as $k => $v) {

					$types[$k]['avail_timeslot'] = rand(10,20);
					$query = $this->db->select('*')->from('reports')->where('id', $test_type)->get();
					$arr = $query->row_array(); 
					if (!empty($arr)) {
						$types[$k]['price'] = $arr['price'];
					}else{
						$types[$k]['price'] = "0";
					}
					$this->db->select('*')->from('booking_slots');
					$this->db->join('booking_member', 'booking_member.id = booking_slots.booking_member_id'); 
					$this->db->join('reports', 'reports.id = booking_member.report_id');
					$this->db->where('booking_member.clinic_id', $v['id']);
					$this->db->where('booking_member.report_id' ,$test_type);
					$this->db->where('booking_slots.date' ,$test_date);
					$this->db->where('booking_slots.slots' ,$test_time);
					$query1 = $this->db->get();
					$arr1 = $query1->result_array(); 
					// echo $this->db->last_query();
					// SELECT * FROM cpass.booking_slots join booking_member on booking_member.id=booking_slots.booking_member_id where CONCAT(booking_slots.date,' ',booking_slots.slots) = "2021-08-21 13:00" and booking_member.clinic_id = 2 and booking_member.report_id = 1;

					if(isset($request['max_distance']) && isset($request['min_distance'])) {
						if ($request['max_distance'] !="" && $request['min_distance'] !="") {
							$distance = 0;
							if(isset($request['lat']) && isset($request['lng']) &&  $request['lat'] != '' && $request['lng'] !='') {
								$distance = $this->distance($v['lat'],$v['long'],$request['lat'],$request['lng'],'kilometers');
								if(floatval($request['min_distance']) <= $distance && floatval($request['max_distance']) >= $distance && $request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price']) {
									array_push($hospitalArr,$types[$k]);
								}
							}else{ 
								if (isset($request['min_price']) && isset($request['max_price'])) { 
									if (($request['min_price'] !="") && ($request['max_price'] !="")) { 
										if($request['min_price'] <= $types[$k]['price'] && $request['max_price'] >= $types[$k]['price']) {
											array_push($hospitalArr,$types[$k]);
										}
									}
								}
							}
						}else{ 
							if (isset($request['lat']) && isset($request['lng']))  { 
								if (($request['lat'] !="") && ($request['lng'] !="")) {
									if ($v['distance'] <= $this->config->item('distance_radius')) {
									 	
										array_push($hospitalArr,$types[$k]);
									} 
								}else{
									array_push($hospitalArr,$types[$k]);
								}
							}else{
								array_push($hospitalArr,$types[$k]); 
							}
						}
					} else if($search_key == '' && $code == '') {
						$distance = 0;
						if($v['lat'] != '' && $v['long']) {
							$distance = $this->distance($v['lat'],$v['long'],$request['lat'],$request['lng'],'kilometers');
							if($distance <= $this->config->item('distance_radius')) {
								array_push($hospitalArr,$types[$k]);	
							}
						}
					} else {
						if (empty($arr1)) {
							array_push($hospitalArr,$types[$k]);
						}
					}
				}
				if (!empty($hospitalArr)) {
					$data['status'] = true;
					$data['data'] = $hospitalArr;
				}else{
					$data['status'] = true;
					$data['data'] = $hospitalArr;
					$data['message'] = "Clinic is not available."; 

				}
			}else {
				$types = array();
				$data['status'] = true; 
				$types = $query->result_array(); 
				$data['message'] = "Clinic is not available."; 
				$data['data'] = $types;
			}

		}
		$this->response($data);
	}

	public function validateDateFormat($str) {
      if($str != ''){
          if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$str)) {
              $this->form_validation->set_message('validateDateFormat', 'The {field} field contain invalid date format.');
              return false;
          }
      }
      return TRUE;
  }

  public function validateTimeFormat($str) {
      if($str != ''){
          if (!preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/",$str)) {
              $this->form_validation->set_message('validateTimeFormat', 'The {field} field contain invalid time format.');
              return false;
          }
      }
      return TRUE;
  }



  function getTimeSlot($interval, $start_time, $end_time)
	{
		$start = new DateTime($start_time);
		$end = new DateTime($end_time);
		$startTime = $start->format('H:i');
		$endTime = $end->format('H:i');
		$i=0;
		$time = [];
		while(strtotime($startTime) <= strtotime($endTime)){
			$start = $startTime;
			$end = date('H:i',strtotime('+'.$interval.' minutes',strtotime($startTime)));
			$startTime = date('H:i',strtotime('+'.$interval.' minutes',strtotime($startTime)));
			$i++;
			if(strtotime($startTime) <= strtotime($endTime)){
				$time[$i]['slot_time'] = $start;
				$time[$i]['is_booked'] = 0;
				// $time[$i]['slot_start_time'] = $start;
				// $time[$i]['slot_end_time'] = $end;
			}
		}
		return $time;
	}

	public function list_post(){
		
	    $this->form_validation->set_rules('purpose_id', 'purpose id', 'trim|required|is_natural_no_zero');
	    $this->form_validation->set_rules('test_type', 'test type', 'trim|required|is_natural_no_zero');
	    $this->form_validation->set_rules('test_date', 'test date', 'trim|required|callback_validateDateFormat');
	    $this->form_validation->set_rules('test_time', 'test time', 'trim|required|callback_validateTimeFormat');
	    $this->form_validation->set_rules('search_key', 'search key', 'trim|required');
	    $this->form_validation->set_rules('lat', 'latitude', 'trim');
	    $this->form_validation->set_rules('lng', 'longitude', 'trim');
	    $this->form_validation->set_rules('max_distance', 'maximum distance', 'trim');

	    if ($this->form_validation->run() == FALSE) {
	        $data['status'] = FALSE;
	        $data['message'] = validation_errors_response();
	        $this->response($data);
	        exit;
	    } else {

	        $request = $_POST;
	        if($request['purpose_id'] == '3' || $request['purpose_id'] == '4'){
	        	$request['purpose_id'] = 2;
	        }
	        $request = array_map('trim', $request);

	        if($request['search_key'] == 'postcode' || $request['search_key'] == 'region'){
	            if(!isset($request['search_value']) || $request['search_value'] == ''){
	                $response['status'] = FALSE; 
	                $response['message'] = 'Search value parameter is required.'; 
	                $this->response($response);
	                exit;
	            }
	        }

	        if($request['search_key'] == 'postcode'){
							// $radius = $this->config->item('distance_radius');
	      			// 	$this->db->having('distance <', $radius);

	        }else if($request['search_key'] == 'region'){

	        	$lat_lng = $this->db->get_where('county', array('id' => $request['search_value']))->row_array();
						$request['lat'] = $lat_lng['lat'];
						$request['lng'] = $lat_lng['lng'];

	        }else if($request['search_key'] == 'map' || $request['search_key'] == 'distance'){	 
	        	if (isset($request['max_distance']) && $request['max_distance'] != '') {
		       	}else{
	        		// $radius = $this->config->item('distance_radius');
	          //   $this->db->having('distance <', $radius);  						
  	       	}       	
	        }else{
            $response['status'] = FALSE; 
            $response['message'] = 'Search key parameter is invalid.'; 
            $this->response($response);
            exit;
	        }

	        if(isset($request['max_distance']) && $request['max_distance'] != '' && ($request['search_key'] !== 'postcode')){
	            if(isset($request['lat']) && $request['lat'] != '' && isset($request['lng']) && $request['lng'] != ''){
	                $latitude = $request['lat'];
	                $longitude = $request['lng'];
	                // _pre();
	            }else{
	                $response['status'] = FALSE; 
	                $response['message'] = 'Latitude & longitude are required for find max distance.'; 
	                $this->response($response);
	                exit;
	            }
	        }

	        $test_date = DateTime::createFromFormat('Y-m-d', $request['test_date']);
	        $day = strtolower($test_date->format('D'));

	        if($request['search_key'] == 'postcode'){
	            // if search by postcode
	            $google_api_key = $this->config->item("google_api_key");
	            // $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request['search_value'])."&sensor=false&key=".$google_api_key;

	            $url = "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:".rawurlencode(strtolower($request['search_value'].'|'))."country:UK&key=".$google_api_key;
	            // _pre($url);
	            $result_string = file_get_contents($url);
	            $result = json_decode($result_string, true);

	            // _pre($result);
	            if(!empty($result['results'])){
	                $latitude = $result['results'][0]['geometry']['location']['lat'];
	                $longitude = $result['results'][0]['geometry']['location']['lng'];
	            }else{
	                $response['status'] = FALSE; 
	                $response['message'] = 'Postcode is invalid.'; 
	                $this->response($response);
	                exit;
	            }

	        	// $this->db->where('hospital.postal_code', $request['search_value']);

	        }else if($request['search_key'] == 'map' || $request['search_key'] == 'distance'){
	            // if search by map
	            if(isset($request['lat']) && $request['lat'] != '' && isset($request['lng']) && $request['lng'] != ''){
	                $latitude = $request['lat'];
	                $longitude = $request['lng'];
	            }else{
	                $response['status'] = FALSE; 
	                $response['message'] = 'Latitude & longitude are required for search in map.'; 
	                $this->response($response);
	                exit;
	            }
	        }elseif ($request['search_key'] == "region") {
	        	// $google_api_key = $this->config->item("google_api_key");
	         //    // $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request['search_value'])."&sensor=false&key=".$google_api_key;

          //   $url = "https://maps.googleapis.com/maps/api/geocode/json?components=region:".rawurlencode(strtolower($request['search_value'].'|'))."country:UK&key=".$google_api_key;
          //   // _pre($url);
          //   $result_string = file_get_contents($url);
          //   $result = json_decode($result_string, true);

          //   // _pre($result);
          //   if(!empty($result['results'])){
          //       $latitude = $result['results'][0]['geometry']['location']['lat'];
          //       $longitude = $result['results'][0]['geometry']['location']['lng'];
          //   }else{
          //       $response['status'] = FALSE; 
          //       $response['message'] = 'Postcode is invalid.'; 
          //       $this->response($response);
          //       exit;
          //   }
	        }
	    
	        $sql_select = array(
	                    "hospital.id",
	                    "hospital.company_name",
	                    "hospital.address",
	                    "hospital.lat",
	                    "hospital.lng as long",
	                    "users.profile_picture",
	                    "network_report_price.price",
	                    // '111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$latitude.')) * COS(RADIANS(hospital.lng - '.$longitude.')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$latitude.'))))) AS distance',
	                );
	        if(isset($latitude) && isset($longitude) && $latitude != '' && $longitude != ''){
	            $distance = '111.111 * DEGREES(ACOS(LEAST(1.0, COS(RADIANS(hospital.lat)) * COS(RADIANS('.$latitude.')) * COS(RADIANS(hospital.lng - '.$longitude.')) + SIN(RADIANS(hospital.lat)) * SIN(RADIANS('.$latitude.'))))) AS distance';          
	            array_push($sql_select, $distance);
	            // $min = 'MIN(distance)';
	            // array_push($sql_select,$min);
	            // $max = 'MAX(distance)';
	            // array_push($sql_select,$max);
	        }

	        if(isset($request['max_distance']) && $request['max_distance'] != ''){
	            $this->db->having("distance <=", $request['max_distance']);
	            $this->db->order_by("distance", "asc");
	        } 


	        $this->db->select($sql_select);
	        $this->db->from('hospital');
	        $this->db->join('slot', 'slot.user_id = hospital.id', 'left');
	        $this->db->join('users', 'users.id = hospital.user_id', 'left');
	        $this->db->join('network_report_price', 'network_report_price.hospital_id = hospital.id', 'left');
	        $this->db->where('network_report_price.report_type_id', $request['test_type']);
	        $this->db->where('network_report_price.purpose_type', $request['purpose_id']);
	        $this->db->where('slot.day', $day);
	        $this->db->where('users.deleted_at', null);
          // $this->db->order_by("distance", "asc");
	            
					// $this->db->order_by('DESC');
	        $this->db->where('hospital.status', '1');

	        if (isset($request['min_price']) && $request['min_price'] != ''){
	            //$request['min_price'] = number_format((float)$request['min_price'], 2, '.', '');
	            $this->db->where('network_report_price.price >=', (int) $request['min_price']);
	        }
	        if (isset($request['max_price']) && $request['max_price'] != ''){
	            //$request['max_price'] = number_format((float)$request['max_price'], 2, '.', '');
	            $this->db->where('network_report_price.price <=', (int) $request['max_price']);
	        }

	        if($request['search_key'] == 'region'){
	            $this->db->where('hospital.county', $request['search_value']);
	        }elseif ($request['search_key'] =="postcode") {
	        	$this->db->order_by("distance", "asc");
	        }

	        $this->db->group_by('hospital.id');
	            // $this->db->order_by("distance", "asc");

	        $sql_query = $this->db->get();
	        // _pre($this->db->last_query());
	        if ($sql_query->num_rows() > 0){
	            $result_data = $sql_query->result_array();

	            if($request['search_key'] == 'postcode' || $request['search_key'] == 'map'){
	            	$result_data_3 = [];
	            	$result_data_5 = [];
	            	$result_data_10 = [];
		            $result_data_3 = array_filter($result_data, function($var)
							  {
							     if($var['distance'] < 3){
								  		return $var;
								    }
							  });

		            if(count($result_data_3) == 0)
		            {
								  $result_data_5 = array_filter($result_data, function($var)
								  {
								  	if($var['distance'] < 5){
								  		return $var;
								  	}
								  });
							  }


							  if(isset($result_data_5) && count($result_data_5) ==0 && count($result_data_3) == 0)
		            {
								  $result_data_10 = array_filter($result_data, function($var)
								  {
								    if($var['distance'] < 10){
								  		return $var;
								  	}
								  });
							  }
							  $result_data = [];
							  $result_data = array_merge($result_data_3,$result_data_5,$result_data_10);
							}

							if(count($result_data) == 0)
							{
								$response['status'] = TRUE; 
		            $response['message'] = "Clinic is not available.";
		            $response['data'] = [];
		            $this->response($response);
		            exit;
							}

	      			//$distance_array = array_column($result_data, 'distance');
	      			//$min_value = min($distance_array);
							// $max_value = max($distance_array);
							// $result_data['min_distance']  = $min_value;
							// $result_data['max_distance']  = $max_value;
	            foreach ($result_data as $key => $value) {
	            	// if($request['purpose_id'] != '5'){
	            	// 	$result_data[$key]['avail_timeslot'] = 5;
	            	// }else{
	            	// 	$result_data[$key]['avail_timeslot'] = 0;
	            	// }
	            	if ($result_data[$key]['profile_picture'] == "") {
	            		
		            	$result_data[$key]['profile_picture'] = 'https://c-pass.s3.eu-west-2.amazonaws.com/production/assets/images/users/272133542.jpg';
	            	}else{
		            	$result_data[$key]['profile_picture'] = $result_data[$key]['profile_picture'];
	            	}
	            }
	            $hospital_ids = array_column($result_data, 'id');
	            // _pre($hospital_ids);


	            // echo "string";

	            if($request['purpose_id'] != '5'){
	            	// _pre($request['purpose_id']);

	            	$this->db->select('booking_member.clinic_id');
		            $this->db->from('booking_slots');
		            $this->db->join('booking_member', 'booking_member.id = booking_slots.booking_member_id'); 
		            $this->db->join('reports', 'reports.id = booking_member.report_id');
		            $this->db->where_in('booking_member.clinic_id', $hospital_ids);
		            $this->db->where('booking_member.report_id', $request['test_type']);
		            $this->db->where('booking_slots.date', $request['test_date']);
		            $this->db->where('booking_slots.slots', $request['test_time']);
		            $this->db->group_by('booking_member.clinic_id');
		            $sql_query = $this->db->get();
		            if ($sql_query->num_rows() > 0){
		                $booked_data = $sql_query->result_array();

		                $booked_clinics = array_column($booked_data, 'clinic_id');
		                foreach ($result_data as $key => $value) {
		                    if(in_array($value['id'], $booked_clinics)){
		                        unset($result_data[$key]);
		                    }
		                }
		                $result_data = array_values($result_data);
		            }

	            }


	            foreach($result_data as $result_key => $clinic_data)
	            {
	            	 $request['report_type'] = $request['test_type'];
	            	 $request['slot_date'] = $request['test_date'];
	            	 $request['hospital_id'] = $clinic_data['id'];

	            	 $date = strtotime($request['slot_date']);
								$day = strtolower(date('D', $date));
								// $status = $request['slot_time'].'_status';
								$morning_available_time = Slot::where('user_id',$request['hospital_id'])->where('day',$day)->where('morning_status', 1)->first();
								$afternoon_available_time = Slot::where('user_id',$request['hospital_id'])->where('day',$day)->where('afternoon_status', 1)->first();
								$report_detail = Report::find($request['report_type']);
								$bookings = BookingsMembers::where(['clinic_id'=>$request['hospital_id'], 'report_id'=>$request['report_type']])->get()->toArray();
								// _pre($bookings);
								
								$morning_start = isset($morning_available_time) ? $morning_available_time->morning_start : '08:00';
								$morning_end = isset($morning_available_time) ? $morning_available_time->morning_end : '12:00';
								$afternoon_start = isset($afternoon_available_time) ? $afternoon_available_time->afternoon_start : '12:05';
								$afternoon_end = isset($afternoon_available_time) ? $afternoon_available_time->afternoon_end : '16:00';
								$slots = [];

								$slots['morning'] = $this->getTimeSlot($report_detail->minutes,$morning_start, $morning_end);
								foreach($bookings as $key => $booking){
									$booking_slot = $this->db->select('*')->from('booking_slots')->where('booking_member_id',$booking['id'])->where('date',$request['slot_date'])->get()->row_array();
									// _pre($booking_slot);
									$bookings[$key]['slot'] = $booking_slot['slots'];

								}

								foreach($bookings as $booking){
									foreach($slots['morning'] as $key => $slot){
										// print_r($booking['slot']);
										if(isset($booking['slot']) && $booking['slot'] == $slot['slot_time']){	
								
											// $slots['morning'][$key]['is_booked'] = 1;
											unset($slots['morning'][$key]);
										}
									}
								}

								$slots['morning'] = array_values($slots['morning']);

								$slots['afternoon'] = $this->getTimeSlot($report_detail->minutes,$afternoon_start, $afternoon_end);
								foreach($bookings as $key => $booking){
									$booking_slot = $this->db->select('*')->from('booking_slots')->where('booking_member_id',$booking['id'])->where('date',$request['slot_date'])->get()->row_array();
									// _pre($booking_slot);
									$bookings[$key]['slot'] = $booking_slot['slots'];
									
								}

								foreach($bookings as $booking){
									foreach($slots['afternoon'] as $key => $slot){
										if(isset($booking['slot']) && $booking['slot'] == $slot['slot_time']){						
											// $slots['afternoon'][$key]['is_booked'] = 1;
											unset($slots['afternoon'][$key]);
										}
									}
								}

								$slots['afternoon'] = array_values($slots['afternoon']);

								$finalArr = array_merge($slots['morning'],$slots['afternoon']);
								// _pre($finalArr);

									$result_data[$result_key]['avail_timeslot'] = count($finalArr);
								
	            }



	            if(!empty($result_data)){
	                $response['status'] = TRUE; 
	                $response['data'] = $result_data;
	                $this->response($response);
	                exit;
	            }else{
	            	// echo "string";
	            }

	        }else{
	            $response['status'] = TRUE; 
	            $response['message'] = "Clinic is not available.";
	            $response['data'] = [];
	            $this->response($response);
	            exit;
	        }

	    }
	}


    function distance($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'kilometers') {
      $theta = $longitude1 - $longitude2; 
      $distance = (sin(deg2rad((float)$latitude1)) * sin(deg2rad((float)$latitude2))) + (cos(deg2rad((float)$latitude1)) * cos(deg2rad((float)$latitude2)) * cos(deg2rad((float)$theta))); 
      $distance = acos($distance); 
      $distance = rad2deg($distance); 
      $distance = $distance * 60 * 1.1515; 
      switch($unit) { 
        case 'miles': 
          break; 
        case 'kilometers' : 
          $distance = $distance * 1.609344; 
      } 
      return (round($distance,2)); 
    }

}
