<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Hotel extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('Hotels');
	}
	public function list_get()
	{

		$current_date = current_date();
	
		$types = array();
		$query = $this->db->select('id,name,address')->from('hotel')
			->where('status', 1)
			->order_by('hotel.id', 'desc')
			->get();
		if ($query->num_rows()>0)
		{
			$types = $query->result();
			$data['status'] = true;
			$data['data'] = $types;
		}else {
			$data['status'] = false; 
		}
			
		$this->response($data);
		
	}

	public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		$err_array = explode('|', $err_str);
		$err_array = array_filter($err_array);
		return $err_array;
	}

}
