<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
class Jumio extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		
		$this->load->model('Country');
	}
	public function list_get()
	{
		$types = Country::select('id','name','days')->whereStatus(1)->orderBy('name','asc')->get();
		$data['status'] = true;
		$data['data'] = $types;
		$this->response($data);
	}

    public function create_account_post() {
        $ch = curl_init();
        $res2 = array();
        $data1 ="grant_type=client_credentials";
        $api_key = "658h455ob52nfbtdrcvcir9guj";
        $secret_key = "1kqivtadhc333m3hfii1kh2hv7npdq84bfajrcj24c4m3ehqjhmo";
        $username =  base64_encode($api_key.":".$secret_key);

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://auth.emea-1.jumio.ai/oauth2/token',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
          CURLOPT_HTTPHEADER => array(
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic '.$username
            // 'Authorization: Basic NjU4aDQ1NW9iNTJuZmJ0ZHJjdmNpcjlndWo6MWtxaXZ0YWRoYzMzM20zaGZpaTFraDJodjducGRxODRiZmFqcmNqMjRjNG0zZWhxamhtbw==',
          //  'Cookie: XSRF-TOKEN=47405cb3-0563-48f1-945b-d6b43b8747fd'
          ),
        ));

        $res1 = curl_exec($curl);

        curl_close($curl);
        $res = json_decode($res1);

        if (isset($res->access_token)) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://account.emea-1.jumio.ai/api/v1/accounts/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                    "customerInternalReference": "CUSTOMER_INTERNAL_REFERENCE",
                    "workflowDefinition": {
                    "key": 3,
                    "credentials": [
                                                {
                            "category": "ID",
                            "type": {
                                "values": ["DRIVING_LICENSE", "ID_CARD", "PASSPORT"]
                            }
                        }
                    ]
                },
                    "callbackUrl": "https://dev.coronatest.co.uk/",
                    "userReference": "110005255"
                }',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'User-Agent: User Demo',
                'Authorization: Bearer '.$res->access_token,
                'Content-Type: application/json'
              ),
            )); 

            $response = curl_exec($curl);

            curl_close($curl);
            if (!empty($response)) {
                $result = json_decode($response);
                $res2['access_token'] = $res->access_token;
                $res2['sdk_token'] = $result->sdk->token;
                $res2['web_href'] = $result->web->href;

                $res2['account_id'] = $result->account->id;
                $res2['workflow_id'] = $result->workflowExecution->id;
                $res2['status'] = true;
                
            }else{

                $res2['access_token'] = "";
                $res2['sdk_token'] = "";
                $res2['account_id'] = "";
                $res2['workflow_id'] = "";
                $res2['status'] = false;
            }
            // echo $response;
            $data =$res2; 
            $this->response($data);

        }
       // code...
    }

    public function jumio_status_post() {

        $this->form_validation->set_rules('token', 'Token', 'required');
        $this->form_validation->set_rules('account_id', 'Account Id', 'required');
        $this->form_validation->set_rules('workflow_id', 'Workflow Id', 'required');

        $req = $this->post();

        if ($this->form_validation->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        }else{
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://retrieval.emea-1.jumio.ai/api/v1/accounts/'.$req['account_id'].'/workflow-executions/'.$req['workflow_id'].'/status',
              //CURLOPT_URL => 'https://retrieval.emea-1.jumio.ai/api/v1/accounts/86ec9fcc-17aa-41ee-8890-a2436b0b234b/workflow-executions/e41f5073-d235-4e7c-aa1f-17489d64f8c1',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Bearer '.$req['token']
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $this->response(json_decode($response));
        }

        // echo $response;

    }

	public function get_workflow_details_post() {

        $this->form_validation->set_rules('token', 'Token', 'required');
        $this->form_validation->set_rules('account_id', 'Account Id', 'required');
        $this->form_validation->set_rules('workflow_id', 'Workflow Id', 'required');

        $req = $this->post();

        if ($this->form_validation->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        }else{
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://retrieval.emea-1.jumio.ai/api/v1/accounts/'.$req['account_id'].'/workflow-executions/'.$req['workflow_id'],
              //CURLOPT_URL => 'https://retrieval.emea-1.jumio.ai/api/v1/accounts/86ec9fcc-17aa-41ee-8890-a2436b0b234b/workflow-executions/e41f5073-d235-4e7c-aa1f-17489d64f8c1',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Bearer '.$req['token']
              ),
            ));

            $response = curl_exec($curl);
			$response = json_decode($response);
			$res_data['status'] = true;
			$res_data['data'] = array();
			$status = "INITIATED";
			if(isset($response->workflow->status)){
				$status = $response->workflow->status;
			}

			$response_data['decision_type'] = "NOT_EXECUTED";
			$response_data['status'] = $status;
            if($status == "PROCESSED"){
                if(isset($response->capabilities->extraction[0]->data) && !empty($response->capabilities->extraction[0]->data)){
                    $data = $response->capabilities->extraction[0]->data;
                    $response_data['type'] = isset($data->type)?$data->type:'';
                    $response_data['subType'] = isset($data->subType)?$data->subType:'';
                    $response_data['issuingCountry'] = isset($data->issuingCountry)?$data->issuingCountry:'';
                    $response_data['firstName'] = isset($data->firstName)?$data->firstName:'';
                    $response_data['lastName'] = isset($data->lastName)?$data->lastName:'';
                    $response_data['dateOfBirth'] = isset($data->dateOfBirth)?$data->dateOfBirth:'';
                    $response_data['documentNumber'] = isset($data->documentNumber)?$data->documentNumber:'';
                    $response_data['decision_type'] = isset($response->capabilities->extraction[0]->decision->type)?$response->capabilities->extraction[0]->decision->type:'NOT_EXECUTED';

                }
            }
			$res_data['data'] = $response_data;
            if ($response_data['decision_type'] !="PASSED") {
                $res_data['message'] = "There is an issue with your ID or face verification is failed, Please try again.";
            }
            curl_close($curl);
            $this->response($res_data);
        }

        // echo $response;

    }

	public function demo_post() {

        $api_key = "1ifsd1q8qpi9a6g51e5862ci34";
        $secret_key = "a8nft7m1pic7bclgb8b0bclu6i0ol2mh68cmtram0jegm364pq7";
        // _pre($api_key);
        $data1 ="{grant_type:client_credentials}";

        $username =  $api_key.":".$secret_key;
   
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://auth.amer-1.jumio.ai/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, $data1);
        curl_setopt($ch, CURLOPT_USERPWD, '658h455ob52nfbtdrcvcir9guj' . ':' . '1kqivtadhc333m3hfii1kh2hv7npdq84bfajrcj24c4m3ehqjhmo');
        // curl_setopt($ch, CURLOPT_USERPWD, '4b110fab-1cbb-4932-8966-12552b714595' . ':' . 'bwCcwC28mye4RTgaPzCVZsYazitMHysY');

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: user '.$username;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        _pre($result);
    }
}
