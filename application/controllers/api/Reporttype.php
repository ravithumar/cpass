<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Reporttype extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('Country');
	}
	public function reportbycountry_old_post() {

		$id = $this->input->post('id');
		$purpose_id = $this->input->post('purpose_id');
				
		$types = array();
		$rules = array();
		if ($purpose_id =="1") {
			$query = $this->db->select('id,name,price')->where('is_domestic', 'yes')->order_by('id', 'desc')->where('status', 1)->get('reports');
		}elseif ($purpose_id == "3") {
			if($this->input->post('step')){
				if ($this->input->post('step') !="") { 
					$query = $this->db->select('id,name,price')->where('id', '5')->order_by('id', 'desc')->where('status', 1)->get('reports');
				} else{
					$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
				}
			}else{
				$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
			}
		}elseif ($purpose_id == "5") {
			$request = $this->input->post();
			if(isset($request['excempted_cat']) && isset($request['vaccination_status'])) {
				$str = "FIND_IN_SET('".$request['vaccination_status']."',vaccination_status) AND 1=1";
				$query1 = $this->db->select('id,description')->where('cat_id', $request['excempted_cat'])->where($str)->get('category_rules');
				$rules = $query1->row_array();
				if (!empty($rules)) {
					$data['rules'] = $rules['description'];
				}else{
					$data['rules'] = "";
				}
				if(isset($request['country_id'])){
					$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $request['country_id'])->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
				}
				if(isset($request['vaccination_status']) && isset($request['country_id'])) { 
					$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $request['country_id'])->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
				}else{
					$query = $this->db->select('id,name,price')->where('id', '7')->order_by('id', 'desc')->where('status', 1)->get('reports');
				}
			}else{
				$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
			}
		}elseif ($purpose_id == "4"){
			// country wise report will come...
			$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
		}else{
			$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->join('country','country.zone = reports.zone','left')->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
		}
		if ($query->num_rows()>0) {
			$types = $query->result();
			$data['status'] = true;
			$data['tax_price'] = TAX_PRICE;
			$data['data'] = $types;
			
			$this->response($data);
		}else {
			$data['status'] = false; 
			$data['tax_price'] = TAX_PRICE;
			$data['rules'] = "";
			$this->response($data);
		}
		
	}

	public function reportbycountry_post() {

		$id = $this->input->post('id');
		$purpose_id = $this->input->post('purpose_id');
		$is_booking = true;
		$types = array();
		$rules = array();
		$find_set = "FIND_IN_SET('".$purpose_id."',purpose_type) AND 1=1";
		if ($purpose_id =="1") {
			//$query = $this->db->select('id,name,price')->where('is_domestic', 'yes')->order_by('id', 'desc')->where('status', 1)->where($find_set)->get('reports');
			$query = $this->db->select('reports.id,reports.name,reports.price')->where($find_set)->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left')->group_by("reports.id")->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
		}elseif ($purpose_id == "3") {
			if($this->input->post('step')){
				if ($this->input->post('step') !="") { 
					$query = $this->db->select('id,name,price')->where('id', '5')->order_by('id', 'desc')->where('status', 1)->get('reports');
				} else{
					$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->where($find_set)->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left')->group_by("reports.id")->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
				}
			}else{
				$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->where($find_set)->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left')->group_by("reports.id")->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
			}
		}elseif ($purpose_id == "5") {

			$request = $this->input->post();
			//$vaccine_status_array = $this->config->item("vaccine_status");
			
			if(isset($request['excempted_cat']) && isset($request['vaccination_status'])) {
				//$vaccine_status_index = array_search($request['vaccination_status'],array_column($vaccine_status_array,"name"));
				//$vaccine_status_id = $vaccine_status_array[$vaccine_status_index]["id"];
				$str = "FIND_IN_SET('".$request['vaccination_status']."',vaccination_status) AND 1=1";
				$query1 = $this->db->select('id,description')->where('cat_id', $request['excempted_cat'])->where($str)->get('category_rules');
				$rules = $query1->row_array();
				if (!empty($rules)) {
					$data['rules'] = $rules['description'];
				}else{
					$data['rules'] = "";
				}				
			}
			$this->db->select("reports.id,reports.name,reports.price");
			$this->db->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left');

			if(isset($request['vaccination_status']) && !empty($request['vaccination_status'])){
				//$vaccine_status_index = array_search($request['vaccination_status'],array_column($vaccine_status_array,"name"));
				//$vaccine_status_id = $vaccine_status_array[$vaccine_status_index]["id"];
				$vaccination_status_where = "FIND_IN_SET('".$request['vaccination_status']."',reports.vaccination_status) AND 1=1";
				$this->db->where($vaccination_status_where);
			}

			if(isset($request['country_id']) && !empty($request['country_id'])){
				$this->db->where("country.id",$request['country_id']);
			}
			/*if(isset($request['is_medical_excempted']) && !empty($request['is_medical_excempted'])){
				$this->db->where("reports.medical_exemptions",$request['is_medical_excempted']);
			}*/
			if(isset($request['vaccine_id']) && !empty($request['vaccine_id'])){
				$vaccine_where = "FIND_IN_SET('".$request['vaccine_id']."',reports.vaccine) AND 1=1";
				$this->db->where($vaccine_where);
			}
			$this->db->where($find_set);
			$this->db->where("reports.status",1);
			$this->db->group_by("reports.id");
			$this->db->order_by('reports.id', 'desc');
			$query = $this->db->get('reports');

			if(isset($request['country_id']) && !empty($request['country_id'])){
				$country_zone = get_column_value("country",array("id"=>$request['country_id']),"zone");
				if($country_zone == 1){
					$is_booking = false;
				}
			}

		}elseif ($purpose_id == "4"){
			// country wise report will come...
			$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->where($find_set)->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left')->group_by("reports.id")->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
		}else{
			$query = $this->db->select('reports.id,reports.name,reports.price')->where('country.id', $id)->where($find_set)->join('country','FIND_IN_SET(country.zone, reports.zone) > 0','left')->group_by("reports.id")->order_by('id', 'desc')->where('reports.status', 1)->get('reports');
		}

		
		if($is_booking){
			if ($query->num_rows()>0) {
				$types = $query->result();
				$data['status'] = true;
				$data['tax_price'] = TAX_PRICE;
				$data['is_booking'] = true;
				$data['data'] = $types;
				$this->response($data);
			}else {
				$data['status'] = true; 
				$data['tax_price'] = TAX_PRICE;
				$data['is_booking'] = true;
				$data['rules'] = "";
				$data['data'] = array();
				$this->response($data);
			}
		}else{
			$data['status'] = true; 
			$data['tax_price'] = TAX_PRICE;
			$data['rules'] = "";
			$data['is_booking'] = false;
			$data['data'] = array();
			$data['booking_link'] = "https://quarantinehotelbookings.ctmportal.co.uk/";
			$this->response($data);
		}
		
	}
}
