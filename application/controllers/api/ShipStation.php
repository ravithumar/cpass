<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class ShipStation extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
	}
	public function add_order_post()
	{
		$this->form_validation->set_rules('orderNumber', 'Order Number', 'required');

        // $data['orderNumber'] = "10023224357";
        // $data['orderDate'] = "2021-09-28T08:46:27.0000000";
        // $data['orderStatus'] = "awaiting_shipment";
        // $data['billTo'] = "10023224357";
        // $data['shipTo'] = "10023224357";
        // getReports
        $data1 = '{"orderNumber":"1002322435","orderDate":"2021-09-29T08:46:27.0000000","paymentDate":"2021-09-29T08:46:27.0000000","shipByDate":"2021-09-29T00:00:00.0000000","orderStatus":"awaiting_shipment","customerId":37701499,"customerUsername":"dharii@yopmail.com","customerEmail":"dharii@yopmail.com","billTo":{"name":"Terris"},"shipTo":{"name":"The President","company":"US Govt","street1":"1600 Pennsylvania Ave 1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave1600 Pennsylvania Ave","city":"Washington","state":"DC","postalCode":"","country":"US","phone":"555-555-5555"},"items":[{"lineItemKey":"vd08-MSLbtx","sku":"ABC123","name":"Test item #1","weight":{"value":24,"units":"ounces"},"quantity":2,"productId":5072970}],"customerNotes":"Please ship as soon as possible!","internalNotes":"Customer called and would like to upgrade shipping","paymentMethod":"Credit Card","shipDate":"2021-09-29","weight":{"value":25,"units":"ounces"},"advancedOptions":{"storeId":38870,"source":"Webstore"}}';
        // $data1 = "{\n \"orderNumber\": \"1002322435\",\n  \"orderDate\": \"2021-09-28T08:46:27.0000000\",\n  \"paymentDate\": \"2021-09-28T08:46:27.0000000\",\n  \"shipByDate\": \"2021-09-29T00:00:00.0000000\",\n  \"orderStatus\": \"awaiting_shipment\",\n  \"customerId\": 37701499,\n  \"customerUsername\": \"dharii@yopmail.com\",\n  \"customerEmail\": \"dharii@yopmail.com\",\n  \"billTo\": {\n    \"name\": \"Terris\",\n    \"company\": null,\n    \"street1\": null,\n    \"street2\": null,\n    \"street3\": null,\n    \"city\": null,\n    \"state\": null,\n    \"postalCode\": null,\n    \"country\": null,\n    \"phone\": null,\n    \"residential\": null\n  },\n  \"shipTo\": {\n    \"name\": \"The President\",\n    \"company\": \"US Govt\",\n    \"street1\": \"1600 Pennsylvania Ave\",\n    \"street2\": \"Oval Office\",\n    \"street3\": null,\n    \"city\": \"Washington\",\n    \"state\": \"DC\",\n    \"postalCode\": \"20500\",\n    \"country\": \"US\",\n    \"phone\": \"555-555-5555\",\n    \"residential\": true\n  },\n  \"items\": [\n    {\n      \"lineItemKey\": \"vd08-MSLbtx\",\n      \"sku\": \"ABC123\",\n      \"name\": \"Test item #1\",\n      \"imageUrl\": null,\n      \"weight\": {\n        \"value\": 24,\n        \"units\": \"ounces\"\n      },\n      \"quantity\": 2,\n      \"unitPrice\": 0.00,\n      \"taxAmount\": 0.00,\n      \"shippingAmount\": 5,\n      \"warehouseLocation\": \"Aisle 1, Bin 7\",\n      \"productId\": 5072970,\n      \"adjustment\": false,\n      \"upc\": \"32-65-98\"\n    }],\n  \"amountPaid\": 0.00,\n  \"taxAmount\": 5,\n  \"shippingAmount\": 10,\n  \"customerNotes\": \"Please ship as soon as possible!\",\n  \"internalNotes\": \"Customer called and would like to upgrade shipping\",\n  \"gift\": true,\n  \"giftMessage\": \"Thank you!\",\n  \"paymentMethod\": \"Credit Card\",\n  \"requestedShippingService\": \"Priority Mail\",\n  \"carrierCode\": \"fedex\",\n  \"serviceCode\": \"fedex_2day\",\n  \"packageCode\": \"package\",\n  \"confirmation\": \"delivery\",\n  \"shipDate\": \"2021-09-29\",\n  \"weight\": {\n    \"value\": 25,\n    \"units\": \"ounces\"\n  },\n  \"insuranceOptions\": {\n    \"provider\": \"carrier\",\n    \"insureShipment\": true,\n    \"insuredValue\": 200\n  },\n  \"advancedOptions\": {\n    \"storeId\": 38870,\n    \"source\": \"Webstore\",\n    \"billToParty\": null,\n    \"billToAccount\": null,\n    \"billToPostalCode\": null,\n    \"billToCountryCode\": null\n  }}";
        // _pre($data1);

        // storeId\": 38870 , productId\": 5072970
        $curl = curl_init();
        $api_key = SHIP_API_KEY;
        $secret_key = SHIP_SECRET_KEY;
        $username =  base64_encode($api_key.":".$secret_key);

        curl_setopt_array($curl, array(
            CURLOPT_URL => SHIP_URL."orders/createorder",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$data1,
            
 
            CURLOPT_HTTPHEADER => array(
                "Host: ssapi.shipstation.com",
                "Authorization: Basic ".$username,
                "Content-Type: application/json"

            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);
        // _pre($response);

		// if ($this->form_validation->run() == FALSE)
  //       {
  //           $data['status']=false;
  //           $data['error'] = validation_errors_response(); 
  //       }   
  //       else
  //       {		
        	$data = $response;
        // }
        $this->response($data);
	}
}
