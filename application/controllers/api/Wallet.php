<?php defined('BASEPATH') or exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;
use PKPass\PKPass;

class Wallet extends REST_Controller
{
    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('ion_auth_model');
        $this->load->library(['ion_auth', 'form_validation']);
        // date_default_timezone_set('Asia/Kolkata');
    }

    public function add_booking_post() {
        $data['status'] = false;

        $request = $this->post();
        $pass = new PKPass(base_url()."Certificates.p12", 'apple');

        $booking_id = time();
        $name = "Jogn Doe";
        $result = "POSITIVE";
        $test_type = "PCR Same Day";

        // Variables
        $id = rand(100000, 999999) . '-' . rand(100, 999) . '-' . rand(100, 999); // Every card should have a unique serialNumber
        $balance = '$' . rand(0, 30) . '.' . rand(10, 99); // Create random balance
        $name = stripslashes($name);

        $pass->setData('{
            "passTypeIdentifier": "pass.com.g16.CPass", 
            "formatVersion": 1,
            "organizationName": "CPASS",
            "teamIdentifier": "D483AH4KH4",
            "serialNumber": "' . $id . '",
            "backgroundColor": "rgb(240,240,240)",
            "logoText": "C-PASS",
            "description": "C Pass",
            "storeCard": {
                "primaryFields": [
                    {
                        "key": "name",
                        "label": "NAME",
                        "value": "' . $name . '"
                    },
                    {
                        "key": "result",
                        "label": "RESULT",
                        "value": "' . $result . '"
                    }

                ],
                "secondaryFields": [
                    {
                        "key": "yxx",
                        "label": "PERSON",
                        "value": "' . $name . '"
                    }

                ],
                "auxiliaryFields": [
                    {
                        "key": "resultd",
                        "label": "RESULTD",
                        "value": "' . $result . '"
                    },
                    {
                        "key": "test_type",
                        "label": "TEST TYPE",
                        "value": "' . $test_type . '"
                    }

                ],
                "backFields": [
                    {
                        "key": "id",
                        "label": "Booking Number",
                        "value": "' . $booking_id . '"
                    }
                ]
            }
            }');

        // add files to the PKPass package
        $pass->addFile('/var/www/dev.coronatest.co.uk/application/controllers/api/icon.png');
        // $pass->addFile('icon@2x.png');
        $pass->addFile('/var/www/dev.coronatest.co.uk/application/controllers/api/logo.png');
        $pass->addFile('/var/www/dev.coronatest.co.uk/application/controllers/api/background.png');

        if(!$pass->create(true, $booking_id)) { // Create and output the PKPass
            $response['error'] = 'Error: ' . $pass->getError();
        }

        
        $this->response($pass);
    }
}