<?php 
/**
 * summary
 */
// require APPPATH . '/libraries/REST_Controller.php';
abstract class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        /* if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }   */
    }

    public function renderWeb($page = null,$params = null, $return = false) {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('web/layout/header',$params);
        
        if($page != null){
            $this->load->view('web/pages/'.$page,$params,$return);
        }
        $this->load->view('web/layout/footer',$params);
    }
    
    public function renderAdmin($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('admin/layout/header',$params);
        if($page != null){
            $this->load->view('admin/pages/'.$page,$params,$return);
        }
        $this->load->view('admin/layout/footer',$params);
    }
    public function renderNetwork($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('network/layout/header',$params);
        if($page != null){
            $this->load->view('network/pages/'.$page,$params,$return);
        }
        $this->load->view('network/layout/footer',$params);
    }

    public function renderHospital($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('hospital/layout/header',$params);
        if($page != null){
            $this->load->view('hospital/pages/'.$page,$params,$return);
        }
        $this->load->view('hospital/layout/footer',$params);
    }
    public function renderLab($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('lab/layout/header',$params);
        if($page != null){
            $this->load->view('lab/pages/'.$page,$params,$return);
        }
        $this->load->view('lab/layout/footer',$params);
    }
    public function renderGov($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('gov/layout/header',$params);
        if($page != null){
            $this->load->view('gov/pages/'.$page,$params,$return);
        }
        $this->load->view('gov/layout/footer',$params);
    }
    
    public function _generate_body_class()
    {
        if($this->uri->segment_array() == null){
            $uri = array('index');
        }else{
            $uri = $this->uri->segment_array();
            if(end($uri) == 'index'){
                array_pop($uri);
            }
        }
        return implode('-', $uri);
    }
}
