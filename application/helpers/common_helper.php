<?php

if (!function_exists('check_file_exist_without_base'))
{	
	function check_file_exist_without_base($file)
	{
        $file = file_exists($file) ? $file : 'assets/images/common/no-image.jpg';
        return $file;
	}
}

if (!function_exists('check_file_exist'))
{   
    function check_file_exist($file)
    {
        $file = file_exists($file) ? BASE_URL.$file : BASE_URL.'assets/images/common/no-image.jpg';
        return $file;
    }
}
if (!function_exists('get_column_value'))
{   
    function get_column_value($table,$array,$column_name)
    {
        $ci=& get_instance();
        $data = $ci->db->select($column_name)->get_where($table,$array)->row_array();  
		if(!empty($data)){
			return $data[$column_name];
		}      
        return false;
    }
}

if (!function_exists('check_data'))
{   
    function check_data($table,$array)
    {
        $ci=& get_instance();
        $data = $ci->db->get_where($table,$array)->num_rows();                
        return $data;
    }
}

if (!function_exists('get_single_record'))
{   
    function get_single_record($table,$array)
    {
        $ci=& get_instance();
        $data = $ci->db->get_where($table,$array)->row_array();        
        return $data;
    }
}
if (!function_exists('get_multi_record'))
{   
    function get_multi_record($table,$array)
    {
        $ci=& get_instance();
        $data = $ci->db->get_where($table,$array)->result_array();        
        return $data;
    }
}


if (!function_exists('num_format_value'))
{   
    function num_format_value($value)
    { 
        $num_fromated_value = number_format($value,2);
        return $num_fromated_value;
    }
}

if (!function_exists('round_value'))
{   
    function round_value($value)
    { 
        $rounded_value = round($value,2);
        return $rounded_value;
    }
} 

if (!function_exists('convert_seconds'))
{   
    function convert_seconds($value)
    { 
        $timeformat = gmdate("H:i:s", $value);
        return $timeformat;
    }
}
if (!function_exists('append_currency'))
{   
    function append_currency($value)
    { 
        $ci=& get_instance();
        $currency = CURRENCY;
        $value = $currency.number_format($value,2);
        return $value;
    }
}


if (!function_exists('__date'))

{	

	function __date($date, $timestamp=false)

	{

		if($timestamp)

		{
            return date('j M, Y h:i A', $date);
            
		}

		else

		{

			return date('j M, Y h:i A', strtotime($date));

		}

	}

}


if (!function_exists('_pre'))
{	
	function _pre($array)
	{
        echo "<pre>";
        print_r($array);
        echo "<pre>";
        exit;
	}
}

if (!function_exists('send_mail'))
{
	function send_mail($to,$subject,$message)
	{
        //print_r($message);exit;
        $ci=& get_instance();
		$mail_type = MAIL_TYPE;
        if($mail_type == 'SMTP')
        {
            $config['protocol'] = "smtp";
            $config['smtp_host'] = SMTP_HOST;
            $config['smtp_port'] = SMTP_PORT;
            $config['smtp_user'] = SMTP_USER;
            $config['smtp_pass'] = SMTP_PASS;
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $ci->email->initialize($config);

            $ci->email->from(SMTP_FROM,SMTP_HEADER);           
            $ci->email->to($to);
            // $ci->email->reply_to('my-email@gmail.com', 'Explendid Videos');
            $ci->email->subject($subject);
            $ci->email->message($message);
            //$ci->email->attach('http://example.com/filename.pdf');
            //$ci->email->attach('/path/to/photo3.jpg');
            $ci->email->send();
            //echo $ci->email->print_debugger();exit;
        }
        else if($mail_type == 'mandrill')
        {
            $ci->load->config('mandrill');
            $ci->load->library('mandrill');

            $mandrill_ready = NULL;

            try 
            {
                $ci->mandrill->init( $ci->config->item('mandrill_api_key') );
                $mandrill_ready = TRUE;
                
            } 
            catch(Mandrill_Exception $e) 
            {
                $mandrill_ready = FALSE;                
            }

            if($mandrill_ready) 
            {

                //Send us some email!
                $email = array(
                    'html' => $message, //Consider using a view file
                   // 'text' => ,
                    'subject' => $subject,
                    'from_email' => $ci->config->item('from_email'),
                    'from_name' => $ci->config->item('from_name'),
                    'to' => array(array('email' => $to)) //Check documentation for more details on ci one
                    //'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
                    );

                $result = $ci->mandrill->messages_send($email);
                
            } 
        }  
	}
}

if (!function_exists('send_push'))
{   
    function send_push($user_id = '',$user_type = '',$push_title = '',$push_message = '',$message_type = '')
    {
        $ci=& get_instance();
        if($user_id != '' && $user_type != '')
        {
            $ci->db->select(array('device_type','device_token'));
            $row = (array)$ci->db->get_where($user_type,array('id' => $user_id))->row();

            $key = FCM_KEY;
            define('API_ACCESS_KEY', $key);
            $token = $row['device_token']; 

            if($row['device_type'] == 'ios')
            {
                $fcmFields = array(
                    'priority' => 'high',
                    'to' => $token,
                    'sound' => 'default',
                    'notification' => array( 
                        "title"=> $push_title,
                        "body"=> $push_message,
                        "type"=> $message_type,
                        )
                    );
            }
            else
            {
                $fcmFields = array(
                    'priority' => 'high',
                    'to' => $token,
                    'sound' => 'default',
                    'data' => array( 
                        "title"=> $push_title,
                        "body"=> $push_message,
                        "type"=> $message_type,
                        )
                    );
            }

            $headers = array('Authorization: key=' . API_ACCESS_KEY,'Content-Type: application/json');
             
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
            //echo $result . "\n\n";exit;
        }
    }
}

if (!function_exists('send_push_bulk'))
{   
    function send_push_bulk($user_id = '',$user_type = '',$device_type = '',$device_token ='',$push_title = '',$push_message = '',$message_type = '')
    {
        $ci=& get_instance();
        if($user_id != '' && $user_type != '')
        {           

            $key = FCM_KEY;
            define('API_ACCESS_KEY', $key);
            $token = $device_token; 

            if($device_type == 'ios')
            {
                $fcmFields = array(
                    'priority' => 'high',
                    'to' => $token,
                    'sound' => 'default',
                    'notification' => array( 
                        "title"=> $push_title,
                        "body"=> $push_message,
                        "type"=> $message_type,
                        )
                    );
            }
            else
            {
                $fcmFields = array(
                    'priority' => 'high',
                    'to' => $token,
                    'sound' => 'default',
                    'data' => array( 
                        "title"=> $push_title,
                        "body"=> $push_message,
                        "type"=> $message_type,
                        )
                    );
            }

            $headers = array('Authorization: key=' . API_ACCESS_KEY,'Content-Type: application/json');
             
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
            //echo $result . "\n\n";
        }
    }
}


if (!function_exists('create_email_template'))
{
	function create_email_template($template)
	{
        $ci=& get_instance();
		$base_url = BASE_URL();
		$template = str_replace('##SITEURL##', $base_url, $template);
		$template = str_replace('##SITENAME##', SITE_NAME, $template);
		$template = str_replace('##SITEEMAIL##', SITE_EMAIL, $template);
		$template = str_replace('##COPYRIGHTS##', COPYRIGHTS, $template);
		$template = str_replace('##EMAILTEMPLATEHEADERLOGO##', LOGO_EMAIL_HEADER , $template);
		$template = str_replace('##EMAILTEMPLATEFOOTERLOGO##', LOGO_EMAIL_FOOTER , $template);
		$template = str_replace('##CURRENCY##', CURRENCY, $template);         
        $template = str_replace('##TEMPLATECOLOR##', TEMPLATE_COLOR, $template);         
		return $template;
	}
}

if (!function_exists('generate_qr_code'))
{
    function generate_qr_code($string)
    {
        $url = '';
        if($string != '')
        {
            $unique_code = base64_encode($string);
            
            $googleapis = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=".$unique_code."&choe=UTF-8";
            $file = file_get_contents($googleapis);

            $path = "./assets/images/qrcode/".$unique_code.".png";
            file_put_contents($path, $file);

            $url = "assets/images/qrcode/".$unique_code.".png";
        }
        return $url;
    }
}

if (!function_exists('upload_image'))
{
    function upload_image($image_name,$folder_name,$id)
    {
        $ci=& get_instance();
        if(!is_dir('assets/images/'.$folder_name.'/')){
            mkdir('assets/images/'.$folder_name, 0777,true);         
            chmod('assets/images/'.$folder_name, 0777);
        }

        $config['upload_path']   = 'assets/images/'.$folder_name;
        $config['allowed_types'] = '*';
        $config['max_size']      = 20240;
        $config['encrypt_name'] = TRUE;
        $ci->load->library('upload', $config);
        if (!$ci->upload->do_upload($image_name)) {
            $error            = $ci->upload->display_errors();
            $return['status'] = 0;
            $return['error']  = $error;
        } else {
            $upload_data      = $ci->upload->data();
            $file_name        = $upload_data['file_name'];
            $return['status'] = 1;
            $return['path']   = 'assets/images/'.$folder_name.'/'.$file_name;
        }
        return $return;
    }
}

if (!function_exists('upload_image2'))
{    
    function upload_image2($image_name,$folder_name,$id)
    {
        $CI = get_instance();
        if(!is_dir('assets/uploads/'.$folder_name.'/')){
            mkdir('assets/uploads/'.$folder_name, 0777,true);         
            chmod('assets/uploads/'.$folder_name, 0777);
        }
        if(!is_dir('assets/images/'.$folder_name.'/'.$id.'/')){
            mkdir('assets/images/'.$folder_name.'/'.$id, 0777,true);         
            chmod('assets/images/'.$folder_name.'/'.$id, 0777);
        }
        $config['upload_path']   = 'assets/images/'.$folder_name.'/'.$id;
        $config['allowed_types'] = '*';
        $config['max_size']      = 20240;
        $config['encrypt_name'] = TRUE;
        //$config['image_lib'] = 'gd2';
        $CI->load->library('upload', $config);
        if (!$CI->upload->do_upload($image_name)) {
            $error            = $CI->upload->display_errors();
            $return['status'] = 0;
            $return['error']  = $error;
        } else {
            $upload_data      = $CI->upload->data();
            $file_name        = $upload_data['file_name'];
            $return['status'] = 1;
            $return['path']   = 'assets/images/'.$folder_name.'/'.$id.'/'.$file_name;
        }
        $quality = 70;
        $source_url = $return['path'];
        $destination_url = $return['path'];
        $info = getimagesize($source_url);

        //print_r($info);exit;

        if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($source_url);

        elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($source_url);

        elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($source_url);

        imagejpeg($image, $destination_url, $quality);
        return $return;
    }
}

if (!function_exists('upload_video'))
{
    function upload_video($image_name,$folder_name,$id)
    {
        $ci=& get_instance();
        if(!is_dir('assets/images/'.$folder_name.'/')){
            mkdir('assets/images/'.$folder_name, 0777,true);         
            chmod('assets/images/'.$folder_name, 0777);
        }
        if(!is_dir('assets/images/'.$folder_name.'/'.$id.'/')){
            mkdir('assets/images/'.$folder_name.'/'.$id, 0777,true);         
            chmod('assets/images/'.$folder_name.'/'.$id, 0777);
        }
        $config['upload_path']   = 'assets/images/'.$folder_name.'/'.$id;
        $config['allowed_types'] = '*';        
        $config['encrypt_name'] = TRUE;
        $ci->load->library('upload', $config);
        if (!$ci->upload->do_upload($image_name)) {
            $error            = $ci->upload->display_errors();
            $return['status'] = 0;
            $return['error']  = $error;
        } else {
            $upload_data      = $ci->upload->data();
            $file_name        = $upload_data['file_name'];
            $return['status'] = 1;
            $return['path']   = 'assets/images/'.$folder_name.'/'.$id.'/'.$file_name;
        }
        return $return;
    }
}

if (!function_exists('move_image'))
{
    function move_image($driver_id,$img_path_temp)
    {
        if(!is_dir('assets/images/driver/'.$driver_id.'/')){
            mkdir('assets/images/driver/'.$driver_id, 0777,true);         
            chmod('assets/images/driver/'.$driver_id, 0777);
        }

        $destination='/var/www/html/assets/images/driver/'.$driver_id;
        $org_image='/var/www/html/'.$img_path_temp;

        $img_name=basename($org_image);

        if(rename( $org_image , $destination.'/'.$img_name ))
        {
            return 'assets/images/driver/'.$driver_id.'/'.$img_name;
        } 
        else 
        {
            return "";
        }
    }
}

if (!function_exists('validate_post_fields'))
{
    function validate_post_fields($post_fields)
    {
        $error = array();
        foreach ($post_fields as $field => $value) 
        {
            if(!isset($field) || $value == '' || is_null($value) || $value == 'undefined')
            {
                $error[]= $field ." paramter missing"; 
            }
        }
        return $error;
    }
}

if (!function_exists('validate_file_fields'))
{
    function validate_file_fields($post_fields)
    {
        $error = array();
        foreach ($file_fields as $field => $value) 
        {
            if($_FILES[$field]['name'] == '' ||  $_FILES[$field]['name'] == null)
            {
                $error[]= $field ." File Is Missing"; 
            }
        }
        return $error;
    }

}

if (!function_exists('string_encrypt'))
{
    function string_encrypt($string) 
    {
        $ci=& get_instance(); 
        $key = ENCRYPTION_KEY;
        $result = '';
        for($i=0, $k= strlen($string); $i<$k; $i++) 
        {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)+ord($keychar));
            $result .= $char;
        }
        return base64_encode($result);
    }
}

if (!function_exists('string_decrypt'))
{
    function string_decrypt($string) 
    {
        $ci=& get_instance(); 
        $key = ENCRYPTION_KEY;
        $result = '';
        $string = base64_decode($string);
        for($i=0,$k=strlen($string); $i< $k ; $i++) 
        {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % strlen($key))-1, 1);
            $char = chr(ord($char)-ord($keychar));
            $result.=$char;
        }
        return $result;       
    }
}

if (!function_exists('check_valid_card'))
{
    function check_valid_card($number) 
    {
          $number=preg_replace('/\D/', '', $number);
          $number_length=strlen($number);
          $parity=$number_length % 2;
          $total=0;
          for ($i=0; $i<$number_length; $i++) {
            $digit=$number[$i];
            if ($i % 2 == $parity) {
              $digit*=2;
              if ($digit > 9) {
                $digit-=9;
              }
            }
            $total+=$digit;
          }
          return ($total % 10 == 0) ? TRUE : FALSE;
    }
}
   

if (!function_exists('get_card_type'))
{
    function get_card_type($str, $format = 'string')
    {
        if (empty($str)) {
            return false;
        }

        $matchingPatterns = [
            'visa' => '/^4[0-9]{12}(?:[0-9]{3})?$/',
            'mastercard' => '/^5[1-5][0-9]{14}$/',
            'amex' => '/^3[47][0-9]{13}$/',
            'diners' => '/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',
            'discover' => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
            'jcb' => '/^(?:2131|1800|35\d{3})\d{11}$/',
            'any' => '/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/'
        ];

        $ctr = 1;
        $type = 'other';
        foreach ($matchingPatterns as $key=>$pattern) {
            if (preg_match($pattern, $str)) {
                 $type = $key ;
                 break;
            }
            $ctr++;
        }
        return $type;
    }
}

if (!function_exists('pay_post'))
{   
    function pay_post($card_array = [])
    {
        if(!empty($card_array))
        {
            $ci=& get_instance();

            //$ci->load->library(); 

            $res['payment_status'] = 'success';
            $res['reference_id'] = rand(11111111,99999999);
            $data['status'] = true;
            $data['data']=$res;

        }else
        {

            $data['status'] = false;
            $data['data']=[];
        }
        return $data;
    }
}

if (!function_exists('get_distance'))
{   
    function get_distance($pickup_origins = "",$dropoff_origins = "")
    {
        if($pickup_origins != "" && $dropoff_origins != "")
        {
            $ci=& get_instance();  
            $key = GOOGLE_KEY;        
            $distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$pickup_origins."&destinations=".$dropoff_origins."&mode=driving&key=".$key;          
            $distance = (array)json_decode(file_get_contents($distance_url));
            $distance_type = DISTANCE_TYPE;          
            $meters = $distance['rows'][0]->elements[0]->distance->value;
            if($distance_type == 'km')
            {
                $total_distance = $meters / 1000;
            }
            else
            {
                $total_distance = ($meters / 1609.34);
            }            
            $duration = $distance['rows'][0]->elements[0]->duration->value;
            
            $data['distance'] =  (Float)$total_distance;
            $data['duration_in_second'] =  $duration;
            $data['duration_in_minute'] =  (Int)($duration / 60);            
            return $data;

        }
        else
        {
            $data['distance'] =  0;
            $data['duration_in_second'] =  0;
            $data['duration_in_minute'] =  0;            
            return $data;
        }        
    }
}
if (!function_exists('distance'))
{
    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2))
        {
            return 0;
        }
        else 
        {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
              return ($miles * 1.609344);
            } else if ($unit == "N") {
              return ($miles * 0.8684);
            } else {
              return $miles;
            }
        }
    }
}


if (!function_exists('validate_location'))
{   
    function validate_location($lat = "",$lng = "")
    {
        if($lat != "" && $lng != "" && $lat > 0 && $lng > 0 && $lat != undefined && $lng != undefined && $lat != null && $lng != null)
        {
            return true;
        }
        else
        {
            return false;   
        }        
    }
}

     function validate_key($key = '') {
        if($key != '') {            
            if(API_KEY  ==  $key) {
                return true; }
            else {
                return false; }
        } else {
            return false;
        }
    }

   
if (!function_exists('upload_user_image'))

{    

    function upload_user_image($image_name,$folder_name)

    {

        $CI = get_instance();

        if(!is_dir('assets/uploads/'.$folder_name.'/')){

            mkdir('assets/uploads/'.$folder_name, 0777,true);         

            chmod('assets/uploads/'.$folder_name, 0777);

        }


        $config['upload_path']   = 'assets/images/'.$folder_name;

        $config['allowed_types'] = '*';

        $config['max_size']      = 100000;

        $config['encrypt_name'] = TRUE;

        //$config['image_lib'] = 'gd2';

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload($image_name)) {

            $error            = $CI->upload->display_errors();

            $return['status'] = 0;

            $return['error']  = $error;

        } else {

            $upload_data      = $CI->upload->data();

            $file_name        = $upload_data['file_name'];

            $return['status'] = 1;

            $return['path']   = 'assets/images/'.$folder_name.'/'.$file_name;

        }

        $quality = 30;

        $source_url = $return['path'];

        $destination_url = $return['path'];

        $info = getimagesize($source_url);



        //print_r($info);exit;



        if ($info['mime'] == 'image/jpeg')

        $image = imagecreatefromjpeg($source_url);



        elseif ($info['mime'] == 'image/gif')

        $image = imagecreatefromgif($source_url);



        elseif ($info['mime'] == 'image/png')

        $image = imagecreatefrompng($source_url);



        imagejpeg($image, $destination_url, $quality);

        return $return;

    }

}
// if (!function_exists('api_call')){    

//     function api_call($url,$data=null){
//         $CI = get_instance();
//         $api_key =  's4w8scwsk0wo4o0wkg4s8w4g88s8koogs80448k8';        
//         /* Init cURL resource */
//         $ch = curl_init($url);
//         /* pass encoded JSON string to the POST fields */
//         if(!empty($data)){
//             curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//         }
//         /* set the content type json */
//         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                     'Content-Type:application/json',
//                     'x-api-key:'.$api_key
//                 ));
            
//         /* set return type json */
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
//         /* execute request */
//         $result = curl_exec($ch);
//         /* close cURL resource */
//         curl_close($ch);
//         return $result;
//     }

// }

if (!function_exists('api_call')){    

    function api_call($url,$data=null,$api_key = '',$method = ''){

        //_pre($url);
        $CI = get_instance();
        
        $curl = curl_init();
        // $api_key = $CI->session->userdata('members')['api_key'];

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            'x-api-key: '.$api_key,
            'Cookie: ci_session=e31mir92upckj5bikt91eul4rgbgjo3t'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

}

if (!function_exists('ethnicity')){    

    function ethnicity($api_key){
        $ci=& get_instance();
        $url = base_url().'/api/customer/nationalitylist';
        // $purpose_type_url = base_url().'api/reporttype/reportbycountry';
        $method = 'GET';
        //$api_key = $api_key;
        $post_data =null;
        $response = api_call($url,$post_data,$api_key,$method);
        $response = json_decode($response, true);
        // echo "</pre>";
        // print_r($response);
        // exit;
        return $response['data'];
    }
}

if (!function_exists('countries_list')){    

    function countries_list($api_key){
        $ci=& get_instance();
        $url = base_url().'/api/countries/list';
        // $purpose_type_url = base_url().'api/reporttype/reportbycountry';
        $method = 'GET';
        //$api_key = $api_key;
        $post_data =null;
        $response = api_call($url,[],$api_key,$method);
        $response = json_decode($response, true);
        // echo "</pre>";
        // print_r($response);
        // exit;
        return $response;
    }
}

if (!function_exists('exemptioncat_list')){    

    function exemptioncat_list($api_key){
        $ci=& get_instance();
        $url = base_url().'/api/customer/excempted_categories';
        // $purpose_type_url = base_url().'api/reporttype/reportbycountry';
        $method = 'GET';
        //$api_key = $api_key;
        $post_data =null;
        $response = api_call($url,[],$api_key,$method);
        $response = json_decode($response, true);
        // echo "</pre>";  print_r($response); exit;
        return $response;
    }
}


if (!function_exists('certified_vaccines_list')){    

    function certified_vaccines_list($api_key){
        $ci=& get_instance();
        $url = base_url().'/api/customer/certified_vaccines_list';
        $method = 'POST';
        $post_data =null;
        $response = api_call($url,[],$api_key,$method);
        $response = json_decode($response, true);
        return $response;
    }
}
