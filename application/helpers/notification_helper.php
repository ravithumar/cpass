<?php
if (!function_exists('SendNotification')) {
	function SendNotification($title, $message, $token, $data = []) {
		$API_SERVER_KEY = config('fcm_key');
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => $token,
			'priority' => 10,
			'notification' => array('title' => $title, 'body' => $message, 'sound' => 'Default', 'image' => 'Notification Image', 'data' => $data),
		);
		$headers = array(
			'Authorization:key=' . $API_SERVER_KEY,
			'Content-Type:application/json',
		);
		// Open connection
		$ch = curl_init();
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		return true;
	}
}
if (!function_exists('sendEmail')) {
	function sendEmail($subject, $to, $template, $data = []) {
		$CI = get_instance();
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => config('smtp_host'),
			'smtp_port' => config('smtp_port'),
			'smtp_user' => config('smtp_user'),
			'smtp_pass' => config('smtp_pass'),
			'mailtype' => 'html',
			'charset' => 'utf-8',
		);
		// $config = [];
		$CI->email->initialize($config);
		$CI->email->set_mailtype("html");
		$CI->email->set_newline("\r\n");
		//Email content
		$htmlContent = $CI->load->view('email/' . $template, $data, TRUE);
		$CI->email->to($to);
		$CI->email->from(config('from_email'), config('from_name'));
		$CI->email->subject($subject);
		$CI->email->message($htmlContent);
		//Send email
		return $CI->email->send();
	}
}
