<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



if (!function_exists('pay_stripe'))
{
    function pay_stripe($card_info)
    {
        //print_r($card_info);exit;
         $CI =& get_instance();
        // print_r($CI);exit;
         $CI->load->library('Stripe');

          $card_array['name'] = $card_info['Fullname'];
          $card_array['card_number'] = $card_info['credit_card_no'];

          $monthyear = explode('/',$card_info['expiry_date']);
          $card_array['month'] = $monthyear[0];
          $card_array['year'] = "20".$monthyear[1];
          $card_array['cvc_number'] = $card_info['security_code'];
          $card_array['address_zip'] = $card_info['address_zip'];

         // print_r($card_array);exit;

        $stripeCardObject=$CI->stripe->createToken($card_array);
        //print_r($stripeCardObject);exit;
        if(isset($stripeCardObject['status']) && $stripeCardObject['status'] == true)
        {            

            $stripe_pay['amount'] = $card_info['cost'];
            $stripe_pay['source'] = $stripeCardObject['token'];
            $booking_id = 0;
            if(isset($card_info['purchase_id']))
            {
              $booking_id = $card_info['purchase_id'];
            }
            $stripe_pay['description'] = $card_info['Description'];

            $paymentResponse = $CI->stripe->addCharge($stripe_pay);
            if(isset($paymentResponse['status']) && $paymentResponse['status'] == true)
            {
                
                //print_r($paymentResponse['success']->balance_transaction);
                //print_r($paymentResponse['success']->id);exit;
                $response['Status'] = $paymentResponse['status'];
                $response['ReferenceId'] = $paymentResponse['success']->id;
                $response['id'] = $paymentResponse['success']->id;
                return $response;
            }
            else
            {
                $response['Status'] = 0;
                $response['ReferenceId'] = 0;
                //$response['id'] = $paymentResponse['success']->id;
                return $response;
            }
        }
        else
        {
           
            //print_r($stripeCardObject['error']);
            $stripeCardObject['Status'] = $stripeCardObject['status'];
            return $stripeCardObject;
        }
        exit;
    } 


    function refund_stripe($id ='')
    {
      $CI =& get_instance();
         $CI->load->library('Stripe');
      $paymentResponse = $CI->stripe->addRefund($id);

      return $paymentResponse;
      //print_r($paymentResponse);exit;
    }

}

if (!function_exists('stripe_refund'))
{
    function stripe_refund($id ='')
    {        
        $CI =& get_instance();
        $CI->load->library('Stripe');
        $payment_mode = $CI->config->item('mode','stripe');
        if ($payment_mode == 'live') 
        {
            $sk = $CI->config->item('sk_live','stripe');
        } else
        {
            $sk = $CI->config->item('sk_test','stripe');
        }
        $stripe = new \Stripe\StripeClient($sk);                
        $transaction_data = $stripe->paymentIntents->retrieve(
          $id
        );
                    
        if (!empty($transaction_data)) {
            if (!empty($transaction_data->charges->data)) {                 
                $charge_id = $transaction_data->charges->data[0]->id;
                try {
                    $refund_data = $stripe->refunds->create([
                      'charge' => $charge_id,
                    ]);
                    $data['status'] = true;
                    $data['message'] = "Successfully";
                    $data['data'] = $transaction_data->charges->data[0]->id;
                } catch (Exception $e) {
                    $body = $e->getJsonBody();
                    $err  = $body['error'];                         
                    $data['status'] = false;
                    $data['message']=$err['message'];                    
                }                   

            } else
            {
                $data['status'] = false;
                $data['message'] = "Something went wrong";
            }
        } else
        {
            $data['status'] = false;
            $data['message'] = "Something went wrong.";
        }    
        return $data;
    }

}


if (!function_exists('createPaymentIntent'))
{
    function createPaymentIntent($amount = 0)
    {
        /*$CI =& get_instance();
        $CI->load->library('Stripe');
        $response = $CI->stripe->createPaymentIntent();
        return $response;*/


        $CI =& get_instance();
        $CI->load->library('Stripe');
        $payment_mode = $CI->config->item('mode','stripe');
        if ($payment_mode == 'live') 
        {
            $sk = $CI->config->item('sk_live','stripe');
        } else
        {
            $sk = $CI->config->item('sk_test','stripe');
        }
        $stripe = new \Stripe\StripeClient($sk);                
        $transaction_data = $stripe->paymentIntents->create(
          array(
                            "amount" => $amount * 100,
                            "currency" => 'usd',
                            "payment_method_types" => ['card']
                    )
        );
                      
        return $transaction_data;
    }

}

if (!function_exists('get_payment_card'))
{
    function get_payment_card($id ='')
    {        
        $CI =& get_instance();
        $CI->load->library('Stripe');
        
        $data = $CI->stripe->getPaymentCards($id);
        return $data;
    }

}
if (!function_exists('add_payment_card'))
{
    function add_payment_card($customer_id, $token)
    {        
        $CI =& get_instance();
        $CI->load->library('Stripe');
        
        $data = $CI->stripe->addPaymentCard($customer_id, $token);
        return $data;
    }

}
if (!function_exists('create_token'))
{
    function create_token($card)
    {        
        $CI =& get_instance();
        $CI->load->library('Stripe');
        
        $data = $CI->stripe->createToken($card);
        return $data;
    }

}

if (!function_exists('addCharge'))
{
    function addCharge($stripe_pay = array())
    {        
        $CI =& get_instance();
        $CI->load->library('Stripe');
        
        $data = $CI->stripe->addCharge($stripe_pay);
        return $data;
    }

}


