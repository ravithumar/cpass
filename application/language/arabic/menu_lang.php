<?php
/* Menu */
$lang['Dashboard'] = 'لوحة القيادة';
$lang['Users Management'] = 'إدارة المستخدمين;';
$lang['Pre-purchased hours'] = 'إساعات ما قبل الشراء;';
$lang['Truck Management'] = 'إدارة الشاحنات';
$lang['Truck Types'] = 'أنواع الشاحنات';
$lang['Load Types'] = 'أنواع التحميل';
$lang['Pricing Factors'] = 'عوامل التسعير';
$lang['Seasons'] = 'مواسم';
$lang['Payload Dimension'] = 'حمولة البعد';
$lang['City'] = 'مدينة';
$lang['Zone'] = 'منطقة';
$lang['Configuration'] = 'ترتيب';
$lang['System'] = 'النظام';
$lang['Application'] = 'الوضعية';
$lang['Shippers'] = 'الشاحنين';
$lang['Carrier'] = 'الناقل';
$lang['Approvers'] = 'الموافقون';
$lang['Supervisor'] = 'مشرف';
$lang['Control Agents']= 'وكلاء التحكم';
$lang['Customer Care']= 'رعاية العميل';
$lang['Accountant']= 'محاسب';
$lang['Managers']= 'مدراء';