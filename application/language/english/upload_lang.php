<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$lang['upload_userfile_not_set'] = "The POST variable with the name userfile could not be found.";
$lang['upload_file_exceeds_limit'] = "The size of the uploaded file is greater than the maximum size allowed in the PHP config file.";
$lang['upload_file_exceeds_form_limit'] = "The size of the uploaded file is greater than the maximum size allowed in the submission form.";
$lang['upload_file_partial'] = "Only part of the file has been uploaded.";
$lang['upload_no_temp_directory'] = "Temporary folder is missing.";
$lang['upload_unable_to_write_file'] = "The file could not be saved on the server.";
$lang['upload_stopped_by_extension'] = "File upload stopped due to its type.";
$lang['upload_no_file_selected'] = "You did not choose a file to upload.";
$lang['upload_invalid_filetype'] = "The type of file you are trying to upload is not allowed.";
$lang['upload_invalid_filesize'] = "The file you are trying to upload is larger than the allowed size.";
$lang['upload_invalid_dimensions'] = "The height or width of the image you are trying to upload is greater than the specified limit.";
$lang['upload_destination_error'] = "An error occurred while trying to move the uploaded file to the final position.";
$lang['upload_no_filepath'] = "The file upload path appears incorrect.";
$lang['upload_no_file_types'] = "You did not specify the types of files allowed to be uploaded.";
$lang['upload_bad_filename'] = "The name of the file you are uploading already exists.";
$lang['upload_not_writable'] = "It appears that the folder to upload files to cannot be written";
