<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class BookingSession extends Eloquent{
    protected $table = 'booking_session';
	public $timestamps = false; 
}
