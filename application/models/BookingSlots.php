<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class BookingSlots extends Eloquent{
    protected $table = 'booking_slots';
	public $timestamps = false; 
}
