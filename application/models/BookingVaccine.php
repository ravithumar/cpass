<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class BookingVaccine extends Eloquent{
    protected $table = 'booking_vaccines';
	public $timestamps = false; 
}
