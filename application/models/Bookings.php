<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Bookings extends Eloquent{
    protected $table = 'booking';
	public $timestamps = false; 
}
