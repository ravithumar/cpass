<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class BookingsMembers extends Eloquent{
    protected $table = 'booking_member';
	public $timestamps = false; 
}
