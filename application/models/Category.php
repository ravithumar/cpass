<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Category extends Eloquent{
    protected $table = 'category';
	public $timestamps = false;

    public function getDriverInfoAttribute() {
		$CI = &get_instance();
		$CI->load->model('SubCategory');
		return SubCategory::whereCategoryId($this->id)->get();
	}
}
