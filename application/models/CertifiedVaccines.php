<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class CertifiedVaccines extends Eloquent{
    protected $table = 'certified_vaccines';
	public $timestamps = false;

}
