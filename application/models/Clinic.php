<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Clinic extends Eloquent{
    protected $table = 'hospital';
	public $timestamps = false; 
}
