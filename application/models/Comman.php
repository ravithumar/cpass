<?php
class Comman extends CI_Model
{

	public function get_all_record($table, $select = '*')
	{
		return $this->db->select($select)->get($table)->result_array();
	}

	public function get_record_byid($table, $id, $select = '*')
	{
		return $this->db->select($select)->get_where($table, ['id' => $id])->row_array();
	}

	public function get_record_except($table, $key, $value, $select = '*')
	{
		return $this->db->select($select)->where_not_in($key, $value)->get($table)->result_array();
	}

	public function get_record_by_condition($table, $conditions, $select = '*')
	{
		return $this->db->select($select)->get_where($table, $conditions)->result_array();
	}

	public function insert_record($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_record($table, $data, $id)
	{
		return  $this->db->update($table, $data, array('id' => $id));
	}

	public function update_record_by_condition($table, $data, $conditions)
	{
		return  $this->db->update($table, $data, $conditions);
	}

	public function add_notification($doctor_id, $patients_id, $notification_title, $notification_description)
	{
		$notification = [];
		foreach ($patients_id as $key => $value) {
			$notification[$key] = [
				'sent_by' => $doctor_id,
				'sent_to' => $value,
				'title'   => $notification_title,
				'description' => $notification_description,
				'created_at' => current_date()
			];
		}
		$this->db->insert_batch('notification', $notification);
	}

	/**
	 *  grabs the user from db and checks if the key applies and if does then changes the coins and upgrades the users level
	 */
	public function addCoins($userId, $keyName, $userType)
	{
		$user = $this->db->query("SELECT * FROM users WHERE id = " . $userId)->row();
		$oldLevel = $user->level;
		$tableName = $user->level . '_coins';
		if ($oldLevel != "gold") {
			//go ahead only if the user has not already reached the max 
			$currentCoins = $user->coins;
			$toAddCoinQuery =	$this->get_record_by_condition($tableName, ['user_type' => $userType, 'status' => 1, 'key_name' => $keyName]);
			if ($toAddCoinQuery) {
				//if the key exists
				$toAddCoins = $toAddCoinQuery[0]['coin_value'];
				$newCoinsValue = $currentCoins + $toAddCoins;
				$goAhead = true;

				if ($toAddCoinQuery[0]['single_use'] == 1) {
					//if the key can not be used more then one time then

					$alreadyExisting = $this->db
						->from('add_coin_history')
						->where('user_id', $userId)
						->where('record_id', $toAddCoinQuery[0]['id'])
						->where('table_name', $tableName)
						->count_all_results();
					if ($alreadyExisting > 0) {
						$goAhead = false;
					}
				}

				if ($goAhead) {
					if ($newCoinsValue < 50) {
						$newLevel = 'basic';
					} else if ($newCoinsValue >= 50 && $newCoinsValue < 150) {
						$newLevel = 'silver';
					} else if ($newCoinsValue >= 150 && $newCoinsValue < 300) {
						$newLevel = 'bronze';
					} else if ($newCoinsValue >= 300) {
						$newLevel = 'gold';
					}

					$data['coins'] = $newCoinsValue;
					$data['level'] = $newLevel;

					if ($oldLevel != $newLevel) {
						$data['current_shares'] = 0;

						$title = SITENAME;
						$request_type_user = 'user_level_upgraded';
						$description = 'Your level has been upgraded to '.$newLevel;
						$params = ['type' => $request_type_user, 'user_id' => $userId, 'level' => $newLevel];
						send_push($userId, 'users', $title, $description, $request_type_user, $params);
						// $this->add_notification($request['doctor_id'],[$value['user_id']],$title,$description);
					}
					$this->update_record('users', $data, $userId);

						$title = SITENAME;
						$request_type_user = 'coins_added';
						$description = $toAddCoins.' - Coins are added in your account for '.$toAddCoinQuery[0]['name'];
						$params = ['type' => $request_type_user, 'user_id' => $userId, 'coins' => $toAddCoins];
						send_push($userId, 'users', $title, $description, $request_type_user, $params);
					//update the user coin and level
					$this->insert_record('add_coin_history', ['user_id' => $userId, 'record_id' => $toAddCoinQuery[0]['id'], 'table_name' => $tableName, 'previous_coins' => $currentCoins, 'added_coins' => $toAddCoins, 'created_at' =>  date('Y-m-d H:i:s')]);
					//insert the history for coin addition

					if ($oldLevel != $newLevel) {
						//if we want to notify the user about the level upgraded do something here
					}
				}
			}
		}
	}

	public function delete_recordbyid($table, $id)
	{
		return $this->db->where('id', $id)->delete($table);
	}
}
