<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Country extends Eloquent{
    protected $table = 'country';
	public $timestamps = false;

}
