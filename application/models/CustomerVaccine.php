<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class CustomerVaccine extends Eloquent{
    protected $table = 'customer_vaccine';
	public $timestamps = false; 
}
