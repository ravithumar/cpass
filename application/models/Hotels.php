<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Hotels extends Eloquent{
    protected $table = 'hotel';
	public $timestamps = false; 
}
