<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Lab extends Eloquent{
    protected $table = 'lab';
	public $timestamps = false; 
}
