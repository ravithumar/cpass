<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Report extends Eloquent{
    protected $table = 'reports';
	public $timestamps = false;
    protected $appends = ['report_type'];

    public function getDriverInfoAttribute() {
		$CI = &get_instance();
		$CI->load->model('ReportType');
		return ReportType::whereCategoryId($this->id)->get();
	}
}