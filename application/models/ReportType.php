<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class ReportType extends Eloquent{
    protected $table = 'report_type';
	public $timestamps = false; 
}
