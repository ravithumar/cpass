<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Slot extends Eloquent{
    protected $table = 'slot';
	public $timestamps = false; 
}
