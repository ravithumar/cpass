<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class SubCategory extends Eloquent{
    protected $table = 'sub_category';
	public $timestamps = false;
    
}
