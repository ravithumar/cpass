<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model as Eloquent;
class User extends Eloquent {
	use SoftDeletes;
	protected $table = 'users';
	public $timestamps  = false;
	protected $hidden = ['password', 'created_at', 'updated_at', 'deleted_at',
		'activation_selector', 'activation_code', 'forgotten_password_selector', 'forgotten_password_code', 'forgotten_password_time', 'remember_selector', 'remember_code', 'created_on', 'last_login', 'username', 'ip_address', 'city_id'];
	public function getCompanyNameAttribute($company_name) {
		return $company_name == null ? '' : $company_name;
	}
	public function getCompanyPhoneAttribute($company_phone) {
		return $company_phone == null ? '' : $company_phone;
	}
	public function getCompanyAddressAttribute($company_address) {
		return $company_address == null ? '' : $company_address;
	}
	public function getCompanyCertificateAttribute($company_certificate) {
		return $company_certificate == null ? '' : 'users/'.$company_certificate;
	}
	public function getCompanyRegisterAttribute($company_register) {
		return $company_register == null ? '' : $company_register;
	}
	public function getCatNumberAttribute($vat_number) {
		return $vat_number == null ? '' : $vat_number;
	}
	public function getImageAttribute($image) {
		return $image == null ? '' : $image;
	}
	public function role()
	{
		$CI =& get_instance();
		return $CI->db->get_where('users_groups',['user_id'=>$this->id])->row();
	}
	public function city(){
		$CI =& get_instance();
		$CI->load->model('City');
		return $this->hasOne('City', 'id','city_id');
	}
	
	public static function registerResponse() {
		return [
			"id" => $this->id,
			"email" => $this->email,
			"active" => $this->active,
			"first_name" => $this->first_name,
			"last_name" => $this->last_name,
			"phone" => $this->phone,
			"alt_phone" => $this->alt_phone,
			"gender" => $this->gender,
			"date_of_birth" => $this->date_of_birth,
			"type" => $this->type,
			"referral" => $this->referral,
			"code" => $this->code,
			"company_name" => $this->company_name,
			"company_phone" => $this->company_phone,
			"company_address" => $this->company_address,
			"company_certificate" => $this->company_certificate,
			"company_register" => $this->company_register,
			"image" => $this->image,
		];
	}

	public function scopeIsWithinMaxDistance($query, $coordinates) {

	    $haversine = "(111.13384 * acos(cos(radians(" . $coordinates['latitude'] . ")) 
	                    * cos(radians(`latitude`)) 
	                    * cos(radians(`longitude`) 
	                    - radians(" . $coordinates['longitude'] . ")) 
	                    + sin(radians(" . $coordinates['latitude'] . ")) 
	                    * sin(radians(`latitude`))))";

	    return $query->select('*')->selectRaw("{$haversine} AS distance");
	}
}
