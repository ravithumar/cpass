<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Zone extends Eloquent{
    protected $table = 'zones';
	public $timestamps = false;

}
