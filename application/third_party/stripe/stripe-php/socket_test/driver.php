
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script type="text/javascript">


	var node_url = 'https://www.peppea.com:8080';
	var socket = io(node_url);
	window.onload = function() {

		socket.on('update_driver_location', function(data){
			console.log('update_driver_location',data);
		
		});

		socket.on('forward_booking_request', function(data){
			console.log('forward_booking_request',data);
			document.getElementById('booking_id').value = data.booking_info.id;
			document.getElementById('message').innerHTML = data.message;
		});

		socket.on('ask_for_tips', function(data){
			console.log('ask_for_tips',data);
			document.getElementById('message').innerHTML = data.message;
		});
		socket.on('receive_tips', function(data){
			console.log('receive_tips',data);
			document.getElementById('message').innerHTML = data.message;
		});

		socket.on('accept_booking_request', function(data){
			console.log('accept_booking_request',data);
		});

		socket.on('start_trip', function(data){
			console.log('start_trip',data);
			document.getElementById('message').innerHTML = data.message;
		});

		

		socket.emit('start_duty',{driver_id:<?php echo (Int)$_GET['id']; ?>,location:[72.5162488,23.0721174],vehicle_type_id:["1"],busy:<?php echo (Int)$_GET['busy']; ?>});
		socket.emit('update_driver_location',{driver_id:<?php echo (Int)$_GET['id']; ?>,lng:72.5162488,lat:23.0721175});
		//socket.emit('connect_customer',{customer_id:22,lng:72.5152488,lat:23.0701174});

		socket.emit('get_estimate_fare',{customer_id:1,pickup_lng:72.5291184,pickup_lat:23.0636726,dropoff_lat:23.0305179,dropoff_lng:72.5053514}); 

		
		setInterval(function(){ 
			socket.emit('track_trip',{customer_id:1,lng:72.5162488,lat:23.0721174});
		}, 1000);
	}

	function cancel_request()
	{
		var driver_id = <?php echo (Int)$_GET['id']; ?>;
		var booking_id = document.getElementById('booking_id').value;
		socket.emit('forward_booking_request_to_another_driver',{customer_id:driver_id,booking_id:booking_id});

	}

	function accept_request()
	{
		var driver_id = <?php echo (Int)$_GET['id']; ?>;
		var booking_id = document.getElementById('booking_id').value;
		//document.getElementById('message').innerHTML = data.message;
		socket.emit('accept_booking_request',{driver_id:driver_id,booking_id:booking_id,booking_type :'book_now'});

	}

	function on_the_way()
	{
		var driver_id = <?php echo (Int)$_GET['id']; ?>;
		var booking_id = document.getElementById('booking_id').value;
		//document.getElementById('message').innerHTML = data.message;
		//alert(driver_id);alert(booking_id);
		socket.emit('on_the_way_booking_request',{driver_id:31,booking_id:"494"});

	}

	function ask_tips()
	{		
		var booking_id = document.getElementById('booking_id').value;
		socket.emit('ask_for_tips',{booking_id:booking_id});
	}

	function start_trip()
	{
		var booking_id = document.getElementById('booking_id').value;
		socket.emit('start_trip',{booking_id:booking_id});
	}

</script>

<input type="hidden" name="booking_id" id="booking_id" value="">
<input type="button" name="cancel" onclick="cancel_request();" value="cancel">
<input type="button" name="accept" onclick="accept_request();" value="accept">
<input type="button" name="start" onclick="start_trip();" value="start trip">
<input type="button" name="ask_tips" onclick="ask_tips();" value="ask tips">
<input type="button" name="on_the_way" onclick="on_the_way();" value="on the way">
<br />

<center><div style="color: red;font: 24px;" id="message"></div></center>

