<div class="left-side-menu">
    <div class="slimscroll-menu">
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="<?php echo base_url('admin'); ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/network'); ?>" class="waves-effect"><i class="fa fa-google"></i>  <span>Company</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/clinic'); ?>" class="waves-effect"><i class="fas fa-plus-square"></i>  <span>Providers</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/lab'); ?>" class="waves-effect"><i class="fas fa-plus-square"></i>  <span>Lab</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/booking'); ?>" class="waves-effect"><i class="fas fa-book"></i>  <span>Bookings</span></a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/block-slot'); ?>" class="waves-effect"><i class="fas fa-book"></i>  <span>Block Slot</span></a>
                </li>

                <li>
                    <a href="<?php echo base_url('admin/user'); ?>" class="waves-effect"><i class="fas fa-user"></i>  <span>Users</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/category'); ?>" class="waves-effect"><i class="fas fa-tags"></i>  <span>Exemption Category</span></a>
                </li>
				<li>
                    <a href="<?php echo base_url('admin/certified-vaccines'); ?>" class="waves-effect"><i class="la la-globe"></i> <span> Certified Vaccines </span></a>
                </li>
				<li>
                    <a href="<?php echo base_url('admin/zone'); ?>" class="waves-effect"><i class="la la-globe"></i> <span> Zone </span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('admin/country'); ?>" class="waves-effect"><i class="la la-globe"></i> <span> Country </span></a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-tasks"></i> <span>Report Management</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?php echo base_url('admin/report-type'); ?>" class="waves-effect">- <span>Report Type</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/report'); ?>" class="waves-effect">- <span>Reports</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span> <?php echo __('Configuration') ?> </span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?php echo base_url('admin/system/setting'); ?>" class="waves-effect"><span> <?php echo __('System') ?> </span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/app/setting'); ?>" class="waves-effect"><span> <?php echo __('Application') ?> </span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    
</div>
<div class="content-page">
