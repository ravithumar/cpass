<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <li class="breadcrumb-item"><?= $clinic->company_name; ?></li>
                     <li class="breadcrumb-item active">Slot Details</li>
                  </ol>
               </div>
               <h4 class="page-title">Block Slot Details</h4>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-lg-4 col-xl-4">
         <div class="card-box text-center">

            <h4 class="mb-0"><?= $clinic->company_name; ?></h4>

            <div class="text-left mt-3">
               <h4 class="font-13 text-uppercase">About Me :</h4>
               <p class="text-muted mb-2 font-13"><strong>Company Website :</strong> <span class="ml-2"><?= $clinic->company_website; ?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2"><?= $clinic->point_of_contact; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Location :</strong> <span class="ml-2"><?= $clinic->address; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Postal Code :</strong> <span class="ml-2"><?= $clinic->postal_code; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Number of employees :</strong> <span class="ml-2"><?= $clinic->no_of_employee; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Daily Basis Cases :</strong> <span class="ml-2"><?= $clinic->daily_basis_case; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>County :</strong> <span class="ml-2"><?= $clinic->county; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Number Of Sites :</strong> <span class="ml-2"> <?= $clinic->no_of_sites; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Currently offer COVID testing services?</strong> <span class="ml-2"><?= $clinic->covid_testing; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Performed Fit For Travel covid tests before? </strong> <span class="ml-2"><?= $clinic->travel_covid_test; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Start Offering Service Date :</strong> <span class="ml-2"> <?= date("d-m-Y",strtotime($clinic->start_offering_service)); ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Advertising that now offer COVID travel tests? :</strong> <span class="ml-2"><?= $clinic->advertising; ?></span></p>
               
            </div>
         </div> 
      </div> 
      <div class="col-lg-8 col-xl-8">
         <div class="card-box">
            <div class="tab-content">
               <div class="tab-pane active" id="aboutme">
                  <h5 class="mb-3 mt-0 text-uppercase text-center"><i class="mdi mdi-cards-variant mr-1"></i>
                     Block Slots</h5>
                  <div class="table-responsive">
                     <table class="table table-borderless mb-0">
                        <thead class="thead-light">
                           <tr>
                              <th>#</th>
                              <th>Start Date</th>
                              <th>End Date</th>
                              <th>Start Time</th>
                              <th>End Time</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php if (isset($block_slot)) {
                              foreach ($block_slot as $key => $value) {?>
                                 <tr>
                                    <td><?php echo $value['clinic_id']; ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($value['start_date'])); ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($value['end_date'])); ?></td>
                                    <td><?php echo date('h:i A', strtotime($value['start_time'])); ?></td>
                                    <td><?php echo date('h:i A', strtotime($value['end_time'])); ?></td>
                                    <td><span class="badge badge-danger">Inactive</span></td>
                                 </tr>
                              <?php }
                           } ?>

                        </tbody>
                     </table>
                  </div>
               </div> 
            </div> <!-- end tab-content -->
         </div> <!-- end card-box-->
      </div> <!-- end col -->
   </div>
   <!-- end row-->
</div>