<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title --> 
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="category_add" enctype="multipart/form-data" action="<?php echo base_url('admin/category/add'); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 ">
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Name'); ?></label>
                              <input type="text" class="form-control " name="name" data-parsley-required-message="Please Enter Name" required="" placeholder="Please Enter Name">
                           </div>
                           <!-- <div class="form-group">
                              <label class="form-control-label"><?php echo __('Image'); ?></label>
                              <div class="custom-file">
                                 <input type="file" name="image" required class="custom-file-input parsley-error category_image" id="category_image" data-parsley-required-message="Please Select Image" data-parsley-errors-container="#category_image_error" accept="image/*" id="form_image" data-parsley-fileextension="jpg||png||jpeg||JPG||PNG||JPEG"  >
                                 <label class="custom-file-label" >Choose file</label>
                              </div>
                              <div id="category_image_error"></div>
                              <div class="" id="category_image_preview" style="margin-top: 5px;"></div>
                              <div class="col-12 mt-2">
                                 <span class="category_image_error" id="category_image_error"></span>
                              </div>
                           </div> -->
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#category_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
