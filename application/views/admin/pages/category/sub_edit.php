<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title --> 
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="category_add" enctype="multipart/form-data" action="<?php echo base_url('admin/category/sub/edit/'.$id); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 ">
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Zone'); ?></label>
                              <?php $zone = explode(',',$sub_category['zone']); ?>
                              <select class="form-control select2_dropdown" name="zone[]" data-parsley-required-message="Please Select Zone" required="" data-placeholder="Please Select Zone" multiple="">
                                 <option value="" disabled="">Select Zone</option>
                                 <?php
											if(!empty($zones)){
												foreach($zones as $zone_row){ ?>
                                 		<option value="<?=$zone_row['id']?>" <?=in_array($zone_row['id'],$zone)?"selected":""?>><?=$zone_row['name']?></option><?php 
												} 
											} ?>
                              </select>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Vaccination Status'); ?></label>
                              <?php $vaccination_status = explode(',',$sub_category['vaccination_status']); ?>
                              <select class="form-control select2_dropdown" name="vaccination_status[]" data-parsley-required-message="Please Select Vaccination Status" required="" data-placeholder="Please Select Vaccination Status" multiple="">
                                 <option value="" disabled="">Select Vaccination Status</option>
                                 <?php
											if(!empty($vaccine_status)){
												foreach($vaccine_status as $vaccine_status_row){ ?>
													<option value="<?=$vaccine_status_row['id']?>" <?=in_array($vaccine_status_row['id'],$vaccination_status)?"selected":""?>><?=$vaccine_status_row['name']?></option><?php
												}
											} ?>
                              </select>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Description'); ?></label>
                              <textarea type="text" class="form-control " name="description" data-parsley-required-message="Please Enter Description" required="" placeholder="Please Enter Description"><?php echo $sub_category['description']; ?></textarea>
                           </div>
                         
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Days'); ?></label>
                                 <?php
                                 if ($sub_category['days'] != NULL) {
                                    $days = explode(",",$sub_category['days']);
                                    foreach ($days as $key => $value) { 
                                       if ($key== 0) { ?>
                                          <div class="row records d-flex justify-content-betweeen">
                                             <div class="col-lg-10"> 
                                                <input name="days[]" type="number" placeholder="PLease Enter Total Days" value="<?= $value; ?>" class="form-control mr-2 dayscount" required="" data-parsley-required-message="Please Enter Total Days">
                                             </div>
                                             <div class="col-lg-2 extra-fields"> 
                                                <a class="btn btn-success ml-2" href="javascript:void(0);">+</a>
                                             </div>
                                          </div>
                                          <div class="records_dynamic">
                                       <?php }else{ ?>
                                       <div class="remove row d-flex justify-content-between my-2 ">
                                          <div class="col-lg-10"> 
                                             <input name="days[]" type="number" value="<?= $value; ?>" placeholder="PLease Enter Total Days" class="form-control mr-2 dayscount" >
                                          </div> 
                                          <div class="col-lg-2 remove-field">
                                             <a href="javascript:void(0);" class="remove-field btn btn-danger ml-2 btn-remove-days">-</a>
                                          </div>
                                       </div>
                                    <?php } }
                                 }else{ ?>
                                    <div class="row records d-flex justify-content-betweeen">
                                       <div class="col-lg-10"> 
                                          <input name="days[]" type="number" placeholder="PLease Enter Total Days" value="<?= $value; ?>" class="form-control mr-2 dayscount" required="" data-parsley-required-message="Please Enter Total Days">
                                       </div>
                                       <div class="col-lg-2 extra-fields"> 
                                          <a class="btn btn-success ml-2" href="javascript:void(0);">+</a>
                                       </div>
                                    </div>
                                    <div class="records_dynamic">

                                 <?php }
                                 ?>
                              </div>
                           </div> 
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Report Type'); ?></label>
                              <?php $days_report = explode(',',$sub_category['report_id']); ?>
                              <select class="form-control select2_dropdown" name="report_id[]" data-parsley-required-message="Please Select Report Type" required="" data-placeholder="Please Select Report Type" multiple="">
                                 <option value="" disabled="">Select Report Type</option>
                                 <?php foreach($reports as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>" <?php echo (in_array($value['id'], $days_report) ? 'selected':''); ?>><?php echo ucfirst($value['name']); ?></option>
                                 <?php } ?>
                              </select>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#category_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
