<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form id="clinic_add" enctype="multipart/form-data" action="<?php echo base_url('admin/clinic/add'); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="row">
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Name'); ?> <span class="text-danger">*</span> </label>
                              <input type="text" class="form-control " name="company_name" placeholder="Please Enter Company Name" data-parsley-required-message="Please Enter Company Name" required="">
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_name'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Number'); ?></label>
                              <input type="text" class="form-control " name="company_number" placeholder="Please Enter Company Number" >
                              
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('VAT Number'); ?></label>
                              <input type="text" class="form-control " name="vat_number" placeholder="Please Enter VAT Number">
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Website'); ?> </label>
                              <input type="url" class="form-control " name="company_website" data-parsley-type="url" data-parsley-type-message="Please Enter Vaild Company Website" data-parsley-required-message="Please Enter Company Website" placeholder="Please Enter Company Website">
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_website'); ?>
                              </div>
                           </div>
                           <!-- <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Username'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="username" required=""  data-parsley-required-message="Please Enter Company Username" placeholder="Please Enter Company Username">
                              <div class="validation-error-label" >
                                 <?php echo form_error('username'); ?>
                              </div>
                           </div> -->
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Email Address'); ?> <span class="text-danger">*</span> </label>
                              <input type="email" class="form-control " name="email" data-parsley-type="email" required data-parsley-type-message="Please Enter Vaild Company Email Address" data-parsley-required-message="Please Enter Company Email" placeholder="Please Enter Company Email Address" value="<?= set_value('email'); ?>">
                              <div class="validation-error-label" >
                                 <?php echo form_error('email'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label for="product_name">Number of Employees</label>
                              <select class="form-control select2_dropdown " id="no_of_employee" name="no_of_employee" data-parsley-errors-container="#num_of_emp_error"  data-parsley-required-message="Please Select Number of Employees" >
                                 <option value="">Please select</option>
                                 <option value="1" data-index="0">1</option>
                                 <option value="2 to 5" >2 to 5</option>
                                 <option value="6 to 10" >6 to 10</option>
                                 <option value="11 to 25" >11 to 25</option>
                                 <option value="26 to 50" >26 to 50</option>
                                 <option value="51 to 200" >51 to 200</option>
                                 <option value="201 to 1000" >201 to 1000</option>
                                 <option value="1001 to 10000" >1001 to 10000</option>
                                 <option value="10001 or more" >10001 or more</option>
                              </select>
                              <div id="num_of_emp_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_employee'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Direct phone number for point of contact'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="point_of_contact" data-parsley-required-message="Please Enter Contact Number" required="" placeholder="Please Enter Contact Number">
                              <div class="validation-error-label" >
                                 <?php echo form_error('point_of_contact'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many tests is your company able to perform on a daily basis?'); ?></label>
                              <input type="number" class="form-control " name="daily_basis_case" data-parsley-type="number" data-parsley-required-message="Please Enter details" data-parsley-type-message="Please Enter only digits">
                              <div class="validation-error-label" >
                                 <?php echo form_error('daily_basis_case'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Address where tests will be performed'); ?> <span class="text-danger">*</span> </label>
                              <input type="text" class="form-control" name="address" autocomplete="off" id="address" rows="2" placeholder="Please Enter Address" required data-parsley-required-message="Please Enter Address" value="<?= set_value('address'); ?>">
                              <input type="hidden" class="form-control" name="latitude" id="latitude"  value="<?= set_value('latitude'); ?>" />
                              <input type="hidden" class="form-control" name="longitude" id="longitude" value="<?= set_value('longitude'); ?>" />
                              <div class="validation-error-label" >
                                 <?php echo form_error('address'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label for="product_name"><?php echo __('County'); ?> <span class="text-danger">*</span> </label>
                              <select class="form-control select2_dropdown " id="county" name="county" data-parsley-required-message="Please Select County" required>
                                 <?php if (!empty($all_county)) {
                                    foreach ($all_county as $key => $val) { ?>
                                       <option value="<?= $val->id; ?>" <?php echo ($val->id == set_value('county')) ? 'selected' : ''; ?>><?= $val->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                              <div class="validation-error-label" >
                                 <?php echo form_error('county'); ?>
                              </div>
                           </div> 
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Postal Code'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="postal_code" placeholder="Please Enter Postal Code" data-parsley-required-message="Please Enter postal code" required="" >
                              <div class="validation-error-label" >
                                 <?php echo form_error('postal_code'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many sites do you have?'); ?></label>
                              <select class="form-control select2_dropdown " id="no_of_sites" name="no_of_sites" data-parsley-errors-container="#num_of_sites_error"  data-parsley-required-message="Please Select Number of Sites" >
                                 <option value="">Please select</option>
                                 <option value="1" >1</option>
                                 <option value="2" >2</option>
                                 <option value="3" >3</option>
                                 <option value="4" >4</option>
                                 <option value="5" >5</option>
                                 <option value="6-10" >6-10</option>
                                 <option value="11-20" >11-20</option>
                                 <option value="21-50" >21-50</option>
                                 <option value="51-100" >51-100</option>
                                 <option value="100+" >100+</option>
                              </select>
                              <div id="num_of_sites_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_sites'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Do you currently offer COVID testing services?'); ?></label>
                              <select class="form-control select2_dropdown " id="covid_testing" name="covid_testing" data-parsley-errors-container="#covid_testing_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" >Yes</option>
                                 <option value="No" >No</option>
                              </select>
                              <div id="covid_testing_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('covid_testing'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Have you performed Fit For Travel covid tests before?'); ?></label>
                              <select class="form-control select2_dropdown " id="travel_covid_test" name="travel_covid_test" data-parsley-errors-container="#travel_covid_test_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" >Yes</option>
                                 <option value="No" >No</option>
                              </select>
                              <div id="travel_covid_test_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('travel_covid_test'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('When would you like to start offering our service?'); ?></label>
                              <input type="date" class="form-control datepicker" name="start_offering_service" data-parsley-required-message="Please Select Date" placeholder="Please Select Date">
                              <div class="validation-error-label" >
                                 <?php echo form_error('start_offering_service'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Will you be advertising that you can now offer COVID travel tests?'); ?></label>
                              <textarea class="form-control" name="advertising" rows="2" placeholder="Please Enter Details"  data-parsley-required-message="Please Enter Details"></textarea> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('advertising'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Clinic Profile') ?></label>
                              <input class="form-control pt-1" type="file" name="profile_picture">
                           </div>

                           <div class="col-md-12">
                              <div class="row">
                                 <div class="form-group col-6">
                                    <label for="product_name"><?php echo __('Report Types'); ?> <span class="text-danger">*</span> </label>
                                    <select class="form-control select2_dropdown " id="report_id" name="report_type_id[]" required data-parsley-errors-container="#reports_error" data-parsley-required-message="Please Select Reports">
                                       <?php if (!empty($reports)) {
                                          foreach ($reports as $key => $val) { ?>
                                             <option value="<?= $val->id; ?>"><?= $val->name; ?></option>
                                          <?php }
                                       } ?> 
                                    </select>
                                    <div class="validation-error-label" >
                                       <?php echo form_error('report_type_id'); ?>
                                    </div>
                                 </div>
                                 <div class="form-group col-6">
                                    <label for="product_name"><?php echo __('Purpose Types'); ?> <span class="text-danger">*</span> </label>
                                    <select class="form-control select2_dropdown " id="purpose_type" name="purpose_type[]" required data-parsley-errors-container="#reports_error" data-parsley-required-message="Please Select Purpose Types">
                                       <?php if (!empty($purpose_type)) {
                                          foreach ($purpose_type as $purpose_type_row) { 
                                             if(!empty($purpose_type_row['subcat'])){ 
                                                foreach($purpose_type_row['subcat'] as $sub_row){ ?>
                                                   <option value="<?=$sub_row['id']?>"><?= $sub_row['name']; ?></option>
                                                <?php } ?>
                                             <?php } else{ ?>
                                                <option value="<?=$purpose_type_row['id']?>"><?= $purpose_type_row['name']; ?></option><?php
                                             }
                                           }
                                       } ?> 
                                    </select>
                                    <div class="validation-error-label" >
                                       <?php echo form_error('purpose_type'); ?>
                                    </div>
                                 </div>
                                 <div class="form-group col-6">
                                    <label class="form-control-label"><?php echo __('Price'); ?> <span class="text-danger">*</span> </label>
                                    <input type="text" class="form-control " name="price[]" placeholder="Please Enter Price" required data-parsley-required-message="Please Enter Price">
                                    <div class="validation-error-label" >
                                       <?php echo form_error('price'); ?>
                                    </div>
                                 </div>
                                 <div class="form-group col-6"> 
                                    <a class="btn btn-success ml-2 mt-3 add_more_reports_price" href="javascript:void(0);">+</a>
                                 </div>
                              </div>
                              <div class="paste_report_price"></div>
                           </div>

                           <div class="col-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#clinic_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="copy_report_price" style="display: none;">
   <div class="row">
      <div class="form-group col-6">
         <label for="product_name"><?php echo __('Report Types'); ?><span class="text-danger">*</span></label>
         <select class="form-control select2_dynamic_dropdown " id="report_id" name="report_type_id[]" required data-parsley-errors-container="#reports_error"  data-parsley-required-message="Please Select Reports" >
            <?php if (!empty($reports)) {
               foreach ($reports as $key => $val) { ?>
                  <option value="<?= $val->id; ?>" ><?= $val->name; ?></option>
               <?php }
            } ?> 
         </select>
      </div>
      <div class="form-group col-6">
         <label for="product_name"><?php echo __('Purpose Types'); ?><span class="text-danger">*</span></label>
         <select class="form-control select2_dynamic_dropdown " id="purpose_type" name="purpose_type[]" required data-parsley-errors-container="#reports_error"  data-parsley-required-message="Please Select Purpose Types" >
            <?php if (!empty($purpose_type)) {
               foreach ($purpose_type as $purpose_type_row) { 
                  if(!empty($purpose_type_row['subcat'])){ 
                     foreach($purpose_type_row['subcat'] as $sub_row){ ?>
                        <option value="<?=$sub_row['id']?>"><?= $sub_row['name']; ?></option>
                     <?php } ?>
                  <?php } else{ ?>
                     <option value="<?=$purpose_type_row['id']?>"><?= $purpose_type_row['name']; ?></option><?php
                  }
                }
            } ?> 
         </select>
      </div>
      <div class="form-group col-6">
         <label class="form-control-label"><?php echo __('Price'); ?> <span class="text-danger">*</span> </label>
         <input type="text" class="form-control " name="price[]" placeholder="Please Enter Price" data-parsley-required-message="Please Enter Price" required="">
      </div>
      <div class="form-group col-6"> 
         <a class="btn btn-danger ml-2 mt-3 remove_more_reports_price" href="javascript:void(0);">-</a>
      </div>
   </div>
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key');?>&libraries=places"></script>

<script type="text/javascript">
   
   google.maps.event.addDomListener(window, 'load', function () {
     var pickup_places = new google.maps.places.Autocomplete(document.getElementById('address'));
     google.maps.event.addListener(pickup_places, 'place_changed', function () {
         var pickup_place = pickup_places.getPlace();
         var address = pickup_place.address_components;
         $('#latitude').val(pickup_place.geometry.location.lat());
         $('#longitude').val(pickup_place.geometry.location.lng());
      });
   });


   $(document).on("click",".add_more_reports_price",function(){
      var clone = $(".copy_report_price").children().clone();
      clone.find(".select2_dynamic_dropdown").select2({
         "width":"100%"
      });
      $(".paste_report_price").append(clone);
   });

   $(document).on("click",".remove_more_reports_price",function(){
      $(this).closest(".row").remove();
   });
   
</script>