<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="country_add" name="add_category_form" enctype="multipart/form-data" action="<?php echo base_url('admin/country/add'); ?>" method="post">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
						   		<label for="name">Country Name</label>
                                <input type="text" class="form-control" required="" name="name" id="name" placeholder="Enter country name" data-parsley-maxlength="50"> 
                           </div>
                           <div class="form-group">
						   			<label for="name">Country Zone</label>
										<select name="zone" id="zone" class="form-control" required>
											<?php if (!empty($zones)) {
                                    foreach ($zones as $zone_row) { ?>
                                       <option value="<?= $zone_row->id; ?>" ><?= $zone_row->name; ?></option>
                                    <?php }
                                 } ?> 
										</select>
                           </div>
									<div class="form-group">
                              <label for="product_name">Certified Vaccines</label>
                              <select class="form-control select2_dropdown " id="certified_vaccine_id" multiple="" name="certified_vaccine_id[]" data-parsley-errors-container="#reports_error"  data-parsley-required-message="Please Select Certified Vaccines" >
                                 <?php if (!empty($certified_vaccines)) {
                                    foreach ($certified_vaccines as $key => $val) { ?>
                                       <option value="<?= $val->id; ?>" ><?= $val->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                              <div id="reports_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_employee'); ?>
                              </div>
                           </div>

                           <div class="form-group">
                              <label for="product_name">Reports</label>
                              <select class="form-control select2_dropdown " id="reports_id" multiple="" name="reports_id[]" data-parsley-errors-container="#reports_error"  data-parsley-required-message="Please Select Reports" >
                                 <?php if (!empty($reports)) {
                                    foreach ($reports as $key => $val) { ?>
                                       <option value="<?= $val->id; ?>" ><?= $val->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                              <div id="reports_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('reports_id'); ?>
                              </div>
                           </div>

                           <div class="form-group">
                           <label for="name">One Way Days</label>
                                <input type="number" class="form-control" required="" name="one_way_days" id="one_way_days" placeholder="Enter One Way Days" min="1"> 
                           </div>
                           <div class="form-group">
                           <label for="name">International Days</label>
                                <input type="number" class="form-control" required="" name="international_days" id="international_days" placeholder="Enter International Days" min="1"> 
                           </div>
                           <div class="form-group">
                              <label class="country_img">Country Image</label>
                              <input class="form-control pt-1" type="file" name="country_img">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#country_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
