<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="country_add" enctype="multipart/form-data" action="<?php echo base_url('admin/country/edit/'.$country['id']); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
						            <label for="name">Country Name</label>
                              <input type="text" class="form-control" name="name" id="name" required placeholder="Enter country name" value='<?php echo $country['name']; ?>' data-parsley-maxlength="50">
                           </div>
                           <div class="form-group">
						   			<label for="name">Country Zone</label>
									   <?php
                                 $field_value = NULL;
                                 $temp_value = set_value('zone');
                                 if (isset($temp_value) && !empty($temp_value)) {
                                    $field_value = $temp_value;
                                 } else{
                                    $field_value = stripslashes($country['zone']);
                                 }
                              ?>
                              <select name="zone" id="zone" class="form-control">
      									<?php if (!empty($zones)) {
                                    foreach ($zones as $zone_row) { ?>
                                       <option value="<?= $zone_row->id; ?>" <?php echo $field_value == $zone_row->id ? 'selected' : ''; ?>><?= $zone_row->name; ?></option>
                                    <?php }
                                 } ?> 
   									</select>
                           </div>
									<?php
									$certified_vaccine_id_array = array();
									if(!empty($country['certified_vaccine_id'])){
										$certified_vaccine_id_array = explode(',',$country['certified_vaccine_id']);
									}
									?>
									<div class="form-group">
                              <label for="product_name">Certified Vaccines</label>
                              <select class="form-control select2_dropdown " id="certified_vaccine_id" multiple="" name="certified_vaccine_id[]" data-parsley-required-message="Please Select Certified Vaccines" >
                              <!-- <select class="form-control select2_dropdown " id="certified_vaccine_id" multiple="" name="certified_vaccine_id[]" required data-parsley-errors-container="#reports_error"  data-parsley-required-message="Please Select Certified Vaccines" > -->
                                 <?php if (!empty($certified_vaccines)) {
                                    foreach ($certified_vaccines as $key => $val) { ?>
                                       <option value="<?= $val->id; ?>" <?=in_array($val->id,$certified_vaccine_id_array)?"selected":""?>><?= $val->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                              <div id="reports_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_employee'); ?>
                              </div>
                           </div>

                           <?php
                           $reports_id_array = array();
                           if(!empty($country['reports'])){
                              $reports_id_array = explode(',',$country['reports']);
                           }
                           ?>
                           <div class="form-group">
                              <label for="product_name">Reports</label>
                              <select class="form-control select2_dropdown " id="reports_id" multiple="" name="reports_id[]" data-parsley-required-message="Please Select Reports">
                                 <?php if (!empty($reports)) {
                                    foreach ($reports as $key => $val) { ?>
                                       <option value="<?= $val->id; ?>" <?=in_array($val->id,$reports_id_array)?"selected":""?>><?= $val->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                              <div id="reports_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('reports_id'); ?>
                              </div>
                           </div>

                           <div class="form-group">
                           <label for="name">One Way Days</label>
                                <input type="number" class="form-control" required="" name="one_way_days" id="one_way_days" placeholder="Enter One Way Days" min="1" value='<?php echo $country['oneway_days']; ?>'> 
                           </div>
                           <div class="form-group">
                           <label for="name">International Days</label>
                                <input type="number" class="form-control" required="" name="international_days" id="international_days" placeholder="Enter International Days" min="1" value='<?php echo $country['days']; ?>'> 
                           </div>
                           <div class="form-group">
                              <label class="country_img">Country Image</label>
                              <input class="form-control pt-1" type="file" name="country_img">
                           </div>
                           <div class="form-group">
                              <img src="<?= $country['image']; ?>" class="rounded-circle avatar-lg img-thumbnail mt-1" alt="country-image">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="page-title-box">
                              <div class="page-title-right">
                                 <input type="button" onclick="$('#country_add').submit();" class="btn btn-default pull-right mt-2 mb-2 mr-2" value="<?php echo __('Save'); ?>">
                               </div>
                            </div>
                         </div>
                      </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
