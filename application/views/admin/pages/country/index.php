<style>
    #country_length, .dt-buttons {float: left;}
    .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>

<div class="row">
   <div class="col-12">    
        <div class="py-2 pb-1 text-right">
            <div class="d-sm-block">
                <a class="btn btn-theme waves-effect waves-light btn-bg" href="<?php echo $add_url; ?>"><i class="la la-plus pr-1"></i>Add</a>
            </div>
         </div>
      </div>
   </div>      
<div class="col-lg-12">
<div class="datatable card">
   <div class="card-content collapse show table-responsive">
      <div class="card-body card-dashboard">
         <div >
            <?php echo $this->datatables->generate(); ?>
         </div>
      </div>
   </div>
</div>
</div>
</div>