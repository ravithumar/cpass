<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form id="network_add" enctype="multipart/form-data" action="<?php echo base_url('admin/network/add'); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="row">
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Name'); ?> <span class="text-danger">*</span> </label>
                              <input type="text" class="form-control " name="company_name" placeholder="Please Enter Company Name" data-parsley-required-message="Please Enter Company Name" required="">
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_name'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Number'); ?></label>
                              <input type="text" class="form-control " name="company_number" placeholder="Please Enter Company Number" >
                              
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('VAT Number'); ?></label>
                              <input type="text" class="form-control " name="vat_number" placeholder="Please Enter VAT Number">
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Website'); ?> <span class="text-danger">*</span></label>
                              <input type="url" class="form-control " name="company_website" data-parsley-type="url"  required="" data-parsley-type-message="Please Enter Vaild Company Website" data-parsley-required-message="Please Enter Company Website" placeholder="Please Enter Company Website">
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_website'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Username'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="username" required=""  data-parsley-required-message="Please Enter Company Username" placeholder="Please Enter Company Username">
                              <div class="validation-error-label" >
                                 <?php echo form_error('username'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Email Address'); ?> <span class="text-danger">*</span></label>
                              <input type="email" class="form-control " name="email" data-parsley-type="email"  required="" data-parsley-type-message="Please Enter Vaild Company Email Address" data-parsley-required-message="Please Enter Company Website" placeholder="Please Enter Company Email Address">
                              <div class="validation-error-label" >
                                 <?php echo form_error('email'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label for="product_name">Number of Employees <span class="text-danger">*</span></label>
                              <select class="form-control select2_dropdown " id="no_of_employee" name="no_of_employee" required data-parsley-errors-container="#num_of_emp_error"  data-parsley-required-message="Please Select Number of Employees" >
                                 <option value="">Please select</option>
                                 <option value="1" data-index="0">1</option>
                                 <option value="2 to 5" >2 to 5</option>
                                 <option value="6 to 10" >6 to 10</option>
                                 <option value="11 to 25" >11 to 25</option>
                                 <option value="26 to 50" >26 to 50</option>
                                 <option value="51 to 200" >51 to 200</option>
                                 <option value="201 to 1000" >201 to 1000</option>
                                 <option value="1001 to 10000" >1001 to 10000</option>
                                 <option value="10001 or more" >10001 or more</option>
                              </select>
                              <div id="num_of_emp_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_employee'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Direct phone number for point of contact'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="point_of_contact" data-parsley-required-message="Please Enter Contact Number" required="" placeholder="Please Enter Contact Number">
                              <div class="validation-error-label" >
                                 <?php echo form_error('point_of_contact'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many tests is your company able to perform on a daily basis?'); ?> <span class="text-danger">*</span></label>
                              <input type="number" class="form-control " name="daily_basis_case" data-parsley-type="number" data-parsley-required-message="Please Enter details" data-parsley-type-message="Please Enter only digits" required="" >
                              <div class="validation-error-label" >
                                 <?php echo form_error('daily_basis_case'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Address where tests will be performed'); ?> <span class="text-danger">*</span></label>
                              <textarea class="form-control" name="address" rows="2" placeholder="Please Enter Address" data-parsley-required-message="Please Enter Address"  required=""></textarea> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('address'); ?>
                              </div>
                           </div> 
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Postal Code'); ?> <span class="text-danger">*</span></label>
                              <input type="text" class="form-control " name="postal_code" placeholder="Please Enter Postal Code" data-parsley-required-message="Please Enter postal code" required="" >
                              <div class="validation-error-label" >
                                 <?php echo form_error('postal_code'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many sites do you have?'); ?> <span class="text-danger">*</span></label>
                              <select class="form-control select2_dropdown " id="no_of_sites" name="no_of_sites" required data-parsley-errors-container="#num_of_sites_error"  data-parsley-required-message="Please Select Number of Sites" >
                                 <option value="">Please select</option>
                                 <option value="1" >1</option>
                                 <option value="2" >2</option>
                                 <option value="3" >3</option>
                                 <option value="4" >4</option>
                                 <option value="5" >5</option>
                                 <option value="6-10" >6-10</option>
                                 <option value="11-20" >11-20</option>
                                 <option value="21-50" >21-50</option>
                                 <option value="51-100" >51-100</option>
                                 <option value="100+" >100+</option>
                              </select>
                              <div id="num_of_sites_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_sites'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Do you currently offer COVID testing services?'); ?> <span class="text-danger">*</span></label>
                              <select class="form-control select2_dropdown " id="covid_testing" name="covid_testing" required data-parsley-errors-container="#covid_testing_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" >Yes</option>
                                 <option value="No" >No</option>
                              </select>
                              <div id="covid_testing_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('covid_testing'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Have you performed Fit For Travel covid tests before?'); ?> <span class="text-danger">*</span></label>
                              <select class="form-control select2_dropdown " id="travel_covid_test" name="travel_covid_test" required data-parsley-errors-container="#travel_covid_test_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" >Yes</option>
                                 <option value="No" >No</option>
                              </select>
                              <div id="travel_covid_test_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('travel_covid_test'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('When would you like to start offering our service?'); ?> <span class="text-danger">*</span></label>
                              <input type="date" class="form-control datepicker" name="start_offering_service" data-parsley-required-message="Please Select Date" required="" placeholder="Please Select Date">
                              <div class="validation-error-label" >
                                 <?php echo form_error('start_offering_service'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Will you be advertising that you can now offer COVID travel tests?'); ?> <span class="text-danger">*</span></label>
                              <textarea class="form-control" name="advertising" rows="2" placeholder="Please Enter Details"  data-parsley-required-message="Please Enter Details"  required=""></textarea> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('advertising'); ?>
                              </div>
                           </div>
                           <div class="col-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#network_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>