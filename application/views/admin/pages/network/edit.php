<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form id="network_add" enctype="multipart/form-data" action="<?php echo base_url('admin/network/edit/'.$network->id); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">   
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="row">
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Name'); ?></label>
                              <input type="text" class="form-control " name="company_name" data-parsley-required-message="Please Enter Name" required="" value="<?php echo $network->company_name; ?>" placeholder="Please Enter Name">
                              <input type="hidden" class="form-control " name="id" value="<?php echo $network->id; ?>">
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_name'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Number'); ?></label>
                              <input type="text" class="form-control " name="company_number"  value="<?php echo $network->company_number; ?>" placeholder="Please Enter Company Number"> 
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('VAT Number'); ?></label>
                              <input type="text" class="form-control " name="vat_number"  value="<?php echo $network->vat_number; ?>" placeholder="Please Enter Company Number"> 
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Website'); ?></label>
                              <input type="text" class="form-control " name="company_website"  value="<?php echo $network->company_website; ?>" placeholder="Please Enter Company Website"> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('company_website'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Username'); ?></label>
                              <input type="text" class="form-control " name=""  value="<?php echo $user->username; ?>" disabled> 
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Company Email Address'); ?></label>
                              <input type="text" class="form-control " name=""  value="<?php echo $user->email; ?>" disabled> 
                           </div>

                           <div class="form-group col-6">
                              <label for="product_name">Number of Employees</label>
                              <select class="form-control select2_dropdown " id="no_of_employee" name="no_of_employee" required data-parsley-errors-container="#num_of_emp_error"  data-parsley-required-message="Please Select Number of Employees" >
                                 <option value="">Please select</option>
                                 <option value="1" <?php echo ($network->no_of_employee == "1") ? 'selected' : ''; ?> >1</option>
                                 <option value="2 to 5" <?php echo ($network->no_of_employee == "2 to 5") ? 'selected' : ''; ?> >2 to 5</option>
                                 <option value="6 to 10" <?php echo ($network->no_of_employee == "6 to 10") ? 'selected' : ''; ?> >6 to 10</option>
                                 <option value="11 to 25" <?php echo ($network->no_of_employee == "11 to 25") ? 'selected' : ''; ?> >11 to 25</option>
                                 <option value="26 to 50" <?php echo ($network->no_of_employee == "26 to 50") ? 'selected' : ''; ?> >26 to 50</option>
                                 <option value="51 to 200" <?php echo ($network->no_of_employee == "51 to 200") ? 'selected' : ''; ?> >51 to 200</option>
                                 <option value="201 to 1000" <?php echo ($network->no_of_employee == "201 to 1000") ? 'selected' : ''; ?> >201 to 1000</option>
                                 <option value="1001 to 10000" <?php echo ($network->no_of_employee == "1001 to 10000") ? 'selected' : ''; ?> >1001 to 10000</option>
                                 <option value="10001 or more" <?php echo ($network->no_of_employee == "10001 or more") ? 'selected' : ''; ?> >10001 or more</option>
                              </select>
                              <div id="num_of_emp_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_employee'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Direct phone number for point of contact'); ?></label>
                              <input type="text" class="form-control " name="point_of_contact" data-parsley-required-message="Please Enter Contact Number" value="<?php echo $network->point_of_contact; ?>" required="" placeholder="Please Enter Contact Number">
                              <div class="validation-error-label" >
                                 <?php echo form_error('point_of_contact'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many tests is your company able to perform on a daily basis?'); ?></label>
                              <input type="number" class="form-control " name="daily_basis_case" value="<?php echo $network->daily_basis_case; ?>" data-parsley-type="number" data-parsley-required-message="Please Enter details" data-parsley-type-message="Please Enter only digits" required="" >
                              <div class="validation-error-label" >
                                 <?php echo form_error('daily_basis_case'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Address where tests will be performed'); ?></label>
                              <textarea class="form-control" name="address" rows="2" placeholder="Please Enter Address" data-parsley-required-message="Please Enter Address"  required=""><?php echo $network->address; ?></textarea> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('address'); ?>
                              </div>
                           </div> 
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Postal Code'); ?></label>
                              <input type="text" class="form-control" name="postal_code" value="<?php echo $network->postal_code; ?>" placeholder="Please Enter Postal Code" data-parsley-required-message="Please Enter postal code" required="" >
                              <div class="validation-error-label" >
                                 <?php echo form_error('postal_code'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('How many sites do you have?'); ?></label>
                              <select class="form-control select2_dropdown " id="no_of_sites" name="no_of_sites"  required data-parsley-errors-container="#num_of_sites_error"  data-parsley-required-message="Please Select Number of Sites" >
                                 <option value="">Please select</option>
                                 <option value="1" <?php echo ($network->no_of_sites == "1") ? 'selected' : ''; ?> >1</option>
                                 <option value="2" <?php echo ($network->no_of_sites == "2") ? 'selected' : ''; ?> >2</option>
                                 <option value="3" <?php echo ($network->no_of_sites == "3") ? 'selected' : ''; ?> >3</option>
                                 <option value="4" <?php echo ($network->no_of_sites == "4") ? 'selected' : ''; ?> >4</option>
                                 <option value="5" <?php echo ($network->no_of_sites == "5") ? 'selected' : ''; ?> >5</option>
                                 <option value="6-10" <?php echo ($network->no_of_sites == "6-10") ? 'selected' : ''; ?> >6-10</option>
                                 <option value="11-20" <?php echo ($network->no_of_sites == "11-20") ? 'selected' : ''; ?> >11-20</option>
                                 <option value="21-50" <?php echo ($network->no_of_sites == "21-50") ? 'selected' : ''; ?> >21-50</option>
                                 <option value="51-100" <?php echo ($network->no_of_sites == "51-100") ? 'selected' : ''; ?> >51-100</option>
                                 <option value="100+" <?php echo ($network->no_of_sites == "100+") ? 'selected' : ''; ?> >100+</option>
                              </select>
                              <div id="num_of_sites_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('no_of_sites'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Do you currently offer COVID testing services?'); ?></label>
                              <select class="form-control select2_dropdown " id="covid_testing" name="covid_testing" required data-parsley-errors-container="#covid_testing_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" <?php echo ($network->covid_testing == "Yes") ? 'selected' : ''; ?> >Yes</option>
                                 <option value="No" <?php echo ($network->covid_testing == "No") ? 'selected' : ''; ?>>No</option>
                              </select>
                              <div id="covid_testing_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('covid_testing'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Have you performed Fit For Travel covid tests before?'); ?></label>
                              <select class="form-control select2_dropdown " id="travel_covid_test" name="travel_covid_test" required data-parsley-errors-container="#travel_covid_test_error"  data-parsley-required-message="Please Select Any One Option" >
                                 <option value="">Please select</option>
                                 <option value="Yes" <?php echo ($network->travel_covid_test == "Yes") ? 'selected' : ''; ?> >Yes</option>
                                 <option value="No" <?php echo ($network->travel_covid_test == "No") ? 'selected' : ''; ?> >No</option>
                              </select>
                              <div id="travel_covid_test_error"></div>
                              <div class="validation-error-label" >
                                 <?php echo form_error('travel_covid_test'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('When would you like to start offering our service?'); ?></label>
                              <input type="date" class="form-control datepicker" name="start_offering_service" value="<?php echo $network->start_offering_service; ?>" data-parsley-required-message="Please Select Date" required="" placeholder="Please Select Date">
                              <div class="validation-error-label" >
                                 <?php echo form_error('start_offering_service'); ?>
                              </div>
                           </div>
                           <div class="form-group col-6">
                              <label class="form-control-label"><?php echo __('Will you be advertising that you can now offer COVID travel tests?'); ?></label>
                              <textarea class="form-control" name="advertising" rows="2" placeholder="Please Enter Details"  data-parsley-required-message="Please Enter Details"  required=""><?= $network->advertising; ?></textarea> 
                              <div class="validation-error-label" >
                                 <?php echo form_error('advertising'); ?>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#network_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
