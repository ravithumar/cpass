<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="report_type_add" enctype="multipart/form-data" action="<?php echo base_url('admin/report/edit/'.$report->id); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">   
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 ">
						         <div class="form-group">
                              <label for="product_name">Report Type</label>
                              <select class="form-control" id="report_id" name="report_id" data-parsley-required="true" data-parsley-errors-container="#report-type-error" data-parsley-error-message="Please Select Report Type">
                                 <option value="">Select Report Type</option>
                                 <?php
                                    if (isset($report_type) && !empty($report_type)) {
                                      foreach ($report_type as $key => $value) {
                                        if ($report->report_type_id == $value->id) {
                                          $selected_value = 'selected';
                                        } else {
                                          $selected_value = '';
                                    }?>
                                 <option value="<?php echo $value->id; ?>" <?php echo $selected_value; ?> ><?php echo ucfirst($value->name); ?></option>
                                 <?php }
                                    }
                                    ?>
                              </select>
                              <span id="report-type-error"></span>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Name'); ?></label>
                              <input type="text" class="form-control " name="name" data-parsley-required-message="Please Enter Name" required="" value="<?php echo $report->name; ?>" placeholder="Please Enter Name">
                              <input type="hidden" class="form-control " name="id" value="<?php echo $report->id; ?>">
                           </div>
									<div class="form-group">
                              <label class="form-control-label"><?php echo __('Total Minutes'); ?></label>
                              <input type="text" class="form-control " name="minutes" data-parsley-required-message="Please Enter Minutes" required="" placeholder="Please Enter Minutes" value="<?php echo $report->minutes; ?>">
                           </div>
                           <div class="form-group"> 
                              <label for="name">Country Zone</label>
                              <select name="zone[]" id="zone" class="form-control select2_dropdown" data-parsley-required-message="Please Select Country Zone" required="" multiple>
											<?php 
											$zone_array = array();
											if(!empty($report->zone)){
												$zone_array = explode(',',$report->zone);
											}
											if (!empty($zones)) {
                                    foreach ($zones as $zone_row) { ?>
                                       <option value="<?= $zone_row->id; ?>" <?=in_array($zone_row->id,$zone_array)?"selected":""?>><?= $zone_row->name; ?></option>
                                    <?php }
                                 } ?> 
                              </select>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('SKU'); ?></label>
                              <input type="text" class="form-control " name="SKU" data-parsley-required-message="Please Enter SKU"  value="<?php echo $report->SKU; ?>" required="" placeholder="Please Enter SKU">
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Price'); ?></label>
                              <input type="text" class="form-control" name="price" data-parsley-type="number" min="1" value="<?php echo $report->price; ?>" data-parsley-required-message="Please Enter Price" data-parsley-type-message="Please Enter only digits" required="" placeholder="Please Enter Price">
                           </div>
                           <!-- <div class="form-group"> 
                              <label for="name">Is Domestic?</label>
                              <select name="is_domestic" id="is_domestic" class="form-control"  >
                                 <option value="yes" <?php echo $report->is_domestic == "yes" ? 'selected' : ''; ?> >Yes</option>
                                 <option value="no" <?php echo $report->is_domestic == "no" ? 'selected' : ''; ?> >No</option>
                              </select>
                           </div> -->
									<div class="form-group">
                              <label for="name">Purpose Type</label>
                              <select name="purpose_type[]" id="purpose_type" class="form-control select2_dropdown" data-parsley-required-message="Please Select Purpose Type" required="" multiple onchange="updateValue()">
											<?php 
											$purpose_type_array = array();
											if(!empty($report->purpose_type)){
												$purpose_type_array = explode(',',$report->purpose_type);
											}
											if (!empty($purpose_type)) {
                                    foreach ($purpose_type as $purpose_type_row) { 
													if(!empty($purpose_type_row['subcat'])){ 
														foreach($purpose_type_row['subcat'] as $sub_row){ ?>
                                       		<option value="<?=$sub_row['id']?>" <?=in_array($sub_row['id'],$purpose_type_array)?"selected":""?>><?= $sub_row['name']; ?></option>
														<?php } ?>
													<?php } else{ ?>
														<option value="<?=$purpose_type_row['id']?>" <?=in_array($purpose_type_row['id'],$purpose_type_array)?"selected":""?>><?= $purpose_type_row['name']; ?></option><?php
													}
                                 	 }
                                 } ?> 
                              </select>
                           </div>

                           <div class="form-group" style="display:none;" id="select_step">
                              <label for="select_steps">Select Steps</label>
                              <select name="select_step" class="form-control">
                                 <option value="1" <?php if($report->select_steps == '1'){echo 'selected';} ?>>1 Steps</option>
                                 <option value="2"  <?php if($report->select_steps == '2'){echo 'selected';} ?>>2 Steps</option>
                              </select>
                           </div>

									<div class="form-group">
                              <label for="name">Vaccines</label>
                              <select name="vaccine[]" id="vaccine" class="form-control select2_dropdown" multiple>
											<?php 
											$vaccines_array = array();
											if(!empty($report->vaccine)){
												$vaccines_array = explode(',',$report->vaccine);
											}
											if (!empty($vaccines)) {
                                    foreach ($vaccines as $vaccines_row) { ?>
													<option value="<?=$vaccines_row['id']?>" <?=in_array($vaccines_row['id'],$vaccines_array)?"selected":""?>><?= $vaccines_row['name']; ?></option><?php
												}
                                 } ?> 
                              </select>
                           </div>
									<div class="form-group">
                              <label for="name">Vaccine Status</label>
                              <select name="vaccination_status[]" id="vaccination_status" class="form-control select2_dropdown" multiple>
											<?php 
											$vaccine_status_array = array();
											if(!empty($report->vaccination_status)){
												$vaccine_status_array = explode(',',$report->vaccination_status);
											}
											if (!empty($vaccine_status)) {
                                    foreach ($vaccine_status as $vaccine_status_row) { ?>
													<option value="<?=$vaccine_status_row['id']?>" <?=in_array($vaccine_status_row['id'],$vaccine_status_array)?"selected":""?>><?= $vaccine_status_row['name']; ?></option><?php
												}
                                 } ?> 
                              </select>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label">Medical Exemptions</label>
                              <select name="medical_exemptions" class="form-control">
                                 <option value="no" <?=$report->medical_exemptions == "no"?"selected":""?>>No</option>
                                 <option value="yes" <?=$report->medical_exemptions == "yes"?"selected":""?>>Yes</option>
                              </select>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Days'); ?></label>
                                 <?php
                                 if ($report->days != NULL) {
                                    $days = explode(",",$report->days);
                                    foreach ($days as $key => $value) { 
                                       if ($key== 0) { ?>
                                          <div class="row records d-flex justify-content-betweeen">
                                             <div class="col-lg-10"> 
                                                <input name="days[]" type="number" placeholder="PLease Enter Total Days" value="<?= $value; ?>" class="form-control mr-2 dayscount" required="" data-parsley-required-message="Please Enter Total Days">
                                             </div>
                                             <div class="col-lg-2 extra-fields"> 
                                                <a class="btn btn-success ml-2" href="javascript:void(0);">+</a>
                                             </div>
                                          </div>
                                          <div class="records_dynamic">
                                       <?php }else{ ?>
                                       <div class="remove row d-flex justify-content-between my-2 ">
                                          <div class="col-lg-10"> 
                                             <input name="days[]" type="number" value="<?= $value; ?>" placeholder="PLease Enter Total Days" class="form-control mr-2 dayscount" >
                                          </div> 
                                          <div class="col-lg-2 remove-field">
                                             <a href="javascript:void(0);" class="remove-field btn btn-danger ml-2 btn-remove-days">-</a>
                                          </div>
                                       </div>
                                    <?php } }
                                 }else{ ?>
                                    <div class="row records d-flex justify-content-betweeen">
                                       <div class="col-lg-10"> 
                                          <input name="days[]" type="number" placeholder="PLease Enter Total Days" value="<?= $value; ?>" class="form-control mr-2 dayscount" required="" data-parsley-required-message="Please Enter Total Days">
                                       </div>
                                       <div class="col-lg-2 extra-fields"> 
                                          <a class="btn btn-success ml-2" href="javascript:void(0);">+</a>
                                       </div>
                                    </div>
                                    <div class="records_dynamic">

                                 <?php }
                                 ?>
                              </div>
                           </div> 
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="form-control-label">Reports SKU</label>
                           </div>
                        </div>
                        

                        <div class="past_report_sku">
                           <?php 
                           if(!empty($reports_sku)){
                              foreach($reports_sku as $sku_key=>$sku_row){
                                 $zone_id = array();
                                 if(!empty($sku_row['zone_id'])){
                                    $zone_id = explode(',',$sku_row['zone_id']);
                                 }
                                 $vaccination_status = array();
                                 if(!empty($sku_row['vaccination_status'])){
                                    $vaccination_status = explode(",",$sku_row['vaccination_status']);
                                 }
                                 ?>
                                 <div class="col-12 remove_sku">
                                    <div class="form-group">
                                       <div class="row d-flex justify-content-betweeen">
                                          <div class="col-lg-6"> 
                                             <label class="form-control-label">Zone</label>
                                             <input type="hidden" name="report_zone[]" class="hidden_report_zone" value="<?=$sku_row['zone_id']?>">
                                             <select id="report_zone" class="form-control select2_dropdown report_zone" multiple>
                                                <?php if (!empty($zones)) {
                                                   foreach ($zones as $zone_row) { ?>
                                                      <option value="<?= $zone_row->id; ?>" <?=in_array($zone_row->id,$zone_id)?"selected":""?>><?= $zone_row->name; ?></option>
                                                   <?php }
                                                } ?> 
                                             </select>
                                             
                                          </div>
                                          <div class="col-lg-6"> 
                                             <label class="form-control-label">Vaccine Status</label>
                                             <select id="report_vaccination_status" class="form-control select2_dropdown report_vaccination_status" multiple>
                                                <?php 
                                                if (!empty($vaccine_status)) {
                                                   foreach ($vaccine_status as $vaccine_status_row) { ?>
                                                      <option value="<?=$vaccine_status_row['id']?>" <?=in_array($vaccine_status_row['id'],$vaccination_status)?"selected":""?>><?= $vaccine_status_row['name']; ?></option><?php
                                                   }
                                                } ?> 
                                             </select>
                                             <input type="hidden" name="report_vaccination_status[]" class="hidden_report_vaccination_status" value="<?=$sku_row['vaccination_status']?>">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="row d-flex justify-content-betweeen">
                                          <div class="col-lg-6"> 
                                             <label class="form-control-label">Booking Test Type</label>
                                             <input type="text" name="report_booking_test_type[]" class="form-control" placeholder="PCR002NN" value="<?=$sku_row['booking_test_type']?>">
                                          </div>
                                          <div class="col-lg-6"> 
                                             <label class="form-control-label">Lab Test</label>
                                             <input type="text" name="report_lab_test[]" class="form-control" placeholder="PCR000L" value="<?=$sku_row['lab_test']?>">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row text-right">
                                       <div class="col-lg-12"> 
                                          <?php
                                          if($sku_key == 0){ ?>
                                             <a class="btn btn-success ml-2 add_more_reports_sku" href="javascript:void(0);">+</a>
                                          <?php }else { ?>
                                             <a class="btn btn-danger ml-2 remove_reports_sku" href="javascript:void(0);">-</a><?php
                                          } ?>
                                       </div>
                                    </div>
                                 </div><?php 
                              } 
                           }else{ ?>
                              <div class="col-12">
                                 <div class="form-group">
                                    <div class="row d-flex justify-content-betweeen">
                                       <div class="col-lg-6"> 
                                          <label class="form-control-label">Zone</label>
                                          <input type="hidden" name="report_zone[]" class="hidden_report_zone">
                                          <select id="report_zone" class="form-control select2_dropdown report_zone" multiple>
                                             <?php if (!empty($zones)) {
                                                foreach ($zones as $zone_row) { ?>
                                                   <option value="<?= $zone_row->id; ?>" ><?= $zone_row->name; ?></option>
                                                <?php }
                                             } ?> 
                                          </select>
                                          
                                       </div>
                                       <div class="col-lg-6"> 
                                          <label class="form-control-label">Vaccine Status</label>
                                          <select id="report_vaccination_status" class="form-control select2_dropdown report_vaccination_status" multiple>
                                             <?php 
                                             if (!empty($vaccine_status)) {
                                                foreach ($vaccine_status as $vaccine_status_row) { ?>
                                                   <option value="<?=$vaccine_status_row['id']?>"><?= $vaccine_status_row['name']; ?></option><?php
                                                }
                                             } ?> 
                                          </select>
                                          <input type="hidden" name="report_vaccination_status[]" class="hidden_report_vaccination_status">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="row d-flex justify-content-betweeen">
                                       <div class="col-lg-6"> 
                                          <label class="form-control-label">Booking Test Type</label>
                                          <input type="text" name="report_booking_test_type[]" class="form-control" placeholder="PCR002NN">
                                       </div>
                                       <div class="col-lg-6"> 
                                          <label class="form-control-label">Lab Test</label>
                                          <input type="text" name="report_lab_test[]" class="form-control" placeholder="PCR000L">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row text-right">
                                    <div class="col-lg-12"> 
                                       <a class="btn btn-success ml-2 add_more_reports_sku" href="javascript:void(0);">+</a>
                                    </div>
                                 </div>
                              </div>
                           <?php } ?>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#report_type_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="copy_report_sku" style="display:none;">
   <div class="col-12 remove_sku">
      <div class="form-group">
         <div class="row d-flex justify-content-betweeen">
            <div class="col-lg-6"> 
               <label class="form-control-label">Zone</label>
               <select id="report_zone" class="form-control select2_dynamic_dropdown report_zone" multiple>
                  <?php if (!empty($zones)) {
                     foreach ($zones as $zone_row) { ?>
                        <option value="<?=$zone_row->id; ?>"><?= $zone_row->name; ?></option>
                     <?php }
                  } ?> 
               </select>
               <input type="hidden" name="report_zone[]" class="hidden_report_zone">
            </div>
            <div class="col-lg-6"> 
               <label class="form-control-label">Vaccine Status</label>
               <select id="report_vaccination_status" class="form-control select2_dynamic_dropdown report_vaccination_status" multiple>
                  <?php 
                  if (!empty($vaccine_status)) {
                     foreach ($vaccine_status as $vaccine_status_row) { ?>
                        <option value="<?=$vaccine_status_row['id']?>"><?= $vaccine_status_row['name']; ?></option><?php
                     }
                  } ?> 
               </select>
               <input type="hidden" name="report_vaccination_status[]" class="hidden_report_vaccination_status">
            </div>
         </div>
      </div>
      <div class="form-group">
         <div class="row d-flex justify-content-betweeen">
            <div class="col-lg-6"> 
               <label class="form-control-label">Booking Test Type</label>
               <input type="text" name="report_booking_test_type[]" class="form-control" placeholder="PCR002NN">
            </div>
            <div class="col-lg-6"> 
               <label class="form-control-label">Lab Test</label>
               <input type="text" name="report_lab_test[]" class="form-control" placeholder="PCR000L">
            </div>
         </div>
      </div>
      <div class="row text-right">
         <div class="col-lg-12"> 
            <a class="btn btn-danger ml-2 remove_reports_sku" href="javascript:void(0);">-</a>
         </div>
      </div>
   </div>
</div>


<script>
   
   var report = <?php echo json_encode($purpose_type_array); ?>;
   
   let result = report.includes('3'); 

      if(result == true)
      {
         document.getElementById('select_step').style.display = "block";
      }
      else
      {
         document.getElementById('select_step').style.display = "none";
      }


</script>

<script>
   function updateValue()
   {
      var x = document.getElementById("purpose_type");

         var selectedValues = [];    
         $("#purpose_type :selected").each(function(){
            selectedValues.push($(this).val()); 
         });

         let result = selectedValues.includes('3'); 

      if(result == true)
      {
         document.getElementById('select_step').style.display = "block";
      }
      else
      {
         document.getElementById('select_step').style.display = "none";
      }
              
   }

</script>