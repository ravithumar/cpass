<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="promocode_add" enctype="multipart/form-data" action="<?php echo base_url('admin/service/add'); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-6">
                           <div class="form-group">
                              <label for="product_name">Provider</label>
                              <select class="form-control select2_dropdown " id="provider" name="provider_id" required data-parsley-errors-container="#category_id_error"  data-parsley-required-message="Please Select Provider" >
                                 <option value="">Please Select Provider</option>
                                 <?php
                                    if(isset($provider) && !empty($provider))
                                    {
                                        foreach ($provider as $key => $value) { ?>                                                            
                                 <option value="<?php echo $value->id; ?>"><?php echo $value->username; ?></option>
                                 <?php }
                                    }
                                    ?>                                                
                              </select>
                              <div id="category_id_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Booking Price</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="text" id="" class="form-control number_pnt" name="booking_price"  required="" data-parsley-required-message="Please Select Start Date" placeholder="Please Enter Booking Price" data-parsley-errors-container="#from_date_error" >
                              </fieldset>
                              <div id="from_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Fixed Price</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="text" id=""  class="form-control number_pnt" name="fixed_price"  required="" data-parsley-required-message="Please Select End Date" placeholder="Please Enter Fixed Price" data-parsley-errors-container="#to_date_error ">
                              </fieldset>
                              <div id="to_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label">Image</label>
                              <div class="custom-file">
                                 <input type="file" name="image" required class="custom-file-input parsley-error service_image" id="service_image" data-parsley-required-message="Please Select Image" data-parsley-errors-container="#service_image_error" accept="image/*" id="form_image" data-parsley-fileextension="jpg||png||jpeg||JPG||PNG||JPEG"  >
                                 <label class="custom-file-label" >Choose file</label>
                              </div>
                              <div id="service_image_error"></div>
                              <div class="" id="service_image_preview" style="margin-top: 5px;"></div>
                              <div class="col-12 mt-2">
                                 <span class="service_image_error" id="service_image_error"></span>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#promocode_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
