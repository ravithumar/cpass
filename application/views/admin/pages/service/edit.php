<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="promocode_add" enctype="multipart/form-data" action="<?php echo base_url('admin/service/edit/'.$service->id); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-6">                           
                           <div class="form-group">
                              <label>Provider</label><br />
                              <fieldset class="form-group form-group-style">                                 
                                 <input type="text" id="" class="form-control " name="provider_name"  data-parsley-required-message="Please Select Provider" placeholder="Please Enter Provider" data-parsley-errors-container="#Provider_error" value="<?php echo $users->name; ?>" readonly>
                              </fieldset>
                                 <input type="hidden" id="" class="form-control " name="provider_id"  data-parsley-required-message="" placeholder="" data-parsley-errors-container="#Provider_error" value="<?php echo $service->user_id; ?>">
                              <div id="Provider_error"></div>
                           </div>
                           <div class="form-group">
                              <label for="product_name">Category</label>
                              <select class="form-control select2_dropdown " id="" name="category_id" required data-parsley-errors-container="#category_id_error"  data-parsley-required-message="Please Select Category" >
                                 <option value="">Select Select Category</option>
                                 <?php
                                    if (isset($category) && !empty($category)) {
                                      foreach ($category as $key => $value) {
                                        if ($service->category_id == $value->id) {
                                          $selected_value = 'selected';
                                        } else {
                                          $selected_value = '';
                                    }?>
                                 <option value="<?php echo $value->id; ?>" <?php echo $selected_value; ?> ><?php echo ucfirst($value->name); ?></option>
                                 <?php }
                                    }
                                    ?>
                              </select>
                              <div id="category_id_error"></div>
                           </div>
                           <div class="form-group">
                              <label for="product_name">Sub Category</label>
                              <select class="form-control select2_dropdown " id="" name="sub_category_id" required data-parsley-errors-container="#sub_category_id_error"  data-parsley-required-message="Please Select Sub Category" >
                                 <option value="">Select Select Sub Category</option>
                                 <?php
                                    if (isset($sub_category) && !empty($sub_category)) {
                                      foreach ($sub_category as $key => $value) {
                                        if ($service->sub_category_id == $value->id) {
                                          $selected_value = 'selected';
                                        } else {
                                          $selected_value = '';
                                    }?>
                                 <option value="<?php echo $value->id; ?>" <?php echo $selected_value; ?> ><?php echo ucfirst($value->name); ?></option>
                                 <?php }
                                    }
                                    ?>
                              </select>
                              <div id="sub_category_id_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Title</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="text" id="" class="form-control " name="title"  required="" data-parsley-required-message="Please Enter Title" placeholder="Please Enter Title" data-parsley-errors-container="#from_date_error" value="<?php echo $service->title; ?>">
                              </fieldset>
                              <div id="from_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Description</label><br />
                              <fieldset class="form-group form-group-style">
                                <textarea id=""  class="form-control " name="description"  required="" data-parsley-required-message="Please Enter Description" placeholder="Please Enter Description" data-parsley-errors-container="#to_date_error " style="height: 200px;"><?php echo $service->description; ?></textarea>
                              </fieldset>
                              <div id="to_date_error"></div>
                           </div> 
                           <div class="form-group">
                              <label>Booking Price</label><br />
                              <fieldset class="form-group form-group-style">
                                 <input type="text" id="" class="form-control number_pnt" name="booking_price"  required="" data-parsley-required-message="Please Select Start Date" placeholder="Please Enter Booking Price" data-parsley-errors-container="#from_date_error" value="<?php echo $service->booking_price; ?>">
                              </fieldset>
                              <div id="from_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Fixed Price</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="text" id=""  class="form-control number_pnt" name="fixed_price"  required="" data-parsley-required-message="Please Select End Date" placeholder="Please Enter Fixed Price" data-parsley-errors-container="#to_date_error " value="<?php echo $service->fixed_price; ?>">
                              </fieldset>
                              <div id="to_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label">Image</label>
                              <div class="custom-file">
                                 <input type="file" name="image[]"  class="custom-file-input service_image" id="service_image" data-parsley-required-message="Please Select Image" data-parsley-errors-container="#service_image_error" accept="image/*" id="form_image" data-parsley-fileextension="jpg||png||jpeg||JPG||PNG||JPEG"  multiple="">
                                 <label class="custom-file-label" >Choose file</label>
                              </div>
                              <div class="" id="service_image_preview" style="margin-top: 5px;"> </div>
                              <div class="row col-12 mt-2">
                                 <?php foreach ($service_image as $key => $value) {
                                    ?>
                                 <?php if (isset($value->image) && $value->image != '') {
                                    if (file_exists($value->image)) {
                                      $option = BASE_URL() . $value->image;
                                      $image_class = 'd-block';
                                    } else {
                                      $option = BASE_URL() . '/assets/images/default-profile.png';
                                      $image_class = '';
                                    }
                                    ?>
                                 <div class="col-md-4 mb-2 remove_properties_more_image_<?php echo $value->id; ?>">                                    
                                    <a href="<?php echo $option; ?>" class="image-popup">
                                    <img id="images_<?php echo $option; ?>" onerror="this.src='<?php echo BASE_URL('assets/images/default-profile.png') ?>'" src="<?php echo $option; ?>" class="border rounded preview <?php echo $image_class; ?>" alt="Category" style="height: auto; width: 78px;height:100px; object-fit: contain;" >
                                    </a>
                                    <button type="button" class="btn btn-danger remove_properties_more_image" id="<?php echo $value->id; ?>">Remove</button>
                                 </div>
                                 <?php
                                    }?>
                                 <?php
                                    }?>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#promocode_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
