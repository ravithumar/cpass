<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="sub_category_add" enctype="multipart/form-data" action="<?php echo base_url('admin/sub-category/edit/'.$sub_category->id); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-6">
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Name'); ?></label>
                              <input type="text" class="form-control " name="name" data-parsley-required-message="Please Enter Name" required="" value="<?php echo $sub_category->name; ?>" placeholder="Please Enter Name">
                              <input type="hidden" class="form-control " name="id" value="<?php echo $sub_category->id; ?>">
                           </div>
                           <div class="form-group">
                              <label for="product_name">Category</label>
                              <select class="form-control" id="category_id" name="category_id" data-parsley-required="true" data-parsley-errors-container="#category-error" data-parsley-error-message="Please Select Category">
                                 <option value="">Select Category</option>
                                 <?php
                                    if (isset($category) && !empty($category)) {
                                      foreach ($category as $key => $value) {
                                        if ($sub_category->category_id == $value->id) {
                                          $selected_value = 'selected';
                                        } else {
                                          $selected_value = '';
                                    }?>
                                 <option value="<?php echo $value->id; ?>" <?php echo $selected_value; ?> ><?php echo ucfirst($value->name); ?></option>
                                 <?php }
                                    }
                                    ?>
                              </select>
                              <span id="category-error"></span>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#sub_category_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
