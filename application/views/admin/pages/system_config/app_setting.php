<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <form id="config_form" enctype="multipart/form-data" action="<?php echo base_url('Admin/SystemController/save_app_config'); ?>" method="post" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                           <input type="submit" class="btn btn-default pull-right clearfix" value="<?php echo __('Save'); ?>">
                        </div>
                        <h4 class="page-title"><?php echo __('App Configuration'); ?></h4>
                        <?php echo $this->breadcrumbs->show(); ?>
                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/includes/message'); ?>
            <div class="row">
                <div class="col-sm-12">
             
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="portlet ">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('Covid app maintenance & update settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#customer-app"><i class="ion-minus-round"></i></a>
                            </div>
                            
                        </div>
                        <div id="customer-app" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Android version'); ?></label>
                                        <input type="text" value="<?php echo $app[0]->app_version; ?>" class="form-control" name="android_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="android_update" name="android_update" type="checkbox"  <?php echo $app[0]->updates==1?"checked":""; ?>>
                                            <label for="android_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('IOS version'); ?></label>
                                        <input type="text" value="<?php echo $app[1]->app_version; ?>" class="form-control" name="ios_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="ios_update" name="ios_update" type="checkbox"  <?php echo $app[1]->updates==1?"checked":""; ?>>
                                            <label for="ios_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Maintenance Mode'); ?></label>
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="maintenance_mode" name="maintenance_mode" type="checkbox"  <?php echo $app[2]->updates==1?"checked":""; ?>>
                                            <label for="maintenance_mode" class="text-danger">
                                                <?php echo __('Maintenance Mode Enable?'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>