<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <!-- <li class="breadcrumb-item"><?= $user->full_name; ?></li> -->
                     <li class="breadcrumb-item active">User Details</li>
                  </ol>
               </div>
               <h4 class="page-title">User Details</h4>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-lg-4 col-xl-4">
         <div class="card-box text-center">
            <?php if ($user->profile_picture != "") { ?>
               <img src="<?php echo base_url($user->profile_picture); ?>" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
            <?php } ?>
            <h4 class="mb-0"><?= $user->full_name; ?></h4>
            <div class="text-left mt-3">
               <h4 class="font-13 text-uppercase">About Me :</h4>
               <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2"><?= $user->full_name; ?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2"><?= $user->phone; ?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2 "><?= $user->email; ?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Passport Number :</strong> <span class="ml-2 "><?= $user->passport; ?></span></p>
               <p class="text-muted mb-1 font-13"><strong>Birth Date :</strong> <span class="ml-2"><?= $user->date_of_birth; ?></span></p>
               <!-- <p class="text-muted mb-1 font-13"><strong>Total Test Count :</strong> <span class="ml-2"><?= $user->count; ?></span></p> -->
            </div>
            <div class="row mt-4">
                 <div class="col-4">
                     <div class="mt-3">
                         <h4>3</h4>
                         <p class="mb-0 text-muted text-truncate">Total Tests</p>
                     </div>
                 </div>
                 <div class="col-4">
                     <div class="mt-3">
                         <h4 class="text-danger">1</h4>
                         <p class="mb-0 text-muted text-truncate">Positive</p>
                     </div>
                 </div>
                 <div class="col-4">
                     <div class="mt-3">
                         <h4 class="text-success">2</h4>
                         <p class="mb-0 text-muted text-truncate">Negative</p>
                     </div>
                 </div>
             </div>
         </div> <!-- end card-box -->
         <?php if (!empty($members)) { ?>
         <div class="card-box">
            <h4 class="header-title mb-3">Member List </h4>
            <div class="inbox-widget slimscroll" style="max-height: 310px;">
               <?php foreach ($members as $k => $v) { ?>
                     <div class="inbox-item">

                        <?php if ($v->profile_picture !="") { ?>
                           <div class="inbox-item-img"><img src="<?php echo base_url($v->profile_picture); ?>" class="rounded-circle" alt="profile-image"></div>
                        <?php } ?>
                        <p class="inbox-item-author"><?= $v->full_name; ?></p>
                        <p class="inbox-item-text"><?= $v->email; ?></p>
                        <p class="inbox-item-date">
                           <a href="<?php echo base_url('admin/user/details/'.$v->id); ?>" class="btn btn-sm btn-link text-info font-13"> View </a>
                        </p>
                     </div>
                  <?php 
               } ?>
            </div> 
         </div>
         <?php } ?>
      </div> 
      <div class="col-lg-8 col-xl-8">
         <div class="card-box">
            <div class="tab-content">
               <div class="tab-pane active" id="aboutme">
                  <h5 class="mb-3 mt-0 text-uppercase text-center"><i class="mdi mdi-cards-variant mr-1"></i>
                     Bookings</h5>
                  <div class="table-responsive">
                     <table class="table table-borderless mb-0">
                        <thead class="thead-light">
                           <tr>
                              <th>#</th>
                              <th>Booking Name</th>
                              <th>Price</th>
                              <th>Total</th>
                              <th>Date</th>
                              <th>Payment Status</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php if (isset($booking)) {
                              foreach ($booking as $key => $value) {?>
                                 <tr>
                                    <td>1</td>
                                    <td>App design and development</td>
                                    <td>01/01/2015</td>
                                    <td>10/15/2018</td>
                                    <td><span class="badge badge-info">Work in Progress</span></td>
                                    <td>Halette Boivin</td>
                                 </tr>
                              <?php }
                           } ?>

                        </tbody>
                     </table>
                  </div>
               </div> 
            </div> <!-- end tab-content -->
         </div> <!-- end card-box-->
      </div> <!-- end col -->
   </div>
   <!-- end row-->
</div>