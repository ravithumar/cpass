<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="country_add" enctype="multipart/form-data" action="<?php echo base_url('admin/zone/edit/'.$row['id']); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
						            <label for="name">Zone Name</label>
                              <input type="text" class="form-control" name="name" id="name" required placeholder="Enter zone name" value='<?php echo $row['name']; ?>' data-parsley-maxlength="50">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="page-title-box">
                              <div class="page-title-right">
                                 <input type="button" onclick="$('#country_add').submit();" class="btn btn-default pull-right mt-2 mb-2 mr-2" value="<?php echo __('Save'); ?>">
                               </div>
                            </div>
                         </div>
                      </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
