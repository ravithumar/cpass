<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo config('site_meta'); ?>">
    <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
    <title><?php echo config('site_title'); ?></title>
    <link href="<?php echo assets_css('bootstrap.min'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_css('icons'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assets_css('style'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo assets_js('modernizr.min'); ?>"></script>
  </head>
  <body class="">
    <div class="bg-white row">
      <div class="col-md-6" style="background-color: #f8f8f8;">
        <div class="wrapper-page">
          <div class="card-box" style="border:none;">
            <div class="panel-heading logo text-info">
              <img src="<?php echo base_url('assets/images/logo/logo.png') ?>" style="width:100%;height: 150px;object-fit: scale-down;padding: 25px;">
            </div>
            <?php $this->load->view('admin/includes/message'); ?>
            <?php echo form_open("auth/login");?>
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
            <div class="form-group ">
              <div class="col-12">
                <label class="form-control-label">Email</label>
                <input class="form-control" style="border: 2px solid #E3E3E3;padding: 25px;" type="email" required="" name="identity" id="identity" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <div class="col-12">
                <label class="form-control-label">Password</label>
                <input class="form-control" type="password" style="border: 2px solid #E3E3E3;padding: 25px;" id="password" required="" name="password" placeholder="Password">
                <a href="forgot_password" class="text-dark p-t-1"><i class="fa fa-lock m-r-5"></i> Forgot
                your password?</a>
              </div>
            </div>
            <div class="form-group text-center m-t-40">
              <div class="col-12">
                <button class="btn btn-default btn-block text-uppercase waves-effect waves-light"
                type="submit" style="background-color: #0C5B97;border: 1px solid #0C5B97;padding: 14px;">Log In
                </button>
              </div>
            </div>
            <div class="form-group m-t-30 m-b-0">
              <div class="col-12">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6" style="background-color:#f8f8f8;text-align: center;">
        <img src="<?php echo assets('images/login-image.png'); ?>" style="width: 58%;
        margin:25% auto;">
      </div>
    </div>
    
    
    
    <script>
    var resizefunc = [];
    </script>
    <!-- jQuery  -->
    <script src="<?php echo assets_js('jquery.min'); ?>"></script>
    <script src="<?php echo assets_js('popper.min'); ?>"></script><!-- Popper for Bootstrap -->
    <script src="<?php echo assets_js('bootstrap.min'); ?>"></script>
    <script src="<?php echo assets_js('detect'); ?>"></script>
    <script src="<?php echo assets_js('fastclick'); ?>"></script>
    <script src="<?php echo assets_js('jquery.slimscroll'); ?>"></script>
    <script src="<?php echo assets_js('jquery.blockUI'); ?>"></script>
    <script src="<?php echo assets_js('waves'); ?>"></script>
    <script src="<?php echo assets_js('wow.min'); ?>"></script>
    <script src="<?php echo assets_js('jquery.nicescroll'); ?>"></script>
    <script src="<?php echo assets_js('jquery.scrollTo.min'); ?>"></script>
    <script src="<?php echo assets_js('jquery.core'); ?>"></script>
    <script src="<?php echo assets_js('jquery.app'); ?>"></script>
    <script type="text/javascript" src="<?php echo assets('plugins/parsleyjs/parsley.min.js');?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('form').parsley();
    });
    </script>
    
  </body>
</html>