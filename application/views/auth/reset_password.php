<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>C - Pass</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-datepicker.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/flatpickr.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	<script type="text/javascript"> var BASE_URL = '<?php echo base_url(); ?>'; </script>
	
	<!-- <link rel="stylesheet" href="css/slick.css"> -->
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<section class="change-password"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	  <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">
			     <div class="back-close">
		 		   	<a href="<?=base_url('login')?>" class="back position-absolute">
                      <img src="<?=assets('web/images/back.svg')?>" alt="Back Arrow" class="img-fluid">
			        </a>			   
	 		     </div>
	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?=assets('web/images/c-pass-large-logo.png')?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">	 		  	 			
			    <div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
					<?php $this->load->view('message'); ?> 	
			    	<h2 class="font-34 t-bluetwo font-weight-bold mb-4">Change Password</h2>			 
					<form action="<?php echo base_url('auth/reset_password/').$code?>" method="post" accept-charset="utf-8">	
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">		
						<?php echo form_input($user_id);?>
						<?php echo form_hidden($csrf); ?>
						<input type="hidden" name="role" value="2">
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="new" class="mb-0 font500 font-13">New Password</label>
							<input type="password" class="form-control font-14" id="new" name="new" required="" data-parsley-minlength="8" data-parsley-minlength-message="Minimum 8 characters are required" data-parsley-required-message="Please Enter New Password" aria-describedby="fullname" placeholder="Enter New password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?=assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid">
							</a>
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="new_confirm" class="mb-0 font500 font-13">Confirm Password</label>
							<input type="password" class="form-control font-14" data-parsley-equalto = '#new' name="new_confirm" id="new_confirm" required="" aria-describedby="fullname" data-parsley-minlength="8" data-parsley-equalto-message="This value should be the same as new password." data-parsley-minlength-message="Minimum 8 characters are required" data-parsley-required-message="Please Enter Confirm Password" placeholder="Enter Confirm password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?=assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid">
							</a>
						</div>
						<div class="btn-wrap mt-4 mt-md-5 mb-3">
							<button type="submit" class="btn-green-lg border-r8 d-block font600" onclick="$('#forgot_password').submit();">Save</button>
						</div>
					</form>
			    </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap-datepicker.min.js');?>"></script>
<script src="<?php echo assets('web/js/popper.min.js');?>"></script>
<script src="<?php echo assets('v3/libs/parsleyjs/parsley.min.js');?>"></script>
<script src="<?php echo assets('web/js/custom.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {    
	$('form').parsley();
	$(document).on("click",".eye-click",function(){
		var closest = $(this).parent();
		
		closest.find(".password_checker").removeClass("eye-click");
		closest.find(".password_checker").addClass("eye-close");
		closest.find("input").attr("type","text");
		closest.find(".password_checker img").attr('src', "<?php echo assets('web/images/eye.png');?>");
 	});

	$(document).on("click",".eye-close",function(){
		var closest = $(this).parent();
		closest.find(".password_checker").removeClass("eye-close");
		closest.find(".password_checker").addClass("eye-click");
		closest.find("input").attr("type","password");
		closest.find(".password_checker img").attr('src', "<?php echo assets('web/images/eyes.svg');?>");
	});
});

</script>
</body>
</html>
<script type="text/javascript">
	$('.alert').delay(3000).fadeOut(300);
</script> 
