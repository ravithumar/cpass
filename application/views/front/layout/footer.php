<footer class="footer-section">
   <div class="footer-top">
      <div class="container pt-5 pb-4">
         <div class="row justify-content-center align-self-center">
            <div class="col-md-12 col-lg-12 ">
               <div class="row justify-content-center align-self-center">
                  <div class="col-md-5">
                     <div class="footer-logo">
                        <a href="index.html"> <img src="<?php echo assets('front/images/footer-logo.png'); ?>" class="img-fluid header-logo"></a>
                        <p class="wht-small-pera pt-3 ">Professional housecleaning services, you want to make sure that you find a team that can accommodate your schedule, and cleaning preferences.</p>
                        <div class="pt-4 social-link d-flex">
                           <ul class="d-flex">
                              <li class="pr-4"><a href="#"><img src="<?php echo assets('front/images/facebook.png'); ?>"></a></li>
                              <li class="pr-4"><a href="#"><img src="<?php echo assets('front/images/insta.png'); ?>"></a></li>
                              <li class="pr-4"><a href="#"><img src="<?php echo assets('front/images/twitter.png'); ?>"></a></li>
                              <li class="pr-4"><a href="#"><img src="<?php echo assets('front/images/youtube.png'); ?>"></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-7 padtopxs50">
                     <div class="row">
                        <div class="col-md-3 col-12 pr-md-0">
                           <p class="footer-title-font mb-2">Links</p>
                           <div class="footer-link-font">
                              <ul>
                                 <li><a href="<?php echo('home') ?>">Home</a></li>
                                 <li><a href="<?php echo('services') ?>">Services</a></li>
                                 <li><a href="<?php echo('about-us') ?>">About</a></li>
                                 <li><a href="<?php echo('contact-us') ?>">Contact Us</a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-4 col-12 padtopxs20">
                           <p class="footer-title-font mb-2">About</p>
                           <div class="footer-link-font">
                              <ul>
                                 <li><a href="<?php echo('privacy-policy') ?>">Terms & Conditions</a></li>
                                 <li><a href="<?php echo('terms-conditions') ?>">Privacy Policy</a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-md-5 col-12 padtopxs20">
                           <p class="footer-title-font mb-2">Our Office</p>
                           <div class="footer-link-font">
                              <p class="wht-small-pera"><span class="fontwbold">Singapur</span><br>
                                 5 Mattar Rd, Singapore<br>
                                 387713
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="footer-bottom border-top-lite">
      <div class="container pt-3 pb-3">
         <div class="col-md-12 text-center">
            <div class="row justify-content-center align-self-center">
               <p class="wht-small-pera mb-0">© 2021 HIRELY All rights reserved.</p>
            </div>
         </div>
      </div>
   </div>
</footer>
<!-- //footer section -->
<!-- partial -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
<!-- <script src="<?php echo assets('front/js/slick.js'); ?>" type="text/javascript"></script>   -->
<script src="<?php echo assets('front/js/custom.js'); ?>" type="text/javascript"></script>
<script src="<?php echo assets('front/js/jquery.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo assets('front/js/bootstrap.min.js'); ?>" type="text/javascript"></script>  
</body>
</html>
