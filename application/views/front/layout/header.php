<!DOCTYPE html>
<html lang="en" >
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Mobile Metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Hirely - Privacy Policy</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo assets('front/css/bootstrap.min.css')?>">
      <link rel="stylesheet" href="<?php echo assets('front/css/style.css');?>"> 
      <link rel="stylesheet" href="<?php echo assets('front/css/responsive.css');?>">            
      <!-- <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="css/responsive.css"> -->
      <link rel="preconnect" href="https://fonts.gstatic.com">
      <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   </head>
   <!-- menu section -->
   <header class="header-section">
      <nav class="navbar navbar-expand-lg navbar-light background-dark-pink fixed-top">
         <div class="container">
            <a href="index.html"><img src="<?php echo assets('front/images/header-logo.png'); ?>" class="img-fluid header-logo" id="navbardrop" alt="Hirely"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
               <div class="dropdown-divider"></div>
               <ul class="navbar-nav d-flex mt-2">
                  <li class="nav-item ">
                     <a class="nav-link" href="<?php echo('home') ?>">Home</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="<?php echo('services') ?>">Services</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="<?php echo('about-us') ?>">About</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="<?php echo('contact-us') ?>">Contact Us</a>
                  </li>
               </ul>
            </div>
            <div class="call-section" >
               <div class="call-box d-flex align-items-center">
                  <div class=""><img src="<?php echo assets('front/images/icon-call.png'); ?>" alt="phone"></div>
                  <div class="call-link-text pl-3 padleft7xs"><span class="font-sm-pink text-uppercase">Call us now:</span><br><a href="tel:123433123">+123 - 433 - 123 </a></div>
               </div>
            </div>
         </div>
      </nav>
   </header>