<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper about-bg">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtb150">
               <h1 class="text-center fontwebold">About Us</h1>
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- why choose section -->
   <div class="why-choose-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row pt-3">
            <div class="col-md-12">
               <h2 class="pb-5">Why Choose Us</h2>
            </div>
         </div>
         <div class="row pt-5">
            <div class="col-md-6 pr-5">
               <div class="row">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">Excepteur sint occaecat cupidatat non proident sunt iculpa qui officia deserunt mollit anim est. laborum sed perspiciatis unde omnis natus error sit voluptatem accusantium dolore mque laudantium totam rem aperiam.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">Excepteur sint occaecat cupidatat non proident sunt iculpa qui officia deserunt mollit anim est. laborum sed perspiciatis unde omnis natus error sit voluptatem accusantium dolore mque laudantium totam rem aperiam.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">Excepteur sint occaecat cupidatat non proident sunt iculpa qui officia deserunt mollit anim est. laborum sed perspiciatis unde omnis natus error sit voluptatem accusantium dolore mque laudantium totam rem aperiam.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/team-group.png'); ?>" class="img img-fluid" alt="team group">
            </div>
         </div>
      </div>
   </div>
   <!--//why choose section -->
   <!-- video section -->
   <div class="video-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row justify-content-center">
            <div class="col-md-8 px-md-0">
               <div class="hp-about-sciprep-video">
                  <div class="wrapper">
                     <img  id="video-cover" src="<?php echo assets('front/images/video-img.png'); ?>" alt="Video title">
                     <iframe id="player" width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                     <button id="play" class="play-btn">
                     <img src="<?php echo assets('front/'); ?>images/play-button-img.png" alt="">
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //video section -->
   <!-- right image left text section -->
   <div class="right-img-left-text-wrapper">
      <div class="container pt-5 padbot160 padtop0xs padbotxs50">
         <div class="row d-flex align-items-center row-reverse">
            <div class="col-md-6 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3 max-width500">Lorem Ipsum is simply dummy text of the</h2>
               <p class="max-width500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took   </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/person-img.png'); ?>" class="img img-fluid" alt="person-img">
            </div>
         </div>
      </div>
   </div>
   <!-- //right image left text section -->
   <!-- pink mobile app -->
   <div class="mobile-app background-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtbipad50">
               <div class="max-width400">
                  <h2 class="font-white fontwbold padtopxs50 font45">Get The App
                     Free&nbsp;From Here
                  </h2>
                  <p class="font-white pt-4">Task management is the process of  managing a
                     task through its life cycle. It involves planning.
                     Task management is the process.
                  </p>
                  <div class="row pt-3">
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">  Get the app on</span><br>
                              <span class="fontw600">Google Playstore</span>
                           </p>
                        </a>
                     </div>
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12 pl-4 padtopxs20 padleft15xs">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">Available On</span><br>
                              <span class="fontw600">App Store</span>
                           </p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/mobile-pink-app.png'); ?>" class="img img-fluid mtb85" alt="mobile app">
            </div>
         </div>
      </div>
   </div>
   <div class="padbot160"></div>
   <!-- //pink mobile app -->
   <!-- contact section -->
   <div class="contact-section background-grey">
      <div class="container padtb100">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <p class="small-pink-font text-uppercase mb-2">contact us</p>
               <h2 class="border-bottom-pink pb-3 max-width500">How Can I Help You?</h2>
               <p class="max-width450">Our service helps you live smarter, giving you time to focus on what's most important. Our skilled professionals go above and beyond on every job. </p>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="tel:123433123" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/phone-icon.png'); ?>" class="img img-fluid" alt="phone"> 
                        <p class="mb-0 pl-4 font18"> +123 - 433 - 123</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="mailto:hello@hirely.com" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/mail-icon.png'); ?>" class="img img-fluid" alt="mail"> 
                        <p class="mb-0 pl-4 font18"> hello@hirely.com</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4">
                  <div class="whtbox">
                     <div class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/map-icon.png'); ?>" class="img img-fluid" alt="map"> 
                        <p class="mb-0 pl-4 font18"> South Carolina - 25112</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="whtbox-form">
                  <form action="">
                     <div class="form-group">
                        <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Full name">
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control" id="" aria-describedby="emailHelp" placeholder="E-mail address">
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Subject">
                     </div>
                     <div class="form-group">
                        <textarea class="form-control pt-3" id="exampleFormControlTextarea1" rows="5" placeholder="Write us a messege..."></textarea>
                     </div>
                     <button type="submit" class="pink-button-big">Submit</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //contact section -->
</section>
<!-- //body section -->
