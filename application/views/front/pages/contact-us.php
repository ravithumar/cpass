<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper search-tablet-bg">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtb150">
               <h1 class="text-center fontwebold">Contact Us</h1>
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- contact section -->
   <div class="contact-section">
      <div class="container padtb100">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <p class="small-pink-font text-uppercase mb-2">contact us</p>
               <h2 class="border-bottom-pink pb-3 max-width500">How Can I Help You?</h2>
               <p class="max-width450">Our service helps you live smarter, giving you time to focus on what's most important. Our skilled professionals go above and beyond on every job. </p>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="tel:123433123" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/phone-icon.png'); ?>" class="img img-fluid" alt="phone"> 
                        <p class="mb-0 pl-4 font18"> +123 - 433 - 123</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="mailto:hello@hirely.com" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/mail-icon.png'); ?>" class="img img-fluid" alt="mail"> 
                        <p class="mb-0 pl-4 font18"> hello@hirely.com</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4">
                  <div class="whtbox">
                     <div class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/map-icon.png'); ?>" class="img img-fluid" alt="map"> 
                        <p class="mb-0 pl-4 font18"> South Carolina - 25112</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="whtbox-form">
                  <form action="">
                     <div class="form-group">
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full name">
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="E-mail address">
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Subject">
                     </div>
                     <div class="form-group">
                        <textarea class="form-control pt-3" id="exampleFormControlTextarea1" rows="5" placeholder="Write us a messege..."></textarea>
                     </div>
                     <button type="submit" class="pink-button-big">Submit</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //contact section -->
</section>
<!-- //body section -->
