<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper background-dark-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtopxs20 padtbipad50">
               <h1>Complete <span class="font-pink">Cleaning</span> And <span class="font-pink">Disinfection</span> For Your&nbsp;<span class="font-pink">Home</span></h1>
               <p class="pt-3 max-width500 line-height35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.
               </p>
               <div class="row pt-3">
                  <div class="col-md-6 col-lg-4 pr-md-0 col-10 padleft0pro">
                     <a href="#" class="blackbox d-flex align-items-center">
                        <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                        <p class="font13 pl-3 mb-0">
                           <span class="fontw300">  Get the app on</span><br>
                           <span class="fontw600">Google Playstore</span>
                        </p>
                     </a>
                  </div>
                  <div class="col-md-6 col-lg-4 pr-md-0 col-10 pl-md-4 padtopxs20">
                     <a href="#" class="blackbox d-flex align-items-center">
                        <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                        <p class="font13 pl-3 mb-0">
                           <span class="fontw300">Available On</span><br>
                           <span class="fontw600">App Store</span>
                        </p>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-6 padtopxs20">
               <img src="<?php echo assets('front/images/header-mobile-img.png'); ?>" class="img img-fluid" class="mobile app">
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- best ervices section -->
   <div class="best-services-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row">
            <div class="col-md-12">
               <p class="small-pink-font text-uppercase mb-0">best services</p>
               <h2 class="border-bottom-pink pb-3">What Our Services</h2>
               <div class="row">
                  <div class="col-md-6">
                     <p class="max-width500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.   </p>
                  </div>
                  <div class="col-md-6">
                     <div class="pink-button-box d-flex">
                        <a href="<?php echo ('services'); ?>"><button type="button" class="pink-button d-flex align-items-center">See More</button></a>
                     </div>
                  </div>
               </div>
               <div class="row pt-5">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/recurring-cleaning.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Recurring Cleaning</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Beauty Service</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/repairing-service.png'); ?>" class="" alt="Repairing Service">
                        <h4 class="pt-4">Repairing Service</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
               </div>
               <div class="row padtop33">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/doctor-service.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Doctor Service</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Beauty Service</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Beauty Service</h4>
                        <p class="max-width500 pt-3 line-height25">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been .</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--//best ervices section -->
   <!-- left image right text section -->
   <div class="left-img-right-text-wrapper">
      <div class="container pt-4 pb-5">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/cleaning-persons.png'); ?>" class="img img-fluid" alt="cleaning-persons">
            </div>
            <div class="col-md-6 pr-md-5 pl-lg-0 padtopxs20 padleft10ipad">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3">Lorem Ipsum is simply dummy text of the</h2>
               <p class="max-width500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took   </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //left image right text section -->
   <!-- right image left text section -->
   <div class="right-img-left-text-wrapper">
      <div class="container pt-5 padbot160 padtop0xs padbotxs50">
         <div class="row d-flex align-items-center row-reverse">
            <div class="col-md-6 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3 max-width500">Lorem Ipsum is simply dummy text of the</h2>
               <p class="max-width500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took   </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/person-img.png'); ?>" class="img img-fluid" alt="person-img">
            </div>
         </div>
      </div>
   </div>
   <!-- //right image left text section -->
   <!-- pink mobile app -->
   <div class="mobile-app background-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtbipad50">
               <div class="max-width400">
                  <h2 class="font-white fontwbold padtopxs50 font45">Get The App
                     Free&nbsp;From Here
                  </h2>
                  <p class="font-white pt-4">Task management is the process of  managing a
                     task through its life cycle. It involves planning.
                     Task management is the process.
                  </p>
                  <div class="row pt-3">
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">  Get the app on</span><br>
                              <span class="fontw600">Google Playstore</span>
                           </p>
                        </a>
                     </div>
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12 pl-4 padtopxs20 padleft15xs">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">Available On</span><br>
                              <span class="fontw600">App Store</span>
                           </p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/mobile-pink-app.png'); ?>" class="img img-fluid mtb85" alt="mobile app">
            </div>
         </div>
      </div>
   </div>
   <div class="padbot160"></div>
   <!-- //pink mobile app -->
   <!-- testimonial section -->
   <div class="testimonial">
      <div class="container pb-5">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3">What Our Clients' Say</h2>
               <p class="max-width400 line-height25">This is what clients have been saying after using the great service we do for clients.</p>
            </div>
         </div>
      </div>
   </div>
   <div class="testimonial-slider slider container-fluid">
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“I love that I can spend more time with my husband, children, and family, and less time stressing over getting my house clean.”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/braun-yes.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Nazmi Javier<br><span class="font14">Househusband</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/chintya-xin.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Chintya Xin<br><span class="font14">Housewife</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/nazmi-jaview.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Braun Yes<br><span class="font14">CEO Of Arcane</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/nazmi-jaview.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Braun Yes<br><span class="font14">CEO Of Arcane</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //testimonial section -->
   <!-- contact section -->
   <div class="contact-section background-grey">
      <div class="container padtb100">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <p class="small-pink-font text-uppercase mb-2">contact us</p>
               <h2 class="border-bottom-pink pb-3 max-width500">How Can I Help You?</h2>
               <p class="max-width450">Our service helps you live smarter, giving you time to focus on what's most important. Our skilled professionals go above and beyond on every job. </p>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="tel:123433123" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/phone-icon.png'); ?>" class="img img-fluid" alt="phone"> 
                        <p class="mb-0 pl-4 font18"> +123 - 433 - 123</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4 pb-3">
                  <div class="whtbox">
                     <a href="mailto:hello@hirely.com" class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/mail-icon.png'); ?>" class="img img-fluid" alt="mail"> 
                        <p class="mb-0 pl-4 font18"> hello@hirely.com</p>
                     </a>
                  </div>
               </div>
               <div class="pt-4">
                  <div class="whtbox">
                     <div class="d-flex align-items-center">
                        <img src="<?php echo assets('front/images/map-icon.png'); ?>" class="img img-fluid" alt="map"> 
                        <p class="mb-0 pl-4 font18"> South Carolina - 25112</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="whtbox-form">
                  <form action="">
                     <div class="form-group">
                        <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Full name">
                     </div>
                     <div class="form-group">
                        <input type="email" class="form-control" id="" aria-describedby="emailHelp" placeholder="E-mail address">
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" id="" aria-describedby="emailHelp" placeholder="Subject">
                     </div>
                     <div class="form-group">
                        <textarea class="form-control pt-3" id="exampleFormControlTextarea1" rows="5" placeholder="Write us a messege..."></textarea>
                     </div>
                     <button type="submit" class="pink-button-big">Submit</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //contact section -->
</section>
<!-- //body section -->
