<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="<?php echo base_url('hospital'); ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                </li>       
                
				<li>
                    <a href="<?php echo base_url('hospital/lab'); ?>" class="waves-effect"><i class="fas fa-plus-square"></i>  <span>Lab</span></a>
                </li> 
                <li>
                    <a href="<?php echo base_url('hospital/booking'); ?>" class="waves-effect"><i class="fas fa-book"></i>  <span>Bookings</span></a>
                </li> 
				
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<div class="content-page">
