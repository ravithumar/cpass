<!-- Start content -->
<div class="content">
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Dashboard</h4>
            <ol class="breadcrumb m-0">
               <li class="breadcrumb-item"><a href="javascript: void(0);">Welcome to COVID Hospital panel !</a></li>
            </ol>
         </div>
      </div>
   </div>
   <?php $this->load->view('hospital/includes/message'); ?>
   <div class="row">
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                     <i class="ti-truck text-info font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($user) ? $user : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Users'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($category) ? $category : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Category'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>      
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo rand(1,99); ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Bookings'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
   </div>
</div>
