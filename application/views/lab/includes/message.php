<?php
if($this->session->flashdata('error')!="") {
?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="line-height: 0.8;">×</button>
    <?php echo $this->session->flashdata('error');$this->session->unset_userdata('error'); ?>
</div>
<?php
}
?>
<?php
if($this->session->flashdata('success')!="") {
?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="line-height: 0.8;">×</button>
    <?php 
    echo $this->session->flashdata('success');
    $this->session->unset_userdata('success'); 
    ?>
</div>
<?php
}
?>
<?php
if($this->session->flashdata('info')!="") {
?>
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="line-height: 0.8;">×</button>
    <?php echo $this->session->flashdata('info');$this->session->unset_userdata('info'); ?>
</div>
<?php
}
?>
<?php
if($this->session->flashdata('warning')!="") {
?>
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="line-height: 0.8;">×</button>
    <?php echo $this->session->flashdata('warning');$this->session->unset_userdata('warning'); ?>
</div>
<?php
}
?>