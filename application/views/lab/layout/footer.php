<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                2015 - 2019 &copy;  All rights reserved.
            </div>
            
        </div>
    </div>
</footer>
</div>
</div>
<script src="<?php echo assets('v3/js/vendor.min.js');?>"></script>
 <script src="<?php echo assets('v3/libs/parsleyjs/parsley.min.js');?>"></script>
<script src="<?php echo assets('v3/js/app.min.js');?>"></script>
<script src="<?php echo assets('v3/js/custom.js');?>"></script>
<script src="<?php echo assets('plugins/select2/select2.full.min.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo assets('v3/js/sweetalert2@9.js'); ?>" type="text/javascript"></script>
<script src="<?php echo assets('plugins/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
<script src="<?php echo assets('plugins/magnific-popup/jquery.magnific-popup.js'); ?>"></script> 
<script>
</script>
<?php
if(isset($datatable) && $datatable)
{
$this->load->view('lab/includes/datatable');
}
?>
<script type="text/javascript" src="<?php echo assets('plugins/parsleyjs/parsley.min.js');?>"></script>
 <script type="text/javascript" src="<?php echo assets('v3/libs/magnific-popup/jquery.magnific-popup.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.select2_dropdown').select2();
    
	$('form').parsley();
    $(".image-popup").magnificPopup({
        type: "image",
        closeOnContentClick: !1,
        closeBtnInside: !1,
        mainClass: "mfp-with-zoom mfp-img-mobile",
        image: {
            verticalFit: !0,
            titleSrc: function(e) {
                return e.el.attr("title")
            }
        },
        gallery: {
            enabled: !0
        },
        zoom: {
            enabled: !0,
            duration: 300,
            opener: function(e) {
                return e.find("img")
            }
        }
    }), $(".filter-menu .filter-menu-item").click(function() {
        $(".filter-menu .filter-menu-item").removeClass("active"), $(this).addClass("active")
    }), $(function() {
        var e = "";
        $(".filter-menu-item").click(function() {
            e = $(this).attr("data-rel"), $(".filterable-content").fadeTo(100, 0), $(".filterable-content .filter-item").not("." + e).fadeOut().removeClass(""), setTimeout(function() {
                $("." + e).fadeIn().addClass(""), $(".filterable-content").fadeTo(300, 1)
            }, 300)
        })
    })
});
</script>
</body>
</html>