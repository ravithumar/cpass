<style type="text/css">

    .alert{

        width: 100%;

    }

    .alert p{

        margin-bottom: 0;

    }

</style>



<?php

if($this->session->flashdata('error')!="") {

?>

<div class="alert alert-danger">

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="mdi mdi-close text-danger"></i></button>

    <?php echo $this->session->flashdata('error'); ?>

</div>

<?php

}

?>

 

<?php

if($this->session->flashdata('success')!="") {

?>

<div class="alert alert-success">

    <button type="button" class="close theme-color" data-dismiss="alert" aria-hidden="true" ><i class="mdi mdi-close text-success"></i></button>

    <?php echo $this->session->flashdata('success'); ?>

</div>



<?php

}

?>



<?php

if($this->session->flashdata('message')!="") {

?>

<div class="alert alert-success">

    <button type="button" class="close theme-color" data-dismiss="alert" aria-hidden="true" ><i class="mdi mdi-close text-success"></i></button>

    <?php echo $this->session->flashdata('message'); ?>

</div>



<?php

}

?>



<?php

if($this->session->flashdata('info')!="") {

?>

<div class="alert alert-info">

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" ><i class="mdi mdi-close text-info"></i></button>

    <?php echo $this->session->flashdata('info'); ?>

</div>

<?php

}

?>



<?php

if($this->session->flashdata('warning')!="") {

?>

<div class="alert alert-warning">

    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" ><i class="mdi mdi-close text-warning"></i></button>

    <?php echo $this->session->flashdata('warning'); ?>

</div>

<?php

}

?>

<script type="text/javascript">

    $('.alert').delay(3000).fadeOut(300);

</script>