<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
         <title><?php echo config('site_title'); ?></title>
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
        <!-- Plugins css -->
        <link href="<?php echo assets('v3/libs/flatpickr/flatpickr.min.css');?>" rel="stylesheet" type="text/css" />
        <!-- Font awesome -->
        <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"rel="stylesheet">
        <!-- App css -->

        <link href="<?php echo assets('v3/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
        <link href="<?php echo assets('plugins/select2/select2.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo assets('v3/css/icons.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assets('v3/css/app.min.css');?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assets('v3/css/style.css');?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo assets('js/jquery.min.js');?>"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link rel="stylesheet" href="<?php echo assets('plugins/magnific-popup/magnific-popup.css');?>"/>
        <script type="text/javascript"> var BASE_URL = '<?php echo base_url(); ?>'; </script>
        <!-- css for calendar -->
         <link href="<?php echo assets('v3/libs/fullcalendar/fullcalendar.min.css');?>" rel="stylesheet" type="text/css" />
    </head>
								<?php

                                    $user_data = $this->comman->get_record_byid('users', $this->session->userdata('network')['user_id'], 'first_name, profile_picture');

                                    $user_data['profile_picture'] = is_null($user_data['profile_picture']) ? assets('images/avatar.png') : base_url($user_data['profile_picture']);

                                ?>
    <body>
        <body class="fixed-left">
            <div id="wrapper">
                <!-- Topbar Start -->
                <div class="navbar-custom">
                    <ul class="list-unstyled topnav-menu float-right mb-0">
                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="<?php echo $user_data['profile_picture']; ?>" alt="user-image" class="rounded-circle">
                                <span>Hello <?php echo $this->session->userdata('network')['name'];?></span>
								<span class="pro-user-name ml-1">
                                   <i class="mdi mdi-chevron-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow m-0">Welcome ! <?php echo $this->session->userdata('network')['name'];?> </h6>
                                </div>
                                <a href="<?php echo base_url('change-password/network'); ?>" class="dropdown-item notify-item">
								<i class="fa fa-briefcase"></i> <span><?php echo __('Change password') ?></span>
                                </a>
                                <a href="<?php echo base_url('auth/logout/network'); ?>" class="dropdown-item notify-item">
								<i class="fa fa-power-off"></i> <span>Logout</span>
                                </a>
                            </div>
                        </li>
                        
                    </ul>
                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="<?php echo base_url('network'); ?>" class="logo text-center">
                            <span class="logo-lg">
                                <span class="logo-lg-text-light">Covid</span>
                            </span>
                            <span class="logo-sm">
                                <span class="logo-lg-text-light">C</span>
                                
                            </span>
                        </a>
                    </div>
                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                        <li>
                            <button class="button-menu-mobile waves-effect waves-light">
                            <i class="fe-menu"></i>
                            </button>
                        </li>
                        
                    </ul>
                </div>
                <?php $this->load->view('network/layout/sidebar'); ?>
              