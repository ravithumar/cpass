<?php
    $folder = $this->uri->segment(1);
    $controller = $this->uri->segment(2);
    $clinic_class = ($folder == 'network' && $controller == 'clinic' ? 'active':'');
    $booking_class = ($folder == 'network' && $controller == 'booking' ? 'active':'');
    $user_class = ($folder == 'network' && $controller == 'user' ? 'active':'');
    $booking_history_class = ($folder == 'network' && $controller == 'booking-history' ? 'active':'');
    $slot_class = ($folder == 'network' && $controller == 'slot' ? 'active':'');
    $tracking_class = ($folder == 'network' && $controller == 'test-tracking' ? 'active':'');

    $blockslot_class = ($folder == 'network' && $controller == 'block-slot' ? 'active':'');
?>
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="<?php echo base_url('network'); ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>
                </li>       
                
				<li class="<?php echo $clinic_class; ?>">
                    <a href="<?php echo base_url('network/clinic'); ?>" class="waves-effect <?php echo $clinic_class; ?>"><i class="fas fa-plus-square"></i>  <span>Clinic</span></a>
                </li>
                <li class="<?php echo $booking_class; ?>">
                    <a href="<?php echo base_url('network/booking'); ?>" class="waves-effect <?php echo $booking_class; ?>"><i class="fas fa-book"></i>  <span>Bookings</span></a>
                </li>
				 <li class="<?php echo $booking_history_class; ?>">
                    <a href="<?php echo base_url('network/booking-history'); ?>" class="waves-effect <?php echo $booking_history_class; ?>"><i class="fas fa-book"></i>  <span>Booking History</span></a>
                </li> 
				<li class="<?php echo $user_class; ?>">
                    <a href="<?php echo base_url('network/user'); ?>" class="waves-effect <?php echo $user_class; ?>"><i class="fas fa-user"></i>  <span>Users</span></a>
                </li>
                <li class="<?php echo $slot_class; ?>">
                    <a href="<?php echo base_url('network/slot'); ?>" class="waves-effect <?php echo $slot_class; ?>"><i class="fas fa-book"></i>  <span>Slot</span></a>
                </li> 
                <li class="<?php echo $slot_class; ?>">
                    <a href="<?php echo base_url('network/block-slot'); ?>" class="waves-effect <?php echo $blockslot_class; ?>"><i class="fas fa-book"></i>  <span>Block Slot</span></a>
                </li>
                 <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-tasks"></i> <span>Fullfillments</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="<?php echo base_url('network/shipStation/getReports'); ?>" class="waves-effect">- <span>Initiate Fullfillment</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('network/shipStation/shipAwaiting'); ?>" class="waves-effect">- <span>Awaiting Shipment</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('network/shipStation/'); ?>" class="waves-effect">- <span>Shipped</span></a>
                        </li>
						<li>
                            <a href="<?php echo base_url('network/shipStation/carriers'); ?>" class="waves-effect">- <span>Carriers</span></a>
                        </li>
						<li>
                            <a href="<?php echo base_url('network/shipStation/products'); ?>" class="waves-effect">- <span>Products</span></a>
                        </li>
						<li>
                            <a href="<?php echo base_url('network/shipStation/stores'); ?>" class="waves-effect">- <span>Stores</span></a>
                        </li>
						<li>
                            <a href="<?php echo base_url('network/shipStation/shipments'); ?>" class="waves-effect">- <span>Shipments</span></a>
                        </li>
                        <!-- <li>
                            <a href="<?php echo base_url('network/shipStation/shipAwaiting'); ?>" class="waves-effect">- <span>Customers</span></a>
                        </li> -->
                    </ul>
                </li>
                <li class="<?php echo $tracking_class; ?>">
                    <a href="<?php echo base_url('network/test-tracking'); ?>" class="waves-effect <?php echo $tracking_class; ?>"><i class="fas fa-book"></i>  <span>Tracking</span></a>
                </li> 
                <!-- <li>
                    <a href="<?php echo base_url('network/lab'); ?>" class="waves-effect"><i class="fas fa-plus-square"></i>  <span>Lab</span></a>
                </li>  -->
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<div class="content-page">
