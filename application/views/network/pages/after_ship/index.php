<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);">Tracking</a></li>
                     <li class="breadcrumb-item active">Tracking</li>
                  </ol>
               </div>
               <h4 class="page-title">Tracking</h4>
         </div>
	  </div>
   </div>
   <div class="row">
		<div class="col-md-12">
         	<div class="card-box mt-3">
				<form method="get" action="">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>Courier</label>
								<select class="form-control" name="slug">
									<option value="fedex">FedEx</option>
									<option value="usps">USPS</option>
									<option value="abcustom">abcustom</option>
									<option value="alliedexpress">alliedexpress</option>
									<option value="amazon-fba-us">amazon-fba-us</option>
									<option value="apc-overnight-connum">apc-overnight-connum</option>
									<option value="ark-logistics">ark-logistics</option>
									<option value="arrowxl">arrowxl</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Tracking Number</label>
								<input type="text" class="form-control" name="tracking_number" placeholder="Tracking Number">
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Find</button>
						</div>
					</div>
				</form>
				<?php
				if(!empty($tracking_data) && isset($tracking_data['data']['tracking'])){ ?>
					<br/>
					<div class="row">
						<div class="col-md-12">
							<h3><?=$tracking_data['data']['tracking']['tag']?></h3>
						</div>
						<?php
						if(isset($tracking_data['data']['tracking']['checkpoints']) && !empty($tracking_data['data']['tracking']['checkpoints'])){ 
							foreach($tracking_data['data']['tracking']['checkpoints'] as $checkpoint){ ?>
								
										<div class="col-md-12">
											<p>
												<?=$checkpoint['message']?> <br/>
												<?php if(!empty($checkpoint['location'])){ ?>
													<i class="fa fa-map-marker" aria-hidden="true"></i> <?=$checkpoint['location']?><br/>
												<?php } ?>
												<i class="fa fa-calendar" aria-hidden="true"></i> <?=date('d/m/Y h:i A',strtotime($checkpoint['checkpoint_time']))?>
											</p><br/>
										</div>
								<?php 
							} 
						} ?>
					</div>
				<?php } ?>
         </div>
      </div>
   </div>
   <div class="row">
      
   </div>
   <!-- end row-->
</div>
