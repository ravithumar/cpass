<div class="container-fluid">
   <div class="row">
		<div class="col-md-12">
         	<div class="card-box">
				<?php
				if(!empty($tracking_data) && isset($tracking_data['data']['tracking'])){ ?>
					<div class="row">
						<div class="col-md-12">
							<h3><?=$tracking_data['data']['tracking']['tag']?></h3>
						</div>
						<?php
						if(isset($tracking_data['data']['tracking']['checkpoints']) && !empty($tracking_data['data']['tracking']['checkpoints'])){ 
							foreach($tracking_data['data']['tracking']['checkpoints'] as $checkpoint){ ?>
										<div class="col-md-12">
											<p>
												<?=$checkpoint['message']?> <br/>
												<?php if(!empty($checkpoint['location'])){ ?>
													<i class="fa fa-map-marker" aria-hidden="true"></i> <?=$checkpoint['location']?><br/>
												<?php } ?>
												<i class="fa fa-calendar" aria-hidden="true"></i> <?=date('d/m/Y h:i A',strtotime($checkpoint['checkpoint_time']))?>
											</p><br/>
										</div>
								<?php 
							} 
						}elseif ($tracking_data['data']['tracking']['tag'] == "Pending") { ?>
						<div class="col-md-12">
							<p>Awaiting updates from the courier</p>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
         </div>
      </div>
   </div>
</div>