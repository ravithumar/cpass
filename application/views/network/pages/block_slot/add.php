<style type="text/css">
.switch {position: relative;display: inline-block;width: 72px;height: 28px;}
.switch input {display:none;}
.slider {position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #ca2222;-webkit-transition: .4s;transition: .4s;}
.slider:before {position: absolute;content: "";height: 23px;width: 25px;left: 4px;bottom: 3px;background-color: white;-webkit-transition: .4s;transition: .4s;}
input:checked + .slider {background-color: #2ab934;}
input:focus + .slider {box-shadow: 0 0 1px #2196F3;}
input:checked + .slider:before {-webkit-transform: translateX(39px);-ms-transform: translateX(39px);transform: translateX(39px);}
/*------ ADDED CSS ---------*/
.on{display: none;}
.on{color: white;position: absolute;transform: translate(-50%,-50%);top: 50%;left: 30%;font-size: 10px;font-family: Verdana, sans-serif;}
.off{color: white;position: absolute;transform: translate(-50%,-50%);top: 50%;left: 70%;font-size: 10px;font-family: Verdana, sans-serif;}
input:checked+ .slider .on{display: block;}
input:checked + .slider .off{display: none;}
/*--------- END --------*/
/* Rounded sliders */
.slider.round {border-radius: 34px;}
.slider.round:before {border-radius: 50%;}
</style>
<?php
// get from to time
$from_time = DateTime::createFromFormat('H:i',$this->config->item("start_time"));
$to_time = DateTime::createFromFormat('H:i',$this->config->item("end_time"));

$s1_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot1_start_time_defualt"));
$s1_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot1_end_time_defualt"));

$s1_from_time_selected = $s1_from_time_selected->format('h:i A');
$s1_to_time_selected = $s1_to_time_selected->format('h:i A');


$s2_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot2_start_time_defualt"));
$s2_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot2_end_time_defualt"));

$s2_from_time_selected = $s2_from_time_selected->format('h:i A');
$s2_to_time_selected = $s2_to_time_selected->format('h:i A');

$s3_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot3_start_time_defualt"));
$s3_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot3_end_time_defualt"));

$s3_from_time_selected = $s3_from_time_selected->format('h:i A');
$s3_to_time_selected = $s3_to_time_selected->format('h:i A');

for($j = $from_time; $j <= $to_time;){
    $available_time[] = $j->format('h:i A');
    $j->add(new DateInterval('PT30M'));
}
?>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('network/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form enctype="multipart/form-data" action="<?php echo base_url('network/block-slot/add'); ?>" method="post" name="add_slot" id="add_slot" data-parsley-validate="">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                     	<div class="row form-group">
                            <div class="col-md-6">
                                <label>Clinic<span class="text-danger">*</span></label>
                                <select class="form-control" placeholder="Select Hospital" name="clinic_id" required="" data-parsley-required-message="Please Select Clinic">
                                	<option value="" selected="" disabled="">Select Clinic</option>
                                	<?php foreach($hospital as $key => $value) { ?>
                                		<option value="<?php echo $value['id']; ?>"><?php echo $value['company_name']; ?></option>
                                	<?php } ?>
                                </select>
                            </div> 
                        </div>
                        <div class="row">
                           <div class="col-lg-12 mb-3">
                                <div class="row">
                                    <div class="col-3 text-center"><h4>Start Date</h4></div>
                                    <div class="col-3 text-center"><h4>End Date</h4></div>
                                    <div class="col-3 text-center"><h4>Start Time</h4></div>
                                    <div class="col-3 text-center"><h4>End Time</h4></div>

                                    <div class="col-3 text-center"><input type="date" class="form-control" name="start_date[]"></div>
                                    <div class="col-3 text-center"><input type="date" class="form-control" name="end_date[]"></div>
                                    <div class="col-3 text-center"><input type="time" class="form-control" name="start_time[]"></div>
                                    <div class="col-3 text-center"><input type="time" class="form-control" name="end_time[]"></div>
                                </div>
                                <div class="paste_report_price"></div>
                                <div class="float-right">
                                    <a href="javascript:void(0)" class="btn btn-success pl-4 pr-4 mt-3 add_more_reports_price">+ ADD</a>
                                </div>
                                <!-- <div class="table-responsive b-0" data-pattern="priority-columns">
                                    <table class="table table-hover mb-0" id="table-working-hours">
                                        <tr class="text-center">
                                            <th>Start date</th>
                                            <th></th>
                                            <th>End date</th>
                                            <th></th>
                                            <th>Start Time</th>
                                            <th></th>
                                            <th>End Time</th>
                                            <th></th>
                                            <th>More</th>
                                        </tr>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="date" class="form-control" name="start_date">
                                                </td>
                                                <td class="text-center">
                                                    To
                                                </td>
                                                <td>
                                                     <input type="date" class="form-control" name="end_date">
                                                </td>
                                                <td class="text-center">
                                                    &
                                                </td>
                                                <td>
                                                     <input type="time" class="form-control" name="start_time">
                                                </td>
                                                <td class="text-center">
                                                    To
                                                </td>
                                                <td>
                                                    <input type="time" class="form-control" name="end_time">
                                                </td>
                                                <td class="text-center">
                                                        
                                                </td>
                                            </tr>
                                        </tbody>
                                        <div class="paste_report_price"></div>
                                    </table>
                                </div> -->
                                
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group text-right m-b-0">
                    <button class="btn btn-default pull-right mt-2 mb-2" type="submit">
                    Save
                    </button>
                </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div class="copy_report_price" style="display: none;">
    
        <div class="row mt-3">

            <div class="col-3 text-center"><input type="date" class="form-control" name="start_date[]"></div>
            <div class="col-3 text-center"><input type="date" class="form-control" name="end_date[]"></div>
            <div class="col-3 text-center"><input type="time" class="form-control" name="start_time[]"></div>
            <div class="col-3 text-center"><input type="time" class="form-control" name="end_time[]"></div>
            <div class="col-12 float-right">
                <a href="javascript:void(0)" class="btn btn-danger pl-4 pr-4 mt-3 remove_more_reports_price">-</a>
            </div>
        </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
<script src="<?php echo assets('v3/js/form.js'); ?>"></script>

<script>
    $(document).on("click",".add_more_reports_price",function(){
      var clone = $(".copy_report_price").children().clone();
      clone.find(".select2_dynamic_dropdown").select2({
         "width":"100%"
      });
      $(".paste_report_price").append(clone);
   });

   $(document).on("click",".remove_more_reports_price",function(){
      $(this).closest(".row").remove();
   });
</script>