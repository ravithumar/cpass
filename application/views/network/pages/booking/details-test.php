<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <li class="breadcrumb-item active">Booking Details</li>
                  </ol>
               </div>
               <h4 class="page-title">Booking Details</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <?php if (!empty($booking)) {
         foreach ($booking as $key => $value) { ?>
            <div class="col-lg-4 col-xl-4">
               <div class="card-box text-center">
                  <h4 class="bg-light p-2 mt-0 mb-3 text-left"><i class="fa fa-user mr-2"></i> Member Information</h4>
                  <?php 
                  if ($value['booking_member']['profile_picture'] != "") { ?>
                     <img src="<?php echo $value['booking_member']['profile_picture']; ?>" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                  <?php } ?>
                  <?php if ($value['booking_member']['full_name']!= "") { ?> 
                     <h4 class="mb-0"><?= $value['booking_member']['full_name']; ?></h4>
                  <?php } ?>
                  <div class="text-left mt-3">
                     <p class="text-muted mb-1 font-13"><strong>Purpose Type :</strong> <span class="ml-2"><?= $value['type']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Report Name :</strong> <span class="ml-2"><?= $value['report_name']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Slot Date :</strong> <span class="ml-2"><?= date('d-m-Y',strtotime($value['booking_slots']['date'])); ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Slot Time :</strong> <span class="ml-2"><?= $value['booking_slots']['slots']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Is Exempted? </strong> <span class="ml-2"><?= $value['is_excempted']; ?></span></p>
                     <?php if ($value['is_excempted'] =="yes") { ?> 
                        <p class="text-muted mb-1 font-13"><strong>Exemption category: </strong> <span class="ml-2"><?= $value['cat_name']; ?></span></p>
                     <?php } ?>
                     <p class="text-muted mb-1 font-13"><strong>Has Covid? </strong><span class="ml-2"><?= $value['has_covid']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Date Of Birth :</strong> <span class="ml-2 "><?= $value['booking_member']['date_of_birth']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Email Adress :</strong> <span class="ml-2 "><?= $value['booking_member']['email']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Phone Number :</strong> <span class="ml-2 "><?= $value['booking_member']['phone']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Home Address :</strong> <span class="ml-2"><?= $value['home_address']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Home Postal Code :</strong> <span class="ml-2"><?= $value['homepostal_code']; ?></span></p>
                     <p class="text-muted mb-1 font-13"><strong>Booking Status :</strong> <span class="ml-2"><?= $value['booking_slots']['booking_status']; ?></span></p>
                  </div>
               </div> <!-- end card-box -->
            </div> <!-- end col-->
         <?php }
      } ?>
   </div>
   <!-- end row-->
</div>