<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <!-- <li class="breadcrumb-item"><?= $booking->company_name; ?></li> -->
                     <li class="breadcrumb-item active">Booking Details</li>
                  </ol>
               </div>
               <h4 class="page-title">Booking Details</h4>
         </div>

         <div class="status_success_message mt-3">
                                      
         </div>
      </div>
   </div>
   <div class="row">
      <?php 
	  if (!empty($booking)) {
         foreach ($booking as $key => $value) { ?>
            <div class="col-lg-12 col-xl-12">
					<div class="card-box text-left">	
						<div class="row">
							<div class="col-md-5">
								<h4 class="bg-light p-2 mt-0 mb-3 text-left"><i class="fa fa-user mr-2"></i> Member Information</h4>
								<?php 
								if ($value['booking_member']['profile_picture'] != "") { ?>
									<img src="<?php echo $value['booking_member']['profile_picture']; ?>" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
									<?php } ?>
									<?php if ($value['booking_member']['full_name']!= "") { ?> 
									<h4 class="mb-0"><?= $value['booking_member']['full_name']; ?></h4>
									<?php } ?>
								
								<div class="text-left mt-3">
									
									<div class="border-bottom">
                              
                              <p class="mb-1"><strong><i class="fas fa-calendar"></i> Date Of Birth :</strong> <span class="ml-2 "><?= $value['booking_member']['date_of_birth']; ?></span></p>
                              <p class="mb-1"><strong><i class="fas fa-envelope"></i> Email Adress :</strong> <span class="ml-2 "><?= $value['booking_member']['email']; ?></span></p>
                              <p class="mb-1"><strong><i class="fas fa-phone"></i> Phone Number :</strong> <span class="ml-2 "><?= $value['booking_member']['phone']; ?></span></p>
                              <p class="mb-1"><strong><i class="fas fa-map-marker-alt"></i> Home Address :</strong> <span class="ml-2"><?= $value['home_address']; ?></span></p>
                              <p class="mb-1"><strong><i class="fas fa-map-pin"></i> Home Postal Code :</strong> <span class="ml-2"><?= $value['homepostal_code']; ?></span></p>

                           </div>

                           <div class="border-bottom mt-2">
                              
                              <p class="mb-1"><strong>Purpose Type :</strong> <span class="ml-2"><?= $value['type']; ?></span></p>
                               <p class="mb-1"><strong>place Type :</strong> <span class="ml-2"><?= $value['place_type']; ?></span></p>
                              <p class="mb-1"><strong>Report Name :</strong> <span class="ml-2"><?= $value['report_name']; ?></span></p>
                              <p class="mb-1"><strong>Slot Date :</strong> <span class="ml-2"><?= date('d-m-Y',strtotime($value['booking_slots']['date'])); ?></span></p>
                              <p class="mb-1"><strong>Slot Time :</strong> <span class="ml-2"><?= $value['booking_slots']['slots']; ?></span></p>

                              <?php if($value['vaccine_id']){ ?>
                                 <p class="mb-1"><strong>Vaccine name :</strong> <span class="ml-2"><?= $value['vaccine_name']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['vaccine_date']){ ?>
                                 <p class="mb-1"><strong>Vaccine Date :</strong> <span class="ml-2"><?= $value['vaccine_date']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['vaccine_status']){ ?>
                                 <p class="mb-1"><strong>Vaccine Status :</strong> <span class="ml-2"><?= $value['vaccine_status']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['report_status']){ ?>
                                 <p class="mb-1"><strong>Report Status :</strong> <span class="ml-2"><?= $value['report_status']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['shipping_date']){?>
                                 <p class="mb-1"><strong>Shipping Date :</strong> <span class="ml-2"><?= $value['shipping_date']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['shipping_address']){?>
                                 <p class="mb-1"><strong>Shipping Address :</strong> <span class="ml-2"><?= $value['shipping_address']; ?></span></p> 
                              <?php } ?>
                              <?php if($value['residency_flag'] == 1){?>
                                 <p class="mb-1"><strong>Residency flag :</strong> <span class="ml-2">UK resident</span></p> 
                              <?php }elseif($value['residency_flag'] == 2){ ?>
                                 <p class="mb-1"><strong>Residency flag :</strong> <span class="ml-2">Non UK resident</span></p> 
                              <?php } ?>

                              <?php if($value['uk_isolation_address']){?>
                                 <p class="mb-1"><strong>Uk isolation address :</strong> <span class="ml-2"><?= $value['uk_isolation_address']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['residency_postcode']){?>
                                 <p class="mb-1"><strong>Residency postcode :</strong> <span class="ml-2"><?= $value['residency_postcode']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['residency_address']){?>
                                 <p class="mb-1"><strong>Residency address :</strong> <span class="ml-2"><?= $value['residency_address']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['shipping_city']){?>
                                 <p class="mb-1"><strong>Shipping City :</strong> <span class="ml-2"><?= $value['shipping_city']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['shipping_state']){?>
                                 <p class="mb-1"><strong>Shipping State :</strong> <span class="ml-2"><?= $value['shipping_state']; ?></span></p> 
                              <?php } ?>

                              <p class="mb-1 font-13"><strong>Is Exempted? </strong> <span class="ml-2"><?= $value['is_excempted']; ?></span></p>
                              <?php if ($value['is_excempted'] =="yes") { ?> 
                                 <p class="mb-1"><strong>Exemption category: </strong> <span class="ml-2"><?= $value['cat_name']; ?></span></p>
                              <?php } ?>

                               <?php if($value['symptoms']){ ?>
                                 <p class="mb-1"><strong>Symptoms :</strong> <span class="ml-2"><?= $value['symptoms']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['excempted_cat']){ ?>
                                 <p class="mb-1"><strong>Excempted Category :</strong> <span class="ml-2"><?= $value['cat_name']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['relation_type']){ ?>
                                 <p class="mb-1"><strong>Relation Type :</strong> <span class="ml-2"><?= $value['relation_type']; ?></span></p> 
                              <?php } ?>

                              <p class="mb-1"><strong>Has Covid? </strong><span class="ml-2"><?= $value['has_covid']; ?></span></p>
                              

                              <?php if($value['is_medical_excempted']){ ?>
                                 <p class="mb-1"><strong>Medical excempted :</strong> <span class="ml-2"><?= $value['is_medical_excempted']; ?></span></p> 
                              <?php } ?>


                              <?php if($value['ref_number']){ ?>
                                 <p class="mb-1 font-13"><strong>Ref number :</strong> <span class="ml-2"><?= $value['ref_number']; ?></span></p> 
                              <?php } ?>
                           </div>


                           <div class="border-bottom mt-2 mb-2">
                              
                              <?php if($value['travel_destination']){ ?>
                                 <p class="mb-1"><strong>Travel Destination :</strong> <span class="ml-2"><?= $value['travel_destination']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['date_of_travel']){ ?>
                                 <p class="mb-1"><strong>Travel Date :</strong> <span class="ml-2"><?= $value['date_of_travel']; ?></span></p> 
                              <?php } ?>

                               <?php if($value['departure_date']){ ?>
                                 <p class="mb-1"><strong>Departure Date :</strong> <span class="ml-2"><?= $value['departure_date']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['departure_time']){ ?>
                                 <p class="mb-1"><strong>Departure Time :</strong> <span class="ml-2"><?= $value['departure_time']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['departure_country']){ ?>
                                 <p class="mb-1"><strong>Departure Country :</strong> <span class="ml-2"><?= $value['departure_country']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['destination_country']){ ?>
                                 <p class="mb-1"><strong>Destination Country :</strong> <span class="ml-2"><?= $value['destination_country']; ?></span></p> 
                              <?php } ?>
                          
                              <?php if($value['return_country']){ ?>
                                 <p class="mb-1"><strong>Return Country :</strong> <span class="ml-2"><?= $value['return_country']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['is_visiting_country']){ ?>
                                 <p class="mb-1"><strong>Is_visiting Country :</strong> <span class="ml-2"><?= $value['is_visiting_country']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['visiting_country']){ ?>
                                 <p class="mb-1"><strong>visiting Country :</strong> <span class="ml-2"><?= $value['visiting_country']; ?></span></p> 
                              <?php } ?>
                     
                              <?php if($value['second_report_id']){ ?>
                                 <p class="mb-1"><strong>Secound report id :</strong> <span class="ml-2"><?= $value['report_name']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['transport_type'] == '1'){ ?>
                                 <p class="mb-1"><strong>Transport Type :</strong> <span class="ml-2">Flight</span></p> 
                              <?php }elseif($value['transport_type'] == '2'){ ?>
                                 <p class="mb-1"><strong>Transport Type :</strong> <span class="ml-2">Vessel</span></p> 
                              <?php }elseif($value['transport_type'] == '3'){ ?>
                                 <p class="mb-1"><strong>Transport Type :</strong> <span class="ml-2">Train</span></p> 
                              <?php } ?>

                              <?php if($value['transport_number']){ ?>
                                 <p class="mb-1"><strong>Transport Number :</strong> <span class="ml-2"><?= $value['transport_number']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['pickup_date']){ ?>
                                 <p class="mb-1"><strong>Pickup Date :</strong> <span class="ml-2"><?= $value['pickup_date']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['return_date']){ ?>
                                 <p class="mb-1"><strong>Return Date :</strong> <span class="ml-2"><?= $value['return_date']; ?></span></p> 
                              <?php } ?>

                              <?php if($value['return_time']){ ?>
                                 <p class="mb-1"><strong>Return Time :</strong> <span class="ml-2"><?= $value['return_time']; ?></span></p> 
                              <?php } ?>

                           </div>

                           <p class="mb-1"><strong><i class="fas fa-check"></i> Booking Status :</strong> <span class="ml-2"><label class="badge badge-info"><?= strtoupper($value['booking_slots']['booking_status']); ?></label></span></p>

									<p class="mb-1 mt-2"><strong>Result Status :</strong></p>
									<span class="ml-2">
										<div class="row col-md-12 p-0">
											<div class="col-md-6">
												<select class="form-control booking_status" data-id="<?= $value['id']?>" placeholder="Result awaited">
													<option value="" disabled="" selected="">Result awaited</option>
													<option value="positive" <?= ($value['report_status'] == 'positive'?'selected':''); ?>>Positive Result</option>
													<option value="negative" <?= ($value['report_status'] == 'negative'?'selected':''); ?>>Negative Result</option>
																<option value="void" <?= ($value['report_status'] == 'void'?'selected':''); ?>>Void Result</option>
													<option value="invaild" <?= ($value['report_status'] == 'invaild'?'selected':''); ?>>Invaild Result</option>
												</select>
											</div>
											<input type="hidden" id="status_val_<?= $value['id']?>" value="<?= $value['report_status']; ?>">
											<input type="hidden" id="booking_id_val_<?= $value['id']?>" value="<?= $value['id']?>">
											<div class="col-md-6">
												<button type="button" class="btn btn-info submit_booking_status" data-id="<?= $value['id']?>"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
											</div>
										</div>
									</span>
									<!-- <?php 
									if (isset($value['booking_member']['report_file'])) {
										if ($value['booking_member']['report_file'] !="") { ?>
											<a target="_blank" class="btn btn-success mt-3" href="<?php echo $value['booking_member']['report_file']; ?>">Download Certificate</a><?php 
										}
									} ?> -->

									<div class="row">

                              <?php if($value['report_file']){ ?>
                                 <div class="col-sm-3">
                                    <a href="<?= $value['report_file']; ?>" class="btn btn-sm btn-success mt-1" target="_blank">Certificate</a> 
                                 </div>
                              <?php } ?>

                              <?php if($value['result_file_path']){ ?>
                                 <div class="col-sm-3">
                                    <a href="<?= $value['result_file_path']; ?>" class="btn btn-sm btn-success mt-1" target="_blank">Kit Image</a> 
                                 </div>
                              <?php } ?>

                              <?php if($value['plf_document']){ ?>
                                 <div class="col-sm-3">
                                    <a href="<?= $value['plf_document']; ?>" class="btn btn-sm btn-success mt-1" target="_blank">Plf Dosc</a> 
                                 </div>
                              <?php } ?>

                              <?php if($value['proof_document']){ ?>
                                 <div class="col-sm-3">
                                    <a href="<?= $value['proof_document']; ?>" class="btn btn-sm btn-success mt-1" target="_blank">Proof Dosc</a> 
                                 </div>   
                              <?php } ?>

                              <?php if($value['shipstation_label']){ ?>
                                 <div class="col-sm-3">
                                    <a href="<?= $value['shipstation_label']; ?>" class="btn btn-sm btn-success mt-1" target="_blank">View Label</a> 
                                 </div>
                              <?php } ?>

                           </div>
								</div>
							</div>
							<?php
							if(!empty($value['booking_history'])){ ?>
								<div class="col-md-7">
									<h4 class="bg-light p-2 mt-0 mb-3 text-left"><i class="fa fa-book mr-2"></i> Booking History</h4>
									<div class="table-responsive">
										<table class="table table-striped table-bordered booking_history_datatable">
												<thead>
														<tr>
															<td>#</td>
															<td>Purpose of Type</td>
															<td>Report Type</td>
															<td>Test Result</td>
															<td>Test Date</td>
															<td>Clinic Name</td>
															<td>Download Certificate</td>
															<td>Actions</td>
														</tr>
												</thead>
												<tbody>
														<?php foreach($value['booking_history'] as $booking_history_row){ ?>
															<tr>
																<td><?=$booking_history_row['booking_id']?></td>
																<td><?=ucfirst($booking_history_row['purpose'])?></td>
																<td><?=$booking_history_row['report_name']?></td>
																<td>
																	<?php
																	if(empty($booking_history_row['report_status'])){ ?>
																		<span class="badge badge-secondary cursor-pointer font-15 status_280">Result awaited</span>
																	<?php } else if($booking_history_row['report_status'] == "positive"){ ?>
																		<span class="badge badge-success cursor-pointer font-15 status_280">Positive Result</span><?php 
																	} else if($booking_history_row['report_status'] == "negative"){ ?>
																		<span class="badge badge-danger cursor-pointer font-15 status_280">Negative Result</span><?php 
																	} else{ ?>
																		<span class="badge badge-warning cursor-pointer font-15 status_280"><?= ucfirst($booking_history_row['report_status']) ?></span><?php 
																	} ?>
																</td>
																<td>
																	<?php
																	if($booking_history_row['purpose'] == "international-arrival"){
																		if(!empty($booking_history_row['pickup_date']) && $booking_history_row['shipping_method'] == 1){
																			echo (!empty($booking_history_row['pickup_date']))?date('d/m/Y',strtotime($booking_history_row['pickup_date'])):'';
																		}else{
																			echo (!empty($booking_history_row['shipping_date']))?date('d/m/Y',strtotime($booking_history_row['shipping_date'])):'';
																		}
																	}else{
																		echo !empty($booking_history_row['slot_date'])?date('d/m/Y',strtotime($booking_history_row['slot_date'])):''.' '.$booking_history_row['slot_time'];
																	}
																	?>
																</td>
																<td><?=$booking_history_row['company_name']?></td>
																<td>
																	<?php
																	if(!empty($booking_history_row['report_file'])){
																		echo '<a target="_blank" href="' .$booking_history_row['report_file']. '" >Download Certificate</a>';
																	}
																	?>
																</td>
																<td>
																	<a href="<?=base_url('network/booking/details/'.$booking_history_row['booking_id'])?>" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View"><i class="la la-eye "></i></a>
																</td>
															</tr><?php 
														} ?>
												</tbody>
										</table>
									</div>
								</div><?php 
							} ?>
						</div>	
						
               </div> 
            </div> 

         <?php }
      } ?>
   </div>
   <!-- end row-->
</div>
<script type="text/javascript">
   $(document).ready(function () {
      $(document).on("change",".booking_status",function(){
         var status = $(this).val();
         var booking_id = $(this).data("id");
         
         $('#status_val_'+booking_id).val(status);
      });
      $(document).on("click",".submit_booking_status",function(event){
      var id = $(this).data("id");
      var book_member_id = $('#booking_id_val_'+id).val();
      var status = $('#status_val_'+id).val();
      if(status != '')
      {
         $.ajax({
            url: "<?=base_url()?>network/change-booking-detail-status",
            method: "POST",
            data: {
               status:status,
               book_member_id:book_member_id,
               csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>'
            },
            dataType: "json",
            success: function(returnData) {
               // console.log(returnData);
               if(returnData.status == true)
               {
                  $(".status_success_message").html('<div class="alert alert-success" role="alert"> Status has been changed.</div>');
               }
               else
               {
                 $(".status_success_message").html('<div class="alert alert-danger" role="alert"> Status has been not changed.</div>');
               }
               setTimeout(function(){
                  $(".status_success_message").html('');
               },5000);
            }
         });
      }
      else
      {
         $(".status_success_message").html('<div class="alert alert-danger" role="alert"> Please select status.</div>');
         setTimeout(function(){
                  $(".status_success_message").html('');
               },5000);
      } 
      });
   });
</script>
