<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('network/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form id="report_type_add" enctype="multipart/form-data" action="<?php echo base_url('network/booking/edit/'.$booking_id); ?>" method="post">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">   
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <?php
                        if(!empty($booking)){
                           foreach($booking as $booking_row){ ?>
                              <input type="hidden" class="form-control " name="id[]" value="<?php echo $booking_row['booking_slots']['id']; ?>">
                              <div class="row">
                                 <div class="form-group col-6">
                                    <label><?=$booking_row['booking_member']['full_name']?></label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group col-6">
                                    <label class="form-control-label"><?php echo __('Slot Date'); ?></label>
                                    <input type="date" class="form-control " name="slot_date[]" id="slot_date" data-parsley-required-message="Please Select Slot Date" required="" value="<?php echo $booking_row['booking_slots']['date']; ?>" placeholder="Please Select Slot Date">
                                 </div>
                                 <div class="form-group col-6">
                                    <label class="form-control-label"><?php echo __('Slot Time'); ?></label>
                                    <input type="time" class="form-control" required="" name="slot[]" data-parsley-required-message="Please Enter Slot Time"  value="<?php echo $booking_row['booking_slots']['slots']; ?>" placeholder="Please Enter Slot Time"> 
                                 </div>
                              </div>
                              <hr/><?php 
                           } 
                        } ?>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#report_type_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
