<style type="text/css" src="<?php echo assets('plugins/flatpickr/flatpickr.min.css');?>"></style>
<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>

<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 my-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <form method="POST" id="filter_form" action="<?php echo base_url('network/booking') ?>" class="row col-md-12 mb-2">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">

            <div class="col-md-2">
                 <div class="form-group mt-2">
                    <div class="form-group d-flex align-items-center m-0 flex-wrap choose-label-block" >
                        <select class="form-control" id="" name="filter_value" parsley-trigger="change" >
                            <option value=""><?php echo __('All Status'); ?></option>
                            <option value="pending">Pending</option>
                            <option value="success">Success</option>
                            <option value="in-process">In Process</option>
                            <option value="cancelled">Cancelled</option>
                            <option value="completed">Completed</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                 <div class="form-group mt-2">
                    <div class="form-group d-flex align-items-center m-0 flex-wrap choose-label-block" >
                        <input type="text" name="filter_date" class="form-control flatpickr-input date-picker" placeholder="select date" >
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group mt-2">
                    <div class="form-group d-flex align-items-center">
                        <button type="submit" class="btn btn-success waves-effect waves-light ml-1" id="booking-filter"> <i class="fa fa-search" aria-hidden="true"></i></button>
                        <a href="javascript:void(0);"  class="btn btn-danger refresh waves-effect waves-light ml-1" ><i class="fa fa-refresh" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </form>
         <div class="">
            <?php echo $this->datatables->generate(); ?>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo assets('js/custom/admin/CoreClass.js');?>"></script>
<script type="text/javascript" src="<?php echo assets('plugins/flatpickr/flatpickr.min.js');?>"></script>
<script type="text/javascript">
   flatpickr($('.date-picker'));
   var url = BASE_URL+`network/booking`;
   CoreClass.filter("#booking",url,true);
   CoreClass.reset("#booking",url,true);
</script>
