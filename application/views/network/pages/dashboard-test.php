<!-- Start content -->
<div class="content">
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Dashboard</h4>
            <ol class="breadcrumb m-0">
               <li class="breadcrumb-item"><a href="javascript: void(0);">Welcome to COVID Network panel 1!</a></li>
            </ol>
         </div>
      </div>
   </div>
   <?php $this->load->view('network/includes/message'); ?>
   <div class="row">
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                     <i class="ti-truck text-info font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($user) ? $user : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Users'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($category) ? $category : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Category'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>      
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo rand(1,99); ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Bookings'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
                  <!-- <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                     <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                     <li class="breadcrumb-item active">Calendar</li>
                  </ol> -->
            </div>
            <h4 class="page-title mb-2">Booking Calendar</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">

         <div class="card">
            <div class="card-body">
                  <div class="row">
                     <div class="col-lg-12">
                        <div id="calendar"></div>
                     </div> <!-- end col -->

                  </div>  <!-- end row -->
            </div> <!-- end card body-->
         </div> <!-- end card -->

         <!-- Add New Event MODAL -->
         <div class="modal fade" id="event-modal" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Add New Booking</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                     </div>
                     <div class="text-right p-3">
                        <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success save-event">Create Booking</button>
                        <button type="button" class="btn btn-danger delete-event" data-dismiss="modal">Delete</button>
                     </div>
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
         <!-- end modal-->

         <!-- Modal Add Category -->
         <div class="modal fade" id="add-category" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Add a category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                        <form>
                              <div class="form-group">
                                 <label class="control-label">Category Name</label>
                                 <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name"/>
                              </div>
                              <div class="form-group">
                                 <label class="control-label">Choose Category Color</label>
                                 <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                    <option value="primary">Primary</option>
                                    <option value="success">Success</option>
                                    <option value="danger">Danger</option>
                                    <option value="info">Info</option>
                                    <option value="warning">Warning</option>
                                    <option value="dark">Dark</option>
                                 </select>
                              </div>

                        </form>
                        <div class="text-right pt-2">
                              <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary ml-1   save-category" data-dismiss="modal">Save</button>
                        </div>

                     </div> <!-- end modal-body-->
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
         <!-- end modal-->
         <div class="modal fade" id="showData" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Booking Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                              <div class="form-group">
                              
                              <div class="status_success_message">
                                   
                              </div>
                                 <p>Referance Id: <span id="referance_id"></span></p>
                                 <p>Referance Member Id: <span id="referance_member_id"></span></p>
                                 <p>Test Name: <span id="name"></span></p>
                                 <p>User Name: <span id="user_name"></span></p>
                                 <p>
                                    Slot date: <span id="start_date"></span> 
                                    <!-- <select class="form-control booking_status" data-id="" style="width: 50%;float:right;margin-top:-6px">
                                          <option value="success">Booking Completed</option>
                                          <option value="in-process">In-Fulfillment</option>
                                          <option value="pending">Pending</option>
                                          <option value="being_shipped">Being Shipped </option>
                                          <option value="shipped">Shipped</option>
                                          <option value="in_transit">In-transit</option>
                                          <option value="delivered">Delivered</option>
                                          <option value="processing_by_laboratory">Processing by Laboratory</option>
                                          <option value="processed_by_laboratory">Processed by Laboratory</option>
                                          <option value="result_awaited">Result awaited</option>
                                          <option value="positive_result">Positive Result</option>
                                          <option value="negative_result">Negative Result</option>
                                    </select> -->
                                  </p>
                                 <p>Slot time: <span id="start_time"></span></p>
                                 <p>Phone: <span id="phone"></span></p>
                                 <p>Email: <span id="email"></span></p>
                                 <p>Paid: <span id="paid"></span> <label class="badge badge-info" id="payment_status"></label> </p>
                                 <p>Gender: <span id="gender"></span></p>
                                 <p>Date of Birth: <span id="date_of_birth"></span></p>
                                 <p>Address: <span id="address"></span></p>
                                 <p>Test Result: 
                                  <select class="form-control booking_status" data-id="" style="width: 50%;margin-top:6px" placeholder="Result awaited">
                                       <option value="" disabled="" selected="">Result awaited</option>
                                       <option value="positive">Positive Result</option>
                                       <option value="negative">Negative Result</option>
                                    </select>
                                    <input type="hidden" id="status_val" value="">
                                    <input type="hidden" id="booking_id_val" value="">
                                  </p>
                              </div>
                              
                        </form>
                        <div class="text-right pt-2">
                              <a href="" id="booking_detail_link" class="text-info"><button type="button" class="btn btn-warning">Details</button></a>
                              <button type="button" class="btn btn-info submit_booking_status">Save</button>
                              <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                        </div>

                     </div> <!-- end modal-body-->
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
      </div>
      <!-- end col-12 -->
</div> <!-- end row -->   
</div>
<script>

$(document).ready(function () {
    var base_url = '<?php echo base_url().'network/booking/details/'; ?>';
    var calendar = $('#calendar').fullCalendar({
      header: {
         left: 'prev,next today',
         center: 'title',
         right: 'month,basicWeek,basicDay'
      },
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      allDay:false,
      displayEventTime: true,
      eventLimit: 10,
       events:JSON.parse(`<?php echo json_encode($events); ?>`),
        eventRender: function (event, element, view) {

        },
        eventClick: function(event) {
           // alert('Event Start Time : ' + event.start_time+' - Event End Time : '+ event.end_time+' -  Booking Status : '+event.booking_status);
                console.log(event); // Writes "1"
                $(".status_success_message").html('');
            var dateObj = new Date(event.start);                

           document.getElementById('referance_id').textContent = event.title;
           document.getElementById('referance_member_id').textContent = event.reference_member_id;
           document.getElementById('start_date').textContent = dateObj.getDate()+ '-' +dateObj.getMonth()+'-'+dateObj.getFullYear();
           document.getElementById('start_time').textContent = event.slot_time;
           document.getElementById('email').textContent = event.email;
           document.getElementById('phone').textContent = event.phone;
           document.getElementById('name').textContent = event.report_name;
           document.getElementById('user_name').textContent = event.username;
           //document.getElementById('booking_status').textContent = event.booking_status;
           if(event.result_status == 'positive' || event.result_status == 'negative')
           {
              $(".booking_status").val(event.result_status);
              $('#status_val').val(event.result_status);
           }
           // console.log(event.booking_status);
           $(".booking_status").attr("data-id",event.booking_id);
           $('#booking_id_val').val(event.booking_id);
           
           document.getElementById('paid').textContent = event.total_price;
           document.getElementById('payment_status').textContent = event.payment_status;
           document.getElementById('gender').textContent = event.gender;
           document.getElementById('date_of_birth').textContent = event.date_of_birth;
           document.getElementById('address').textContent = event.address;
           // document.getElementById('booking_detail_link').textContent = event.address;
           var a = document.getElementById('booking_detail_link'); //or grab it by tagname etc
           a.href = base_url + event.booking_id;
           $('#showData').modal('show');
        },
        eventMouseover: function(event, jsEvent, view) {
          var dateObj = new Date(event.start);
          // $(this).append('<div id="'+event.booking_id+'" class="hover-end"><p class="mb-1">Bookingid:'+ event.title +'</p><p class="mb-1">Name:'+ event.username +'</p><p class="mb-1">Date: '+ dateObj.getDate()+ '-' +dateObj.getMonth()+'-'+dateObj.getFullYear() +' </p><p class="mb-1">Phone:'+ event.phone +'</p><p class="mb-1">Test Name:'+ event.report_name +'</p></div>');
        },

        eventMouseout: function(event, jsEvent, view) {
            $('#'+event.booking_id).remove();
        },
        eventLimitClick: function(cellInfo, jsEvent, view) {
           window.location.href = "<?=base_url('network/booking')?>";
        },

    });

    $(document).on("change",".booking_status",function(){
      var status = $(this).val();
      var booking_id = $(this).data("id");
      
      $('#status_val').val(status);
      $('#booking_id_val').val(booking_id);
    });

    $(document).on("click",".submit_booking_status",function(event){
      var status = $('#status_val').val();
      var booking_id = $('#booking_id_val').val();
         event.preventDefault();
         $.ajax({
            url: "<?=base_url()?>network/change-booking-status",
            method: "POST",
            data: {
               status:status,
               booking_id:booking_id,
               csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>'
            },
            dataType: "json",
            success: function(data) {
               $(".status_success_message").html('<div class="alert alert-success" role="alert"> Status has been changed.</div>');
               // setTimeout(function(){
               //    $(".status_success_message").html('');
               // },45000);
            }
         });
    });
});

</script>
<style type="text/css">
  .hover-end {
    padding: 5px 0px;
    margin: 0 auto;
    font-size: 13px;
    text-align: left;
    position: absolute;
    width: 100%;
    opacity: 1;
    background: #fff;
    z-index: 5;
   /* height: 100px;*/
    top: 18px;
    color: #000;
    left: 0;
    right: 0;
}

</style>