<!-- Start content -->
<div class="content">
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Dashboard</h4>
            <ol class="breadcrumb m-0">
               <li class="breadcrumb-item"><a href="javascript: void(0);">Welcome to COVID Network panel 1!</a></li>
            </ol>
         </div>
      </div>
   </div>
   <?php $this->load->view('network/includes/message'); ?>
   <div class="row">
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                     <i class="ti-truck text-info font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($user) ? $user : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Users'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($category) ? $category : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Category'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>      
      <div class="col-md-6 col-xl-4">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-mobile text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo rand(1,99); ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Total Bookings'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
                  <!-- <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                     <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                     <li class="breadcrumb-item active">Calendar</li>
                  </ol> -->
            </div>
            <h4 class="page-title mb-2">Booking Calendar</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">

         <div class="card">
            <div class="card-body">
                  <div class="row">
                     <div class="col-lg-12">
                        <div id="calendar"></div>
                     </div> <!-- end col -->

                  </div>  <!-- end row -->
            </div> <!-- end card body-->
         </div> <!-- end card -->

         <!-- Add New Event MODAL -->
         <div class="modal fade" id="event-modal" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Add New Booking</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                     </div>
                     <div class="text-right p-3">
                        <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success save-event">Create Booking</button>
                        <button type="button" class="btn btn-danger delete-event" data-dismiss="modal">Delete</button>
                     </div>
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
         <!-- end modal-->

         <!-- Modal Add Category -->
         <div class="modal fade" id="add-category" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Add a category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                        <form>
                              <div class="form-group">
                                 <label class="control-label">Category Name</label>
                                 <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name"/>
                              </div>
                              <div class="form-group">
                                 <label class="control-label">Choose Category Color</label>
                                 <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                    <option value="primary">Primary</option>
                                    <option value="success">Success</option>
                                    <option value="danger">Danger</option>
                                    <option value="info">Info</option>
                                    <option value="warning">Warning</option>
                                    <option value="dark">Dark</option>
                                 </select>
                              </div>

                        </form>
                        <div class="text-right pt-2">
                              <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary ml-1   save-category" data-dismiss="modal">Save</button>
                        </div>

                     </div> <!-- end modal-body-->
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
         <!-- end modal-->
         <div class="modal fade" id="showData" tabindex="-1">
            <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h4 class="modal-title">Booking Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                     </div> 
                     <div class="modal-body p-3">
                              <div class="form-group">
                              
                              <div class="status_success_message">
                                   
                              </div>

                              <div id="" class="row"> 
                                 <div class="col-md-12"> 
                                    <h3 class="mb-1">Bookingid: <span id="referance_id"></span></h3> 
                                    <div class="border-bottom"> 
                                       <p>Referance Member Id: <span id="referance_member_id"></span></p>
                                       <p class="mb-1"><b>Name: </b><span id="user_name"></span></p> 
                                       <p class="mb-1"><b>Email: </b><span id="email"></span></p> 
                                       <p class="mb-1"><b>Phone: </b><span id="phone"></span></p> 
                                    </div> 
                                    <div class="border-bottom mt-2"> 
                                       <p class="mb-1"><b>Test Name: </b><span id="name"></span></p>    
                                       <p  class="mb-1" style="font-weight: bold;"><span class="slot_date_label">Slot date:</span> <span id="start_date"></span></p>
                                       <p class="mb-1" style="font-weight: bold;" class="slot_time_label">Slot time: <span id="start_time"></span></p>
                                       <p class="mb-1" style="font-weight: bold;" >Paid: <span id="paid"></span> <label class="badge badge-info" id="payment_status"></label> </p>
                                    </div> 
                                   <!--  <p class="mb-1 mt-2"><i class="fas fa-calendar"></i> Date of Birth: <span id="date_of_birth"></span></p> 
                                    <p><i class="fas fa-dollar-sign"></i> <span id="paid">'+ event.total_price +'</span> <span style="background:blue;color:#fff" class="p-2" >'+ event.payment_status +'</label> </p>  -->
                                    <p class="mb-1"><span></span></p> 
                                    <div class="mt-3"> 
                                       <h4 class="mb-0"><i class="fas fa-user"></i> Gender</h4> <p class="pl-3"><span id="gender"></span></p> 
                                       <h4 class="mt-2 mb-0"><i class="fas fa-calendar"></i> Date of birth</h4> <p class="pl-3 mb-0"><span id="date_of_birth"></span></p> 
                                       <h4 class="mt-2"><i class="fas fa-map-marker-alt"></i> Address</h4> <p class="pl-3"><span id="address"></span></p> 
                                    </div> 
                                 </div>
                                 <div class="col-md-6">
                                       <p>Test Result: 
                                       <select class="form-control booking_status" data-id="" style="margin-top:6px" placeholder="Result awaited">
                                          <option value="" disabled="" selected="">Result awaited</option>
                                          <option value="positive">Positive Result</option>
                                          <option value="negative">Negative Result</option>
														<option value="void">Void Result</option>
                                          <option value="invaild">Invaild Result</option>
                                       </select>
                                       <input type="hidden" id="status_val" value="">
                                       <input type="hidden" id="booking_id_val" value="">
                                       </p>
                                 </div>
                                 <div class="col-md-6 mt-3">
                                    <button type="button" class="btn btn-info submit_booking_status">Save</button>

                                 </div>
                              </div> 

                                 
                                 

                              </div>
                              
                        </form>
                        <div class="text-right pt-2">
                              <a href="" id="booking_detail_link" class="text-info"><button type="button" class="btn btn-warning">View Details</button></a>
                              
                              <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                        </div>

                     </div> <!-- end modal-body-->
                  </div> <!-- end modal-content-->
            </div> <!-- end modal dialog-->
         </div>
      </div>
      <!-- end col-12 -->
</div> <!-- end row -->   
</div>
<script>

$(document).ready(function () {
    var base_url = '<?php echo base_url().'network/booking/details/'; ?>';
    var eve = <?php echo json_encode($events); ?>;
    //console.log(eve);
    var calendar = $('#calendar').fullCalendar({
      header: {
         left: 'prev,next today',
         center: 'title',
         right: 'month,basicWeek,basicDay'
      },
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      allDay:false,
      displayEventTime: true,
      eventLimit: 10,
       events:eve,
       eventRender: function(event, element){
            var dateObj = new Date(event.start); 
            element.popover({
              animation:true,
              delay: 300,
              content: '<div id="" class="row"> <div class="col-md-12"> <h3 class="mb-1">Bookingid:'+ event.title +'</h3> <div class="border-bottom"> <p class="mb-1"><b>Name: </b>'+ event.username +'</p> <p class="mb-1"><b>Email: </b>'+ event.email +'</p> <p class="mb-1"><b>Phone: </b>'+ event.phone +'</p> </div> <div class="border-bottom mt-2"> <p class="mb-1"><b>Test Name: </b>'+ event.report_name +'</p> <p class="mb-1"><b>Test Result: </b>'+ event.booking_status +'</p> </div> <p class="mb-1 mt-2"><i class="fas fa-calendar"></i> '+ dateObj.getDate()+ '-' +dateObj.getMonth()+'-'+dateObj.getFullYear() +' '+ event.slot_time + ' </p> <p><i class="fas fa-dollar-sign"></i> <span id="paid">'+ event.total_price +'</span> <span style="background:blue;color:#fff" class="p-2" >'+ event.payment_status +'</label> </p> <p class="mb-1"><span></span></p> <div class="mt-3"> <h4 class="mb-0"><i class="fas fa-user"></i> Gender</h4> <p class="pl-3">'+ event.gender +'</p> <h4 class="mt-2 mb-0"><i class="fas fa-calendar"></i> Date of birth</h4> <p class="pl-3 mb-0">'+ event.date_of_birth +'</p> <h4 class="mt-2"><i class="fas fa-map-marker-alt"></i> Address</h4> <p class="pl-3">'+ event.address +'</p> </div> </div> </div>',
              trigger: 'hover',              
              container: 'body',
              html: true
            });
         },
        eventClick: function(event) {
            console.log(event);
           // alert('Event Start Time : ' + event.start_time+' - Event End Time : '+ event.end_time+' -  Booking Status : '+event.booking_status);
             $(".status_success_message").html('');
            var dateObj = new Date(event.start);           
            var slotMonth = dateObj.getMonth() + 1;
            $(".slot_date_label").html(event.date_label);
            if(event.type == "international-arrival"){
               $(".slot_time_label").hide();
            }else{
               $(".slot_time_label").show();
            }
           document.getElementById('referance_id').textContent = event.title;
           document.getElementById('referance_member_id').textContent = event.reference_member_id;
           document.getElementById('start_date').textContent = dateObj.getDate()+ '-' +slotMonth+'-'+dateObj.getFullYear();
           document.getElementById('start_time').textContent = event.slot_time;
           document.getElementById('email').textContent = event.email;
           document.getElementById('phone').textContent = event.phone;
           document.getElementById('name').textContent = event.report_name;
           document.getElementById('user_name').textContent = event.username;
           //document.getElementById('booking_status').textContent = event.booking_status;
           if(event.result_status == 'positive' || event.result_status == 'negative' || event.result_status == 'void' || event.result_status == 'invaild')
           {
              $(".booking_status").val(event.result_status);
              $('#status_val').val(event.result_status);
           }
           // console.log(event.booking_status);
           $(".booking_status").attr("data-id",event.booking_id);
           $('#booking_id_val').val(event.booking_id);
           
           document.getElementById('paid').textContent = event.total_price;
           document.getElementById('payment_status').textContent = event.payment_status;
           document.getElementById('gender').textContent = event.gender;
           document.getElementById('date_of_birth').textContent = event.date_of_birth;
           document.getElementById('address').textContent = event.address;
           // document.getElementById('booking_detail_link').textContent = event.address;
           var a = document.getElementById('booking_detail_link'); //or grab it by tagname etc
           a.href = base_url + event.booking_id;
           $('#showData').modal('show');
        },
       /*  eventMouseover: function(event, jsEvent, view) {
          var dateObj = new Date(event.start);
          var slotMonth = dateObj.getMonth() + 1;
          $(this).append('<div id=\"'+event.booking_id+'\" class=\"hover-end\"><p class="mb-1">Bookingid:'+ event.title +'</p><p class="mb-1">Name:'+ event.username +'</p><p class="mb-1">Date: '+ dateObj.getDate()+ '-' +slotMonth+'-'+dateObj.getFullYear() +' </p><p class="mb-1">Phone:'+ event.phone +'</p><p class="mb-1">Test Name:'+ event.report_name +'</p></div>');
        },

        eventMouseout: function(event, jsEvent, view) {
            $('#'+event.booking_id).remove();
        }, */
        eventLimitClick: function(cellInfo, jsEvent, view) {
           window.location.href = "<?=base_url('network/booking')?>";
        },

    });

    $(document).on("change",".booking_status",function(){
      var status = $(this).val();
      var booking_id = $(this).data("id");
      
      $('#status_val').val(status);
      $('#booking_id_val').val(booking_id);
    });

    $(document).on("click",".submit_booking_status",function(event){
      var status = $('#status_val').val();
		var booking_id = $('#booking_id_val').val();
		if(status == ""){
			$(".status_success_message").html('<div class="alert alert-danger" role="alert"> Please select status</div>');
		}else{
			event.preventDefault();
         $.ajax({
            url: "<?=base_url()?>network/change-booking-status",
            method: "POST",
            data: {
               status:status,
               booking_id:booking_id,
               csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>'
            },
            dataType: "json",
            success: function(data) {
               $(".status_success_message").html('<div class="alert alert-success" role="alert"> Status has been changed.</div>');
               // setTimeout(function(){
               //    $(".status_success_message").html('');
               // },45000);
            }
         });
		}
    });
});

</script>
<style type="text/css">
  .hover-end {
    padding: 5px 0px;
    margin: 0 auto;
    font-size: 13px;
    text-align: left;
    position: absolute;
    width: 100%;
    opacity: 1;
    background: #fff;
    z-index: 5;
   /* height: 100px;*/
    top: 18px;
    color: #000;
    left: 0;
    right: 0;
}

</style>
