<link rel="stylesheet" href="<?php echo assets('plugins/notiflix/notiflix.css');?>"><style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>

<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 mt-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                  
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card " style="margin-top:20px;">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <table id="shippedstation" class="table table-bordered " style="width:100%">
              <thead>
                  <tr>
                      <th>Order Id</th>
                      <th>Order Number</th>
                      <th>Order Date</th>
                      <th>Item SKU</th>
                      <th>Item Name</th>
                      <th>Recipient</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
               <?php if (isset($orders)) {
                  foreach ($orders as $key => $val) { ?>
                     <tr>
                        <td><?= $val->orderId; ?></td>
                        <td><?= $val->orderNumber; ?></td>
                        <td><?= ($val->orderDate !="") ? date('m/d/Y',strtotime($val->orderDate)) : "";?></td>
                        <td><?php if (!empty($val->items)) { foreach ($val->items as $k => $v) { echo $v->sku; }  }  ?>   
                        </td>
                        <td><?php if (!empty($val->items)) {
                           foreach ($val->items as $k => $v) {    echo $v->name; } 
                        }  ?></td>
                        <td><?php echo $val->shipTo->name; ?></td>
                        <!-- <td><button class="btn btn-md btn-success" onclick="order_shipped(<?= $val->orderId; ?>);">Mark as Shipped</button></td> -->
                           <td>
                              <!-- <a class="btn btn-md btn-success" href="<?=base_url()?>network/shipStation/create-label-for-order/<?=$val->orderId?>">Mark as Shipped</a> -->
                              <a class="btn btn-md btn-success btn_get_ship_order" href="javascript:void(0)" data-id="<?=$val->orderId?>">Mark as Shipped</a>
                           </td>

                     </tr>   
                  <?php } 
               }   ?>
              </tbody>
            </table>
          
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="mark_as_shipped_order" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="createLabelTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered w-100" role="document">
    <div class="modal-content">
      <form method="POST" action="javascript:void(0)" id="mark_as_shipped_order_frm">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
         <div class="modal-header">
           <h4 class="modal-title" id="createLabelTitle">Order Details</h4>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body order_edit_popup">

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary btn_edit_order"> <i class="fa fa-spinner fa-spin" style="display:none"></i> Save</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </form>
    </div>
  </div>
</div>

<link href="<?php echo assets('plugins/datatables/dataTables.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/responsive.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/buttons.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/select.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/sweet-alert/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo assets('plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.bootstrap4.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.responsive.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/responsive.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.html5.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.flash.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.print.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.keyTable.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.select.min.js');?>"></script>
<script src="<?php echo assets('plugins/sweet-alert/sweetalert.js');?>"></script>
<script src="<?php echo assets('plugins/notiflix/notiflix.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/vfs_fonts.js');?>"></script>
<script type="text/javascript">
   function save_status() {
      // body...
         Notiflix.Confirm.Show( 
            'Order Shipped', 
            'Are you sure that you want to change status of this record?', 
            'Yes',
            'No',
            function(){  // Yes button callback 
               // You also can get your parameters as a Variable declared before. (The values have to be in String format)
               Notiflix.Report.Success('Status Changed','Status successfully changed.');
               // alert('Thank you.'); 
            }, function(){  // No button callback 
               // Notiflix.Report.Success('Status Changed','Status successfully changed.');
               // alert('If you say so...'); 
            }
         );
   }
  
  $('#shippedstation').DataTable({
      "order": [[ 2, "desc" ]],
      "lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "All"]]

  });

  $(document).on("click",".btn_get_ship_order",function(){
      var order_id = $(this).data("id");
      $.ajax({
         url: '<?=base_url()?>network/shipStation/get_order_detail',
         type: "POST",
         data:{
            order_id:order_id,
            csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>',
         },
         dataType:"json",
         success: function (returnData) {
            $(".order_edit_popup").html(returnData.html);
            $("#mark_as_shipped_order").modal("show");
            //$(".date_picker").datepicker();
         },
         error: function (xhr, ajaxOptions, thrownError) {
            
         }
      }); 
   });

  $(document).on("submit","#mark_as_shipped_order_frm",function(){
      $(".btn_edit_order").prop("disabled",true);
      $(".btn_edit_order i").show();
      $.ajax({
            url: BASE_URL + "network/shipStation/create-order-label",
            method: "POST",
            data: $(this).serialize(),
            dataType: "json",
            success: function(data) {
               $(".btn_edit_order").prop("disabled",false);
               $(".btn_edit_order i").hide();
               if(data.status == true){
                  $(".status_success_message").html('<div class="alert alert-success" role="alert"> Order Mark as shipped successfully.</div>');
                  setTimeout(function(){
                     $(".status_success_message").html('');
                     window.location.reload();
                  },1000);
               }else{
                  $(".status_success_message").html('<div class="alert alert-danger" role="alert"> '+data.message+' </div>');
               }
            }
        });
   });
</script>
