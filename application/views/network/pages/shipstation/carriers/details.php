<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <li class="breadcrumb-item">Carrier Details</li>
                     <li class="breadcrumb-item active"><?=isset($carrier_detail->name)?$carrier_detail->name:''?></li>
                  </ol>
               </div>
               <h4 class="page-title">Carrier Details - <?=isset($carrier_detail->name)?$carrier_detail->name:''?></h4>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-lg-4 col-xl-4">
         <div class="card-box text-center">
            <div class="text-left mt-3">
               <p class="text-muted mb-2 font-13"><strong>Name :</strong> <span class="ml-2"><?=isset($carrier_detail->name)?$carrier_detail->name:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Code :</strong><span class="ml-2"><?=isset($carrier_detail->code)?$carrier_detail->code:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Account Number :</strong> <span class="ml-2 "><?=isset($carrier_detail->accountNumber)?$carrier_detail->accountNumber:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Balance :</strong> <span class="ml-2 "><?=isset($carrier_detail->balance)?$carrier_detail->balance:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Nick Name :</strong> <span class="ml-2 "><?=isset($carrier_detail->nickname)?$carrier_detail->nickname:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Shipping Provider Id :</strong> <span class="ml-2 "><?=isset($carrier_detail->shippingProviderId)?$carrier_detail->shippingProviderId:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Primary :</strong> <span class="ml-2 ">
						<?php
						if(isset($carrier_detail->primary) && $carrier_detail->primary == 1){ ?>
							<span class="badge badge-success cursor-pointer font-15">Yes</span>
						<?php } else{ ?>
							<span class="badge badge-danger cursor-pointer font-15">No</span><?php
						} ?>
					</span></p>
            </div>
         </div>
      </div> 
      <div class="col-lg-8 col-xl-8">
         <div class="card-box">
            <div class="tab-content">
               <div class="tab-pane active" id="aboutme">
                  <h5 class="mb-3 mt-0 text-uppercase text-center"><i class="mdi mdi-cards-variant mr-1"></i>
                     Package List</h5>
                  <div class="table-responsive">
                     <table class="table table-borderless mb-0" id="package_list_datatable">
                        <thead class="thead-light">
                           <tr>
                              <th>Name</th>
                              <th>Code</th>
                              <th>Domestic</th>
                              <th>International</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
									if (!empty($package_list)) {
										foreach ($package_list as $key => $package_list_row) {?>
											<tr>
												<td><?=$package_list_row->name?></td>
												<td><?=$package_list_row->code?></td>
												<td>
													<?php
														if(isset($package_list_row->domestic) && $package_list_row->domestic == 1){ ?>
															<span class="badge badge-success cursor-pointer font-15">Yes</span><?php 
														} else{ ?>
															<span class="badge badge-danger cursor-pointer font-15">No</span><?php
														} ?>
												</td>
												<td>
													<?php
														if(isset($package_list_row->international) && $package_list_row->international == 1){ ?>
															<span class="badge badge-success cursor-pointer font-15">Yes</span><?php 
														} else{ ?>
															<span class="badge badge-danger cursor-pointer font-15">No</span><?php
														} 
													?>
												</td>
											</tr><?php
										}
                           } ?>
                        </tbody>
                     </table>
                  </div>
               </div> 
            </div> <!-- end tab-content -->
         </div> <!-- end card-box-->
      </div> <!-- end col -->
		<div class="col-md-12">
		<div class="card-box">
            <div class="tab-content">
               <div class="tab-pane active" id="aboutme">
                  <h4 class="mb-3 mt-0 text-uppercase text-left"><i class="mdi mdi-cards-variant mr-1"></i>Services List</h4>
                  <div class="table-responsive">
                     <table class="table table-borderless mb-0" id="service_list_datatable">
                        <thead class="thead-light">
                           <tr>
                              <th>Name</th>
                              <th>Code</th>
                              <th>Domestic</th>
                              <th>International</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php 
									if (!empty($service_list)) {
										foreach ($service_list as $key => $service_list_row) {?>
											<tr>
												<td><?=$service_list_row->name?></td>
												<td><?=$service_list_row->code?></td>
												<td>
													<?php
														if(isset($service_list_row->domestic) && $service_list_row->domestic == 1){ ?>
															<span class="badge badge-success cursor-pointer font-15">Yes</span><?php 
														} else{ ?>
															<span class="badge badge-danger cursor-pointer font-15">No</span><?php
														} ?>
												</td>
												<td>
													<?php
														if(isset($service_list_row->international) && $service_list_row->international == 1){ ?>
															<span class="badge badge-success cursor-pointer font-15">Yes</span><?php 
														} else{ ?>
															<span class="badge badge-danger cursor-pointer font-15">No</span><?php
														} 
													?>
												</td>
											</tr><?php
										}
                           } ?>
                        </tbody>
                     </table>
                  </div>
               </div> 
            </div> <!-- end tab-content -->
         </div> <!-- end card-box-->
		</div>
   </div>
   <!-- end row-->
</div>

<link href="<?php echo assets('plugins/datatables/dataTables.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/responsive.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/buttons.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/select.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/sweet-alert/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo assets('plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.bootstrap4.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.responsive.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/responsive.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.html5.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.flash.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.print.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.keyTable.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.select.min.js');?>"></script>
<script src="<?php echo assets('plugins/sweet-alert/sweetalert.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/vfs_fonts.js');?>"></script>
<script type="text/javascript">
  
  $('#package_list_datatable').DataTable({
		"lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "All"]]
  });
  $('#service_list_datatable').DataTable({
		"lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "All"]]
  });
</script>
