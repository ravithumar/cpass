<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 mt-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                  
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card " style="margin-top:20px;">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <table id="shippedstation" class="table table-bordered " style="width:100%">
              <thead>
                  <tr>
                      <!-- <th>Order Id</th> -->
                      <th>Name</th>
                      <th>Code</th>
                      <th>Account Number</th>
                      <th>Balance</th>
                      <th>Nickname</th>
							 <th>Shipping Provider ID</th>
							 <th>Primary</th>
							 <th>Action</th>
                  </tr>
              </thead>
              <tbody>
               <?php 
					if (!empty($result)) {
                  foreach ($result as $key => $row) { ?>
                       <tr>
								  <td><?=$row->name?></td>
								  <td><?=$row->code?></td>
								  <td><?=$row->accountNumber?></td>
								  <td><?=$row->balance?></td>
								  <td><?=$row->nickname?></td>
								  <td><?=$row->shippingProviderId?></td>
								  <td>
								  		<?php
										if(isset($row->primary) && $row->primary == 1){ ?>
											<span class="badge badge-success cursor-pointer font-15">Yes</span>
										<?php } else{ ?>
											<span class="badge badge-danger cursor-pointer font-15">No</span><?php
										} ?>
								  </td>
								  <td>
								  		<a href="<?=base_url()?>network/shipStation/carriers/<?=$row->code?>" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="la la-eye "></i></a>
								  </td>
							  </tr>
                     
                  <?php }

               } ?>
                
                 
              </tbody>
       
               </table>
          
         </div>
      </div>
   </div>
</div>
<link href="<?php echo assets('plugins/datatables/dataTables.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/responsive.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/buttons.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/select.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/sweet-alert/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo assets('plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.bootstrap4.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.responsive.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/responsive.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.html5.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.flash.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.print.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.keyTable.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.select.min.js');?>"></script>
<script src="<?php echo assets('plugins/sweet-alert/sweetalert.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/vfs_fonts.js');?>"></script>
<script type="text/javascript">
  
  $('#shippedstation').DataTable({
           "lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "All"]]

  });
</script>
