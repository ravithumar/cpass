<?php if (isset($data)) {
    if (!empty($data)) { ?>
        <div class="row">
            <div class="col-md-12">
                 <div class="status_success_message"></div>
            </div>
        </div>
        <div class="row">
            <input type="hidden" name="id" value="<?=$data['id']?>">
            <div class="col-md-3">
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Enter Barcode Number <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" name="barcode_number" required placeholder="29567946565" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Order Number <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm orderId" disabled="" value="<?= $data['reference_member_id']; ?>" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Order Date <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" disabled="" value="<?= date('d-m-Y',strtotime($data['created_at'])); ?>" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Paid Date <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" disabled="" value="<?= date('d-m-Y',strtotime($data['created_at'])); ?>" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Ship Date <span class="text-danger">*</span></label>
                    <input type="date" class="form-control form-control-sm datepicker" name="shipping_date" value="<?= date('Y-m-d',strtotime($data['shipping_date'])); ?>" autocomplete="off" required>
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>                
            </div>
            <div class="col-md-5 row">
                <h4>Recipient</h4>
                <div class="col-md-12">
                    <label for="field-1" class="control-label">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" value="<?=$data['name']?>" name="name" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Country <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" name="country" value="United Kingdom" readonly autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Address <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm " name="address_line1" value="<?=$data['address_line1']?>" placeholder="Address Line 1" autocomplete="off" data-parsley-maxlength="49" required>
                    <input type="text" class="form-control form-control-sm mt-2"  value="<?=$data['address_line2']?>" name="address_line2" placeholder="Address Line 2" autocomplete="off" data-parsley-maxlength="49">
                    <input type="text" class="form-control form-control-sm mt-2"  value="<?=$data['address_line3']?>" name="address_line3" placeholder="Address Line 3" autocomplete="off" data-parsley-maxlength="49">
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-4 mt-2">
                    <label for="field-1" class="control-label">City <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" required value="<?=$data['shipping_city']?>" name="city" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-4 mt-2">
                    <label for="field-1" class="control-label">State <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" required value="<?=$data['shipping_state']?>" name="state" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-4 mt-2">
                    <label for="field-1" class="control-label">Postal Code <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" required value="<?=$data['shippingpostal_code']?>" name="postcode" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-6 mt-2">
                    <label for="field-1" class="control-label">Phone <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" required value="<?=$data['phone']?>" name="phone" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-6 mt-2">
                    <label for="field-1" class="control-label">Email <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" required value="<?=$data['email']?>" name="email" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <!-- <div class="col-md-12 mt-2 text-right"> <a href="javascript:void(0);" class="validate_address">Validate Address</a> </div> -->
            </div>
            <div class="col-md-4">
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Weight (gr) <span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" data-parsley-min="0" data-parsley-type="number" required name="weight" placeholder="Weight" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Service <span class="text-danger">*</span></label>
                    <select class="form-control" name="service_code" required>
                        <option value="">Select</option>
                        <?php
                        if(!empty($service_list)){
                            foreach($service_list as $service_list_row){ ?>
                                <option value="<?=$service_list_row->code?>"><?=$service_list_row->name?></option><?php
                            }
                        } ?>
                    </select>
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Package <span class="text-danger">*</span></label>
                    <select class="form-control" name="package_code" required>
                        <option value="">Select</option>
                        <?php
                        if(!empty($package_list)){
                            foreach($package_list as $package_list_row){ ?>
                                <option value="<?=$package_list_row->code?>"><?=$package_list_row->name?></option><?php
                            }
                        } ?>
                    </select>
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Length (CM)<span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" data-parsley-type="digits" required name="lenght" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Width (CM)<span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" data-parsley-type="digits" required name="width" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>
                <div class="col-md-12 mt-2">
                    <label for="field-1" class="control-label">Height (CM)<span class="text-danger">*</span></label>
                    <input type="text" class="form-control form-control-sm" data-parsley-type="digits" required name="height" autocomplete="off" >
                    <div class="text-danger err_barcode" style="display:none;"></div>
                </div>             
            </div>          
        </div>    
    <?php }
} ?>