<style type="text/css" src="<?php echo assets('plugins/flatpickr/flatpickr.min.css');?>"></style>
<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
   <link href="<?php echo base_url('assets/v3/libs/switchery/switchery.min.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/multiselect/multi-select.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/select2/select2.min.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/selectize/selectize.bootstrap3.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css');?>" rel="stylesheet" type="text/css" />

<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 my-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <form method="POST" id="filter_form" action="<?php echo base_url('network/booking') ?>" class="row col-md-12 mb-2">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>">

        </form>
         <div class="">
            <?php echo $this->datatables->generate(); ?>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo assets('js/custom/admin/CoreClass.js');?>"></script>
<script type="text/javascript" src="<?php echo assets('plugins/flatpickr/flatpickr.min.js');?>"></script>
<script type="text/javascript">
   flatpickr($('.date-picker'));
   var url = BASE_URL+`network/booking`;
   CoreClass.filter("#booking",url,true);
   CoreClass.reset("#booking",url,true);

   function save_barcode(id) {
     var csrf_token = $("#csrf_token").val();
     var barcode = $('.member'+id).val();

     // data-memberId
     // if (barcode =="") {
     //     $('.err_barcode').html('please enter barcode.');
     //     $('.err_barcode').show();
     //  }else{
     //     $('.err_barcode').hide();
         $.ajax({
            url: BASE_URL + "network/shipStation/save_barcode",
            method: "POST",
            data: {
               csrf_token: csrf_token,
               id: id,
               barcode:barcode
            },
            dataType: "json",
            success: function(data) {
               // console.log();
               if (data.data.message == "success") {
                  $(".status_success_message"+id).html('<div class="alert alert-success" role="alert"> Barcode added successfully.</div>');
                  setTimeout(function(){
                     $(".status_success_message"+id).html('');
                     $('#modal').modal('toggle');
                     window.location.replace("shipAwaiting");
                  },1000);
               }else{
                  $(".status_success_message"+id).html('<div class="alert alert-danger" role="alert">'+data.data.message +'</div>');
               }

            }
        });
      // }
   }

</script>
