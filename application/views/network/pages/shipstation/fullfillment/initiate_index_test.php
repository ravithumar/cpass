<style type="text/css" src="<?php echo assets('plugins/flatpickr/flatpickr.min.css');?>"></style>
<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
   <link href="<?php echo base_url('assets/v3/libs/switchery/switchery.min.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/multiselect/multi-select.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/select2/select2.min.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/selectize/selectize.bootstrap3.css');?>" rel="stylesheet" type="text/css" />
   <link href="<?php echo base_url('assets/v3/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css');?>" rel="stylesheet" type="text/css" />

<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 my-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <form method="POST" id="filter_form" action="<?php echo base_url('network/booking') ?>" class="row col-md-12 mb-2">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo $this->security->get_csrf_hash(); ?>">

        </form>
         <div class="">
            <?php echo $this->datatables->generate(); ?>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="add_order" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="createLabelTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered w-100" role="document">
    <div class="modal-content">
      <form method="POST" action="javascript:void(0)" id="save_order_frm">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
         <div class="modal-header">
           <h4 class="modal-title" id="createLabelTitle">Order Details</h4>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body order_edit_popup">

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary btn_edit_order"> <i class="fa fa-spinner fa-spin" style="display:none"></i> Save</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?php echo assets('js/custom/admin/CoreClass.js');?>"></script>
<script type="text/javascript" src="<?php echo assets('plugins/flatpickr/flatpickr.min.js');?>"></script>
<script type="text/javascript">
   
   $(document).on("click",".edit_order",function(){
      var booking_member_id = $(this).data("booking_member_id");
     
      $.ajax({
         url: '<?=base_url()?>network/order_edit_popup',
         type: "POST",
         data:{
            booking_member_id:booking_member_id,
            csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>',
         },
         dataType:"json",
         success: function (returnData) {
            $(".order_edit_popup").html(returnData.html);
            $("#add_order").modal("show");
            //$(".date_picker").datepicker();
         },
         error: function (xhr, ajaxOptions, thrownError) {
            
         }
      }); 
   });

   $(document).on("submit","#save_order_frm",function(){
      $(".btn_edit_order").prop("disabled",true);
      $(".btn_edit_order i").show();
      $.ajax({
            url: BASE_URL + "network/shipStation/save_shipstation_order",
            method: "POST",
            data: $(this).serialize(),
            dataType: "json",
            success: function(data) {
               $(".btn_edit_order").prop("disabled",false);
               $(".btn_edit_order i").hide();
               $(".status_success_message").html('<div class="alert alert-success" role="alert"> Order updated successfully.</div>');
               setTimeout(function(){
                  $(".status_success_message").html('');
                  window.location.reload();
               },1000);
            }
        });
   });

   function save_barcode(id) {
     var csrf_token = $("#csrf_token").val();
     var barcode = $('.member'+id).val();
     // data-memberId
     if (barcode =="") {
         $('.err_barcode').html('please enter barcode.');
         $('.err_barcode').show();
      }else{
         $('.err_barcode').hide();
         $.ajax({
            url: BASE_URL + "network/shipStation/save_barcode",
            method: "POST",
            data: {
               csrf_token: csrf_token,
               id: id,
               barcode:barcode
            },
            dataType: "json",
            success: function(data) {
               $(".status_success_message"+id).html('<div class="alert alert-success" role="alert"> Order updated successfully.</div>');
               setTimeout(function(){
                  $(".status_success_message"+id).html('');
                  // $('#modal').modal('toggle');
                  window.location.replace("shipAwaiting");

               },1000);
            }
        });
      }
   }

</script>
<style type="text/css">
   #add_order .modal-lg{
      max-width: 1000px;
   }
   .form-control:disabled, .form-control[readonly] {
      background-color: #fafafa;
      /*color: inherit;*/
      
   }
</style>