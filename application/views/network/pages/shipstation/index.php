<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>

<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 mt-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                  
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card " style="margin-top:20px;">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <table id="shippedstation" class="table table-bordered " style="width:100%">
              <thead>
                  <tr>
                      <!-- <th>Order Id</th> -->
                      <th>Order Number</th>
                      <th>Order Date</th>
                      <th>Item SKU</th>
                      <th>Item Name</th>
                      <th>Recipient</th>
                      <th>Tracking Number</th>
							 <th>Action</th>
                  </tr>
              </thead>
              <tbody>
               <?php if (isset($orders)) {
                  foreach ($orders as $key => $val) { 
							$booking_data = (array)$this->db->get_where('booking',array('booking_no' => $val->orderNumber))->row();
							$booking_memeber_data = array();
							// if(!empty($booking_data)){
								$booking_memeber_data = $this->db->get_where('booking_member',array('reference_member_id' => $val->orderNumber))->row();
							// }
							
							?>
                       <tr>
                      		<td><?= $val->orderNumber; ?></td>
                      		<td><?= ($val->orderDate !="") ? date('m/d/Y',strtotime($val->orderDate)) : "";?></td>
                      		<td><?php 
									 	if (!empty($val->items)) {
											foreach ($val->items as $k => $v) {
												echo $v->sku;
											}
                      			}  ?>
									</td>
									<td><?php 
									if (!empty($val->items)) {
										foreach ($val->items as $k => $v) {
											echo $v->name;
										}
									}  ?></td>
									<td><?php echo $val->shipTo->name;?></td>
									<td>
									<?php
										if(!empty($booking_memeber_data) && isset($booking_memeber_data->tracking_number) && !empty($booking_memeber_data->tracking_number)){ ?>
											<a href="javascript:void(0)" class="aftership_tracking" data-tracking_number="<?=$booking_memeber_data->tracking_number?>"><?=$booking_memeber_data->tracking_number?></a>
										<?php } ?>
									</td>
									<td>
										<?php
										if(!empty($booking_memeber_data) && isset($booking_memeber_data->shipstation_label) && !empty($booking_memeber_data->shipstation_label)){ ?>
											<a class="btn btn-md btn-success" target="_blank" href="<?=$booking_memeber_data->shipstation_label?>">View Label</a>
										<?php } ?>
									</td>
                  		</tr>
                     
                  <?php }

               } ?>
                
                 
              </tbody>
       
               </table>
          
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="createLabel" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="createLabelTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="createLabelTitle">Tracking History</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tracking_history">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<link href="<?php echo assets('plugins/datatables/dataTables.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/responsive.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/buttons.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/datatables/select.bootstrap4.css');?>" rel="stylesheet" type="text/css" />
<link href="<?php echo assets('plugins/sweet-alert/sweetalert.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?php echo assets('plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.bootstrap4.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.responsive.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/responsive.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.buttons.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.bootstrap4.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.html5.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.flash.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/buttons.print.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.keyTable.min.js');?>"></script>
<script src="<?php echo assets('plugins/datatables/dataTables.select.min.js');?>"></script>
<script src="<?php echo assets('plugins/sweet-alert/sweetalert.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?php echo assets('plugins/pdfmake/vfs_fonts.js');?>"></script>
<script type="text/javascript">
  
  $('#shippedstation').DataTable({
      "order": [[ 1, "desc" ]],
      "lengthMenu": [[20, 30, 50, -1], [20, 30, 50, "All"]]
  });

  $(document).ready(function(){
		$(document).on("click",".aftership_tracking",function(){
			var tracking_number = $(this).data("tracking_number");
			$.ajax({
				url: '<?=base_url()?>network/tracking',
				type: "POST",
				data:{
					tracking_number:tracking_number,
					csrf_token:'<?php echo $this->security->get_csrf_hash(); ?>',
				},
				dataType:"json",
				success: function (returnData) {
					$(".tracking_history").html(returnData.html);
					$("#createLabel").modal("show");
				},
				error: function (xhr, ajaxOptions, thrownError) {
					
				}
			}); 
		});
  });
</script>
