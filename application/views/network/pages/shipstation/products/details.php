<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <li class="breadcrumb-item">Product Details</li>
                  </ol>
               </div>
               <h4 class="page-title">Product Details</h4>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-lg-4 col-xl-4">
         <div class="card-box text-center">
            <div class="text-left mt-3">
               <p class="text-muted mb-2 font-13"><strong>Name :</strong> <span class="ml-2"><?=isset($result->name)?$result->name:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>SKU :</strong><span class="ml-2"><?=isset($result->sku)?$result->sku:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Product ID :</strong> <span class="ml-2 "><?=isset($result->productId)?$result->productId:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Price :</strong> <span class="ml-2 "><?=isset($result->price)?$result->price:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Default Cost :</strong> <span class="ml-2 "><?=isset($result->defaultCost)?$result->defaultCost:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Length :</strong> <span class="ml-2 "><?=isset($result->length)?$result->length:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Width :</strong> <span class="ml-2 "><?=isset($result->width)?$result->width:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Height :</strong> <span class="ml-2 "><?=isset($result->height)?$result->height:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Fulfillment SKU :</strong> <span class="ml-2 "><?=isset($result->fulfillmentSku)?$result->fulfillmentSku:''?></span></p>
            </div>
         </div>
      </div> 
   </div>
   <!-- end row-->
</div>
