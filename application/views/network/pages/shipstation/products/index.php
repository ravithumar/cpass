<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 mt-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                  
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card " style="margin-top:20px;">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <table id="shippedstation" class="table table-bordered " style="width:100%">
              <thead>
                  <tr>
                      <!-- <th>Order Id</th> -->
                      <th>Product ID</th>
                      <th>Name</th>
                      <th>SKU</th>
                      <th>Price</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
						<?php 
						if (!empty($result->products)) {
							foreach ($result->products as $key => $row) { ?>
								<tr>
									<td><?=$row->productId?></td>
									<td><?=$row->name?></td>
									<td><?=$row->sku?></td>
									<td><?=$row->price?></td>
									<td>
										<a href="<?=base_url()?>network/shipStation/products/detail/<?=$row->productId?>" class="on-default text-secondary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View"><i class="la la-eye "></i></a>
									</td>
								</tr>
							<?php }

						}else{ ?>
							<tr>
								<td colspan="5" align="center">No data available in table</td>
							</tr><?php
						} ?>
              </tbody>
       
               </table>
					<?php
					if($result->pages > 1){ ?>
						<ul class="pagination float-right">
							<li class="paginate_button page-item previous <?=$result->page < 2?"disabled":""?>">
								<a href="<?=base_url()?>network/shipStation/products?page=<?=$result->page - 1?>" class="page-link">Previous</a>
							</li>
							<?php for ($i=1; $i<=$result->pages; $i++) { ?>
								<li class="paginate_button page-item <?=$i == $result->page?"active":""?>">
									<a href="<?=base_url()?>network/shipStation/products?page=<?=$i?>"class="page-link"><?=$i?></a>
								</li>
							<?php } ?>
							<?php
							if($result->page < $result->pages){ ?>
								<li class="paginate_button page-item next">
									<a href="<?=base_url()?>network/shipStation/products?page=<?=$result->page + 1?>" class="page-link">Next</a>
								</li>
							<?php } ?>
						</ul>
					<?php } ?>
         </div>
      </div>
   </div>
</div>
