<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 mt-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                  
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<?php $this->load->view('network/includes/message'); ?>
<div class="datatable card " style="margin-top:20px;">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <table id="shippedstation" class="table table-bordered " style="width:100%">
              <thead>
                  <tr>
                      <!-- <th>Order Id</th> -->
                      <th>Shipment ID</th>
                      <th>Order Number</th>
                      <th>Customer Email</th>
                      <th>Ship Date</th>
                      <th>Tracking Number</th>
                      <th>Shipment Cost</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
						<?php 
						if (!empty($result->shipments)) {
							foreach ($result->shipments as $key => $row) { ?>
								<tr>
									<td><?=$row->shipmentId?></td>
									<td><?=$row->orderNumber?></td>
									<td><?=$row->customerEmail?></td>
									<td><?=$row->shipDate?></td>
									<td><?=$row->trackingNumber?></td>
									<td><?=$row->shipmentCost?></td>
									<td>
										<a class="btn btn-md btn-success btn_create_label" href="javascript:void(0)" data-carrierCode="<?=$row->carrierCode?>" data-serviceCode="<?=$row->serviceCode?>" data-packageCode="<?=$row->packageCode?>">Create Label</a>
									</td>
								</tr>
							<?php }

						}else{ ?>
							<tr>
								<td colspan="7" align="center">No data available in table</td>
							</tr><?php
						} ?>
              </tbody>
       
               </table>
					<?php
					if($result->pages > 1){ ?>
						<ul class="pagination float-right">
							<li class="paginate_button page-item previous <?=$result->page < 2?"disabled":""?>">
								<a href="<?=base_url()?>network/shipStation/shipments?page=<?=$result->page - 1?>" class="page-link">Previous</a>
							</li>
							<?php for ($i=1; $i<=$result->pages; $i++) { ?>
								<li class="paginate_button page-item <?=$i == $result->page?"active":""?>">
									<a href="<?=base_url()?>network/shipStation/shipments?page=<?=$i?>"class="page-link"><?=$i?></a>
								</li>
							<?php } ?>
							<?php
							if($result->page < $result->pages){ ?>
								<li class="paginate_button page-item next">
									<a href="<?=base_url()?>network/shipStation/shipments?page=<?=$result->page + 1?>" class="page-link">Next</a>
								</li>
							<?php } ?>
						</ul>
					<?php } ?>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="createLabel" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="createLabelTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="createLabelTitle">Create Shipment Label</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Confirmation</label>
						<select class="form-control" name="confirmation">
							<option value="">Select</option>
							<option value="none">None</option>
							<option value="delivery">Delivery</option>
							<option value="signature">Signature</option>
							<option value="adult_signature">Adult Signature</option>
							<option value="direct_signature">Direct Signature</option>
						</select>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Ship Date</label>
						<input type="text" name="shipDate" class="form-control" placeholder="Ship Date">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Shipment Date</label>
						<input type="text" name="shipDate" class="form-control" placeholder="Shipment Date">
					</div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Create</button>
      </div>
    </div>
  </div>
</div>
<script>
	$(document).ready(function(){
		$(document).on("click",".btn_create_label",function(){
			$("#createLabel").modal("show");
		});
	});
</script>
