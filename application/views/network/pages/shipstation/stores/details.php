<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="javascript: void(0);"><?= $this->title; ?></a></li>
                     <li class="breadcrumb-item">Store Details</li>
                  </ol>
               </div>
               <h4 class="page-title">Store Details</h4>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-lg-4 col-xl-4">
         <div class="card-box text-center">
            <div class="text-left mt-3">
               <p class="text-muted mb-2 font-13"><strong>Store ID :</strong> <span class="ml-2 "><?=isset($result->storeId)?$result->storeId:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Store Name :</strong> <span class="ml-2"><?=isset($result->storeName)?$result->storeName:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Market Place Name :</strong><span class="ml-2"><?=isset($result->marketplaceName)?$result->marketplaceName:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Account Name :</strong> <span class="ml-2 "><?=isset($result->accountName)?$result->accountName:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2 "><?=isset($result->email)?$result->email:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Integration Url :</strong> <span class="ml-2 "><a href="<?=isset($result->integrationUrl)?$result->integrationUrl:'javascript:void(0)'?>"><?=isset($result->integrationUrl)?$result->integrationUrl:''?></a></span></p>
               <p class="text-muted mb-2 font-13"><strong>Company Name :</strong> <span class="ml-2 "><?=isset($result->companyName)?$result->companyName:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Phone :</strong> <span class="ml-2 "><?=isset($result->phone)?$result->phone:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Public Email :</strong> <span class="ml-2 "><?=isset($result->publicEmail)?$result->publicEmail:''?></span></p>
               <p class="text-muted mb-2 font-13"><strong>Website :</strong> <span class="ml-2 "><a href="<?=isset($result->website)?$result->website:'javascript:void(0)'?>"><?=isset($result->website)?$result->website:''?></a></span></p>
					<p class="text-muted mb-2 font-13"><strong>Status :</strong> <span class="ml-2 ">
						<?php
						if(isset($result->active) && $result->active == 1){ ?>
							<span class="badge badge-success cursor-pointer font-15">Yes</span>
						<?php } else{ ?>
							<span class="badge badge-danger cursor-pointer font-15">No</span><?php
						} ?>
					</span></p>
            </div>
         </div>
      </div> 
   </div>
   <!-- end row-->
</div>
