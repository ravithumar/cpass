<style type="text/css">
.switch {position: relative;display: inline-block;width: 72px;height: 28px;}
.switch input {display:none;}
.slider {position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #ca2222;-webkit-transition: .4s;transition: .4s;}
.slider:before {position: absolute;content: "";height: 23px;width: 25px;left: 4px;bottom: 3px;background-color: white;-webkit-transition: .4s;transition: .4s;}
input:checked + .slider {background-color: #2ab934;}
input:focus + .slider {box-shadow: 0 0 1px #2196F3;}
input:checked + .slider:before {-webkit-transform: translateX(39px);-ms-transform: translateX(39px);transform: translateX(39px);}
/*------ ADDED CSS ---------*/
.on{display: none;}
.on{color: white;position: absolute;transform: translate(-50%,-50%);top: 50%;left: 30%;font-size: 10px;font-family: Verdana, sans-serif;}
.off{color: white;position: absolute;transform: translate(-50%,-50%);top: 50%;left: 70%;font-size: 10px;font-family: Verdana, sans-serif;}
input:checked+ .slider .on{display: block;}
input:checked + .slider .off{display: none;}
/*--------- END --------*/
/* Rounded sliders */
.slider.round {border-radius: 34px;}
.slider.round:before {border-radius: 50%;}
</style>
<?php
// get from to time
$from_time = DateTime::createFromFormat('H:i',$this->config->item("start_time"));
$to_time = DateTime::createFromFormat('H:i',$this->config->item("end_time"));

$s1_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot1_start_time_defualt"));
$s1_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot1_end_time_defualt"));

$s1_from_time_selected = $s1_from_time_selected->format('h:i A');
$s1_to_time_selected = $s1_to_time_selected->format('h:i A');


$s2_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot2_start_time_defualt"));
$s2_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot2_end_time_defualt"));

$s2_from_time_selected = $s2_from_time_selected->format('h:i A');
$s2_to_time_selected = $s2_to_time_selected->format('h:i A');

$s3_from_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot3_start_time_defualt"));
$s3_to_time_selected = DateTime::createFromFormat('H:i',$this->config->item("slot3_end_time_defualt"));

$s3_from_time_selected = $s3_from_time_selected->format('h:i A');
$s3_to_time_selected = $s3_to_time_selected->format('h:i A');

for($j = $from_time; $j <= $to_time;){
    $available_time[] = $j->format('h:i A');
    $j->add(new DateInterval('PT30M'));
}
?>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('network/includes/message'); ?>
      <div class="row">
         <div class="col-sm-12">
            <form enctype="multipart/form-data" action="<?php echo base_url('network/slot/edit/'.$user_id); ?>" method="post" name="add_slot" id="add_slot" data-parsley-validate="">
               <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label>Clinic<span class="text-danger">*</span></label>
                                <select class="form-control" placeholder="Select Hospital" name="user_id" required="" data-parsley-required-message="Please Select Clinic">
                                 <option value="" selected="" disabled="">Select Clinic</option>
                                 <?php foreach($hospital as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>" <?php echo ($user_id == $value['id'] ? 'selected':'')?>><?php echo $value['company_name']; ?></option>
                                 <?php } ?>
                                </select>
                            </div> 
                        </div>
                        <div class="row">
                           <div class="col-lg-12 mb-3">
                                <div class="table-responsive b-0" data-pattern="priority-columns">
                                    <table class="table table-hover mb-0" id="table-working-hours">
                                        <tbody>
                                            <?php foreach ($days as $key => $value) { ?>
                                                <tr data-id='<?php echo $key; ?>' data-day='<?php echo substr($value['name'],0,3); ?>' class="day_<?php echo substr($value['name'],0,3); ?>">
                                                    <td class="day-name">
                                                      <h5><span class="badge badge-primary"><?php echo substr($value['name'],0,3); ?></span></h5>
                                                   </td>
                                                    <td class="from_td" title="Slot 1 - from time">
                                                        <select class="form-control  slot-1 from_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >From Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="symbol-dash text-center">
                                                        To
                                                    </td>
                                                    <td class="to_td" title="Slot 1 - to time">
                                                        <select class="form-control  slot-1 to_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >To Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="symbol-and text-center">
                                                        &
                                                    </td>
                                                    <td class="from_td" title="Slot 2 - from time">
                                                        <select class="form-control  slot-2 from_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >From Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="symbol-dash text-center">
                                                        To
                                                    </td>
                                                    <td class="to_td" title="Slot 2 - to time">
                                                        <select class="form-control  slot-2 to_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >To Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="symbol-and text-center">
                                                        &
                                                    </td>
                                                    <td class="from_td" title="Slot 3 - from time">
                                                        <select class="form-control  slot-3 from_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >From Time</option>
                                                        </select>
                                                    </td>
                                                    <td class="symbol-dash text-center">
                                                        To
                                                    </td>
                                                    <td class="to_td" title="Slot 3 - to time">
                                                        <select class="form-control  slot-3 to_time" name="<?php echo substr($value['name'],0,3); ?>[]">
                                                            <option disabled >To Time</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <div class="validation-error-label">
                                        <?php echo form_error('availibality'); ?>
                                    </div>
                                    <div class="validation-availibality">
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group text-right m-b-0">
                    <button class="btn btn-default pull-right mt-2 mb-2" type="submit">
                    Save
                    </button>
                </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
<script src="<?php echo assets('v3/js/form.js'); ?>"></script>
<script type="text/javascript">
    $('.is_closed').on('click', function() {

        $id = $(this).attr('id').split('_')[1];

        if($(this).prop("checked") == true){
            $('.without_ampm_'+ $id).removeAttr('disabled');
        }
        else if($(this).prop("checked") == false){
            $('.without_ampm_'+ $id).attr('disabled','disabled');
        }
    });

    var available_time = jQuery.parseJSON('<?php echo json_encode($available_time); ?>');

    var s1_from_time_selected = '<?php echo $s1_from_time_selected; ?>';
    var s1_to_time_selected = '<?php echo $s1_to_time_selected; ?>';

    var s2_from_time_selected = '<?php echo $s2_from_time_selected; ?>';
    var s2_to_time_selected = '<?php echo $s2_to_time_selected; ?>';

    var s3_from_time_selected = '<?php echo $s3_from_time_selected; ?>';
    var s3_to_time_selected = '<?php echo $s3_to_time_selected; ?>';

   $( document ).ready(function(){

       var dt = new Date();
       $.each(available_time, function(k, v) {
           $('.from_time').append($("<option>" , { text: v, value: v }));
           $('.to_time').append($("<option>" , { text: v, value: v }));
       });

      var slot_avil = '<?php echo json_encode($slot_avail);  ?>';
      var slot_avil_data = $.parseJSON(slot_avil);

      $.each(slot_avil_data, function(k, v) {
         console.log(v);
         $('.day_'+v.day).find('.from_td .slot-1.from_time option[value="'+v.morning_start+'"]').attr('selected', 'selected');
         $('.day_'+v.day).find('.to_td .slot-1.to_time option[value="'+v.morning_end+'"]').attr('selected', 'selected');

         $('.day_'+v.day).find('.from_td .slot-2.from_time option[value="'+v.afternoon_start+'"]').attr('selected', 'selected');
         $('.day_'+v.day).find('.to_td .slot-2.to_time option[value="'+v.afternoon_end+'"]').attr('selected', 'selected'); 

         $('.day_'+v.day).find('.from_td .slot-3.from_time option[value="'+v.evening_start+'"]').attr('selected', 'selected');
         $('.day_'+v.day).find('.to_td .slot-3.to_time option[value="'+v.evening_end+'"]').attr('selected', 'selected'); 
      });

   
   });
    var s1_from_time_option = s1_from_time_selected.toString();
    var s1_to_time_option = s1_to_time_selected.toString();

    var s2_from_time_option = s2_from_time_selected.toString();
    var s2_to_time_option = s2_to_time_selected.toString();

    var s3_from_time_option = s3_from_time_selected.toString();
    var s3_to_time_option = s3_to_time_selected.toString();

    
</script>