<style>
   #users_length, .dt-buttons {float: left;}
   .export_record_div .buttons-excel {margin-left: 5% !important;}
</style>
<div class="row">
   <div class="col-12">
      <div class="row">
         <div class="col-6 my-2">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $title; ?></h4>               
            </div>
         </div>
         <div class="col-6">
            <div class="page-title-box">
               <div class="page-title-right">
                
               </div>
            </div>
         </div>
      </div>
   </div>      
</div>
<div class="datatable card">
   <div class="card-content table-responsive">
      <div class="card-body card-dashboard">
         <div class="">
            <?php echo $this->datatables->generate(); ?>
         </div>
      </div>
   </div>
</div>
