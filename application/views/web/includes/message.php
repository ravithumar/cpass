<style type="text/css">
    .alert p {
        margin-bottom: 0;
    }
</style>
<?php
if($this->session->flashdata('error')!="") {
?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $this->session->flashdata('error'); ?>
</div>
<?php
}
?>
<?php
if($this->session->flashdata('success')!="") {
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $this->session->flashdata('success'); ?>
</div>

<?php
}
?>
<?php
if($this->session->flashdata('info')!="") {
?>
<div class="alert alert-info alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $this->session->flashdata('info'); ?>
</div>
<?php
}
?>
<?php
if($this->session->flashdata('warning')!="") {
?>
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php
}
?>


