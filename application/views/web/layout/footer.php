<?php   
    $url = $this->uri->segment(1); 
?>
<?php if($url != 'login' && $url != 'signup' && $url != 'forgot-password' && $url != 'change-password' && $url != 'resets-password' && $url != 'otp-verify'){ ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 mb-4">
                <a class="navbar-brand mb-2 d-inline-block" href="<?= base_url('home'); ?>"><img src="<?=assets('web/images/c-pass-logo.png')?>" alt="" class="img-fluid"></a>
                <p class="t-black font-14">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <ul class="social-icons-footer d-flex align-items-center mb-0">
                    <li><a href="javascript:void(0);" title=""><img src="<?=assets('web/images/footer-fb.png')?>" alt=""></a></li>
                    <li><a href="javascript:void(0);" title=""><img src="<?=assets('web/images/footer-insta.png')?>" alt=""></a></li>
                    <li><a href="javascript:void(0);" title=""><img src="<?=assets('web/images/footer-in.png')?>" alt=""></a></li>
                    <li class="mb-0"><a href="javascript:void(0);" title=""><img src="<?=assets('web/images/footer-tw.png')?>" alt=""></a></li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-3 mb-4">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Company</h3>
                <ul class="quick-links font-14 mb-0">
                    <li><a href="javascript:void(0);" title="" class="black-link">About Us</a></li>
                    <li><a href="javascript:void(0);" title="" class="black-link">Service</a></li>
                    <li class="mb-0"><a href="javascript:void(0);" title="" class="black-link">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-3 mb-4">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Product</h3>
                <ul class="quick-links font-14 mb-0">
                    <li><a href="javascript:void(0);" title="" class="black-link">Pricing</a></li>
                    <li><a href="javascript:void(0);" title="" class="black-link">Security</a></li>
                    <li><a href="javascript:void(0);" title="" class="black-link">Apps</a></li>
                    <li class="mb-0"><a href="javascript:void(0);" title="" class="black-link">Chating</a></li>
                </ul>
            </div>
            <div class="col-sm-4 col-md-3 mb-4">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Address</h3>
                <p class="font-14 t-black">3674 Harrison Street, San Francisco, 94143, CA, USA.</p>
            </div>
        </div>
    </div>
</footer>
<div class="bg-blue py-3">
	<p class="text-center text-white mb-0">Copyright © <?php echo date('Y'); ?> C-PASS</p>
</div>
<?php }?>
<!-- filter-options-modal -->
<div class="modal fade filter-options-popup p-0" id="filter-options-popup" tabindex="-1" role="dialog" aria-labelledby="filter-options-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal650 modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Filter Options</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="progress-block mb-3">
                    <p class="font-15 font600 t-black mb-0">Distance</p>
                    <p class="font-12 font600 t-blue">1.0 KM TO 5.0 KM</p>
                    <div class="progress  border-r6">
                         <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>    
                </div>
                <div class="progress-block mb-3">
                    <p class="font-15 font600 t-black mb-0">Price</p>
                    <p class="font-12 font600 t-blue">$50.00 TO $250.00</p>
                    <div class="progress  border-r6">
                         <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>    
                </div>
            
                <ul class="d-flex mb-3 p-0 radio-block">
                       
                  <li class="mr-3">
                     <input type="radio" id="test1" name="radio-group" checked>
                     <label for="test1" class="font-14 font500">Departure</label>
                  </li>
                  <li>
                     <input type="radio" id="test2" name="radio-group">
                     <label for="test2" class="font-14 font500">Domestic</label>
                  </li>

                </ul>
                           
                                
                <div class="btn-wrap text-center mt-4">
                    <button type="button" class="btn-green-lg border-r8 font-17 font600 mb-2 mb-sm-0 mr-0 mr-sm-3" data-dismiss="modal" aria-label="Close">Ok</button>
                    <button type="button" class="btn-red-lg border-r8 font-17 font600 ml-0 ml-sm-3" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>              
            </div>
        </div>
    </div>
</div>
<!-- booking-information-modal -->
<!-- <div class="modal fade booking-information-popup p-0" id="booking-information-popup" tabindex="-1" role="dialog" aria-labelledby="booking-information-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Booking Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0">Domestic</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0">Integrative Health</p>
                    </div>
                </div> 
                 <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0">Antigen Lateral flow</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0">09:30 AM</p>
                    </div>
                </div>
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0">July 25 2021</p>
                    </div>
                </div>      
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="<?=base_url('complete-your-registration')?>" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div> -->

<!-- add-new-member-modal -->
<div class="modal fade add-member-popup p-0" id="add-member-popup" tabindex="-1" role="dialog" aria-labelledby="add-member-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            
            <div class="modal-body p-4">
                <div class="form-group mb-2">
                    <p class="t-black font-16 font-weight-bold mb-2">Do you want to add a new member for booking</p>
                    <ul class="d-flex">
                        <li class="mr-4">
                            <input type="radio" id="test6" name="radio-group" checked="">
                            <label for="test6" class="d-block d-xl-flex align-items-center font-14 font500 mb-1">Yes</label>
                        </li>
                        <li class="ml-4">
                            <input type="radio" id="test7" name="radio-group">
                            <label for="test7" class="d-block d-xl-flex align-items-center  font-14 font500 mb-1">No</label>
                        </li>
                    </ul>                    
                    
                </div>
                <hr class="my-2">  
                <div class="form-group mb-2">
                    <p class="t-black font-16 font-weight-bold mb-2">Shall We Clone The Current Booking To The New Member That You Are Adding?</p>
                    <ul class="d-flex">
                        <li class="mr-4">
                            <input type="radio" id="test6" name="radio-group" checked="">
                            <label for="test6" class="d-block d-xl-flex align-items-center font-14 font500 mb-1">Yes</label>
                        </li>
                        <li class="ml-4">
                            <input type="radio" id="test7" name="radio-group">
                            <label for="test7" class="d-block d-xl-flex align-items-center  font-14 font500 mb-1">No</label>
                        </li>
                    </ul>                    
                    
                </div>          
                                
                <div class="btn-wrap text-center mt-4">                    
                   <a href="<?=base_url()?>member-list" class="btn-green-lg font-16 font600 mb-2">Next</a>                   
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- Pay Now modal -->
<div class="modal fade add-member-popup p-0" id="pay-now-popup" tabindex="-1" role="dialog" aria-labelledby="pay-now-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-body text-center p-4">
                <img src="<?=assets('web/images/tick-popup.jpg')?>" alt="" class="mb-3">
                <h6 class="font-22 font-weight-bold t-black">Thank You For Order</h6>
                <p class="t-blue font-14 font-weight-bold mb-2">Order Reference: <span class="t-black" id="booking_reference_id">#000000</span></p>
                <p class="t-black font-14">Order Information Has Been Sent To Your Registered Email Address And Also Added To Your Order History</p>
                <div class="btn-wrap text-center mt-4">                    
                   <a href="<?=base_url('my-booking')?>" class="btn-green-lg font-16" id="booking-success-btn">OK</a>                   
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- fit_to_fly -->


<!-- fit_to_fly_booking-information-modal -->
<div class="modal fade fit-to-fly-popup p-0" id="fit-to-fly-popup" tabindex="-1" role="dialog" aria-labelledby="fit-to-fly-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Order Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0">Fit to Fly - UK Departure (Oneway Only)</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0">Integrative Health</p>
                    </div>
                </div> 
                 <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0">Antigen Lateral flow</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0">09:30 AM</p>
                    </div>
                </div>
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0">July 25 2021</p>
                    </div>
                </div>      
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="<?=base_url()?>fit-to-fly/scan-document" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div>


<!-- fit_to_fly_add_new_member_modal -->
<div class="modal fade add-member-popup p-0" id="fit-to-fly-add-member-popup" tabindex="-1" role="dialog" aria-labelledby="add-member-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            
            <div class="modal-body p-4">
                <div class="form-group mb-2">
                    <p class="t-black font-16 font-weight-bold mb-2">Do you want to add a new member for order</p>
                    <ul class="d-flex">
                        <li class="mr-4">
                            <input type="radio" id="test6" name="radio-group" checked="">
                            <label for="test6" class="d-block d-xl-flex align-items-center font-14 font500 mb-1">Yes</label>
                        </li>
                        <li class="ml-4">
                            <input type="radio" id="test7" name="radio-group">
                            <label for="test7" class="d-block d-xl-flex align-items-center  font-14 font500 mb-1">No</label>
                        </li>
                    </ul>                    
                    
                </div>
                <hr class="my-2">  
                <div class="form-group mb-2">
                    <p class="t-black font-16 font-weight-bold mb-2">Shall We Clone The Current Order To The New Member That You Are Adding?</p>
                    <ul class="d-flex">
                        <li class="mr-4">
                            <input type="radio" id="test6" name="radio-group" checked="">
                            <label for="test6" class="d-block d-xl-flex align-items-center font-14 font500 mb-1">Yes</label>
                        </li>
                        <li class="ml-4">
                            <input type="radio" id="test7" name="radio-group">
                            <label for="test7" class="d-block d-xl-flex align-items-center  font-14 font500 mb-1">No</label>
                        </li>
                    </ul>                    
                    
                </div>          
                                
                <div class="btn-wrap text-center mt-4">                    
                   <a href="<?=base_url()?>fit-to-fly/clinic-detail" class="btn-green-lg font-16 font600 mb-2">Next</a>                   
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- fit_to_fly_uk_departure_booking-information-modal -->
<div class="modal fade fit-to-fly-popup p-0" id="fit-to-fly-uk-departure-popup" tabindex="-1" role="dialog" aria-labelledby="fit-to-fly-uk-departure-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">UK Departure Order Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0">Fit to Fly - UK Departure (Oneway Only)</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0">Integrative Health</p>
                    </div>
                </div> 
                 <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0">Antigen Lateral flow</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0">09:30 AM</p>
                    </div>
                </div>
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0">July 25 2021</p>
                    </div>
                </div>      
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="<?=base_url()?>fit-to-fly/second-step" class="btn-green-lg font-17 font500 mb-3">Proceed To Next Step</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- fit_to_fly_Pay Now modal -->
<div class="modal fade add-member-popup p-0" id="fit-to-flypay-now-popup" tabindex="-1" role="dialog" aria-labelledby="pay-now-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-body text-center p-4">
                <img src="<?=assets('web/images/tick-popup.jpg')?>" alt="" class="mb-3">
                <h6 class="font-22 font-weight-bold t-black">Thank You For Order</h6>
                <p class="t-blue font-14 font-weight-bold mb-2">Order Reference: <span class="t-black">#000000</span></p>
                <p class="t-black font-14">Order Information Has Been Sent To Your Registered Email Address And Also Added To Your Order History</p>
                <div class="btn-wrap text-center mt-4">                    
                   <a href="<?=base_url()?>my-booking" class="btn-green-lg font-16">OK</a>                   
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- international-booking-information-modal -->
<div class="modal fade international-booking-information-popup p-0" id="international-booking-information-popup" tabindex="-1" role="dialog" aria-labelledby="booking-information-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Order Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0" id="purpose-of-test">RT-PCR Test</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-3" style="display: none;">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0" id="test-type">RT-PCR Test</p>
                    </div>
                    <div class="col-lg-6 mb-3" style="display: none;">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0" id="place-of-test">Attend Clinic</p>
                    </div>
                    <div class="col-lg-6 mb-3" style="display: none;">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0" id="date">Jan 01 1970</p>
                    </div>
                    <div class="col-lg-6 mb-3" style="display: none;">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0" id="time">01:00 AM</p>
                    </div>
                </div>
                   
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                         <a href="<?=base_url()?>select-members" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div>


<div class="modal fade logout-popup p-0" id="logout_modal" tabindex="-1" role="dialog" aria-labelledby="logout-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content border-r6 shadow-1">
			<div class="modal-body p-4">
				<div class="row">
					<div class="col-md-12">
						<h3 class="font-20 font600 t-blue mb-0 text-center">Are you sure you want to logout?</h3>
					</div>
				</div>
				<div class="btn-wrap text-center mt-4">
					<div class="btn-area">
						<a href="<?=base_url('auth/logout/members')?>" class="btn-green-lg font-17 font600 mb-3 select_other_member">Yes</a>
					</div>
					<div class="btn-area">
						<button type="button" class="btn-red-lg" data-dismiss="modal" aria-label="Close">No</button>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div>

<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap-datepicker.min.js');?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="<?php echo assets('web/js/moment.min.js');?>"></script>
<script src="<?php echo assets('web/js/popper.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap-datetimepicker.min.js');?>"></script>
<script src="<?php echo assets('v3/libs/parsleyjs/parsley.min.js');?>"></script>
<script src="<?php echo assets('plugins/notiflix/notiflix.js');?>"></script>
<script src="<?php echo assets('plugins/select2/select2.full.min.js');?>"></script>
<script src="<?php echo assets('web/js/custom.js');?>"></script>
<script src="<?php echo assets('web/js/customtest.js');?>"></script>
<script src="<?php echo assets('web/js/jquery.ccpicker.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {    
	$('.alert').delay(3000).fadeOut(300);
	$('form').parsley();
	$("#register_phone").CcPicker({"countryCode":"in"});
    $('.select2').select2();
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });
    $('#departure_date, #return_date, #date_of_travel').datepicker({
        format: 'dd-mm-yyyy',
        startDate: "today",
        autoclose: true
    }).on("changeDate",function(ev){
        var purpose_id = $('input[name="purpose_id"]:checked').val();
        if(purpose_id == "4"){
            var selectDate = ev.date;
            var test_date = $("#booking_date").datepicker('getDate');
            if(test_date != null){
                var days = $("#country_id").find(':selected').attr('data-days');
                var depature_date= ev.date;
                var date1 = new Date(test_date);
                var date2 = new Date(depature_date);
                var Difference_In_Time = date2.getTime() - date1.getTime();
                var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                Difference_In_Days = Math.round(Difference_In_Days);
                if(Difference_In_Days >= 1){
                    if(Difference_In_Days > days){
                        //Notiflix.Notify.Failure("Please select a test date that is at least "+days+" days before departure date.");
                        $(".test_datepicker").val('');
                    }
                }else{
                    //Notiflix.Notify.Failure("Please select a test date that is at least "+days+" days before departure date.");
                    $(".test_datepicker").val('');
                }
            }
        }
    });

    $('#return_date').datepicker({
        format: 'dd-mm-yyyy',
        startDate: "today",
        autoclose: true
    }).on("changeDate",function(ev){
        $(".timepicker").timepicker("destroy");
        $(".timepicker").val("");
        if($(this).val() == "<?=date('d-m-Y')?>"){
            $('.timepicker').timepicker({
                timeFormat: 'HH:mm',
                interval: 15,
                minTime: "<?=date("H:i")?>",
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
        }else{
            $('.timepicker').timepicker({
                timeFormat: 'HH:mm',
                interval: 15,
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
        }
    });


    $('#booking_date').datepicker({
        format: 'dd-mm-yyyy',
        startDate: "today",
        autoclose: true
    }).on("changeDate",function(ev){
        var purpose_id = $('input[name="purpose_id"]:checked').val();
        var days = $("#country_id").find(':selected').attr('data-days');
        //console.log(days);
        if(purpose_id == "4"){
            var depature_date = $("#departure_date").datepicker('getDate');

            var test_date = ev.date;
            var date1 = new Date(test_date);
            var date2 = new Date(depature_date);
            var Difference_In_Time = date2.getTime() - date1.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            Difference_In_Days = Math.round(Difference_In_Days);
            if(Difference_In_Days >= 1){
                if(Difference_In_Days > days){
                    Notiflix.Notify.Failure("Please select a date that is at least "+days+" days before departure date.");
                    $(this).val('');
                }
            }else{
                Notiflix.Notify.Failure("Please select a date that is at least "+days+" days before departure date.");
                $(this).val('');
            }
            
        }
    });

    $('#dob').datepicker({
        format: 'dd-mm-yyyy',
        endDate: "today",
        autoclose: true
    }).on('changeDate', function(e) {  
		var parent = $(this).parent();
		parent.find(".parsley-errors-list li").remove();
	});
    $('#expiry-month-year').datepicker({
        format: 'mm-yyyy',
        startView: "years", 
        minViewMode: "months",
        startDate: "today",
        autoclose: true
    });
    $('#departure_time').timepicker({
        timeFormat: 'HH:mm',
        interval: 15,
        dynamic: true,
        dropdown: true,
        scrollbar: true
    });
    $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 15,
        dynamic: true,
        dropdown: true,
        scrollbar: true
    });
    $('.datetimepicker').datetimepicker({
        //language:  'fr',
        format: 'dd-mm-yyyy hh:ii P',
        weekStart: 1,
        todayBtn:  0,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        minuteStep: 15
    });


});

</script>
<script type="text/javascript">
jQuery(document).ready(function(){
	/* $(document).on("click",".eye-click",function(){
		$(".password_checker").removeClass("eye-click");
		$(".password_checker").addClass("eye-close");
		jQuery("#password").attr("type","text");
		jQuery(".password_checker img").attr('src', "<?php echo assets('web/images/eye.png');?>");
 	});

	$(document).on("click",".eye-close",function(){
		$(".password_checker").removeClass("eye-close");
		$(".password_checker").addClass("eye-click");
		jQuery("#password").attr("type","password");
		jQuery(".password_checker img").attr('src', "<?php echo assets('web/images/eyes.svg');?>");
	}); */

	$(document).on("click",".eye-click",function(){
		var closest = $(this).parent();
		
		closest.find(".password_checker").removeClass("eye-click");
		closest.find(".password_checker").addClass("eye-close");
		closest.find("input").attr("type","text");
		closest.find(".password_checker img").attr('src', "<?php echo assets('web/images/eye.png');?>");
 	});

	$(document).on("click",".eye-close",function(){
		var closest = $(this).parent();
		closest.find(".password_checker").removeClass("eye-close");
		closest.find(".password_checker").addClass("eye-click");
		closest.find("input").attr("type","password");
		closest.find(".password_checker img").attr('src', "<?php echo assets('web/images/eyes.svg');?>");
	});

	$(document).on("click",".header_logout_btn",function(){
		$("#logout_modal").modal("show");
	});
	
});
</script>
</body>
</html>
