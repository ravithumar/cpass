<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>C - Pass</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-datepicker.min.css');?>">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	<link rel="stylesheet" href="<?php echo assets('web/css/flatpickr.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-datetimepicker.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('plugins/notiflix/notiflix.css');?>">
	<link rel="stylesheet" href="<?php echo assets('plugins/select2/select2.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/jquery.ccpicker.css');?>">
	
	<script type="text/javascript"> var BASE_URL = '<?php echo base_url(); ?>'; </script>
	<!-- <meta http-equiv="refresh" content="2"> -->


</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<?php if($url != 'login' && $url != 'signup' && $url != 'forgot-password' && $url != 'change-password' && $url != 'resets-password' && $url != 'otp-verify'){ ?>
	<div class="header-topbar bg-blue py-2">
		<div class="container d-flex align-items-center">
			<div class="social-icons-box mr-4">
				<ul class="social-icons d-flex align-items-center mb-0">
					<li><a href="#" title=""><img src="<?php echo assets('web/images/fb.png');?>" alt=""></a></li>
					<li><a href="#" title=""><img src="<?php echo assets('web/images/tw.png');?>" alt=""></a></li>
					<li><a href="#" title=""><img src="<?php echo assets('web/images/yt.png');?>" alt=""></a></li>
					<li><a href="#" title=""><img src="<?php echo assets('web/images/t.png');?>" alt=""></a></li>
				</ul>
			</div>
			<div class="ml-auto d-flex align-items-center text-white">
				<p class="mb-0 font-13 d-none d-lg-block location">9873 Ridgewood Street Elgin, IL 60120</p>
				<p class="mb-0 mx-4 d-none d-lg-block">|</p>
				<a href="tel:7955582795" title="" class="font-13 call white-link">+1-795-5582-795</a>
				<p class="mb-0 font-17 ml-3 ml-sm-5">
					<?php
					if (!$this->session->userdata('members')) { ?>
						<a href="<?=base_url('login');?>" title="" class="white-link">Login </a>/
						<a href="<?=base_url('signup');?>" title="" class="white-link">Sign Up</a>
					<?php }else{ ?>
						<a href="javascript:void(0)" title="Logout" class="white-link header_logout_btn">Logout</a><?php
					} ?>
				</p>
			</div>
		</div>
	</div>
	<div class="menu">
		<nav class="navbar navbar-expand-lg navbar-light container">
			<a class="navbar-brand" href="<?=base_url('/home');?>"><img src="<?php echo assets('web/images/c-pass-logo.png');?>" alt="" class="img-fluid"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto align-items-center">
					<li class="nav-item active">
						<a class="nav-link" href="<?=base_url('/home');?>">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">News</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?=base_url('clinic-registration')?>">Clinic Registration</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Contact</a>
					</li>
				</ul>
			</div>
			<div class="search-section d-flex align-items-center">
				<div class="search-box ml-4 d-none d-sm-block">
					<a href="#" title="" class="d-inline-block"><img src="<?php echo assets('web/images/search-icon.jpg');?>" alt="" class="border-50 shadow-1"></a>
				</div>
				<?php if ( $this->session->userdata('members')) { ?>
					<div class="header-booknow ml-4">
						<a href="<?=base_url('new-booking');?>" class="btn-green">BOOK NOW</a>
					</div>
				<?php } ?>
			</div>
		</nav>
	</div>
	<div class="mx-3">
		<?php
		$this->load->view('/web/includes/message');
		?>
	</div>
	<?php } ?>
