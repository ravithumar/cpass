<section class="add-card py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Add Card</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="<?=base_url('add-payment-card')?>" method="POST" accept-charset="utf-8">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						<div class="row align-items-end justify-content-center">
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Card holder name</label>
									<input type="text" name="name" value="" placeholder="Enter card holder name" required class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Card Number</label>
									<input type="number" name="card_number" value="" placeholder="Enter card number" required class="form-control" data-parsley-type="digits" data-parsley-type-message="Enter valid card number">
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Expires</label>
									<input type="text" name="expire" value="" placeholder="Select expiry month/year" class="form-control expiry-month-year" id="expiry-month-year" required>
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">CVV</label>
									<input type="number" name="cvc_number" value="" placeholder="Enter CVV" class="form-control" required data-parsley-minlength="3" data-parsley-maxlength="4" data-parsley-minlength-message="Enter valid CVV" data-parsley-maxlength-message="Enter valid CVV">
								</div>
							</div>
						</div>
						<div class="text-center mt-5">
							<input type="submit" name="submit" value="Add Card" class="text-success btn-green-lg m-2">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>