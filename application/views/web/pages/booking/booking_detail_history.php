<?php  include("header.php"); ?>

<section class="booking-detail py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Detail</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height">
					<div class="row">
						<div class="col-lg-4 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Booking Reference</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13">#213546</p>
							</div>
						</div>
						<div class="col-lg-4 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Purpose Of Test</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13">Domestic</p>
							</div>
						</div>
						<div class="col-lg-4 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Place of test</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13">Integrative Health</p>
							</div>
						</div>
						<div class="col-md-12 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Shipping Address</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
							</div>
						</div>
					</div>
					<div class="pt-2">
						<h6 class="font-16 font-weight-bold t-blue mb-3">Member List</h6>
						<div class="row">
							<div class="col-md-6 col-xl-4 mb-4">
								<div class="booking-processing bg-white h-100 p-3 border-r10 border-blue">
								    <div class="d-flex justify-content-between align-items-center flex-wrap-reverse flex-wrap">
								    	<h6 class="font-16 mr-2 font-weight-bold my-1 t-blue d-inline-block">John Doe</h6>
								    	<div class="ml-auto">
								    		<button type="submit"><img src="images/download-icon.png" alt=""></button>
								    		<label class="negative ml-2 font-12 text-white font-weight-bold mb-0">Negative</label>
								    	</div>
								    </div>
								    <p class="mb-0 t-black font-weight-bold font-12">johndoe@domain.com</p>
								    <p class="mb-0 t-black font-weight-bold font-12">+44 655 6625 655</p>
								    <p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Passport Id :</span> 565656656</p>
								    <p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Date Of Birth :</span> 16 July 1994</p>
								    <div class="border-top pt-2 mt-2">
								    	<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Test Type :</span> Antigen Lateral flow</p>
								    	<div class="d-flex flex-wrap mb-2">
								    		<p class="mb-0 mr-4 t-black font-weight-bold font-12"><span class="t-blue">Date :</span> July 25 2021 </p>
								    		<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Time :</span> 09:30 am </p>
								    	</div>
								    	<h6 class="font-14 font-weight-bold t-blue mb-0">Home Address</h6>
								    	 <p class="mb-0 t-black font-weight-bold font-12">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
								    </div>
							    </div>
							</div>
						    <div class="col-md-6 col-xl-4 mb-4">
							    <div class="booking-processing bg-white h-100 p-3 border-r10 border-blue">
								    <div class="d-flex justify-content-between align-items-center flex-wrap-reverse flex-wrap">
								    	<h6 class="font-16 mr-2 font-weight-bold my-1 t-blue d-inline-block">John Doe</h6>
								    	<div class="ml-auto">
								    		<button type="submit"><img src="images/download-icon.png" alt=""></button>
								    		<label class="negative ml-2 font-12 text-white font-weight-bold mb-0">Negative</label>
								    	</div>
								    </div>
								    <p class="mb-0 t-black font-weight-bold font-12">johndoe@domain.com</p>
								    <p class="mb-0 t-black font-weight-bold font-12">+44 655 6625 655</p>
								    <p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Passport Id :</span> 565656656</p>
								    <p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Date Of Birth :</span> 16 July 1994</p>
								    <div class="border-top pt-2 mt-2">
								    	<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Test Type :</span> Antigen Lateral flow</p>
								    	<div class="d-flex flex-wrap mb-2">
								    		<p class="mb-0 mr-4 t-black font-weight-bold font-12"><span class="t-blue">Date :</span> July 25 2021 </p>
								    		<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Time :</span> 09:30 am </p>
								    	</div>
								    	<h6 class="font-14 font-weight-bold t-blue mb-0">Home Address</h6>
								    	 <p class="mb-0 t-black font-weight-bold font-12">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
								    </div>
							    </div>
							</div>
						</div>
					</div>
					<div class="text-center mt-5">
						<a href="#" title="" class="btn-blue m-2">Sync to Wallet</a>
						<a href="#" title="" class="btn-green-lg m-2">Clone Booking</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>