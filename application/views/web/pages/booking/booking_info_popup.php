<div class="modal booking-information-popup" id="booking-information-popup" tabindex="-1" role="dialog" aria-labelledby="booking-information-popupTitle" aria-modal="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0" id="title">Order Information</h6>              
            </div>
            <div class="modal-body p-4">
            	<div class="row mb-3">
            		<div class="col-lg-6">
            			<p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0" id="purpose-of-test">RT-PCR Test</p>
            		</div>
            	</div>
            	<div class="row">
            		<div class="col-lg-6 mb-3" style="display: none;">
            			<p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0" id="test-type">RT-PCR Test</p>
            		</div>
            		<!-- <div class="col-lg-6 mb-3" style="display: none;">
            			<p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0" id="place-of-test">Attend Clinic</p>
            		</div> -->
            		<div class="col-lg-6 mb-3" style="display: none;">
            			<p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0" id="date">Jan 01 1970</p>
            		</div>
            		<div class="col-lg-6 mb-3" style="display: none;">
            			<p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0" id="time">01:00 AM</p>
            		</div>
            	</div>

                <input type="hidden" value="" data-place-name="" id="selected-place-id">
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="javascript:void(0);" id="complete-your-registration" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                        <a href="javascript:void(0);" id="complete-your-registration-fit2fly-1way" class="btn-green-lg font-17 font600 mb-3" style="display: none;">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>