<section class="add-member py-5">
	<div class="container-large">
		<style type="text/css">
		.time-slot li{
			border: 1px solid #0C5B97;
		}
		.time-slot li.active{
			background-color: #187932;
			
		}
		.time-slot li.active a{
			color: white;
		}

		</style>
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			//_pre($_SESSION);
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Edit Detail</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative d-flex flex-column justify-content-between">
					<form action="<?=base_url()?>edit_detail_save" method="POST" accept-charset="utf-8">
					<div>
						<div class="row">
							<input type="hidden" name="purpose_id" id="purpose_id" value="<?php echo $purpose_id; ?>" >
							<input type="hidden" name="place_type" id="place_type" value="<?php echo $place_type; ?>" >
							<?php
							if($purpose_id != "1"){ ?>
								<div class="col-lg-4 col-md-12">
									<div class="form-group">
										<label class="font-14 font600 t-light-blue mb-0">Place Of Test</label>
										<input class="form-control font-14" type="text" readonly value="<?php echo $this->session->userdata("place_of_test_name"); ?>">
									</div>
								</div>
							<?php } ?>
							<?php
							//_pre($_SESSION);
							?>
							<div class="col-lg-4 col-md-12">
								<div class="form-group position-relative">
									<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font-14 font600">Test Type</label>
									<div class="select position-relative">
										<select name="test_type" id="test_type" class="form-control padding-r30 font-14" id="exampleFormControlSelect1">
											<option selected disabled>Select Test Type</option>
											<?php
											if(isset($test_types) && !empty($test_types)){
												foreach ($test_types as $key => $value) {
												?>
												<option value="<?php echo $value->id; ?>" <?php echo ($this->session->userdata("report_id") == $value->id)?'selected':''; ?>><?php echo $value->name; ?></option>
												<?php
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-12">
								<div class="form-group position-relative">
									<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font-14 font600 d-block">Select Test Date</label>
									<input type="text" name="booking_date" id="booking_date" class="form-control test_datepicker" placeholder="Select Test Date" readonly="" value="<?php echo $test_date; ?>" style="background-color: unset;" onchange="refreshSlot()" >
								</div>
							</div>
						</div>
						<?php
						
						if(($purpose_id == '1' && $place_type == '1') || ($purpose_id == '4')){
							?>
							<div class="row mt-3">
								<div class="col">
									<p class="t-light-blue font-weight-bold font-16">Select Time Slot</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul class="time-slot d-flex flex-wrap">
										<!-- <li class="time-red"><a href="#">Booked</a></li> -->
										<?php
										if(isset($slots) && !empty($slots)){
											foreach ($slots as $key => $value) {
												$active = '';
												$class = '';
												$disabled = '';
												$colorClass = '';
												$event = "showIsbooked('".$value->slot_time."')";
												if($value->slot_time == $test_time){
													$active = 'active';
												}
												if($value->is_booked == '1'){
													$class = 'btn btn-danger';
													$colorClass = 'color: #fff';
													$disabled = "pointer-events: none;";
													// $value->slot_time = 'Booked';
												}
												echo '<li class="'.$active.' '.$class.'" onclick="'.$event.'" data-id="'.$value->slot_time.'" style="'.$disabled.'"><a href="javascript:void(0);" style="'.$colorClass.'" >'.$value->slot_time.'</a></li>';
											}
										}
										?>
									</ul>
									<input type="hidden" name="selected_slot" value="<?= $value->slot_time ?>" id="selected_slot">		
								</div>
							</div>
							<?php
						}
						?>
							
					</div>
					<div class="row mt-3 mt-md-5">
						<div class="col-md-12">
							<div class="btn-wrap text-center">
								<!-- <a href="javascript:void(0);" class="btn-green-lg edit-detail-save-btn">Submit</a> -->
								<button type="submit" class="btn-green-lg edit-detail-save-btn">Submit</button>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
function showIsbooked(isbooked) {
	$('#selected_slot').val(isbooked);
	// $(".btn btn-danger").addClass('disabled');
 //    Notiflix.Notify.Failure("This timeslot is already booked");
 //    return false;
}


</script>