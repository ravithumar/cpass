
<style>
	.green-border{
		border: 3px solid green;
	}
	.member-img{
		height: 80px;
    	width: 80px;
    	object-fit: cover;
	}
</style>
<section class="py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Select members</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<div class="member-list-page">
						<div class="row align-item-center">
							<?php
							$is_already_selected = 0;
							$selected_booking_id = 0;
							$selected_user_id = 0;
							//_pre($members);
							$default_picture = $this->config->item("default_profile_picture");
							if(isset($members) && count($members) > 0){
								foreach ($members as $key => $value) {
									?>
									
									<div class="col-md-12 col-lg-6 mb-4">
										<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue <?php echo ($value->booking_id == '')?'':'green-border' ;?>">
											<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
												<img src="<?php echo ($value->profile_picture == '')?$default_picture:$value->profile_picture; ?>" alt="" class="border border-50 member-img">
											</div>
											<div class="member-list-detail w-100">
												<h6 class="t-blue font-14 font-weight-bold mb-0"><?=$value->full_name?></h6>
												<?php if(!empty($value->address)){ ?>
													<p class="mem-location t-black font-13 mb-0"><?php echo ($value->address == '')?'-':$value->address ; ?></p>
												<?php } ?>
												<div class="d-flex align-items-end justify-content-between">
													<div class="mr-2">
														<p class="font-10 font-weight-bold mb-0 t-black"><?=$value->email?></p>
														<p class="font-10 font-weight-bold mb-0 t-black"><?=$value->phone?></p>
													</div>
													<div>
														
														<?php
														if(isset($purpose_id) && $purpose_id == 3){
															if($value->booking_id == ''){ ?>
																<a href="javascript:void(0)" data-id="<?=$value->id?>" data-booking_id="<?=$value->booking_id?>" class="mr-2 edit_member"><img src="<?=assets('web/images/edit-icon.jpg')?>" alt=""></a>
															<?php } else {
																$is_already_selected = 1; 
																$selected_booking_id = $value->booking_id;
																$selected_user_id = $value->id;?>
																<a href="<?php echo base_url('booking/member/remove').'/'.$value->id.'/'.$value->booking_id; ?>" class=""><img src="<?=assets('web/images/delete-icon.jpg')?>" alt=""></a><?php
															}
														}else{
															if($value->booking_id == ''){
															?>
																<a href="<?php echo base_url('booking-edit-member').'/'.$value->id; ?>" class="mr-2"><img src="<?=assets('web/images/edit-icon.jpg')?>" alt=""></a>
															<?php
															}else{
															?>
																<a href="<?php echo base_url('booking/member/remove').'/'.$value->id.'/'.$value->booking_id; ?>" class=""><img src="<?=assets('web/images/delete-icon.jpg')?>" alt=""></a>
															<?php
															}
														}
														?>
														
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							}
							?>
						
						</div>
					</div>
					<div class="text-center mt-5">
					<!-- <?php //if($purpose_id == 3){ 
							if($is_already_selected == 1){ ?>
								<a href="javascript:void(0)" class="btn-blue m-2 add_new_member">Add Member</a>
							<?php }else{ ?>
								<a href="<?=base_url('scan-documents')?>" class="btn-blue m-2">Add Member</a>
							<?php } ?> -->
						<?php //} ?>
						
						<label class="btn-green-lg m-2 book-now-btn pointer" id="review-and-confirm">Book Now</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<input type="hidden" class="selected_flag" value="<?=$is_already_selected?>">
<input type="hidden" class="selected_booking_id" value="<?=$selected_booking_id?>">
<input type="hidden" class="selected_user_id" value="<?=$selected_user_id?>">

<div class="modal fade fit-to-fly-popup p-0" id="confirm_member_modal" tabindex="-1" role="dialog" aria-labelledby="fit-to-fly-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content border-r6 shadow-1">
			<!-- <div class="modal-header">
			<h6 class="t-blue font-weight-bold mb-0">Booking Information</h6>              
			</div> -->
			<input type="hidden" class="other_member_id" value="">
			<div class="modal-body p-4">
				<div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
					<div class="row">
						<div class="col-md-12">
							<p class="font-14 font600 t-blue mb-0 text-left">Your previously selected member will be deselected as currently we are allowing booking for one member at a time, Are you sure you want to select this member?</p>
						</div>
					</div>
				</div>				
				<div class="btn-wrap text-center mt-4">
					<div class="btn-area">
						<a href="javascript:void(0)" data-href="<?=base_url()?>booking/member/remove-selected-member/<?=$selected_user_id?>/<?=$selected_booking_id?>" class="btn-green-lg font-17 font600 mb-3 select_other_member">Yes</a>
					</div>
					<div class="btn-area">
						<button type="button" class="btn-red-lg" data-dismiss="modal" aria-label="Close">No</button>
					</div>
				</div>       
				<!-- <div class="btn-wrap text-center mt-4">
                    <button type="button" class="btn-green-lg border-r8 font-17 font600 mb-2 mb-sm-0 mr-0 mr-sm-3" data-dismiss="modal" aria-label="Close">Ok</button>
                    <button type="button" class="btn-red-lg border-r8 font-17 font600 ml-0 ml-sm-3" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div> -->
			</div>
		</div>
	</div>
</div>
