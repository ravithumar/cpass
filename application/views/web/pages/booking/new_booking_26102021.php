<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/web/css/mobiscroll.javascript.min.css'); ?>">
<script src="<?php echo base_url('assets/web/js/mobiscroll.javascript.min.js'); ?>"></script>

<section class="new-booking py-5">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey border-r10 p-4 py-5 account-div-height domestic-radio-section">
					<form action="javascript:void(0)" id="search_hospital_frm" method="post" accept-charset="utf-8" autocomplete="off">	 	
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">
						<!-- start -->
						<div class="row">
							<div class="col-lg-12 mb-5">
								<div id="accordion">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Purpose Of Test</a>								   
									   </div>
									   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
									      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
										  <ul class="mb-3 p-0 radio-block" id="new-booking-radio">
											<?php
											$first_purpose_name = "";
											if($purpose_type_response->status == true){
												$counter = 1; 
												foreach($purpose_type_response->data as $key=>$purpose_type_item){ 
													if($key == 0){
														$first_purpose_name = $purpose_type_item->name;
													}?>
													<li>
														<input type="radio" id="domestic<?=$counter?>" required="" name="purpose_id" data-value="<?=$purpose_type_item->name?>" value="<?=$purpose_type_item->id?>" <?=$key==0?'checked':''?> data-purpose-type-name="<?=$purpose_type_item->name?>">
														<label for="domestic<?=$counter?>" class="d-block d-xl-flex align-items-center">
															<p class="mb-0 font-15 font500"><?=$purpose_type_item->name?></p>  
														</label>
														<?php
														if(!empty($purpose_type_item->subcat)){
															$count = 1;
															foreach($purpose_type_item->subcat as $subcat_item){ ?>
																<ul class="ml-4 test-purpose d-none">
																	<li>
																		<input type="radio" id="domestic<?=$counter?><?=$count?>" data-value="<?=$subcat_item->name?>" required="" name="purpose_id" value="<?=$subcat_item->id?>" data-purpose-type-name="<?=$subcat_item->name?>">
																		<label for="domestic<?=$counter?><?=$count?>" class="d-block d-xl-flex align-items-center">
																		<p class="mb-0 font-15 font500"><?=$subcat_item->name?></p>  
																	</li>
																</ul><?php 
																$count++;
															} 
														} ?>
													</li><?php 
													$counter++;
												}
											} ?>
											<input type="hidden" class="purpose_of_test_name" name="purpose_of_test_name" value="<?=$first_purpose_name?>">
											</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" style="display: none;" id="select-destination-country">
								<div id="accordion03" class="new-book2">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
									   	 	<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Destination Country</a>								   
									   </div>
									   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion03">
									      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
										        <ul class="mb-3 p-0 radio-block input-field-ui">
										        	<select class="form-control select2" name="country_id" id="country_id" style="width:100% !important;">
										        	 	<option value="">Select Country</option>
													  	<?php
														  	if($country_list['status'] == true){
															  foreach($country_list['data'] as $key=>$country){  ?>
															  		<option value="<?php echo $country['id'];?>" data-days="<?=$country['days']?>"><?php echo $country['name'];?></option>
																	<?php 
																}
															} 
														?>
													</select>
		                						</ul>
									      	</div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" id="test-type-booking">
								<div id="accordion02" class="new-book1">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Choose Test Type</a>
									   </div>
									   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion02">
									      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
									        <ul class="mb-3 p-0 radio-block test_type_list">
	                						</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" id="type-of-delivery">
								<div id="accordion04">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Type of Delivery</a>								   
									   </div>
									   <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion04">
									      <div class="card-body bg-white  pt-5 px-3 px-md-4 pb-3">
									        <ul class="mb-3 p-0 radio-block">
											  <?php
											  if($place_test_response->status == true){
												  foreach($place_test_response->data as $key=>$place_test_item){ ?>
														<li>
															<input type="radio" id="place_test_<?=$place_test_item->id?>" name="place_test" value="<?=$place_test_item->id?>" <?=$key==0?'':''?> data-place-test-name="<?=$place_test_item->name?>">
															<label for="place_test_<?=$place_test_item->id?>" class="d-flex align-items-center"><p class="mb-0 font-15 font500"><?=$place_test_item->name?></label>
														</li><?php 
													} 
												} ?>
	                						</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>

							<div class="col-lg-12 mb-5 internation-arrival new-book5" style="display: none;" id="internatonal-arrival-booking">
								<div class="input-field-ui account-div-height residency-radio-section add-uk">
									<div class="row mt-3 mt-md-2">
										<div class="col-lg-12">
											<div id="accordion-select01" class="passenger">
											  <div class="card border-0 mb-5">
											   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 <a href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Passenger Residency Status</a>
											   </div>
											   <div id="collapseFive" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
											        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio">
							                          	<li class="mr-3 ">
									                     	<input type="radio" id="status1" name="status_radio1" value="1" checked="">
									                     	<label for="status1" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">UK Residents</p></label>
									                  	</li>
									                  	<li>
									                     	<input type="radio" id="status2" name="status_radio1" value="2">
									                     	<label for="status2" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">Non-UK Residents</p></label>
									                  	</li>
													</ul>
											      	</div>
												</div>
											</div>
										  </div>
										
										</div>
										<div class="col-lg-12">
											<div class="uk-address">
											  <div class="form-group mb-4">	
											  	<p class="t-black font-weight-bold font-17 mb-2">Please Provide Home Address</p>	
												 <input type="text" name="residency_postcode" value="" id="postcode1" placeholder="Home Address" class="search-address search-intl-home-address residency_postcode form-control form-control-box add_search-icon mb-2">
												 	<select name="residency_address" class="residency_address form-control form-control-box select-angle-blue " id="home-address-dropdown">
												   </select>
											   </div>
												 
											</div>
											<div class="non-uk-address" >
												<div class="form-group mb-4">
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Address Line 1</label>
														<input type="text" name="nonukaddress1" placeholder="Enter Address" class="nonukaddress1 form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Address Line 2</label>
														<input type="text" name="nonukaddress2" placeholder="Enter Address" class="nonukaddress2 form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">City</label>
														<input type="text" name="nonukcity" placeholder="Enter City" class="nonukcity form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Post/Zip Code</label>
														<input type="text" name="nonresidency_postcode" placeholder="Enter Post/Zip Code" class="nonresidency_postcode form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Country</label>
														<ul class="mb-3 p-0 radio-block input-field-ui">
											        	<select class="form-control select2 nonukcountry" style="width:100% !important;" name="nonukcountry" >
											        	 	<option value="">Select Country</option>
														  	<?php
															  	if($country_list['status'] == true){
																  foreach($country_list['data'] as $key=>$country){ ?>
																  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																		<?php 
																	} 
																} 
															?>
														</select>
													</ul>
													</div>
													<div class="form-group mb-2">	
												  		<p class="t-black font-weight-bold font-17 mb-2">UK Isolation Address</p>	
													 	<input type="text" name="uk_isolation_postcode" value="WC1N 3AX" id="uk_isolation_postcode" placeholder="Home Address" class="search-address search-home-address uk_isolation_postcode form-control form-control-box add_search-icon mb-2">
													 	<select name="uk_isolation_address" class="uk_isolation_address form-control form-control-box select-angle-blue " id="uk_isolation_address">
													   </select>
												   </div>

												</div>
											</div>
										</div>								
									</div>
									<div class="row">
										<div class="col-lg-12">
										  	<div id="accordion-select01">
											   	<div class="card border-0 mb-5">
												    <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
												   	 	<a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Country Arriving From</a>								   
												    </div>
													<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      		<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
														    <ul class="mb-3 p-0 radio-block input-field-ui">
													        	<select class="form-control select2" id="dest_country" name="dest_country" class="dest_country" style="width:100% !important;">
													        	 	<option value="">Select Country Arriving From</option>
																  	<?php
																	  	if($country_list['status'] == true){
																		    foreach($country_list['data'] as $key=>$country){ ?>
																		  		<option value="<?php echo $country['id'];?>" ><?php echo $country['name'];?></option>
																				<?php 
																			} 
																		} 
																	?>
																</select>
								    						</ul>
													      </div>
													</div> 
											   	</div>
									    	</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">During Your Trip, Will You Be Visiting / Stopping Over Or Transiting Through Any Islands or Country?</label>
												<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_visiting_country_yes" name="is_visiting_country" value="1" >
														<label class="mb-0 pointer t-blue" for="is_visiting_country_yes">Yes</label>
													</div>
													<div class="px-0 relative">
														<input type="radio" id="is_visiting_country_no" name="is_visiting_country" value="2" checked="">
														<label class="mb-0 pointer t-blue" for="is_visiting_country_no">No</label>
													</div>
												</div>
											</div>
									    </div>
									</div>
									<div class="row">
										<div class="col-lg-12" id="transiting_country" style="display:none;">
										  <div id="accordion-select01">
										   		<div class="card border-0 mb-5">
											    	<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 		<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Transiting Country</a>   
											    	</div>
													<div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
											        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio">
						                              		<select class="form-control select2" id="visiting_country" name="visiting_country" style="width:100% !important;">
											        	 		<option value="">Select Transiting Country</option>
														  		<?php
															  	if($country_list['status'] == true){
																  	foreach($country_list['data'] as $key=>$country){ ?>
																  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																		<?php 
																	} 
																}  ?>
															</select>
						    							</ul>
											      </div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Do You Qualify For Any Exemption?</label>
												<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_excempted_yes" name="is_excempted" value="1" >
														<label class="mb-0 pointer t-blue" for="is_excempted_yes">Yes</label>
													</div>
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_excempted_no" name="is_excempted" value="2" checked="">
														<label class="mb-0 pointer t-blue" for="is_excempted_no">No</label>
													</div>
												</div>
											</div>
									    </div>
									    <div class="col-lg-12" id="exemption_country" style="display:none;">
										    <div id="accordion-select01">
										   		<div class="card border-0 mb-5">
											    	<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 		<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Exemption Category</a>   
											    	</div>
													<div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
												      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
												        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio1">
							                              		<select class="form-control select2" name="excempted_cat" id="excempted_cat" style="width:100% !important;">
												        	 		<option value="">Select Exemption Category</option>
															  		<?php
																  	if($exemptioncat_list['status'] == true){
																	  	foreach($exemptioncat_list['data'] as $key=>$cat){ ?>
																	  		<option value="<?php echo $cat['id'];?>"><?php echo $cat['name'];?></option>
																			<?php 
																		} 
																	}  ?>
																</select>
							    							</ul>
												      	</div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Do you have any Medical exemptions?</label>
												<div class="w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_medical_excempted_yes" name="is_medical_excempted" value="yes" >
														<label class="mb-0 pointer t-blue" for="is_medical_excempted_yes">Yes</label>
													</div>
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_medical_excempted_no" name="is_medical_excempted" value="no" checked="">
														<label class="mb-0 pointer t-blue" for="is_medical_excempted_no">No</label>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12 international_proof_document" style="display: none;">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Upload Proof Document</label>
												<br/>
												<input type="file" name="proof_document" id="proof_document">
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-12">
										  	<div id="accordion-select01">
										   		<div class="card border-0 mb-5">
													<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
														<a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">What is Your Covid-19 Vaccination Status?</a>   
													</div>
													<div id="collapseThree" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
														<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
															<div class="home-address-radio-btn w-100 align-items-center ">
																<div class="px-0  mr-3">
																	<input type="radio" id="fully_vaccinated" name="vaccine_status" data-val="1" value="Fully Vaccinated" >
																	<label class="mb-0 pointer t-blue" for="fully_vaccinated">Fully Vaccinated</label>
																</div>
																<div class="px-0 mr-3">
																	<input type="radio" id="partially_vaccinated" name="vaccine_status" data-val="2" value="Partially Vaccinated" >
																	<label class="mb-0 pointer t-blue" for="partially_vaccinated">Partially Vaccinated</label>
																</div>
																<div class="px-0  mr-3">
																	<input type="radio" id="not_vaccinated" name="vaccine_status" data-val="3" value="Not Vaccinated" >
																	<label class="mb-0 pointer t-blue" for="not_vaccinated">Not Vaccinated</label>
																</div>
															</div>
														</div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-12 international_vaccine_list_div" style="display: none;">
										  	<div id="accordion-select0123">
										   		<div class="card border-0 mb-5">
													<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne1">
														<a href="#" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true" aria-controls="collapseThree1" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Vaccine type</a>   
													</div>
													<div id="collapseThree1" class="collapse" aria-labelledby="headingOne1" data-parent="#accordion-select0123">
														<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
															<select class="form-control select2" name="vaccine_id" id="vaccine_id" style="width:100% !important;">
											        	 		<option value="">Select Vaccine type</option>
														  		<?php
															  	if($certified_vaccines_list['status'] == true){
																  	foreach($certified_vaccines_list['data'] as $key=>$vaccine_list_row){ ?>
																  		<option value="<?php echo $vaccine_list_row['id'];?>" data-days="<?=$vaccine_list_row['days']?>"><?php echo $vaccine_list_row['name'];?></option>
																		<?php 
																	} 
																}  ?>
															</select>
															
														</div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-6 mt-2 ">
											<div class="form-group">
												<label class="t-black font-weight-bold mb-2">Vaccine Date</label>	
												<input type="text" name="vaccine_date" id="vaccine_date" class="form-control" placeholder="Select Date" readonly style="background-color: unset;">
											</div>
										</div>
										<div class="col-lg-6 mt-2">
											<div class="form-group">
												<label class="t-black font-weight-bold mb-2">Date of Birth</label>	
												<input type="text" name="date_of_birth" id="date_of_birth" class="form-control" placeholder="Select Date" readonly style="background-color: unset;">
											</div>
										</div>	
										<div class="col-lg-12">
										  	<div id="accordion-select01">
										   		<div class="card border-0 mb-5">
												    <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
												   	 	<a href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>
												    </div>
													<div id="collapseFour" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
												      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
													        <ul class="mb-3 p-0 radio-block test_type_list1">
	                										</ul>
													      </div>
														</div>
										   			</div>
								        		</div>
							    			</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="d-flex font-15 font-weight-bold t-black mb-0">
						                     		Arrival Date &amp; Time
						                     	</div>
											</div>
											<div class="col-lg-6 mt-2 ">
												<div class="form-group">
													<input type="text" name="return_date" id="return_date" class="form-control" placeholder="Select Date" readonly style="background-color: unset;">
												</div>
											</div>
											<div class="col-lg-6 mt-2">
												<div class="form-group">
													<input type="text" name="return_time" id="return_time" class="form-control timepicker" placeholder="Select Time" readonly style="background-color: unset;">
												</div>
											</div>									  	
										 	<div class="col-lg-12">
												<div class="form-group mb-4">
													<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Select Place to Collect Test Kit</label>
													<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
														<div class="px-0 relative mr-3">
															<input type="radio" id="shipping_method_clinic" name="shipping_method" value="1" checked="" >
															<label class="mb-0 pointer t-blue" for="shipping_method_clinic">Pickup from clinic</label>
														</div>
														<div class="px-0 relative mr-3">
															<input type="radio" id="shipping_method_home" name="shipping_method" value="2" >
															<label class="mb-0 pointer t-blue" for="shipping_method_home">Ship to UK Address</label>
														</div>
													</div>
												</div>
										    </div>
										    <div class="col-lg-12 non-uk-shipping-address ">
												<div class="form-group mb-4">
													<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Is the shipping address the same as your provide UK isolation address above?</label>
													<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
														<div class="px-0 relative mr-3">
															<input type="radio" id="residency_flag_yes" name="residency_flag" value="1" checked="" >
															<label class="mb-0 pointer t-blue" for="residency_flag_yes">Yes</label>
														</div>
														<div class="px-0 relative mr-3">
															<input type="radio" id="residency_flag_no" name="residency_flag" value="2" >
															<label class="mb-0 pointer t-blue" for="residency_flag_no">No</label>
														</div>
													</div>
												</div>
										    </div>
										    <div class="col-lg-12 uk-shipping-div uk-shipaddress" style="display:none;">
											   	<div class="form-group mb-4">	
												 	<input type="text" name="" value="WC1N 3AX" placeholder="" id="postcode2" class="form-control form-control-box add_search-icon mb-2 search-shipping-address">
												 	<select name="shipping-address-dropdown" id="shipping-address-dropdown" class="form-control form-control-box select-angle-blue resshipping_address">
												   	</select>
											   	</div>
											</div>
										    <div class="col-lg-12 pickup-date-center-div" >
											  	<div class="form-group mb-4">	
											  		<label class="t-black font-weight-bold font-17 mb-2">Pickup Date</label>	
												 	<input type="text" name="pickup_date" placeholder="26-10-2021" class="pickup_date form-control form-control-box mb-2 datepicker">
											  </div>
										 	</div>
										 	<div class="col-lg-12 uk-shipping-div" style="display:none;">
										 		<div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
									         	  	<div class="select-date">								 
													  	<div class="form-group position-relative">
											    			<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0 ukshipping_date" readonly style="background-color: unset;">Select Shipping Date</label>
											    	  	</div>
									         	   	</div>
								           		</div>
									            <div class="calender-view mb-3">
									             	<input type="text" id="datepicker" name="shipping_date" placeholder="Date" class="p-2 form-control datepicker ukshipping_date1">
									            </div>
									        </div>
											<div class="col-md-12 pickup-search-center-div">
												<div class="test-center-info">
													<p class="font-15 font-weight-bold t-black">Select A Pickup Centre</p>
													<ul class="nav nav-tabs border-0" id="myTab">
													  <li class="nav-item">
													    <a class="nav-link active border-0 font-15 t-grey" id="post-code-tab" data-toggle="tab" href="#post-code" role="tab" aria-controls="home" aria-selected="true">Search By Post Code</a>
													  </li>
													  <li class="nav-item">
													    <a class="nav-link border-0 t-grey font-15" id="country-tab" data-toggle="tab" href="#country" role="tab" aria-controls="profile" aria-selected="false">Search By Region</a>
													  </li>
													  <li class="nav-item">
													    <a class="nav-link border-0 t-grey font-15 search_by_map" id="map-tab"  role="tab" aria-controls="contact" aria-selected="false" href="javascript:void(0);">Search By Map</a>
													  </li>
													</ul>
													<div class="tab-content border-r7 pt-4 pb-3 px-3 bg-white mt-3 shadow-3" id="myTabContent">
													  <div class="tab-pane fade show active" id="post-code" role="tabpanel" aria-labelledby="post-code-tab">
														<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
														<div class="form-group">
														    <input class="form-control font-12" type="text" name="hospitalpostcode" id="hospitalpostcode" placeholder="Enter Postcode" aria-label="Search">
														</div>
													  </div>
													  <div class="tab-pane fade" id="country" role="tabpanel" aria-labelledby="country-tab">
													  	<p class="mb-0 font-16 t-blue font-weight-bold">Region</p>
														<div class="form-group">
															<select name="regionId" class="form-control border-0 px-3 py-1 border-r10 inter_regionId">
																<option value="" disabled="" selected="">Please select region</option>
																<?php foreach($county as $key => $value) { ?>
																	<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
																<?php } ?>
															</select>
														    <!-- <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search"> -->
														</div>
													  </div>
													  <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
													  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
														<div class="form-group">
														    <input class="form-control font-12" type="text" placeholder="Please enter postcode" name="mappostcode" aria-label="Search">
														</div>
													  </div>
													</div>
												</div>
											</div>										 	
										</div>
									<!-- <div class="row shipping-div" style="display:none;">
										<div class="col-lg-12 ">
										  <div class="non-uk-shipping-address">
										   <div class="form-group mb-4">	
										  	<p class="t-black font-weight-bold font-17 mb-2">Shipping Address</p>	
											 <input type="text" name="" value="WC1N 3AX" placeholder="" id="postcode2" class="form-control form-control-box add_search-icon mb-2 search-shipping-address">
											 <select name="shipping-address-dropdown" id="shipping-address-dropdown" class="form-control form-control-box select-angle-blue ">
												
											   </select>
										   </div>
											 
										   </div>
										</div>
									</div> -->
								</div>
							</div>	

						</div>
						<!-- End -->
						<div class="row mt-3" id="departure-date-time-selection" style="display: none;">
							<div class="col-lg-6">
								<div class="col-md-12">
									<div class="d-flex font-15 font-weight-bold t-black mb-0">
			                     		Select Departure Date
			                     	</div>
								</div>
								<div class="col-md-12 mt-2">
									<div class="form-group">
										<input type="text" name="departure_date" id="departure_date" class="form-control depature_datepicker" placeholder="Select Date" readonly style="background-color: unset;">
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="col-md-12">
									<div class="d-flex font-15 font-weight-bold t-black mb-0">
			                     		Select Departure Time
			                     	</div>
								</div>
								<div class="col-md-12 mt-2">
									<div class="form-group">
										<input type="text" name="departure_time" id="departure_time" class="form-control" placeholder="Select Time" readonly style="background-color: unset;">
									</div>
								</div>
							</div>						
							
						</div>
						<div class="col-lg-6 return_dateroundtrip" style="display: none;">
							<div class="d-flex font-15 font-weight-bold t-black mb-0">
								Return Date
							</div>
							<div class=" mt-2 ">
								<div class="form-group">
									<input type="text" name="return_date1" id="return_date1" class="form-control return_datepicker" placeholder="Select Date" readonly style="background-color: unset;">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="mt-3 col-lg-6" id="date-selection">
								<div class="col-md-12">
									<div class="d-flex font-15 font-weight-bold t-black mb-0">
			                     		Select Test Date
			                     	</div>
								</div>
								<div class="col-md-12 mt-2">
									<div class="form-group">
										<input type="text" name="booking_date" id="booking_date" class="form-control test_datepicker" placeholder="Select Date" readonly style="background-color: unset;">
									</div>
								</div>
							</div>
							<div class=" mt-3 col-lg-6" id="time-selection">
								<div class="col-md-12">
									<div class="d-flex font-15 font-weight-bold t-black mb-0">
			                     		Select Test Time
			                     	</div>
								</div>
								<div class="col-md-12 mt-2">
									<div class="form-group">
										<input type="text" name="booking_time" id="booking_time" class="form-control timepicker" placeholder="Select Time" readonly style="background-color: unset;">
									</div>
								</div>
							</div>
						</div>
						
						<div class="row mt-3 " id="search-center-div" >
							<div class="col-md-12">
								<div class="test-center-info">
									<p class="font-15 font-weight-bold t-black">Select A Test Centre</p>
									<ul class="nav nav-tabs border-0" id="myTab">
									  <li class="nav-item">
									    <a class="nav-link active border-0 font-15 t-grey" id="post_code_tab" data-toggle="tab" href="#post_code" role="tab" aria-controls="post_code" aria-selected="true">Search By Post Code</a>
									  </li>
									  <li class="nav-item">
									    <a class="nav-link border-0 t-grey font-15" id="county_tab" data-toggle="tab" href="#county" role="tab" aria-controls="county" aria-selected="false">Search By Region</a>
									  </li>
									  <li class="nav-item">
									    <a class="nav-link border-0 t-grey font-15" id="map_tab" href="<?=base_url()?>search-map" role="tab" aria-controls="map-tab" aria-selected="false">Search By Map</a>
									  </li>
									</ul>
									<div class="tab-content border-r7 pt-4 pb-3 px-3 bg-white mt-3 shadow-3" id="myTabContent">
									  <div class="tab-pane fade show active" id="post_code" role="tabpanel" aria-labelledby="post_code_tab">
										<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
										<div class="form-group">
										    <input class="form-control font-12" type="text" name="postcode" id="txtpostcode" placeholder="Enter Postcode" aria-label="Search">
										</div>
									  </div>
									  <div class="tab-pane fade" id="county" role="tabpanel" aria-labelledby="county_tab">
									  	<p class="mb-0 font-16 t-blue font-weight-bold">Region</p>
										<div class="form-group">
											<select name="regionId" class="form-control border-0 px-3 py-1 border-r10 regionId">
												<option value="" disabled="" selected="">Please select region</option>
												<?php foreach($county as $key => $value) { ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php } ?>
											</select>
										    <!-- <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search"> -->
										</div>
									  </div>
									  <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map_tab">
									  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
										<div class="form-group">
										    <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search" id="">
										</div>
									  </div>
									</div>

								</div>
							</div>
						</div>
						<div class="row othersubmit" >
							<div class="col">
								<div class="btn-wrap mt-4 justify-content-center text-center"> 
									<label class="btn-green-lg font-16 font600"  id="search-hospital-btn">Search</label>
								</div>
							</div>
						</div>
						<div class="row intersubmit"  style="display: none;" >
							<div class="col">
								<div class="btn-wrap mt-4 justify-content-center text-center"> 
									<!-- <label class="btn-green-lg font-16 font600"  id="search-hospital-btn">Search</label> -->
									<button type="button" id="search-inter-hospital-btn" class="btn-green-lg font-16 font600">Search</button>
								</div>
							</div>
						</div>

						<div class="row roundtripsubmit" style="display: none;">
						<div class="col">
								<div class="btn-wrap mt-4 justify-content-center text-center">
									<button type="submit" class="btn-green-lg font-16 font600">Search</button>
								</div>
							</div>
						</div>
					
						<div class="proceed-to-book-div" style="display: none;">
							<div class="row">
								<div class="col">
									<div class="btn-wrap mt-4 justify-content-center text-center">
										<button type="submit" class="btn-green-lg font-16 font600" id="proceed-to-book-home-delivery">Procced to Book</button>
									</div>
								</div>
							</div>
						</div>
						
						
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
include('booking_info_popup.php');
?>
<script>

	// var d1 = new Date();
	// var now_hour = d1.getUTCHours();
	// var today_date = new Date(d1.getUTCFullYear() + '-' + d1.getUTCMonth() + '-' + d1.getUTCDate() + ' 00:00:00');
	// if(now_hour >= '22'){

	// 	var today_date = d1.getUTCDate() + 1;
	// }else{
	// 	var today_date = d1.getUTCDate();
	// }

	


    $('input[type=radio][name=place_test]').change(function() {
	    if (this.value == '1') {
	        
	        $('#search-btn-div, #search-center-div, #time-selection, .othersubmit').show();
	        $('.proceed-to-book-div').hide();
	    }
	    else if (this.value == '2') {
	        $('#search-btn-div, #search-center-div, #time-selection, .othersubmit').hide();
	        $('.proceed-to-book-div').show();
	    }
	});

	$('#proceed-to-book-home-delivery').click(function() {
		var title = "Booking Information";
		var purpose_of_test = $("input[type='radio'][name='purpose_id']:checked").attr('data-purpose-type-name');
		var test_type = $("input[type='radio'][name='test_type']:checked").attr('data-test-type-name');
		var place_test = $("input[type='radio'][name='place_test']:checked").attr('data-place-test-name');
		var date = $('#booking_date').val();
		var time = $('#booking_time').val();
		var report_id = $('input[type=radio][name=test_type]:checked').val();
		//console.log('purpose_of_test', purpose_of_test, 'test_type', test_type, 'place_test', place_test, 'date' , date);
		if(report_id == undefined){
		    Notiflix.Notify.Failure('Test type is required');
		    return false;
	  	}
		if(date == ''){
			Notiflix.Notify.Failure('Select test date to procced.');
			return false;
		}
		/*if(time == ''){
			Notiflix.Notify.Failure('Select test time to procced.');
			return false;
		}*/
		//return false;
		$('#booking-information-popup #purpose-of-test').html(purpose_of_test);
		$('#booking-information-popup #purpose-of-test').parent().show();

		$('#booking-information-popup #test-type').html(test_type);
		$('#booking-information-popup #test-type').parent().show();

		$('#booking-information-popup #place-of-test').html(place_test);
		$('#booking-information-popup #place-of-test').parent().show();

		$('#booking-information-popup #date').html(date + ' '+ time);
		$('#booking-information-popup #date').parent().show();

		$('#booking-information-popup #title').html(title);

		//$('#complete-your-registration').hide();
	    $('#booking-information-popup').modal('show');
		return false;
	    
	});


    function change_date(type)
    {
    	if(type == 'monthly')
    	{
    		$('#weekly_date').hide();
    		$('#monthly_date').show();
    		$('.weekly_date').removeClass('active');
    		$('.weekly_date a').removeClass('text-underline');
    		$('.monthly_date').addClass('active');
    		$('.monthly_date a').addClass('text-underline');
    	}
    	else
    	{
    		$('#weekly_date').show();
    		$('#monthly_date').hide();
    		$('.monthly_date').removeClass('active');
    		$('.monthly_date a').removeClass('text-underline');
    		$('.weekly_date').addClass('active');
    		$('.weekly_date a').addClass('text-underline');
    	}
    }

    $(document).ready(function() {
	    $('#demo-month-week-view, #demo-one-month-view').on('click', function(e) {
	    	var year = $(this).find('.mbsc-calendar-year').text();
	    	var selected = $(this).find('.mbsc-selected').attr('aria-label');
	    	$('#booking_date_picker').val(selected+' '+year);
		})

		
		$(document).on("change","#country_id",function(){
			var csrf_token = $("#csrf_token").val();
			var purpose_id = $('input[name="purpose_id"]:checked').val();
			var country_id = $('#country_id').find(":selected").val();
			var days = $(this).find(':selected').attr('data-days');
			
			if(purpose_id != '' && country_id != '' && purpose_id == '3'){
				$.ajax({
					url: BASE_URL+'get-report-test',
					type: "POST",
					data:{
						purpose_id:purpose_id,
						csrf_token: csrf_token,
						country_id:country_id
					},
					success: function (returnData) {
							returnData = $.parseJSON(returnData);
							$('.add-fitfly .new-book1').show();
							$('.headingThreeDiv, #accordion-select01, .uk-address, .non-uk-address, .Exemption, .arrival_date, .shipping_date, .ship_add').css('display','none');
							var html = '';
							$.each(returnData  ,function(key,value){
									var checked = '';
									if(key == 0){
										checked = "";
									}
									html += '<li><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
							});
							$(".test_type_list").html(html);
					},
					error: function (xhr, ajaxOptions, thrownError) {
							console.log('error');
							Notiflix.Notify.Failure('Something went wrong');
					}
				}); 
			}
			
		});

		$("#internatonal-arrival-booking #vaccine_date,#internatonal-arrival-booking #date_of_birth").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true,
			todayBtn: true,
			endDate: "today"
		});
		
	
		$(".depature_datepicker").datepicker({
			format: 'dd-mm-yyyy',
        	autoclose: true,
			todayBtn: true,
			startDate: new Date(),
			
		}).on("changeDate",function(ev){

			$("#departure_time").timepicker("destroy");
		    $("#departure_time").val("");
			if($(this).val() == "<?=date('d-m-Y')?>"){
				$('#departure_time').timepicker({
			        timeFormat: 'HH:mm',
			        interval: 15,
			        minTime: "<?=date("H:i")?>",
			        dynamic: true,
			        dropdown: true,
			        scrollbar: true
			    });
			}else{
				$('#departure_time').timepicker({
			        timeFormat: 'HH:mm',
			        interval: 15,
			        dynamic: true,
			        dropdown: true,
			        scrollbar: true
	   		 	});
			}

			var purpose_id = $('input[name="purpose_id"]:checked').val();
			if(purpose_id == "3"){
				var selectDate = ev.date;
				var returnDate = $(".return_datepicker").datepicker('getDate');
				if(returnDate != null){
					var date1 = new Date(returnDate);
					var date2 = new Date(selectDate);
					if(date1 < date2){
						Notiflix.Notify.Failure("You can not select return date before departure date");
						$('.return_datepicker').datepicker('clearDates');
					}
				}

				var test_date = $(".test_datepicker").datepicker('getDate');
				if(test_date != null){
					var days = $("#country_id").find(':selected').attr('data-days');
					var depature_date= ev.date;
					var date1 = new Date(test_date);
					var date2 = new Date(depature_date);
					var Difference_In_Time = date2.getTime() - date1.getTime();
					var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
					Difference_In_Days = Math.round(Difference_In_Days);
					if(Difference_In_Days >= 1){
						if(Difference_In_Days > days){
							//Notiflix.Notify.Failure("Please select a test date that is at least "+days+" days before departure date.");
							$(".test_datepicker").val('');
						}
					}else{
						//Notiflix.Notify.Failure("Please select a test date that is at least "+days+" days before departure date.");
						$(".test_datepicker").val('');
					}
				}
			}
		});
		
		$(".test_datepicker").datepicker({
			format: "dd-mm-yyyy",
			// showMeridian: true,
			autoclose: true,
			startDate: new Date(),
			todayBtn: true,
			//startView: 2,
        	//forceParse: 0,
        	// showMeridian: 1,
        	// minuteStep: 15
		}).on("changeDate",function(ev){
			var purpose_id = $('input[name="purpose_id"]:checked').val();
			var days = $("#country_id").find(':selected').attr('data-days');
		    $(".timepicker").timepicker("destroy");
		    $(".timepicker").val("");
			if($(this).val() == "<?=date('d-m-Y')?>"){
				$('.timepicker').timepicker({
			        timeFormat: 'HH:mm',
			        interval: 15,
			        minTime: "<?=date("H:i")?>",
			        dynamic: true,
			        dropdown: true,
			        scrollbar: true
			    });
			}else{
				$('.timepicker').timepicker({
			        timeFormat: 'HH:mm',
			        interval: 15,
			        dynamic: true,
			        dropdown: true,
			        scrollbar: true
	   		 	});
			}

			if(purpose_id == "3"){
				var depature_date = $(".depature_datepicker").datepicker('getDate');
				var test_date = ev.date;
				var date1 = new Date(test_date);
				var date2 = new Date(depature_date);
				var Difference_In_Time = date2.getTime() - date1.getTime();
				var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
				Difference_In_Days = Math.round(Difference_In_Days);
				if(Difference_In_Days >= 1){
					if(Difference_In_Days > days){
						Notiflix.Notify.Failure("Please select a date that is at least "+days+" days before departure date.");
						$(this).val('');
					}
				}else{
					Notiflix.Notify.Failure("Please select a date that is at least "+days+" days before departure date.");
					$(this).val('');
				}
				
			}
		});

		$(".return_datepicker").datepicker({
			format: 'dd-mm-yyyy',
        	autoclose: true,
			todayBtn: true,
			startDate: new Date(),
		}).on("changeDate",function(ev){
			var purpose_id = $('input[name="purpose_id"]:checked').val();
			if(purpose_id == "3"){
				var returnDate = ev.date;
				var depature_date = $(".depature_datepicker").datepicker('getDate');
				var date1 = new Date(returnDate);
				var date2 = new Date(depature_date);
				if(returnDate != ""){
					if(date1 < date2){
						Notiflix.Notify.Failure("You can not select return date before departure date");
						$('.return_datepicker').datepicker('clearDates');
					}
				}
			}
		});
	});

	function get_full_date(d){ 
		var monthVar = d.getMonth() + 1;
		var date = (d.getDate() < 10 ? '0' : '') + d.getDate();
		var month = (monthVar < 10 ? '0' : '') + monthVar;
		var year = d.getFullYear();
		return date+'-'+month+'-'+year;
	}

</script>
<style type="text/css">
	.mbsc-calendar-labels{display: none;}
</style>

<!-- search by map -->
<script type="text/javascript">
	$('#map_tab').click(function(e){
		var purpose_id = $('input[type=radio][name=purpose_id]:checked').val();
	  var place_type = $('input[type=radio][name=place_test]:checked').val();


	  if(purpose_id == '2'){
	    Notiflix.Notify.Failure('Please select round trip or one way.');
	    return false;
	  }

	  var test_date = $('#booking_date').val();
	  var test_time = $('#booking_time').val();

	  var departure_date = $('#departure_date').val();
	  var departure_time = $('#departure_time').val();
	  var country_id = $('#country_id').val();

	  var postcode = $('#txtpostcode').val();
	  //postcode = postcode.replace(/ /g,'');


	  if(purpose_id == '4'){
	      if(country_id == ''){
	        Notiflix.Notify.Failure('Destination country is required');
	        return false;
	      }else if(departure_date == ''){
	        Notiflix.Notify.Failure('Departure date is required');
	        return false;
	      }else if(departure_time == ''){
	        Notiflix.Notify.Failure('Departure time is required');
	        return false;
	      }else if(test_date == ''){
	        Notiflix.Notify.Failure('Test date is required');
	        return false;
	      }else if(test_time == ''){
	        Notiflix.Notify.Failure('Test time is required');
	        return false;
	      }else{

	      }

	  }else if(purpose_id == '1' && place_type == '1'){
	      if(test_date == ''){
	        Notiflix.Notify.Failure('Test date is required');
	        return false;
	      }else if(test_time == ''){
	        Notiflix.Notify.Failure('Test time is required');
	        return false;
	      }else{

	      }

	  }else if(purpose_id == '3'){
	      if($("#country_id").val() == ''){
	        Notiflix.Notify.Failure('Country ID is required');
	        return false;
	      }else if($("#departure_date").val() == ''){
	        Notiflix.Notify.Failure('departure date is required');
	        return false;
	      }else if($("#departure_time").val() == ''){
	        Notiflix.Notify.Failure('departure time is required');
	        return false;
	      }else if($("#return_date1").val() == ''){
	        Notiflix.Notify.Failure('return date is required');
	        return false;
	      }else if($('input[type=radio][name=test_type]:checked').val() == ''){
	        Notiflix.Notify.Failure('Test type is required');
	        return false;
	      }else if(test_date == ''){
	        Notiflix.Notify.Failure('Test date is required');
	        return false;
	      }else if(test_time == ''){
	        Notiflix.Notify.Failure('Test time is required');
	        return false;
	      }

	  }else{
	      Notiflix.Notify.Failure('Something went wrong');
	      return false;
	  }

	  var report_id = $('input[type=radio][name=test_type]:checked').val();
	  var place_of_test_name = $('input[type=radio][name=place_test]:checked').attr('data-place-test-name');
	  
	  var purpose_of_test_name = $('input[type=radio][name=purpose_id]:checked').attr('data-purpose-type-name');
	  var test_type_name = $('input[type=radio][name=test_type]:checked').attr('data-test-type-name');
	  var test_price = $('input[type=radio][name=test_type]:checked').attr('data-test-type-price');
	    
	    $.ajax({
	        url: BASE_URL+'save_booking_session',
	        type: "POST",
	        data:{
	          place_type:place_type,
	          report_id:report_id,
	          purpose_id:purpose_id,
	          country_id:country_id,
	          place_of_test_name:place_of_test_name,
	          purpose_of_test_name:purpose_of_test_name,
	          test_type_name:test_type_name,
	          test_price:test_price,
	          test_date:test_date,
	          test_time:test_time,
	          return_date1:$("#return_date1").val(),
	          departure_date:departure_date,
	          departure_time:departure_time
	        },
	        success: function (returnData) {
	            returnData = $.parseJSON(returnData);
				if($("#post_code_tab").hasClass("active")){
    				window.location = BASE_URL+'search-map';
				}else{
					var regionId = $('.regionId option:selected').val();
					window.location = BASE_URL+'search?regionId='+regionId;
				}
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            Notiflix.Notify.Failure('Something went wrong');
	        }
	    });  

	});
	$('#map-tab').click(function(e){
		var test_date = $('#booking_date').val();
	  var test_time = $('#booking_time').val();

	  var departure_date = $('#departure_date').val();
	  var departure_time = $('#departure_time').val();
	  var country_id = $('#country_id').val();

	  // var postcode = $('#txtpostcode').val();
	  var postcode = $('#hospitalpostcode').val();

	  var purpose_id    = $('input[name=purpose_id]:checked').val();
	  var ukresident    = $("#passenger-status-radio input[name=status_radio1]:checked").val();
	  var shipping_method    = $("input[name=shipping_method]:checked").val();
	  var dest_country  = $('#internatonal-arrival-booking #dest_country').find(":selected").val();
	  var residency_flag    = $("input[name=residency_flag]:checked").val();
	  var resshipping_address ="";
	  
	  if (residency_flag == undefined) {
	    residency_flag ="";
	  }else if (residency_flag == "1") {
	    var resshipping_address = $('.resshipping_address').find(":selected").val();
	  }else{
	    // var resshipping_address ="";
	  }

	  var return_date = $('#return_date').val();
	  var return_time = $('#return_time').val();
	  var pickup_date = $('.pickup_date').val();
	  var ukshipping_date = $('.ukshipping_date1').val();
	  var ukshipping_postcode = $('.uk_isolation_postcode').val();
	   
	  if (ukresident == "1") {
	    var res_add = $('.residency_address').find(":selected").val();
	    // console.log(res_add);
	    if (res_add == undefined) { 
	      Notiflix.Notify.Failure('Please select home address.');
	      return false;
	    }
	    var nonukaddress1 = "";
	    var nonukaddress2 = "";
	    var nonukcity = "";
	    var nonresidency_postcode = "";
	    var nonukcountry = "";
	  }else{
	    var res_add = "";
	    var nonukaddress1 = $('.nonukaddress1').val();
	    var nonukaddress2 = $('.nonukaddress2').val();
	    var nonukcity = $('.nonukcity').val();
	    var nonresidency_postcode = $('.nonresidency_postcode').val();
	    var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
	    // console.log(res_add);
	    if (nonukaddress1 == "") { 
	      Notiflix.Notify.Failure('Please add address line 1.');
	      return false;
	    }
	    if (nonukaddress2 == "") {
	      Notiflix.Notify.Failure('Please add address line 2.');
	      return false;
	    }
	    if (nonukcity == "") {
	      Notiflix.Notify.Failure('Please add home city.');
	      return false;
	    }
	    if (nonresidency_postcode == "") {
	      Notiflix.Notify.Failure('Please add home postcode.');
	      return false;
	    }
	    if (nonukcountry == "") {
	      Notiflix.Notify.Failure('Please select home country.');
	      return false;
	    }
	  }

	  var is_visiting_country = $('input[name=is_visiting_country]:checked').val();
	  var is_excempted = $('input[name=is_excempted]:checked').val();
	  var vaccination_status = $("input[name=vaccine_status]:checked").val();
	  if (vaccination_status == undefined) {
	      Notiflix.Notify.Failure('Please select vaccination status.');
	      return false; 
	  }

	  if (is_visiting_country == "1") {
	    var visiting_country = $('#visiting_country').find(":selected").val();
	    if (visiting_country =="") {
	      Notiflix.Notify.Failure('Please select visiting country.');
	      return false;
	    }else{
	      visiting_country = visiting_country;
	    }
	  }else{
	      visiting_country = "";
	  }
	  if (is_excempted =="1") {
	    var excempted_cat = $('#excempted_cat').find(":selected").val();
	    if (excempted_cat == "") {
	      Notiflix.Notify.Failure('Please select exemption category.');
	      return false;
	    }
	  }else{
	    excempted_cat ="";
	  }

	  if(shipping_method == '1'){
	    if(pickup_date == ''){
		    Notiflix.Notify.Failure('Please select pickup date');
		  	ukshipping_date = "";
	      return false;
	    }
	  }else{
	  	// ukshipping_date = "";
	    pickup_date ="";
	  }

	var vaccine_date = $("#vaccine_date").val();
	if(vaccine_date == ""){
		Notiflix.Notify.Failure('Please select vaccine date.');
      return false; 
	}
	var date_of_birth = $("#date_of_birth").val();
	if(date_of_birth == ""){
		Notiflix.Notify.Failure('Please select date of birth.');
      return false; 
	}
	var is_medical_excempted = $('input[name=is_medical_excempted]:checked').val();
	var vaccine_id = "";
	if(vaccination_status != "Not Vaccinated"){
		vaccine_id = $('#vaccine_id').find(":selected").val();
		if (vaccine_id == undefined) {
      Notiflix.Notify.Failure('Please select vaccine type.');
      return false; 
  	}
	}
	var proof_document = "";
	if (is_medical_excempted =="yes") {
    	if( document.getElementById("proof_document").files.length == 0 ){
			Notiflix.Notify.Failure('Please select proof document.');
      		return false; 
		}else{
			var files = $('#proof_document')[0].files;
			proof_document = files[0];
		}
  	}

	  if (dest_country == "") {
	    Notiflix.Notify.Failure('Please select arriving country.');
	    return false;
	  }else if(return_date == ''){
	    Notiflix.Notify.Failure('Please enter return date.');
	    return false;
	  }else if(return_time == ''){
	    Notiflix.Notify.Failure('Please enter return time.');
	    return false;
	  }else{
	    var ukresident = $("#passenger-status-radio input[name=status_radio1]:checked").val();
	    var residency_postcode = $('.residency_postcode').val();
	    var residency_address = $('.residency_address').val();
	    var nonresidency_postcode = $('.nonresidency_postcode').val();
	    var nonukaddress1 = $('.nonukaddress1').val();
	    var nonukaddress2 = $('.nonukaddress2').val();
	    var nonukcity = $('.nonukcity').val();
	    var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
	    var dest_country  = $('#internatonal-arrival-booking #dest_country').find(":selected").val();
	    var vaccination_status = $("input[name=vaccine_status]:checked").val();
	    var report_id = $('input[type=radio][name=test_type]:checked').val();
	    var purpose_id = $('input[type=radio][name=purpose_id]:checked').val();
	    var place_of_test_name = $('input[type=radio][name=place_test]:checked').attr('data-place-test-name');
	    var purpose_of_test_name = $('input[type=radio][name=purpose_id]:checked').attr('data-purpose-type-name');
	    var test_type_name = $('input[type=radio][name=test_type]:checked').attr('data-test-type-name');
	    var test_price = $('input[type=radio][name=test_type]:checked').attr('data-test-type-price');
	    var csrf_token = $("#csrf_token").val();

		 var formData = new FormData();
	     formData.append('resshipping_address', resshipping_address);
	     formData.append('ukshipping_postcode', ukshipping_postcode);
	     formData.append('residency_flag', residency_flag);
	     formData.append('shipping_method', shipping_method);
	     formData.append('return_date', return_date);
	     formData.append('return_time', return_time);
	     formData.append('is_excempted', is_excempted);
	     formData.append('excempted_cat', excempted_cat);
	     formData.append('shipping_date', ukshipping_date);
	     formData.append('pickup_date', pickup_date);
	     formData.append('res_add', res_add);
	     formData.append('is_visiting_country', is_visiting_country);
	     formData.append('visiting_country', visiting_country);
	     formData.append('ukresident', ukresident);
	     formData.append('residency_postcode', residency_postcode);
	     formData.append('residency_address', residency_address);
	     formData.append('nonresidency_postcode', nonresidency_postcode);
	     formData.append('nonukaddress1', nonukaddress1);
	     formData.append('nonukaddress2', nonukaddress2);
	     formData.append('nonukcity', nonukcity);
	     formData.append('nonukcountry', nonukcountry);
	     formData.append('dest_country', dest_country);
	     formData.append('vaccination_status', vaccination_status);
	     formData.append('postcode', postcode);
	     formData.append('report_id', report_id);
	     formData.append('purpose_id', purpose_id);
	     formData.append('country_id', country_id);
	     formData.append('csrf_token', csrf_token);
	     formData.append('purpose_of_test_name', purpose_of_test_name);
	     formData.append('test_type_name', test_type_name);
	     formData.append('test_price', test_price);
	     formData.append('vaccine_date', vaccine_date);
	     formData.append('date_of_birth', date_of_birth);
	     formData.append('is_medical_excempted', is_medical_excempted);
	     formData.append('vaccine_id', vaccine_id);
	     formData.append('proof_document', proof_document);


	      $.ajax({
	          	url: BASE_URL+'save_booking_session',
	          	type: "POST",
	          	cache: false,
			  	contentType: false,
				processData: false,
          		dataType: 'json',
          		data:formData,
	          /*data:{
	            resshipping_address:resshipping_address,
	            ukshipping_postcode:ukshipping_postcode,
	            residency_flag:residency_flag,
	            shipping_method:shipping_method,
	            return_date:return_date,
	            return_time:return_time,
	            is_excempted:is_excempted,
	            excempted_cat:excempted_cat,
	            pickup_date:pickup_date,
	            shipping_date:ukshipping_date,
	            res_add:res_add,
	            is_visiting_country:is_visiting_country, 
	            visiting_country:visiting_country,
	            ukresident:ukresident,
	            residency_postcode:residency_postcode,
	            residency_address:residency_address,
	            nonresidency_postcode:nonresidency_postcode,
	            nonukaddress1:nonukaddress1,
	            nonukaddress2:nonukaddress2,
	            nonukcity:nonukcity,
	            nonukcountry:nonukcountry,
	            dest_country:dest_country,
	            vaccination_status:vaccination_status,
	            postcode:postcode,
	            report_id:report_id,
	            purpose_id:purpose_id,
	            country_id:country_id,
	            csrf_token:csrf_token,
	            purpose_of_test_name:purpose_of_test_name,
	            test_type_name:test_type_name,
	            test_price:test_price,
	            vaccine_date:vaccine_date,
            	date_of_birth:date_of_birth,
            	is_medical_excempted:is_medical_excempted,
            	vaccine_id:vaccine_id,
	          },*/
	       
	          success: function (returnData) {
	            //returnData = $.parseJSON(returnData);
				if($("#post_code_tab").hasClass("active")){
    				window.location = BASE_URL+'search-map';
				}else{
					var regionId = $('.regionId option:selected').val();
					window.location = BASE_URL+'search?regionId='+regionId;
				}
	        },
	          error: function (xhr, ajaxOptions, thrownError) {
	              console.log('error');
	              Notiflix.Notify.Failure('Something went wrong');
	          }
	      });  

	  }
	});
</script>
