<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/web/css/mobiscroll.javascript.min.css'); ?>">
<script src="<?php echo base_url('assets/web/js/mobiscroll.javascript.min.js'); ?>"></script>

<section class="new-booking py-5">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey border-r10 p-4 py-5 account-div-height domestic-radio-section">
					<form action="<?=base_url()?>search" id="search_hospital_frm" method="post" accept-charset="utf-8" autocomplete="off">	 	
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">
						<!-- start -->
						<div class="row">
							<div class="col-lg-12 mb-5">
								<div id="accordion">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Purpose Of Test</a>								   
									   </div>
									   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
									      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
									        <ul class="mb-3 p-0 radio-block" id="new-booking-radio">
											<?php
											if($purpose_type_response->status == true){
												$counter = 1; 
												foreach($purpose_type_response->data as $key=>$purpose_type_item){ ?>
													<li>
														<input type="radio" id="domestic<?=$counter?>" required="" name="purpose_id" value="<?=$purpose_type_item->id?>" <?=$key==0?'checked':''?> data-purpose-type-name="<?=$purpose_type_item->name?>">
														<label for="domestic<?=$counter?>" class="d-block d-xl-flex align-items-center">
															<p class="mb-0 font-15 font500"><?=$purpose_type_item->name?></p>  
														</label>
														<?php
														if(!empty($purpose_type_item->subcat)){
															$count = 1;
															foreach($purpose_type_item->subcat as $subcat_item){ ?>
																<ul class="ml-4 test-purpose d-none">
																	<li>
																		<input type="radio" id="domestic<?=$counter?><?=$count?>" required="" name="purpose_id" value="<?=$subcat_item->id?>" data-purpose-type-name="<?=$subcat_item->name?>">
																		<label for="domestic<?=$counter?><?=$count?>" class="d-block d-xl-flex align-items-center">
																		<p class="mb-0 font-15 font500"><?=$subcat_item->name?></p>  
																	</li>
																</ul><?php 
																$count++;
															} 
														} ?>
													</li><?php 
													$counter++;
												}
											} ?>

	                						</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" style="display: none;" id="select-destination-country">
								<div id="accordion03" class="new-book2">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
									   	 	<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Destination Country</a>								   
									   </div>
									   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion03">
									      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
										        <ul class="mb-3 p-0 radio-block input-field-ui">
										        	<select class="form-control select2" name="country_id" id="country_id" style="width:100% !important;">
										        	 	<option value="">Select Country</option>
													  	<?php
														  	if($country_list['status'] == true){
															  foreach($country_list['data'] as $key=>$country){  ?>
															  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																	<?php 
																}
															} 
														?>
													</select>
		                						</ul>
									      	</div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" id="test-type-booking">
								<div id="accordion02" class="new-book1">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Choose Test Type</a>
									   </div>
									   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion02">
									      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
									        <ul class="mb-3 p-0 radio-block test_type_list">
	                						</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 mb-5" id="type-of-delivery">
								<div id="accordion04">
									<div class="card border-0">
									   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
									   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Type of Delivery</a>								   
									   </div>
									   <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion04">
									      <div class="card-body bg-white  pt-5 px-3 px-md-4 pb-3">
									        <ul class="mb-3 p-0 radio-block">
											  <?php
											  if($place_test_response->status == true){
												  foreach($place_test_response->data as $key=>$place_test_item){ ?>
													<li>
														<input type="radio" id="place_test_<?=$place_test_item->id?>" name="place_test" value="<?=$place_test_item->id?>" <?=$key==0?'checked':''?> data-place-test-name="<?=$place_test_item->name?>">
														<label for="place_test_<?=$place_test_item->id?>" class="d-flex align-items-center"><p class="mb-0 font-15 font500"><?=$place_test_item->name?></label>
													</li><?php 
													} 
												} ?>
	                						</ul>
									      </div>
	   								    </div>
									</div>
								</div>
							</div>

							<div class="col-lg-12 mb-5 internation-arrival new-book5" style="display: none;" id="internatonal-arrival-booking">
								<div class="input-field-ui account-div-height residency-radio-section add-uk">
									<div class="row mt-3 mt-md-2">
										<div class="col-lg-12">
											<div id="accordion-select01" class="passenger">
											  <div class="card border-0 mb-5">
											   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 <a href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Passenger Residency Status</a>
											   </div>
											   <div id="collapseFive" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
											        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio">
							                          	<li class="mr-3 ">
									                     	<input type="radio" id="status1" name="status_radio1" value="1" checked="">
									                     	<label for="status1" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">UK Residents</p></label>
									                  	</li>
									                  	<li>
									                     	<input type="radio" id="status2" name="status_radio1" value="2">
									                     	<label for="status2" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">Non-UK Residents</p></label>
									                  	</li>
													</ul>
											      	</div>
												</div>
											</div>
										  </div>
										
										</div>
										<div class="col-lg-12">
											<div class="uk-address">
											  <div class="form-group mb-4">	
											  	<p class="t-black font-weight-bold font-17 mb-2">Please Provide Home Address</p>	
												 <input type="text" name="residency_postcode" value="WC1N 3AX" id="postcode1" placeholder="Home Address" class="search-address search-home-address residency_postcode form-control form-control-box add_search-icon mb-2">
												 	<select name="residency_address" class="residency_address form-control form-control-box select-angle-blue " id="home-address-dropdown">
												   </select>
											   </div>
												 
											</div>
											<div class="non-uk-address" >
												<div class="form-group mb-4">
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Address Line 1</label>
														<input type="text" name="nonukaddress1" placeholder="Enter Address" class="nonukaddress1 form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Address Line 2</label>
														<input type="text" name="nonukaddress2" placeholder="Enter Address" class="nonukaddress2 form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">City</label>
														<input type="text" name="nonukcity" placeholder="Enter City" class="nonukcity form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Post/Zip Code</label>
														<input type="text" name="nonresidency_postcode" placeholder="Enter Post/Zip Code" class="nonresidency_postcode form-control">
													</div>
													<div class="form-group mb-2">
														<label for="" class="t-violet font-weight-bold font-14 mb-0">Country</label>
														<ul class="mb-3 p-0 radio-block input-field-ui">
											        	<select class="form-control select2 nonukcountry" style="width:100% !important;" name="nonukcountry" >
											        	 	<option value="">Select Country</option>
														  	<?php
															  	if($country_list['status'] == true){
																  foreach($country_list['data'] as $key=>$country){ ?>
																  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																		<?php 
																	} 
																} 
															?>
														</select>
													</ul>
													</div>
													<div class="form-group mb-2">	
												  		<p class="t-black font-weight-bold font-17 mb-2">UK Isolation Address</p>	
													 	<input type="text" name="uk_isolation_postcode" value="WC1N 3AX" id="uk_isolation_postcode" placeholder="Home Address" class="search-address search-home-address uk_isolation_postcode form-control form-control-box add_search-icon mb-2">
													 	<select name="uk_isolation_address" class="uk_isolation_address form-control form-control-box select-angle-blue " id="uk_isolation_address">
													   </select>
												   </div>

												</div>
											</div>
										</div>								
									</div>
									<div class="row">
										<div class="col-lg-12">
										  	<div id="accordion-select01">
											   	<div class="card border-0 mb-5">
												    <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
												   	 	<a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Country Arriving From</a>								   
												    </div>
													<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      		<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
														    <ul class="mb-3 p-0 radio-block input-field-ui">
													        	<select class="form-control select2" id="dest_country" name="dest_country" class="dest_country" style="width:100% !important;">
													        	 	<option value="">Select Country Arriving From</option>
																  	<?php
																	  	if($country_list['status'] == true){
																		    foreach($country_list['data'] as $key=>$country){ ?>
																		  		<option value="<?php echo $country['id'];?>" ><?php echo $country['name'];?></option>
																				<?php 
																			} 
																		} 
																	?>
																</select>
								    						</ul>
													      </div>
													</div> 
											   	</div>
									    	</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">During Your Trip, Will You Be Visiting / Stopping Over Or Transiting Through Any Islands or Country?</label>
												<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_visiting_country_yes" name="is_visiting_country" value="1" checked="">
														<label class="mb-0 pointer t-blue" for="is_visiting_country_yes">Yes</label>
													</div>
													<div class="px-0 relative">
														<input type="radio" id="is_visiting_country_no" name="is_visiting_country" value="2">
														<label class="mb-0 pointer t-blue" for="is_visiting_country_no">No</label>
													</div>
												</div>
											</div>
									    </div>
									</div>
									<div class="row">
										<div class="col-lg-12" id="transiting_country">
										  <div id="accordion-select01">
										   		<div class="card border-0 mb-5">
											    	<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 		<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Transiting Country</a>   
											    	</div>
													<div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
											      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
											        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio">
						                              		<select class="form-control select2" id="visiting_country" name="visiting_country" style="width:100% !important;">
											        	 		<option value="">Select Transiting Country</option>
														  		<?php
															  	if($country_list['status'] == true){
																  	foreach($country_list['data'] as $key=>$country){ ?>
																  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
																		<?php 
																	} 
																}  ?>
															</select>
						    							</ul>
											      </div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-12">
											<div class="form-group mb-4">
												<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Do You Qualify For Any Exemption?</label>
												<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_excempted_yes" name="is_excempted" value="1" checked="">
														<label class="mb-0 pointer t-blue" for="is_excempted_yes">Yes</label>
													</div>
													<div class="px-0 relative mr-3">
														<input type="radio" id="is_excempted_no" name="is_excempted" value="2" >
														<label class="mb-0 pointer t-blue" for="is_excempted_no">No</label>
													</div>
												</div>
											</div>
									    </div>
									    <div class="col-lg-12" id="exemption_country">
										    <div id="accordion-select01">
										   		<div class="card border-0 mb-5">
											    	<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 		<a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Exemption Category</a>   
											    	</div>
													<div id="collapseTwo" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
												      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
												        	<ul class="mb-3 p-0 radio-block" id="passenger-status-radio1">
							                              		<select class="form-control select2" name="excempted_cat" id="excempted_cat" style="width:100% !important;">
												        	 		<option value="">Select Exemption Category</option>
															  		<?php
																  	if($exemptioncat_list['status'] == true){
																	  	foreach($exemptioncat_list['data'] as $key=>$cat){ ?>
																	  		<option value="<?php echo $cat['id'];?>"><?php echo $cat['name'];?></option>
																			<?php 
																		} 
																	}  ?>
																</select>
							    							</ul>
												      	</div>
													</div>
										   		</div>
									      	</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
										  <div id="accordion-select01">
										   <div class="card border-0 mb-5">
											    <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
											   	 	<a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">What is Your Covid-19 Vaccination Status?</a>   
											    </div>
												<div id="collapseThree" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
												    <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
												    	<div class="home-address-radio-btn w-100 align-items-center ">
															<div class="px-0  mr-3">
																<input type="radio" id="fully_vaccinated" name="vaccine_status" data-val="1" value="Fully Vaccinated" >
																<label class="mb-0 pointer t-blue" for="fully_vaccinated">Fully Vaccinated</label>
															</div>
															<div class="px-0 mr-3">
																<input type="radio" id="partially_vaccinated" name="vaccine_status" data-val="2" value="Partially Vaccinated" >
																<label class="mb-0 pointer t-blue" for="partially_vaccinated">Partially Vaccinated</label>
															</div>
															<div class="px-0  mr-3">
																<input type="radio" id="not_vaccinated" name="vaccine_status" data-val="3" value="Not Vaccinated" >
																<label class="mb-0 pointer t-blue" for="not_vaccinated">Not Vaccinated</label>
															</div>
														</div>
												    </div>
													</div>
										   		</div>
									      	</div>
										</div>
										<div class="col-lg-12">
										  	<div id="accordion-select01">
										   		<div class="card border-0 mb-5">
												    <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
												   	 	<a href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>
												    </div>
													<div id="collapseFour" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
												      	<div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
													        <ul class="mb-3 p-0 radio-block test_type_list">
	                										</ul>
													      </div>
														</div>
										   			</div>
								        		</div>
							    			</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="d-flex font-15 font-weight-bold t-black mb-0">
						                     		Arrival Date &amp; Time
						                     	</div>
											</div>
											<div class="col-lg-6 mt-2">
												<div class="form-group">
													<input type="text" name="return_date" id="return_date" class="form-control return_datepicker" placeholder="Select Date" readonly style="background-color: unset;">
												</div>
											</div>
											<div class="col-lg-6 mt-2">
												<div class="form-group">
													<input type="text" name="return_time" id="return_time" class="form-control timepicker" placeholder="Select Time" readonly style="background-color: unset;">
												</div>
											</div>									  	
										 	<div class="col-lg-12">
												<div class="form-group mb-4">
													<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Select Place to Collect Test Kit</label>
													<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
														<div class="px-0 relative mr-3">
															<input type="radio" id="shipping_method_clinic" name="shipping_method" value="1" checked="" >
															<label class="mb-0 pointer t-blue" for="shipping_method_clinic">Pickup from clinic</label>
														</div>
														<div class="px-0 relative mr-3">
															<input type="radio" id="shipping_method_home" name="shipping_method" value="2" >
															<label class="mb-0 pointer t-blue" for="shipping_method_home">Ship to UK Address</label>
														</div>
													</div>
												</div>
										    </div>
										    <div class="col-lg-12 non-uk-shipping-address ">
												<div class="form-group mb-4">
													<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Is the shipping address the same as your provide UK isolation address above?</label>
													<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
														<div class="px-0 relative mr-3">
															<input type="radio" id="residency_flag_yes" name="residency_flag" value="1" checked="" >
															<label class="mb-0 pointer t-blue" for="residency_flag_yes">Yes</label>
														</div>
														<div class="px-0 relative mr-3">
															<input type="radio" id="residency_flag_no" name="residency_flag" value="2" >
															<label class="mb-0 pointer t-blue" for="residency_flag_no">No</label>
														</div>
													</div>
												</div>
										    </div>
										    <div class="col-lg-12 uk-shipping-div uk-shipaddress" style="display:none;">
											   	<div class="form-group mb-4">	
												 	<input type="text" name="" value="WC1N 3AX" placeholder="" id="postcode2" class="form-control form-control-box add_search-icon mb-2 search-shipping-address">
												 	<select name="shipping-address-dropdown" id="shipping-address-dropdown" class="form-control form-control-box select-angle-blue resshipping_address">
												   	</select>
											   	</div>
											</div>
										    <div class="col-lg-12 pickup-date-center-div" >
											  	<div class="form-group mb-4">	
											  		<label class="t-black font-weight-bold font-17 mb-2">Pickup Date</label>	
												 	<input type="text" name="pickup_date" placeholder="26-10-2021" class="pickup_date form-control form-control-box mb-2 datepicker">
											  </div>
										 	</div>
										 	<div class="col-lg-12 uk-shipping-div" style="display:none;">
										 		<div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
									         	  	<div class="select-date">								 
													  	<div class="form-group position-relative">
											    			<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0 ukshipping_date" readonly style="background-color: unset;">Select Shipping Date</label>
											    	  	</div>
									         	   	</div>
								           		</div>
									            <div class="calender-view mb-3">
									             	<input type="text" id="datepicker" name="shipping_date" placeholder="Date" class="p-2 form-control datepicker">
									            </div>
									        </div>
											<div class="col-md-12 pickup-search-center-div">
												<div class="test-center-info">
													<p class="font-15 font-weight-bold t-black">Select A Pickup Centre</p>
													<ul class="nav nav-tabs border-0" id="myTab">
													  <li class="nav-item">
													    <a class="nav-link active border-0 font-15 t-grey" id="post-code-tab" data-toggle="tab" href="#post-code" role="tab" aria-controls="home" aria-selected="true">Search By Post Code</a>
													  </li>
													  <li class="nav-item">
													    <a class="nav-link border-0 t-grey font-15" id="country-tab" data-toggle="tab" href="#country" role="tab" aria-controls="profile" aria-selected="false">Search By Region</a>
													  </li>
													  <li class="nav-item">
													    <a class="nav-link border-0 t-grey font-15" id="map-tab" href="<?=base_url()?>search-map" role="tab" aria-controls="contact" aria-selected="false">Search By Map</a>
													  </li>
													</ul>
													<div class="tab-content border-r7 pt-4 pb-3 px-3 bg-white mt-3 shadow-3" id="myTabContent">
													  <div class="tab-pane fade show active" id="post-code" role="tabpanel" aria-labelledby="post-code-tab">
														<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
														<div class="form-group">
														    <input class="form-control font-12" type="text" name="hospitalpostcode" id="hospitalpostcode" placeholder="Enter Postcode" aria-label="Search">
														</div>
													  </div>
													  <div class="tab-pane fade" id="country" role="tabpanel" aria-labelledby="country-tab">
													  	<p class="mb-0 font-16 t-blue font-weight-bold">Region</p>
														<div class="form-group">
															<select name="regionId" class="form-control font-17 font600 t-black border-0 px-3 py-2 border-r14">
																<option value="" disabled="" selected="">Please select region</option>
																<?php foreach($county as $key => $value) { ?>
																	<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
																<?php } ?>
															</select>
														    <!-- <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search"> -->
														</div>
													  </div>
													  <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
													  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
														<div class="form-group">
														    <input class="form-control font-12" type="text" placeholder="Please enter postcode" name="mappostcode" aria-label="Search">
														</div>
													  </div>
													</div>
												</div>
											</div>										 	
										</div>
									<!-- <div class="row shipping-div" style="display:none;">
										<div class="col-lg-12 ">
										  <div class="non-uk-shipping-address">
										   <div class="form-group mb-4">	
										  	<p class="t-black font-weight-bold font-17 mb-2">Shipping Address</p>	
											 <input type="text" name="" value="WC1N 3AX" placeholder="" id="postcode2" class="form-control form-control-box add_search-icon mb-2 search-shipping-address">
											 <select name="shipping-address-dropdown" id="shipping-address-dropdown" class="form-control form-control-box select-angle-blue ">
												
											   </select>
										   </div>
											 
										   </div>
										</div>
									</div> -->
								</div>
							</div>	

						</div>
						<!-- End -->
						<div class="row mt-3" id="departure-date-time-selection" style="display: none;">
							<div class="col-md-12">
								<div class="d-flex font-15 font-weight-bold t-black mb-0">
		                     		Select Departure Date & Time
		                     	</div>
							</div>
							<div class="col-md-6 mt-2">
								<div class="form-group">
									<input type="text" name="" id="departure_date" class="form-control" placeholder="Select Date" readonly style="background-color: unset;">
								</div>
							</div>
							<div class="col-md-6 mt-2">
								<div class="form-group">
									<input type="text" name="" id="departure_time" class="form-control timepicker" placeholder="Select Time" readonly style="background-color: unset;">
								</div>
							</div>
						</div>
					
							
						<div class="row mt-3" id="date-time-selection" style="display: none;">
							<div class="col-md-12">
								<div class="d-flex font-15 font-weight-bold t-black mb-0">
		                     		Select Test Date
		                     	</div>
							</div>
							<div class="col-md-12 mt-2">
								<div class="form-group">
									<input type="text" name="" id="booking_date" class="form-control" placeholder="Select Date" readonly style="background-color: unset;">
								</div>
							</div>
						</div>

						<div class="row mt-3" id="time-selection" style="display: none;">
							<div class="col-md-12">
								<div class="d-flex font-15 font-weight-bold t-black mb-0">
		                     		Select Test Time
		                     	</div>
							</div>
							<div class="col-md-6 mt-2">
								<div class="form-group">
									<input type="text" name="" id="booking_time" class="form-control timepicker" placeholder="Select Time" readonly style="background-color: unset;">
								</div>
							</div>
						</div>
						
						<div class="row mt-3 " id="search-center-div" style="display: none;">
							<div class="col-md-12">
								<div class="test-center-info">
									<p class="font-15 font-weight-bold t-black">Select A Test Centre</p>
									<ul class="nav nav-tabs border-0" id="myTab">
									  <li class="nav-item">
									    <a class="nav-link active border-0 font-15 t-grey" id="post-code-tab" data-toggle="tab" href="#post-code" role="tab" aria-controls="home" aria-selected="true">Search By Post Code</a>
									  </li>
									  <li class="nav-item">
									    <a class="nav-link border-0 t-grey font-15" id="country-tab" data-toggle="tab" href="#country" role="tab" aria-controls="profile" aria-selected="false">Search By Region</a>
									  </li>
									  <li class="nav-item">
									    <a class="nav-link border-0 t-grey font-15" id="map-tab" href="<?=base_url()?>search-map" role="tab" aria-controls="contact" aria-selected="false">Search By Map</a>
									  </li>
									</ul>
									<div class="tab-content border-r7 pt-4 pb-3 px-3 bg-white mt-3 shadow-3" id="myTabContent">
									  <div class="tab-pane fade show active" id="post-code" role="tabpanel" aria-labelledby="post-code-tab">
										<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
										<div class="form-group">
										    <input class="form-control font-12" type="text" name="postcode" required="" placeholder="Enter Postcode" aria-label="Search">
										</div>
									  </div>
									  <div class="tab-pane fade" id="country" role="tabpanel" aria-labelledby="country-tab">
									  	<p class="mb-0 font-16 t-blue font-weight-bold">Region</p>
										<div class="form-group">
											<select class="form-control font-17 font600 t-black border-0 px-3 py-2 border-r14">
												<option value="" disabled="" selected="">Please select region</option>
												<?php foreach($county as $key => $value) { ?>
													<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
												<?php } ?>
											</select>
										    <!-- <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search"> -->
										</div>
									  </div>
									  <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
									  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
										<div class="form-group">
										    <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search">
										</div>
									  </div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="search-btn-div" style="display:none;">
							<div class="col">
								<div class="btn-wrap mt-4 justify-content-center text-center">
									<button type="button" id="search-inter-hospital-btn" class="btn-green-lg font-16 font600">Search</button>
								</div>
							</div>
						</div>						
						<div class="proceed-to-book-div">
							<div class="row">
								<div class="col">
									<div class="btn-wrap mt-4 justify-content-center text-center">
										<button type="submit" class="btn-green-lg font-16 font600" id="proceed-to-book-home-delivery">Procced to Book</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
	include('booking_info_popup.php');
?>
<script>

	// var d1 = new Date();
	// var now_hour = d1.getUTCHours();
	// var today_date = new Date(d1.getUTCFullYear() + '-' + d1.getUTCMonth() + '-' + d1.getUTCDate() + ' 00:00:00');
	// if(now_hour >= '22'){

	// 	var today_date = d1.getUTCDate() + 1;
	// }else{
	// 	var today_date = d1.getUTCDate();
	// }

    $('input[type=radio][name=place_test]').change(function() {
	    if (this.value == '1') {
	        
	        $('#search-btn-div, #search-center-div, #time-selection-div').show();
	        $('.proceed-to-book-div').hide();
	    }
	    else if (this.value == '2') {
	        $('#search-btn-div, #search-center-div, #time-selection-div').hide();
	        $('.proceed-to-book-div').show();
	    }
	});

	$('#proceed-to-book-home-delivery').click(function() {
		var title = "Booking Information";
		var purpose_of_test = $("input[type='radio'][name='purpose_id']:checked").attr('data-purpose-type-name');
		var test_type = $("input[type='radio'][name='test_type']:checked").attr('data-test-type-name');
		var place_test = $("input[type='radio'][name='place_test']:checked").attr('data-place-test-name');
		var date = $('#booking_date').val();
		var time = $('#booking_time').val();

		console.log('purpose_of_test', purpose_of_test, 'test_type', test_type, 'place_test', place_test, 'date' , date);

		if(date == ''){
			Notiflix.Notify.Failure('Select test date to procced.');
			return false;
		}
		/*if(time == ''){
			Notiflix.Notify.Failure('Select test time to procced.');
			return false;
		}*/
		//return false;
		$('#booking-information-popup #purpose-of-test').html(purpose_of_test);
		$('#booking-information-popup #purpose-of-test').parent().show();

		$('#booking-information-popup #test-type').html(test_type);
		$('#booking-information-popup #test-type').parent().show();

		$('#booking-information-popup #place-of-test').html(place_test);
		$('#booking-information-popup #place-of-test').parent().show();

		$('#booking-information-popup #date').html(date + ' '+ time);
		$('#booking-information-popup #date').parent().show();

		$('#booking-information-popup #title').html(title);
	    $('#booking-information-popup').modal('show');

	    
	});


    function change_date(type)
    {
    	if(type == 'monthly')
    	{
    		$('#weekly_date').hide();
    		$('#monthly_date').show();
    		$('.weekly_date').removeClass('active');
    		$('.weekly_date a').removeClass('text-underline');
    		$('.monthly_date').addClass('active');
    		$('.monthly_date a').addClass('text-underline');
    	}
    	else
    	{
    		$('#weekly_date').show();
    		$('#monthly_date').hide();
    		$('.monthly_date').removeClass('active');
    		$('.monthly_date a').removeClass('text-underline');
    		$('.weekly_date').addClass('active');
    		$('.weekly_date a').addClass('text-underline');
    	}
    }

    $(document).ready(function() {
	    $('#demo-month-week-view, #demo-one-month-view').on('click', function(e) {
	    	var year = $(this).find('.mbsc-calendar-year').text();
	    	var selected = $(this).find('.mbsc-selected').attr('aria-label');
	    	$('#booking_date_picker').val(selected+' '+year);
		})

		
	});


</script>
<style type="text/css">
	.mbsc-calendar-labels{display: none;}
</style>
