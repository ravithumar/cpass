<section class="add-card py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Payment</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="" method="get" accept-charset="utf-8">
						<div class="col-lg-10 px-0">

							<div class="">
								<ul class="payment-card-add payment-card-list row">
									<li class="col-lg-6 mb-4 bg-white border-r5 shadow-1" id="add-new-card-div">
										<div class="pointer align-items-center p-2 p-xl-3">
											<a href="javascript:void(0);" data-toggle="modal" data-target="#add-card" class="d-block text-center text-dark mb-0">Add New Card</a>
										</div>
									</li>
									<?php
									$existing_card = array();
									if(isset($cards) && count($cards) > 0)
										foreach ($cards as $key => $value) {
											?>
											<li class="col-lg-6 mb-4 <?php echo ($key == 0)?'active':''; ?>" data-id="<?php echo $value->id ?>">
												<div class="pointer cards-list d-flex align-items-center bg-white border-r5 shadow-1 p-2 p-xl-3">
													<div class="mr-3">
														<img src="<?=assets('web/images/visa-card.png')?>" alt="">
													</div>
													<div>
														<p class="t-black mb-0 font-16">**** **** **** <?php echo $value->last4; ?></p>
														<p class="t-grey mb-0 font-13">Expires <?php echo $value->exp_month.'-'.$value->exp_year; ?></p>
													</div>
												</div>
											</li>
											<?php
										}

									?>
									
									
								</ul>
								
							</div>
						</div>
						<div class="text-center pt-lg-5 mt-5">
							<a href="<?=base_url('add-cart')?>" title="" class="btn-blue m-2 d-none">Add Card</a>
							<a href="#" title="" id="pay-now" class="btn-green-lg m-2" data-booking-id="<?php echo $booking_id; ?>" data-booking-reference-id="<?php echo $booking_reference_id; ?>"  data-customer-id="<?php echo $customer_id; ?>">Pay Now</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal" tabindex="-1" role="dialog" id="add-card">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    	<form action="" method="get" accept-charset="utf-8" id="add-card-form">
	      <div class="modal-header">
	        <h5 class="modal-title">Add New Card</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        
	        	<div class="form-group col-lg-12">
				    <label for="name">Card holder name</label>
				    <input type="text" name="name" class="form-control" id="name" placeholder="Enter cardholder name" required>
				</div>
				<div class="form-group col-lg-12">
				    <label for="card_number">Card number</label>
				    <input type="text" name="card_number" class="form-control" data-parsley-type="number" id="card_number" placeholder="Enter card number" required>
				</div>
				<div class="form-group col-lg-12">
					<label for="expire">Expires</label>
					<input type="text" name="expire" value="" placeholder="Select expiry month/year" class="form-control expiry-month-year" id="expiry-month-year" required>
				</div>
				<div class="form-group col-lg-12">
					<label for="cvc_number">CVV</label>
					<input type="text" name="cvc_number" id="cvc_number" placeholder="Enter CVV" class="form-control" required data-parsley-minlength="3" data-parsley-maxlength="4" data-parsley-type="digits" data-parsley-minlength-message="Enter valid CVV" data-parsley-maxlength-message="Enter valid CVV">
				</div>
	        
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-success" value="Add">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </div>
	    </form>
    </div>
  </div>
</div>