<section class="py-5 review-confirm-page">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			//_pre($_SESSION);
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Review &amp; Confirm</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative">
					<div class="count-down position-absolute text-white text-center px-4 py-1 font-12 font600">You Have (<span class="countdown">10:00</span>) Remaining</div>
					<div class="review-detail">
						<div class="row">
							<div class="col-lg-10 col-xl-9">
								<div class="row">
									<?php
									if($purpose_id != 1){ 

										if($purpose_id == 3){ 
										 	$place_of_test_name = $session_data['test_place_name']; 
										}else{ 
											$place_of_test_name = $this->session->userdata('place_of_test_name'); 
										}
										if(!empty($place_of_test_name)){ 
										?>
										<div class="col-md-6">
											<div class="form-group">
												<label class="font-14 font600 t-light-blue mb-0">Place Of Test</label>
							   					<input class="form-control font-13 font500" type="text" readonly value="<?php  ?>">
											</div>
										</div>
									<?php } } ?>
									<div class="col-md-6">
										<div class="form-group">
											<label class="font-14 font600 t-light-blue mb-0">Purpose Of Test</label>
						   					<input class="form-control font-13 font500" type="text" readonly value="<?php if($purpose_id == 3){ echo $session_data['purpose_of_test_name']; } else{ echo $this->session->userdata('purpose_of_test_name'); } ?>">
										</div>
									</div>
								</div>
								<div class="member-list mt-3 mt-md-4">
									<p class="font-16 font-weight-bold t-blue">Member List</p>	
									<div class="row">
										<?php
										$total_member = 0;
										if(isset($members) && count($members) > 0){
											// _pre($members);
											foreach ($members as $key => $value) {
												if($value->booking_id == ''){
													continue;
												}else{
													$total_member++;
												}
												?>
												<div class="col-md-6">
													<div class="list-block bg-white border-r8 p-3 mb-3 mb-md-0">
														<p class="font-16 font600 t-blue mb-1">
															<?php echo $value->full_name; ?>
														</p>
														<div class="top-one d-block d-sm-flex d-md-block d-xl-flex justify-content-between">
															<div class="info">
																<p class="font-12 t-black font500 mb-0"><?=$value->email?></p>
																<p class="font-12 t-black font500 mb-0"><?=$value->phone?></p>
																<span class="d-block t-black font-12 font500"><em class="t-blue font500">Passport Number </em> : <?=$value->passport?></span>
																<span class="t-black d-block font-12 font500"><em class="t-blue font500">Date Of Birth</em>  : <?=$value->date_of_birth?></span>
															</div>
															<a href="<?php echo base_url('booking-edit-member/').$value->id.'/1'; ?>" class="t-blue edit text-underline font-12 font500">Edit Information</a>
														</div>
														<hr class="my-2">
														<div class="bottom-one">
															<div class="info">
																<div class="edits-one d-block d-xl-flex justify-content-between">
																	<span class="d-block t-black font-12 font500"><em class="t-blue font500">Test Type </em> : <?=($purpose_id == 3)? $session_data['test_type_name']:$this->session->userdata('test_type_name')?></span>
																	<a href="<?php echo base_url('details/edit'); ?>" class="t-blue edit text-underline font-12 font500">Edit Information</a>
																</div>
																<ul class="d-block d-lg-flex">
																	<?php
																	if($purpose_id == 3){
																		$test_date = $session_data['booking_date'];
																		$test_time = $session_data['booking_time'];
																		$date = date('d-m-Y',strtotime($test_date));
																		$time = date('H:i A',strtotime($test_time));
																	}else{
																		if ($this->session->userdata('test_date') == "") {
																			$date = "";
																		}else{
																			$test_date = $this->session->userdata('test_date');
																			$test_date_obj = DateTime::createFromFormat('d-m-Y', $test_date);
																			$date = $test_date_obj->format('d-m-Y');
																		}
																		
	
																		if($this->session->userdata('test_time') != ''){
																			$test_time = $this->session->userdata('test_time');
																			$test_time_obj = DateTime::createFromFormat('H:i', $test_time);
																			$time = $test_time_obj->format('H:i');
																		}else{
																			$time = '';
																		}
																	}
																	
																	//$time = $test_date_obj->format('H:i A');
																	?>
																	<?php if(!empty($date)){ ?>
																	<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Date </em>  : <?php echo $date; ?></span></li>
																	<?php } ?>
																	<?php if(!empty($time)){ ?>
																	<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Time</em>  : <?php echo $time; ?></span></li>
																	<?php } ?>
																</ul>
																<?php if(!empty($value->vaccine_status)){ ?>
																	<div class="edits-one d-block d-xl-flex justify-content-between">
																		<span class="d-block t-black font-12 font500"><em class="t-blue font500">What is Your Covid-19 Vaccination Status? </em> <br/> <?=$value->vaccine_status?></span>
																	</div>
																	
																<?php } 
																if($purpose_id == 1 && $this->session->userdata('place_type') == 1){ ?>
																	<div class="edits-one d-block d-xl-flex justify-content-between">
																		<span class="d-block t-black font-12 font500"><em class="t-blue font500">Clinic Name </em> <br/> <?=$this->session->userdata('place_of_test_name')?></span>
																	</div><?php
																}
																?>
															</div>
															
														</div>
													</div>
												</div>
												<?php
											}
										}
										?>
										
																	
									</div>
										
								</div>
								<?php
								// _pre($purpose_id);
								if($purpose_id == 3){
									$sub_total = $total_member * $session_data['clinic_price'];
									$test_name1 = $session_data['test_type_name'];
									$clinic_price = $session_data['clinic_price'];
									$test_name = isset($session_data['test_name']) ? $session_data['test_name'] : '';
									$test_price = isset($session_data['test_price']) ? $session_data['test_price'] : 0;
								}else{
									$sub_total = $total_member * $this->session->userdata('test_price');
									$test_name1 = isset($booking_data['test_type_name']) ? $booking_data['test_type_name'] : '';
									$clinic_price = isset($booking_data['test_price']) ? $booking_data['test_price'] : 0;
									$test_price = 0;
								}
								
								?>
								<div class="test-quote mt-3">
									<p class="font-16 font-weight-bold t-blue">Test Price</p>
									<ul>
										<?php if($purpose_id == 3){ ?>
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black">Fit to Fly <?php echo $test_name1 ?>(outbound) </span>
											
											<span class="d-block font-14 font-weight-bold t-blue">$<?php echo $clinic_price; ?></span>
										</li>
										<?php if(isset($test_name)){ ?>
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black">Fit to Fly <?php echo $test_name ?>(return) </span>
											
											<span class="d-block font-14 font-weight-bold t-blue">$<?php echo $test_price; ?></span>
										</li>
										<?php } } else { ?>
											<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black"><?php echo $test_name1 ?> </span>
											
											<span class="d-block font-14 font-weight-bold t-blue">$<?php echo $clinic_price; ?></span>
										</li>

										<?php } ?>
										<?php
										$tax = $this->session->userdata('tax_price');
										// $total = $sub_total + $tax;
										$total = $clinic_price+$test_price;
										?>
										<!-- <li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black">Tax</span><span class="d-block font-14 font-weight-bold t-blue">$<?php echo $tax; ?></span></li> -->
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-15 font-weight-bold t-green">Total</span><span class="d-block font-16 font-weight-bold t-green">$<?php echo $total; ?></span></li>
									</ul>	
								</div>
								
							</div>
							<div class="col-lg-2 col-xl-3"></div>
						</div>
						<div class="row mt3 mt-md-5 text-center">
							<div class="col">
								<div class="btn-wrap">
									<a href="<?=base_url('select-payment-card')?>" class="btn-green-lg font-16 font600">Pay Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	var timer2 = "10:00";
	var interval = setInterval(function() {
		var timer = timer2.split(':');
		//by parsing integer, I avoid all extra string processing
		var minutes = parseInt(timer[0], 10);
		var seconds = parseInt(timer[1], 10);
		--seconds;
		minutes = (seconds < 0) ? --minutes : minutes;
		if (minutes < 0) clearInterval(interval);
		seconds = (seconds < 0) ? 59 : seconds;
		seconds = (seconds < 10) ? '0' + seconds : seconds;
		$('.countdown').html(minutes + ':' + seconds);
		timer2 = minutes + ':' + seconds;
		if(minutes < 0){
			window.location.href = "<?=base_url("new-booking")?>"
		}
	}, 1000);
</script>
