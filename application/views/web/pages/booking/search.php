<section class="search-page py-3 py-md-5 mt-0 mt-md-4">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative">
					<ul class="filter d-flex position-absolute m-0 p-0">
						<li><a href="javascript:void(0)" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 mr-2"><img src="<?=assets('web/images/filter.svg')?>" alt="Filter" class="img-fluid" data-toggle="modal" data-target="#filter-options-popup"></a></li>
						<li><a href="<?=base_url()?>search-map" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2"><img src="<?=assets('web/images/location.svg')?>" alt="Filter" class="img-fluid"></a></li>
					</ul>
					<?php if(isset($list) && !empty($list)) { ?>
						<input type="hidden" name="purpose_of_test_name" id="purpose_of_test_name" value="<?php echo $this->session->userdata('purpose_of_test_name'); ?>">
						<!-- <input type="hidden" name="place_of_test_name" id="place_of_test_name" value="<?php //echo $this->session->userdata('place_of_test_name'); ?>"> -->
						<input type="hidden" name="test_type_name" id="test_type_name" value="<?php echo $this->session->userdata('test_type_name'); ?>">
						<?php if ($this->session->userdata('purpose_id') == "5") { 
							// echo "str";
							if ($this->session->userdata('shipping_date') == "") {  ?>
							<input type="hidden" name="test_date" id="test_date" value="<?php echo $this->session->userdata('pickup_date'); ?>">
							<?php }else{ ?>
							<input type="hidden" name="test_date" id="test_date" value="<?php echo $this->session->userdata('shipping_date'); ?>"> 
						<?php 	}
						 }else{ ?> 
							<input type="hidden" name="test_date" id="test_date" value="<?php echo $this->session->userdata('test_date'); ?>">
						<?php } ?>
						<input type="hidden" name="test_time" id="test_time" value="<?php echo $this->session->userdata('test_time'); ?>">
						<div class="row">
							<?php foreach($list as $key => $value) { ?>
								<div class="col-md-6">
									<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-lg-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
										<div class="media">
											<img src="<?php echo $value['profile_picture']; ?>" alt="Health" class="img-fluid border-r7">
										</div>
										<div class="content-wrapper pl-0 pl-lg-2">
											<div class="top-part pr-2">
											   <p class="font-16 font600 t-blue mb-0"><?php echo $value['company_name']; ?></p>
											   <span class="location d-flex align-items-center position-relative p-0"><img src="<?=assets('web/images/small-map.svg')?>" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3"><?php echo $value['address']; ?></span></span>
											    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline"><?php echo $value['avail_timeslot']; ?></span></label>									
											</div>
											<div class="bottom-part mt-2 d-block d-lg-flex align-items-center justify-content-between">
												<div class="price-area d-flex align-items-center">
													<p class="mb-0 t-green font-16 font600 mr-2"><?php echo '$'.$value['price']; ?></p>
													<p class="mb-0 t-light-black font-10">Price for Test</p>
												</div>
												<div class="btn-area position-absolute">
													<a href="#" class="btn-blueone book-now-booking-info" data-hospital-id="<?php echo $value['id']; ?>" data-place-of-test-name="<?php echo $value['company_name']; ?>">Book Now</a>
												</div>
											</div>
										 </div>
									</div>						
								</div>	
							<?php } ?>	 					
						</div>	
					<?php } else { ?>
						<div class="row">
							<div class="col-md-12">
								<p>No result found</p>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
	    </div>
	</div>	
</section>
<?php
include('booking_info_popup.php');
?>

<!-- filter-options-modal -->
<div class="modal fade filter-options-popup p-0" id="filter-options-popup" tabindex="-1" role="dialog" aria-labelledby="filter-options-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal650 modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Filter Options</h6>              
            </div>
            <div class="modal-body p-4">
				<form method="get" action="<?=base_url()?>search">
					<!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token"> -->
					<!-- <input type="hidden" name="is_filter" value="yes"> -->
					<?php
					if(isset($postcode)){ ?>
						<input type="hidden" name="postcode" value="<?=$postcode?>">
					<?php } ?>
					<?php
					if(isset($regionId)){ ?>
						<input type="hidden" name="regionId" value="<?=$regionId?>">
					<?php } ?>
					<div class="progress-block mb-3">
						<p class="font-15 font600 t-black mb-0">Distance</p>
						<p class="font-12 font600 t-blue"><?=$filter_data['min_dist']?> MILE TO <?=$filter_data['max_dist']?> MILE</p>
					<!--  <div class="progress border-r6">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="<?=$filter_data['min_dist']?>" aria-valuemax="<?=$filter_data['max_dist']?>"></div>
						</div>     -->
						<?php if(isset($max_distance)){ ?>
							<input id="distance_slider" type="text" name="distance_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_dist']?>" data-slider-max="<?=$filter_data['max_dist']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_dist']?>,<?=$max_distance?>]"/>

						<?php } else { ?>
						<input id="distance_slider" type="text" name="distance_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_dist']?>" data-slider-max="<?=$filter_data['max_dist']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_dist']?>,<?=$filter_data['max_dist']?>]"/>
						<?php } ?>
					</div>
					<div class="progress-block mb-3">
						<p class="font-15 font600 t-black mb-0">Price</p>
						<p class="font-12 font600 t-blue">$<?=$filter_data['min_price']?> TO $<?=$filter_data['max_price']?></p>
						<!-- <div class="progress  border-r6">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="<?=$filter_data['min_price']?>" aria-valuemax="<?=$filter_data['max_price']?>"></div>
						</div>     -->
						<?php if(isset($max_price)){ ?>
							<input id="price_slider" type="text" name="price_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_price']?>" data-slider-max="<?=$filter_data['max_price']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_price']?>,<?=$max_price?>]"/>

						<?php } else { ?>
						<input id="price_slider" type="text" name="price_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_price']?>" data-slider-max="<?=$filter_data['max_price']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_price']?>,<?=$filter_data['max_price']?>]"/>
						<?php } ?>
					</div>
				
					<!-- <ul class="d-flex mb-3 p-0 radio-block">
						<?php
						if(!empty($filter_data['reports'])){
							$counter = 1;
							foreach($filter_data['reports'] as $report_type){ ?>
								<li class="mr-3">
									<input type="radio" id="test<?=$counter?>" name="radio-group">
									<label for="test<?=$counter?>" class="font-14 font500"><?=$report_type['name']?></label>
								</li><?php 
								$counter++;
							} 
						} ?>
					</ul> -->
							
									
					<div class="btn-wrap text-center mt-4">
						<button type="submit" class="btn-green-lg border-r8 font-17 font600 mb-2 mb-sm-0 mr-0 mr-sm-3">Ok</button>
						<button type="button" class="btn-red-lg border-r8 font-17 font600 ml-0 ml-sm-3" data-dismiss="modal" aria-label="Close">Cancel</button>
					</div>    
				</form>          
            </div>
        </div>
    </div>
</div>
<style>
	.slider{
		width: 100% !important;
	}
	.slider-track,.slider-selection {
    	background: #187932 !important;
	}
</style>
<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-slider.css');?>">
<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-slider.min.css');?>">
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?=assets('web/js/bootstrap-slider.js')?>"></script>
<script src="<?=assets('web/js/bootstrap-slider.min.js')?>"></script>
<script>
	$(document).ready(function(){

		var distance_slider = new Slider("#distance_slider");
		var price_slider = new Slider("#price_slider");

		$(document).on("click",".open_fit_to_fly_popup",function(){
			var closets = $(this).closest(".content-wrapper");
			var company_name = closets.find(".company_name").text();

			var clinic_id = $(this).data("id");
			var price = $(this).data("price");
			$(".clinic_id").val(clinic_id);
			$(".clinic_price").val(price);
			$(".place_of_test_name").html(company_name);
			$(".test_place_name").val(company_name);
			$("#fit-to-fly-popup").modal("show");
		});
	});
</script>