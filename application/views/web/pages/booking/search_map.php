<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<section class="search-page py-3 py-md-5 mt-0 mt-md-4">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 mb-5 mb-md-0">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-5">
						<li class="active"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="#" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="#" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">				
					
					<div class="map-wrapper  position-relative">					
					  <ul class="filter d-flex position-absolute m-0 p-0">
					    <li><a href="#" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 mr-2"><img src="<?=assets('web/images/filter.svg')?>" alt="Filter" class="img-fluid" data-toggle="modal" data-target="#filter-options-popup"></a></li>
						<!-- <li><a href="<?=base_url()?>search" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2 mr-4"><img src="<?=assets('web/images/list.svg')?>" alt="Filter" class="img-fluid"></a></li> -->
						

						<!-- <li><a href="javascript:history.go(-1);" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2 mr-4"><img src="<?=assets('web/images/list.svg')?>" alt="Filter" class="img-fluid"></a></li> -->

						<?php 
						$current_url = current_url();
						 if( substr($current_url, strrpos($current_url, '/') + 1) == 'roundtrip-search') { ?>
						 	<form id="myform" method="post" action="<?=base_url()?>roundtrip-search"></form>
							<li><a onclick="document.myform.submit()" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2 mr-4"><img src="<?=assets('web/images/list.svg')?>" alt="Filter" class="img-fluid"></a></li>
						<?php } else { ?>

						<li><a href="javascript:history.go(-1)" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2 mr-4"><img src="<?=assets('web/images/list.svg')?>" alt="Filter" class="img-fluid"></a></li>

						<?php } ?>
						<li>
							<input class="form-control w-500" name="address" id="autocomplete" placeholder="Choose Location" type="text" autocomplete="off"/>
                            <input type="hidden" class="form-control" name="latitude" id="latitude" />
                            <input type="hidden" class="form-control" name="longitude" id="longitude" />
                            <input type="hidden" class="form-control" name="street" id="street" value="" />
                            <input type="hidden" class="form-control" name="city" id="city" value="" />
                            <input type="hidden" class="form-control" name="state" id="state" value="" />
						</li>
					  </ul>
					  <div id="map" style="width: 100%; height: 700px;">
					  	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2962.0848154592336!2d-88.37567818425829!3d42.062801361813726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880f1a77406562eb%3A0x69e243ee4864c3b6!2sRidgewood%20Ln%2C%20Elgin%2C%20IL%2060124%2C%20USA!5e0!3m2!1sen!2sin!4v1629949627987!5m2!1sen!2sin" width="100%" height="780" style="border:0;" allowfullscreen="" loading="lazy" class="border-r12"></iframe>
					  </div>  
					  
					</div>						
			</div>
	    </div>
	</div>	
</section>
<style type="text/css">
	.w-500 {width: 500px;}
	.slider{
		width: 100% !important;
	}
	.slider-track,.slider-selection {
    	background: #187932 !important;
	}
</style>

<!-- filter-options-modal -->
<div class="modal fade filter-options-popup p-0" id="filter-options-popup" tabindex="-1" role="dialog" aria-labelledby="filter-options-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal650 modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Filter Options</h6>              
            </div>
            <div class="modal-body p-4">
				<form method="get" action="<?=base_url()?>search">
					<!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token"> -->
					<!-- <input type="hidden" name="is_filter" value="yes"> -->
					<?php
					if(isset($postcode)){ ?>
						<input type="hidden" name="postcode" value="<?=$postcode?>">
					<?php } ?>
					<?php
					if(isset($regionId)){ ?>
						<input type="hidden" name="regionId" value="<?=$regionId?>">
					<?php } ?>
					<div class="progress-block mb-3">
						<p class="font-15 font600 t-black mb-0">Distance</p>
						<p class="font-12 font600 t-blue"><?=$filter_data['min_dist']?> MILE TO <?=$filter_data['max_dist']?> MILE</p>
					<!--  <div class="progress border-r6">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="<?=$filter_data['min_dist']?>" aria-valuemax="<?=$filter_data['max_dist']?>"></div>
						</div>     -->
						<input id="distance_slider" type="text" name="distance_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_dist']?>" data-slider-max="<?=$filter_data['max_dist']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_dist']?>,<?=$filter_data['max_dist']?>]"/>
					</div>
					<div class="progress-block mb-3">
						<p class="font-15 font600 t-black mb-0">Price</p>
						<p class="font-12 font600 t-blue">$<?=$filter_data['min_price']?> TO $<?=$filter_data['max_price']?></p>
						<!-- <div class="progress  border-r6">
							<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="<?=$filter_data['min_price']?>" aria-valuemax="<?=$filter_data['max_price']?>"></div>
						</div>     -->
						<input id="price_slider" type="text" name="price_filter" class="span2" value="" data-slider-min="<?=$filter_data['min_price']?>" data-slider-max="<?=$filter_data['max_price']?>" data-slider-step="1" data-slider-value="[<?=$filter_data['min_price']?>,<?=$filter_data['max_price']?>]"/>
					</div>
				
					<!-- <ul class="d-flex mb-3 p-0 radio-block">
						<?php
						if(!empty($filter_data['reports'])){
							$counter = 1;
							foreach($filter_data['reports'] as $report_type){ ?>
								<li class="mr-3">
									<input type="radio" id="test<?=$counter?>" name="radio-group">
									<label for="test<?=$counter?>" class="font-14 font500"><?=$report_type['name']?></label>
								</li><?php 
								$counter++;
							} 
						} ?>
					</ul> -->
							
									
					<div class="btn-wrap text-center mt-4">
						<button type="submit" class="btn-green-lg border-r8 font-17 font600 mb-2 mb-sm-0 mr-0 mr-sm-3">Ok</button>
						<button type="button" class="btn-red-lg border-r8 font-17 font600 ml-0 ml-sm-3" data-dismiss="modal" aria-label="Close">Cancel</button>
					</div>    
				</form>          
            </div>
        </div>
    </div>
</div>

<div class="modal fade fit-to-fly-popup p-0" id="search_map_popup" tabindex="-1" role="dialog" aria-labelledby="fit-to-fly-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content border-r6 shadow-1">
			<div class="modal-header">
			<h6 class="t-blue font-weight-bold mb-0">Order Information</h6>              
			</div>
			<div class="modal-body p-4">
				<form method="post" action="<?=base_url()?>fit-to-fly-return">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">
					<input type="hidden" name="clinic_id" class="clinic_id" value="">
					<input type="hidden" name="clinic_price" class="clinic_price" value="">
					<input type="hidden" name="country_name" class="country_name" value="">
					<input type="hidden" name="test_type_name" class="test_type_name" value="">
					<input type="hidden" name="test_place_name" class="test_place_name" value="">
					<div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
						<div class="left-side">
							<p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
							<p class="font-13 t-black mb-0 purpose_of_test_name"></p>
						</div>
						<div class="right-side text-right country_div" style="display: none;">
							<p class="font-14 font600 t-blue mb-0">Country</p>
							<p class="font-13 t-black mb-0 country_name"></p>
						</div>
					</div> 
					<div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
						<div class="left-side">
							<p class="font-14 font600 t-blue mb-0">Test Type</p>
							<p class="font-13 t-black mb-0 test_type_name"></p>
						</div>
						<div class="right-side text-right">
							<p class="font-14 font600 t-blue mb-0">Place of test</p>
							<p class="font-13 t-black mb-0 place_of_test_name"></p>
						</div>
						
					</div>
					<div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
						<div class="left-side">
							<p class="font-14 font600 t-blue mb-0">Date</p>
							<p class="font-13 t-black mb-0 test_date"></p>
						</div>
						<div class="right-side text-right">
							<p class="font-14 font600 t-blue mb-0">Time</p>
							<p class="font-13 t-black mb-0 test_time"></p>
						</div>
					</div>      
									
					<div class="btn-wrap text-center mt-4">
						<div class="btn-area fit_to_fly_btn" style="display: none;">
							<button type="submit" class="btn-green-lg font-17 font600 mb-3">Proceed To Next Step</button>
						</div>
						<div class="btn-area complete_reg_btn">
							<a href="javascript:void(0)" class="btn-green-lg font-17 font600 mb-3" style="">Complete Your Registration</a>
						</div>
						<div class="btn-area">
							<button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
						</div>
					</div>       
				</form>       
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-slider.css');?>">
<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap-slider.min.css');?>">
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?=assets('web/js/bootstrap-slider.js')?>"></script>
<script src="<?=assets('web/js/bootstrap-slider.min.js')?>"></script>
<script>
	$(document).ready(function(){

		var distance_slider = new Slider("#distance_slider");
		var price_slider = new Slider("#price_slider");

		$(document).on("click",".open_fit_to_fly_popup",function(){
			var closets = $(this).closest(".content-wrapper");
			var company_name = closets.find(".company_name").text();

			var clinic_id = $(this).data("id");
			var price = $(this).data("price");
			$(".clinic_id").val(clinic_id);
			$(".clinic_price").val(price);
			$(".place_of_test_name").html(company_name);
			$(".test_place_name").val(company_name);
			$("#fit-to-fly-popup").modal("show");
		});
	});
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key');?>&libraries&libraries=places"></script>

<script type="text/javascript">
	
  google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#street').val(street);
            $('#city').val(city);
            $('#state').val(state);
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());

           

			var tourStops = [];
			var centerLat = pickup_place.geometry.location.lat();
			var centerLng = pickup_place.geometry.location.lng();
			$.ajax({
				url: BASE_URL+'get-clinic-from-map',
				type: "POST",
				async:false,
				data:{
					lat:pickup_place.geometry.location.lat(),
					lng: pickup_place.geometry.location.lng(),
				},
				dataType:"json",
				success: function (returnData) {
					tourStops = returnData.data;
					if(returnData.length > 0){
						centerLat = returnData.data[0][0]['lat'];
						centerLng = returnData.data[0][0]['lng'];
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {

				}
			}); 

			//var latlng = new google.maps.LatLng(pickup_place.geometry.location.lat(),pickup_place.geometry.location.lng());
			var latlng = new google.maps.LatLng(centerLat,centerLng);
			var map = new google.maps.Map(document.getElementById('map'), {
		      center: latlng,
		      zoom: 14
		    });

			//var latlngmarker = new google.maps.LatLng([pickup_place.geometry.location.lat(),pickup_place.geometry.location.lng()],[51.504189,-3.376255]);
			
			/* tourStops = [
				[{ lat: 51.504189, lng: -3.376255 }, "G-16 corona Enterprise"],
				[{ lat: 53.805691, lng: -1.523557 }, "G-16 Enterprise"],
				[{ lat: 51.453476, lng: -2.615936 }, "Family Care Centre"],
			]; */
			
			console.log(tourStops);


			var infowindow = new google.maps.InfoWindow();
			//var latlngmarker = { lat: 51.504189, lng: -3.376255 };

			tourStops.forEach(([position, title,address,clinic_id,purpose_id,purpose_of_test_name,test_type_name,test_date,test_time,clinic_price], i) => {
				const marker = new google.maps.Marker({
					position:position,
					map: map,
					title: `${i + 1}. ${title}`,
					label: `${i + 1}`,
					//optimized: false,
					draggable: false,
					anchorPoint: new google.maps.Point(0, -29)
				});

				google.maps.event.addListener(marker, 'click', function() {
					$(".purpose_of_test_name").html(purpose_of_test_name);
					$(".test_type_name").html(test_type_name);
					$(".test_type_name").val(test_type_name);
					$(".test_place_name").val(title);
					$(".place_of_test_name").html(title);
					$(".test_date").html(test_date);
					$(".test_time").html(test_time);
					$(".clinic_id").val(clinic_id);
					$(".clinic_price").val(clinic_price);
					$(".complete_reg_btn a").attr("href","<?=base_url()?>save-hospital-id/"+clinic_id);
					if(purpose_id == 3){
						$(".fit_to_fly_btn").show();
						$(".complete_reg_btn").hide();
					}else{
						$(".complete_reg_btn").show();
						$(".fit_to_fly_btn").hide();
					}
					var booking_popup = '<a href="javascript:void(0)" class="btn-blueone book-now-map-booking-info" data-hospital_id="'+clinic_id+'" data-place_of_test_name="'+title+'" style="margin-top:20px">Book Now</a>';
					var iwContent = '<div id="iw_container">';
					iwContent += '<div class="iw_title"><b>Clinic Name</b> : '+title+'<br/><b>Address: </b> '+address+'</div>';
					iwContent += booking_popup;
					iwContent += '</div>';
					// including content to the infowindow
					infowindow.setContent(iwContent);
					// opening the infowindow in the current map and at the current marker location
					infowindow.open(map, marker);
				});

			});
		   

		    /* var marker = new google.maps.Marker({
		      map: map,
		      position: latlngmarker,
		      draggable: false,
		      anchorPoint: new google.maps.Point(0, -29)
		   }); */
		    
		    /* google.maps.event.addListener(marker, 'click', function() {
		       var iwContent = '<div id="iw_container">' +
		       '<div class="iw_title"><b>Location</b> : '+pickup_place+'</div></div>';
		      // including content to the infowindow
		      infowindow.setContent(iwContent);
		      // opening the infowindow in the current map and at the current marker location
		      infowindow.open(map, marker);
		    }); */

        });
    });

  	google.maps.event.addDomListener(window, 'load', initialize);

  	function initialize() 
  	{
  		var latlng = new google.maps.LatLng(51.5121951,-0.08603140000000001);
	    var map = new google.maps.Map(document.getElementById('map'), {
	      center: latlng,
	      zoom: 13
	    });
	    var marker = new google.maps.Marker({
	      map: map,
	      position: latlng,
	      draggable: false,
	      anchorPoint: new google.maps.Point(0, -29)
	   });
	    var infowindow = new google.maps.InfoWindow();   
	    google.maps.event.addListener(marker, 'click', function() {
	      var iwContent = '<div id="iw_container">' +
	      '<div class="iw_title"><b>Location</b> : Noida</div></div>';
	      // including content to the infowindow
	      infowindow.setContent(iwContent);
	      // opening the infowindow in the current map and at the current marker location
	      infowindow.open(map, marker);
	    });
  	}

	  $(document).on("click",".book-now-map-booking-info",function(){
		var place_test_name = $(this).data("place_of_test_name");
		var hospital_id = $(this).data("hospital_id");
		$("#search_map_popup").modal("show");
	});
</script>
