<section class="add-member py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4  shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Time Slot</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative d-flex flex-column justify-content-between">
					<div>
						<div class="row">
							<div class="col-lg-4 col-md-12">
								<div class="form-group">
									<label class="font-14 font600 t-light-blue mb-0">Place Of Test</label>
									<input class="form-control font-12" type="text" placeholder="" value="Integrative Health">
								</div>
							</div>
							<div class="col-lg-4 col-md-12">
								<div class="form-group position-relative">
									<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font-14 font600">Test Type</label>
									<div class="select position-relative">
										<select class="form-control padding-r30 font-14" id="exampleFormControlSelect1">
											<option>Antigen Lateral flow</option>
											<option>Antigen Lateral flow</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-12">
								<div class="form-group position-relative">
									<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font-14 font600 d-block">Date</label>
									<input type="text" id="datepicker" class="form-control" value="25 Aug 2021">
									<div class="cal-icon position-absolute">
										<img src="images/calendar.svg" alt="" class="img-fluid">
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-3">
							<div class="col">
								<p class="t-light-blue font-weight-bold font-16">Select Time Slot</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="time-slot d-flex flex-wrap">
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">9:15 AM</a></li>
									<li class="time-green"><a href="#">9:30 AM</a></li>
									<li class="time-border"><a href="#">9:45 AM</a></li>
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">10:15 AM</a></li>
									<li class="time-border"><a href="#">10:30 AM</a></li>
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">11:00 AM</a></li>
									<li class="time-border"><a href="#">11:15 AM</a></li>
									<li class="time-border"><a href="#">11:30 AM</a></li>
									<li class="time-border"><a href="#">11:45 AM</a></li>
									<li class="time-border"><a href="#">12:00 PM</a></li>
									<li class="time-border"><a href="#">12:15 PM</a></li>
									<li class="time-border"><a href="#">12:30 PM</a></li>
									<li class="time-border"><a href="#">12:45 PM</a></li>
									<li class="time-border"><a href="#">01:15 PM</a></li>
									<li class="time-border"><a href="#">01:15 PM</a></li>
									<li class="time-border"><a href="#">01:15 PM</a></li>
									<li class="time-border"><a href="#">01:15 PM</a></li>
									<li class="time-border"><a href="#">01:15 PM</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="row mt-3 mt-md-5">
						<div class="col-md-12">
							<div class="btn-wrap text-center">
								<a href="<?=base_url()?>review-confirm" class="btn-green-lg">Submit</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>