<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Us</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center">
	  	<div class="row text-center w-100">
			<div class="col-md-6 offset-md-3">
				<div class="col-md-12 p-4 p-lg-5 align-items-center">
					<img src="<?=assets("images/logo/clinic.png")?>" class="img-responsive">
					<h4 class="mb-5 text-black">G16 Global Covid Health Passport</h4>
					<h4 class="text-black">About Us</h4>
				</div>
				<div class="col-md-12">
					<p class="text-left">G16 Global Ltd has over a decade of experience in the UK, working with with world leading manufacturers testing for infectious diseases and our selected products are of the highest quality and meet all ISO standards enabling us to provide our clients the highest quality products and professional service.</p>
				</div>
				<div class="col-md-12">
					<p class="text-left"><b>FEBRUARY 2020:</b>  The first company to introduce the COVID-19 antibody tests to the UK.</p>
					<p class="text-left"><b>JUNE 2020:</b> The first company to trial and offer lateral flow antigen testing in the UK.</p>
					<p class="text-left"><b>NOVEMBER 2020:</b>  The first company in Europe to open a fully functioning COVID test facility at an airport</p>
					<p class="text-left"><b>JANUARY 2021:</b>  The first company to offer RT-PCR saliva spit testing for COVID-19.</p>
				</div>
				<div class="row text-center">
					<a href="<?=base_url('data-privacy/'.$user_id)?>" class="btn-green-lg d-block font600 border-r8" style="margin:0 auto">Continue</a>
				</div>
			</div>
	 	</div>
	</div>
<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
</body>
</html>

