<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Us</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	<style>
		.points li{
			list-style-type: disc;
		}
	</style>
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center">
	  	<div class="row text-center w-100">
			<div class="col-md-6 offset-md-3">
				<div class="col-md-12 p-4 p-lg-5 align-items-center">
					<img src="<?=assets("images/logo/clinic.png")?>" class="img-responsive">
					<h4 class="mb-5 text-black">G16 Global Covid Health Passport</h4>
					<h4 class="text-black">Registration and Data Privacy</h4>
				</div>
				<div class="col-md-12">
					<p class="text-left">The lawful and correct treatment of personal information is vital to the successful operation of the  <strong>G16 Global Ltd.</strong> It helps maintain confidence within the <strong>G16 Global Ltd</strong>, and the individuals  
with whom it deals.</p>
				</div>
				<div class="col-md-12">
					<p class="text-left">Therefore, the <strong>G16 Global Ltd</strong> will demonstrate its compliance with the GDPR by  ensuring personal information is: </p>
					<ul class="text-left points offset-md-1">
						<li>Processed lawfully, fairly and in a transparent manner in relation to individuals;</li>
						<li>Collected for specified, explicit and legitimate purposes and not further processed in a  manner that is incompatible with those purposes; </li>
						<li>Adequate, relevant and limited to what is necessary in relation to the purposes for  which they are processed; </li>
						<li>Accurate and, where necessary, kept up to date;  </li>
						<li>Kept in a form which permits identification of data subjects for no longer than is  necessary for the purposes for which the personal data are processed; and </li>
						<li>Processed in a manner that ensures appropriate security of the personal data, Prepared, Approved and Issued By Lead Clinical and Group Information Governance Officer including protection against unauthorised or unlawful processing and against accidental  loss, destruction or damage, using appropriate technical or organisational measures. </li>
					</ul>
				</div>
				<div class="row text-center">
					<a href="<?=base_url('registration-consent/'.$user_id)?>" class="btn-green-lg d-block font600 border-r8" style="margin:0 auto">I understand</a>
				</div>
			</div>
	 	</div>
	</div>
<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
</body>
</html>

