<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Us</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	<style>
		.points li{
			list-style-type: disc;
		}
	</style>
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center">
	  	<div class="row text-center w-100">
			<div class="col-md-6 offset-md-3">
				<div class="col-md-12 p-4 p-lg-5 align-items-center">
					<img src="<?=assets("web/images/c-pass-logo.png")?>" class="img-responsive" width="200">
					
				</div>
				<div class="col-md-12">
					<h3 class="text-left" style="color: #000000;font-weight: bold;">Welcome To C-Pass</h3>
					<p class="text-left" style="color: #000000;">Lorem Ipsum is simply dummy text of the printing</p>
				</div>
				<div class="col-md-12">
					<h3 class="text-left" style="color: #000000;font-weight: bold;">But First, Legal Stuff:</h3>
					<p class="text-left" style="color: #000000;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>
					<p class="text-left" style="color: #000000;">If that's OK, tap "Accept all" or customize your preferences in your data privacy settings.</p>
				</div>
				<div class="row text-center">
					<a href="javascript:void(0)" class="btn-green-lg d-block font600 border-r8 accept_condition_btn" style="margin:0 auto">Accept all</a>
				</div>
			</div>
	 	</div>
	</div>
	<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
	<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on("click",".accept_condition_btn",function() {
				$.ajax({
			      	url: "<?=base_url()?>accept_terms_condition",
			      	method: "POST",
			      	data: {
			          	user_id: '<?=$user_id?>'
			      	},
			      	dataType: "json",
			      	success: function(data) {
			           window.location.href = "<?=base_url()?>";
			      	}
		  		});
			});
			
		});
	</script>
</body>
</html>

