<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Us</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	<style>
		.points li{
			list-style-type: disc;
		}
	</style>
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
	<div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center">
	  	<div class="row text-center w-100">
			<div class="col-md-6 offset-md-3">
				<div class="col-md-12 p-4 p-lg-5 align-items-center">
					<img src="<?=assets("images/logo/clinic.png")?>" class="img-responsive">
					<h4 class="mb-5 text-black">G16 Global Covid Health Passport</h4>
					<h4 class="text-black">Registration of Consent</h4>
				</div>
				<div class="col-md-12">
					<p class="text-left">I, hereby give consent to the <strong> G16 Global</strong>, as the data store administor to collecting:</p>
				</div>
				<div class="col-md-12">
					<ul class="text-left points offset-md-1">
						<li>my registration information</li>
						<li>my passenger locators form</li>
						<li>my Passport number</li>
						<li>my NHS number</li>
						<li>my Vaccine details (i.e. dates, batches)</li>
						<li>my Covid test result certificates</li>
						<li>information about my contact with other G16 Global Covid Health Passport users, if another user I have come into contact with tests positive for Covid-19 and uploads their contact data</li>
						<li>information to ensure that G16 Global Covid Health Passport is working properly on my device</li>
					</ul>
					<p class="text-left">Select "I give consent" to confirm you agree with your data being collected.</p>
				</div>
				<div class="row text-center">
					<a href="javascript:void(0)" class="btn-green-lg d-block font600 border-r8 accept_condition_btn" style="margin:0 auto">I give consent</a>
				</div>
			</div>
	 	</div>
	</div>
	<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
	<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on("click",".accept_condition_btn",function() {
				$.ajax({
			      	url: "<?=base_url()?>accept_terms_condition",
			      	method: "POST",
			      	data: {
			          	user_id: '<?=$user_id?>'
			      	},
			      	dataType: "json",
			      	success: function(data) {
			           window.location.href = "<?=base_url()?>";
			      	}
		  		});
			});
			
		});
	</script>
</body>
</html>

