<section class="add-new-member-form py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
					<li class="active"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="#" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Add New Member</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="" method="get" accept-charset="utf-8">
						<div class="row align-items-end">
							<div class="col-md-12 mb-4">
								<div class="mem-prof-img border-50">
									<img src="<?=assets('web/images/member-list-image.jpg')?>" class="border-50" alt="">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Full Name <span class="t-red">*</span></label>
									<input type="text" name="" value="" placeholder="John Doe" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Email Address <span class="t-red">*</span></label>
									<input type="text" name="" value="" placeholder="johndoe@domain.com" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">City</label>
									<input type="text" name="" value="" placeholder="Lexington" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<p class="t-blue font-weight-bold font-14 mb-0">Home address</p>
								<div class="form-group mb-0">
									<label for="" class="t-grey font-weight-bold font-12 mb-0">Do you have an address in the United kingdom </label>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked>
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<input type="text" name="" value="" placeholder="WC1N 3AX" class="form-control form-control-box add_search-icon">
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<select name="" class="form-control form-control-box select-angle-blue">
										<option value="1">27 Old Gloucester Street, London</option>
										<option value="2">27 Old Gloucester</option>
										<option value="3">27 Old Havana</option>
										<option value="4">27 Old pochinki</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Gender<span class="t-red">*</span></label>
									<div class="w-100 d-flex form-control">
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="male" name="example" value="customEx" checked>
											<label class="mb-0 pointer" for="male">Male</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="female" name="example" value="customEx">
											<label class="mb-0 pointer" for="female">Female</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="Other" name="example" value="customEx">
											<label class="mb-0 pointer" for="Other">Other</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of Birth <span class="t-red">*</span></label>
									<div class="relative">
										<input type="text" name="" value="" placeholder="Enter Date of Birth" class="form-control pr-4">
									    <button type="submit" class="date-range-btn"><img src="<?=assets('web/images/date-range-icon.png')?>" alt=""></button>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Ethnicity <span class="t-red">*</span></label>
									<input type="text" name="" value="" placeholder="Enter Ethnicity" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Telephone number <span class="t-red">*</span> </label>
									<input type="text" name="" value="" placeholder="+44 988 9888 656" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Nationality <span class="t-red">*</span></label>
									<input type="text" name="" value="" placeholder="Enter Nationality" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Travel Destination (if travel)</label>
									<input type="text" name="" value="" placeholder="Enter Travel Destination (if travel)" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of Travel (if travel)</label>
									<input type="text" name="" value="" placeholder="Enter Date of Travel (if travel)" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Passport Number</label>
									<input type="text" name="" value="" placeholder="Enter Passport Number" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Do you have symptoms?<span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">No</option>
										<option value="2">Yes</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">What is your covid-19 vaccination<span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">Partially Vaccinated</option>
										<option value="2">Vaccinated</option>
										<option value="3">Pending Both Dose</option>
										<option value="4">Pending Dose 2</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Are You Exempted? <span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">No</option>
										<option value="2">Yes</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Choose Exemption Category<span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">XYZ</option>
										<option value="2">ABC</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Have you ever had Covid-19?<span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">No</option>
										<option value="2">Yes</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">What is your relation with member?<span class="t-red">*</span></label>
									<select name="" class="form-control">
										<option value="1">Father</option>
										<option value="2">Son</option>
									</select>
								</div>
							</div>
						</div>
					<!-- 	<div class="row">
							<div class="col-md-12">
								<div class="fit-to-fly-add-member">
									<div class="row">
										<div class="col-md-6 col-lg-4 mb-4">
											<p class="t-blue font-weight-bold font-14 mb-0">Home address</p>
											<div class="form-group mb-0">
												<label for="" class="t-grey font-weight-bold font-12 mb-0">Do you have an address in the United kingdom </label>
												<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
													<div class="px-0 relative mr-3">
														<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked="">
														<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
													</div>
													<div class="px-0 relative">
														<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
														<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
						<div class="custom-checkbox-green custom-control custom-checkbox d-flex align-items-center">
							<input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
							<label class="custom-control-label t-violet font-14 font-weight-bold pointer pl-2" for="customCheck">Confirm Member Details Are Accurate.</label>
						</div>
						<div class="text-center mt-5">
							<a href="#" title="" data-toggle="modal" data-target="#fit-to-fly-add-member-popup" class="btn-green-lg m-2">Confirm & Proceed</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>