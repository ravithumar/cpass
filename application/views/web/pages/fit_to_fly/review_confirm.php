<section class="py-5 review-confirm-page fit_fly_review">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
					<li class="active"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="#" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Review &amp; Confirm</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative">
					<div class="count-down position-absolute text-white text-center px-4 py-1 font-12 font600">You Have (00 : 10 MIN) Remaining</div>
					<div class="review-detail">
						<div class="row">
							<div class="col-lg-10 col-xl-9">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="font-14 font600 t-light-blue mb-0">Purpose Of Test</label>
						   					<input class="form-control font-13 font500" type="text" placeholder="" value="Fit to Fly (UK Departure and Return to UK)">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="font-14 font600 t-light-blue mb-0">Place Of Test</label>
						   					<input class="form-control font-13 font500" type="text" placeholder="" value="Integrative Health">
										</div>
									</div>
									
								</div>
								<div class="member-list mt-3 mt-md-4">
									
									<div class="row">
										<div class="col-md-6">
											<div id="accordion">
												<div class="card border-0 mb-5">
													<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
														    <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">John Doe</a>								   
												    </div>
												     <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
																			      
											            <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
																<div class="list-block">
																	
																	<div class="top-one d-block d-sm-flex d-md-block d-xl-flex justify-content-between">
																		<div class="info">
																			<p class="font-12 t-black font500 mb-0">johndoe@domain.com</p>
																			<p class="font-12 t-black font500 mb-0">+44 655 6625 655</p>
																			<span class="d-block t-black font-12 font500"><em class="t-blue font500">Passport Id </em> : 565656656</span>
																			<span class="t-black d-block font-12 font500"><em class="t-blue font500">Date Of Birth</em>  : 16 July 1994</span>
																		</div>
																		<a href="fit_to_fly_edit_profile.php" class="t-blue edit text-underline font-12 font500">Edit Information</a>
																	</div>
																	<hr class="my-2">
																	<div class="bottom-one">
																		<div class="info">
																			<div class="edits-one d-block d-xl-flex justify-content-between">
																				<span class="d-block t-black font-12 font500"><em class="t-blue font500">Test Type </em> : Antigen Lateral flow</span>
																				<a href="fit_to_fly_edit_detail.php" class="t-blue edit text-underline font-12 font500">Edit Information</a>
																			</div>
																			<ul class="d-block d-lg-flex">
																				<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Date </em>  : July 25 2021</span></li>
																				<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Time</em>  : 09:30 am</span></li>
																			</ul>
																			
																		</div>
																		
																		</div>
																</div>				      
														</div>

											         </div> 
																			  
											  </div>
																		
											</div>
										
										</div>
										<div class="col-md-6">
											  <div class="card border-0 mb-5">
													<div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
														    <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Darsy Roy</a>								   
												    </div>
												     <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
																			      
											            <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
																<div class="list-block">
																	
																	<div class="top-one d-block d-sm-flex d-md-block d-xl-flex justify-content-between">
																		<div class="info">
																			<p class="font-12 t-black font500 mb-0">johndoe@domain.com</p>
																			<p class="font-12 t-black font500 mb-0">+44 655 6625 655</p>
																			<span class="d-block t-black font-12 font500"><em class="t-blue font500">Passport Id </em> : 565656656</span>
																			<span class="t-black d-block font-12 font500"><em class="t-blue font500">Date Of Birth</em>  : 16 July 1994</span>
																		</div>
																		<a href="fit_to_fly_edit_profile.php" class="t-blue edit text-underline font-12 font500">Edit Information</a>
																	</div>
																	<hr class="my-2">
																	<div class="bottom-one">
																		<div class="info">
																			<div class="edits-one d-block d-xl-flex justify-content-between">
																				<span class="d-block t-black font-12 font500"><em class="t-blue font500">Test Type </em> : Antigen Lateral flow</span>
																				<a href="fit_to_fly_edit_detail.php" class="t-blue edit text-underline font-12 font500">Edit Information</a>
																			</div>
																			<ul class="d-block d-lg-flex">
																				<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Date </em>  : July 25 2021</span></li>
																				<li><span class="t-black d-block font-12 font500"><em class="t-blue font500">Time</em>  : 09:30 am</span></li>
																			</ul>
																			
																		</div>
																		
																		</div>
																</div>				      
														</div>

											         </div> 
																			  
											  </div>
											
										</div>										
									</div>
										
								</div>
								<div class="test-quote mt-3">
									<p class="font-16 font-weight-bold t-blue">Test Price</p>
									<ul>
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black">Member x3 ($50.00)</span><span class="d-block font-14 font-weight-bold t-blue">$150.00</span></li>
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-13 font500 t-black">Tax</span><span class="d-block font-14 font-weight-bold t-blue">$15.00</span></li>
										<li class="d-flex justify-content-between mb-1 pb-1"><span class="d-block font-15 font-weight-bold t-green">Total</span><span class="d-block font-16 font-weight-bold t-green">$165.00</span></li>
									</ul>	
								</div>
								
							</div>
							<div class="col-lg-2 col-xl-3"></div>
						</div>
						<div class="row mt3 mt-md-5 text-center">
							<div class="col">
								<div class="btn-wrap">
									<a href="<?=base_url()?>fit-to-fly/payment" class="btn-green-lg font-16 font600">Confirm & Proceed To Payment</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>