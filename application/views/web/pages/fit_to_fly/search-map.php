<section class="search-page py-3 py-md-5 mt-0 mt-md-4">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 mb-5 mb-md-0">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-5">
					<li class="active"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="#" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">				
					
					<div class="map-wrapper  position-relative">					
					  <ul class="filter d-flex position-absolute m-0 p-0">
					    <li><a href="#" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 mr-2"><img src="<?=assets('web/images/filter.svg')?>" alt="Filter" class="img-fluid" data-toggle="modal" data-target="#filter-options-popup"></a></li>
						<li><a href="<?=base_url()?>fit-to-fly/search" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2"><img src="<?=assets('web/images/list.svg')?>" alt="Filter" class="img-fluid"></a></li>
					  </ul>
					  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2962.0848154592336!2d-88.37567818425829!3d42.062801361813726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880f1a77406562eb%3A0x69e243ee4864c3b6!2sRidgewood%20Ln%2C%20Elgin%2C%20IL%2060124%2C%20USA!5e0!3m2!1sen!2sin!4v1629949627987!5m2!1sen!2sin" width="100%" height="780" style="border:0;" allowfullscreen="" loading="lazy" class="border-r12"></iframe>
					</div>						
			</div>
	    </div>
	</div>	
</section>