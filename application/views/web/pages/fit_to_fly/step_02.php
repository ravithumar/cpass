
<section class="add-new-member-form py-5 fit-fly-steps">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<ul class="d-flex steps justify-content-center">
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-blue">Step 1</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Departing from UK)</p>
                                </div>                               
                              </a>
                            </li>
                            <li class="active position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-grey">Step 2</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Returning to UK)</p>
                                </div>
                              </a>
                            </li>
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid d-none"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-grey">Step 3</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(International Arrival to UK)</p>
                                </div>                            
                              </a>
                            </li>
                    </ul>
					<form action="<?=base_url()?>save-fit-to-fly-return" method="post" accept-charset="utf-8">
						<input type="hidden" name="purpose_id" id="purpose_id" value="<?=$search_data['purpose_id']?>">
						<input type="hidden" name="postcode2" id="postcode2" value="<?=$search_data['postcode']?>">
						<input type="hidden" class="depature_date" value="<?=$search_data['departure_date']?>">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">
						<div class="row mt-3 mt-md-5">
							<div class="col-lg-12">
								<div id="accordion-select01" class="select-trips">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Return Country</a>								   
								   </div>
								   <div id="collapseOne" class="collapse fit_to_fly_collapse" aria-labelledby="headingOne" data-parent="#accordion-select01">
								      <div class=" card-body bg-white pt-5 px-3 pt-5 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
											<select class="form-control select2" style="width: 100%;" name="return_country" id="return_country" required data-parsley-required-message="Please select return country.">
								        	 	<option value="">Select Country</option>
											  	<?php
												  	if($country_list['status'] == true){
													  foreach($country_list['data'] as $key=>$country){ ?>
													  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
															<?php 
														} 
													} 
												?>
											</select>
                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							<div class="col-lg-12">
								
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">During Your Trip, Will You Be Visiting / Stopping Over Or Transiting Through Any Islands or Country?</label>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="is_visiting_country" value="yes" onchange="update_value()">
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="is_visiting_country" value="no" checked>
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-3 mt-md-5">
							<div class="col-lg-12 visiting_country" style="display:none;" id="select_visiting_country">
								<div id="accordion-select02" class="select-trips">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Visiting Country</a>								   
								   </div>
								   <div id="collapseTwo" class="collapse fit_to_fly_collapse" aria-labelledby="headingTwo" data-parent="#accordion-select02">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
											<select class="form-control select2" style="width: 100%;" name="visiting_country" id="visiting_country" data-parsley-required-message="Please select visiting country.">
								        	 	<option value="">Select Country</option>
											  	<?php
												  	if($country_list['status'] == true){
													  foreach($country_list['data'] as $key=>$country){ ?>
													  		<option value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
															<?php 
														} 
													} 
												?>
											</select>
                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							<div class="col-lg-12">
								<div id="accordion-select03" class="">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>								   
								   </div>
								   <div id="collapseThree" class="collapse fit_to_fly_collapse" aria-labelledby="headingThree" data-parent="#accordion-select03">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block fit_to_fly_test_type_list">
                                          
                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							
						</div>
						<div class="row mt-3 mt-md-5">
							<div class="col-md-12 mb-4">
                              <div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
                             	<div class="select-date">								 
								  <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0">Select Return Date</label>
  								   </div>
                             	</div>
                             </div>
                             <div class="calender-view">
                             	<input type="text" id="return_date" value="<?=isset($search_data['return_date1'])?date('d-m-Y',strtotime($search_data['return_date1'])):""?>" name="return_date" placeholder="Date" required data-parsley-required-message="Please select return date." class="p-2 form-control fit_to_fly_datepicker">
                             	<input type="hidden" id="earlier_return_date" value="<?=isset($search_data['return_date1'])?date('d-m-Y',strtotime($search_data['return_date1'])):""?>">
                             </div>    
						    </div>
						    <div class="col-md-12 mb-4">
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Please Provide The Following Information.</label>
									<div class="home-address-radio-btn w-100 d-block d-sm-flex d-md-block d-lg-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="transport_type_1" name="transport_type" value="1" checked="">
											<label class="mb-0 pointer t-blue" for="transport_type_1">Flight No</label>
										</div>
										<div class="px-0 relative mr-3">
											<input type="radio" id="transport_type_2" name="transport_type" value="2">
											<label class="mb-0 pointer t-blue" for="transport_type_2">Vessel No</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="transport_type_3" name="transport_type" value="3">
											<label class="mb-0 pointer t-blue" for="transport_type_3">Train No</label>
										</div>
									</div>
								</div>
							</div>
							
							
							<div class="col-md-12 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0 transport_type_label">Enter Flight No</label>
									<input type="text" name="transport_number" id="transport_number" value="" required data-parsley-required-message="Please Enter Flight No" placeholder="Enter Flight No" class="form-control t-blue">
								</div>
							</div>


							<div class="col-md-12 mb-4">
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">You have booked a clinic for a fit-to-fly departure to the UK on <?=date('Y-m-d',strtotime($search_data['departure_date']))?>. so would you like to pick up this Fit-to-fly(Return to UK) test from the Clinic?</label>
									<div class="home-address-radio-btn w-100 d-block d-sm-flex d-md-block d-lg-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="shipping_method_1" name="shipping_method" value="1" checked="">
											<label class="mb-0 pointer t-blue" for="shipping_method_1">Yes</label>
										</div>
										<div class="px-0 relative mr-3">
											<input type="radio" id="shipping_method_2" name="shipping_method" value="2">
											<label class="mb-0 pointer t-blue" for="shipping_method_2">No</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row shipping_detail" style="display: none;">
							<div class="col-md-12 mb-4">
                              	<div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
									<div class="select-date">								 
										<div class="form-group position-relative">
											<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0">Select Shipping Date</label>
										</div>
									</div>
                             	</div>
                             	<div class="calender-view">
                             		<input type="text" id="shipping_date" data-parsley-required-message="Please select shipping date" name="shipping_date" placeholder="Date" class="p-2 form-control fit_to_fly_shipping_date">
                             	</div>    
						    </div>
							<div class="col-md-12 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Shipping Address</label>
								</div>
							</div>
							<div class="col-md-12">
								<label for="" class="t-violet font-weight-bold font-14 mb-0">Start search with postcode</label>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<!-- <input type="text" name="shippingpostal_code" id="shippingpostal_code" value="" placeholder="WC1N 3AX" class="form-control form-control-box add_search-icon mb-3 mb-md-0 search-shipping-address"> -->
											<input type="text" name="shippingpostal_code" value="" id="postcode1" placeholder="WC1N 3AX" class="search-address search-intl-home-address residency_postcode form-control form-control-box add_search-icon mb-2" style="text-transform:uppercase">
										</div>
										<div class="col-md-6">
										<!-- <select name="shippingaddress" id="shipping-address-dropdown" data-parsley-required-message="Please select shipping address" style="width: 100%;" class="select2 form-control form-control-box select-angle-blue shippingaddress">
											<option value="">Select Shipping Address</option>
										</select> -->
										<select name="residency_address" class="residency_address form-control form-control-box select-angle-blue " id="home-address-dropdown"><option value="">Select Shipping Address</option>
												   </select>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="text-center mt-5">
							<button type="submit" title="" class="btn-green-lg m-2">Next</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	function update_value()
	{
		var val = document.getElementById('home-address-yes').value;

		if (val == 'yes') 
		{
			document.getElementById('select_visiting_country').style.display = "block";
			document.getElementById('select_visiting_country').rules('add', 'required');
		}
		else
		{
			document.getElementById('select_visiting_country').style.display = "none";
			document.getElementById('select_visiting_country').rules('remove', 'required');
		}
	}
</script>