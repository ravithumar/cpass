<section class="add-new-member-form py-5 fit-fly-steps last">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">			
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height residency-radio-section">
					<ul class="d-flex steps justify-content-center">
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-blue">Step 1</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Departing from UK)</p>
                                </div>                               
                              </a>
                            </li>
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-blue">Step 2</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Returning to UK)</p>
                                </div>
                              </a>
                            </li>
                            <li class="active position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="<?=assets('web/images/check-mark-blue.png')?>" alt="" class="img-fluid d-none"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-blue">Step 3</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(International Arrival to UK)</p>
                                </div>                            
                              </a>
                            </li>
                    </ul>
					<form action="" method="get" accept-charset="utf-8">
						<div class="row mt-3 mt-md-5">
							<div class="col-md-12">
								<p>Dear Passenger,</p>
								<p>You have now completed your fit-to-fly return journey to the UK. Only remaining step is to purchase your international arrival test. You do not need to make this booking at this point as we will send you a reminder with a link 72 hours before your return to the UK to complete the purchase of your international arrival test kit. </p>
								<p>Thank you and have a safe trip, <br/>
								C-Pass
								</p>
							</div>
						</div>
						<div class="text-center mt-5">
							<!-- <a href="#" title="" data-toggle="modal" data-target="#fit-to-fly-add-member-popup" class="btn-green-lg m-2">Next</a> -->
							<a href="<?=base_url()?>select-members" title="" class="btn-green-lg m-2">Next</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
