<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 mb-4 order-1 order-sm-1 order-md-1">
                <a class="navbar-brand mb-2 d-inline-block" href="index.php"><img src="images/c-pass-logo.png" alt="" class="img-fluid"></a>
                <p class="t-black font-14">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                <ul class="social-icons-footer d-flex align-items-center mb-0">
                    <li><a href="#" title=""><img src="images/footer-fb.png" alt=""></a></li>
                    <li><a href="#" title=""><img src="images/footer-insta.png" alt=""></a></li>
                    <li><a href="#" title=""><img src="images/footer-in.png" alt=""></a></li>
                    <li class="mb-0"><a href="#" title=""><img src="images/footer-tw.png" alt=""></a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 mb-4 order-2 order-sm-3 order-md-2">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Company</h3>
                <ul class="quick-links font-14 mb-0">
                    <li><a href="#" title="" class="black-link">About Us</a></li>
                    <li><a href="#" title="" class="black-link">Service</a></li>
                    <li class="mb-0"><a href="#" title="" class="black-link">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 mb-4 order-3 order-sm-4 order-md-3">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Product</h3>
                <ul class="quick-links font-14 mb-0">
                    <li><a href="#" title="" class="black-link">Pricing</a></li>
                    <li><a href="#" title="" class="black-link">Security</a></li>
                    <li><a href="#" title="" class="black-link">Apps</a></li>
                    <li class="mb-0"><a href="#" title="" class="black-link">Chating</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-3 mb-4 order-4 order-sm-2 order-md-4">
                <h3 class="t-blue mt-lg-5 my-md-4 ">Address</h3>
                <p class="font-14 t-black">3674 Harrison Street, San Francisco, 94143, CA, USA.</p>
            </div>
        </div>
    </div>
</footer>
<div class="bg-blue py-3">
	<p class="text-center text-white mb-0">Copyright © 2021 C-PASS</p>
</div>

<!-- filter-options-modal -->
<div class="modal fade filter-options-popup p-0" id="filter-options-popup" tabindex="-1" role="dialog" aria-labelledby="filter-options-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal650 modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Filter Options</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="progress-block mb-3">
                    <p class="font-15 font600 t-black mb-0">Distance</p>
                    <p class="font-12 font600 t-blue">1.0 KM TO 5.0 KM</p>
                    <div class="progress  border-r6">
                         <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>    
                </div>
                <div class="progress-block mb-3">
                    <p class="font-15 font600 t-black mb-0">Price</p>
                    <p class="font-12 font600 t-blue">$50.00 TO $250.00</p>
                    <div class="progress  border-r6">
                         <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>    
                </div>
            
                <ul class="d-flex mb-3 p-0 radio-block">
                       
                  <li class="mr-3">
                     <input type="radio" id="test1" name="radio-group" checked>
                     <label for="test1" class="font-14 font500">Departure</label>
                  </li>
                  <li>
                     <input type="radio" id="test2" name="radio-group">
                     <label for="test2" class="font-14 font500">Domestic</label>
                  </li>

                </ul>
                           
                                
                <div class="btn-wrap text-center mt-4">
                    <button type="button" class="btn-green-lg border-r8 font-17 font600 mb-2 mb-sm-0 mr-0 mr-sm-3" data-dismiss="modal" aria-label="Close">Ok</button>
                    <button type="button" class="btn-red-lg border-r8 font-17 font600 ml-0 ml-sm-3" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- booking-information-modal -->
<div class="modal fade booking-information-popup p-0" id="booking-information-popup" tabindex="-1" role="dialog" aria-labelledby="booking-information-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Booking Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0">Domestic</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0">Integrative Health</p>
                    </div>
                </div> 
                 <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0">Antigen Lateral flow</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0">09:30 AM</p>
                    </div>
                </div>
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0">July 25 2021</p>
                    </div>
                </div>      
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="add_new_member.php" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div>

<!-- add-new-me-modal -->
<div class="modal fade booking-information-popup p-0" id="booking-information-popup" tabindex="-1" role="dialog" aria-labelledby="booking-information-popupTitle" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
        <div class="modal-content border-r6 shadow-1">
            <div class="modal-header">
              <h6 class="t-blue font-weight-bold mb-0">Booking Information</h6>              
            </div>
            <div class="modal-body p-4">
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Purpose Of Test</p>
                        <p class="font-13 t-black mb-0">Domestic</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Place of test</p>
                        <p class="font-13 t-black mb-0">Integrative Health</p>
                    </div>
                </div> 
                 <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Test Type</p>
                        <p class="font-13 t-black mb-0">Antigen Lateral flow</p>
                    </div>
                    <div class="right-side text-right">
                        <p class="font-14 font600 t-blue mb-0">Time</p>
                        <p class="font-13 t-black mb-0">09:30 AM</p>
                    </div>
                </div>
                <div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
                    <div class="left-side">
                        <p class="font-14 font600 t-blue mb-0">Date</p>
                        <p class="font-13 t-black mb-0">July 25 2021</p>
                    </div>
                </div>      
                                
                <div class="btn-wrap text-center mt-4">
                    <div class="btn-area">
                        <a href="search-03.php" class="btn-green-lg font-17 font600 mb-3">Complete Your Registration</a>
                    </div>
                    <div class="btn-area">
                        <button type="button" class="btn-blue" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                    
                    
                </div>              
            </div>
        </div>
    </div>
</div>



<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/popper.min.js"></script>

<!-- <script src="js/slick.min.js"></script> -->
<script src="js/custom.js"></script>

</body>
</html>