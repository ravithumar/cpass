
	<section class="forgot-password"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	  <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">
			     <div class="back-close">
		 		   	<a href="<?=base_url('login')?>" class="back position-absolute">
                      <img src="<?php echo assets('web/images/back.svg')?>" alt="Back Arrow" class="img-fluid">
			        </a>			   
	 		     </div>
	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?php echo assets('web/images/c-pass-large-logo.png');?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">	 		  	 
			    <div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
					<script type="text/javascript">
						$('.alert').delay(3000).fadeOut(300);
					</script> 
					<?php $this->load->view('message'); ?> 	
					<form action="<?php echo base_url('auth/forgot_password'); ?>" method="post" accept-charset="utf-8" id="forgot_password">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">		
					<input type="hidden" name="role" value="members">
						<h2 class="font-34 t-bluetwo font-weight-bold mb-4">Forgot Password</h2>			    	
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="identity" class="mb-0 font500 font-13">Email Address</label>
							<input type="email" class="form-control font-14" id="identity" name="identity" required="" data-parsley-required-message="Please enter email address" data-parsley-type="email" data-parsley-type-message="Please enter a valid email address." aria-describedby="fullname" placeholder="Enter Email Address">
							<!-- <a href="#" class="icon position-absolute">
								<img src="<?php echo assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid">
							</a> -->
						</div>
						<div class="btn-wrap mt-4 mt-md-5 mb-3">
							<button type="submit" class="btn-green-lg border-r8 d-block font600" onclick="$('#forgot_password').submit();">Save</button>
						</div>
					</form>
			    </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
