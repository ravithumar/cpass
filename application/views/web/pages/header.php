<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>C - Pass</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/style-2.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/responsive1.css">
	<link rel="stylesheet" href="css/flatpickr.min.css">
	<link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
	
	<!-- <link rel="stylesheet" href="css/slick.css"> -->
</head>
<body class="relative">
	<div class="header-topbar bg-blue py-2">
		<div class="container d-flex align-items-center">
			<div class="social-icons-box mr-4">
				<ul class="social-icons d-flex align-items-center mb-0">
					<li><a href="#" title=""><img src="images/fb.png" alt=""></a></li>
					<li><a href="#" title=""><img src="images/tw.png" alt=""></a></li>
					<li><a href="#" title=""><img src="images/yt.png" alt=""></a></li>
					<li><a href="#" title=""><img src="images/t.png" alt=""></a></li>
				</ul>
			</div>
			<div class="ml-auto d-flex align-items-center text-white">
				<p class="mb-0 font-13 d-none d-lg-block location">9873 Ridgewood Street Elgin, IL 60120</p>
				<p class="mb-0 mx-4 d-none d-lg-block">|</p>
				<a href="tel:7955582795" title="" class="font-13 call white-link">+1-795-5582-795</a>
				<p class="mb-0 font-17 ml-3 ml-sm-5"><a href="sign-in.php" title="" class="white-link">Login </a>/<a href="sign-up.php" title="" class="white-link">Sign Up</a></p>
			</div>
		</div>
	</div>
	<div class="menu">
		<nav class="navbar navbar-expand-lg navbar-light container">
			<a class="navbar-brand" href="index.php"><img src="images/c-pass-logo.png" alt="" class="img-fluid"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto align-items-center">
					<li class="nav-item active">
						<a class="nav-link" href="index.php">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Services</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">News</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="clinic-registration.php">Clinic Registration</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Contact</a>
					</li>
				</ul>
			</div>
			<div class="search-section d-flex align-items-center">
				<div class="search-box ml-4 d-none d-sm-block">
					<a href="#" title="" class="d-inline-block"><img src="images/search-icon.jpg" alt="" class="border-50 shadow-1"></a>
				</div>
				<div class="header-booknow ml-4">
				    <a href="new_booking.php" class="btn-green">BOOK NOW</a>
			    </div>
			</div>
		</nav>
	</div>