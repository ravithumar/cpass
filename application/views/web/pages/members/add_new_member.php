<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/web/css/mobiscroll.javascript.min.css'); ?>">
<script src="<?php echo base_url('assets/web/js/mobiscroll.javascript.min.js'); ?>"></script>
<section class="add-new-member-form py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<style type="text/css">
				.input-group .input-group-text{
					background: transparent;
				    border-right: 0;
				    border-top: 0;
				    border-left: 0;
				    border-bottom-right-radius: 0;
				    cursor: pointer;
				}
				.select2-container--default .select2-selection--single{
					background-color:transparent !important;
					border-right: 0 !important;
				    border-top: 0 !important;
				    border-left: 0 !important;
				    border-bottom-right-radius: 0;
				    border-bottom-left-radius: 0;
				}
				.btn-red-lg:hover{
					color: #E81935;
				}
				.avatar-edit.mem-prof-img{
					height: unset !important;
				}
				
			</style>
			<?php
			//_pre($_SESSION);
			// _pre($member);
			
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold"><?php echo (isset($title) && $title != '')?$title:'Add New Member'; ?></h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<?php	
					if(isset($member->id) && $member->id != ''){	
						$submit_url = base_url('booking/member/edit/').$member->id;	
					}else{	
						$submit_url = base_url('members/add');	
					}	
					?>
					<form action="<?php echo $submit_url; ?>" method="post" enctype="multipart/form-data" class="edit-member-profile-form">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						<input type="hidden" name="is_review" value="<?=isset($is_review)?$is_review:0?>">
						<div class="col-lg-12 text-center">
							<?php if(isset($member->profile_picture) && $member->profile_picture != ''){
								$url = $member->profile_picture;
								}	
								else
								{
								$url = base_url('assets/web/images/dummy-user.png');
								}
							?>
                             <div class="avatar-upload">
                             	 <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url('<?=$url;?>');"></div>
                                </div>
                                <div class="avatar-edit mem-prof-img">
                                    <label for="imageUpload" class="edit-pic border-blue mt-3 font-12 d-inline-block border-r50 t-blue"><input type="file" id="imageUpload" name="profile_picture" accept=".png, .jpg, .jpeg">Edit Picture</label>
                                </div>
                               
                            </div>
                        </div>

						<div class="row align-items-end">
							<div class="col-md-12 mb-4">
								<div class="mem-prof-img border-50 d-none">
									<img src="<?=assets('web/images/member-list-image.jpg')?>" class="border-50" alt="">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Full Name <span class="t-red">*</span></label>
									<?php
									if(isset($member->full_name)){
										$field_value = $member->full_name;
									}else{
										$field_value = NULL;
									}
									if(isset($jumio_member_data)){
										if(!empty($jumio_member_data['fullName'])){
											$field_value = $jumio_member_data['fullName'];
										}
									}
									?>
									<input type="text" name="full_name" value="<?php echo $field_value; ?>" placeholder="Enter Full Name" class="form-control" required data-parsley-required-message="Please enter full name" pattern="[A-Z a-z]+" data-parsley-pattern-message="Please enter valid full name" minlength="2" data-parsley-minlength-message="Please enter valid full name" style="text-transform: capitalize;">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<?php
									if(isset($member->email)){
										$field_value = $member->email;
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Email Address <span class="t-red">*</span></label>
									<input type="email" name="email" value="<?php echo $field_value; ?>" placeholder="Enter Email Address" class="form-control" required data-parsley-required-message="Please enter email address" data-parsley-type="email" data-parsley-type-message="Please enter valid email address" >
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<?php
									if(isset($member->city)){
										$field_value = $member->city;
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">City <span class="t-red">*</span></label>
									<input type="text" name="city" value="<?php echo $field_value; ?>" class="form-control" placeholder="Enter City" required data-parsley-required-message="Please enter city">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<p class="t-blue font-weight-bold font-14 mb-0">Home address</p>
								<div class="form-group mb-0">
									<label for="" class="t-grey font-weight-bold font-12 mb-0">Do you have an address in the United kingdom </label>
										<?php
									if(isset($member->is_homeaddress) && $member->is_homeaddress != ''){
										$field_value = $member->is_homeaddress;
									}else{
										$field_value = 'yes';
										if(isset($member->is_homeaddress)){
											$member->is_homeaddress = 'yes';
										}
									}
									?>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="is_homeaddress" value="home-address-yes" <?php echo ($field_value == 'yes')?'checked':''; ?>>
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="is_homeaddress" value="home-address-no" <?php echo ($field_value == 'no')?'checked':''; ?> >
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-4 mb-4">
								<label for="postcode1" class="t-violet font-weight-bold font-14 mb-0">Postcode <span class="t-red">*</span></label>
								<div class="input-group">
									<?php
									if(isset($member->postcode)){
										$field_value = $member->postcode;
									}else{
										$field_value = NULL;
									}
									?>
									
								  	<input type="text" name="postcode" class="form-control" id="postcode1" placeholder="Enter Postcode" aria-label="RPostcode" aria-describedby="basic-addon2" value="<?php echo $field_value; ?>" required data-parsley-errors-container="#postcode-error" data-parsley-required-message="Please enter post code" style="text-transform:uppercase">
								  	<div class="input-group-append">
								    	<span class="input-group-text search-address search-home-address" id="basic-addon2" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'style="display:none;"':''; ?> ><img src="<?=assets('web/images/add-search.png')?>" alt=""></span>
								  	</div>
								</div>
								<div id="postcode-error">
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<select name="address_dropdown" class="form-control address_dropdown" id="home-address-dropdown" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'style="display:none;"':''; ?>>
										<option value="0" selected disabled>Select Address</option>
									</select>
									<?php
									if((isset($member->is_homeaddress) && $member->is_homeaddress == 'no')){
										$field_value = $member->address;
									}else{
										$field_value = NULL;
									}
									?>
									<input type="text" name="address" value="<?php echo $field_value; ?>" placeholder="Enter Address" class="form-control address-txt" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'':'style="display:none;"';  ?>>
									<!-- echo ($member->is_homeaddress == 'yes' || (!isset($member->is_homeaddress)))?'style="display:none;"':''; -->
								</div>
							</div>
							<input type="hidden" name="home_address_line1" id="home_address_line1">
							<input type="hidden" name="home_address_line2" id="home_address_line2">
							<input type="hidden" name="home_line3" id="home_address_line3">
							<input type="hidden" name="home_city" id="home_city">
							<input type="hidden" name="home_state" id="home_state">

							<?php
							$is_shippingaddress_checked = "";
							if(isset($member->is_shippingaddress) && $member->is_shippingaddress == 'yes'){
								$is_shippingaddress_checked = 'checked';
							}
							if($this->session->userdata("is_shippingaddress_selected")){

								if($this->session->userdata("is_shippingaddress_selected") == "yes"){

									$is_shippingaddress_checked = 'checked';
								}
							}
							if(($this->session->userdata('purpose_id')) && ($this->session->userdata('place_type')) && $this->session->userdata('purpose_id') == 1 && $this->session->userdata('place_type') == 2){
							?>

							<div class="col-sm-6 col-md-6 col-lg-4">
								<div class="form-group form-check">
								    <input type="checkbox" name="is_shippingaddress" class="form-check-input" id="shipping_address_same" value="yes" <?=$is_shippingaddress_checked?> >
								    <label class="form-check-label" for="shipping_address_same"><small>Shipping address is same as above address?</small></label>
								</div>
							</div>

							<div class="col-sm-6 col-md-6 col-lg-4 mb-4 not-same-address" <?php if($is_shippingaddress_checked == "checked"){?> style="display: none;" <?php } ?>>
								<div class="input-group">
								  <input type="text" name="shipping_postcode" class="form-control" id="postcode2" placeholder="Enter Postcode" value="<?php echo  (isset($member->postcode))?$member->postcode:''; ?>" style="text-transform:uppercase">
								  <div class="input-group-append">
								    <span class="input-group-text search-address search-shipping-address" id="basic-addon1" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'style="display:none;"':''; ?> ><img src="<?=assets('web/images/add-search.png')?>" alt=""></span>
								  </div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6 col-lg-4 mb-4 not-same-address" <?php if($is_shippingaddress_checked == "checked"){?> style="display: none;" <?php } ?>>
								<div class="form-group mb-0">
									<select name="shipping_address_dropdown" class="form-control shipping_address_split address_dropdown" id="shipping-address-dropdown" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'style="display:none;"':''; ?> >
										<option>Select Address</option>
										<?php
										if($member->shipping_address != ''){
											echo '<option value="'.$member->shipping_address.'" selected>'.$member->shipping_address.'</option>';
										}
										?>
									</select>
									<input type="text" name="shipping_address" value="<?php echo (isset($member->shipping_address))?$member->shipping_address:''; ?>" placeholder="Enter Address" class="form-control address-txt" <?php echo (isset($member->is_homeaddress) && $member->is_homeaddress == 'no')?'':'style="display:none;"'; ?> >
								</div>
								<input type="hidden" name="address_line1" id="shipping_address_line1">
								<input type="hidden" name="address_line2" id="shipping_address_line2">
								<input type="hidden" name="address_line3" id="shipping_address_line3">
								<input type="hidden" name="shipping_city" id="shipping_city">
								<input type="hidden" name="shipping_state" id="shipping_state">
							</div>

							<?php
							$postcode = $address = '';
							if(isset($member->is_shippingaddress) && $is_shippingaddress_checked == 'checked'){
								$postcode = $member->postcode;
								$address = $member->address;
							}
							?>

							<div class="col-sm-6 col-md-6 col-lg-6 mb-4 same-address" style="<?php if(empty($is_shippingaddress_checked)){ ?> display: none; <?php } ?>">
								<p class="t-blue font-weight-bold font-14 mb-0">Shipping address</p>
								<div class="form-group mb-0">
									<label for="" class="font-weight-bold font-12 mb-0"><span class='same-postcode'><?php echo $postcode; ?></span> <span class="same-address-txt"><?php echo $address; ?></span></label>
								</div>
							</div>

							<?php
							}
							?>
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Gender</label>
									<div class="w-100 d-flex form-control">
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="male" name="gender" value="male" <?php echo (!isset($member->gender) || $member->gender == 'male')?'checked':''; ?> >
											<label class="mb-0 pointer" for="male">Male</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="female" name="gender" value="female" <?php echo (isset($member->gender) && $member->gender == 'female')?'checked':''; ?> >
											<label class="mb-0 pointer" for="female">Female</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="Other" name="gender" value="other" <?php echo (isset($member->gender) && $member->gender == 'other')?'checked':''; ?>>
											<label class="mb-0 pointer" for="Other">Other</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of Birth <span class="t-red">*</span></label>
									<div class="relative">
										<?php
										if(isset($member->date_of_birth)){
											$field_value = $member->date_of_birth;
										}else{
											$field_value = NULL;
										}
										if(isset($jumio_member_data)){
											if(!empty($jumio_member_data['dateOfBirth'])){
												$field_value = date('d-m-Y',strtotime($jumio_member_data['dateOfBirth']));
											}
										}
										?>
										<input type="text" name="date_of_birth" value="<?php echo $field_value; ?>" placeholder="Enter Date of Birth" class="form-control pr-4" id="dob" required data-parsley-required-message="Please enter date of birth" readonly>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Ethnicity <span class="t-red">*</span></label>
									<?php
									if(isset($member->ethnicity)){
										$field_value = $member->ethnicity;
									}else{
										$field_value = NULL;
									}
									?>
									<input type="text" name="ethnicity"  value="<?php echo $field_value; ?>" placeholder="Enter Ethnicity" class="form-control" required data-parsley-required-message="Please enter ethnicity" list="ethnicity_list">
									<datalist id="ethnicity_list">
										<?php
										if(isset($ethnicity) && count($ethnicity) > 0){
											foreach ($ethnicity as $key => $value) {
												?>
												<option value="<?php echo $value->name; ?>" />
												<?php
											}
										}
										?>
									</datalist>


									
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<?php
									if(isset($member->phone)){
										$field_value = $member->phone;
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Telephone Number <span class="t-red">*</span> </label>
									<input type="text" data-parsley-type="number" data-parsley-type="digits" name="phone" value="<?php echo $field_value; ?>" placeholder="Enter Telephone Number" class="form-control" required data-parsley-required-message="Please enter telephone number" pattern="[1-9]{1}[0-9]{9}" data-parsley-pattern-message="Please enter valid telephone number">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Nationality <span class="t-red">*</span></label>
									<?php
									if(isset($member->nationality)){
										$field_value = $member->nationality;
									}else{
										$field_value = NULL;
									}
									?>
									<input type="text" name="nationality" id="nationality"  value="<?php echo $field_value; ?>" placeholder="Enter Nationality" class="form-control" required data-parsley-required-message="Please enter nationality" list="nationality_list">
									<datalist id="nationality_list">
										<?php
										if(isset($nationality_list) && count($nationality_list) > 0){
											foreach ($nationality_list as $key => $value) {
												?>
												<option value="<?php echo $value->name; ?>" />
												<?php
											}
										}
										?>
									</datalist>
								</div>
							</div>

							<?php
							if(isset($purpose_id) && ($purpose_id == '4' || $purpose_id == '5')){
							?>

							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									
									<?php
									if(isset($member->travel_destination)){
										$field_value = $member->travel_destination;
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Travelling (if travel)</label>
									<input type="text" name="travel_destination" value="<?php echo $field_value; ?>" placeholder="Enter Travel Destination" class="form-control" list="countries_list">
									<datalist id="countries_list">
										<?php
										if(isset($countries_list) && count($countries_list) > 0){
											foreach ($countries_list as $key => $value) {
												?>
												<option value="<?php echo $value->name; ?>" />
												<?php
											}
										}
										?>
									</datalist>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<?php
									if(isset($member->date_of_travel) && $member->date_of_travel != ''){
										$date_of_travel = $member->date_of_travel;
										$date_of_travel = DateTime::createFromFormat('Y-m-d', $date_of_travel);
										$field_value = $date_of_travel->format('Y-m-d');
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of Travel (if travel)</label>
									<input type="text" name="date_of_travel" id="date_of_travel" value="<?php echo $field_value; ?>" placeholder="Select Date of Travel" class="form-control pr-4 datepicker" readonly>
									
								</div>
							</div>

							<?php
							}
							?>
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<?php
									if(isset($member->passport)){
										$field_value = $member->passport;
									}else{
										$field_value = NULL;
									}
									if(isset($jumio_member_data)){
										if(!empty($jumio_member_data['documentNumber'])){
											$field_value = $jumio_member_data['documentNumber'];
										}
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Passport Number</label>
									<input type="text" name="passport" value="<?php echo $field_value; ?>" placeholder="Enter Passport Number" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Do you have symptoms? <span class="t-red">*</span></label>
									<select name="symptoms" class="form-control">
										<option value="no" <?php echo (isset($member->symptoms) && $member->symptoms == 'no')?'selected':'';  ?> >No</option>
										<option value="yes" <?php echo (isset($member->symptoms) && $member->symptoms == 'yes')?'selected':'';  ?> >Yes</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">What is your covid-19 vaccination status?</label>
									<select name="vaccine_status" class="form-control member_vaccine_status">
										
										<option value="Fully Vaccinated" <?php echo (isset($member->vaccine_status) && $member->vaccine_status == 'Fully Vaccinated')?'selected':'';  ?> >Fully Vaccinated</option>
										<option value="Partly Vaccinated" <?php echo (isset($member->vaccine_status) && $member->vaccine_status == 'Partly Vaccinated')?'selected':'';  ?> >Partly Vaccinated</option>
										<option value="Not Vaccinated" <?php echo (isset($member->vaccine_status) && $member->vaccine_status == 'Not Vaccinated')?'selected':'';  ?> >Not Vaccinated</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4 vaccine_type_list_div" style="display: <?=(isset($member->vaccine_status) && $member->vaccine_status == 'Not Vaccinated')?'none':''?>;">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Vaccine Type<span class="t-red">*</span></label>
									<select name="vaccine_id" class="form-control member_vaccine_id" required data-parsley-required-message="Please select vaccine type">
										<option value="">Select Vaccine Type</option>
										<?php
										if($certified_vaccines_list['status']){
											foreach($certified_vaccines_list['data'] as $certified_vaccines_list_row){ ?>
												<option value="<?=$certified_vaccines_list_row['id']?>" <?php echo (isset($member->vaccine_id) && $member->vaccine_id == $certified_vaccines_list_row['id'])?'selected':'';  ?>><?=$certified_vaccines_list_row['name']?></option><?php
											}
										} ?>
									</select>
								</div>
							</div>

							<?php
							if(isset($purpose_id) && ($purpose_id == '4' || $purpose_id == '5')){
							?>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Are you exempted? <span class="t-red">*</span></label>
									<select name="is_excempted" class="form-control">
										<option value="no" <?php echo (isset($member->is_excempted) &&  ($member->is_excempted == 'no' || $member->is_excempted == '') )?'selected':'';  ?> >No</option>
										<option value="yes" <?php echo (isset($member->is_excempted) && $member->is_excempted == 'yes')?'selected':'';  ?> >Yes</option>
									</select>
								</div>
							</div>

							<div class="col-md-6 col-lg-4 mb-4" <?php echo (isset($member->is_excempted) && $member->is_excempted == 'yes')?'':'style="display:none;"';  ?> id="excempted_cat">
								<div class="form-group mb-0">
									
									<?php
									if(isset($member->excempted_cat)){
										$field_value = $member->excempted_cat;
									}else{
										$field_value = NULL;
									}
									?>
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Choose exemption category <span class="t-red">*</span></label>
									<input type="text" name="excempted_cat" value="<?php echo $field_value; ?>" placeholder="Enter exemption category" class="form-control" list="excempted_categories" data-parsley-required-message="Please select excempted category">
									<datalist id="excempted_categories">
										<?php
										if(isset($excempted_categories) && count($excempted_categories) > 0){
											foreach ($excempted_categories as $key => $value) {
												?>
												<option value="<?php echo $value->name; ?>" />
												<?php
											}
										}
										?>
									</datalist>
								</div>
							</div>
							<?php
							}
							?>
							
					
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Have you caught covid-19? <span class="t-red">*</span> </label>
									<select name="has_covid" class="form-control">
										<option value="no"  <?php echo (isset($member->has_covid) && $member->has_covid == 'no')?'selected':'';  ?>>No</option>
										<option value="yes"  <?php echo (isset($member->has_covid) && $member->has_covid == 'yes')?'selected':'';  ?>>Yes</option>
									</select>
								</div>
							</div>
						</div>
						<div class="custom-checkbox-green custom-control custom-checkbox d-flex align-items-center">
							<input type="checkbox" class="custom-control-input" id="customCheck" name="confirm" required data-parsley-required-message="Please confirm your details">
							<label class="custom-control-label t-violet font-14 font-weight-bold pointer pl-2" for="customCheck">Confirm Member Details Are Accurate.</label>
						</div>
						<div class="text-center mt-5">
							
							<a href="<?php echo base_url('select-members'); ?>" class="btn-red-lg m-2 d-none">Cancel</a>
							<input class="btn-green-lg text-success m-2" type="submit" name="submit" value="Save">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	var is_homeaddress = '<?php echo isset($member->is_homeaddress)?$member->is_homeaddress:""; ?>';
	var address = '<?php echo isset($member->address)?$member->address:""; ?>';
	var is_shippingaddress = '<?php echo isset($member->is_shippingaddress)?$member->is_shippingaddress:""; ?>';

	$( document ).ready(function() {
	
		if(is_shippingaddress == 'yes'){

		}

		if(is_homeaddress != '' ){
			if(is_homeaddress == 'yes'){
				var home_address = $("#home-address-dropdown");
				home_address.append(new Option(address, address));
				$('#home-address-dropdown option[value="'+address+'"]').prop('selected', true);
			}else{
				$('.address-txt').val(address);
				$('.address-txt').show();
				$("#home-address-dropdown").hide();
			}
		}

		$(document).on("change",".member_vaccine_status",function(){
			if($(this).val() == "Not Vaccinated"){
				$(".vaccine_type_list_div").hide();
				$('.member_vaccine_id').prop('required',false);
			}else{
				$(".vaccine_type_list_div").show();
				$('.member_vaccine_id').prop('required',true);
			}
		});

		$('#is_excempted').on('change', function() {
	      if ( this.value == 'yes')
	      //.....................^.......
	      {
	        $("#excempted_cat").show();
	        $("#excempted_cat_input").attr("required", "true");
	      }
	      else
	      {
	        $("#excempted_cat").hide();
	      }
	    });

	});
	
</script>
