<section class="add-new-member-form py-5">
	<div class="container-large">
		<div class="row">
			<?php $this->load->view('/web/pages/sidebar'); ?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-center align-items-center">
					<h5>Your session has been expire</h5>
				</div>
			</div>
		</div>
	</div>
</section>