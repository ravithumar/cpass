<section class="member-list py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Member List</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<div class="row">
						<?php if(!empty($member_list['data'])){
							foreach ($member_list['data'] as $key => $list) { ?>
								<div class="col-md-12 col-lg-6 mb-4">
									<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue">
										<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
											<?php
											$img_url = ($list['profile_picture'] == '')?assets('web/images/member-list-image.jpg'):$list['profile_picture'];
											?>
											<img src="<?php echo $img_url; ?>" alt="" class="border-50">
										</div>
										<div class="member-list-detail w-100">
											<h6 class="t-blue font-14 font-weight-bold mb-0"><?=$list['full_name'] ?? '';?></h6>
											<?php
											if(!empty($list['address'])){ ?>
												<p class="mem-location t-black font-13 mb-0"><?=$list['address'] ?? '';?>, <?=$list['city'] ?? '';?>, <?=$list['postcode'] ?? '';?></p>
											<?php } ?>
											<div class="d-flex align-items-end justify-content-between">
												<div class="mr-2">
													<p class="font-10 font-weight-bold mb-0 t-black"><?=$list['email'] ?? '';?></p>
													<p class="font-10 font-weight-bold mb-0 t-black"><?=$list['phone'] ?? '';?></p>
												</div>
												<div>
													<a href="<?=base_url('members/edit-member/')?><?=$list['id']?>"><img src="<?=assets('web/images/edit-icon.jpg')?>" alt=""></a>
													<a href="<?=base_url('members/delete/')?><?=$list['id']?>" class="delete-member"><img src="<?=assets('web/images/delete-icon.jpg')?>" alt=""></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php }
						} ?>
						
					</div>
					<!-- <div class="text-center mt-5">
						<a href="<?=base_url('members/scan-documents')?>" title="" class="btn-green-lg m-2">Add Member</a>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section>
