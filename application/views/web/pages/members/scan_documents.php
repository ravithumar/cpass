<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<section class="member-list py-5 review-confirm-page">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Scan Documents</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<div class="row">
						<div class="col-md-12 col-lg-12 mb-4">
							 	<div class="count-down position-absolute text-white text-center px-4 py-1  font-12 font600">You Have (<span class="countdown">59:59</span>) Remaining</div>
							 	<a href="<?=base_url('members/add')?>" class="btn-blue m-2">Skip & Add Member</a> 
							<div class="please_wait text-center" style="display: none;"><h3>Please wait.....</h3></div>
							<div class="embed-responsive embed-responsive-16by9 jumio_iframe mt-5">
								<input type="hidden" id="access_token" name="access_token" value="<?=$access_token?>">
								<input type="hidden" name="sdk_token" value="<?=$sdk_token?>">
								<input type="hidden" id="account_id" name="account_id" value="<?=$account_id?>">
								<input type="hidden" id="workflow_id" name="workflow_id" value="<?=$workflow_id?>">
								
								<iframe class="embed-responsive-item" src="<?=$web_href?>" allow="camera;fullscreen;accelerometer;gyroscope;magnetometer" allowfullscreen></iframe>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade fit-to-fly-popup p-0" id="confirm_attempt_modal" tabindex="-1" role="dialog" aria-labelledby="fit-to-fly-popupTitle" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered" role="document">
		<div class="modal-content border-r6 shadow-1">
			<div class="modal-body p-4">
				<div class="info-area d-block d-sm-flex align-items-center justify-content-between mb-3">
					<div class="row">
						<div class="col-md-12">
							<p class="font-14 font600 t-blue mb-0 text-left">Your previously attempt is faild are you sure want to continue?</p>
						</div>
					</div>
				</div>				
				<div class="btn-wrap text-center mt-4">
					<div class="btn-area">
						<a href="<?=base_url()?>scan-documents" class="btn-green-lg font-17 font600 mb-3">Yes</a>
					</div>
					<div class="btn-area">
						<a href="<?=base_url()?>members" class="btn-green-lg font-17 font600 mb-3">No</a>
					</div>
				</div>       
			</div>
		</div>
	</div>
</div>
<script>
	var timer2 = "60:00";
	var interval = setInterval(function() {
		var timer = timer2.split(':');
		//by parsing integer, I avoid all extra string processing
		var minutes = parseInt(timer[0], 10);
		var seconds = parseInt(timer[1], 10);
		--seconds;
		minutes = (seconds < 0) ? --minutes : minutes;
		if (minutes < 0) clearInterval(interval);
		seconds = (seconds < 0) ? 59 : seconds;
		seconds = (seconds < 10) ? '0' + seconds : seconds;
		$('.countdown').html(minutes + ':' + seconds);
		timer2 = minutes + ':' + seconds;
		if(minutes < 0){
			window.location.href = "<?=base_url("expire-token")?>"
		}
	}, 1000);
	get_jumio_status();
	setInterval(function(){
		get_jumio_status();
	},10000);
	function get_jumio_status(){
		var access_token = $("#access_token").val();
		var account_id = $("#account_id").val();
		var workflow_id = $("#workflow_id").val();
		$.ajax({
			url: BASE_URL+'get-jumio-status',
			type: "POST",
			data:{
				access_token:access_token,
				account_id: account_id,
				workflow_id:workflow_id,
			},
			dataType:"json",
			success: function (returnData) {
				if(returnData.status == true){
					if(returnData.is_loader == true){
						$(".please_wait").show();
						$(".jumio_iframe").hide();
					}
					if(returnData.is_open_modal == true){
						$("#confirm_attempt_modal").modal("show");
					}else{
						if(returnData.is_redirect_page == true){
							window.location.href = "<?=base_url("members/add")?>"
						}
					}
				}else{
					window.location.href = "<?=base_url("expire-token")?>";
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				Notiflix.Notify.Failure('Something went wrong');
			}
		}); 
	}
</script>
