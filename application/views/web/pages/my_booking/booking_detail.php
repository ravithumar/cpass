<section class="booking-detail py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<?php //print_r($booking_detail['member_details']);?>
				<h5 class="t-blue font-24 font-weight-bold">Detail</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height">
					<div class="row">
						<div class="col-lg-6 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Order Reference</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13">#<?=$booking_detail['booking_refernce'];?></p>
							</div>
						</div>
						<div class="col-lg-6 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Purpose Of Test</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13"><?=ucwords(str_replace('-',' ',$booking_detail['type']));?></p>
							</div>
						</div>
<!-- 						<div class="col-lg-4 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Place of test</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13"><?=$booking_detail['member_details'][0]['place_of_test'];?></p>
							</div>
						</div> -->
						<!-- <div class="col-md-12 mb-4">
							<div class="border-bottom pb-2">
								<p class="mb-0 t-darkblue font-weight-bold font-14">Shipping Address</p>
								<p class="mb-0 t-darkblue font-weight-bold font-13"></p>
							</div>
						</div> -->
					</div>
					<div class="pt-2">
						<h6 class="font-16 font-weight-bold t-blue mb-3">Member booking details</h6>
						<div class="row">
							<?php 
							$tracking_number = "";
							if(!empty($booking_detail['member_details'])){
							foreach ($booking_detail['member_details'] as $key => $detail) { 
								$tracking_number = $detail['tracking_no']; ?>
								<div class="col-md-8 col-xl-6 mb-4">
								<div class="booking-processing bg-white h-100 p-3 border-r10 border-blue">
								    <div class="d-flex justify-content-between align-items-center flex-wrap-reverse flex-wrap">
								    	<h6 class="font-16 mr-2 font-weight-bold my-1 t-blue d-inline-block"><?=$detail['full_name'] ?? '';?></h6>

								    	<?php
								    	if($detail['test_result'] != 'Result-awaiting'){
								    	?>

								    	<div class="ml-auto">
								    		<a href="<?= $detail['pdf']; ?>" target="_blank"><img src="<?=assets('web/images/download-icon.png')?>" alt="Download Certificate"></a>
								    		<label class="negative ml-2 font-12 text-white font-weight-bold mb-0" style="background-color: <?= $detail['test_color']; ?>" ><?=$detail['test_result'] ?? '';?></label>
								    	</div>

								    	<?php
								    	}
								    	?>
								    	
								    </div>
								    <p class="mb-0 t-black font-weight-bold font-12"><?=$detail['email'] ?? '';?></p>
								    <p class="mb-0 t-black font-weight-bold font-12"><?=$detail['phone'] ?? '';?></p>
								    <?php if(!empty($detail['passport'])){ ?>
								    	<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Passport Number :</span> <?=$detail['passport']?></p>
									<?php } ?>
								    <p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Date Of Birth :</span> <?=$detail['date_of_birth'] ?? '';?></p>
								    <div class="border-top pt-2 mt-2">
								    	<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Test Type :</span> <?=$detail['test'] ?? '';?></p>
								    	<div class="d-flex flex-wrap mb-2">
								    		<p class="mb-0 mr-4 t-black font-weight-bold font-12"><span class="t-blue">Date :</span><?=$detail['booking_date'] ?? '';?> </p>
								    		<?php
								    		if(isset($detail['booking_time']) && $detail['booking_time'] != ''){
								    		?>
								    		<p class="mb-0 t-black font-weight-bold font-12"><span class="t-blue">Time :</span> <?=$detail['booking_time'] ?? '';?></p>
								    		<?php
								    		}
								    		?>
								    		
								    	</div>

								    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-0">What is your Covid-19 vaccination status?</h6>
								    	<p class="mb-0 t-black font-weight-bold font-12"><?=$detail['vaccine_status']?></p>

								    	<?php if ($detail['place_of_test'] != "") { ?>
									    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-0">Clinic Name</h6>
									    	<p class="mb-0 t-black font-weight-bold font-12"><?=$detail['place_of_test'] ?? '';?></p>
								    	<?php } ?>

								    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-0">Home Address</h6>
								    	<p class="mb-0 t-black font-weight-bold font-12"><?=$detail['home_address'] ?? '';?></p>

								    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-0">Test Result</h6>
								    	<p class="mb-0 t-black font-weight-bold font-12"><?=$detail['test_result'] ?? '';?></p>

								    	<?php
								    	if($booking_detail['type'] == 'international-arrival'){
								    		if($detail['plf_document'] != ''){
								    			?>
								    			<h6 class="font-14 mt-2 font-weight-bold t-blue mb-2"><a href="<?= $detail['plf_document']; ?>" target="_blank">Click here to PLF view form</a></h6>
								    			
								    			<?php
								    		}else{
								    			?>
								    			<form method="post" action="<?php echo base_url('my-booking/upload-plf-form/').$booking_detail['booking_id'].'/'.$detail['member_id']; ?>" enctype="multipart/form-data">
										    		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
											    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-2">PLF Form Upload</h6>
											    	<div class="input-group">
													  <div class="custom-file">
													    <input type="file" name="document" class="custom-file-input" id="plf-form" name="plf_form" required data-parsley-required-message="PLF form is required" data-parsley-errors-container="#doc-error">
													    <label class="custom-file-label" for="plf-form">Choose file</label>
													  </div>
													  <div class="input-group-append">
													    <input type="submit" class="btn btn-outline-primary" value="UPLOAD">
													  </div>
													</div>
													<div id="doc-error">
													</div>
												</form>

								    			<?php
								    		}
								    	}
								    	?>
								    	
								    </div>
							    </div>
							</div>
							<?php } 
							}
							else{
								echo "No Member found.";
							}
							?>
							
						   
						</div>
					</div>
					<?php
					if(!empty($tracking_number)){ ?>
						<div class="text-center mt-5">
							<a href="<?=base_url()?>tracking/<?=$tracking_number?>" title="" class="btn btn-blue">Track Report</a>
						</div><?php 
					} ?>
				</div>
			</div>
		</div>
	</div>
</section>