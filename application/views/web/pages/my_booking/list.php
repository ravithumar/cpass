<section class="my-booking py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="my-booking-inner bg-grey shadow border-r10 p-4 pt-5 account-div-height">
					<ul class="nav nav-tabs border-r50 justify-content-around justify-content-center mx-auto" role="tablist">
						<li class="nav-item">
							<a class="nav-link border-0 active" data-toggle="tab" href="#tabs-1" role="tab">Schedule</a>
						</li>
						<li class="nav-item">
							<a class="nav-link border-0" data-toggle="tab" href="#tabs-2" role="tab">Historic</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content mt-5">
						<div class="tab-pane fade show active" id="tabs-1" role="tabpanel">
							<ul class="my-booking-lists row">

								<?php 
								//$booking_list = '';border-r10
								
								if(!empty($booking_list['data'])){
									foreach ($booking_list['data'] as $key => $list) { ?>
										<li class=" col-lg-6 mb-4">
											<a href="<?=base_url('my-booking/detail/')?><?=$list['booking_id']?>" title="">
												<div class="my-booking-list-box d-flex bg-white shadow-blue p-2">
													<div class="my-booking-lists-div1">
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo $list['test'];?></p>
														</div>
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo $list['booking_date'];?></p>
														</div>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Address</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['home_address']?></p>
														</div>
													</div>
													<div class="my-booking-lists-div2">
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo $list['name'];?></p>
														</div>
														<?php
														if(!empty($list['booking_time'])){ ?>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo $list['booking_time'];?></p>
														</div>
														<?php } ?>
													</div>
													<div class="my-booking-lists-div3 text-right">
														<?php
														if(!empty($list['place_of_test'])){ ?>
															<div class="mb-1">
																<label class="t-blue font-12 font-weight-bold mb-0">Clinic Name</label>
																<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['place_of_test']?></p>
															</div>
														<?php } ?>
														<div class="mb-1">
															<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0" style="background-color: <?= $list['color']; ?>;"><?=$list['booking_status']?></label>
														</div>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Order Reference</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['booking_refernce']?></p>
														</div>
														<!-- <div class="">
															<label class="t-blue font-12 font-weight-bold mb-0 mr-3">Upload your kit</label><br/>
															<input type="file" name="imageUrl" id="imageUrl" class="t-black font-12 font-weight-bold mb-0 mr-2" style="width: 100%;">
														</div> -->
													</div>
												</div>
												<?php if(!empty($list['report_type'] == '5')){ ?>
												<div class="bg-white shadow-blue p-2">
												<form method="post" action="<?php echo base_url('my-booking/upload-kit/').$list['booking_id']; ?>" enctype="multipart/form-data">
										    		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
											    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-2">Upload your kit</h6>
											    	<div class="input-group">
													  <div class="custom-file">
													    <input type="file" name="upload_kit" class="custom-file-input" id="upload_kit" required data-parsley-required-message="Kit is required"  accept="image/*">
													    <input type="hidden" name="booking_member_id" value="<?=$list['booking_member_id']?>">
													    <label class="custom-file-label" for="plf-form" style="padding-left: 12px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;width: 100%;display: inline-block;font-weight: normal;">Choose file</label>
													  </div>
													  <div class="input-group-append">
													    <input type="submit" class="btn btn-outline-primary" id="submit_button" value="UPLOAD" onclick="upload_image()">
													  </div>
													</div>
													<div id="doc-error">
													</div>
												</form>
														<p class="text-success" id="processing_message" style="display: none;">Your result is being processed</p>
												</div>

												<?php } ?>
											</a>
										</li>
									<?php }
								}
								else{
									echo "No Booking Found.";
								}
								?>
							</ul>
						</div>
						<div class="tab-pane fade" id="tabs-2" role="tabpanel">
							<ul class="my-booking-lists row">
								<?php 
								
								if(!empty($booking_list_history['data'])){
									foreach ($booking_list_history['data']as $key => $list) { ?>
										<li class="active col-lg-6 mb-4">
											<a href="<?=base_url('my-booking/detail/')?><?=$list['booking_id']?>" title="">
												<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
													<div class="my-booking-lists-div1">
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['test']?></p>
														</div>
														
														<?php
														if(!empty($list['booking_date'])){ ?>
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['booking_date']?></p>
														</div>
														<?php } ?>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Address</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['home_address']?></p>
														</div>
													</div>
													<div class="my-booking-lists-div2">
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['name']?></p>
														</div>
														<?php
														if(!empty($list['booking_time'])){ ?>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo $list['booking_time'];?></p>
														</div>
														<?php } ?>
													</div>
													<div class="my-booking-lists-div3 text-right">
														<div class="mb-1">
															<label class="t-blue font-12 font-weight-bold mb-0">Clinic Name</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?php echo ($list['place_of_test'] == '')?'-':$list['place_of_test']; ?></p>
														</div>
														<div class="mb-1">
															<!-- <a href="<?= $list['pdf']; ?>" target="_blank"><img src="<?=assets('web/images/download-icon.png')?>" alt="Download Certificate"></a> -->
															<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0" style="background-color: <?= $list['test_color']; ?>;"><?=$list['test_result']?></label>

															<!-- <label class="upcoming font-14 my-2 font-weight-bold mb-0"><?=$list['booking_status']?></label> -->
														</div>
														<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Order Reference</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['booking_refernce']?></p>
														</div>
														<!-- <div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Test Result</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['test_result']?></p>
														</div> -->
														<!-- <?php if(isset($list['result_file_path'])) { ?>
															<div class="">
															<a href="<?=$list['result_file_path']?>" target="_blank" class="t-blue font-12 font-weight-bold mb-0">View Result Image</a>
															</div>
														<?php } if(isset($list['ref_number'])) {  ?>
															<div class="">
															<label class="t-blue font-12 font-weight-bold mb-0">Reference Number</label>
															<p class="mb-0 font-12 font-weight-bold t-black"><?=$list['ref_number']?></p>
														</div>
														<?php }  ?> -->
													</div>
												</div>
												<!-- <div class="bg-white shadow-blue p-2">
												<form method="post" action="<?php echo base_url('my-booking/upload-kit/').$list['booking_id']; ?>" enctype="multipart/form-data">
										    		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
											    	<h6 class="font-14 mt-2 font-weight-bold t-blue mb-2">Upload your kit</h6>
											    	<div class="input-group">
													  <div class="custom-file">
													    <input type="file" name="upload_kit" class="custom-file-input" id="upload_kit" required data-parsley-required-message="Kit is required" data-parsley-errors-container="#doc-error">
													    <input type="hidden" name="booking_member_id" value="<?=$list['booking_member_id']?>">
													    <label class="custom-file-label" for="plf-form">Choose file</label>
													  </div>
													  <div class="input-group-append">
													    <input type="submit" class="btn btn-outline-primary" value="UPLOAD">
													  </div>
													</div>
													<div id="doc-error">
													</div>
												</form>
												</div> -->
											</a>
									<?php }
								}
								else{
									echo "No Booking Found.";
								}
								?>
								
								
							</ul>
						</div>
					</div>
					<!-- tab close -->
				</div>
			</div>
		</div>
	</div>
</section>

 <script type="text/javascript">

	function upload_image()
	{
        var val = document.getElementById("upload_kit").value;

        if (val != '') 
        {	
        	Notiflix.Loading.Init({
			  customSvgUrl:'<?php echo base_url('assets/images/kitupload-Loading.gif'); ?>',
			  svgSize:'200%'
			}); 

			Notiflix.Loading.Custom('Your result is being processed...');
   
        }
        else
        {
        	document.getElementById('processing_message').style.display = "none";
        	document.getElementById("loder_button").style.display = "none";
        	document.getElementById("submit_button").style.display = "block";
        }
    
	}
		

</script> 