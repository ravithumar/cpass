<section class="booking-detail py-5">
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<?php //print_r($booking_detail['member_details']);?>
				<h5 class="t-blue font-24 font-weight-bold">Track Report</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height">
					<?php
						$total_progress = floor($response->statusCount / count($response->tracking_data));
					?>
					<div class="row">
						<div class="col-lg-12 mb-4">
							<div class="border-bottom pb-2">
							<div class="progress">
							    <div class="progress-bar w-75" role="progressbar" aria-valuenow="<?=$total_progress?>" aria-valuemin="0" aria-valuemax="<?=$response->statusCount?>"></div>

							</div>
							</div>
						</div>
					</div>
					<div class="row">
						<?php
						if(!empty($response->tracking_data)){
							foreach($response->tracking_data as $checkpoint){ ?>
								<div class="col-md-12">
									<p>
										<?=$checkpoint->message?> <br/>
										<i class="fa fa-calendar" aria-hidden="true"></i> <?=date('M d Y H:i A',strtotime($checkpoint->checkpoint_time))?>
										<?php if(!empty($checkpoint->location)){ ?>
											<i class="fa fa-map-marker" aria-hidden="true"></i> <?=$checkpoint->location?>
										<?php } ?>
										
									</p><br/>
								</div><?php 
							} 
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>