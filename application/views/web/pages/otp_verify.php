
<section class="log-in"> 
	<div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	 <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">

	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?php echo assets('web/images/c-pass-large-logo.png');?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">
	 		   <div class="skip-area">
		 		   	<!-- <a href="javascript:;" class="skip-one text-right text-white font-16 font600 position-absolute">
	                  Skip <img src="<?php echo assets('web/images/right-arrow.png');?>" alt="Right Arrow" class="img-fluid ml-2">
			        </a>	 -->		   
	 		   </div>	 			
				<form action="<?=base_url('otp-verify')?>" id="login" method="post" enctype="multipart/form-data" accept-charset="utf-8" autocomplete="off">	 	
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">		
					<input type="hidden" name="role" value="2">
					<div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
						<script type="text/javascript">
							$('.alert').delay(3000).fadeOut(300);
						</script> 
						<?php $this->load->view('message'); ?> 
						<h2 class="font-34 t-bluetwo font-weight-bold">Verify Mobile Number</h2>
						<div class="form-group mb-3 mb-sm-4 position-relative">
						<label for="otp" class="mb-0 font500 font-13">Enter OTP</label>
						<input type="text" class="form-control font-14" id="otp" aria-describedby="otp" name="otp" required="" data-parsley-required-message="Please Enter OTP" data-parsley-type="number" placeholder="Enter OTP">
						
						</div>
						
						<div class="forgot-pass d-flex align-items-center justify-content-between my-4 my-md-5">
							<button type="submit" onclick="$('#login').submit();" class="btn-green d-block font600 border-r8">Verify</button>
							<a href="<?=base_url('resend-otp')?>" class="t-black font-12">Resend OTP</a>	
						</div>
						<!-- <p class="text-center t-bluefour font-12 mt-5 mb-0">Don’t Have an Account? <a href="<?=base_url('signup')?>" class="t-green font-14 font600 text-underline">Sign Up</a></p> -->
					</div>
				</form>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
