
	<section class="change-password"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	  <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">
			     <div class="back-close">
		 		   	<a href="<?=base_url('edit-profile')?>" class="back position-absolute">
                      <img src="<?=assets('web/images/back.svg')?>" alt="Back Arrow" class="img-fluid">
			        </a>			   
	 		     </div>
	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?=assets('web/images/c-pass-large-logo.png')?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">	 		  	 			
			    <div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
					<script type="text/javascript">
						$('.alert').delay(3000).fadeOut(300);
					</script> 
					<?php $this->load->view('message'); ?> 
			    	<h2 class="font-34 t-bluetwo font-weight-bold mb-4">Change Password</h2>	
					<form action="<?php echo base_url('auth/change_password/members')?>" method="post" accept-charset="utf-8">			    	
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">		
						
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="old" class="mb-0 font500 font-13">Current Password</label>
							<input type="password" class="form-control font-14" id="old" name="old" required="" data-parsley-minlength="8" data-parsley-minlength-message="Minimum 8 characters are required" aria-describedby="old" placeholder="Enter Current password" data-parsley-required-message="Please Enter Current Password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?=assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid mb-3">
							</a>
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="new" class="mb-0 font500 font-13">New Password</label>
							<input type="password" class="form-control font-14" id="new" name="new" required="" data-parsley-minlength="8" data-parsley-minlength-message="Minimum 8 characters are required" aria-describedby="new" placeholder="Enter New password" data-parsley-required-message="Please Enter New Password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?=assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid mb-3">
							</a>
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="new_confirm" class="mb-0 font500 font-13">Confirm Password</label>
							<input type="password" class="form-control font-14" id="new_confirm" name="new_confirm" data-parsley-minlength="8" data-parsley-minlength-message="Minimum 8 characters are required" data-parsley-equalto-message="This value should be the same as new password." data-parsley-equalto='#new' required="" aria-describedby="new_confirm" placeholder="Enter Confirm password" data-parsley-required-message="Please Enter Confirm Password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?=assets('web/images/eyes.svg')?>" alt="Eyes" class="img-fluid mb-3">
							</a>
						</div>
						<div class="btn-wrap mt-4 mt-md-5 mb-3">
							<button type="submit" class="btn-green-lg d-block border-r8 font600">Save</button>
						</div>
					</form>
			    </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
