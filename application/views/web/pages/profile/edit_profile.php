<section class="edit-member-profile-form py-5">
	<style type="text/css">
		.register_country_code .cc-picker{
			left: -241px;
		}
	</style>
	<div class="container-large">
		<div class="row">
			<?php
			$this->load->view('/web/pages/sidebar');
			?>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<!-- <script type="text/javascript">
					jQuery('.alert').delay(3000).fadeOut(300);
				</script> -->
				<?php //$this->load->view('message'); ?> 
				<h5 class="t-blue font-24 font-weight-bold mb-3">Edit Profile</h5>
				
				<div class="input-field-ui bg-grey border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between mt-5 mt-sm-0">
					<form action="<?php echo base_url('edit-profile'); ?>" method="post" id="edit_profile" accept-charset="utf-8" enctype="multipart/form-data" accept-charset="utf-8" autocomplete="off">
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
						<?php if(isset($member_detail['profile_picture']) && $member_detail['profile_picture'] != '') {
							$url = $member_detail['profile_picture'];
							}	
							else
							{
							$url = '../assets/web/images/dummy-user.png';
							}
						?>
						<div class="col-lg-12 text-center mt-4 mb-2">
                             <div class="avatar-upload">
                             	 <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url('<?=$url;?>');"></div>
                                </div>
                                <div class="avatar-edit mem-prof-img">
                                    <label for="imageUpload" class="edit-pic border-blue mt-3 font-12 d-inline-block border-r50 t-blue"><input type="file" id="imageUpload" name="profile_picture"  accept=".png, .jpg, .jpeg">Edit Picture</label>
                                </div>
                               
                            </div>
                        </div>
						<div class="row align-items-end mt-3 mt-md-5">
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Full name</label>
									<input type="text" name="full_name" value="<?=$member_detail['full_name'] ?? '';?>" placeholder="Full Name" class="form-control" required="" data-parsley-required-message="Please Enter Full name" data-parsley-pattern="/^[a-zA-Z\s]+$/" data-parsley-pattern-message="Please enter only character" data-parsley-maxlength="70">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Email address </label>
									<input type="text" name="email" value="<?=$member_detail['email'] ?? '';?>" placeholder="Enter Email Address" class="form-control" required="" readonly data-parsley-required-message="Please Enter Email Address">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Address </label>
									 <input type="text" name="address" value="<?=$member_detail['address'] ?? '';?>" placeholder="Enter Address" class="form-control" required="" data-parsley-required-message="Please Enter Address">
								
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">City</label>
									<input type="text" name="city" value="<?=$member_detail['city'] ?? '';?>" placeholder="Enter City" class="form-control" required="" data-parsley-required-message="Please Enter City">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Post code</label>
									<input type="text" name="postal_code" value="<?=$member_detail['postcode'] ?? '';?>" placeholder="Enter Post code" class="form-control" required="" data-parsley-required-message="Please Enter Post code">
								</div>
							</div>
							
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Gender</label>
									<div class="w-100 d-flex form-control">
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="male" name="gender" value="male" <?php echo (isset($member_detail['gender']) && $member_detail['gender'] == 'male')?'checked':''; ?> >
											<label class="mb-0 pointer" for="male">Male</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="female" name="gender" value="female" <?php echo (isset($member_detail['gender']) && $member_detail['gender'] == 'female')?'checked':''; ?> >
											<label class="mb-0 pointer" for="female">Female</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="Other" name="gender" value="other" <?php echo (isset($member_detail['gender']) && $member_detail['gender'] == 'other')?'checked':''; ?>>
											<label class="mb-0 pointer" for="Other">Other</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Ethnicity</label>
									<?php
									if(isset($member_detail['ethnicity'])){
										$field_value = $member_detail['ethnicity'];
									}else{
										$field_value = NULL;
									}
									?>
									<input type="text" name="ethnicity"  value="<?php echo $field_value; ?>" placeholder="Enter Ethnicity" class="form-control" required list="ethnicity_list" data-parsley-required-message="Please Enter Ethnicity">
									<datalist id="ethnicity_list">
										<?php
										if(isset($ethnicity) && count($ethnicity) > 0){
											foreach ($ethnicity as $key => $value) {
												?>
												<option value="<?php echo $value['name']; ?>" />
												<?php
											}
										}
										?>
									</datalist>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0 register_country_code">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Phone number (OTP Verification)</label>
									<input type="text" name="phone" value="<?=$member_detail['phone'] ?? '';?>" placeholder="Enter Phone number" id="register_phone" class="form-control" required="" data-parsley-required-message="Please Enter Phone number">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Nationality </label>
									<?php
									if(isset($member_detail['nationality'])){
										$field_value = $member_detail['nationality'];
									}else{
										$field_value = NULL;
									}
									?>
										<input type="text" name="nationality" id="nationality"  value="<?php echo $field_value; ?>" placeholder="Enter Nationality" class="form-control" required list="nationality_list" data-parsley-required-message="Please Enter Nationality">
										<datalist id="nationality_list">
											<?php
											if(isset($nationality) && count($nationality) > 0){
												foreach ($nationality as $key => $value) {
													?>
													<option value="<?php echo $value['name']; ?>" />
													<?php
												}
											}
											?>
										</datalist>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Passport number</label>
									<input type="text" name="passport" value="<?=$member_detail['passport'] ?? '';?>" placeholder="Enter Passport number" class="form-control" required>
								</div>
							</div>
							<!-- <div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<p class="t-darkblue font600 mb-0">Upload Passport Photo</p>
									<label for="" class="custom-file-label t-darkblue font-13 mb-0">Upload Passport Photo</label>
									<input type="file" class="custom-file-input" id="customFile" name="filename">
								</div>
							</div> -->
						
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">NHS number</label>
									<input type="text" name="nhs_number" value="<?=$member_detail['nhs_number'] ?? '';?>" value="<?=$member_detail['nhs_number'] ?? '';?>" required="" data-parsley-required-message="Please Enter NHS number" data-parsley-length="[10, 10]" data-parsley-length-message="Please enter valid NHS number" placeholder="Enter NHS number" class="form-control">
								</div>
							</div>
						
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of birth </label>
									<div class="relative">
										<input type="text" id="dob" value="<?=$member_detail['date_of_birth'] ?? '';?>" readonly="" required="" name="date_of_birth" id="dob" data-parsley-required-message="Please Enter Date of birth"  placeholder="Enter Date of Birth" class="form-control pr-4" readonly>
									  <!--   <button type="submit" class="date-range-btn"><img src="images/date-range-icon.png" alt=""></button> -->
									</div>
								</div>
							</div>
						
						</div>
						<div class="mt-5 d-flex justify-content-center align-items-center flex-wrap flex-wrap-reverse">
							<a href="<?=base_url('change-password')?>" class="btn-blue font-16 m-2">Change Password</a>
							<!-- <a href="#" class="btn-green-lg font-16 m-2">Save</a> -->
							<div class="text-center">
								<input class="btn-green-lg text-success m-2" type="submit" name="submit" value="Save">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
