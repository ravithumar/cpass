<div class="col-md-4 col-lg-3">
	<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
		<ul class="my-profile-nav pl-1 py-4 py-5">
			<?php
			$new_booking_routes = array('new-booking', 'member-list', 'select-members', 'review-confirm', 'select-payment-card');
			?>
			<li class="<?php echo (in_array($this->uri->segment(1), $new_booking_routes)  == 'new-booking')?'active':''; ?>"><a href="<?=base_url('new-booking')?>" title="" class="prof-nav-booking text-white">New Order</a></li>
			<?php
			$my_booking_routes = array('my-booking');
			?>
			<li class="<?php echo (in_array($this->uri->segment(1), $my_booking_routes)  == 'new-booking')?'active':''; ?>"><a href="<?=base_url('my-booking')?>" title="" class="prof-nav-booking text-white">My Orders</a></li>
			<?php
			$members_routes = array('members/add', 'members');
			?>
			<li class="<?php echo (in_array($this->uri->segment(1), $members_routes)  == 'new-booking')?'active':''; ?>"><a href="<?=base_url('members')?>" title="" class="prof-nav-member text-white">Members</a></li>
			<li><a href="<?=base_url('edit-profile')?>" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
			<li><a href="#" title="" class="prof-nav-help text-white">Help & Support</a></li>
		</ul>
		<a href="#" title="" class="logout white-link mb-5">Logout</a>
	</div>
</div>