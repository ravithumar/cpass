
<section class="sign-up"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	 <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">

	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?php echo assets('web/images/c-pass-large-logo.png');?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">
	 		   <div class="skip-area">
		 		   	<!-- <a href="javascript:;" class="skip-one text-right text-white font-16 font600 position-absolute">
                      Skip <img src="<?php echo assets('web/images/right-arrow.png');?>" alt="Right Arrow" class="img-fluid ml-2">
			        </a> -->			   
	 		   </div>
				<form action="<?=base_url('signup')?>" id="register" method="post" enctype="multipart/form-data" accept-charset="utf-8" autocomplete="off">	 	
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token">		
					<div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
						<script type="text/javascript">
							$('.alert').delay(3000).fadeOut(300);
						</script> 
						<?php $this->load->view('message'); ?> 
						<h2 class="font-34 t-bluetwo font-weight-bold">Sign Up</h2>
						<p class="t-bluethree font-12">Enter to continue and explore within</p>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="name" class="mb-0 font500 font-13">Full Name</label> 
							<input type="text" class="form-control font-14" id="name" name="name" required="" data-parsley-pattern="/^[a-zA-Z\s]+$/" data-parsley-pattern-message="Please enter only character" data-parsley-maxlength="70" data-parsley-required-message="Please Enter Full Name" aria-describedby="fullname" placeholder="Enter your full name">
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="email" class="mb-0 font500 font-13">Email Address (Unique ID for user)</label>
							<input type="email" class="form-control font-14" id="email" name="email" required="" data-parsley-type="email" data-parsley-type-message="Please enter a valid email address." data-parsley-checkemail="" data-parsley-checkemail-message="Email Already Exists" data-parsley-trigger="focusout" data-parsley-required-message="Please Enter Email Address" aria-describedby="email" placeholder="Enter your email address">
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative register_country_code">
							<label for="phone" class="mb-0 font500 font-13">Phone Number</label>
							<input type="text" class="form-control font-14" id="register_phone" name="phone" required="" data-parsley-required-message="Please Enter Phone Number" placeholder="Enter your phone number">
						</div>
						<div class="form-group mb-3 mb-sm-4 position-relative">
							<label for="password" class="mb-0 font500 font-13">Password</label>
							<input type="password"  class="form-control font-14" id="password" aria-describedby="password" name="password" required="" data-parsley-minlength="8" data-parsley-minlength-message="Minimum 8 characters are required" data-parsley-required-message="Please Enter Password" placeholder="Enter your password">
							<a href="javascript:void(0)" class="icon position-absolute password_checker eye-click">
								<img src="<?php echo assets('web/images/eyes.svg');?>" alt="Eyes" class="img-fluid">
							</a>
						</div>
						<div class="btn-wrap my-4 my-md-5">
							<button type="submit" onclick="$('#register').submit();" class="btn-green-lg d-block font600 border-r8">Sign Up</button>
						</div>  				    
						<p class="text-center t-bluefour font-12 mb-0">Already Have an Account? <a href="<?=base_url('login')?>" class="t-green font-14 font600 text-underline">Sign In</a></p>
					</div>
				</form>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
