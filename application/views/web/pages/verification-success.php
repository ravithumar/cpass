<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Us</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/logo/logo.png') ?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('v3/css/app.min.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/style-2.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive.css');?>">
	<link rel="stylesheet" href="<?php echo assets('web/css/responsive1.css');?>">
	
</head>
<?php   
	$url = $this->uri->segment(1); 
?>
<body class="relative ">
<section class="sign-up"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	 <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">

	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="<?php echo assets('web/images/c-pass-large-logo.png');?>" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>	
	 		</div>
	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">
				<div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					Your email verification has been successfully.
				</div>
					<div class="btn-wrap my-4 my-md-5">
						<a href="<?=base_url()?>about-us" class="btn-green-lg d-block font600 border-r8">Continue</a>
					</div>  				    
				</div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
<script src="<?php echo assets('web/js/jquery.min.js');?>"></script>
<script src="<?php echo assets('web/js/bootstrap.min.js');?>"></script>
</body>
</html>
