 window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
 }, 4000);
 $('.reset').click(function(){
 	 window.history.back();
 });
 
 $('.confirm_model').click(function(){
 	alert();
 	// $('#confirm_delete_model').model('show');
 });
 function delete_confirm(e)
 {
 	
 	swal({
	  title: "Are you sure that you want to delete this record?",
	  text: "You will not be able to recover this imaginary file!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, delete it!",
	  cancelButtonText: "No, cancel please!",
	  closeOnConfirm: false,
	  closeOnCancel: false
	},
	function(isConfirm) {
	  if (isConfirm) {
	    swal("Deleted!", "Your imaginary file has been deleted.", "success");
	    location.href=$(e).attr('data-href');
	  } else {
	    swal("Cancelled", "Your imaginary file is safe :)", "error");
	  }
	});
 }
 $(function() {
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {
          console.log($(this).get(0).files);
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if( input.length ) {
                input.val(log);
            } else {
                
            }
        });
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
        $(":file").change(function() {
          readURL(this);
        });
    });
          
