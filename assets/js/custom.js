$('.change_status').click(function(){
    alert();
});
function fun_change_state(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    swal({
      title: "Are you sure that you want to change status of this record?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, change it!",
      cancelButtonText: "No, cancel please!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        swal("Status changed!", "status has been changed.", "success");
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if(xmlhttp.responseText == 1)
                {
                    $('.status_'+id).html('Active');
                    $('.status_'+id).removeClass('label-danger');
                    $('.status_'+id).addClass('label-success');
                }
                else if(xmlhttp.responseText == 0)
                {
                    $('.status_'+id).html('Inactive');
                    $('.status_'+id).addClass('label-danger');
                    $('.status_'+id).removeClass('label-success');  
                }
            }
        };
        xmlhttp.open("GET",BASE_URL+'admin/change/status/'+table+'/'+id,true);
        xmlhttp.send();
      } else {
        swal("Cancelled", table+"   record status is safe :)", "error");
      }
    });
}
// $.extend($.validator.messages, {
//         required: "هذا الحقل إلزامي",
//         remote: "يرجى تصحيح هذا الحقل للمتابعة",
//         email: "رجاء إدخال عنوان بريد إلكتروني صحيح",
//         url: "رجاء إدخال عنوان موقع إلكتروني صحيح",
//         date: "رجاء إدخال تاريخ صحيح",
//         dateISO: "رجاء إدخال تاريخ صحيح (ISO)",
//         number: "رجاء إدخال عدد بطريقة صحيحة",
//         digits: "رجاء إدخال أرقام فقط",
//         creditcard: "رجاء إدخال رقم بطاقة ائتمان صحيح",
//         equalTo: "رجاء إدخال نفس القيمة",
//         extension: "رجاء إدخال ملف بامتداد موافق عليه",
//         maxlength: $.validator.format( "الحد الأقصى لعدد الحروف هو {0}" ),
//         minlength: $.validator.format( "الحد الأدنى لعدد الحروف هو {0}" ),
//         rangelength: $.validator.format( "عدد الحروف يجب أن يكون بين {0} و {1}" ),
//         range: $.validator.format( "رجاء إدخال عدد قيمته بين {0} و {1}" ),
//         max: $.validator.format( "رجاء إدخال عدد أقل من أو يساوي {0}" ),
//         min: $.validator.format( "رجاء إدخال عدد أكبر من أو يساوي {0}" ),
//         emailValidation: "من فضلك أدخل بريد أليكترونى صحيح.",
//         equalTo: "من فظلك ادخل نفس القيمة مرة أخرى."
//     });
