class CoreClass {
    filter(e, t, a = 0) {
        $("#filter_form").submit(function() {
            var r = $(this).serialize().split("&"),
                s = {};
            for (var l in r) s[r[l].split("=")[0]] = r[l].split("=")[1];                
            return $(e).dataTable().fnDestroy(), $(e).DataTable({
                processing: !0,
                serverSide: !0,
                searchDelay: 500,
                autoWidth: !0,
                responsive: false,
                lengthMenu: [
                    [10, 25, 50, 10000],
                    [10, 25, 50, "All"]
                ],
                order: [
                    [0, "desc"]
                ],
                ajax: {
                    url: t,
                    type: "POST",
                    data: s
                },
                createdRow: function(e, t, a) {
                    $(e).attr("id", "row_" + t[0])
                }
            }), a && $.extend($.fn.dataTable.defaults, {
                // dom: "Bfrtip",
                // buttons: [{
                //     extend: "csv",
                //     title: "Report-" + Date.now(),
                //     text: '<i class="fa fa-file-excel-o"></i>',
                //     exportOptions: {
                //         modifier: {
                //             page: "applied",
                //             order: "applied"
                //         }
                //     }
                //     // ,
                //     // action: function(e, dt, node, config) {
                //     //     var myButton = this;
                //     //     dt.one('draw', function() {
                //     //         jQuery.fn.dataTable.ext.buttons.excelHtml5.action.call(myButton, e, dt, node, config);
                //     //     });
                //     //     // dt.page.len(dt.page.len()).draw();
                //     //     dt.page.len(1000).draw();
                //     // }
                // }, {
                //     extend: ["pageLength"],
                // }]
            }), !1
        })
    }
    filter_export(e, t, a = 1) {
        $("#filter_form").submit(function() {
            var data = $(this).serialize().split("&");
            var obj = {};
            for (var key in data) {
                console.log(data[key]);
                obj[data[key].split("=")[0]] = data[key].split("=")[1];
            }
            $(e).dataTable().fnDestroy();
            $(e).DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                lengthMenu: [
                    [10, 25, 50, 10000],
                    [10, 25, 50, "All"]
                ],
                fixedHeader: {
                        headerOffset: 63,
                        header:true
                    },
                searchDelay: 500,
                autoWidth: true,
                order: [
                    [0, "desc"]
                ],
                // dom: "Bfrtip",
                // buttons: [{
                //     extend: "csv",
                //     title: "Report-" + Date.now(),
                //     text: '<i class="fa fa-file-excel-o"></i>',
                //     exportOptions: {
                //         modifier: {
                //             page: "all",
                //             search: 'none'
                //         }
                //     }
                // }, {
                //     extend: ["pageLength"],
                // }],
                ajax: {
                    url: t,
                    type: "POST",
                    data: obj
                },
                createdRow: function(row, data, dataIndex) {
                    $(row).attr("id", "row_" + data[0]);
                }

            });
            // $.extend(true, $.fn.dataTable.defaults, {
            //     dom: "Bfrtip",
            //     buttons: [{
            //         extend: "csv",
            //         title: "Report-" + Date.now(),
            //         text: '<i class="fa fa-file-excel-o"></i>',
            //         exportOptions: {
            //             modifier: {
            //                 search: "applied",
            //                 order: "applied"
            //             }
            //         }
            //     }, {
            //         extend: ["pageLength"],
            //     }]
            // });
            return false;
        })
    }
    filter_export1(e, t, a = 1) {
        // $("#filter_form").submit(function() {
            var data = $(this).serialize().split("&");
            var obj = {};
            for (var key in data) {
                console.log(data[key]);
                obj[data[key].split("=")[0]] = data[key].split("=")[1];
            }
            $(e).dataTable().fnDestroy();
            $(e).DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                lengthMenu: [
                    [10, 25, 50, 10000],
                    [10, 25, 50, "All"]
                ],
                fixedHeader: {
                    headerOffset: 63,
                    header:true
                },
                searchDelay: 500,
                autoWidth: true,
                order: [
                    [0, "desc"]
                ],
                dom: "Bfrtip",
                buttons: [{
                    extend: "csv",
                    title: "Report-" + Date.now(),
                    text: '<i class="fa fa-file-excel-o"></i>',
                    exportOptions: {
                        modifier: {
                            page: "all",
                            search: 'none'
                        }
                    }
                }, {
                    extend: ["pageLength"],
                }],
                ajax: {
                    url: t,
                    type: "POST",
                    data: obj
                },
                createdRow: function(row, data, dataIndex) {
                    $(row).attr("id", "row_" + data[0]);
                }
            });
    }
    reset(e, t, a = 0) {
        $(".refresh").click(function() {
            $("#filter_form")[0].reset(), $("#filter_form").parsley().reset(), $(e).dataTable().fnDestroy(), $(e).DataTable({
                processing: !0,
                serverSide: !0,
                searchDelay: 500,
                autoWidth: true,
                responsive: false,
                lengthMenu: [
                    [10, 25, 50, 10000],
                    [10, 25, 50, "All"]
                ],
                order: [
                    [0, "desc"]
                ],
                ajax: {
                    url: t,
                    type: "POST",
                    data: {}
                },
                createdRow: function(e, t, a) {
                    $(e).attr("id", "row_" + t[0])
                }
            }), a && $.extend($.fn.dataTable.defaults, {
                
            })
        })
    }

    async Request(url, method, param = []) {
        if (method == 'get') {
            return $.get(url, function(result) {
                Promise.resolve(result);
            });
        } else {
            return $.post(url, param, function(result) {
                Promise.resolve(result);
            });
        }

    }
    exportAll(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function(e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
            dt.one('preDraw', function(e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function(e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
}
CoreClass = new CoreClass;