 var validator = $(".form-validate").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        // Different components require proper error label placement
        errorPlacement: function(error, element) {
                if (element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }else if (element.parents('div').hasClass('input-group')) {
                    error.insertAfter(element.parent());
                }else{
                    error.insertAfter(element);
                }    
                
        },
        validClass: "validation-valid-label",
        rules: {
            first_name:{
                required: true,
                minlength:2,
                maxlength:50,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            last_name:{
                required: true,
                minlength:2,
                maxlength:50,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            email: {
                required:true,
                emailValidation:true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            city:{
                required: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            phone:{
                required: true,
                digits:true,
                minlength: 8,
                maxlength: 9,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            password:{
                required: true,
                minlength: 6
            },
            c_password:{
                required: true,
                equalTo: "#password"
            },
            user_image:{
                accept: "image/jpg, image/jpeg, image/png",
                filesize: 2
            }
        },
        messages: {
            email:{
                emailValidation: "Please enter a valid email address."
            },
            user_image:{
                accept: "Accepted image formats: jpg, jpeg, png",
                filesize: "File size limit executed: 2MB Maximum"
            }
        },
        submitHuser_imageandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod("emailValidation", function (value, element) {
        return this.optional(element) || /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(value);
    });
    $.validator.addMethod("filesize", function(value, element, param) {
        return this.optional(element) || ((element.files[0].size/1024)/1024 <= param);
    });
    // $( document ).ready(function(){
    //     $('[name="phone"]').inputmask("999999999",{"placeholder": ""});
    // });
