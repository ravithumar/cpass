function initialize() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 20,
        center: new google.maps.LatLng(24.0233697, 40.5809056),
        // disableDefaultUI: true,
        zoomControl: true
    });
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(24.0233697, 40.5809056),
        title: 'Point A',
        map: map,
        draggable: false
    });
    var polyOptions = {
        strokeWeight: 0,
        fillOpacity: 0.45,
        editable: true,
        draggable: true
    };
    if (poyArea != "") {
        var triangleCoords = poyArea;
        var bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true,
            draggable: true
        });
        google.maps.event.addListener(bermudaTriangle.getPath(), 'set_at', function() {
            console.log(bermudaTriangle.getPath());
            var pathArr = bermudaTriangle.getPath();
            var arr = [];
            for (var i = 0; i < pathArr.length; i++) {
                arr.push({
                    lat: pathArr.getAt(i).lat(),
                    lng: pathArr.getAt(i).lng(),
                });
            };
            // console.log(JSON.stringify(arr));
            $('#cords').val(JSON.stringify(arr));
        });
        bermudaTriangle.setMap(map);
    }
    // Creates a drawing manager attached to the map that allows the user to draw
    // markers, lines, and shapes.
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: ['polygon'] //, 'circle', 'polygon', 'polyline', 'rectangle'
        },
        map: map
    });
    google.maps.event.addListener(drawingManager, 'drag', function() {
        console.log('Bounds changed.');
    });
    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(event) {
        var pathArr = event.getPath();
        var arr = [];
        for (var i = 0; i < pathArr.length; i++) {
            arr.push({
                lat: pathArr.getAt(i).lat(),
                lng: pathArr.getAt(i).lng(),
            });
        };
        console.log(JSON.stringify(arr));
        $('#cords').val(JSON.stringify(arr));
    });
    // google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
    // google.maps.event.addListener(map, 'click', clearSelection);
}
google.maps.event.addDomListener(window, 'load', initialize);