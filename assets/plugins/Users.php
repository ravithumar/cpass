<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
class Users extends REST_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Ticket');
		$this->load->model('Campaigns');
	}
	public function campagion_list_get($page = 1) {
		$limit = 10;
		$skip = ($page - 1) * $limit;
		$campagion = $this->db->query('SELECT c.id,u.name as merchant_name,u.image as merchant_image,c.title,c.image,c.start_time,c.end_time,c.price,c.youtube_link FROM `campaigns` as c join users as u on u.id=c.merchant_id where DATE_FORMAT(NOW(),"%m-%d-%Y") BETWEEN DATE_FORMAT(SUBSTRING_INDEX(c.start_time," ",1), "%m-%d-%Y") and DATE_FORMAT(SUBSTRING_INDEX(c.end_time," ",1), "%m-%d-%Y") order by c.id desc limit ' . $skip . ',' . $limit)->result();
		$data['status'] = true;
		$data['data'] = $campagion;
		$this->response($data);
	}
	public function merchant_list_get($page = 1, $user_id = "") {
		$limit = 10;
		if ($page == 0) {
			$page = 1;
		}
		$skip = ($page - 1) * $limit;
		$merchant = $this->db->query('select users.id,users.name,users.image,users.prefix,count(c.id) as user_count from users join user_roles on user_roles.user_id=users.id right join campaigns as c on c.merchant_id=users.id  where user_roles.role_id=2 group by users.id order by users.id desc limit ' . $skip . ',' . $limit)->result();
		$i = 0;
		$data = [];
		foreach ($merchant as $value) {
			$data[$i] = $value;
			// $campagion = $this->db->query('SELECT id,title,image,start_time,end_time,price,youtube_link FROM `campaigns`where  merchant_id = ' . $value->id . ' order by id desc')->result();
			$campagion=Campaigns::select('id','title','image','start_time','end_time','price','youtube_link')->where('merchant_id',$value->id)->orderBy('id','desc')->get();
			
			$data[$i]->play = true;
			$data[$i]->tickets_count = 0;
			if ($user_id != "") {
				$data[$i]->tickets_count = Ticket::whereMerchantId($value->id)->whereUserId($user_id)->whereCampaign_id(0)->count();
			}
			$data[$i]->campagions = $campagion;
			$i++;
		}
		$resp['status'] = true;
		$resp['data'] = $data;
		$resp['tickets'] = Ticket::whereUser_id($user_id)->count();
		$this->response($resp);
	}
	public function update_post() {
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('dob', 'dob', 'required');
		$this->form_validation->set_rules('bio', 'bio', 'required');
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			if (isset($_FILES['image'])) {
				if ($_FILES['image']['name'] != "") {
					$image_resp = image_upload('image', 'images/users');
					if ($image_resp['status']) {
						$request['image'] = $image_resp['file_name'];
					}
				}
			}
			$user_id = $request['user_id'];
			unset($request['user_id']);
			unset($request['imageList']);
			$user = User::whereId($user_id)->update($request);
			$user_response = User::find($user_id);
			$role = $this->db->get_where('user_roles', array('user_id' => $user_id))->row();
			$user_response->role = $role->role_id;
			unset($user_response->password);
			unset($user_response->address);
			unset($user_response->gender);
			unset($user_response->prefix);
			unset($user_response->deleted_at);
			unset($user_response->updated_at);
			$data['status'] = true;
			$data['data'] = $user_response;
		}
		$this->response($data);
	}
	public function validation_errors_response() {
		$err_array = array();
		$err_str = "";
		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));
		$err_str = ltrim($err_str, '|');
		$err_str = rtrim($err_str, '|');
		// $err_array = explode('|', $err_str);
		// $err_array = array_filter($err_array);
		return $err_str;
	}
	public function scan_ticket_post() {
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('merchant_id', 'merchant_id', 'required');
		$this->form_validation->set_rules('qty', 'qty', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$ticket = [];
			$ticket_data['user_id'] = $request['user_id'];
			$ticket_data['merchant_id'] = $request['merchant_id'];
			$merchant = User::find($request['merchant_id']);
			for ($i = 0; $i < $request['qty']; $i++) {
				$ticket[$i] = $merchant->prefix . rand(1000, 9999);
				$ticket_data['code'] = $ticket[$i];
				Ticket::insert($ticket_data);
			}
			$data['status'] = true;
			$data['data'] = $ticket;
		}
		$this->response($data);
	}
	public function tickets_get($user_id = "", $merchant_id = "") {
		if ($user_id != "" && $merchant_id != "") {
			$tickets = Ticket::select('id', 'code', 'merchant_id', 'campaign_id', 'bid_number', 'ticket_group')->whereUserId($user_id)->whereMerchantId($merchant_id)->get();
			$data['status'] = true;
			$data['data'] = $tickets;
		} else {
			$data['status'] = false;
			$data['error'] = "Missing parameter!";
		}
		$this->response($data);
	}
	public function history_get($page = 1, $user_id) {
		if ($user_id != "") {
			$limit = 10;
			if ($page == 0) {
				$page = 1;
			}
			$skip = ($page - 1) * $limit;
			$tickets = $this->db->query('select u.id,u.name,u.image,count(t.id) as ticket_count from tickets as t join users as u on u.id=t.merchant_id where t.user_id = ' . $user_id . ' group by t.merchant_id order by t.id desc limit ' . $skip . ',' . $limit)->result();
			$i = 0;
			$ticket_array = [];
			foreach ($tickets as $value) {
				$ticket_array[$i] = $value;
				$ticket_array[$i]->tickets = Ticket::select('id', 'code', 'bid_number','winner')->whereMerchantId($value->id)->get();
				$i++;
			}
			$data['status'] = true;
			$data['data'] = $ticket_array;
		} else {
			$data['status'] = false;
			$data['error'] = "Missing parameter!";
		}
		$this->response($data);
	}
	public function generate_bid_post() {
		$this->form_validation->set_rules('user_id', 'user_id', 'required');
		$this->form_validation->set_rules('merchant_id', 'merchant_id', 'required');
		$this->form_validation->set_rules('campaign_id', 'campaign_id', 'required');
		$this->form_validation->set_rules('qty', 'qty', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = $this->validation_errors_response();
		} else {
			$request = $this->post();
			$check_user_number = Ticket::where('merchant_id', $request['merchant_id'])->where('bid_number', '!=', null)->where('user_id',	$request['user_id'])->get();
			if($request['qty'] <= 2)
			{
				$get_total_ticket = Ticket::whereCampaign_id($request['campaign_id'])->where('bid_number', '!=', null)->get();
				$check_max_bid=Campaigns::find($request['campaign_id']);
				if($check_max_bid->max_bid >= count($get_total_ticket))
				{
				$serial_no = [11, 12, 13, 14, 15, 16, 21, 22, 23, 24, 25, 26, 31, 32, 33, 34, 35, 36, 41, 42, 43, 44, 45, 46, 51, 52, 53, 54, 55, 56, 61, 62, 63, 64, 65, 66];
				$merchant = User::find($request['merchant_id']);
				$campaigns = Campaigns::find($request['campaign_id']);
				$end_date = rtrim($campaigns->end_time, 'AM');
				$end_date = rtrim($end_date, 'PM');
				
				if (strtotime(date('Y-m-d h:i')) <= strtotime($end_date)) {
					$blank_tickets = Ticket::where('merchant_id', $request['merchant_id'])->where('bid_number', '=', null)->limit($request['qty'])->orderBy('id', 'asc')->get();
					$j = 0;
					$bid_number = [];
					foreach ($blank_tickets as $row) {
						$tickets = Ticket::where('merchant_id', $request['merchant_id'])->where('bid_number', '!=', '')->count();
						// $tickets = 359;
						$tickets = $tickets + 1;
						if (is_float($tickets / 36)) {
							$group = (floor($tickets / 36) + 1);
							$total = $tickets - (($group - 2) * 36);
						} else {
							$group = floor($tickets / 36);
							$total = $tickets - (($group - 1) * 36);
						}
						// echo $group."============".($total-37)."==========".$total;exit;
						if ($total == 36) {
							$bid_number[$j] = $merchant->prefix . '-' . (rand(1000, 9999) + $group) . '-' . $serial_no[35];
							$bid_data['bid_number'] = $bid_number[$j];
							$bid_data['ticket_group'] = $group;
							$bid_data['bid_date'] = date('Y-m-d h:i:s');
						} else {
							$bid_number[$j] = $merchant->prefix . '-' . (rand(1000, 9999) + $group) . '-' . $serial_no[($total - 37)];
							$bid_data['bid_number'] = $bid_number[$j];
							$bid_data['ticket_group'] = $group;
							$bid_data['bid_date'] = date('Y-m-d h:i:s');
						}
						$bid_data['campaign_id'] = $request['campaign_id'];
						$j++;
						Ticket::whereId($row->id)->update($bid_data);
					}
					$data['status'] = true;
					$data['data'] = $bid_number;
				}
				else
				{
					$data['status'] = false;
					$data['error'] = "Campaign date is expired";
				}
			}
			else
			{
				$data['status'] = false;
				$data['error'] = "Campaign Max bid limit is over";
			}
			} else {
				$data['status'] = false;
				$data['error'] = "User only allow to play 2 tickets!";
			}
		}
		$this->response($data);
	}
	public function test_get($id) {
		$campaigns = Campaigns::find($id);
		$end_date = rtrim($campaigns->end_time, 'AM');
		$end_date = rtrim($end_date, 'PM');
		if (time() < strtotime($end_date)) {
			echo "Allow";
		} else {
			echo "not allowed";
		}
	}
}
