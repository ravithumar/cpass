<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <h4 class="page-title text-uppercase"><?php echo readSession('name'); ?></h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?php echo __('Dashboard'); ?></a></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo $title; ?></a></li>
                    <li class="breadcrumb-item active"><a href="#">Edit</a></li>
                </ol>
            </div>
        </div>
        <?php $this->load->view('includes/message'); ?>
        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 m-b-20 header-title"><?php echo $title; ?></h4>
                    <p></p>
                    <form action="<?php echo base_url('farm/AnimalController/update/'.$animal->id) ?>" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label><?php echo __('Name') ?></label>
                                    <input type="text" class="form-control" value="<?php echo $animal->name; ?>" name="name" parsley-trigger="change" required>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('Tag') ?></label>
                                    <input type="text" class="form-control" value="<?php echo $animal->tag; ?>" name="tag" parsley-trigger="change" required>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('DOB') ?></label>
                                    <input type="text" class="form-control datepicker" readonly value="<?php echo $animal->dob; ?>" name="dob" data-parsley-trigger="change" required>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('DOB Weight(kgs)') ?></label>
                                    <input type="text" class="form-control touch_input" value="<?php echo $animal->dob_weight==''?0:$animal->dob_weight; ?>" name="dob_weight" data-parsley-trigger="change"  required data-parsley-errors-container=".dob_weight_error" required>
                                    <span class="dob_weight_error"></span>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('DOB Height(cm)') ?></label>
                                    <input type="text" class="form-control touch_input" value="<?php echo $animal->dob_height==''?0:$animal->dob_height; ?>" name="dob_height" data-parsley-trigger="change"  required data-parsley-errors-container=".dob_height_error" required>
                                    <span class="dob_height_error"></span>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label><?php echo __('Type') ?></label>
                                    <select class="form-control" required data-parsley-trigger="change" name="type">
                                        <option value="">Select Type</option>
                                        <option value="<?php echo $animal->type; ?>" selected><?php echo $animal->type; ?></option>
                                        <?php inputSelect($types,'type','type',$animal->type); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('Gender') ?></label>
                                    <div class="mt-1 ml-1">
                                        <div class="radio radio-success mb-2">
                                            <input required type="radio" <?php echo $animal->gender=='male'?"checked":""; ?>  name="gender" id="radio14" value="male">
                                            <label for="radio14" class="m-r-10"><b>Male</b></label>
                                            <input required type="radio" name="gender" <?php echo $animal->gender=='female'?"checked":""; ?> id="radio15" value="female" >
                                            <label for="radio15"><b>Female</b></label>
                                        </div>
                                       
                                    </div>
                                    
                                </div>
                                 <div class="form-group">
                                    <label><?php echo __('Twins') ?></label>
                                    <input type="text" class="form-control touch_input" value="<?php echo $animal->twins ==''?0:$animal->twins ; ?>" name="twins" data-parsley-trigger="change"  required data-parsley-errors-container=".error_twins">
                                    <span class="error_twins"></span>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('Sire code') ?></label>
                                    <select class="form-control" required data-parsley-trigger="change" name="sire_code">
                                        <option value="">Select sire</option>
                                        <?php inputSelect($sire,'tag','tag',$animal->sire_code); ?>
                                    </select>
                                  
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('Dam') ?></label>
                                    <select class="form-control" required data-parsley-trigger="change" name="dam_code">
                                        <option value="">Select dam</option>
                                        <?php inputSelect($dam,'tag','tag',$animal->dam_code); ?>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label><?php echo __('Grand dam') ?></label>
                                    <select class="form-control" required data-parsley-trigger="change" name="grand_dam_code">
                                        <option value="">Select grand dam</option>
                                        <?php inputSelect($dam,'tag','tag',$animal->grand_dam_code); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo __('Color') ?></label>
                                    <input type="text" class="form-control" value="<?php echo $animal->color; ?>" name="color" parsley-trigger="change">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-default waves-effect waves-light float-right">Save</button>
                                <button type="button" class="btn btn-secondary waves-effect reset ">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo assets('plugins/moment/moment.js')?>"></script>
<script src="<?php echo assets('plugins/timepicker/bootstrap-timepicker.js')?>"></script>
<script src="<?php echo assets('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script>
<script src="<?php echo assets('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="<?php echo assets('plugins/clockpicker/js/bootstrap-clockpicker.min.js')?>"></script>
<script src="<?php echo assets('plugins/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script src="<?php echo assets('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')?>"></script>
<script type="text/javascript">
$('.datepicker').datepicker({
format:'yyyy-mm-dd'
}).on('changeDate', function(e) {
        $(this).parsley().validate();
        });
$(".touch_input").TouchSpin({
min: 0,
max: 100,
step: 5,
decimals: 0,
boostat: 5,
maxboostedstep: 10,
});
</script>