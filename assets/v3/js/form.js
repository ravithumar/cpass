//------------ slot validation ----------------//
$(document).ready(function () {

    $(document).on("click", ".submit_slot", function(e) {
        e.preventDefault();
        if(validate_slot1_store_time() && validate_slot2_store_time() && validate_slot3_store_time() && validate_between_slots_time())
        {
            console.log('hy');
            $('#add_slot').submit();
        }
        else
        {
                console.log(error_msg);
            $(".validation-availibality").append('<label class="validation-error-label text-danger" style="">'+error_msg+'</label>');
        }    
    })

});

function validate_slot1_store_time(){
    var return_val = true;
    $( ".slot-1.from_time" ).each(function( index ) {
        var from = "11/24/2014 "+ $(this).val();
        var to = "11/24/2014 "+ $(this).closest('tr').find('.slot-1.to_time').val();

        var fromDate = new Date(from).getTime();
        var toDate = new Date(to).getTime();

        if(fromDate >= toDate){
            error_msg = 'The slot 1 time of '+$(this).closest('tr').data("day").toLowerCase()+' is invalid.';
            return_val =  false;
            return return_val;
          }
    });
    return return_val;
}

function validate_slot2_store_time(){
    var return_val = true;
    $( ".slot-2.from_time" ).each(function( index ) {
        var from = "11/24/2014 "+ $(this).val();
        var to = "11/24/2014 "+ $(this).closest('tr').find('.slot-2.to_time').val();

        var fromDate = new Date(from).getTime();
        var toDate = new Date(to).getTime();

        if(fromDate > toDate){
            error_msg = 'The slot 2 time of '+$(this).closest('tr').data("day").toLowerCase()+' is invalid.';
            return_val =  false;
            return return_val;
          }
    });
    return return_val;
}

function validate_slot3_store_time(){
    var return_val = true;
    $( ".slot-3.from_time" ).each(function( index ) {
        var from = "11/24/2014 "+ $(this).val();
        var to = "11/24/2014 "+ $(this).closest('tr').find('.slot-3.to_time').val();

        var fromDate = new Date(from).getTime();
        var toDate = new Date(to).getTime();

        if(fromDate > toDate){
            error_msg = 'The slot 3 time of '+$(this).closest('tr').data("day").toLowerCase()+' is invalid.';
            return_val =  false;
            return return_val;
          }
    });
    return return_val;
}

function validate_between_slots_time(){
    var return_val = true;
    $( ".slot-1.to_time" ).each(function( index ) {
        var from = "11/24/2014 "+ $(this).val();
        var to = "11/24/2014 "+ $(this).closest('tr').find('.slot-2.from_time').val();

        var fromDate = new Date(from).getTime();
        var toDate = new Date(to).getTime();

        console.log('fromDate : '+fromDate+' || '+'toDate : '+toDate);

        if(fromDate > toDate){
            error_msg = 'The slot 2 time of '+$(this).closest('tr').data("day").toLowerCase()+' should be greater than slot 1.';
            return_val =  false;
            return return_val;
          }
    });
    $( ".slot-2.to_time" ).each(function( index ) {
        var from = "11/24/2014 "+ $(this).val();
        var to = "11/24/2014 "+ $(this).closest('tr').find('.slot-3.from_time').val();

        var fromDate = new Date(from).getTime();
        var toDate = new Date(to).getTime();

        console.log('fromDate : '+fromDate+' || '+'toDate : '+toDate);

        if(fromDate > toDate){
            error_msg = 'The slot 3 time of '+$(this).closest('tr').data("day").toLowerCase()+' should be greater than slot 2.';
            return_val =  false;
            return return_val;
          }
    });
    return return_val;
}