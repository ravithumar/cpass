
$(document).on("change","#country_id",function(){
		console.log("country_d");
		var csrf_token = $("#csrf_token").val();
		var purpose_id = $('input[name="purpose_id"]:checked').val();
		var country_id = $('#country_id').find(":selected").val();
		if(purpose_id != '' && country_id != '' && purpose_id == '3')
		{
			$.ajax({
						url: BASE_URL+'get-report-test',
						type: "POST",
						data:{
							purpose_id:purpose_id,
							csrf_token: csrf_token,
							country_id:country_id
						},
						success: function (returnData) {
								returnData = $.parseJSON(returnData);
								$('.add-fitfly .new-book1').show();
								$('.headingThreeDiv, #accordion-select01, .uk-address, .non-uk-address, .Exemption, .arrival_date, .shipping_date, .ship_add').css('display','none');
								var html = '';
								$.each(returnData  ,function(key,value){
										var checked = '';
										if(key == 0){
											checked = "checked";
										}
										html += '<li><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
								});
								$(".test_type_list").html(html);
						},
						error: function (xhr, ajaxOptions, thrownError) {
								console.log('error');
								Notiflix.Notify.Failure('Something went wrong');
						}
				}); 
		}
});
/* $("#country_id").on('change', function(){
	
}); */

function getReportTest(purpose_id){
  var csrf_token = $("#csrf_token").val();
  $.ajax({
      url: BASE_URL + "get-report-test",
      method: "POST",
      data: {
          csrf_token: csrf_token,
          purpose_id: purpose_id
      },
      dataType: "json",
      success: function(data) {
          var html = '';
          $.each(data,function(key,value){
              var checked = '';
              if(key == 0){
                checked = "checked";
              }
              html += '<li><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
          });
          $(".test_type_list").html(html);
      }
  });
}
