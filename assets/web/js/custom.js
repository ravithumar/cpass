// sticky header js
$(window).scroll(function(){
  if ($(window).scrollTop() >= 200) {
    $('.menu').addClass('sticky-header');
   }
   else {
    $('.menu').removeClass('sticky-header');
   }
});

// on page change add active class to link
$(function(){
  var current_page_URL = location.href;
  $( ".navbar-nav a" ).each(function() {
     if ($(this).attr("href") !== "#") {
       var target_URL = $(this).prop("href");
       if (target_URL == current_page_URL) {
          $('.navbar-nav a').parents('li').removeClass('active');
          $(this).parent('li').addClass('active');
          return false;
       }
     }
  });
});


// select address
$("ul.my-booking-lists li").on('click', function(){
    $("ul.my-booking-lists li").removeClass('active');
    $(this).addClass('active');
});


// payment-card-list
$(document).on('click',"ul.payment-card-list li", function(){
    $("ul.payment-card-list li").removeClass('active');
    $(this).addClass('active');
});


// onscroll  search box close js
// $(window).on('scroll', function(e){ 
//     $(".search-field1").removeClass('form-active'); 
// });

$( function() {
    /*$("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        format:"dd-mm-yyyy"
    });
    
    $("#booking_date_picker").datepicker({
        autoclose: true,
        todayHighlight: true,
        format:"dd-mm-yyyy",
       startDate: new Date(),
    }).change(dateChanged);*/
    
});

function dateChanged(ev) {
  var csrf_token = $("#csrf_token").val();
  $.ajax({
      url: BASE_URL + "get-hours",
      method: "POST",
      data: {
          csrf_token: csrf_token,
          date: $(this).val()
      },
      dataType: "json",
      success: function(data) {
          var options = "";
          $.each(data.current_time_array,function(key,value){
              options += '<option value="'+value+'">'+value+'</option>';
          });
          $("#test_hours").html(options);
      }
  });
}

//member-list-page
/*$(".member-list-page a.list-block").on('click', function(){
     $(this).toggleClass("active"); 
     
       
});*/


// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// search box
/* document.getElementById("click").addEventListener("click", function () {
  document.getElementById("search-field").classList.toggle("form-active");
}); */

// onscroll  search box close js
$(window).on('scroll', function(e){ 
    $(".search-field1").removeClass('form-active'); 
});

var purpose_id = $('input[name="purpose_id"]:checked').val();
//getReportTest(purpose_id);
$("#new-booking-radio input[name=purpose_id]:radio").click(function(ev) {
  $(".purpose_of_test_name").val($(this).data("value"));
  if (ev.currentTarget.value == "1") {
    $('#domestic1').parent().removeClass('select-radios');
    $('#domestic2').parent().addClass('select-radios');
    $('.domestic-radio-section').addClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('.domestic-radio-section').removeClass('add-international');
    $('#type-of-delivery, #test-type-booking').show();
    $('#select-destination-country, #departure-date-time-selection, #internatonal-arrival-booking, .proceed-to-book-div').hide();
    $('#date-selection, #time-selection').show();
    $('.roundtripsubmit').hide(); 
    $('.othersubmit').show(); 
    $('.return_dateclass').hide();
    $('.intersubmit').hide(); 
    $('#search-center-div').show(); 
    $('.return_dateroundtrip').hide();
    $("#search_hospital_frm").attr("action","javascript:void(0)");
    // search-center-div
  } 
  else if (ev.currentTarget.value == "2") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide(); 
    $('.roundtripsubmit').hide(); 
    $('.othersubmit').show(); 
    $('.return_dateclass').hide();
    $('.intersubmit').hide(); 
    $('.return_dateroundtrip').hide();
    $('#select-destination-country').hide();
    $('#type-of-delivery').hide();
    $('#departure-date-time-selection').hide();
    $('#date-selection, #time-selection, #search-center-div').hide();
    $('#test-type-booking').hide();
    $("#search_hospital_frm").attr("action","javascript:void(0)");
  }
  else if (ev.currentTarget.value == "5") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-international');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').show();
    $('#select-destination-country').hide();
    $('#test-type-booking').hide();
    $('#type-of-delivery').hide();
    $('.roundtripsubmit').hide(); 
    $('.othersubmit').hide(); 
    $('.intersubmit').show(); 
    $('.return_dateclass').show();
    $('#date-selection, #time-selection, #search-center-div').hide();
    $("#search_hospital_frm").attr("action","javascript:void(0)"); 
  }

  else if (ev.currentTarget.value == "3") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide(); 
    $('.roundtripsubmit').show(); 
    $('.othersubmit').hide(); 
    $('#select-destination-country').show();
    $('#type-of-delivery, .proceed-to-book-div, #internatonal-arrival-booking').hide();
    $('#departure-date-time-selection').show();
    $('#test-type-booking').show();
    $('.return_dateroundtrip').show();
    $('#search-center-div').show(); 
    $('.intersubmit').hide(); 
    $('#date-selection, #time-selection').show();
    $("#search_hospital_frm").attr("action",BASE_URL+"roundtrip-search");

  }
    else if (ev.currentTarget.value == "4") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide();

    $('#select-destination-country').show();
    $('#type-of-delivery, .proceed-to-book-div, #internatonal-arrival-booking').hide();
    $('#departure-date-time-selection').show();
    $('#date-selection, #time-selection, #search-center-div').show();
    $('#test-type-booking').show(); 
    $('.othersubmit').show(); 
    $('.intersubmit').hide(); 
    
    $('.return_dateclass').hide();    
    $('.roundtripsubmit').hide(); 
    $("#search_hospital_frm").attr("action","javascript:void(0)");
  }

  getReportTest(ev.currentTarget.value);

});

$("#country_id").change(function() {
  var purpose_id = $('input[name="purpose_id"]:checked').val();
  if(purpose_id != 3){

    var country_id = this.value;
    console.log(country_id);
    if(country_id != ''){
      getReportTest(purpose_id, country_id);
    }else{
      Notiflix.Notify.Warning('Please select any one country.');
    }
  }
  
});

function getReportTest(purpose_id, country_id = ''){

  $('#collapseThree, #collapseTwo').collapse('hide');

  var csrf_token = $("#csrf_token").val();
  $.ajax({
      url: BASE_URL + "get-report-test",
      method: "POST",
      data: {
          csrf_token: csrf_token,
          purpose_id: purpose_id,
          country_id: country_id
      },
      dataType: "json",
      success: function(data) {
        console.log(data);
          var html = '';
          $.each(data,function(key,value){
              var checked = '';
              if(key == 0){
                checked = "";
              }
              html += '<li><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
          });
          $(".test_type_list").html(html);
      }
  });
}

function getAddressFromPostcode(){
 
  var postcode = $("#PostcodeSearch").val();

   $.ajax({
        url: BASE_URL+'assets/web/file/address_json.json',
        // url: "https://api.getaddress.io/find/"+ postcode +"?expand=true&api-key=Hhg-CLw_0E69UohtFnZGvw32470",
        method:"GET",
        dataType: "json",  // jQuery will infer this, but you can set explicitly
        success: function( data, textStatus, jqXHR ) {
            // var resourceContent = data; // can be a global variable too...
          var html = '';
          html += '<option value="">Select Address</option>';
          $.each(data.addresses,function(key,value){
             
              html += '<option value="'+value.formatted_address+'">'+value.formatted_address+'</option>'
          });
          $("#select_address").html(html);
        }
    });
}


$("#passenger-status-radio input[name=status_radio1]:radio").click(function(ev) {
  
  if (ev.currentTarget.value == "1") {
    $('#status1').parent().removeClass('select-radios');
    $('#status2').parent().addClass('select-radios');
    $('.residency-radio-section').addClass('add-uk');
    $('.residency-radio-section').removeClass('add-non-uk');    
    
  } 
  else if (ev.currentTarget.value == "2") {
    $('#status1').parent().addClass('select-radios');
    $('#status2').parent().removeClass('select-radios');
    $('.residency-radio-section').addClass('add-non-uk');
    $('.residency-radio-section').removeClass('add-uk');

  }

});

// Check registration email duplication
$(document).ready(function() {

  getReportTest('1');

  $('.exists_email').parsley();

  var old_value = $("#old_name").val();
  var csrf_token = $("#csrf_token").val();
  var table_name = 'users';

  var field_name = 'email';


  window.ParsleyValidator.addValidator('checkemail', {

      validateString: function(value) {

          return $.ajax({

              url: BASE_URL + "check-duplicate-data",

              method: "POST",

              data: {

                  new_value: value,
                  csrf_token: csrf_token,

                  old_value: old_value,

                  table_name: table_name,

                  field_name: field_name

              },

              dataType: "json",

              success: function(data) {

                  return true;

              }

          });

      }

  });

});

$('.book-now-booking-info').click(function() {
    var title = "Booking Information";
    var purpose_of_test = $("#purpose_of_test_name").val();
    var test_type = $("#test_type_name").val();
    var date = $("#test_date").val();
    var time = $("#test_time").val();
    var place_test = $(this).attr('data-place-of-test-name');
    var place_test_id = $(this).attr('data-hospital-id');

    $('#booking-information-popup #purpose-of-test').html(purpose_of_test);
    $('#booking-information-popup #purpose-of-test').parent().show();

    $('#booking-information-popup #test-type').html(test_type);
    $('#booking-information-popup #test-type').parent().show();

    $('#booking-information-popup #place-of-test').html(place_test);
    $('#booking-information-popup #place-of-test').parent().show();

    $('#booking-information-popup #date').html(date);
    $('#booking-information-popup #date').parent().show();

    $('#booking-information-popup #time').html(time);
    $('#booking-information-popup #time').parent().show();

    $('#booking-information-popup #title').html(title);

    $('#selected-place-id').val(place_test_id);
    $('#selected-place-id').attr('data-place-name', place_test);
    $('#complete-your-registration').hide();
    $("#complete-your-registration-fit2fly-1way").attr("href", BASE_URL+'save-hospital-id/'+place_test_id)
    $('#complete-your-registration-fit2fly-1way').show();
    //$("#complete-your-registration-fit2fly-1way").attr("href", BASE_URL+"save-place-session")

    $('#booking-information-popup').modal('show');

      
  });

$('#complete-your-registration-fit2fly-1way').click(function(e){
  //$(this).
});

$('.time-slot li').click(function(e) {
    $('.time-slot li').removeClass('active');

    var $this = $(this);
    if (!$this.hasClass('active')) {
        $this.addClass('active');
    }
    //e.preventDefault();
});


$('.edit-detail-save-btn').click(function(){

  if($("#purpose_id").val() == "4"){

  }
  var booking_date = $("#booking_date").val();
  var test_type = $("#test_type :selected").val();
  var test_type_name = $("#test_type :selected").text();


  if(booking_date == ''){
    Notiflix.Notify.Failure('Test date is required.');
  }else if(test_type == ''){
    Notiflix.Notify.Failure('Test type is required.');
  }else{

    if($("#purpose_id").val() == "4" || ($("#purpose_id").val() == "1" && $("#place_type").val() == "1")){
      var selected_slot = $('.time-slot li.active').attr('data-id');
      if(selected_slot == undefined){
        Notiflix.Notify.Failure('Please select any one time slot.');
        return false;
      }
    }else{
      var selected_slot = '';
    }

    $.ajax({
        url: BASE_URL+'edit_detail_save',
        type: "POST",
        data:{
          test_date:booking_date,
          test_type:test_type,
          test_type_name:test_type_name,
          selected_slot:selected_slot
        },
        success: function (returnData) {
            returnData = $.parseJSON(returnData);
            console.log(returnData);
            window.location = BASE_URL+'review-confirm';
            //return false;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('error');
            Notiflix.Notify.Failure('Something went wrong');
        }
    }); 

  }

  

});

// Fit to fly round trip
$(document).on("submit","#search_hospital_frm",function(e){
  var purpose_id = $('input[name="purpose_id"]:checked').val();
  if(purpose_id == 3){
    
    if($("#country_id").val() == ""){
        Notiflix.Notify.Failure('Destination country is required');
        console.log("COUNTRY");
        return false;
    }
   
    else if($('input[type=radio][name=test_type]:checked').val() == undefined){
        Notiflix.Notify.Failure('Test type is required');
        return false;
    }
    else if($('#departure_date').val() == ""){
        Notiflix.Notify.Failure('Departure date is required');
        return false;
    }
    else if($('#departure_time').val() == ""){
        Notiflix.Notify.Failure('Departure time is required');
        return false;
    }
    else if($('#return_date1').val() == ""){
        Notiflix.Notify.Failure('Return date is required');
        return false;
    }
    else if($('#booking_date').val() == ""){
        Notiflix.Notify.Failure('Test date is required');
        return false;
    }
    else if($('#booking_time').val() == ""){
        Notiflix.Notify.Failure('Test time is required');
        return false;
    }
    
    else if($('#txtpostcode').val() == '' && $("#post_code_tab").hasClass("active")){
        Notiflix.Notify.Failure('Postcode is required');
        return false;
    }
    else if($('.regionId').val() == undefined && $("#county_tab").hasClass("active")){
        Notiflix.Notify.Failure('Region is required');
        return false;
    }
  }
});

//set booking session

$('#search-hospital-btn').click(function(e){

  var purpose_id = $('input[type=radio][name=purpose_id]:checked').val();
  var place_type = $('input[type=radio][name=place_test]:checked').val();
  

  if(purpose_id == '2'){
    Notiflix.Notify.Failure('Please select round trip or one way.');
    return false;
  }

  var test_date = $('#booking_date').val();
  var test_time = $('#booking_time').val();

  var departure_date = $('#departure_date').val();
  var departure_time = $('#departure_time').val();
  var country_id = $('#country_id').val();

  var postcode = $('#txtpostcode').val();
  //postcode = postcode.replace(/ /g,'');

  if(purpose_id == "1"){
    if(place_type == undefined){
      Notiflix.Notify.Failure('Type of delivery is required');
      return false;
    }
  }

  if(purpose_id == '4'){
      if(country_id == ''){
        Notiflix.Notify.Failure('Destination country is required');
        return false;
      }else if(departure_date == ''){
        Notiflix.Notify.Failure('Departure date is required');
        return false;
      }else if(departure_time == ''){
        Notiflix.Notify.Failure('Departure time is required');
        return false;
      }else if(test_date == ''){
        Notiflix.Notify.Failure('Test date is required');
        return false;
      }else if(test_time == ''){
        Notiflix.Notify.Failure('Test time is required');
        return false;
      }else if(postcode == '' && $("#post_code_tab").hasClass("active")){
        Notiflix.Notify.Failure('Postcode is required');
        return false;
      }else if($('.regionId').val() == undefined && $("#county_tab").hasClass("active")){
        Notiflix.Notify.Failure('Region is required');
        return false;
      }else{

      }

  }else if(purpose_id == '1' && place_type == '1'){
      if(test_date == ''){
        Notiflix.Notify.Failure('Test date is required');
        return false;
      }else if(test_time == ''){
        Notiflix.Notify.Failure('Test time is required');
        return false;
      }else if(postcode == '' && $("#post_code_tab").hasClass("active")){
        Notiflix.Notify.Failure('Postcode is required');
        return false;
      }else if($('.regionId').val() == undefined && $("#county_tab").hasClass("active")){
        Notiflix.Notify.Failure('Region is required');
        return false;
      }else{

      }

  }else{
      Notiflix.Notify.Failure('Something went wrong');
      return false;
  }

  var report_id = $('input[type=radio][name=test_type]:checked').val();
  //alert(report_id);
  var place_of_test_name = $('input[type=radio][name=place_test]:checked').attr('data-place-test-name');
  
  var purpose_of_test_name = $('input[type=radio][name=purpose_id]:checked').attr('data-purpose-type-name');
  var test_type_name = $('input[type=radio][name=test_type]:checked').attr('data-test-type-name');
  var test_price = $('input[type=radio][name=test_type]:checked').attr('data-test-type-price');
  if(report_id == undefined){
    Notiflix.Notify.Failure('Test type is required');
    return false;
  }
    
    $.ajax({
        url: BASE_URL+'save_booking_session',
        type: "POST",
        data:{
          place_type:place_type,
          report_id:report_id,
          purpose_id:purpose_id,
          country_id:country_id,
          place_of_test_name:place_of_test_name,
          purpose_of_test_name:purpose_of_test_name,
          test_type_name:test_type_name,
          test_price:test_price,
          test_date:test_date,
          test_time:test_time,
          departure_date:departure_date,
          departure_time:departure_time
        },
        success: function (returnData) {
            returnData = $.parseJSON(returnData);
            //select-members redirect
            //console.log(returnData);
            //return false;
						if($("#post_code_tab").hasClass("active")){
            	window.location = BASE_URL+'search?postcode='+postcode;
						}else{
							var regionId = $('.regionId option:selected').val();
							window.location = BASE_URL+'search?regionId='+regionId;
						}
        },
        error: function (xhr, ajaxOptions, thrownError) {
            
            Notiflix.Notify.Failure('Something went wrong');
        }
    });  

  
});

$('#complete-your-registration').click(function(){
    var place_type = $('input[type=radio][name=place_test]:checked').val();
    var report_id = $('input[type=radio][name=test_type]:checked').val();
    var purpose_id = $('input[type=radio][name=purpose_id]:checked').val();
    var place_of_test_name = $('input[type=radio][name=place_test]:checked').attr('data-place-test-name');
    var purpose_of_test_name = $('input[type=radio][name=purpose_id]:checked').attr('data-purpose-type-name');
    var test_type_name = $('input[type=radio][name=test_type]:checked').attr('data-test-type-name');
    var test_price = $('input[type=radio][name=test_type]:checked').attr('data-test-type-price');
    var test_date = $('#booking_date').val();
    var test_time = $('#booking_time').val();

    /*console.log('place_type '+place_type);
    console.log('report_id '+report_id);
    console.log('place_of_test_name '+place_of_test_name);
    console.log('purpose_of_test_name '+purpose_of_test_name);
    console.log('test_type_name '+test_type_name);
    console.log('test_date_time '+test_date_time);
    return false;*/

    $.ajax({
          url: BASE_URL+'save_booking_session',
          type: "POST",
          data:{
            place_type:place_type,
            report_id:report_id,
            purpose_id:purpose_id,
            place_of_test_name:place_of_test_name,
            purpose_of_test_name:purpose_of_test_name,
            test_type_name:test_type_name,
            test_price:test_price,
            test_date:test_date,
            test_time:test_time
          },
          success: function (returnData) {
              returnData = $.parseJSON(returnData);
              //select-members redirect
              console.log(returnData);
              window.location = BASE_URL+'select-members';
          },
          error: function (xhr, ajaxOptions, thrownError) {
              console.log('error');
              Notiflix.Notify.Failure('Something went wrong');
          }
      });  
});

$('#review-and-confirm').click(function(){

    $.ajax({
          url: BASE_URL+'is_member_selected',
          type: "GET",
          success: function (returnData) {
              returnData = $.parseJSON(returnData);
              console.log(returnData);
              //return false;
              if(returnData.status == true){
                window.location = BASE_URL+'review-confirm';
              }else{
                Notiflix.Notify.Failure('Please select at least one member');
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              console.log('error');
              Notiflix.Notify.Failure('Something went wrong');
          }
      });  
});


// Do you have an address in the United kingdom change

$('input[type=radio][name=is_homeaddress]').change(function() {
  console.log(this.value);
    if (this.value == 'home-address-yes') {

        $('.search-address').show();
        $('.address_dropdown').show();
        $('.address-txt').hide();

        $('#address').show();
        
        
    }
    else if (this.value == 'home-address-no') {

        $('.search-address').hide();
        $('.address_dropdown').hide();
        $('.address-txt').show();

        $('#address').hide();
        $('.address').show();

        $('.not-same-address').show();
         if($('#shipping_address_same').is(":checked")){
            $('#shipping_address_same').prop('checked', false);
            $('.same-address').show();
         }
    }
});

$('#shipping_address_same').change(function() {

  if($('input[type=radio][name=is_homeaddress]:checked').val() == 'home-address-yes'){

      if($(this).is(":checked")) {
          if($('#postcode1').val() == '' || $('.address_dropdown').val() == null){
              Notiflix.Notify.Failure('Please add address');
              return false;
          }
          $('.not-same-address').hide();
          $('.same-address').show();
          
          $('.same-postcode').html($('#postcode1').val());
          $('.same-address-txt').html($('.address_dropdown :selected').text());
          $("#shipping_address_line1").val($("#home_address_line1").val());
          $("#shipping_address_line2").val($("#home_address_line2").val());
          $("#shipping_address_line3").val($("#home_address_line3").val());
          $("#shipping_city").val($("#home_city").val());
          $("#shipping_state").val($("#home_state").val());
      }else{
          $('.not-same-address').show();
          $('.same-address').hide();
          var address_line1 = $(".shipping_address_split").find(':selected').attr("data-address_line1");
          var address_line2 = $(".shipping_address_split").find(':selected').attr("data-address_line2");
          var address_line3 = $(".shipping_address_split").find(':selected').attr("data-address_line3");
          var shipping_city = $(".shipping_address_split").find(':selected').attr("data-shipping_city");
          var shipping_state = $(".shipping_address_split").find(':selected').attr("data-shipping_state");
          $("#shipping_address_line1").val(address_line1);
          $("#shipping_address_line2").val(address_line2);
          $("#shipping_address_line3").val(address_line3);
          $("#shipping_city").val(shipping_city);
          $("#shipping_state").val(shipping_state);
      }

  }else{
      console.log('Not home address radio');
      if($(this).is(":checked")) {
        console.log('checkbox checked');
        console.log($('#postcode1').val(), $('.address-txt').val());
          if($('#postcode1').val() == '' || $('.address-txt').val() == ''){
              Notiflix.Notify.Failure('Please add address');
              return false;
          }
          $('.not-same-address').hide();
          $('.same-address').show();
          
          $('.same-postcode').html($('#postcode1').val());
          $('.same-address-txt').html($('.address-txt').val());
      }else{
          $('.not-same-address').show();
          $('.same-address').hide();
      }

  }
      
});


$('.search-home-address').click(function(){

      var postcode = $('#postcode1').val();
      if(postcode.trim() != ''){
        Notiflix.Loading.Standard();
        $.ajax({
                url: BASE_URL+'get-addresses-by-postcode',
                type: "POST",
                data:{
                  postcode:postcode
                },
                success: function (returnData) {
                   Notiflix.Loading.Remove();
                    returnData = $.parseJSON(returnData);
                    //console.log(returnData);
                    if (typeof returnData != "undefined" && returnData.status == true)
                    {
                        $.each(returnData.addresses, function (key, val) {
                          var full_address = val.formatted_address[0]+', '+val.formatted_address[1]+', '+val.formatted_address[2]+', '+val.formatted_address[3];
                          $('#home-address-dropdown').append($("<option></option>")
                                .attr("value", full_address)
                                .attr("data-address_line1",val.line_1)
                                .attr("data-address_line2",val.line_2)
                                .attr("data-address_line3",val.line_3)
                                .attr("data-shipping_city",val.town_or_city)
                                .attr("data-shipping_state",val.country)
                                .text(full_address)); 
                        });
                        Notiflix.Notify.Success('Please select your address');

                    }else{
                        Notiflix.Notify.Failure('Please enter valid postcode');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error in saving channel');
                    Notiflix.Notify.Failure('Something went wrong');
                }
            });   
      }
    

  });

$(document).on("change",".search-intl-home-address",function(){

      var postcode = $('#postcode1').val();
      if(postcode.trim() != ''){
        Notiflix.Loading.Standard();
        $.ajax({
                url: BASE_URL+'get-addresses-by-postcode',
                type: "POST",
                data:{
                  postcode:postcode
                },
                success: function (returnData) {
                   Notiflix.Loading.Remove();
                    returnData = $.parseJSON(returnData);
                    //console.log(returnData);
                    if (typeof returnData != "undefined" && returnData.status == true)
                    {
                        $('#home-address-dropdown').html("");
                        $.each(returnData.addresses, function (key, val) {
                          var full_address = val.formatted_address[0]+', '+val.formatted_address[1]+', '+val.formatted_address[2]+', '+val.formatted_address[3];
                          $('#home-address-dropdown').append($("<option></option>")
                                .attr("value", full_address)
                                .attr("data-address_line1",val.line_1)
                                .attr("data-address_line2",val.line_2)
                                .attr("data-address_line3",val.line_3)
                                .attr("data-shipping_city",val.town_or_city)
                                .attr("data-shipping_state",val.country)
                                .text(full_address)); 
                        });
                        //Notiflix.Notify.Success('Please select your address');

                    }else{
                        Notiflix.Notify.Failure('Please enter valid postcode');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error in saving channel');
                    Notiflix.Notify.Failure('Something went wrong');
                }
            });   
      }
    

  });

$('.search-shipping-address').click(function(){
      var postcode = $('#postcode2').val();
      if(postcode.trim() != ''){

        $.ajax({
                url: BASE_URL+'get-addresses-by-postcode',
                type: "POST",
                data:{
                  postcode:postcode
                },
                success: function (returnData) {
                    returnData = $.parseJSON(returnData);
                    if (typeof returnData != "undefined" && returnData.status == true)
                    {
                        $.each(returnData.addresses, function (key, val) {
                          var full_address = val.formatted_address[0]+', '+val.formatted_address[1]+', '+val.formatted_address[2]+', '+val.formatted_address[3];
                          $('#shipping-address-dropdown').append($("<option></option>")
                                .attr("value", full_address)
                                .attr("data-address_line1",val.line_1)
                                .attr("data-address_line2",val.line_2)
                                .attr("data-address_line3",val.line_3)
                                .attr("data-shipping_city",val.town_or_city)
                                .attr("data-shipping_state",val.country)
                                .text(full_address)); 
                        });
                        Notiflix.Notify.Success('Please select your address');

                    }else{
                        Notiflix.Notify.Failure('Please enter valid postcode');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error in saving channel');
                    Notiflix.Notify.Failure('Something went wrong');
                }
            });   
      }
    

  });

$(document).on("change",".shipping_address_split",function(){
    var address_line1 = $(this).find(':selected').attr("data-address_line1");
    var address_line2 = $(this).find(':selected').attr("data-address_line2");
    var address_line3 = $(this).find(':selected').attr("data-address_line3");
    var shipping_city = $(this).find(':selected').attr("data-shipping_city");
    var shipping_state = $(this).find(':selected').attr("data-shipping_state");
    console.log(address_line1);
    $("#shipping_address_line1").val(address_line1);
    $("#shipping_address_line2").val(address_line2);
    $("#shipping_address_line3").val(address_line3);
    $("#shipping_city").val(shipping_city);
    $("#shipping_state").val(shipping_state);
});
$(document).on("change","#home-address-dropdown",function(){
    var address_line1 = $(this).find(':selected').attr("data-address_line1");
    var address_line2 = $(this).find(':selected').attr("data-address_line2");
    var address_line3 = $(this).find(':selected').attr("data-address_line3");
    var shipping_city = $(this).find(':selected').attr("data-shipping_city");
    var shipping_state = $(this).find(':selected').attr("data-shipping_state");
    $("#home_address_line1").val(address_line1);
    $("#home_address_line2").val(address_line2);
    $("#home_address_line3").val(address_line3);
    $("#home_city").val(shipping_city);
    $("#home_state").val(shipping_state);
});
/*$(".book-now-btn").on('click', function(){
    var selected_user = $('.member-list-page .list-block.active').attr('data-id');
    console.log(selected_user);

    if(selected_user == undefined){
      Notiflix.Notify.Failure('Please select at least one member');
    }else{
      window.location.replace(BASE_URL+'review-confirm/'+selected_user);
    }

});*/


$("#pay-now").on('click', function(){
    var payment_card = $('.payment-card-list li.active').attr('data-id');
    var booking_id = $(this).attr('data-booking-id');
    var customer_id = $(this).attr('data-customer-id');
    var booking_reference_id = $(this).attr('data-booking-reference-id');

    if(payment_card == undefined){
      Notiflix.Notify.Failure('Please select payment card');
    }else{
        Notiflix.Loading.Standard();
         $.ajax({
                url: BASE_URL+'book',
                type: "POST",
                data:{
                  payment_card:payment_card,
                  booking_id:booking_id,
                  customer_id:customer_id,
                  booking_reference_id:booking_reference_id
                },
                success: function (returnData) {
                    returnData = $.parseJSON(returnData);
                    Notiflix.Loading.Remove();
                    console.log(returnData);
                    //return false;
                    if (typeof returnData != "undefined" && returnData.status == true)
                    {
                        $('#booking_reference_id').html('#'+returnData.booking_reference_id);
                        $('#booking-success-btn').attr('href', BASE_URL+'/my-booking/');
                        $('#pay-now-popup').modal('show');
                        Notiflix.Notify.Success('Success');

                    }else{
                        Notiflix.Notify.Failure(returnData.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                    Notiflix.Notify.Failure('Something went wrong');
                }
            }); 

    }

});

$(document).on('submit','form#add-card-form',function(event ){

    event.preventDefault();
    var name = $('#add-card-form #name').val();
    var card_number = $('#add-card-form #card_number').val();
    var expire = $('#add-card-form #expiry-month-year').val();
    var cvc_number = $('#add-card-form #cvc_number').val();
    
    Notiflix.Loading.Standard();
    $.ajax({
                url: BASE_URL+'add-payment-card',
                type: "POST",
                data:{
                  name: name,
                  card_number: card_number,
                  expire: expire,
                  cvc_number: cvc_number
                },
                success: function (returnData) {
                    returnData = $.parseJSON(returnData);
                    console.log(returnData);
                    Notiflix.Loading.Remove();
                    if (typeof returnData != "undefined" && returnData.status == true)
                    {
												$('form#add-card-form')[0].reset();
                        $('#add-card').modal('hide');
                        $('.payment-card-list li').removeClass('active');
                        var add_str = 
                        '<li class="col-lg-6 mb-4 active" data-id="'+returnData.card_id+'">'+
                          '<div class="pointer cards-list d-flex align-items-center bg-white border-r5 shadow-1 p-2 p-xl-3">'+
                            '<div class="mr-3">'+
                              '<img src="https://dev.coronatest.co.uk/assets/web/images/visa-card.png" alt="">'+
                            '</div>'+
                            '<div>'+
                              '<p class="t-black mb-0 font-16">**** **** **** '+returnData.last4+'</p>'+
                              '<p class="t-grey mb-0 font-13">Expires '+expire+'</p>'+
                            '</div>'+
                          '</div>'+
                        '</li>';
                        $( add_str ).insertAfter( "#add-new-card-div" );
                        Notiflix.Notify.Success('Card added');

                    }else{
                        Notiflix.Notify.Failure(returnData.message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('error');
                    Notiflix.Notify.Failure('Something went wrong');
                }
            }); 


});


$('#pay-now-popup').on('hidden.bs.modal', function () {
    var redirect_url = $('#pay-now-popup #booking-success-btn').attr('href');
    window.location = redirect_url;
})

// ------------------------Maulik Code --------------------//
$(".fit_to_fly_datepicker").datepicker({
  format: 'dd-mm-yyyy',
  autoclose: true,
  startDate: new Date(),
}).on("changeDate",function(ev){
  var selected_return_date = $(this).val();
  var returnDate = ev.date;
  var depature_date = get_full_date($(".depature_date").val());
  var earlier_return_date = $("#earlier_return_date").val();
  var date1 = new Date(returnDate);
  var date2 = new Date(depature_date);
  if(returnDate != ""){

    if(date1 <= date2){
        console.log("IF");
        Notiflix.Notify.Failure("You can not select return date before departure date");
        $('.fit_to_fly_datepicker').datepicker('clearDates');
        return false;
    }
    else if(date1 >= date2 && earlier_return_date != selected_return_date){
        console.log("ELSE IF");
        Notiflix.Notify.Failure("Please select same return date which you choosen earlier");
        $(".fit_to_fly_datepicker").val("");
        return false;
    }
  }
});

$(".fit_to_fly_shipping_date").datepicker({
  format: 'dd-mm-yyyy',
  autoclose: true,
  startDate: new Date(),
});


function get_full_date(d){ 
var split_date = d.split('-');
var split_year = split_date[2].split(" ");
var date = split_date[0];
var month = split_date[1];
var year = split_year[0];
return year+'/'+month+'/'+date;
}

$(document).on("change",'input[name="is_visiting_country"]',function(){
  var value = $(this).val();
  if(value == "yes"){
    $(".visiting_country").show();
    $("#visiting_country").prop('required',true);
  }else{
    $(".visiting_country").hide();
    $("#visiting_country").removeAttr("required");
  }
});
$(document).on("change",'input[name="shipping_method"]',function(){
  var value = $(this).val();
  if(value == "2"){
    $(".shipping_detail").show();
    $("#shipping_date").prop('required',true);
    $(".shippingaddress").prop('required',true);
  }else{
    $(".shipping_detail").hide();
    $("#shipping_date").prop('required',false);
    $(".shippingaddress").prop('required',false);
  }
});
$(document).on("change",'input[name="transport_type"]',function(){
  var value = $(this).val();
  if(value == "1"){
    $(".transport_type_label").html("Enter Flight No");
    $("#transport_number").attr("placeholder","Enter Flight No");
    $("#transport_number").attr("data-parsley-required-message","Please Enter Flight No");
  }else if(value == "2"){
    $(".transport_type_label").html("Enter Vessel No");
    $("#transport_number").attr("placeholder","Enter Vessel No");
    $("#transport_number").attr("data-parsley-required-message","Please Enter Vessel No");
  }else{
    $(".transport_type_label").html("Enter Train No");
    $("#transport_number").attr("placeholder","Enter Train No");
    $("#transport_number").attr("data-parsley-required-message","Please Enter Train No");
  }
});

$(document).on("click",".edit_member",function(){
var selected_flag = $(".selected_flag").val();
var user_id = $(this).data("id");
var booking_id = $(this).data("booking_id");
if(selected_flag == 1){
  $(".other_member_id").val(user_id);
  $("#confirm_member_modal").modal("show");
}else{
  window.location.href = BASE_URL+"booking-edit-member/"+user_id+"/"+booking_id;
}
});
$(document).on("click",".add_new_member",function(){
var selected_flag = $(".selected_flag").val();
var user_id = '';
if(selected_flag == 1){
  $(".other_member_id").val(user_id);
  $("#confirm_member_modal").modal("show");
}else{
  window.location.href = BASE_URL+"members/add";
}
});

$(document).on("click",".select_other_member",function(){
var href = $(this).data("href");
var user_id =   $(".other_member_id").val();
window.location.href = href+"/"+user_id;
})

$(document).on("change","#return_country,#visiting_country",function(){

var csrf_token = $("#csrf_token").val();
var purpose_id = $('input[name="purpose_id"]').val();
var country_id = $(this).val();
if(purpose_id != '' && country_id != ''){
  $.ajax({
    url: BASE_URL+'get-report-test',
    type: "POST",
    data:{
      purpose_id:purpose_id,
      csrf_token: csrf_token,
      country_id:country_id,
      id:country_id,
      step:2,
    },
    success: function (returnData) {
        returnData = $.parseJSON(returnData);
        var html = '';
        $.each(returnData  ,function(key,value){
            var checked = '';
            if(key == 0){
              checked = "";
            }
            html += '<li class="mr-3"><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'">';
            html += '<label for="test_type_'+value.id+'" class="d-flex align-items-center">'+value.name+'</label></li>'
        });
        $(".fit_to_fly_test_type_list").html(html);
    },
    error: function (xhr, ajaxOptions, thrownError) {
        console.log('error');
        Notiflix.Notify.Failure('Something went wrong');
    }
  }); 
}
});


function previewImage(input,isBackground=false) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      if(isBackground){
        $('#imagePreview').css('background-image', 'url('+e.target.result+')');
      }else{
          $('#imagePreview').attr('src', e.target.result);
      }
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#imageUpload").change(function() {
  previewImage(this,true);
});

window.Parsley.on('field:error', function() {
  // This global callback will be called for any field that fails validation.
  //console.log('Validation failed for: ', this.$element);
  $(".fit_to_fly_collapse").addClass("show");
});