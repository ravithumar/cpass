
// onscroll  search box close js
$(window).on('scroll', function(e){ 
    $(".search-field1").removeClass('form-active'); 
});

var purpose_id = $('input[name="purpose_id"]:checked').val();
//getReportTest(purpose_id);
$("#new-booking-radio input[name=purpose_id]:radio").click(function(ev) {

  if (ev.currentTarget.value == "1") {
    $('#domestic1').parent().removeClass('select-radios');
    $('#domestic2').parent().addClass('select-radios');
    $('.domestic-radio-section').addClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('.domestic-radio-section').removeClass('add-international');
    $('.search-btn-div').hide();
  } 
  else if (ev.currentTarget.value == "2") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide(); 
  }
  else if (ev.currentTarget.value == "5") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-international');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').show();
    $('#select-destination-country').hide();
    $('#test-type-booking').hide();
    $('#search-btn-div').show();
    $('.proceed-to-book-div').hide();

  }
  else if (ev.currentTarget.value == "3") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide(); 

  }
    else if (ev.currentTarget.value == "4") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('#internatonal-arrival-booking').hide();

    $('#select-destination-country').show();
    $('#type-of-delivery').hide();
    $('#departure-date-time-selection').show();
    $('#date-time-selection').show();
  }
  getReportTest1(ev.currentTarget.value);
});

$("#internatonal-arrival-booking input[name=is_visiting_country]:radio").click(function(ev) { 
  if (ev.currentTarget.value == "1") { 
    $('#transiting_country').show();
  } 
  else if (ev.currentTarget.value == "2") {
    $('#transiting_country').hide(); 
  } 
});
$('#internatonal-arrival-booking #dest_country').on('change', function (ev) {
  var purpose_id = $('input[name=purpose_id]:checked').val();
  if(purpose_id == '5'){

    var ukresident = $("#passenger-status-radio input[name=status_radio1]:checked").val();
    // console.log(purpose_id);
    if (ukresident == "1") {
      var res_add = $('.residency_address').find(":selected").val();
      if (res_add == undefined) { 
        Notiflix.Notify.Failure('Please select home address.');
        return false;
      }
    }else{
      var nonukaddress1 = $('.nonukaddress1').val();
      var nonukaddress2 = $('.nonukaddress2').val();
      var nonukcity = $('.nonukcity').val();
      var nonresidency_postcode = $('.nonresidency_postcode').val();
      var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
      
      // console.log(res_add);
      if (nonukaddress1 == "") { 
        Notiflix.Notify.Failure('Please add address line 1.');
        return false;
      }
      if (nonukaddress2 == "") {
        Notiflix.Notify.Failure('Please add address line 2.');
        return false;
      }
      if (nonukcity == "") {
        Notiflix.Notify.Failure('Please add home city.');
        return false;
      }
      if (nonresidency_postcode == "") {
        Notiflix.Notify.Failure('Please add home postcode.');
        return false;
      }
      if (nonukcountry == "") {
        Notiflix.Notify.Failure('Please select home country.');
        return false;
      }
    }
  
  }
  
});

$("#internatonal-arrival-booking input[name=is_excempted]:radio").click(function(ev) {
  if (ev.currentTarget.value == "1") {
    $('#exemption_country').show();
  } else if (ev.currentTarget.value == "2") {
    $('#exemption_country').hide(); 
  }
});
$("#internatonal-arrival-booking input[name=is_medical_excempted]:radio").click(function(ev) {
  if (ev.currentTarget.value == "yes") {
    $('.international_proof_document').show();
  } else if (ev.currentTarget.value == "no") {
    $('.international_proof_document').hide(); 
  }
});
$("#internatonal-arrival-booking input[name=shipping_method]:radio").click(function(ev) {
  if (ev.currentTarget.value == "1") {
    $('.uk-shipping-div').hide();
    $('.non-uk-shipping-address').hide();
    $('.pickup-date-center-div').show(); 
    
    $('.pickup-search-center-div').show();
  } else if (ev.currentTarget.value == "2") {
    $('.pickup-date-center-div').hide(); 
    $('.uk-shipping-div').show();
    $('.uk-shipaddress').hide();
    $('.pickup-search-center-div').hide();
    $('.non-uk-shipping-address').show();
    var residency_flag = $(".non-uk-shipping-address  input[name=residency_flag]:checked").val();
    if(residency_flag == "2"){
      $('.uk-shipaddress').show();
    }else{
      $('.uk-shipaddress').hide();
    }
  }
  $('.proceed-to-book-div').hide();
  $('#search-btn-div').show();

});

$("#internatonal-arrival-booking input[name=residency_flag]:radio").click(function(ev) {
  if (ev.currentTarget.value == "1") {
    $('.uk-shipaddress').hide();
  } else if (ev.currentTarget.value == "2") {
    $('.uk-shipaddress').show();
  }

});
$("#internatonal-arrival-booking input[name=vaccine_status]:radio").click(function(ev) {
  var csrf_token = $("#csrf_token").val();
  var vaccination_status = $(this).attr('data-val');

  var purpose_id = $('input[name=purpose_id]:checked').val();
  var is_visiting_country = $('input[name=is_visiting_country]:checked').val();

  if (is_visiting_country == "1") {
    var country_id = $('#visiting_country').find(":selected").val();
    if (country_id =="") {
      Notiflix.Notify.Failure('Please select visiting country.');
      return false;
    }
  }else{
    var country_id = $('#dest_country').find(":selected").val();
  }
  var is_excempted = $('input[name=is_excempted]:checked').val();

  if (is_excempted =="1") {
    var excempted_cat = $('#excempted_cat').find(":selected").val();
    if (excempted_cat == "") {
      Notiflix.Notify.Failure('Please select exemption category.');
      return false;
    }
  }else{
    var excempted_cat = '';
  }

// ------------------validation for top dropdown validation START -------------------- //

  var purpose_id    = $('input[name=purpose_id]:checked').val();
  var ukresident    = $("#passenger-status-radio input[name=status_radio1]:checked").val();
  var dest_country  = $('#internatonal-arrival-booking #dest_country').find(":selected").val();
  var is_medical_excempted    = $('input[name=is_medical_excempted]:checked').val();
    
  if (ukresident == "1") {
    var res_add = $('.residency_address').find(":selected").val();
    // console.log(res_add);
    if (res_add == undefined) { 
      Notiflix.Notify.Failure('Please select home address.');
      return false;
    }
  }else{
    var nonukaddress1 = $('.nonukaddress1').val();
    var nonukaddress2 = $('.nonukaddress2').val();
    var nonukcity = $('.nonukcity').val();
    var nonresidency_postcode = $('.nonresidency_postcode').val();
    var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
    // console.log(res_add);
    if (nonukaddress1 == "") { 
      Notiflix.Notify.Failure('Please add address line 1.');
      return false;
    }
    if (nonukaddress2 == "") {
      Notiflix.Notify.Failure('Please add address line 2.');
      return false;
    }
    if (nonukcity == "") {
      Notiflix.Notify.Failure('Please add home city.');
      return false;
    }
    if (nonresidency_postcode == "") {
      Notiflix.Notify.Failure('Please add home postcode.');
      return false;
    }
    if (nonukcountry == "") {
      Notiflix.Notify.Failure('Please select home country.');
      return false;
    }
  }
  if (dest_country == "") {
    Notiflix.Notify.Failure('Please select arriving country.');
    return false;
  }
	var vaccine_id = "";
	if(vaccination_status == "3"){
		//$(".international_vaccine_list_div input[name='vaccine_id']").prop("checked",false);
    $('.international_vaccine_list_div #vaccine_id').val("").change();
		$(".international_vaccine_list_div").hide();
	}else{
      vaccine_id = $('.international_vaccine_list_div #vaccine_id').find(":selected").val();
			//vaccine_id = $(".international_vaccine_list_div input[name='vaccine_id']:checked").val();
			$(".international_vaccine_list_div").show();
	}
	 
// ------------------validation for top dropdown validation END -------------------- //
  $.ajax({
    url: BASE_URL + "get-report-test1",
    method: "POST",
    data: {
        csrf_token: csrf_token,
        purpose_id: purpose_id,
        country_id: country_id,
        vaccination_status: vaccination_status,
        vaccine_id: vaccine_id,
        is_medical_excempted: is_medical_excempted,
        excempted_cat: excempted_cat
    },
    dataType: "json",
    success: function(data) {
        // console.log(data);
        var html = '';
        $.each(data,function(key,value){
            var checked = '';
            if(key == 0){
              checked = "";
            }
            html += '<li><input type="radio" id="int_test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="int_test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
        });
        $("#internatonal-arrival-booking .test_type_list1").html(html);
    }
  });

});

$("#internatonal-arrival-booking select[name=vaccine_id]").change(function(ev) {
  var csrf_token = $("#csrf_token").val();
	var vaccine_id = $(this).val();
	var purpose_id    = $('input[name=purpose_id]:checked').val();
  var is_medical_excempted    = $('input[name=is_medical_excempted]:checked').val();
	var is_visiting_country = $('input[name=is_visiting_country]:checked').val();

  if (is_visiting_country == "1") {
    var country_id = $('#visiting_country').find(":selected").val();
  }else{
    var country_id = $('#dest_country').find(":selected").val();
  }
  var is_excempted = $('input[name=is_excempted]:checked').val();
	if (is_excempted =="1") {
    var excempted_cat = $('#excempted_cat').find(":selected").val();
  }else{
    var excempted_cat = '';
  }
	var vaccination_status = $('#internatonal-arrival-booking input[name=vaccine_status]:checked').attr("data-val");
	 $.ajax({
      url: BASE_URL + "get-report-test1",
      method: "POST",
      data: {
          csrf_token: csrf_token,
          purpose_id: purpose_id,
          country_id: country_id,
          vaccination_status: vaccination_status,
          vaccine_id: vaccine_id,
          is_medical_excempted: is_medical_excempted,
          excempted_cat: excempted_cat
      },
      dataType: "json",
      success: function(data) {
          // console.log(data);
          var html = '';
          $.each(data,function(key,value){
              var checked = '';
              if(key == 0){
                checked = "";
              }
              html += '<li><input type="radio" id="int_test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="int_test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
          });
          $("#internatonal-arrival-booking .test_type_list1").html(html);
      }
    });
});

$("#internatonal-arrival-booking input[name=is_medical_excempted]").change(function(ev) {
  var csrf_token = $("#csrf_token").val();
  var vaccine_id = $('.international_vaccine_list_div #vaccine_id').find(":selected").val();
  var purpose_id    = $('input[name=purpose_id]:checked').val();
  var is_medical_excempted    = $(this).val();
  var is_visiting_country = $('input[name=is_visiting_country]:checked').val();

  if (is_visiting_country == "1") {
    var country_id = $('#visiting_country').find(":selected").val();
  }else{
    var country_id = $('#dest_country').find(":selected").val();
  }
  var is_excempted = $('input[name=is_excempted]:checked').val();
  if (is_excempted =="1") {
    var excempted_cat = $('#excempted_cat').find(":selected").val();
  }else{
    var excempted_cat = '';
  }
  var vaccination_status = $('#internatonal-arrival-booking input[name=vaccine_status]:checked').attr("data-val");
   $.ajax({
      url: BASE_URL + "get-report-test1",
      method: "POST",
      data: {
          csrf_token: csrf_token,
          purpose_id: purpose_id,
          country_id: country_id,
          vaccination_status: vaccination_status,
          vaccine_id: vaccine_id,
          is_medical_excempted: is_medical_excempted,
          excempted_cat: excempted_cat
      },
      dataType: "json",
      success: function(data) {
          // console.log(data);
          var html = '';
          $.each(data,function(key,value){
              var checked = '';
              if(key == 0){
                checked = "";
              }
              html += '<li><input type="radio" id="int_test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="int_test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
          });
          $("#internatonal-arrival-booking .test_type_list1").html(html);
      }
    });
});

function getReportTest1(purpose_id, country_id = '',vaccination_status = '',excempted_cat = '',vaccine_id = ''){

  var csrf_token = $("#csrf_token").val();
  $.ajax({
    url: BASE_URL + "get-report-test1",
    method: "POST",
    data: {
        csrf_token: csrf_token,
        purpose_id: purpose_id,
        country_id: country_id,
        vaccination_status: vaccination_status,
        excempted_cat: excempted_cat,
        vaccine_id: vaccine_id
    },
      dataType: "json",
      success: function(data) {
          // console.log(data);
          var html = '';
          $.each(data,function(key,value){
              var checked = '';
              if(key == 0){
                checked = "";
              }
              html += '<li><input type="radio" id="test_type_'+value.id+'" value="'+value.id+'" name="test_type" '+checked+' data-test-type-name="'+value.name+'" data-test-type-price="'+value.price+'"><label for="test_type_'+value.id+'" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">'+value.name+'</p></label></li>'
          });
          $(".test_type_list1").html(html);
      }
  });
}
$('.uk_isolation_postcode').click(function(){
  var postcode = $('#uk_isolation_postcode').val();
  if(postcode.trim() != ''){
    Notiflix.Loading.Standard();
    $.ajax({
            url: BASE_URL+'get-addresses-by-postcode',
            type: "POST",
            data:{
              postcode:postcode
            },
            success: function (returnData) {
               Notiflix.Loading.Remove();
                returnData = $.parseJSON(returnData);
                console.log(returnData);
                if (typeof returnData != "undefined" && returnData.status == true)
                {
                    $.each(returnData.addresses, function (key, val) {
                      var full_address = val.formatted_address[0]+', '+val.formatted_address[1]+', '+val.formatted_address[2]+', '+val.formatted_address[3];
                          
                      $('#uk_isolation_address').append($("<option></option>")
                            .attr("value", full_address)
                            .text(full_address)); 
                    });
                    Notiflix.Notify.Success('Please select your address');

                }else{
                    Notiflix.Notify.Failure('Please enter valid postcode');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log('error in saving channel');
                Notiflix.Notify.Failure('Something went wrong');
            }
        });   
  }


});
$('#search-inter-hospital-btn').click(function(e){
// alert('here');
  var test_date = $('#booking_date').val();
  var test_time = $('#booking_time').val();

  var departure_date = $('#departure_date').val();
  var departure_time = $('#departure_time').val();
  var country_id = $('#country_id').val();

  // var postcode = $('#txtpostcode').val();
  var postcode = $('#hospitalpostcode').val();

  var purpose_id    = $('input[name=purpose_id]:checked').val();
  var ukresident    = $("#passenger-status-radio input[name=status_radio1]:checked").val();
  var shipping_method    = $("input[name=shipping_method]:checked").val();
  var dest_country  = $('#internatonal-arrival-booking #dest_country').find(":selected").val();
  var residency_flag    = $("input[name=residency_flag]:checked").val();
  var resshipping_address ="";
  
  if (residency_flag == undefined) {
    residency_flag ="";
  }else if (residency_flag == "1") {
    var resshipping_address = $('.resshipping_address').find(":selected").val();
  }else{
    // var resshipping_address ="";
  }

  var return_date = $('#return_date').val();
  var return_time = $('#return_time').val();
  var pickup_date = $('.pickup_date').val();
  var ukshipping_date = $('.ukshipping_date1').val();

  var ukshipping_postcode = $('.uk_isolation_postcode').val();
   
  if (ukresident == "1") {
    var res_add = $('.residency_address').find(":selected").val();
    // console.log(res_add);
    if (res_add == undefined) { 
      Notiflix.Notify.Failure('Please select home address.');
      return false;
    }
    var nonukaddress1 = "";
    var nonukaddress2 = "";
    var nonukcity = "";
    var nonresidency_postcode = "";
    var nonukcountry = "";
  }else{
    var res_add = "";
    var nonukaddress1 = $('.nonukaddress1').val();
    var nonukaddress2 = $('.nonukaddress2').val();
    var nonukcity = $('.nonukcity').val();
    var nonresidency_postcode = $('.nonresidency_postcode').val();
    var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
    // console.log(res_add);
    if (nonukaddress1 == "") { 
      Notiflix.Notify.Failure('Please add address line 1.');
      return false;
    }
    if (nonukaddress2 == "") {
      Notiflix.Notify.Failure('Please add address line 2.');
      return false;
    }
    if (nonukcity == "") {
      Notiflix.Notify.Failure('Please add home city.');
      return false;
    }
    if (nonresidency_postcode == "") {
      Notiflix.Notify.Failure('Please add home postcode.');
      return false;
    }
    if (nonukcountry == "") {
      Notiflix.Notify.Failure('Please select home country.');
      return false;
    }
  }

  var is_visiting_country = $('input[name=is_visiting_country]:checked').val();
  var is_excempted = $('input[name=is_excempted]:checked').val();
  var vaccination_status = $("input[name=vaccine_status]:checked").val();
  if (vaccination_status == undefined) {
      Notiflix.Notify.Failure('Please select vaccination status.');
      return false; 
  }

  if (is_visiting_country == "1") {
    var visiting_country = $('#visiting_country').find(":selected").val();
    if (visiting_country =="") {
      Notiflix.Notify.Failure('Please select visiting country.');
      return false;
    }else{
      visiting_country = visiting_country;
    }
  }else{
      visiting_country = "";
  }
  if (is_excempted =="1") {
    var excempted_cat = $('#excempted_cat').find(":selected").val();
    if (excempted_cat == "") {
      Notiflix.Notify.Failure('Please select exemption category.');
      return false;
    }
  }else{
    excempted_cat ="";
  }

  if(shipping_method == '1'){
    if(pickup_date == ''){
      Notiflix.Notify.Failure('Please select pickup date');
      return false;
    }
  }else{
    pickup_date ="";
  }

	// var vaccine_date = $("#vaccine_date").val();
	// if(vaccine_date == ""){
	// 	Notiflix.Notify.Failure('Please select vaccine date.');
 //      return false; 
	// }

  //Added by Nayan : 15-11-2021
  if(vaccination_status == 'Fully Vaccinated'){

      var dose1Date = $("#vaccine_date1").val();
      var dose2Date = $("#vaccine_date2").val();
      var doserequired = $("#dose_required").val();
      if(dose1Date == "")
      {

        Notiflix.Notify.Failure('Dose-1 date is required');
        return false;
      }
      else if(dose2Date == "" && doserequired == 2)
      {
        Notiflix.Notify.Failure('Dose-2 date is required');
        return false;
      }  
      else if(dose1Date == "" && dose2Date == "")
      {

      Notiflix.Notify.Failure('Dose-1 & Dose-2 dates are required');
      return false;
      }  
        if(dose1Date != ''){
          var date1 = new Date(dose1Date);
          var date2 = new Date(dose2Date);
          if(date1 > date2){
            Notiflix.Notify.Failure("You can not select dose-2 date before dose-1 date");
            $('#vaccine_date1').datepicker('clearDates');
            $('#vaccine_date2').datepicker('clearDates');
            return false;
          }
        }
    }

  if(vaccination_status == 'Partially Vaccinated'){

      var dose1Date = $("#vaccine_date1").val();
      if(dose1Date == ""){
        
        Notiflix.Notify.Failure('Dose-1 date is required');
      return false;
      }
    }  
  //End Added by Nayan : 15-11-2021

	var date_of_birth = $("#date_of_birth").val();
	if(date_of_birth == ""){
		Notiflix.Notify.Failure('Please select date of birth.');
      return false; 
	}
	var is_medical_excempted = $('input[name=is_medical_excempted]:checked').val();
	var vaccine_id = "";
	if(vaccination_status != "Not Vaccinated"){
		vaccine_id = $('#vaccine_id').find(":selected").val();
		if (vaccine_id == "") {
      Notiflix.Notify.Failure('Please select vaccine type.');
      return false; 
  	}
	} 

	var proof_document = "";
	if (is_medical_excempted =="yes") {
    if( document.getElementById("proof_document").files.length == 0 ){
			Notiflix.Notify.Failure('Please select proof document.');
      return false; 
		}else{
			var files = $('#proof_document')[0].files;
      console.log(files);
			proof_document = files[0];
		}
  }
  if(shipping_method == "2"){
    var shipping_date = $('#internatonal-arrival-booking input[name="shipping_date"]').val();
    if(shipping_date == ""){
      Notiflix.Notify.Failure('Please select shipping date.');
      return false; 
    }
  }

  if($('input[type=radio][name=test_type]:checked').val() == undefined){
      Notiflix.Notify.Failure('Test type is required');
      return false;
  }
  /*var form = new FormData();
  form.append('proof_document',proof_document);
	console.log(proof_document);*/

  if (dest_country == "") {
    Notiflix.Notify.Failure('Please select arriving country.');
    return false;
  }else if(return_date == ''){
    Notiflix.Notify.Failure('Please enter return date.');
    return false;
  }else if(return_time == ''){
    Notiflix.Notify.Failure('Please enter return time.');
    return false;
  }else if(postcode == '' && $("#post-code-tab").hasClass("active") && shipping_method == "1"){
    Notiflix.Notify.Failure('Postcode is required');
    return false;
  }else if($('.inter_regionId').val() == undefined && $("#country-tab").hasClass("active")){
    Notiflix.Notify.Failure('Region is required');
    return false;
  }else{
    // postcode = postcode.replace(/ /g,'');
    var ukresident = $("#passenger-status-radio input[name=status_radio1]:checked").val();
    var residency_postcode = $('.residency_postcode').val();
    var residency_address = $('.residency_address').val();
    var nonresidency_postcode = $('.nonresidency_postcode').val();
    var nonukaddress1 = $('.nonukaddress1').val();
    var ukshipping_date = $('.ukshipping_date1').val();
    var nonukaddress2 = $('.nonukaddress2').val();
    var nonukcity = $('.nonukcity').val();
    var nonukcountry = $('input[name=nonukcountry]').find(":selected").val();
    var dest_country  = $('#internatonal-arrival-booking #dest_country').find(":selected").val();
    var vaccination_status = $("input[name=vaccine_status]:checked").val();
    var report_id = $('input[type=radio][name=test_type]:checked').val();
    var purpose_id = $('input[type=radio][name=purpose_id]:checked').val();
    var place_of_test_name = $('input[type=radio][name=place_test]:checked').attr('data-place-test-name');
    var purpose_of_test_name = $('input[type=radio][name=purpose_id]:checked').attr('data-purpose-type-name');
    var test_type_name = $('input[type=radio][name=test_type]:checked').attr('data-test-type-name');
    var test_price = $('input[type=radio][name=test_type]:checked').attr('data-test-type-price');
    var csrf_token = $("#csrf_token").val();
    var formData = new FormData();
     formData.append('resshipping_address', resshipping_address);
     formData.append('ukshipping_postcode', ukshipping_postcode);
     formData.append('residency_flag', residency_flag);
     formData.append('shipping_method', shipping_method);
     formData.append('return_date', return_date);
     formData.append('return_time', return_time);
     formData.append('is_excempted', is_excempted);
     formData.append('excempted_cat', excempted_cat);
     formData.append('shipping_date', ukshipping_date);
     formData.append('pickup_date', pickup_date);
     formData.append('res_add', res_add);
     formData.append('is_visiting_country', is_visiting_country);
     formData.append('visiting_country', visiting_country);
     formData.append('ukresident', ukresident);
     formData.append('residency_postcode', residency_postcode);
     formData.append('residency_address', residency_address);
     formData.append('nonresidency_postcode', nonresidency_postcode);
     formData.append('nonukaddress1', nonukaddress1);
     formData.append('nonukaddress2', nonukaddress2);
     formData.append('nonukcity', nonukcity);
     formData.append('nonukcountry', nonukcountry);
     formData.append('dest_country', dest_country);
     formData.append('vaccination_status', vaccination_status);
     formData.append('postcode', postcode);
     formData.append('report_id', report_id);
     formData.append('purpose_id', purpose_id);
     formData.append('country_id', country_id);
     formData.append('csrf_token', csrf_token);
     formData.append('purpose_of_test_name', purpose_of_test_name);
     formData.append('test_type_name', test_type_name);
     formData.append('test_price', test_price);
     formData.append('dose_1_date', dose1Date);
     formData.append('dose_2_date', dose2Date);
     formData.append('date_of_birth', date_of_birth);
     formData.append('is_medical_excempted', is_medical_excempted);
     formData.append('vaccine_id', vaccine_id);
     formData.append('proof_document', proof_document);

      $.ajax({
          url: BASE_URL+'save_booking_session',
          type: "POST",
          cache: false,
					contentType: false,
					processData: false,
          dataType: 'json',
          data:formData,
          /*data:{
            resshipping_address:resshipping_address,
            ukshipping_postcode:ukshipping_postcode,
            residency_flag:residency_flag,
            shipping_method:shipping_method,
            return_date:return_date,
            return_time:return_time,
            is_excempted:is_excempted,
            excempted_cat:excempted_cat,
            shipping_date:ukshipping_date,
            pickup_date:pickup_date,
            res_add:res_add,
            is_visiting_country:is_visiting_country, 
            visiting_country:visiting_country,
            ukresident:ukresident,
            residency_postcode:residency_postcode,
            residency_address:residency_address,
            nonresidency_postcode:nonresidency_postcode,
            nonukaddress1:nonukaddress1,
            nonukaddress2:nonukaddress2,
            nonukcity:nonukcity,
            nonukcountry:nonukcountry,
            dest_country:dest_country,
            vaccination_status:vaccination_status,
            postcode:postcode,
            report_id:report_id,
            purpose_id:purpose_id,
            country_id:country_id,
            csrf_token:csrf_token,
            //place_of_test_name:place_of_test_name,
            purpose_of_test_name:purpose_of_test_name,
            test_type_name:test_type_name,
            test_price:test_price,
            vaccine_date:vaccine_date,
            date_of_birth:date_of_birth,
            is_medical_excempted:is_medical_excempted,
            vaccine_id:vaccine_id,
            proof_document:proof_document,
          },*/
          success: function (returnData) {
              if(shipping_method == "1"){
  							if($("#post-code-tab").hasClass("active")){
                		window.location = BASE_URL+'search?postcode='+postcode;
  							}else{
  								var regionId = $('.inter_regionId option:selected').val();
  								window.location = BASE_URL+'search?regionId='+regionId;
  							}
              }else{
                var purpose_of_test = $("input[type='radio'][name='purpose_id']:checked").attr('data-purpose-type-name');
                $('#international-booking-information-popup #purpose-of-test').html(purpose_of_test);
                $('#international-booking-information-popup #purpose-of-test').parent().show();

                $('#international-booking-information-popup #test-type').html(test_type_name);
                $('#international-booking-information-popup #test-type').parent().show();

                /*$('#international-booking-information-popup #place-of-test').html("Ship to UK Address");
                $('#international-booking-information-popup #place-of-test').parent().show();*/

                $('#international-booking-information-popup #date').html(ukshipping_date);
                $('#international-booking-information-popup #date').parent().show();


                $('#international-booking-information-popup').modal('show');
                return false;
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              console.log('error');
              Notiflix.Notify.Failure('Something went wrong');
          }
      });  

  }
  
});
