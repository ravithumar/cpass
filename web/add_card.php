<?php  include("header.php"); ?>

<section class="add-card py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Add Card</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="" method="get" accept-charset="utf-8">
						<div class="row align-items-end justify-content-center">
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Card holder name</label>
									<input type="text" name="" value="" placeholder="John Smith" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Card Number</label>
									<input type="text" name="" value="" placeholder="1234 4564 4566 1454" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">Expires</label>
									<input type="text" name="" value="" placeholder="12/2030" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-5 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-black font-weight-bold font-14 mb-0">CVV</label>
									<input type="password" name="" value="" placeholder="****" class="form-control">
								</div>
							</div>
						</div>
						<div class="text-center mt-5">
							<a href="payment.php" title="" class="btn-green-lg m-2">Add Card</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>