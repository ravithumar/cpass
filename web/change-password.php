<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>C - Pass</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/style-2.css">
	<link rel="stylesheet" href="css/responsive.css">
	<!-- <link rel="stylesheet" href="css/slick.css"> -->
</head>

<body>
	<section class="change-password"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	  <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">
			     <div class="back-close">
		 		   	<a href="sign-in.php" class="back position-absolute">
                      <img src="images/back.svg" alt="Back Arrow" class="img-fluid">
			        </a>			   
	 		     </div>
	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="images/c-pass-large-logo.png" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">	 		  	 			
			    <div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
			    	<h2 class="font-34 t-bluetwo font-weight-bold mb-4">Change Password</h2>			    	
  				    <div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">Current Password</label>
    					<input type="password" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter Current password">
    					<a href="#" class="icon position-absolute">
    						<img src="images/eyes.svg" alt="Eyes" class="img-fluid">
    					</a>
  				    </div>
  				    <div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">New Password</label>
    					<input type="password" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter New password">
    					<a href="#" class="icon position-absolute">
    						<img src="images/eyes.svg" alt="Eyes" class="img-fluid">
    					</a>
  				    </div>
  				     <div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">Confirm Password</label>
    					<input type="password" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter Confirm password">
    					<a href="#" class="icon position-absolute">
    						<img src="images/eyes.svg" alt="Eyes" class="img-fluid">
    					</a>
  				    </div>
  				    <div class="btn-wrap mt-4 mt-md-5 mb-3">
						<a href="#" class="btn-green-lg d-block border-r8 font600">Save</a>
  				    </div>
  				    
  				    
			    </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/popper.min.js"></script>
<!-- <script src="js/slick.min.js"></script> -->
<script src="js/custom.js"></script>
</body>


</html>