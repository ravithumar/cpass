<?php  include("header.php"); ?>

<section class="clinical-registration py-4 mb-5">
	<div class="container-outer bg-grey mx-auto border-r20">
	   <div class="container py-5">
	   	<div class="row">
	   		<div class="col-md-12">
	   			<div class="row mb-3">
					<div class="col">
						<h3 class="t-violet font-26 font600 mb-3">Company Representative</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="form-group mb-4">
    						<label for="exampleInputFullName" class="t-light-blue mb-0 font600 font-13">Full Name</label>
    						<input type="text" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter Full Name">
  						</div>
  												
					</div>
					<div class="col-sm-6 col-md-4">
							<div class="form-group mb-4">
    							<label for="exampleInputJobTitle" class="t-light-blue mb-0 font600 font-13">JobTitle</label>
    							<input type="text" class="form-control font-14" id="exampleInputJobTitle" aria-describedby="jobtitle" placeholder="Enter JobTitle">
  							</div>
					</div>
					<div class="col-md-4"></div>
				</div>		
	   		</div>
	   	</div>
	   	<div class="row mt-3 mt-md-4">
	   		<div class="col-md-12">
	   			<div class="row mb-3">
					<div class="col">
						<h3 class="t-violet font-26 font600 mb-3">Company Information</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="form-group mb-4">
    						<label for="exampleInputCompanyName" class="t-light-blue mb-0 font600 font-13">Company Name</label>
    						<input type="text" class="form-control font-14" id="exampleInputCompanyName" aria-describedby="companyname" placeholder="Enter Company Name">
  						</div>
  												
					</div>
					<div class="col-sm-6 col-md-4">
						 <div class="form-group mb-4">
    							<label for="exampleInputCompanyNumber" class="t-light-blue mb-0 font600 font-13">Company Number</label>
    							<input type="text" class="form-control font-14" id="exampleInputCompanyNumber" aria-describedby="company number" placeholder="Enter Company Number">
  						 </div>
					</div>
					<div class="col-sm-6 col-md-4">
						 <div class="form-group mb-4">
    							<label for="exampleInputVATNumber" class="t-light-blue mb-0 font600 font-13">VAT Number</label>
    							<input type="text" class="form-control font-14" id="exampleInputVATNumber" aria-describedby="vat number" placeholder="Enter VAT Number">
  						 </div>
					</div>
					<div class="col-sm-6 col-md-4">
						 <div class="form-group mb-4">
    							<label for="exampleInputCompanyWebsite" class="t-light-blue mb-0 font600 font-13">Company Website</label>
    							<input type="text" class="form-control font-14" id="exampleInputCompanyWebsite" aria-describedby="company website"placeholder="Enter Company Website">
  						 </div>
					</div>
					<div class="col-sm-6 col-md-4">
						 <div class="form-group position-relative">
						    <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">Number of Employess</label>
						    <div class="select position-relative">
							  <select class="form-control font-14" id="exampleFormControlSelect1">
						       <option>Please Select</option>
						       <option>2</option>
						       <option>3</option>
						       <option>4</option>
						       <option>5</option>
						    </select>
						    </div>
						    						    
  						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="form-group">
						    <label for="exampleInputEmail1" class="t-light-blue mb-0 font600 font-13">Email</label>
						    <input type="email" class="form-control font-14" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email Id">    
  						</div>
					</div>
				</div>
				<div class="row">
					 <div class="col-md-6">
						<div class="form-group mb-4">
    						<label for="exampleInputCompanyName" class="t-light-blue mb-0 font600 font-13">Direct Telephone Number for Our Point of Contact</label>
    						<input type="text" class="form-control font-14" id="exampleInputCompanyName" aria-describedby="companyname" placeholder="Enter Company Website">
  						</div>
					 </div>
					 <div class="col-md-6">
						<div class="form-group mb-4">
    						<label for="exampleInputCompanyName" class="t-light-blue mb-0 font600 font-13">How many Tests Is Your Company Able To Perform On A Daily Basis?</label>
    						<input type="text" class="form-control font-14" id="exampleInputCompanyName" aria-describedby="companyname" placeholder="It takes about 5 minutes per person to perform a test and beg it up.">
  						</div>
					 </div>
				</div>
				<div class="row">
					 <div class="col-md-6">
						<div class="form-group mb-4">
    						<label for="exampleInputCompanyName" class="t-light-blue mb-0 font600 font-13">Address Where Tests Will Be Performed</label>
    						<input type="text" class="form-control font-14" id="exampleInputCompanyName" aria-describedby="companyname" placeholder="If you have more than one sit, please just add one site for now">
  						</div>
					 </div>
					 <div class="col-md-6">
						<div class="form-group mb-4 position-relative">
    						 <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">How Many Sites Do You Have?</label>
						     <div class="select position-relative">
								<select class="form-control font-14" id="exampleFormControlSelect1">
							      <option>Please Select</option>
							      <option>2</option>
							      <option>3</option>
							      <option>4</option>
							      <option>5</option>
						    	</select>
						     </div>

  						</div>
					 </div>
				</div>
				<div class="row">
					 <div class="col-sm-6">
						<div class="form-group mb-4 position-relative">
    						<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">How Many Sites Do You Have?</label>
						    <div class="select position-relative">
							    <select class="form-control font-14" id="exampleFormControlSelect1">
							      <option>Please Select</option>
							      <option>2</option>
							      <option>3</option>
							      <option>4</option>
							      <option>5</option>
							    </select>
						    </div>
						    						    
  						</div>
					 </div>
					 <div class="col-sm-6">
						<div class="form-group mb-4 position-relative">
    						 <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">Do You Currently Offer COVID Testing Services?</label>
    						 <div class="select position-relative">
							     <select class="form-control font-14" id="exampleFormControlSelect1">
							       <option>Please Select</option>
							       <option>Yes</option>
							       <option>No</option>
							     </select>
						 	 </div>						     
  						</div>
					 </div>
				</div>
				<div class="row">
					 <div class="col-sm-6">
						<div class="form-group mb-4 position-relative">
    						 <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">Have You Performed Fit For Travel Covid Test Before</label>
    						 <div class="select position-relative">
	    						 <select class="form-control font-14" id="exampleFormControlSelect1">
							       <option>Please Select</option>
							       <option>Yes</option>
							       <option>No</option>
							     </select>
    						 </div>						    
						     						     
  						</div>
					 </div>
					 <div class="col-sm-6">
						<div class="form-group mb-4 position-relative">
    						 <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">When Would You Like To Start Offering Our Services?</label>
    						 <div class="select position-relative">
							     <select class="form-control font-14" id="exampleFormControlSelect1">
							       <option>Please Select</option>
							       <option>Yes</option>
							       <option>No</option>
							     </select>
						     </div>
  						</div>
					 </div>
				</div>
				<div class="row">
					 <div class="col-md-12">
						<div class="form-group mb-4">
    						 <label for="exampleFormControlSelect1" class="t-light-blue mb-0 font600 font-13">Will You Be Advertising That You Can Now Offer COVID Travel Tests?</label>						    
						     <input type="text" class="form-control font-14" id="exampleInputCompanyName" aria-describedby="companyname" placeholder="If so where you will you be advertising ? Examples: Current email list, banner in the window etc.">
  						</div>
					 </div>
				</div>
				<div class="row">
					<div class="col">						
						<div class="btn-wrap mt-3 mt-md-5 mb-3 text-center">
						 <a href="javascript:void(0);" title="" class="btn-green-lg text-uppercase px-4 py-3 font-weight-bold">looking forward to getting in touch!</a>
						</div>
					</div>
				</div>		
	   		</div>
	   	</div>
		
	
	  </div>
	</div>
	
</section>



<?php  include("footer.php"); ?>