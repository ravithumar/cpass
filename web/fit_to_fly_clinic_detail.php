<?php  include("header.php"); ?>

<section class="add-new-member-form fit-fly py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Add New Member</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="" method="get" accept-charset="utf-8">
						<div class="row align-items-end">
							<div class="col-md-12 mb-4">
								<div class="mem-prof-img border-50">
									<img src="images/member-list-image.jpg" class="border-50" alt="">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Place of Test</label>
									<input type="text" name="" value="Integrative Health" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Test Type</label>
									<input type="text" name="" value="RT-PCR" class="form-control">
								</div>
							</div>
							
							<div class="col-md-6 col-lg-4 mb-4">
								<p class="t-blue font-weight-bold font-14 mb-0"></p>
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Is It the Same Departure Date and Departure Time?</label>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked>
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Departure Date and Time</label>
									<input type="text" name="" value="25 Aug 2021/ 09:30 AM" placeholder="" class="form-control form-control-box font600">
								</div>
							</div>
														
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date</label>
									<div class="relative">
										<input type="text" name="" value="July 25 2021" placeholder="" class="form-control pr-4">
									    <button type="submit" class="date-range-btn"><img src="images/date-range-icon.png" alt=""></button>
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-3">
							   <div class="col">
								<p class="t-light-blue font-weight-bold font-16">Select Time Slot</p>
							   </div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<ul class="time-slot d-flex flex-wrap">
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">9:15 AM</a></li>
									<li class="time-green"><a href="#">9:30 AM</a></li>
									<li class="time-border"><a href="#">9:45 AM</a></li>
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">10:15 AM</a></li>
									<li class="time-border"><a href="#">10:30 AM</a></li>
									<li class="time-red"><a href="#">Booked</a></li>
									<li class="time-border"><a href="#">10:45 AM</a></li>
									
								</ul>
							</div>
						</div>
						<div class="row align-items-end">
							<div class="col-md-6 col-lg-4 mb-4">
								
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Do You Reside In The Same Address?</label>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked="">
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<input type="text" name="" value="WC1N 3AX" placeholder="" class="form-control form-control-box">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<input type="text" name="" value="27 Old Gloucester Street, London" placeholder="" class="form-control form-control-box">
								</div>
							</div>
						</div>	
							
						
						<div class="text-center mt-5">
							<a href="#" title="" class="btn-green-lg m-2">Submit</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>