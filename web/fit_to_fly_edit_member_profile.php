<?php  include("header.php"); ?>

<section class="edit-member-profile-form py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li class="active"><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold mb-3">Edit Profile</h5>
				<div class="input-field-ui bg-grey border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between mt-5 mt-sm-0">
					<form action="" method="get" accept-charset="utf-8">
						<div class="row align-items-end mb-3 mb-md-5">
							<div class="col">
								<div class="mem-prof-img border-50 text-center">
									<img src="images/member-pic.jpg" class="border-50" alt="">
									<!-- <a href="#" class="border-blue mt-3 font-12 d-inline-block border-r14 edit-pic t-blue">Edit Picture</a> -->
									<label class="edit-pic border-blue mt-3 font-12 d-inline-block border-r50 t-blue"><input type="file" name="" value="">edit picture</label>
								</div>
							</div>
						</div>
						<div class="row align-items-end mt-3 mt-md-5">
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Full Name</label>
									<input type="text" name="" value="John Doe" placeholder="John Doe" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Email Address </label>
									<input type="text" name="" value="johndoe@domain.com" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Address </label>
									 <input type="text" name="" value="Lexington USA" placeholder="" class="form-control">
								
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">City</label>
									<input type="text" name="" value="Lexington" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Postcode</label>
									<input type="text" name="" value="98652" placeholder="" class="form-control">
								</div>
							</div>
							
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Gender</label>
									<div class="w-100 d-flex form-control">
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="male" name="example" value="customEx" checked>
											<label class="mb-0 pointer font-15" for="male">Male</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="female" name="example" value="customEx">
											<label class="mb-0 pointer font-15" for="female">Female</label>
										</div>
										<div class="px-0 custom-radio relative">
											<input type="radio" class="custom-control-input" id="Other" name="example" value="customEx">
											<label class="mb-0 pointer font-15" for="Other">Other</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Ethnicity</label>
									<input type="text" name="" value="Enter Ethnicity" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Telephone number (OTP Verification)</label>
									<input type="text" name="" value="+44 988 9888 656" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Nationality </label>
									<input type="text" name="" value="Enter Nationality" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Passport Number</label>
									<input type="text" name="" value="Enter Passport Number" placeholder="" class="form-control">
								</div>
							</div>
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<p class="t-darkblue font600 mb-0">Upload Passport Photo</p>
									<label for="" class="custom-file-label t-darkblue font-13 mb-0">Upload Passport Photo</label>
									<input type="file" class="custom-file-input" id="customFile" name="filename">
								</div>
							</div>
						
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">NHS Number</label>
									<input type="text" name="" value="Enter NHS Number" placeholder="" class="form-control">
								</div>
							</div>
						
							<div class="col-md-6 col-lg-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Date of Birth </label>
									<div class="relative">
										<input type="text" name="" value="Enter Date of Birth" placeholder="" class="form-control pr-4">
									    <button type="submit" class="date-range-btn"><img src="images/date-range-icon.png" alt=""></button>
									</div>
								</div>
							</div>
						
						</div>
						<div class="mt-5 d-flex justify-content-center align-items-center flex-wrap flex-wrap-reverse">
							<a href="change-password.php" class="btn-blue font-16 m-2">Change Password</a>
							<a href="#" class="btn-green-lg font-16 m-2">Save</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>