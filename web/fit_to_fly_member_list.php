<?php  include("header.php"); ?>

<section class="member-list py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li class="active"><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Member List</h5>
				<div class="bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<div class="row">
						<div class="col-md-12 col-lg-6 mb-4">
							<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue">
								<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
									<img src="images/member-list-image.jpg" alt="" class="border-50">
								</div>
								<div class="member-list-detail w-100">
									<h6 class="t-blue font-14 font-weight-bold mb-0">John Doe</h6>
									<p class="mem-location t-black font-13 mb-0">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
									<div class="d-flex align-items-end justify-content-between">
										<div class="mr-2">
											<p class="font-10 font-weight-bold mb-0 t-black">johndoe@domain.com</p>
											<p class="font-10 font-weight-bold mb-0 t-black">+44 655 6625 655</p>
										</div>
										<div>
											<button type="submit" class="mr-2"><img src="images/edit-icon.jpg" alt=""></button>
											<button type="submit"><img src="images/delete-icon.jpg" alt=""></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6 mb-4">
							<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue">
								<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
									<img src="images/member-list-image.jpg" alt="" class="border-50">
								</div>
								<div class="member-list-detail w-100">
									<h6 class="t-blue font-14 font-weight-bold mb-0">John Doe</h6>
									<p class="mem-location t-black font-13 mb-0">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
									<div class="d-flex align-items-end justify-content-between">
										<div class="mr-2">
											<p class="font-10 font-weight-bold mb-0 t-black">johndoe@domain.com</p>
											<p class="font-10 font-weight-bold mb-0 t-black">+44 655 6625 655</p>
										</div>
										<div>
											<button type="submit" class="mr-2"><img src="images/edit-icon.jpg" alt=""></button>
											<button type="submit"><img src="images/delete-icon.jpg" alt=""></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6 mb-4">
							<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue">
								<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
									<img src="images/member-list-image.jpg" alt="" class="border-50">
								</div>
								<div class="member-list-detail w-100">
									<h6 class="t-blue font-14 font-weight-bold mb-0">John Doe</h6>
									<p class="mem-location t-black font-13 mb-0">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
									<div class="d-flex align-items-end justify-content-between">
										<div class="mr-2">
											<p class="font-10 font-weight-bold mb-0 t-black">johndoe@domain.com</p>
											<p class="font-10 font-weight-bold mb-0 t-black">+44 655 6625 655</p>
										</div>
										<div>
											<button type="submit" class="mr-2"><img src="images/edit-icon.jpg" alt=""></button>
											<button type="submit"><img src="images/delete-icon.jpg" alt=""></button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-lg-6 mb-4">
							<div class="d-sm-flex align-items-center bg-white border-r10 h-100 p-3 shadow-blue">
								<div class="member-list-img mr-2 border-50 mb-3 mb-sm-0">
									<img src="images/member-list-image.jpg" alt="" class="border-50">
								</div>
								<div class="member-list-detail w-100">
									<h6 class="t-blue font-14 font-weight-bold mb-0">John Doe</h6>
									<p class="mem-location t-black font-13 mb-0">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
									<div class="d-flex align-items-end justify-content-between">
										<div class="mr-2">
											<p class="font-10 font-weight-bold mb-0 t-black">johndoe@domain.com</p>
											<p class="font-10 font-weight-bold mb-0 t-black">+44 655 6625 655</p>
										</div>
										<div>
											<button type="submit" class="mr-2"><img src="images/edit-icon.jpg" alt=""></button>
											<button type="submit"><img src="images/delete-icon.jpg" alt=""></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="text-center mt-5">
						<a href="add_new_member.php" title="" class="btn-green-lg m-2">Add Member</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>