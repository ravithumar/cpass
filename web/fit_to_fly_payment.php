<?php  include("header.php"); ?>

<section class="add-card py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Payment</h5>
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<form action="" method="get" accept-charset="utf-8">
						<div class="col-lg-10 px-0">
							<h6 class="t-blue font-weight-bold font-14">Test Price</h6>
							<div class="d-flex justify-content-between align-items-center border-bottom py-3">
								<p class="mb-0 t-black font-weight-bold font-14 d-inline-block">Member x3 ($50.00)</p>
								<p class="mb-0 t-blue font-weight-bold font-14 d-inline-block">$150.00</p>
							</div>
							<div class="d-flex justify-content-between pt-3 align-items-center">
								<p class="mb-0 t-black font-weight-bold font-14 d-inline-block">Tax</p>
								<p class="mb-0 t-blue font-weight-bold font-14 d-inline-block">$15.00</p>
							</div>
							<div class="d-flex justify-content-between align-items-center bg-green p-2 px-3 mt-3">
								<p class="mb-0 text-white font-weight-bold font-14 d-inline-block">Total</p>
								<p class="mb-0 text-white font-weight-bold font-16 d-inline-block">$165.00</p>
							</div>
							<div class="mt-4">
								<h6 class="t-blue font-weight-bold font-16 mb-3">Payment Type</h6>
								<ul class="payment-card-list row">
									<li class="col-lg-6 mb-4 active">
										<div class="pointer cards-list d-flex align-items-center bg-white border-r5 shadow-1 p-2 p-xl-3">
											<div class="mr-3">
												<img src="images/master-card.png" alt="">
											</div>
											<div>
												<p class="t-black mb-0 font-16">**** **** **** 6665</p>
												<p class="t-grey mb-0 font-13">Expires 10/2025</p>
											</div>
										</div>
									</li>
									<li class="col-lg-6 mb-4">
										<div class="pointer cards-list d-flex align-items-center bg-white border-r5 shadow-1 p-2 p-xl-3">
											<div class="mr-3">
												<img src="images/visa-card.png" alt="">
											</div>
											<div>
												<p class="t-black mb-0 font-16">**** **** **** 6665</p>
												<p class="t-grey mb-0 font-13">Expires 10/2025</p>
											</div>
										</div>
									</li>
								</ul>
								<div class="mt-4">
									<a href="#" title="" class="mr-3"><img src="images/apple-pay.png" alt=""></a>
									<a href="#" title="" class=""><img src="images/g-pay.png" alt=""></a>
								</div>
							</div>
						</div>
						<div class="text-center pt-lg-5 mt-5">
							<a href="fit_to_fly_add_card.php" title="" class="btn-blue m-2">Add Card</a>
							<a href="#" title="" data-toggle="modal" data-target="#fit-to-flypay-now-popup" class="btn-green-lg m-2">Pay Now</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>