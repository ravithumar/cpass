<?php  include("header.php"); ?>

<section class="add-new-member-form py-5 fit-fly-steps">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				
				<div class="input-field-ui bg-grey shadow border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<ul class="d-flex steps justify-content-center">
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="images/check-mark-blue.png" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-blue">Step 1</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Departing from UK)</p>
                                </div>                               
                              </a>
                            </li>
                            <li class="active position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="images/check-mark-blue.png" alt="" class="img-fluid"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-grey">Step 2</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(Returning to UK)</p>
                                </div>
                              </a>
                            </li>
                            <li class="position-relative">
                              <a href="#" class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center"><img src="images/check-mark-blue.png" alt="" class="img-fluid d-none"></div>
                                <div class="text-wrap">
                                  <p class="font-18 font600 mb-0 t-grey">Step 3</p>
                                  <p class="mb-0 font-14 t-black">Fit to Fly</p>
                                  <p class="mb-0 font-14 t-black">(International Arrival to UK)</p>
                                </div>                            
                              </a>
                            </li>
                    </ul>
					<form action="" method="get" accept-charset="utf-8">
						<div class="row mt-3 mt-md-5">
							<div class="col-lg-6">
								<div id="accordion-select01" class="select-trips">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Return Country</a>								   
								   </div>
								   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select01">
								      <div class=" card-body bg-white pt-5 px-3 pt-5 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li class="mr-3">
						                     <input type="radio" id="country1" name="domestic_radio" value="1" checked="">
						                     <label for="country1" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">India</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="country2" name="domestic_radio" value="2">
						                     <label for="country2" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">USA</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="country3" name="domestic_radio" value="3">
						                     <label for="country3" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">Australia</label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							<div class="col-lg-6">
								
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">During Your Trip, Will You Be Visiting / Stopping Over Or Transiting Through Any Islands or Country?</label>
									<div class="home-address-radio-btn w-100 d-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked="">
											<label class="mb-0 pointer t-blue" for="home-address-yes">Yes</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">No</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-3 mt-md-5">
							<div class="col-lg-6">
								<div id="accordion-select02" class="select-trips">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Visiting Country</a>								   
								   </div>
								   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select02">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li class="mr-3">
						                     <input type="radio" id="country1" name="domestic_radio" value="1" checked="">
						                     <label for="country1" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">India</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="country2" name="domestic_radio" value="2">
						                     <label for="country2" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">USA</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="country3" name="domestic_radio" value="3">
						                     <label for="country3" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">Australia</label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							<div class="col-lg-6">
								<div id="accordion-select03" class="select-trips">
								  <div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>								   
								   </div>
								   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion-select03">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li class="mr-3">
						                     <input type="radio" id="country1" name="domestic_radio" value="1" checked="">
						                     <label for="country1" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">LFT</label>
						                  </li>
                						</ul>
								      </div>
   								    </div>
								</div>
							  </div>
							
							</div>
							
						</div>
						<div class="row mt-3 mt-md-5">
							<div class="col-md-6 mb-4">
                              <div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
                             	<div class="select-date">								 
								  <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0">Select Return Date</label>
						    		<div class="select position-relative">
										<select class="form-control font-14 border-0" id="exampleFormControlSelect1">
									      <option>July 2020</option>
									      <option>July 2021</option>
									      <option>July 2022</option>								      
								    	</select>
						    		
						    		</div>
								    
  								   </div>
                             	</div>
                             	<ul class="d-flex">
                             		<li class="active"><a href="#" class="text-underline font-14 font600">Weekly</a></li>/
                             		<li><a href="#" class="t-black font-14 font600">Monthly</a></li>
                             	</ul>
                             </div>
                             <div class="calender-view">
                             	<input type="text" id="datepicker" placeholder="Date" class="p-2 form-control">
                             </div>    
						    </div>
						    <div class="col-md-6 mb-4">
								<div class="form-group mb-0">
									<label for="" class="shadow-1 t-blue font-weight-bold font-14 mb-2 p-2 border-r7 bg-white">Please Provide The Following Information.</label>
									<div class="home-address-radio-btn w-100 d-block d-sm-flex d-md-block d-lg-flex align-items-center form-control">
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-yes" name="home-address-radio" value="customEx" checked="">
											<label class="mb-0 pointer t-blue" for="home-address-yes">Flight No</label>
										</div>
										<div class="px-0 relative mr-3">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">Vessel No</label>
										</div>
										<div class="px-0 relative">
											<input type="radio" id="home-address-no" name="home-address-radio" value="customEx">
											<label class="mb-0 pointer t-blue" for="home-address-no">Train No</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-4 mb-4">
								<div class="form-group mb-0">
									<label for="" class="t-violet font-weight-bold font-14 mb-0">Enter Flight No</label>
									<input type="text" name="" value="BD1246" placeholder="" class="form-control t-blue">
								</div>
							</div>

							<div class="col-md-8 mb-4">
                              <div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
                             	<div class="select-date">								 
								  <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0">Select Shipping Date</label>
						    		<div class="select position-relative">
										<select class="form-control font-14 border-0" id="exampleFormControlSelect1">
									      <option>July 2020</option>
									      <option>July 2021</option>
									      <option>July 2022</option>								      
								    	</select>
						    		
						    		</div>
								    
  								   </div>
                             	</div>
                             	<ul class="d-flex">
                             		<li class="active"><a href="#" class="text-underline font-14 font600">Weekly</a></li>/
                             		<li><a href="#" class="t-black font-14 font600">Monthly</a></li>
                             	</ul>
                             </div>
                             <div class="calender-view">
                             	<input type="text" id="datepicker" placeholder="Date" class="p-2 form-control">
                             </div>    
						    </div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label for="" class="t-violet font-weight-bold font-14 mb-0">Enter Flight No</label>
								<div class="form-group">
									<div class="row">
									
									<div class="col-md-6">
										<input type="text" name="" value="WC1N 3AX" placeholder="" class="form-control form-control-box add_search-icon mb-3 mb-md-0">
									</div>
									<div class="col-md-6">
									   <select name="" class="form-control form-control-box select-angle-blue">
										<option value="1">27 Old Gloucester Street, London</option>
										<option value="2">27 Old Gloucester</option>
										<option value="3">27 Old Havana</option>
										<option value="4">27 Old pochinki</option>
									   </select>
									</div>
									</div>
								
								</div>
							</div>
						</div>
						
						<div class="text-center mt-5">
							<a href="fit_to_fly_step_03.php" title="" class="btn-green-lg m-2">Next</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>