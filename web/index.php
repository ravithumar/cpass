<?php  include("header.php"); ?>

<section class="banner">
	<div class="hp-banner-container">
		<div class="hp-banner-container-content">
			<div class="row align-items-center">
				<div class="col-xl-5 col-md-6">
					<h4 class="t-violet font-weight-bold">Outpatient base diagnostic center</h4>
					<h1 class="font-weight-bold t-blue">Taking Care of you with the best health services</h1>
					<p class="t-violet">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>
					<a href="#" title="" class="btn-green-lg text-uppercase">book an appointment</a>
				</div>
				<div class="col-xl-7 col-md-6 mt-4 mt-md-0">
					<img src="images/banner-main-img.png" alt="" class="img-fluid banner-main-img">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="hp-what-we-do pt-5 pb-lg-5">
	<div class="hp-banner-container pt-5">
		<div class="hp-banner-container-content ">
			<div class="row align-items-center flex-md-row flex-column-reverse">
				<div class="col-xl-7 col-md-6 mt-4 mt-md-0">
					<img src="images/what-we-do-img.png" alt="" class="img-fluid">
				</div>
				<div class="col-xl-5 col-md-6">
					<h2 class="t-black font-weight-bold mb-4">What We do</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</p>
					<div class="d-sm-flex justify-content-between">
						<div class="text-center">
							<h1 class="font-weight-bold t-blue">350</h1>
							<p class="t-violet">Patients per Day</p>
						</div>
						<div class="text-center mx-4">
							<h1 class="font-weight-bold t-blue">82+</h1>
							<p class="t-violet">Medical Specialties </p>
						</div>
						<div class="text-center">
							<h1 class="font-weight-bold t-blue">9240+</h1>
							<p class="t-violet">Healthy and Satisfied clients </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container mt-5">
		<h2 class="font-weight-bold t-violet text-center mb-5">World Leader in Testing</h2>
		<div class="row">
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">1</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">Clinical biochemistry</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">2</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">Coronavirus care product mix</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">3</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">Rigorous testing in clinical trials</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">4</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">An antibody test</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">5</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">RT-PCR and Combination tests</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
			<div class="col-md-6 col-lg-4 mb-5">
				<div class="testing-list h-100 p-4 border-r10 shadow-blue bg-white">
					<h1 class="font-weight-bold mb-3 pb-0">6</h1>
					<p class="font-weight-bold font-16 t-violet mb-2">Screening Asymptomatic Patients</p>
					<p class="font-14 mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-blue pt-5">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-7">
				<h2 class="font-weight-bold text-white">Download App Now!</h2>
				<p class="text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
				<div>
					<a href="#" title="" class="bg-blue d-inline-block my-2 mr-3"><img src="images/app-store.png" class="border-r10 shadow" alt=""></a>
					<a href="#" title="" class="bg-blue d-inline-block my-2 mr-3"><img src="images/play-store.png" class="border-r10 shadow" alt=""></a>
				</div>
			</div>
			<div class="col-md-5 mt-5 mt-md-0">
				<div class="download-app-img text-center text-md-right">
					<img src="images/download-app.jpg" class="border-r10 shadow" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="py-5 bg-footer-color">
		
	</div>
</section>

<?php  include("footer.php"); ?>