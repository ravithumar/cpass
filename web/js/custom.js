
// sticky header js
$(window).scroll(function(){
  if ($(window).scrollTop() >= 200) {
    $('.menu').addClass('sticky-header');
   }
   else {
    $('.menu').removeClass('sticky-header');
   }
});

// on page change add active class to link
$(function(){
  var current_page_URL = location.href;
  $( ".navbar-nav a" ).each(function() {
     if ($(this).attr("href") !== "#") {
       var target_URL = $(this).prop("href");
       if (target_URL == current_page_URL) {
          $('.navbar-nav a').parents('li').removeClass('active');
          $(this).parent('li').addClass('active');
          return false;
       }
     }
  });
});


// select address
$("ul.my-booking-lists li").on('click', function(){
    $("ul.my-booking-lists li").removeClass('active');
    $(this).addClass('active');
});


// payment-card-list
$("ul.payment-card-list li").on('click', function(){
    $("ul.payment-card-list li").removeClass('active');
    $(this).addClass('active');
});


// onscroll  search box close js
// $(window).on('scroll', function(e){ 
//     $(".search-field1").removeClass('form-active'); 
// });

$( function() {
    $( "#datepicker" ).datepicker();
  } );

// member-list-page
$(".member-list-page a.list-block").on('click', function(){
     $(this).toggleClass("active"); 
     
       
});


// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});


// search box
document.getElementById("click").addEventListener("click", function () {
  document.getElementById("search-field").classList.toggle("form-active");
});

// onscroll  search box close js
$(window).on('scroll', function(e){ 
    $(".search-field1").removeClass('form-active'); 
});


$("#new-booking-radio input[name=domestic_radio]:radio").click(function(ev) {

  if (ev.currentTarget.value == "1") {
    $('#domestic1').parent().removeClass('select-radios');
    $('#domestic2').parent().addClass('select-radios');
    $('.domestic-radio-section').addClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');
    $('.domestic-radio-section').removeClass('add-international');
    
  } 
  else if (ev.currentTarget.value == "2") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-international');
     $('.domestic-radio-section').removeClass('add-fitfly-oneway');

  }
  else if (ev.currentTarget.value == "3") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-international');
    $('.domestic-radio-section').removeClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');

  }

  else if (ev.currentTarget.value == "4") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
    $('.domestic-radio-section').removeClass('add-fitfly-oneway');

  }
    else if (ev.currentTarget.value == "5") {
    $('#domestic1').parent().addClass('select-radios');
    $('#domestic2').parent().removeClass('select-radios');
    $('.domestic-radio-section').addClass('add-fitfly-oneway');
    $('.domestic-radio-section').removeClass('add-international');
    $('.domestic-radio-section').removeClass('add-domestic');
     $('.domestic-radio-section').removeClass('add-fitfly');

  }

});



$("#passenger-status-radio input[name=status_radio]:radio").click(function(ev) {

  if (ev.currentTarget.value == "1") {
    $('#status1').parent().removeClass('select-radios');
    $('#status2').parent().addClass('select-radios');
    $('.residency-radio-section').addClass('add-uk');
    $('.residency-radio-section').removeClass('add-non-uk');    
    
  } 
  else if (ev.currentTarget.value == "2") {
    $('#status1').parent().addClass('select-radios');
    $('#status2').parent().removeClass('select-radios');
    $('.residency-radio-section').addClass('add-non-uk');
    $('.residency-radio-section').removeClass('add-uk');

  }

});