
// sticky header js
$(window).scroll(function(){
  if ($(window).scrollTop() >= 200) {
    $('.menu').addClass('sticky-header');
   }
   else {
    $('.menu').removeClass('sticky-header');
   }
});

// on page change add active class to link
$(function(){
  var current_page_URL = location.href;
  $( ".navbar-nav a" ).each(function() {
     if ($(this).attr("href") !== "#") {
       var target_URL = $(this).prop("href");
       if (target_URL == current_page_URL) {
          $('.navbar-nav a').parents('li').removeClass('active');
          $(this).parent('li').addClass('active');
          return false;
       }
     }
  });
});


// select address
$("ul.my-booking-lists li").on('click', function(){
    $("ul.my-booking-lists li").removeClass('active');
    $(this).addClass('active');
});


// payment-card-list
$("ul.payment-card-list li").on('click', function(){
    $("ul.payment-card-list li").removeClass('active');
    $(this).addClass('active');
});


// onscroll  search box close js
// $(window).on('scroll', function(e){ 
//     $(".search-field1").removeClass('form-active'); 
// });

$( function() {
    $( "#datepicker" ).datepicker();
  } );

// member-list-page
$(".member-list-page a.list-block").on('click', function(){
     $(this).toggleClass("active"); 
     
       
});


// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});