<?php  include("header.php"); ?>

<section class="py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4 shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Member List</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height d-flex flex-column justify-content-between">
					<div class="member-list-page">
						<div class="row align-item-center">
							<div class="col-lg-6">
								<a href="#" class="d-block d-sm-flex  align-items-center list-block bg-white border-r7 p-2 p-xl-3 shadow-3 mb-4">
                                  <span class="media position-relative">
                                  	<img src="images/list-pic01.jpg" alt="Member-pic" class="img-fluid rounded-circle">
                                  </span>
                                  <span class="member-info pl-0 pl-sm-3 d-block mt-2 mt-xl-0">
									<p class="font-14 font600 t-blue mb-0">John Doe</p>
								    <p class="mem-location mb-1 font-10 font600 t-black">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
								    <p class="mb-1 font-10 font600 t-black">johndoe@domain.com</p>
								    <p class="mb-0 font-10 font600 t-black">+44 655 6625 655</p>
                                  </span>
								</a>
							</div>
							<div class="col-lg-6">
								<a href="#" class="d-block d-sm-flex  align-items-center list-block bg-white border-r7 p-2 p-xl-3 shadow-3 mb-4">
                                  <span class="media position-relative">
                                  	<img src="images/list-pic02.jpg" alt="Member-pic" class="img-fluid rounded-circle">
                                  </span>
                                  <span class="member-info pl-0 pl-sm-3 d-block mt-2 mt-xl-0">
									<p class="font-14 font600 t-blue mb-0">John Doe</p>
								    <p class="mem-location mb-1 font-10 font600 t-black">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
								    <p class="mb-1 font-10 font600 t-black">johndoe@domain.com</p>
								    <p class="mb-0 font-10 font600 t-black">+44 655 6625 655</p>
                                  </span>
								</a>
							</div>
							<div class="col-lg-6">
								<a href="#" class="d-block d-sm-flex  align-items-center list-block bg-white border-r7 p-2 p-xl-3 shadow-3 mb-4">
                                  <span class="media position-relative">
                                  	<img src="images/list-pic03.jpg" alt="Member-pic" class="img-fluid rounded-circle">
                                  </span>
                                  <span class="member-info pl-0 pl-sm-3 d-block mt-2 mt-xl-0">
									<p class="font-14 font600 t-blue mb-0">John Doe</p>
								    <p class="mem-location mb-1 font-10 font600 t-black">UK Markey Cancer Centre, 800 Rose St, Lexington</p>
								    <p class="mb-1 font-10 font600 t-black">johndoe@domain.com</p>
								    <p class="mb-0 font-10 font600 t-black">+44 655 6625 655</p>
                                  </span>
								</a>
							</div>
						</div>
					</div>
					<div class="text-center mt-5">
						<a href="complete_your_registration.php" class="btn-blue m-2">Add Member</a>
						<a href="time_slot.php" class="btn-green-lg m-2">Book Now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>