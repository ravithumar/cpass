<?php  include("header.php"); ?>

<section class="my-booking py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li class="active"><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="my-booking-inner bg-grey shadow border-r10 p-4 pt-5 account-div-height">
					<ul class="nav nav-tabs border-r50 justify-content-around justify-content-center mx-auto" role="tablist">
						<li class="nav-item">
							<a class="nav-link border-0 active" data-toggle="tab" href="#tabs-1" role="tab">Schedule</a>
						</li>
						<li class="nav-item">
							<a class="nav-link border-0" data-toggle="tab" href="#tabs-2" role="tab">Historic</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content mt-5">
						<div class="tab-pane fade show active" id="tabs-1" role="tabpanel">
							<ul class="my-booking-lists row">
								<li class="active col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_schedule.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="upcoming font-14 text-white my-2 font-weight-bold mb-0">Upcoming</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<div class="tab-pane fade" id="tabs-2" role="tabpanel">
							<ul class="my-booking-lists row">
								<li class="active col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li class="col-lg-6 mb-4">
									<a href="booking_detail_history.php" title="">
										<div class="my-booking-list-box d-flex bg-white shadow-blue p-2 border-r10">
											<div class="my-booking-lists-div1">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">LFT</p>
												</div>
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Date</label>
													<p class="mb-0 font-12 font-weight-bold t-black">July 22 2021</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Shipping Address</label>
													<p class="mb-0 font-12 font-weight-bold t-black">UK Markey Cancer Centre...</p>
												</div>
											</div>
											<div class="my-booking-lists-div2">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Member</label>
													<p class="mb-0 font-12 font-weight-bold t-black">2</p>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Slot Time</label>
													<p class="mb-0 font-12 font-weight-bold t-black">09:30 AM</p>
												</div>
											</div>
											<div class="my-booking-lists-div3 text-right">
												<div class="mb-1">
													<label class="t-blue font-12 font-weight-bold mb-0">Place of test</label>
													<p class="mb-0 font-12 font-weight-bold t-black">Integrative Health</p>
												</div>
												<div class="mb-1">
													<label class="clone-booking font-14 my-2 font-weight-bold mb-0">Clone Booking</label>
												</div>
												<div class="">
													<label class="t-blue font-12 font-weight-bold mb-0">Booking Reference</label>
													<p class="mb-0 font-12 font-weight-bold t-black">#231264</p>
												</div>
											</div>
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- tab close -->
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>