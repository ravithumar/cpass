<?php  include("header.php"); ?>

<section class="new-booking py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey border-r10 p-4 py-5 account-div-height domestic-radio-section">
					<div class="row">
						<div class="col-lg-6">
							<div id="accordion">
								<div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingOne">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Purpose Of Test</a>								   
								   </div>
								   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block" id="new-booking-radio">
                                          <li>
						                     <input type="radio" id="domestic1" name="domestic_radio" value="1" checked>
						                     <label for="domestic1" class="d-block d-xl-flex align-items-center">
						                     	<p class="mb-0 font-15 font500">Domestic</p>  <p class="mb-0 font-13 font500">(work, events, internal travel, general)</p></label>
						                  </li>
						                  <li>
						                     <input type="radio" id="domestic2" name="domestic_radio" value="2">
						                     <label for="domestic2" class="d-block d-xl-flex align-items-center">
						                     	<p class="mb-0 font-15 font500">Fit to Fly</p>  
											  </label>
                                               <ul class="ml-4 test-purpose d-none">
                                                    <li>
                                                       <input type="radio" id="domestic21" name="domestic_radio" value="4">
						                               <label for="domestic21" class="d-block d-xl-flex align-items-center">
						                     		   <p class="mb-0 font-15 font500">UK Departure & Return (Roundtrip)</p>  
                                                    </li>
                                                    <li>
                                                       <input type="radio" id="domestic22" name="domestic_radio" value="5">
						                               <label for="domestic22" class="d-block d-xl-flex align-items-center">
						                     		   <p class="mb-0 font-15 font500">UK Departure (Oneway Only)</p>  
                                                     </li>
                                                </ul>
						                  </li>
						                  <li>
						                     <input type="radio" id="domestic3" name="domestic_radio" value="3">
						                     <label for="domestic3" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">International Arrival</p>  <p class="mb-0 font-13 font500">(Entry to UK only)</p></label></label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div id="accordion02" class="new-book1">
								<div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Choose Test Type</a>								   
								   </div>
								   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion02">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">Domestic</p>  <p class="mb-0 font-13 font500">(work, events, internal travel, general)</p></label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test4" name="radio-group">
						                     <label for="test4" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">Fit to Fly</p>  <p class="mb-0 font-13 font500">(UK Departure and Return to UK)</p></label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test5" name="radio-group">
						                     <label for="test5" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">International Arrival</p>   <p class="mb-0 font-13 font500">(Entry to UK only)</p></label></label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
								<div class="card border-0 mb-4 mt-3 mt-md-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Place Of Test</a>								   
								   </div>
								   <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion02">
								      <div class="card-body bg-white  pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-flex align-items-center"><p class="mb-0 font-15 font500">Attend Clinic</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test4" name="radio-group">
						                     <label for="test4" class="d-block d-xl-flex align-items-center"><p class="mb-0 font-15 font500">Home Delivery</p>  <p class="mb-0 font-13 font500">(UK only)</p></label>
						                  </li>
						                  

                						</ul>
								      </div>
   								    </div>
								</div>
							</div>
							<div id="accordion03" class="new-book2">
								<div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Country Where You Are Going</a>								   
								   </div>
								   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion03">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                           <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">India</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test4" name="radio-group">
						                     <label for="test4" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">Australia</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test5" name="radio-group">
						                     <label for="test5" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">USA</label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
								<div class="card border-0 mb-4 mt-3 mt-md-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>								   
								   </div>
								   <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion03">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-flex align-items-center mb-1 font-15 font500 pl-0">PCR Test</label></li>
						                  

                						</ul>
								      </div>
   								    </div>
								</div>
							</div>
							<div id="accordion04" class="new-book3">
								<div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Select Your Country Where You Are Going</a>								   
								   </div>
								   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion04">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">India</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test4" name="radio-group">
						                     <label for="test4" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">Australia</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test5" name="radio-group">
						                     <label for="test5" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">USA</label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
								<div class="card border-0 mb-4 mt-3 mt-md-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>								   
								   </div>
								   <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion04">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-flex align-items-center mb-1 font-15 font500 pl-0">RT-PCR</label>
						                 </li>
						                  

                						</ul>
								      </div>
   								    </div>
								</div>
							</div>
							<div id="accordion05" class="new-book4">
								<div class="card border-0 mb-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingTwo">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="collapsed d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Country Arriving From </a>								   
								   </div>
								   <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion05">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">India</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test4" name="radio-group">
						                     <label for="test4" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">Australia</label>
						                  </li>
						                  <li>
						                     <input type="radio" id="test5" name="radio-group">
						                     <label for="test5" class="d-block d-xl-flex align-items-center mb-1 font-15 font500 pl-0">USA</label>
						                  </li>

                						</ul>
								      </div>
   								    </div>
								</div>
								<div class="card border-0 mb-4 mt-3 mt-md-5">
								   <div class="card-header bg-blue p-0 border-r8 shadow-3" id="headingThree">
								   	 <a href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="d-block py-3 py-lg-4 pl-3 pl-lg-4 pr-5 text-white font-18 font600 position-relative">Test Type</a>								   
								   </div>
								   <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion05">
								      <div class="card-body bg-white pt-5 px-3 px-md-4 pb-3">
								        <ul class="mb-3 p-0 radio-block">
                                          <li>
						                     <input type="radio" id="test3" name="radio-group" checked="">
						                     <label for="test3" class="d-flex align-items-center mb-1 font-15 font500 pl-0">PCR Test</label></li>
                						</ul>
								      </div>
   								    </div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-6 border-right">
                             <div class="date-block d-block d-sm-flex d-md-block d-lg-flex justify-content-between">
                             	<div class="select-date">								 
								  <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-15 font-weight-bold t-black mb-0">Select Test Date</label>
						    		<div class="select position-relative">
										<select class="form-control font-14 border-0" id="exampleFormControlSelect1">
									      <option>July 2020</option>
									      <option>July 2021</option>
									      <option>July 2022</option>								      
								    	</select>
						    		
						    		</div>
								    
  								   </div>
                             	</div>
                             	<ul class="d-flex">
                             		<li class="active"><a href="#" class="text-underline font-14 font600">Weekly</a></li>/
                             		<li><a href="#" class="t-black font-14 font600">Monthly</a></li>
                             	</ul>
                             </div>
                             <div class="calender-view mb-3">
                             	<input type="text" id="datepicker" placeholder="02/09/2021" class="p-2 form-control">
                             </div>    
						</div>
						<div class="col-md-6 mt-sm-3 mt-md-0">
							<p class="font-15 font-weight-bold t-black mb-0">Select Test Time</p>
							<div class="row">
								<div class="col-sm-4 col-md-12 col-lg-6 col-xl-4">
								 <div class="select-timing">
								  <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-16 t-black mb-2">Select Hours</label>
								    <div class="select position-relative">
									  <select class="form-control font-17 font600 t-black border-0 px-3 py-2 border-r14" id="exampleFormControlSelect1">
								        <option>10</option>
								        <option>11</option>
								        <option>15</option>								      
								      </select>						    		
								    </div>								   
  								   </div>
								 </div> 	                            	
								</div>
								<div class="col-sm-4 col-md-12 col-lg-6 col-xl-4">
								  <div class="select-timing">
								   <div class="form-group position-relative">
						    		<label for="exampleFormControlSelect1" class="font-16 t-black mb-2">Select Minutes</label>
								    <div class="select position-relative">
									  <select class="form-control font-17 font600 t-black border-0 px-3 py-2 border-r14" id="exampleFormControlSelect1">
								        <option>30</option>
								        <option>40</option>
								        <option>50</option>								      
								      </select>						    		
								    </div>								   
  								   </div>
								 </div>
								</div>
								<div class="col-lg-6"></div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-12">
							<div class="test-center-info">
								<p class="font-15 font-weight-bold t-black">Select A Test Centre</p>
								<ul class="nav nav-tabs border-0" id="myTab">
								  <li class="nav-item">
								    <a class="nav-link active border-0 font-15 t-grey" id="post-code-tab" data-toggle="tab" href="#post-code" role="tab" aria-controls="home" aria-selected="true">Search By Post Code</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link border-0 t-grey font-15" id="country-tab" data-toggle="tab" href="#country" role="tab" aria-controls="profile" aria-selected="false">Search By County</a>
								  </li>
								  <li class="nav-item">
								    <a class="nav-link border-0 t-grey font-15" id="map-tab" href="search-01.php" role="tab" aria-controls="contact" aria-selected="false">Search By Map</a>
								  </li>
								</ul>
								<div class="tab-content border-r7 pt-4 pb-3 px-3 bg-white mt-3 shadow-3" id="myTabContent">
								  <div class="tab-pane fade show active" id="post-code" role="tabpanel" aria-labelledby="post-code-tab">
									<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
									<div class="form-group">
									    <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search">
									</div>
								  </div>
								  <div class="tab-pane fade" id="country" role="tabpanel" aria-labelledby="country-tab">
								  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
									<div class="form-group">
									    <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search">
									</div>
								  </div>
								  <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
								  	<p class="mb-0 font-16 t-blue font-weight-bold">Postcode</p>
									<div class="form-group">
									    <input class="form-control font-12" type="text" placeholder="Please write here" aria-label="Search">
									</div>
								  </div>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="btn-wrap mt-4 justify-content-center text-center">
								<a href="search.php" class="btn-green-lg font-16 font600">Search</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>