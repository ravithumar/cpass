<?php  include("header.php"); ?>

<section class="add-member py-5">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 pt-md-3">
				<div class="bg-blue mt-md-4  shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-4 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<h5 class="t-blue font-24 font-weight-bold">Scan Documents</h5>
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative scan-documents">
					<p class="mb-0 t-red text-center">Please Scan Your ID Documents</p>
					<p class="mb-0 t-red text-center">(Passport, Driving Licence, ID Card)</p>
					
						<div class="row mt-4">
						
							<div class="col-lg-4 col-md-12">
								<div class="form-group position-relative">
									<label for="exampleFormControlSelect1" class="t-light-blue mb-0 font-14 font600">Select the Type of ID</label>
									<div class="select position-relative">
										<select class="form-control padding-r30 font-14" id="exampleFormControlSelect1">
											<option>Passport</option>
											<option>Drving Licence</option>
											<option>ID Card</option>											
										</select>
									</div>
								</div>
							</div>
							
						</div>
						<div class="row mt-3">
							<div class="col-md-3">
								<p class="t-light-blue font-weight-bold font-16 text-center">Scan ID</p>								
								<a href="fit_to_fly_complete_your_registration.php" class="box-container d-block">
									<img src="images/list-pic02.jpg" alt="" class="img-fluid">
									<span class="box1"></span>
									<span class="box2"></span>
									<span class="box3"></span>
									<span class="box4"></span>									
								</a>	
							</div>
						</div>
						
					
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("footer.php"); ?>