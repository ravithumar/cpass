<?php  include("header.php"); ?>

<section class="search-page py-3 py-md-5 mt-0 mt-md-4">
	<div class="container-large">
		<div class="row">
			<div class="col-md-4 col-lg-3 mb-5 mb-md-0">
				<div class="bg-blue shadow border-r10  account-div-height d-flex flex-column justify-content-between">
					<ul class="my-profile-nav pl-1 py-5">
						<li class="active"><a href="new_booking.php" title="" class="prof-nav-booking text-white">New Booking</a></li>
						<li><a href="my_booking.php" title="" class="prof-nav-booking text-white">My Booking</a></li>
						<li><a href="member_list.php" title="" class="prof-nav-member text-white">Members</a></li>
						<li><a href="edit_profile.php" title="" class="prof-nav-edit text-white">Edit Profile</a></li>
						<li><a href="help_support.php" title="" class="prof-nav-help text-white">Help & Support</a></li>
					</ul>
					<a href="index.php" title="" class="logout white-link mb-5">Logout</a>
				</div>
			</div>
			<div class="col-md-8 col-lg-9 mt-5 mt-md-0">
				<div class="bg-grey border-r10 p-4 py-5 account-div-height position-relative">
					<ul class="filter d-flex position-absolute m-0 p-0">
						<li><a href="#" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 mr-2"><img src="images/filter.svg" alt="Filter" class="img-fluid" data-toggle="modal" data-target="#filter-options-popup"></a></li>
						<li><a href="search-01.php" class="d-flex align-items-center justify-content-center bg-white border-r7 shadow-1 ml-2"><img src="images/location.svg" alt="Filter" class="img-fluid"></a></li>
					</ul>
					<div class="row">
						<div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health01.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Integrative Health</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone" data-toggle="modal" data-target="#booking-information-popup">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health02.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Family Care Centre</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health03.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Integrative Health</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health04.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Family Care Centre</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health01.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Integrative Health</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health02.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Family Care Centre</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						  <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health03.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Integrative Health</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						  <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health04.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Family Care Centre</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health01.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Integrative Health</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
						 <div class="col-md-12 col-lg-6">
							<div class="area-block bg-white pl-3 pl-lg-2 py-3 d-block d-sm-flex align-items-center border-r7 shadow-3 mb-4 position-relative">
								<div class="media">
									<img src="images/health02.jpg" alt="Health" class="img-fluid border-r7">
								</div>
								<div class="content-wrapper pl-0 pl-sm-2">
									<div class="top-part pr-2">
									   <p class="font-16 font600 t-blue mb-0">Family Care Centre</p>
									   <span class="location d-flex align-items-center position-relative p-0"><img src="images/small-map.svg" alt="Map" class="position-absolute img-fluid"><span class="text font-12 d-block t-bluefive pl-3">UK Markey Cancer Centre, 800 Rose St, Lexington</span></span>
									    <label class="available d-inline-block p-1 px-2 text-white font-10 mb-0 border-r12">Slot Available : <span class="slots font-weight-bold text-underline">30</span></label>									
									</div>
									<div class="bottom-part mt-2 d-block d-sm-flex align-items-center justify-content-between">
										<div class="price-area d-flex align-items-center">
											<p class="mb-0 t-green font-16 font600 mr-2">$20</p>
											<p class="mb-0 t-light-black font-10">Price for Test</p>
										</div>
										<div class="btn-area position-absolute text-right">
											<a href="#" class="btn-blueone">Book Now</a>
										</div>
									</div>
								 </div>
							</div>
															
						 </div>
												 						
					</div>
						
				</div>
			</div>
	    </div>
	</div>	
</section>

<?php  include("footer.php"); ?>