<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>C - Pass</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/style-2.css">
	<link rel="stylesheet" href="css/responsive.css">
	<!-- <link rel="stylesheet" href="css/slick.css"> -->
</head>

<body>
	<section class="sign-up"> 
 	 <div class="container-fluid align-items-stretch d-block d-lg-flex justify-content-center vh-100">
	 <div class="row text-center w-100">
	 	<div class="col-md-6 p-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="left-one position-relative h-100 align-items-center d-flex justify-content-center w-100 flex-wrap">

	 			 <div class="logo-wrapper mb-3 mb-sm-0">
	 				<img src="images/c-pass-large-logo.png" alt="Logo" class="img-fluid">
	 			 </div>
	 			 <div class="copyright position-absolute t-purple font-12 w-100 text-left">
	 			  ©2021 C PASS
	 			 </div>
		 			
	 		</div>


	 	</div>
	 	<div class="col-md-6 bg-blue p-3 p-sm-4 p-lg-5 align-items-center d-flex justify-content-center">
	 		<div class="right-one position-relative align-items-center w-100">
	 		   <div class="skip-area">
		 		   	<a href="javascript:;" class="skip-one text-right text-white font-16 font600 position-absolute">
                      Skip <img src="images/right-arrow.png" alt="Right Arrow" class="img-fluid ml-2">
			        </a>			   
	 		   </div>	 			
			    <div class="block-area text-left bg-white my-5 my-lg-0 ml-0 ml-sm-3 ml-lg-5 border-r14 p-3 p-md-4  p-xl-5 shadow-2">
			    	<h2 class="font-34 t-bluetwo font-weight-bold">Sign Up</h2>
			    	<p class="t-bluethree font-12">Enter to continue and explore with in</p>
			    	<div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">Full Name</label>
    					<input type="text" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Youraddres@email.com">
  				    </div>
  				    <div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">Email Address (Unique ID for user)</label>
    					<input type="email" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter Your Email Address (Unique ID for user)">
    					<a href="#" class="icon position-absolute">
    						<img src="images/eyes.svg" alt="Eyes" class="img-fluid">
    					</a>
  				    </div>
  				    <div class="form-group mb-3 mb-sm-4 position-relative">
    					<label for="exampleInputFullName" class="mb-0 font500 font-13">Password</label>
    					<input type="password" class="form-control font-14" id="exampleInputFullName" aria-describedby="fullname" placeholder="Enter your password">
    					<a href="#" class="icon position-absolute">
    						<img src="images/eyes.svg" alt="Eyes" class="img-fluid">
    					</a>
  				    </div>
  				    <div class="btn-wrap my-4 my-md-5">
						<a href="#" class="btn-green-lg d-block font600 border-r8">Sign Up</a>
					</div>  				    
  				    <p class="text-center t-bluefour font-12 mb-0">Already Have an Account? <a href="sign-in.php" class="t-green font-14 font600 text-underline">Sign In</a></p>
			    </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/popper.min.js"></script>
<!-- <script src="js/slick.min.js"></script> -->
<script src="js/custom.js"></script>
</body>


</html>